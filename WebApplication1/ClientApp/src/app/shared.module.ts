import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditBankingDetailComponent } from './Components/administators/checkrun/edit-banking-detail/edit-banking-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { RejectClaimPopupComponent } from './Components/administators/checkrun/reject-claim-popup/reject-claim-popup.component';
import { AuthorizationComponent } from './Components/administators/authorization/authorization.component';
import { ContactDetailsComponent } from './Components/shared/contact-details/contact-details.component';

@NgModule({
  declarations: [EditBankingDetailComponent, RejectClaimPopupComponent, AuthorizationComponent, ContactDetailsComponent],
  imports: [
    CommonModule,
    //HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule
  ],
  exports: [EditBankingDetailComponent, RejectClaimPopupComponent, AuthorizationComponent, ContactDetailsComponent]
})
export class SharedModule { }

import { MemberWaitSpecExlModel } from "./MemberWaitSpecExlModel";

export class MemberEligibilityModel{
  membid:string ="" 
  rlship:number 
  lastnm :string =""
  firstnm: string = ""
  email: string = ""
  hpfromdt:Date 
  hpcode :string
  opt :string 
  opfromdt :Date
  opthrudt :string
  birth:string =null
  subssn:string
  relation:string
  dependant :string
  hpname:string 
  lobcode:string 
  termcode:string
  memberStatus: string
  ToServDate: string;
  consultations: number
  days: number
  type: number
  dentist: boolean;
  fromdate: string;
  todate: string;
  waitAndSpecExcls: MemberWaitSpecExlModel[];
}

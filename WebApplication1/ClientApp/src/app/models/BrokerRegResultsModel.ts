export class BrokerRegResultsModel {
  isRegistered: boolean;
  registrationMessage: string;
}

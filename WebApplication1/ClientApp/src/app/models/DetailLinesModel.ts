export class DetailLinesModel {

  public diagdesc: string;
  public diagcode: string;
  public provid: string;
  public provspec: string;
  public provname: string;
  public provdesc: string;
  public claimNo: string;
  public refProvId: string;
  public refProfName: string;
  public specCode: string;
  public desc: string;
  public dateRec: string;
  public tblRowID: string;
  public billed: string;
  public coPay: string;
  public adjCode: string;
  public adjust: string;
  public tarrif: string;
  public net: string;
  public adjCode2: string;
  public comments: string;
  public isRejected: boolean;
  public srvCode: string;
  public srvCodeDesc: string;
  public fromDate: string;
  public tooDate: string;
  public provClaim: string;
  public reversedate: string;
  public claimOriginal: string;
  public claimreverse: string;
  public rejectable: boolean;
  public status: string;
  public clinicalCodes: string;

  //todo: add claim status


}

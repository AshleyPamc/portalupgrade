export class HealthplanModel {

  public name: string;
  public code: string;
  public lobcode: string;
  public activeTab: boolean;
}

export class AdjustcodesModel {
  public code: string;
  public desc: string;
  public notes: string;
}

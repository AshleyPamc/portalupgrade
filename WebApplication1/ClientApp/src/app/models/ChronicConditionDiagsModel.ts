export class ChronicConditionDiagsModel {

  public conditionCode: string;
  public diagCode: string;
  public diagDesc: string;

  public success: boolean;
  public message: string;
  public user: string;
}

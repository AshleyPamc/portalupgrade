export class BenifitResults {
  public days: number;
  public tootno: string;
  public indicate: string;
  public ageMessage: string;
  public age: string;
  public provid: string;
  public membid: string;
  public svcDate: string;
  public months: number;
}

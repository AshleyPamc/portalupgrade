export class UsertypeModel {
  userTypeId: string = '000';
  userType: string;
  createBy: string;
  createDate: string;
  changeBy: string;
  changeDate: string;
  userRegistration:boolean
}

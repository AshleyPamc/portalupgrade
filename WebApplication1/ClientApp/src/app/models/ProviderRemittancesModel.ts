export class ProviderRemittancesModel{
  public provid: string
  public claim: string
  public datepaid:Date
  public net:number
  public file:boolean
  public chprefix:number
}

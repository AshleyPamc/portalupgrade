import { ClaimDetailModel } from "./ClaimDetailModel";

export class ClaimMasterModel {
  hpCode: string = "";
  hpCodeDesc: string = "";
  provHosp: string = "";
  provHospDesc: string = "";
  provId: string = "";
  provName: string = "";
  provSpec: string = "";
  provSpecDescr: string = "";
  vendor: string = "";
  vendorName: string = "";
  refProvId: string = "";
  refProvName: string = "";
  refProvSpec: string = "";
  refProvSpecDescr: string = "";
  provClaim: string = "";
  policyNo: string = "";
  familyOpt: string = "";
  dependant: string = "";
  dependantFirstnm: string = "";
  dependantLastnm: string = "";  
  membId: string = "";
  depOpthruDt: string = "";
  dateReceived: Date = null;
  claimType: string = "";
  claimTypeDesc: string = "";
  authNo: string = "";
  diag1: string = "";
  diag1desc: string = "";
  diag2: string = "";
  diag2desc: string = "";
  diag3: string = "";
  diag3desc: string = "";
  diag4: string = "";
  diag4desc: string = "";
  //diag5: string = "";
  //diag5desc: string = "";
  userName: string = "";
  claimDetails: ClaimDetailModel[] = [];  
  clientCreateDate: string = null;
  apiCreateDate: string = "";
  rec1Id: number;
  fileId: number;
  exception: string = "";

  get detailLinesBilledTotal() : number {
    let total: number = 0;
    this.claimDetails.forEach((el: ClaimDetailModel) => {
      total += Number(el.billed);
    })
    return total;
  }

  CheckValidDates(): string {
    let errLines: string = "";

    // Check if Master received date is before service date on claim detail lines
    this.claimDetails.forEach((el: ClaimDetailModel) => {
      if (this.dateReceived < el.svcDateTo) {
        errLines += el.lineNo + ","
      }
    })  

    // Check if last character is a comma then remove the last comma
    if (errLines.substring(errLines.length - 1, errLines.length) == ",") {
      errLines = errLines.substring(0, errLines.length - 1)
    }

    return errLines;
  }  
}

import { AuthSpecDetail } from "./AuthSpecDetail";

export class SpecCodes {
  public code: string;
  public desc: string;
  public page: number;
  public specDetail: AuthSpecDetail[];
  public selectedProcCode: string;
  public selectedBenCode: string;
  public selected: boolean;
  public procCode: string;
  public benCode: string;
  public id: number;
  public user: string;
  public provId: string;
  public srvDate: string;
  public amount: string;
  public phcode: string;

  public success: boolean;
  public message: string;  
}

export class Recordtype8Model {
  public TYPE: string = "8";
  public LINE: string;
  public ADJCODE: string;
  public ADJUST: string;
  public COMMENT: string;
  public Rec2IDdb: number;
  public Rec2IDArr: number;
}

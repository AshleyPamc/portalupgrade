export class AuthHeadersModel{
	public practice:string;
  public doctor:string;
  public memberno:string= "";
  public dependentcode:string
  public membername:string;
  public plan:string;
  public servicedate:string = "";
  public authno:string;
  public authorizedamount:string;
  public lobcode:string;
  public descr:string;
  public memberid:string;
  public memberdob: Date;
  public reff: string;
}

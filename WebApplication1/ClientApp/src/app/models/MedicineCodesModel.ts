export class MedicineCodesModel {
    
  public svcCode: string;
  public phCode: string;
  public svcDesc: string;
    
  // Validations vars
  public success: boolean;
  public message: string;
  public user: string;
}

export class Recordtype1Model {
  public TYPE: number = 1;
  public PHCODE: string; 
  public PROVCLAIM: string; 
  public EZCAPAUTH: string; 
  public EZCAPPROV: string; 
  public EZCAPMEMB: string; 
  public PLACE: string; 
  public OUTCOME: string;
  public EDIREF: string;
  public CASENUM: string;
  public UNITS: number;
  public Rec0ID: number;
  public Rec1ID: number;
  public CLAIMTYPE: string; 
  public REFPROVID: string; 
  public VENDORID: string; 
  public DATERECD: string;
  public BATCH_NO: string;
  public EXT_SWITCH: string;
  public hpCode: string;
  public lobCode: string;
  public membDepCode: string;
  public createBy: string;
}

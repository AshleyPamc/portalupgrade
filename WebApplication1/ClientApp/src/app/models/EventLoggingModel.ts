export class EventLoggingModel {
  public amount: string;
  public eventDesc: string;
  public member: string;
  public provid: string;
  public username: string;
  public uuid: string;
}

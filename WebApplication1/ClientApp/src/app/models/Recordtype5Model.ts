export class Recordtype5Model {
  public TYPE: number = 5;
  public DIAGREF: number;
  public DIAG: string;
  public INPUTROW: number;
  public MASTERROW: number;
  public FILENAME: string;
  public ROWID: number;
  public FieldID: number;
}

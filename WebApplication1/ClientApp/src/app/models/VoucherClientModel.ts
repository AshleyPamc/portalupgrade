export class VoucherClientModel {

  // From DB
  clientId: string = "0";
  clientName: string = "";
  balance: string = "0.00";
  contactPerson: string = "";
  telNo: string = "";
  email: string = "";
  hpCode: string = "";
  opt: string = "";
  createDate: string;

  // Validation params
  success: boolean = false;
  message: string;

  // Frontend params
  user: string;
  startDate: string;
  endDate: string
}

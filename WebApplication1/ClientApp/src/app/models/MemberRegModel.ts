export class MemberRegModel{
  public membEmail: string
  public membNum: string =""
  public memId: string = ""
  public memPass: string
  public membConfirmpass: string
  public membName:string
  public membDOB: string
  public passworderrorlabel: string;
  public showErrormessage: boolean;
  public showSucessmessage: boolean;
}

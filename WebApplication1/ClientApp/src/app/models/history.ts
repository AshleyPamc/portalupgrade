export class VoucherHistory {

  public success: boolean;
  public time: string;
  public voucherNo: string;
  public voucherName: string;
  public voucherBasket: string;
  public redeemed: boolean;
  public errorMsg: string;

}

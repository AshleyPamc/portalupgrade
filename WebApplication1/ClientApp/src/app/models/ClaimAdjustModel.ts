export class ClaimAdjustModel{
  public claimno: string
  public claimtblrow : string
  public sequence : string
  public adjcode: string
  public adjust: string
  public comments : string
  public createby: string
  public createdate: string
  public lastchangeby: string
  public lastchangedate: string
  public rowid: string
}


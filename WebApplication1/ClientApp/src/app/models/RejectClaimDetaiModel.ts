export class RejectClaimDetailModel {

  public  claimNo: string;
  public  tblRowId: string;
  public  rejectionReason: string;
  public  rejectCode: string;
  public reverse: string;
  public notes: string;
}

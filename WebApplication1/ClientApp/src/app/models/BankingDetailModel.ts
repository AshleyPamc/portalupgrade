export class BankingDetailModel {

  public provId: string;
  public claimno: string;
  public claimType: string;
  public batchNo: string;
  public bankName: string;
  public branchCode: string;
  public accountNo: string;
  public mainPolicyNo: string;
  public accType: string;
  public user: string;
  public hpcode: string;

}

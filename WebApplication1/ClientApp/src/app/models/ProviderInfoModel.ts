export class ProviderInfoModel {  
  public provid: string;  
  public lastName: string;
  public specCode: string;
  public specCodeDescr: string;
}

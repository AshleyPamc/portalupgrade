export class FamilyModel {

  public mainPolicyNumber: string;
  public nameSurnamePrincipleMember: string;
  public bankAccountName: string;
  public bankAccountNumber: string;
  public bankAccountBranchCode: string;
  public page: number;
  public validated: boolean;
  public exclude: boolean;
  public rejected: boolean;

}

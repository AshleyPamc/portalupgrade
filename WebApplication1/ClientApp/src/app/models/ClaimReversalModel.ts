export class ClaimReversalModel {
  memberNumber:string
  healthPlan: string;
  serviceDate: string;
  claimNumber: string;
  chargedAmount: string;
  paidAmount: string;
  paidDate:string
  recievedDate: string;
  showClaimModal: boolean;

}

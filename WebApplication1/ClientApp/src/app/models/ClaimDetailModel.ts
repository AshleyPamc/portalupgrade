export class ClaimDetailModel {
  arrayId: string = "";
  lineNo: number;
  procCode: string = "";
  procCodeDesc: string = "";
  procCodePadded: string = "";
  phCode: string = "";
  phCodeDesc: string = "";
  svcDateFrom: Date = null;
  svcDateTo: Date = null;
  billed: string = "";
  qty: number = 0;
  diag: string = "";
  diagDesc: string = "";
  rejCode: string = "";
  rejCodeDesc: string = "";
  tooth1: string = "";
  tooth1Desc: string = "";
  tooth2: string = "";
  tooth2Desc: string = "";
  tooth3: string = "";
  tooth3Desc: string = "";
  tooth4: string = "";
  tooth4Desc: string = "";
  tooth5: string = "";
  tooth5Desc: string = "";
  tooth6: string = "";
  tooth6Desc: string = "";
  tooth7: string = "";
  tooth7Desc: string = "";
  tooth8: string = "";
  tooth8Desc: string = "";
  modifier1: string = "";
  modifier1Desc: string = "";
  modifier2: string = "";
  modifier2Desc: string = "";
  modifier3: string = "";
  modifier3Desc: string = "";
  modifier4: string = "";
  modifier4Desc: string = "";
  note: string = "";
  rec2Id: number;
  toothNumString: string = "";
  modCodesString: string = "";

  //GetToothNumString(): string {
  //  // Build tooth number string
  //  let toothNumStr: string = "";
  //  if (this.tooth1 != "") { toothNumStr += this.tooth1 + ","; }
  //  if (this.tooth2 != "") { toothNumStr += this.tooth2 + ","; }
  //  if (this.tooth3 != "") { toothNumStr += this.tooth3 + ","; }
  //  if (this.tooth4 != "") { toothNumStr += this.tooth4 + ","; }
  //  if (this.tooth5 != "") { toothNumStr += this.tooth5 + ","; }
  //  if (this.tooth6 != "") { toothNumStr += this.tooth6 + ","; }
  //  if (this.tooth7 != "") { toothNumStr += this.tooth7 + ","; }
  //  if (this.tooth8 != "") { toothNumStr += this.tooth8 + ","; }

  //  // Check if last character is a comma then remove the last comma
  //  if (toothNumStr.substring(toothNumStr.length - 1, toothNumStr.length) == ",") {
  //    toothNumStr = toothNumStr.substring(0, toothNumStr.length - 1)
  //  }
  //  return toothNumStr;
  //}

  //GetModCodesString(): string {
  //  // Build mod code string
  //  let modCodesStr: string = "";
  //  if (this.modifier1 != "") { modCodesStr += this.modifier1 + ","; }
  //  if (this.modifier2 != "") { modCodesStr += this.modifier2 + ","; }
  //  if (this.modifier3 != "") { modCodesStr += this.modifier3 + ","; }
  //  if (this.modifier4 != "") { modCodesStr += this.modifier4 + ","; }

  //  // Check if last character is a comma then remove the last comma
  //  if (modCodesStr.substring(modCodesStr.length - 1, modCodesStr.length) == ",") {
  //    modCodesStr = modCodesStr.substring(0, modCodesStr.length - 1)
  //  }
  //  return modCodesStr;
  //}  
}

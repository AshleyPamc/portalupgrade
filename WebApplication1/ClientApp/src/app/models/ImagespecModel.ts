export class ImagespecModel{
    imgDRC:string;
    widthDRC:number;
    heightDRC:number;

    imgPAMC:string;
    widthPAMC:number;
  heightPAMC: number;


    imgCombined:string;
    widthCombined:number;
    heightCombined:number;

    imgPositionVertical:number;
    imgPositionHorizontal:number;
}

import { VendoridsModel } from './VendoridsModel';

export class ProviderAndVendorInfoModel {
  public provid: string;
  public lastName: string;
  public specCode: string;
  public specCodeDescr: string;
  public vendors: VendoridsModel[];
}


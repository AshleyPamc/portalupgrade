export class FileItemModel {
    
  public name: string;
  public type: string;
  public fileDesc: string;
  public isEnabled: boolean;
  public isProgress: boolean;
  public isProgressValue: string;
  public base64Str: string; 
  
}

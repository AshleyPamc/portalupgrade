export class AllocationListModel {
  basketId: string;
  basketAmount: number;
  fullPrice: number;
  qty: number;
  clientId: string;
  
  // Validation params
  success: boolean = false;
  message: string;

  // Frontend params
  user: string;
}

export class PlaceOfServiceModel {
  public code: string;
  public descr: string;
  public default_YN: Boolean;
}

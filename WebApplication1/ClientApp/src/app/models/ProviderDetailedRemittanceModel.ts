export class ProviderDetailedRemittanceModel{
	public claimno:string
    public provid:string
    public membid:string
    public datefrom:Date
    public billed:number
    public net:number
    public datepaid:Date
}
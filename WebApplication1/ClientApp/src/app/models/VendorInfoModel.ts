export class VendorInfoModel
{
  public vendorType: string;
  public vendorTypeDescr: string;
  public vendorId: string;
  public vendorName: string;   
  public specCode: string;
  public specCodeDescr: string;
}

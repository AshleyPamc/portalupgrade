export class RejectListTotalsModel {

  public claimNo: string;
  public total: string;
  public memberNo: string;
  public productCode: string;
  public amountPaid: string;
}

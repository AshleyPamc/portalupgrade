export class VoucherHpModel {

  // From DB
  hpCode: string = "";
  opt: string = "";
  frontFacingOptDesc: string = "";
  prefix: string = "";
  hpName: string = "";
  lobCode: string = "";
  
  // Validation params
  success: boolean = false;
  message: string;
}

export class ProviderList {

  public bankName: string;
  public accNo: string;
  public branch: string;
  public providerID: string;
  public refProvId: string;
  public practiceName: string;
  public validated: boolean;
  public excluded: boolean;
  public rejected: boolean;
  public page: number;
  public hasBanking: boolean = false;
  public show: boolean = true;
}

export class MemberInfoModel {
  public familynumber: string;
  public firstname: string;
  public lastname: string;
  public healthplan: string;
  public option: string;
  public relation: string;
  public dependant: string;
  public status: string;
}

export class PatientWellnessFormModel {
  public idNo: string;
  public name: string;
  public surname: string;
  public tel: string;
  public cell: string;
  public gender: string;
  public address: string;
  public email: string;
  public medicalAidYN: string;
  public medicalAidPlan: string;
  public medicalAidNo: string;
  public medicalAidDepCode: string;
  public screeningDate: string;
  public assessmentConsent: string;
  public chronicConditions: string;
  public medications: string;
  public height: string;
  public weight: string;
  public bmi: string;
  public waist: string;
  public bloodPressureSystolic: string;
  public bloodPressureDiastolic: string;
  public cholesterolHDL: string;
  public cholesterolTRI: string;
  public cholesterolLDL: string;
  public cholesterolTotal: string;
  public bloodSugarMmols: string;
  public hivConsent: string;
  public sexPastSixMonths: string;
  public condomPastSixMonths: string;
  public getAidsWorry: string;
  public exposedAidsWorry: string;
  public monthLastAidsTest: string;
  public yearLastAidsTest: string;
  public toldAids: string;

  public success: boolean;
  public message: string;

  public user: string;
}

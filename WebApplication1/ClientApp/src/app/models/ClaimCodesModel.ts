export class ClaimCodesModel {

  public id: string;
  public mediRef: string;
  public drcRef: string;
  public provid: string;
  public membId: string;
  public name: string;
  public surname: string;
  public prefix: string;
  public phCode: string;
  public hpCode: string;  
  public option: string;
  public code: string;
  public desc: string;
  public enterQty: boolean;
  public enterTooth: boolean;
  public toothNo: string;
  public qty: string;
  public singleUnitAmount: string;
  public total: string = "0.00";
  public diag: string;
  public user: string;
  public membpay: string = "0.00";
  public provpay: string = "0.00";
  public createClaim: boolean;

}

export class MedicationModel {

  // Params to manage list items in code
  public arrayId: string;  
  public section: string;
  public condCode: string;
  //public hypertensionTherapy: boolean;
  //public hyperlipidaemiaTherapy: boolean;

  // Data to be saved to DB
  public icd10: string;  
  public prescriptionSvcCode: string;
  public svcCodeDescr: string;
  public strength: string;
  public dosage: string;
  public qtyPerMonth: string;
  public numRepeats: string;  
  public historyMed: boolean;
  public comorbid: boolean;
  
        
  // From Database
  public conditionApplicationNumber: string;
  public rowid: string;
}

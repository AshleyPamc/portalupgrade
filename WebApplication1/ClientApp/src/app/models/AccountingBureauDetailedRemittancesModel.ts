export class AccountingBureauDetailedRemittancesModel{
    public prefix:number;
    public payee:string;
    public vendorid:string;
    public amount:number;
}
export class ClaimsModel {

  public user: string;
  public specCode: string;
  public hpCode: string;
  public endDate: string = "";
  public sum: string;
  public amountOfRecords: string;
  public specCodeDesc: string;
  public claimType: string;
  public whereClause: string;
  public sorting: string;
  public provid: string;
  public membid: string;
  public claimno: string;
  public serviceToDate: string;

}

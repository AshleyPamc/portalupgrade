import { Recordtype1Model } from './Recordtype1Model';
import { Recordtype2Model } from './Recordtype2Model';
import { Recordtype4Model } from './Recordtype4Model';
import { Recordtype5Model } from './Recordtype5Model';
import { Recordtype8Model } from './Recordtype8Model';

export class Objectlog {
  public header: Recordtype1Model = new Recordtype1Model()
  public detail: Recordtype2Model[] = [];
  public notes: Recordtype4Model[] = [];
  public diags: Recordtype5Model[] = [];
  public rejects: Recordtype8Model[] = [];
}

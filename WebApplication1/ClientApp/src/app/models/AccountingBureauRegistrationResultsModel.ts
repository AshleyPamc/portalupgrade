export class AccountingBureauRegistrationResultsModel{
     isRegistered: boolean;
     registrationMessage:string;
     matches:number;
     name:string;
}

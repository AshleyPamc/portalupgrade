export class BrokerRegModel {
  email: string = "";
  brokerId: string = "";
  name: string = "";
  password: string = "";
  confirmPassword: string = "";
  passworderrorlabel: string;
  failedRegistration: boolean;
  successRegistration: boolean;
  registrationMessage: string = "";
}

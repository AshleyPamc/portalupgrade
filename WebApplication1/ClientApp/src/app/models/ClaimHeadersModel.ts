export class ClaimHeadersModel{
  provname: string;
    practice:string;
    doctor:string;
    memberNo:string="";
    dependent:string;
    memberName:string;
    plan: string;
    receivedDate: string;
  serviceDate: string;
  serviceDateFrom: string;
  serviceDateTo: string;
  reversedDate: string;
    claimNo:string;
    isPaid:string;
    status:string;
    charged:string;
    paid:string;
    accountNo:string;
    datePaid:string;
    checkNo:number;
    memberDOB:Date;
    memberId:string="";
    providerId:string="";
  paidTo: string;
  opt: string;
  memberLastName: string;
  memberFirstName: string;
  contracted: string;

}

export class VoucherTransactionModel {

  // From DB
  tranId: string = "0";
  tranCode: string = "";
  description: string = "";
  amount: string = "0.00";
  clientId: string = "";
  voucherId: string = "";
  voucherBatch: string = "";
  notes: string = "";
  createDate: string = "";

  //Validation Params
  success: boolean = false;
  message: string;

  //Front end params
  user: string;
  balance: string;
}


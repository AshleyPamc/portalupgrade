export class UserRightsModel {

  public checkrunTab: boolean;
  public claimCaptureTab: boolean;
  public claimSearchRejection: boolean;
  public benefitlookup: boolean;
  public releaseCheckrun: boolean;
  public claimSearchReverse: boolean;
  public editReleaseBatch: boolean;
  public reportTab: boolean;
  public authUploadTabd: boolean;
  public authsubmisionTab: boolean;
  public benSetupTab: boolean;
  public authLogTab: boolean;
  public singleAuth: boolean;
  public membership: boolean;
  public membClaims: boolean;
  public provClaims: boolean;
  public accRegister: boolean;
  public Settings: boolean;
  public hpSetup: boolean;
  public RegUsers: boolean;
  public manuals: boolean;
  public claimsRestore: boolean;
  public userRights: boolean;
  public voucherPartners: boolean;
  public medivouch: boolean;
  public chronicConditions: boolean;

}

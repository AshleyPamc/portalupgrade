export class AuthSpecDetail
{
  public procCode: string;
  public benCode: string;
  public phCodeOnNetwork: string;
  public phCodeOnNetworkDesc: string;
  public phCodeOffNetwork: string;
  public phCodeOffNetworkDesc: string;
  public phCodeDetermined: string = '';
  public phCodeDeterminedDesc: string = '';
  public placeSvcCode: string;
  public displayAvailableBenefit: boolean;
  public enterAmount: boolean;
  public availableBenefitToAmtCheck: boolean;
  public includeLengthOfStay: boolean;
}


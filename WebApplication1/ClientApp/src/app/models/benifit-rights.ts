export class BenifitRights {
  public benName: string;
  public opt: string;
  public hp: string;
  public provid: string;
  public membid: string;
  public enabled: boolean;
  public defaultProc: boolean;
  public id: number;
  public results: string[] = [];
  public preAuth: boolean;
  public reff: string;
  public dentice: boolean;
}

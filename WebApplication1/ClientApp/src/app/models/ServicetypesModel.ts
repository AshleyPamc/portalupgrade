export class ServicetypesModel {
  phCode: string;
  phDesc: string;
  phOpt: string;
  hpCode: string;
  type: string;
}

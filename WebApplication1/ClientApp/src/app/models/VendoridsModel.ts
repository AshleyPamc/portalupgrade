export class VendoridsModel {
  public provider: string;
  public vendor: string;
  public lastName: string;
  public displayItem: string;
}

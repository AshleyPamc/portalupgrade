import { MedicationModel } from './MedicationModel';
import { FileItemModel } from './FileItemModel';
import { MemberEligibilityModel } from './MemberEligibilityModel';

export class ChronicConditionApplicationModel {

  // Base info (applicable to all conditions)
  public conditionApplicationNumber: string;
  public conditionCode: string;
  public conditionDescription: string;
  public membId: string;
  public membFirstName: string;
  public membLastName: string;
  public membEmail: string;
  public provId: string;
  public provEmail: string;
  public referredYN: string;
  public refProvSpec: string;
  public refProvid: string;
  public refProvSurname: string;
  public refProvNotes: string;
  public referralLetterFiles: FileItemModel[];
  public applicationDate: string;
  public status: string;
  public cancellationNote: string;

  // Base Biometric
  public weight: string;
  public height: string;
  public bmi: string;  
  public bloodPresSystolic: string;
  public bloodPresDiastolic: string;
  public cholesterolHDL: string;
  public cholesterolLDL: string;
  public cholesterolTRI: string;
  public cholesterolTotal: string;
  public cholesterolLdlHdlRatio: string;
  public diabetesYN: string;
  public diabetesType: string;
  public ogttResult: string;
  public hba1c: string;
  public fastingGlucoseResult: string;
  public randomGlucoseResult: string;
  public diabTestDate: string;  

  //Risk Factors  
  public predisposingRiskFactors: string;
  public alcohol: string;
  public obesity: string;
  public smoker: string;
  public smokeInPast: string;
  public cessationDate: string;
  public preExistingComplications: string;
  public ischaemicHeartDisease: string;
  public peripheralVascularDisease: string;
  public strokeAttacks: string;

  // Conditions Applied For
  public selectedConditions: string[];

  // Diabetes melutis
  public diabetesIcd10: string;  
  public diabPathResultFiles: FileItemModel[];
   
  // Hyperlipidaemia
  public hyperlipidaemiaIcd10: string;  
  public hyperlipidaemiatherapyYN: string;
  public hyperlipidaemiaTherapyDuration: string;
  public hyperlipidaemiaMedHist: MedicationModel[];
  public hyperlipPathResultFiles: FileItemModel[];
  public hyperlipidaemiaAtheroscleroticYN: string;     
  public hyperlipidaemiaHypertensionTreatmentYN: string;
  public hyperlipidaemiaGeneticHyperlipidaemiaYN: string;
  public hyperlipidaemiaMaleBloodRelativeYN: string;
  public hyperlipidaemiaFemaleBloodRelativeYN: string;
  public hyperlipidaemiaTendonXanthomaYN: string;

  // Hypertension
  public hypertensionIcd10: string;
  public hypertensionSeverity: string;  
  public hypertensionTherapyYN: string;
  public hypertensionMedHist: MedicationModel[];

  // Asthma
  public asthmaIcd10: string;
  public asthmaPleakFlow: string;
  public asthmaYounger3YN: string;
  public asthmaPaedFiles: FileItemModel[];
  public asthmaFlowVolFiles: FileItemModel[];

  // Requested medications
  public requestedMeds: MedicationModel[];
  public medPrescriptionFiles: FileItemModel[];

  // Additional Docs
  public additionalDocsFiles: FileItemModel[];

  // Comorbids
  public comorbidsExist: boolean;

  public asthmaCmICD10: string;
  public asthmaCmMedList: MedicationModel[];

  public diabCmICD10: string;
  public diabCmMedList: MedicationModel[];

  public hyperlipCmICD10: string;
  public hyperlipCmMedList: MedicationModel[];

  public hypertensionCmICD10: string;
  public hypertensionCmMedList: MedicationModel[];

  public addisCmICD10: string;
  public addisCmMedList: MedicationModel[];

  public bipolCmICD10: string;
  public bipolCmMedList: MedicationModel[];

  public bronchCmICD10: string;
  public bronchCmMedList: MedicationModel[];

  public cardiacCmICD10: string;
  public cardiacCmMedList: MedicationModel[];

  public cardiomCmICD10: string;
  public cardiomCmMedList: MedicationModel[];

  public copdCmICD10: string;
  public copdCmMedList: MedicationModel[];

  public renalCmICD10: string;
  public renalCmMedList: MedicationModel[];

  public coronarCmICD10: string;
  public coronarCmMedList: MedicationModel[];

  public crohnsCmICD10: string;
  public crohnsCmMedList: MedicationModel[];

  public diabinsipCmICD10: string;
  public diabinsipCmMedList: MedicationModel[];

  public dysrhytCmICD10: string;
  public dysrhytCmMedList: MedicationModel[];

  public epilCmICD10: string;
  public epilCmMedList: MedicationModel[];

  public glaucomCmICD10: string;
  public glaucomCmMedList: MedicationModel[];

  public haemophilCmICD10: string;
  public haemophilCmMedList: MedicationModel[];

  public hypothCmICD10: string;
  public hypothCmMedList: MedicationModel[];

  public sclerosisCmICD10: string;
  public sclerosisCmMedList: MedicationModel[];

  public parkinsCmICD10: string;
  public parkinsCmMedList: MedicationModel[];

  public rheumaCmICD10: string;
  public rheumaCmMedList: MedicationModel[];

  public schizoCmICD10: string;
  public schizoCmMedList: MedicationModel[];

  public lupusCmICD10: string;
  public lupusCmMedList: MedicationModel[];

  public ulceraCmICD10: string;
  public ulceraCmMedList: MedicationModel[];


  // When searching for submitted applications for whole family
  public familyMembers: MemberEligibilityModel[];

  // Validation Params
  public success: boolean;
  public message: string;

  // Frontend params
  public user: string;
  public currentPage: string;
}

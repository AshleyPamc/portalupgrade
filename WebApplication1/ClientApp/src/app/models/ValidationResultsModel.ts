export class ValidationResultsModel {
  valid: boolean;
  message: string;
}

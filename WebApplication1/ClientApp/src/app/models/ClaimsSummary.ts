export class ClaimsSummary {
  public Hpcode: string ;
  public Opt: string ;
  public IsRiskOpt: string ;
  public ServiceYear: number ;
  public ServiceMonth: number ;
  public PaidYear: number ;
  public PaidMonth: number;
  public ReceivedYear: number ;
  public ReceivedMonth: number ;
  public Billed: number ;
  public Net: number ;
  public Status: string ;
  public ServiceYM: string;
}

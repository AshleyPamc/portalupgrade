export class ChronicConditionCodesModel {
   
  public code: string;
  public description: string;
  public selectable: string;

  // Validation Params
  public success: boolean;
  public message: string;

  // Frontend params
  public user: string;
  public currentPage: string;
}

export class MemberWaitSpecExlModel {
  public membId: string;
  public dependant: string;
  public description: string;
  public fromDate: string;
  public toDate: string;
}

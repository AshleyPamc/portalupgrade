export class UserRightsGridModel {
  public  username: string;
  public objectid: string;
  public enabled: boolean;
  public description: string;
}

import { CheckrunModel } from './CheckrunModel';

export class PatientModel {

  public patientMembNo: string;
  public idNumberofMember: string;
  public patientName: string;
  public patientBirth: string;
  public claimNo: string;
  public amountToPay: string;
  public amoutPayed: string;
  public detailLineArray: CheckrunModel[] = [];
  public validated: boolean;
  public exclude: boolean;
  public reject: boolean;
  public rowCount: number;

}

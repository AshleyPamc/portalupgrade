export class Specmasters {
  public hpcode: string;
  public opt: string;
  public bentype: string;
  public desc: string;
  public id: number;
  public user: string;
  public phcode: string;
}

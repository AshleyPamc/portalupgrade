export class Recordtype4Model {
  NOTE: string;
  TYPE: number = 4;
  LINE: number;
  Rec2ID: number;
  Rec2ArrID: number;
}

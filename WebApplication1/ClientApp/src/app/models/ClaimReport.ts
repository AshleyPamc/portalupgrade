export class ClaimReport {
  public  dateCreated:string;
  public  batchNo:string;
  public  provid:string;
  public  memb:string;
  public  billed:string;
  public  hpcode:string;
  public detailLines: string;
  public user: string;
  public ezcapClaimNo: string;
  public page: number;
  public capturedFrom: string;
  public capturedTo: string;
  public userClaimsOnly: boolean;
  public username: string;
}

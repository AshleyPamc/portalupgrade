export class BatchDetailsModel {

  public paymentType: string;
  public batchNumber: string;
  public whereClause: string;
  public paydate: string;
  public hpCode: string;
  public sorting: string;
}

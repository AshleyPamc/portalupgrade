export class DefaultCodesModel {
  public heading: string;
  public user: string;
  public membName: string;
  public membIDNo: string;
  public membSurname: string;
  public prefix: string;
  public hpCode: string;
  public option: string;  
  public id: string;
  public enabled: boolean;
  public added: boolean;
  public code: string;
  public phcode: string;
  public enterQty: boolean;
  public defaultQty: number;
  public enterTooth: boolean;

  public dependantOnCode: boolean;
  public depCode: string;
  public desc: string;
  public headDesc: string;
  public createClaim: boolean;
  public page: number;
  public price: string;
  public fullPrice: string;
  public provid: string;
  public provSpec: string;
  public provClass: string;
  public total: string;
  public membContact: string;
  public toothNo: string;
  public merchantID: string;
}

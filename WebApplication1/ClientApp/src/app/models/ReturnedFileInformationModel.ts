export class ReturnedFileInformationModel{
  public parsedDate: string;
  public fileName: string;
}

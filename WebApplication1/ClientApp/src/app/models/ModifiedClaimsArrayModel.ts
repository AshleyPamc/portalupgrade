export class ModifiedClaimsArrayModel {

  public description: string;
  public specCode: string
  public amountOfClaims: string;
  public memberPayment: string;
  public providerPayment: string;
  public include: boolean;

}

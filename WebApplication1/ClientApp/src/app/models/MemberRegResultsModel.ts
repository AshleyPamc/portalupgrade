export class MemberRegResultsModel {
  public isRegistered: boolean;
  public registrationMessage: string;
  public registrationUser: string;
}

export class PopUpModel {
  public msgId: number;
  public heading: string;
  public body: string;
  public filename: string;
  public width: number;
  public height: number;
  public show: boolean;
  public forceAccept: boolean;
  public username: string;
  public filetype: number;
  public modalsize: number;
}

export class SettingsModel{
    origanalSettingsId:string
    settingsId: string
    description: string
    required: boolean
    enabled: boolean
    createBy :string;
    createDate:Date;
    changeBy:string;
    changeDate:Date;
    administrator:boolean;
    administratorRequired:boolean;
    provider:boolean;
    providerRequired:boolean;
}
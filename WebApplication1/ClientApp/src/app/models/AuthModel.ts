export class AuthModel {

  public id: string;
  public desc: string; // Description of auth set (Gp Auth, Specialist etc)
  public membno: string;
  public dep: string;
  public hpcode: string;
  public opt: string;  

  public spec: string;
  public specDesc: string;
  public proc: string;
  public procCode: string;  
  public phCode: string;
  public phCodeDesc: string;
  public benType: string;
  public benDesc: string;

  public provid: string;
  public scvdate: string;
  public amount: string;
  public placeSvc: string;

  public refno: string;
  public authno: string;

  public diag1: string = "";
  public diag1desc: string = "";
  public diag2: string = "";
  public diag2desc: string = "";
  public diag3: string = "";
  public diag3desc: string = "";
  public diag4: string = "";
  public diag4desc: string = "";
  public qty: number;
  public authRefNo: string = "";
  public note: string = "";
  public reject: boolean = false;

  public username: string;  
  public create: string;   
  public benefitId: string;
  public displayAvailableBenefit: boolean;
  public enterAmount: boolean = false;
  public availableBenefitToAmtCheck: boolean;
  public includeLengthOfStay: boolean;
}

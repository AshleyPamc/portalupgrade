export class Recordtype2Model {
  public ModCodesString: string;
  public toothNumString: string;
  public rec2IDDb: number;
  public rec2IDArr: number;
  public Rec2IDDb: number;
  public Rec2IDArr: number;
  public TYPE: number = 2;
  public PHCODE: string = 'P';
  public FROMDATE: any;
  public TODATE: any;
  public PROCCODE: string;
  public CURR_MODCODE: string
  public CURR_MODAMOUNT: string
  public MODCODE: string
  public MODAMOUNT: string
  public MODCODE2: string
  public MODAMOUNT2: string
  public MODCODE3: string
  public MODAMOUNT3: string
  public MODCODE4: string
  public MODAMOUNT4: string
  public QTY: string;
  public BILLED: any;
  public DIAGREF: number = 1;
  public BUNDLER: string
  public BUNDLERTYP: string
  public COPAY: string
  public MANDISCOUNT: string
  public TRANSACT_NO: string
  public PROCCODE_DESC: string
  public CLINICALCODE1: string
  public CLINICALCODE2: string
  public CLINICALCODE3: string
  public CLINICALCODE4: string
  public CLINICALCODE5: string
  public CLINICALCODE6: string
  public CLINICALCODE7: string
  public CLINICALCODE8: string
  public MEMO1: string
  public MEMO2: string
  public MEMO3: string
  public MEMO4: string
  public MEMO5: string
  public MEMO6: string
  public MEMO7: string
  public MEMO8: string
  public MEMO9: string
  public MEMO10: string
  public Rec1ID: number;
  public hpCode: string;
  
}


export class RejectionCodesModel {
  public code: string;
  public reason: string;
  public lobCode: string
  public username: string;
  public userType: number;

}

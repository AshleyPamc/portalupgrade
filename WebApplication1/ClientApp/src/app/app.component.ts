import { Component, Input, Inject, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RejectionCodesModel } from './models/RejectionCodesModel';
import { AppService } from './services/app.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DefaultCodesModel } from './models/DefaultCodesModel';
import { VoucherHistory } from './models/history';
import { Faq } from './models/faq';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  //public showCookie: boolean = true;
  public showCookieNotice: boolean = false;
  public showCookieFullInfoModal: boolean = false;
  @Input() PasswordForm: FormGroup;
  @Input() TandCForm: FormGroup;
  private _closemodel: boolean;
  private _downloadpdf: boolean;
  public openSurvey: boolean = false;
  public url: string = this._appService.SettingService.surveyLink;
  public urlSafe: SafeResourceUrl;
  public lobCode: RejectionCodesModel;
  public claims: boolean = false;
  public voucher: boolean = false;
  public info: boolean = false;
  public membership: boolean = false;

  rejectionReasons: RejectionCodesModel[] = [];

  constructor(public router: Router, public _appService: AppService, @Inject('BASE_URL') public baseUrl: string,
    private _http: HttpClient, private fb: FormBuilder, private fbc: FormBuilder, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) {
    if (this._appService.LoginService.IsLoggedIn) {
      this.router.navigateByUrl('/dashboard');
     
     
    } else {
      router.navigateByUrl('/login');
      
    }

    //this.showCookie = true;

    // Cookie
    let c = localStorage.getItem("Cookies");
    if (c == undefined || c == null || c != "Accepted") {
      this.showCookieNotice = true;
    } else if (c == "Accepted") {
      this.showCookieNotice = false;
    }

  }

  ngOnInit() {

    this.PasswordForm = this.fb.group({
      old: ['', Validators.required],
      new: ['', Validators.required],
      confirm: ['', Validators.required]
    });

    this.TandCForm = this.fbc.group({
      accepted: []

    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this._appService.LoginService.ScreenWidth = window.innerWidth;    
  }

  @HostListener('window:online', ['$event'])
  onLine(e: any) {
    this.addEntry("online");
    alert("Internet connection restored");
  }

  @HostListener('window:offline', ['$event'])
  offLine(e: any) {
    this.addEntry("offline");
    alert("No internet connection");
  }

  addEntry(onoff: string) {
    // Parse any JSON previously stored in allEntries
    var existingEntries = JSON.parse(localStorage.getItem("ConnectivityLog"));
    if (existingEntries == null) existingEntries = [];
    //var entryTitle = document.getElementById("entryTitle").value;
    //var entryText = document.getElementById("entryText").value;
    var entry = {
      "title": onoff,
      "text": formatDate(new Date(), 'yyyy/MM/dd HH:mm:ss', 'en')
    };
    //localStorage.setItem("entry", JSON.stringify(entry));
    // Save allEntries back to local storage
    existingEntries.push(entry);
    localStorage.setItem("ConnectivityLog", JSON.stringify(existingEntries));
  };

  public get closemodel(): boolean {
    return this._closemodel;
  }
  public set closemodel(value: boolean) {
    this._closemodel = value;
  }
  public get downloadpdf(): boolean {
    return this._downloadpdf;
  }
  public set downloadpdf(value: boolean) {
    this._downloadpdf = value;
  }

  MembershipToggle() {
    this.membership = false;
  }
  //HideCookie() {
  //  //localStorage.setItem("Cookies", "Accepted");
  //  this.showCookie = false;
  //}
  ShowCookieFullInfoModal() {
    this.showCookieFullInfoModal = true;
  }

  DismissCookieFullInfoModal() {
    this.showCookieFullInfoModal = false;
    this.HideCookieNotice();
  }

  HideCookieNotice() {
    this.showCookieNotice = false;
    localStorage.setItem("Cookies", "Accepted");
  }
  onClick() {
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
      if (this._appService.LoginService.ScreenWidth >= 768) {
        this.joyrideService.startTour({
          steps: ['Dash_1', 'Dash_2', 'Dash_3', 'Dash_4'],
          themeColor: '#313131'
        })
      }
      else {
        this.joyrideService.startTour({
          steps: ['smDash_2', 'smDash_3', 'smDash_4', 'smDash_6', 'smDash_8'],
          themeColor: '#313131'
        })
      }  
    }
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {

      if (this._appService.LoginService.ScreenWidth >= 768) {
        this.joyrideService.startTour({
          steps: ['Dash_2', 'Dash_3', 'Dash_4', 'Dash_6', 'Dash_8'],
          themeColor: '#313131'
        })
      }
      else {
        this.joyrideService.startTour({
          steps: [ 'smDash_2', 'smDash_3', 'smDash_4', 'smDash_6', 'smDash_8'],
          themeColor: '#313131'
        })
      }      
    }
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 2) {
      this.joyrideService.startTour({
        steps: ['Dash_1', 'Dash_2', 'Dash_3', 'Dash_4'],
        themeColor: '#313131'
      })
    }
  }

  //NavigateToMedivouch() {
  //  this.router.navigateByUrl('Medivouch');
  //}

  //VoucherClick(setting: string) {

  //  // Set defaults
  //  this._appService.MedivouchService.InsuranceVoucher = false;
  //  this._appService.MedivouchService.HpCode = "";
  //  this._appService.MedivouchService.Prefix = "";
  //  this._appService.MedivouchService.Option = "";

  //  // Set service variables accorrding to param passed
  //  if (setting == "Insurance") {
  //    this._appService.MedivouchService.InsuranceVoucher = true;
  //    this._appService.MedivouchService.GetBasketHpCodes(this._appService.LoginService.CurrentLoggedInUser.specCode, this._appService.LoginService.CurrentLoggedInUser.provClass);
  //  } else if (setting == "MDV") {
  //    this._appService.MedivouchService.Prefix = 'MV';
  //    this._appService.MedivouchService.HpCode = 'MDV';
  //    this._appService.MedivouchService.Option = 'Medivouch';
  //  } else if (setting == "DRC") {
  //    this._appService.MedivouchService.Prefix = 'DRC'
  //    this._appService.MedivouchService.HpCode = 'DRCV';
  //    this._appService.MedivouchService.Option = 'ORAL';
  //  }

  //  // Open voucher redemption
  //  this.router.navigateByUrl('vouchers');
  //}


  CheckPasswordOld() {

    this._appService.LoginService.passwordChange.oldPassword = this.PasswordForm.get("old").value;
    this._appService.LoginService.CheckPasswordOld();
  }
  CheckPasswordNew() {
    this._appService.LoginService.passwordChange.newPassword = this.PasswordForm.get("new").value;
    this._appService.LoginService.CheckPasswordNew();
  }
  CheckPasswordConfrim() {
    this._appService.LoginService.passwordChange.confirmPassword = this.PasswordForm.get("confirm").value;
    this._appService.LoginService.CheckPasswordConfrim();
  }

  AcceptedTandC() {
    let x: boolean;
    x = this.TandCForm.get('accepted').value;

    if (x == true) {
      this._appService.LoginService.UpdateTCAccepted();
    }
    if (this._downloadpdf == true) {
      this.closemodel = true;
    } else {
      this.closemodel = false;
    }
  }

  Close() {
    this._appService.LoginService.poptcModel = false;
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
      this.onClick();
    }
  }

  ValidateClose() {
    this._downloadpdf = true;

    let x: boolean;
    x = this.TandCForm.get('accepted').value;

    if (x) {
      this.closemodel = true;
    } else {
      this.closemodel = false;
    }
  }

  OnDoSurveyClick() {
    this._appService.SettingService.showSurvey = false;
    this._appService.openSurvey = true;
  }

  GetDefaultCodeForHistVoucher(data: VoucherHistory) {
    if (data.success) {
      let c: DefaultCodesModel = new DefaultCodesModel();
      c.provid = this._appService.LoginService.CurrentLoggedInUser.provID;
      let voucherNo: string;
      let d: string = data.voucherNo;
      voucherNo = data.voucherNo + "_";
      voucherNo = voucherNo + data.voucherBasket;



      c.id = voucherNo;
      this._appService.MedivouchService.voucherNo = c.id;
      this._appService.MedivouchService.GetDefaultCodeDetails(c);
    } else {
      this._appService.MedivouchService.voucherError = true;
      this._appService.MedivouchService.voucherErrorMsg = data.errorMsg;
    }

  }
  GetFaqForProviders() {
    let c: Faq = new Faq();
    c.usertype = "1";
    c.answer = "";
    c.question = "";
    this._appService.SecurityService.GetFaqs(c);
  }
}

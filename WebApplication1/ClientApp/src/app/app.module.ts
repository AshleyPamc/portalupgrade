//import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './Components/shared/login/login.component';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JoyrideModule } from 'ngx-joyride';
import { NgxEchartsModule } from 'ngx-echarts';
import { MembershipComponent } from './Components/shared/membership/membership.component';
import { ProvclaimsComponent } from './Components/shared/provclaims/provclaims.component';
import { ClaimInfoComponent } from './Components/shared/claim-info/claim-info.component';
import { InfoDetailLinesComponent } from './Components/shared/info-detail-lines/info-detail-lines.component';
import { RejectClaimComponent } from './Components/shared/reject-claim/reject-claim.component';
import { MemberClaimsComponent } from './Components/shared/member-claims/member-claims.component';
import { MemberClaimService } from './services/member-claim.service';
import { MemberClaimInfoComponent } from './Components/shared/member-claim-info/member-claim-info.component';
import { AuthsComponent } from './Components/shared/auths/auths.component';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import localeENZA from '@angular/common/locales/en-ZA';
import { ClaimHistoryComponent } from './Components/shared/claim-history/claim-history.component';
import { DownloadsComponent } from './Components/shared/downloads/downloads.component'
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ChronicCondAppComponent } from './Components/chronic-cond/chronic-cond-app/chronic-cond-app.component';
import { ChronicCondSubmittedListComponent } from './Components/chronic-cond/chronic-cond-submitted-list/chronic-cond-submitted-list.component';
import { ChronicCondUpdateComponent } from './Components/chronic-cond/chronic-cond-update/chronic-cond-update.component';
import { MemberFormComponent } from './Components/chronic-cond/chronic-cond-form-components/member-form/member-form.component';
import { ProviderFormComponent } from './Components/chronic-cond/chronic-cond-form-components/provider-form/provider-form.component';
import { BiometricFormComponent } from './Components/chronic-cond/chronic-cond-form-components/biometric-form/biometric-form.component';
import { RiskFactorsFormComponent } from './Components/chronic-cond/chronic-cond-form-components/risk-factors-form/risk-factors-form.component';
import { DeclarationFormComponent } from './Components/chronic-cond/chronic-cond-form-components/declaration-form/declaration-form.component';
import { DiabetesFormComponent } from './Components/chronic-cond/chronic-cond-form-components/diabetes-form/diabetes-form.component';
import { HyperlipidaemiaFormComponent } from './Components/chronic-cond/chronic-cond-form-components/hyperlipidaemia-form/hyperlipidaemia-form.component';
import { HypertensionFormComponent } from './Components/chronic-cond/chronic-cond-form-components/hypertension-form/hypertension-form.component';
import { AsthmaFormComponent } from './Components/chronic-cond/chronic-cond-form-components/asthma-form/asthma-form.component';
import { ConditionFormComponent } from './Components/chronic-cond/chronic-cond-form-components/condition-form/condition-form.component';
import { MedicationFormComponent } from './Components/chronic-cond/chronic-cond-form-components/medication-form/medication-form.component';
import { RequestedMedicationFormComponent } from './Components/chronic-cond/chronic-cond-form-components/requested-medication-form/requested-medication-form.component';
import { ChronicCondDetailViewComponent } from './Components/chronic-cond/chronic-cond-detail-view/chronic-cond-detail-view.component';
import { ComorbidFormComponent } from './Components/chronic-cond/chronic-cond-form-components/comorbid-form/comorbid-form.component';
import { DocumentsFormComponent } from './Components/chronic-cond/chronic-cond-form-components/documents-form/documents-form.component';
import { ClaimCaptureMainComponent } from './Components/administators/claim-capture-main/claim-capture-main.component';
import { ClaimCaptureNewComponent } from './Components/administators/claim-capture-main/claim-capture-new/claim-capture-new.component';
import { ViewCapturedClaimsComponent } from './Components/administators/claim-capture-main/view-captured-claims/view-captured-claims.component';


registerLocaleData(localeENZA);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthsComponent,
    DownloadsComponent,
    MembershipComponent,
    ProvclaimsComponent,
    ClaimInfoComponent,
    InfoDetailLinesComponent,
    RejectClaimComponent,
    ClaimHistoryComponent,
    MemberClaimsComponent,
    MemberClaimInfoComponent,    
    ChronicCondAppComponent,
    ChronicCondSubmittedListComponent,
    ChronicCondUpdateComponent,    
    MemberFormComponent,
    ProviderFormComponent,
    BiometricFormComponent,
    RiskFactorsFormComponent,
    DeclarationFormComponent,
    DiabetesFormComponent,
    HyperlipidaemiaFormComponent,
    HypertensionFormComponent,
    AsthmaFormComponent,
    ConditionFormComponent,
    MedicationFormComponent,
    RequestedMedicationFormComponent,
    ChronicCondDetailViewComponent,
    ComorbidFormComponent,
    DocumentsFormComponent,
    ClaimCaptureMainComponent,
    ClaimCaptureNewComponent,
    ViewCapturedClaimsComponent
  ],
  imports: [
    //BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgxEchartsModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },      
      { path: 'voucher-clients', loadChildren: () => import('./Components/voucheradmin/voucher-clients/voucher-clients.module').then(m => m.VoucherClientsModule) },
      { path: 'voucher-transactions', loadChildren: () => import('./Components/voucheradmin/voucher-transactions/voucher-transactions.module').then(m => m.VoucherTransactionsModule) },
      { path: 'administratordash', loadChildren: () => import('./Components/administators/administrator.module').then(m => m.AdministratorModule) } ,
      { path: 'Report', loadChildren: () => import('./Components/administators/reports/reports.module').then(m => m.ReportsModule) },
      { path: 'downloads', component: DownloadsComponent },
      { path: 'auths', component: AuthsComponent },
      { path: 'Auth-log', loadChildren: () => import('./Components/administators/log-new-auth/logauth.module').then(m => m.LogauthModule) },
      { path: 'membership', component: MembershipComponent },
      { path: 'claims', component: ProvclaimsComponent },
      { path: 'member-claims', component: MemberClaimsComponent },
      { path: 'bureau-remittances', loadChildren: () => import('./Components/bureau/remittances/remittances.module').then(m => m.RimittancesModule) },
      { path: 'ben-setup', loadChildren: () => import('./Components/administators/ben-setup/bensetup.module').then(m => m.BensetupModule) },
      { path: 'vouchers', loadChildren: () => import('./Components/provider/medivouch/medivouch.module').then(m => m.MedivouchModule) },
      { path: 'patient-wellness-form', loadChildren: () => import('./Components/provider/patient-wellness-form/patient-wellness-form.module').then(m => m.PatientWellnessFormModule) },
      { path: 'ClaimHistory', component: ClaimHistoryComponent },
      { path: 'claim-capture', loadChildren: () => import('./Components/administators/claim-capture/claim-capture.module').then(m => m.ClaimCaptureModule) },
      { path: 'checkrun', loadChildren: () => import('./Components/administators/checkrun/checkrun.module').then(m => m.CheckrunModule) },
      { path: 'provider-dash', loadChildren: () => import('./Components/provider/provider.module').then(m => m.ProviderModule) },
      { path: 'UploadAuths', loadChildren: () => import('./Components/provider/provider-file-upload/provider-file-upload.module').then(m => m.ProviderFileUploadModule) },
      { path: 'provider-remittances', loadChildren: () => import('./Components/provider/provremit/provremit.module').then(m => m.ProvremitModule) },
      { path: 'bureau-dash', loadChildren: () => import('./Components/bureau/bureau.module').then(m => m.BureauModule) },
      { path: 'bureau-practices', loadChildren: () => import('./Components/bureau/practices/practices.module').then(m => m.PracticeModule) },
      { path: 'benefits', loadChildren: () => import('./Components/provider/benefit-lookup/benefitlookup.module').then(m => m.BenefitlookupModule) },        
      { path: 'chronic-cond-app', component: ChronicCondAppComponent },
      { path: 'chronic-cond-submitted-list', component: ChronicCondSubmittedListComponent },
      { path: 'chronic-cond-update', component: ChronicCondUpdateComponent },
      { path: 'claim-capture-main', component: ClaimCaptureMainComponent, children: [
          { path: '', redirectTo: '/claim-capture-main', pathMatch: 'full' },
          { path: 'claim-capture-new', component: ClaimCaptureNewComponent },
          { path: 'view-captured-claims', component: ViewCapturedClaimsComponent }
        ]
      }      
    ]),
    ClarityModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    JoyrideModule.forRoot(),
    NgxDropzoneModule
  ],  
  providers: [{ provide: LOCALE_ID, useValue: 'en-za' }, DatePipe,],
  bootstrap: [AppComponent]
})
export class AppModule { }

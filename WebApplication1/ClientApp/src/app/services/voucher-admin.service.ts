import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';

import { VoucherClientModel } from '../models/VoucherClientModel';
import { VoucherTransactionModel } from '../models/VoucherTransactionModel';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { DefaultCodesModel } from '../models/DefaultCodesModel';
import { AllocationListModel } from '../models/AllocationListModel';


@Injectable({
  providedIn: 'root'
})
export class VoucherAdminService {

  private _username: string;

  private _busy: boolean;
  private _error: boolean;
  private _errorMessageHeading: string;
  private _errorMessage: string;

  private _showAddNewClientModal: boolean = false;
  private _showEditClientModal: boolean = false;

  private _voucherClient: VoucherClientModel;
  private _voucherClients: VoucherClientModel[] = [];

  private _clientBalance: string;
  private _clientBalanceAllocation: string;
  private _totalAllocatedR: string;
  private _totalAllocatedQty: number;
  private _clientTransaction: VoucherTransactionModel;
  private _clientTransactions: VoucherTransactionModel[] = [];

  private _showDepositModal: boolean = false;

  private _showAllocationModal: boolean = false;

  private _basketLists: DefaultCodesModel[] = [];
  private _basketDetailLists: DefaultCodesModel[] = [];

  private _allocationList: AllocationListModel[] = [];

  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string) { }

  public get username(): string {
    return this._username;
  }
  public set username(value: string) {
    this._username = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get errorMessageHeading(): string {
    return this._errorMessageHeading;
  }
  public set errorMessageHeading(value: string) {
    this._errorMessageHeading = value;
  }
  public get errorMessage(): string {
    return this._errorMessage;
  }
  public set errorMessage(value: string) {
    this._errorMessage = value;
  }
  public get showAddNewClientModal(): boolean {
    return this._showAddNewClientModal;
  }
  public set showAddNewClientModal(value: boolean) {
    this._showAddNewClientModal = value;
  }
  public get showEditClientModal(): boolean {
    return this._showEditClientModal;
  }
  public set showEditClientModal(value: boolean) {
    this._showEditClientModal = value;
  }

  get VoucherClients() {
    return this._voucherClients;
  }
  set VoucherClients(value: VoucherClientModel[]) {
    this._voucherClients = value;
  }
  get VoucherClient() {
    return this._voucherClient;
  }
  set VoucherClient(value: VoucherClientModel) {
    this._voucherClient = value;
  }

  get ClientTransaction() {
    return this._clientTransaction;
  }
  set ClientTransaction(value: VoucherTransactionModel) {
    this._clientTransaction = value;
  }
  get ClientTransactions() {
    return this._clientTransactions;
  }
  set ClientTransactions(value: VoucherTransactionModel[]) {
    this._clientTransactions = value;
  }
  get ClientBalance() {
    return this._clientBalance;
  }
  set ClientBalance(value: string) {
    this._clientBalance = value;
  }
  get ClientBalanceAllocation() {
    return this._clientBalanceAllocation;
  }
  set ClientBalanceAllocation(value: string) {
    this._clientBalanceAllocation = value;
  }
  get TotalAllocatedR() {
    return this._totalAllocatedR;
  }
  set TotalAllocatedR(value: string) {
    this._totalAllocatedR = value;
  }

  get TotalAllocatedQty() {
    return this._totalAllocatedQty;
  }
  set TotalAllocatedQty(value: number) {
    this._totalAllocatedQty = value;
  }

  public get showDepositModal(): boolean {
    return this._showDepositModal;
  }
  public set showDepositModal(value: boolean) {
    this._showDepositModal = value;
  }

  public get showAllocationModal(): boolean {
    return this._showAllocationModal;
  }
  public set showAllocationModal(value: boolean) {
    this._showAllocationModal = value;
  }

  public get basketLists(): DefaultCodesModel[] {
    return this._basketLists;
  }
  public set basketLists(value: DefaultCodesModel[]) {
    this._basketLists = value;
  }
  public get basketDetailLists(): DefaultCodesModel[] {
    return this._basketDetailLists;
  }
  public set basketDetailLists(value: DefaultCodesModel[]) {
    this._basketDetailLists = value;
  }
  public get AllocationList(): AllocationListModel[] {
    return this._allocationList;
  }
  public set AllocationList(value: AllocationListModel[]) {
    this._allocationList = value;
  }



  GetVoucherClients(searchFilter: string = "") {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._voucherClients = [];
    this._http.get(this._baseUrl + 'api/VoucherAdmin/GetVoucherClients',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: VoucherClientModel[]) => {
        results.forEach((el) => {
          this._voucherClients.push(el);
        })

        // filter list if search was done
        if (searchFilter != "") {
          this._voucherClients = this._voucherClients.filter(x => x.clientName.toUpperCase().includes(searchFilter.toUpperCase()))
        }

      },
        (error) => {
          console.log(error)
        },
        () => {
          this._busy = false;
        })
  }

  CreateNewClient(addNewClientForm: FormGroup) {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/VoucherAdmin/CreateNewClient', this.VoucherClient,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((result: VoucherClientModel) => {

        if (result.success) {
          this._voucherClients = [];                             // Clear clients list
          this._voucherClients.push(result);                     // Put newly added client into list
          this._voucherClient = new VoucherClientModel;          // Clear current client
          this.showAddNewClientModal = false;
          addNewClientForm.reset();
        }
        else {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Create New Client Failed";
          this._errorMessage = result.message;
        }
      },
        (error) => {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Create New Client Failed";
          this._errorMessage = "";
          console.log(error)
        },
        () => {
          this._busy = false;
        })
  }

  UpdateClient(clientToEdit: VoucherClientModel, clientForm: FormGroup) {

    // Clear
    this._voucherClient = clientToEdit;

    // Call Api
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/VoucherAdmin/UpdateClient', this._voucherClient,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((result: VoucherClientModel) => {

        if (result.success) {
          this._voucherClients = [];                             // Clear clients list
          this._voucherClients.push(result);                     // Put newly added client into list
          this._voucherClient = new VoucherClientModel;    // Clear current client
          this.showEditClientModal = false;
          clientForm.reset();
        }
        else {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Update Client Failed";
          this._errorMessage = result.message;
        }
      },
        (error) => {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Update Client Failed";
          this._errorMessage = "";
          console.log(error)
        },
        () => {
          this._busy = false;
        })
  }

  DeleteClient(clientToDelete: VoucherClientModel) {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/VoucherAdmin/DeleteClient', clientToDelete,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((result: ValidationResultsModel) => {

        if (result.valid) {
          this._voucherClients = [];                 // Clear clients list
          this.GetVoucherClients();                  // Reload clients after delete  
        }
        else {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Delete Client Failed";
          this._errorMessage = result.message;
        }
      },
        (error) => {
          /*this._busy = false;*/
          this._error = true;
          this._errorMessageHeading = "Delete Client Failed";
          this._errorMessage = "";
          console.log(error)
        },
        () => {
          this._busy = false;
        })
  }

  GetClientTransactions(client: VoucherClientModel, fromDate: string = "", toDate: string = "") {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._clientTransactions = [];
    this._http.post(this._baseUrl + 'api/VoucherAdmin/GetClientTransactions', client,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: VoucherTransactionModel[]) => {
        results.forEach((el) => {
          this._clientTransactions.push(el);
        })

        if (this._clientTransactions.length > 0) {
          this._clientBalance = this._clientTransactions[0].balance;
        }
      },
        (error) => {
          console.log(error)
        },
        () => {
          this._busy = false;
        })
  }

  SaveDeposit(depositForm: FormGroup) {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/VoucherAdmin/SaveDeposit', this.ClientTransaction,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((result: VoucherTransactionModel) => {

        if (result.success) {

          this.GetClientTransactions(this.VoucherClient);            // Get transactions for current client
          this._clientTransaction = new VoucherTransactionModel;     // Clear current transaction
          this.showDepositModal = false;
          depositForm.reset();
        }
        else {
          /*this._busy = false;*/
          this._errorMessageHeading = "Save Deposit Failed";
          this._errorMessage = result.message;
          this._error = true;
        }
      },
        (error) => {
          /*this._busy = false;*/
          this._errorMessageHeading = "Save Deposit Failed";
          this._errorMessage = "";
          this._error = true;
          console.log(error);
        },
        () => {
          this._busy = false;
        })
  }

  GetBasketHeaders() {
    let c: DefaultCodesModel = new DefaultCodesModel();
    //c.provid = this._loginService.CurrentLoggedInUser.provID;
    c.hpCode = this._voucherClient.hpCode;
    c.option = this._voucherClient.opt;
    this._busy = true;
    this._http.post(this._baseUrl + 'api/VoucherAdmin/GetBasketHeaders', c
    ).subscribe((results: DefaultCodesModel[]) => {
      this.basketLists = [];
      this.basketLists = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        //this._showEmail = false;
      },
      () => {
        this.GetBasketDetails();
        this._busy = false;
      })
  }

  GetBasketDetails() {
    let c: DefaultCodesModel = new DefaultCodesModel();
    //c.provid = this._loginService.CurrentLoggedInUser.provID;
    c.hpCode = this._voucherClient.hpCode;
    c.option = this._voucherClient.opt;
    this._busy = true;
    this._http.post(this._baseUrl + 'api/VoucherAdmin/GetBasketDetails', c
    ).subscribe((results: DefaultCodesModel[]) => {
      this.basketDetailLists = [];
      this.basketDetailLists = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        //this._showEmail = false;
      },
      () => {
        this.SetupAllocationList();
        this._busy = false;
        this._showAllocationModal = true;
      })
  }

  GetTotalView(data: string): string {
    let count: number = 0;
    let code: number = 0;
    this.basketDetailLists.forEach((el) => {
      if (el.id == data) {
        count = count + (+el.total);
      }
    })
    //console.log(this._appService.MedivouchService.codeList)
    return count.toFixed(2);
  }

  SetupAllocationList() {
    this.TotalAllocatedR = "0.00";
    this.TotalAllocatedQty = 0;
    this.ClientBalanceAllocation = this.ClientBalance;

    // Clear list each time model is opend
    this.AllocationList = [];

    //Create new list from baskets returned
    this.basketLists.forEach(basket => {

      let lstItem: AllocationListModel = new AllocationListModel();
      lstItem.basketId = basket.id;
      lstItem.basketAmount = Number(this.GetTotalView(basket.id));      
      lstItem.qty = 0;
      lstItem.clientId = this.VoucherClient.clientId;
      lstItem.user = this.username;
      lstItem.fullPrice = Number(basket.fullPrice);
      this.AllocationList.push(lstItem);
    });
  }

  UpdateAllocationList(event, basketId) {
    //this.totalAllocated = this.totalAllocated + +event.target.value;

    // Change qty to input value
    this.AllocationList.forEach(basket => {
      if (basket.basketId == basketId) {  
        basket.qty = Number(event.target.value);
      }
    });

    this.TotalAllocatedQty = 0;
    this.TotalAllocatedR = "0.00";

    // Update Totals
    this.AllocationList.forEach(basket => {

      this.TotalAllocatedQty = this.TotalAllocatedQty + basket.qty;
      //this.TotalAllocatedR = (Number(this.TotalAllocatedR) + (basket.basketAmount * basket.qty)).toFixed(2);
      this.TotalAllocatedR = (Number(this.TotalAllocatedR) + (basket.fullPrice * basket.qty)).toFixed(2);

    });

    // Update Balance
    this.ClientBalanceAllocation = (Number(this.ClientBalance) + Number(this.TotalAllocatedR)).toFixed(2);
  }

  AllocateVouchers() {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/VoucherAdmin/AllocateVouchers', this.AllocationList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((result: VoucherTransactionModel) => {

        if (result.success) {
          // Update table of transactions
          this.GetClientTransactions(this.VoucherClient);

          // Close allocation model
          this._showAllocationModal = false;

          // Download file in browser    
          var a = document.createElement('a');
          a.target = "_blank";
          a.setAttribute('type', 'hidden');
          //a.href = './../../PamcPortal/assets/Vouchers/' + result.message;
          a.href = 'assets/Vouchers/' + result.message;
          //console.log(a.href);
          a.download = result.message;
          document.body.appendChild(a);
          a.click();
          a.remove();         
        }
        else {          
          this._errorMessageHeading = "Voucher Allocation Failed";
          this._errorMessage = result.message;
          this._error = true;
        }
      },
        (error) => {          
          this._errorMessageHeading = "Voucher Allocation Failed";
          this._errorMessage = "";
          this._error = true;          
        },
        () => {
          this._busy = false;
        })
  }

}

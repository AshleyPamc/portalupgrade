import { Injectable, Inject } from '@angular/core';
import { LoginDetailsModel } from '../models/LoginDetailsModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import * as uuid from 'uuid';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { EventLoggingModel } from '../Models/EventLoggingModel';
import { ClaimCodesModel } from '../models/ClaimCodesModel';
import { VoucherHistory } from '../models/history';
import { formatDate } from '@angular/common';
import { DefaultCodesModel } from '../models/DefaultCodesModel';
import { Emails } from '../Models/emails';
import { LoginService } from './login.service';
import { VoucherHpModel } from '../models/VoucherHpModel';
import { DependantsInfoModel } from '../models/DependantsInfoModel';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MedivouchService {

  private _balanceFail: boolean = false;
  private _busy: boolean = false;
  private _error: boolean = false;
  private _voucherNo: string = "  ";
  private _balanceSuccess: boolean = false;
  private _loginDetails: LoginDetailsModel = new LoginDetailsModel();
  private _historyList: VoucherHistory[] = [];
  private _cardnumber: string;
  private _memberDetails: string;
  private _codeList: ClaimCodesModel[] = [];
  private _procDesc: ValidationResultsModel = new ValidationResultsModel();
  private _otpSend: ValidationResultsModel = new ValidationResultsModel();
  private _successMedivouch: boolean = false;
  private _errorMessage: string;
  public showReference: boolean = false;
  private _drcRef: string = "";
  private _mediRef: string = "";
  private _diagdesc: ValidationResultsModel = new ValidationResultsModel();
  private _diagCode: string = "";
  private _groups: DefaultCodesModel[] = [];
  private _groupDetail: DefaultCodesModel[] = [];
  private _diagCodeFinale: string = '';
  private _historyClaims: ClaimCodesModel[] = [];
  private _orgHistoryClaims: ClaimCodesModel[] = [];
  private _historyClaimsDetail: ClaimCodesModel[] = [];
  private _openDetailModel: boolean = false;
  private _teethNo: boolean = false;
  private _openReferenceModel: boolean = false;
  private _totalMedi: string;
  private _showEmail: boolean = false;
  private _voucherCodes: boolean = false;
  private _BenDetails: boolean = false;
  private _success: boolean = false;
  private _benName: string;
  private _benSurname: string;
  private _benIDNo: string;
  private _benDep: string;
  private _isFamily: boolean;
  private _isSchemeMember: boolean;
  private _allowSkip: boolean;
  private _basketLists: DefaultCodesModel[] = [];
  private _basketDetailLists: DefaultCodesModel[] = [];
  private _dependantList: DependantsInfoModel[] = [];
  private _showBaskets: boolean = false;
  private _voucherError: boolean = false;
  private _voucherErrorMsg: string;
  private _voucherRedeemError: boolean = false;
  private _voucherRedeemErrorMsg: string;
  private _diagCodes: string[] = [];
  private _voucherRedemtionHist: ClaimCodesModel[] = [];
  private _toothCorrect: boolean = true;
  private _hpCode: string = "";
  private _option: string = "";
  private _prefix: string = "";
  //private _insuranceVoucher: boolean = false;
  private _basketId: string = "";
  private _voucherHpList: VoucherHpModel[] = [];

  constructor(private _http: HttpClient, private _router: Router, @Inject('BASE_URL') private _baseUrl: string, private _loginService: LoginService) { }

  /*getter asn setter*/
  public get toothCorrect(): boolean {
    return this._toothCorrect;
  }
  public set toothCorrect(value: boolean) {
    this._toothCorrect = value;
  }
  public get diagCodes(): string[] {
    return this._diagCodes;
  }
  public set diagCodes(value: string[]) {
    this._diagCodes = value;
  }
  public get voucherErrorMsg(): string {
    return this._voucherErrorMsg;
  }
  public set voucherErrorMsg(value: string) {
    this._voucherErrorMsg = value;
  }
  public get voucherError(): boolean {
    return this._voucherError;
  }
  public set voucherError(value: boolean) {
    this._voucherError = value;
  }
  public get voucherRedeemErrorMsg(): string {
    return this._voucherRedeemErrorMsg;
  }
  public set voucherRedeemErrorMsg(value: string) {
    this._voucherRedeemErrorMsg = value;
  }
  public get voucherRedeemError(): boolean {
    return this._voucherRedeemError;
  }
  public set voucherRedeemError(value: boolean) {
    this._voucherRedeemError = value;
  }
  public get basketLists(): DefaultCodesModel[] {
    return this._basketLists;
  }
  public set basketLists(value: DefaultCodesModel[]) {
    this._basketLists = value;
  }
  public get basketDetailLists(): DefaultCodesModel[] {
    return this._basketDetailLists;
  }
  public set basketDetailLists(value: DefaultCodesModel[]) {
    this._basketDetailLists = value;
  }
  public get dependantList(): DependantsInfoModel[] {
    return this._dependantList;
  }
  public set dependantList(value: DependantsInfoModel[]) {
    this._dependantList = value;
  }
  public get benIDNo(): string {
    return this._benIDNo;
  }
  public set benIDNo(value: string) {
    this._benIDNo = value;
  }
  public get allowSkip(): boolean {
    return this._allowSkip;
  }
  public set allowSkip(value: boolean) {
    this._allowSkip = value;
  }
  public get benSurname(): string {
    return this._benSurname;
  }
  public set benSurname(value: string) {
    this._benSurname = value;
  }
  public get benName(): string {
    return this._benName;
  }
  public set benName(value: string) {
    this._benName = value;
  }
  private _benContact: string;
  public get benContact(): string {
    return this._benContact;
  }
  public set benContact(value: string) {
    this._benContact = value;
  }
  public get benDep(): string {
    return this._benDep;
  }
  public set benDep(value: string) {
    this._benDep = value;
  }
  public get isFamily(): boolean {
    return this._isFamily;
  }
  public set isFamily(value: boolean) {
    this._isFamily = value;
  }
  public get isSchemeMember(): boolean {
    return this._isSchemeMember;
  }
  public set isSchemeMember(value: boolean) {
    this._isSchemeMember = value;
  }
  public get success(): boolean {
    return this._success;
  }
  public set success(value: boolean) {
    this._success = value;
  }
  public get BenDetails(): boolean {
    return this._BenDetails;
  }
  public set BenDetails(value: boolean) {
    this._BenDetails = value;
  }
  public get totalMedi(): string {
    return this._totalMedi;
  }
  public get voucherCodes(): boolean {
    return this._voucherCodes;
  }
  public set voucherCodes(value: boolean) {
    this._voucherCodes = value;
  }
  public set totalMedi(value: string) {
    this._totalMedi = value;
  }
  public get openReferenceModel(): boolean {
    return this._openReferenceModel;
  }
  public set openReferenceModel(value: boolean) {
    this._openReferenceModel = value;
  }
  public get errorMessage(): string {
    return this._errorMessage;
  }
  public set errorMessage(value: string) {
    this._errorMessage = value;
  }
  public get mediRef(): string {
    return this._mediRef;
  }
  public set mediRef(value: string) {
    this._mediRef = value;
  }
  public get drcRef(): string {
    return this._drcRef;
  }
  public set drcRef(value: string) {
    this._drcRef = value;
  }
  public get balanceSuccess(): boolean {
    return this._balanceSuccess;
  }
  public set balanceSuccess(value: boolean) {
    this._balanceSuccess = value;
  }
  public get balanceFail(): boolean {
    return this._balanceFail;
  }
  public set balanceFail(value: boolean) {
    this._balanceFail = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get loginDetails(): LoginDetailsModel {
    return this._loginDetails;
  }
  public set loginDetails(value: LoginDetailsModel) {
    this._loginDetails = value;
  }
  public get cardnumber(): string {
    return this._cardnumber;
  }
  public set cardnumber(value: string) {
    this._cardnumber = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get voucherNo(): string {
    return this._voucherNo;
  }
  public set voucherNo(value: string) {
    this._voucherNo = value;
  }
  public get memberDetails(): string {
    return this._memberDetails;
  }
  public set memberDetails(value: string) {
    this._memberDetails = value;
  }
  public get codeList(): ClaimCodesModel[] {
    return this._codeList;
  }
  public set codeList(value: ClaimCodesModel[]) {
    this._codeList = value;
  }
  public get procDesc(): ValidationResultsModel {
    return this._procDesc;
  }
  public set procDesc(value: ValidationResultsModel) {
    this._procDesc = value;
  }
  public get otpSend(): ValidationResultsModel {
    return this._otpSend;
  }
  public set otpSend(value: ValidationResultsModel) {
    this._otpSend = value;
  }
  public get successMedivouch(): boolean {
    return this._successMedivouch;
  }
  public set successMedivouch(value: boolean) {
    this._successMedivouch = value;
  }
  public get HistoryList(): VoucherHistory[] {
    return this._historyList;
  }
  public set HistoryList(value: VoucherHistory[]) {
    this._historyList = value;
  }
  public get diagdesc(): ValidationResultsModel {
    return this._diagdesc;
  }
  public set diagdesc(value: ValidationResultsModel) {
    this._diagdesc = value;
  }
  public get diagCode(): string {
    return this._diagCode;
  }
  public set diagCode(value: string) {
    this._diagCode = value;
  }
  public get groups(): DefaultCodesModel[] {
    return this._groups;
  }
  public set groups(value: DefaultCodesModel[]) {
    this._groups = value;
  }
  public get groupDetail(): DefaultCodesModel[] {
    return this._groupDetail;
  }
  public set groupDetail(value: DefaultCodesModel[]) {
    this._groupDetail = value;
  }
  public get diagCodeFinale(): string {
    return this._diagCodeFinale;
  }
  public set diagCodeFinale(value: string) {
    this._diagCodeFinale = value;
  }
  public get VoucherHistoryClaims(): ClaimCodesModel[] {
    return this._historyClaims;
  }
  public set VoucherHistoryClaims(value: ClaimCodesModel[]) {
    this._historyClaims = value;
  }
  public get OrgHistoryClaims(): ClaimCodesModel[] {
    return this._orgHistoryClaims;
  }
  public set OrgHistoryClaims(value: ClaimCodesModel[]) {
    this._orgHistoryClaims = value;
  }
  public get VoucherHistoryClaimsDetail(): ClaimCodesModel[] {
    return this._historyClaimsDetail;
  }
  public set VoucherHistoryClaimsDetail(value: ClaimCodesModel[]) {
    this._historyClaimsDetail = value;
  }
  public get openDetailModel(): boolean {
    return this._openDetailModel;
  }
  public set openDetailModel(value: boolean) {
    this._openDetailModel = value;
  }
  public get teethNo(): boolean {
    return this._teethNo;
  }
  public set teethNo(value: boolean) {
    this._teethNo = value;
  }
  public get showEmail(): boolean {
    return this._showEmail;
  }
  public set showEmail(value: boolean) {
    this._showEmail = value;
  }
  public get showBaskets(): boolean {
    return this._showBaskets;
  }
  public set showBaskets(value: boolean) {
    this._showBaskets = value;
  }
  public get voucherRedemtionHist(): ClaimCodesModel[] {
    return this._voucherRedemtionHist;
  }
  public set voucherRedemtionHist(value: ClaimCodesModel[]) {
    this._voucherRedemtionHist = value;
  }
  public get HpCode(): string {
    return this._hpCode;
  }
  public set HpCode(value: string) {
    this._hpCode = value;
  }
  public get Option(): string {
    return this._option;
  }
  public set Option(value: string) {
    this._option = value;
  }
  public get Prefix(): string {
    return this._prefix;
  }
  public set Prefix(value: string) {
    this._prefix = value;
  }
  public get basketId(): string {
    return this._basketId;
  }
  public set basketId(value: string) {
    this._basketId = value;
  }
  //public get InsuranceVoucher(): boolean {
  //  return this._insuranceVoucher;
  //}
  //public set InsuranceVoucher(value: boolean) {
  //  this._insuranceVoucher = value;
  //}
  public get VoucherHpList(): VoucherHpModel[] {
    return this._voucherHpList;
  }
  public set VoucherHpList(value: VoucherHpModel[]) {
    this._voucherHpList = value;
  }

  GetDiagCodes() {
    this._busy = true;
    let c: DefaultCodesModel = new DefaultCodesModel();
    c.provid = this._loginService.CurrentLoggedInUser.provID;
    c.hpCode = this.HpCode;
    c.provSpec = this._loginService.CurrentLoggedInUser.specCode;
    c.provClass = this._loginService.CurrentLoggedInUser.provClass;
    this._http.post(this._baseUrl + 'api/Medivouch/GetDiags', c)
      .subscribe((result: ClaimCodesModel[]) => {
        this._diagCodes = [];
        result.forEach((el) => {
          this.diagCodes.push(el.diag);

        })
      },
        (error) => { console.log(error) },
        () => { this.busy = false; })
  }

  VoucherValidation() {

    this.busy = true;
    this.balanceSuccess = false;
    this.balanceFail = false;

    let d: DefaultCodesModel = new DefaultCodesModel();
    d.id = this.voucherNo;
    d.provid = this._loginService.CurrentLoggedInUser.provID;
    d.user = this._loginService.CurrentLoggedInUser.username;
    d.provSpec = this._loginService.CurrentLoggedInUser.specCode;
    d.provClass = this._loginService.CurrentLoggedInUser.provClass;
    //d.hpCode = this.HpCode;
    //d.option = this.Option;

    this._http.post(this._baseUrl + 'api/Medivouch/IdentifyVoucher', d)
      .subscribe((result: ClaimCodesModel) => {

        d.hpCode = result.hpCode;
        d.option = result.option;
        d.prefix = result.prefix;
        this.HpCode = result.hpCode;
        this.Option = result.option;
        this.Prefix = result.prefix;
        
      },
        (error) => {
          console.log(error)
        },
        () => {

          //if (this._loginService.CurrentLoggedInUser.userType == 1) { // Medivouch
          if (d.option == 'MEDIVOUCH') { // Medivouch
            this._http.post(this._baseUrl + 'api/Medivouch/ValidateMedivouchVoucher', d
            ).subscribe((e: ValidationResultsModel) => {

              let c: VoucherHistory = new VoucherHistory();

              if (e.valid) {

                this.balanceSuccess = true;
                this.balanceFail = false;
                this.voucherNo = this.voucherNo + "_" + e.message;
                this.basketId = e.message;

              } else {
                this.basketId = "";
                this.balanceSuccess = false;
                c.voucherNo = d.id;
                c.success = false;
                c.errorMsg = e.message;
                this.voucherErrorMsg = e.message;
                if (this.voucherErrorMsg == "Voucher cannot be redeemed at this merchant!") {
                  if (this._loginService.CurrentLoggedInUser.dentist) {
                    this.voucherErrorMsg = "This voucher can not be redeemed by a Dentist";
                    c.errorMsg = this.voucherErrorMsg;
                  } else {
                    this.voucherErrorMsg = "This voucher can only be redeemed by a Dentist";
                    c.errorMsg = this.voucherErrorMsg;
                  }
                }
                this.voucherError = true;
                let date: Date = new Date;
                c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')
                this.balanceFail = true;
                this._historyList.push(c);

              }
            },
              (error) => {
                console.log(error);
                this.basketId = "";
              },
              () => {

                this.memberDetails = "Test, User";
                let c: VoucherHistory = new VoucherHistory();
                if (this.balanceSuccess) {

                  let d: DefaultCodesModel = new DefaultCodesModel();
                  d.id = this.voucherNo;
                  d.provid = this._loginService.CurrentLoggedInUser.provID;
                  d.user = this._loginService.CurrentLoggedInUser.username;
                  d.provSpec = this._loginService.CurrentLoggedInUser.specCode;
                  d.provClass = this._loginService.CurrentLoggedInUser.provClass;
                  d.hpCode = this.HpCode;
                  this.GetDefaultCodeDetails(d);
                } else {
                  this.busy = false;
                }
              })
          }
          else /*if (this._loginService.CurrentLoggedInUser.userType == 6)*/ { // Internal voucher provider
            this._http.post(this._baseUrl + 'api/Medivouch/ValidateInternalVoucher', d
            ).subscribe((e: ValidationResultsModel) => {

              let c: VoucherHistory = new VoucherHistory();

              if (e.valid) {

                this.balanceSuccess = true;
                this.balanceFail = false;
                this.voucherNo = this.voucherNo + "_" + e.message;
                this.basketId = e.message;

              } else {

                this.balanceSuccess = false;
                this.basketId = "";
                c.voucherNo = d.id;
                c.success = false;
                c.errorMsg = e.message;
                this.voucherErrorMsg = e.message;
                this.voucherError = true;
                let date: Date = new Date;
                c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')
                this.balanceFail = true;
                this._historyList.push(c);

              }
            },
              (error) => {
                console.log(error);
                this.basketId = "";
              },
              () => {

                this.memberDetails = "Test, User";
                let c: VoucherHistory = new VoucherHistory();
                if (this.balanceSuccess) {

                  let d: DefaultCodesModel = new DefaultCodesModel();
                  d.id = this.voucherNo;
                  d.provid = this._loginService.CurrentLoggedInUser.provID;
                  d.user = this._loginService.CurrentLoggedInUser.username;
                  d.provSpec = this._loginService.CurrentLoggedInUser.specCode;
                  d.provClass = this._loginService.CurrentLoggedInUser.provClass;
                  d.hpCode = this.HpCode;
                  this.GetDefaultCodeDetails(d);
                } else {
                  this.busy = false;
                }
              })
          }

        })
  }

  /*Log event on database*/
  LogEvent(event: EventLoggingModel) {
    this._busy = true;
    console.log("Log data   ---");
    this._http.post(this._baseUrl + 'api/Medivouch/LogEvent',
      event,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this._busy = true;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        console.log("Log data   --- Done");
      })
  }

  /*Verify Procedure Code*/
  ValidateProcCode(c: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Medivouch/ValidateProcCode', c
    ).subscribe((results: ValidationResultsModel) => {
      if (results.valid) {
        this.procDesc.message = results.message;
        this.procDesc.valid = true;
      } else {
        this.procDesc.valid = false;
      }
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }

  /*Verify Diagnosis Code*/
  ValidateDiagCode(c: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Medivouch/ValidateDiagCode', c
    ).subscribe((results: ValidationResultsModel) => {
      if (results.valid) {
        this.diagdesc.message = results.message;
        this.diagdesc.valid = true;
      } else {
        this.diagdesc.valid = false;
      }
    },
      (error) => {
        console.log(error);
        this._error = true;
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }

  /*Send OTP with FeverTree Api*/
  SendOtp() {
    /*   console.log("sendOtp---");
       this.busy = true;
       this.loginDetails.uuid = uuid.v4().toUpperCase();
   
       this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/SendOTP",
         {
           MedivouchID: this.loginDetails.uuid,
           CardOrIDNumber: this.cardnumber
         },
         {
           headers: new HttpHeaders({
             "Authorization": "Basic NzkyOmRlbW8=",
             "Content-Type": "application/json",
           })
         }
       ).subscribe((results: any) => {
         console.log(results);
   
       },
         (error) => {
           this._error = true;
           console.log(error);
           this._busy = false;
           this._errorMessage = error.status + ' ' + ':' + error.message;
         },
         () => {
           this.busy = false;
           let c: EventLoggingModel = new EventLoggingModel();
   
           c.provid = this.loginDetails.provID;
           c.member = this.cardnumber;
           c.voucherNo = this.voucherNo;
           c.eventDesc = "OTPSend";
           c.username = this.loginDetails.username;
           c.uuid = this.loginDetails.uuid;
   
           this.LogEvent(c);
   
         });
       setTimeout((q) => {
         this.busy = false;
       console.log("sendOtp---Done");
   
       },200)*/

  }

  /*Confirm OTP with FeverTree Api*/
  ConfirmOtp(otp: string) {
    /*    console.log("Confirm Otp---");
        this.busy = true;
        this.loginDetails.uuid = uuid.v4().toUpperCase();
    
       this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/CheckOTP",
          {
            MedivouchID: this.loginDetails.uuid,
            CardOrIDNumber: this.cardnumber,
            OTP: otp
          },
          {
            headers: new HttpHeaders({
              "Authorization": "Basic NzkyOmRlbW8=",
              "Content-Type": "application/json",
            })
          }
        ).subscribe((results: any) => {
          this.busy = true;
          this.otpSend.message = otp;
          this.otpSend.valid = results.Success;
          console.log(results);
    
        },
          (error) => {
            this._error = true;
            console.log(error);
            this._busy = false;
            this._errorMessage = error.status + ' ' + ':' + error.message;
          },
          () => {
           // this.busy = false;
            console.log("Confirm Otp---done");
            let c: EventLoggingModel = new EventLoggingModel();
    
            c.provid = this.loginDetails.provID;
            c.member = this.cardnumber;
            c.voucherNo = this.voucherNo;
            c.eventDesc = "OTPConfirmation";
            c.username = this.loginDetails.username;
            c.uuid = this.loginDetails.uuid;
    
            this.LogEvent(c);
    
          });*/
    this.otpSend.message = "0000";
    this.otpSend.valid = true;
  }

  /*Process Payment with FeverTree Api*/
  ProcessPayment(total: string) {
    this.successMedivouch = true;
    this.mediRef = "#TESTREF";
    this.showReference = this.successMedivouch;
    /*  console.log("Process Payment ---");
    this.busy = true;
    this.loginDetails.uuid = uuid.v4().toUpperCase();

    this._http.post("https://api.fevertreefinance.co.za/FTIntegration.svc/ProcessMedivouch",
      {
        MedivouchID: this.loginDetails.uuid,
        CardOrIDNumber: this.cardnumber,
        Amount: total,
        OTP: this.otpSend.message
      },
      {
        headers: new HttpHeaders({
          "Authorization": "Basic NzkyOmRlbW8=",
          "Content-Type": "application/json",
        })
      }
    ).subscribe((results: any) => {
      this.busy = true;


    },
      (error) => {
        this._error = true;
        console.log(error);
        this._busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        console.log("Process Payment Done ---");
        let c: EventLoggingModel = new EventLoggingModel();

        c.provid = this.loginDetails.provID;
        c.member = this.cardnumber;
        c.voucherNo = this.voucherNo;
        c.eventDesc = "MedivouchCompleted";
        c.username = this.loginDetails.username;
        c.uuid = this.loginDetails.uuid;

        this.LogEvent(c);
        this.SaveData();

      });*/
    this.SaveData();
  }

  RedeemVoucher() {
    this.busy = true;
    this.balanceSuccess = false;
    this.balanceFail = false;

    let d: DefaultCodesModel = new DefaultCodesModel();
    d.id = this.voucherNo;
    d.provid = this._loginService.CurrentLoggedInUser.provID;
    d.provSpec = this._loginService.CurrentLoggedInUser.specCode;
    d.provClass = this._loginService.CurrentLoggedInUser.provClass;
    d.user = this._loginService.CurrentLoggedInUser.username;
    d.membIDNo = this.benIDNo;
    d.membName = this.benName;
    d.membSurname = this.benSurname;
    d.membContact = this.benContact;
    d.depCode = this.benDep;
    d.prefix = this.Prefix;
    d.hpCode = this.HpCode;
    d.option = this.Option;

    //if (this._loginService.CurrentLoggedInUser.userType == 1) { //medivouch
    if (this.Option == 'MEDIVOUCH') {
      this._http.post(this._baseUrl + 'api/Medivouch/RedeemMedivouchVoucher', d

      ).subscribe((e: ValidationResultsModel) => {

        if (e.valid) {

          let c: any = e;
          let found: boolean = false;

          this._historyList.forEach((el) => {
            if (el != undefined) {
              try {
                let c: string = d.id.substr(0, this.voucherNo.indexOf('_'));

                if (el.voucherNo.includes(c)) {
                  found = true;
                }
              } catch (e) {
                if (e == null) {

                }
              }

            }

          });

          if (!found) {
            let c: VoucherHistory = new VoucherHistory();
            c.voucherNo = this.voucherNo.substr(0, this.voucherNo.indexOf('_'));
            c.voucherName = this.groupDetail[0].headDesc;
            c.voucherBasket = this.voucherNo.substr(this.voucherNo.indexOf('_') + 1,
              this.voucherNo.length - (this.voucherNo.indexOf('_') + 1))
            c.success = true;
            c.redeemed = true;
            let date: Date = new Date;
            c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')
            this._historyList.push(c);
          }
          else {
            this._historyList.forEach((el) => {
              try {
                let c: string = d.id.substr(0, this.voucherNo.indexOf('_'));
                if (el.voucherNo.includes(c)) {
                  el.redeemed = true;
                }
              } catch (e) {
                if (e == null) {

                }
              }

            })
          }


          this.voucherCodes = false;
          this.balanceFail = false;
          this.balanceSuccess = false;
          this.allowSkip = false;

          this.SaveData();
        }
        else {
          this.busy = false;
          this.balanceSuccess = false;
          this.balanceFail = true;
          this.voucherRedeemError = true;
          this.voucherRedeemErrorMsg = e.message;
        }
      },
        (error) => {
          console.log(error);
        },
        () => {
          this.voucherCodes = false;
          this.balanceFail = false;
          this.balanceSuccess = false;
          this.allowSkip = false;
          this.isSchemeMember = false;
        })

    } //else if (this._loginService.CurrentLoggedInUser.userType == 6) { //internal voucher provider
    else {
      this._http.post(this._baseUrl + 'api/Medivouch/RedeemInternalVoucher', d

      ).subscribe((e: ValidationResultsModel) => {

        if (e.valid) {

          let c: any = e;
          let found: boolean = false;

          this._historyList.forEach((el) => {
            if (el != undefined) {
              try {
                let c: string = d.id.substr(0, this.voucherNo.indexOf('_'));

                if (el.voucherNo.includes(c)) {
                  found = true;
                }
              } catch (e) {
                if (e == null) {

                }
              }

            }

          });

          if (!found) {
            let c: VoucherHistory = new VoucherHistory();
            c.voucherNo = this.voucherNo.substr(0, this.voucherNo.indexOf('_'));
            c.voucherName = this.groupDetail[0].headDesc;
            c.voucherBasket = this.voucherNo.substr(this.voucherNo.indexOf('_') + 1, this.voucherNo.length - (this.voucherNo.indexOf('_') + 1))
            c.success = true;
            c.redeemed = true;
            let date: Date = new Date;
            c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')
            this._historyList.push(c);
          }
          else {
            this._historyList.forEach((el) => {
              try {
                let c: string = d.id.substr(0, this.voucherNo.indexOf('_'));
                if (el.voucherNo.includes(c)) {
                  el.redeemed = true;
                }
              } catch (e) {
                if (e == null) {

                }
              }

            })
          }

          this.voucherCodes = false;
          this.balanceFail = false;
          this.balanceSuccess = false;
          this.allowSkip = false;
          this.isSchemeMember = false;

          this.SaveData();
        }
        else {
          this.busy = false;
          this.balanceSuccess = false;
          this.balanceFail = true;
          this.voucherRedeemError = true;
          this.voucherRedeemErrorMsg = e.message;
        }
      },
        (error) => {
          console.log(error);
        },
        () => {
          this.voucherCodes = false;
          this.balanceFail = false;
          this.balanceSuccess = false;
          this.allowSkip = false;

        })
    }


  }

  /*Save Transacion on database*/
  SaveData() {
    console.log("Save data   ---");
    this._busy = true;

    this.codeList = [];

    this.groupDetail.forEach((el) => {
      let c: ClaimCodesModel = new ClaimCodesModel();
      c.id = "test";
      c.user = this._loginService.CurrentLoggedInUser.username;
      c.membId = this.Prefix + this.benIDNo + "-" + this.benDep;
      c.provid = this._loginService.CurrentLoggedInUser.provID;
      c.mediRef = this.voucherNo;
      let d: string[] = this.memberDetails.split(',');
      c.name = this.benName;
      c.surname = this.benSurname;
      c.prefix = this.Prefix;
      c.hpCode = this.HpCode;
      c.option = this.Option;
      c.diag = this.diagCode;
      c.qty = el.defaultQty.toString();
      c.total = el.total;
      c.provpay = el.total;
      c.membpay = "0.00";
      c.phCode = el.phcode;
      c.code = el.code;
      c.desc = el.desc;
      c.singleUnitAmount = el.price;
      if (el.enterTooth) {
        c.enterTooth = true;
        c.toothNo = el.toothNo;
      } else {
        c.enterTooth = false;
        c.toothNo = el.toothNo;
      }
      c.enterTooth = false;
      c.createClaim = el.createClaim;
      this.codeList.push(c);


    })


    this._http.post(this._baseUrl + 'api/Medivouch/SaveData', this.codeList
    ).subscribe((results: ValidationResultsModel) => {
      this.busy = true;
      this.drcRef = results.message;
      this.openReferenceModel = this.successMedivouch;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        console.log("Save data   --- Done");
        this.busy = false;
        this.codeList = [];
        //this.cardnumber = "";
        this.voucherNo = "";
        this.memberDetails = "";
        this.success = true;
        this.voucherCodes = false;
        this.BenDetails = false;

      })
  }


  //GetDefaultCodeGroups() {

  //  this._busy = true;
  //  this._http.get(this._baseUrl + 'api/Medivouch/GetDefaultCodeGroups'
  //  ).subscribe((results: DefaultCodesModel[]) => {
  //    this.groups = [];

  //    results.forEach((el) => {
  //      this.groups.push(el);
  //    });

  //    this.groups = this.groups.sort((a, b) => a.heading.length > b.heading.length ? 1 : -1);
  //  },
  //    (error) => {
  //      console.log(error);
  //      this.error = true;
  //      this.busy = false;
  //      this._errorMessage = error.status + ' ' + ':' + error.message;
  //    },
  //    () => {
  //      this._busy = false;
  //    });
  //}

  GetDefaultCodeDetails(data: DefaultCodesModel) {

    this.busy = true;
    this._http.post(this._baseUrl + 'api/Medivouch/GetDefaultCodeDetails', data
    ).subscribe((results: DefaultCodesModel[]) => {
      this.groupDetail = [];

      results.forEach((el) => {
        this.groupDetail.push(el);

      });

      this.groupDetail.forEach((el) => {
        let c: ClaimCodesModel = new ClaimCodesModel();

        c.code = el.code;
        c.desc = el.desc;
        c.phCode = el.phcode;
        c.enterQty = el.enterQty;
        c.enterTooth = el.enterTooth;
        c.singleUnitAmount = el.price;
        if (c.enterQty) {
          c.qty = el.defaultQty.toString();
        }

        if (c.enterQty) {
          c.total = ((+c.qty) * (+c.singleUnitAmount)).toFixed(2);
        }

        this.codeList.push(c);
      });
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this.busy = false;
        this.voucherCodes = true;
      })
  }

  GetAllClaims(data: LoginDetailsModel) {
    this._busy = true;

    this._http.post(this._baseUrl + 'api/Medivouch/GetAllClaims', data
    ).subscribe((results: ClaimCodesModel[]) => {
      this._historyClaims = [];

      this._historyClaims = results;
      this._orgHistoryClaims = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },

      () => {
        this._busy = false;
      });
  }

  GetClaimDetail(data: ClaimCodesModel) {
    this._busy = true;
    this._openDetailModel = false;
    this._http.post(this._baseUrl + 'api/Medivouch/GetClaimDetail', data
    ).subscribe((results: ClaimCodesModel[]) => {
      this._historyClaimsDetail = [];

      this._historyClaimsDetail = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },

      () => {
        this._busy = false;
        this._openDetailModel = true;
      });
  }

  ValidateToothNo(data: ClaimCodesModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/Medivouch/ValidateToothNo', data
    ).subscribe((results: ValidationResultsModel) => {
      this._teethNo = !results.valid;
      this.toothCorrect = results.valid;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })


  }

  SendEmail(data: Emails) {
    this._busy = true;
    this._showEmail = false;
    this._http.post(this._baseUrl + 'api/Medivouch/SendEmail', data
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        this._showEmail = false;
      },
      () => {
        this._busy = false;
        this._showEmail = false;
        this.cardnumber = "";
        this._router.navigateByUrl("/provider-dash");
      })
  }

  ValidateIDExist(data: DefaultCodesModel, BeneficiaryForm: FormGroup) {
    this.busy = true;

    this._http.post(this._baseUrl + 'api/MediVouch/ValidateMemberExists', data).
      subscribe((results: DependantsInfoModel[]) => {
        //this.allowSkip = result.valid;
        //if (this.allowSkip) {
        //  let c: string[] = [];
        //  c = result.message.split(",");
        //  this.benName = c[0];
        //  this.benSurname = c[1];
        //  this.benContact = c[2];
        //}

        this.dependantList = []; // Clear list
        results.forEach((el) => {
          this.dependantList.push(el);
        })

        if (this.dependantList.length == 1) {

          if (this.dependantList[0].isSchemeMember) {
            this.isSchemeMember = true;
            this.allowSkip = true;
          }
          else {
            this.isSchemeMember = false;
            this.allowSkip = true;
          }

          this.benName = this.dependantList[0].firstname;
          this.benSurname = this.dependantList[0].lastname;
          this.benContact = this.dependantList[0].cell;
          this.benDep = this.dependantList[0].dependant;
          this.Option = this.dependantList[0].option;
          BeneficiaryForm.controls['dependant'].setValue(this.benDep);

        }
        else if (this.dependantList.length > 1) {

          this.isSchemeMember = true;
          this.allowSkip = false;
        }
        else {
          this.benDep = "00";
          BeneficiaryForm.controls['dependant'].setValue(this.benDep);
        }

      },
        (error) => {
          console.log(error);
        },
        () => {
          this.busy = false;
        })
  }

  GetBasketHeaders() {
    let c: DefaultCodesModel = new DefaultCodesModel();
    c.provid = this._loginService.CurrentLoggedInUser.provID;
    c.hpCode = this.HpCode;
    c.option = this.Option;
    c.provSpec = this._loginService.CurrentLoggedInUser.specCode;
    c.provClass = this._loginService.CurrentLoggedInUser.provClass;
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Medivouch/GetBasketHeaders', c
    ).subscribe((results: DefaultCodesModel[]) => {
      this.basketLists = [];
      this.basketLists = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        this._showEmail = false;
      },
      () => {
        this._busy = false;
        this.GetBasketDetails();
      })
  }

  GetBasketDetails() {
    let c: DefaultCodesModel = new DefaultCodesModel();
    c.provid = this._loginService.CurrentLoggedInUser.provID;
    c.hpCode = this.HpCode;
    c.option = this.Option;
    c.provSpec = this._loginService.CurrentLoggedInUser.specCode;
    c.provClass = this._loginService.CurrentLoggedInUser.provClass;
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Medivouch/GetBasketDetails', c
    ).subscribe((results: DefaultCodesModel[]) => {
      this.basketDetailLists = [];
      this.basketDetailLists = results;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
        this._showEmail = false;
      },
      () => {
        this._busy = false;
        this.showBaskets = true;
      })
  }

  GetVoucherRedemtionHistory() {
    this.busy = true;
    let c: DefaultCodesModel = new DefaultCodesModel();
    c.provid = this._loginService.CurrentLoggedInUser.provID;
    this._http.post(this._baseUrl + 'api/MediVouch/GetVoucherRedemtionHistory', c
    ).subscribe((results: ClaimCodesModel[]) => {
      this._voucherRedemtionHist = [];
      this._voucherRedemtionHist = results;

    },
      (error) => {
        console.log(error);
        this.error = true;
        this.busy = false;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }

  GetBasketHpCodes(provSpec: string = "", provClass: string = "") {
    this._busy = true;

    let data: DefaultCodesModel = new DefaultCodesModel();
    data.provSpec = provSpec;
    data.provClass = provClass;
    this.VoucherHpList = [];

    this._http.post(this._baseUrl + 'api/Medivouch/GetBasketHpCodes', data
    ).subscribe((results: VoucherHpModel[]) => {

      results.forEach((el) => {
        this.VoucherHpList.push(el);
      })

    },
      (error) => {
        console.log(error);
        this.error = true;
        this._errorMessage = error.status + ' ' + ':' + error.message;
      },
      () => {
        this._busy = false;
      })
  }
}

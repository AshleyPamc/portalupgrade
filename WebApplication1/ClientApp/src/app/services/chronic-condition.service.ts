import { Injectable, Inject, SecurityContext } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChronicConditionApplicationModel } from '../models/ChronicConditionApplicationModel';
import { ChronicConditionDiagsModel } from '../models/ChronicConditionDiagsModel';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, ValidationErrors } from '@angular/forms';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { LoginService } from './login.service';
import { MemberEligibilityModel } from '../models/MemberEligibilityModel';
//import { MembershipService } from './membership.service';
import { ValidationService } from './validation.service';
import { FileItemModel } from '../models/FileItemModel';
import { MedicationModel } from '../models/MedicationModel';
import { ChronicConditionCodesModel } from '../models/ChronicConditionCodesModel';
import { SpecCodes } from '../models/SpecCodes';
import { MedicineCodesModel } from '../models/MedicineCodesModel';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ChronicConditionService {

  imagePath: SafeResourceUrl;

  // Debug vars
  private _autCompOnOff: string = 'off';
  public get AutCompOnOff(): string { return this._autCompOnOff; }

  // Navigation vars
  private _currentPage: string = ''; // 'ChronicCondApp' or 'ChronicCondUpdate'
  public get CurrentPage(): string { return this._currentPage; }
  public set CurrentPage(value: string) { this._currentPage = value; }

  // Form declarations
  private memberForm: FormGroup;
  private providerForm: FormGroup;
  private biometricForm: FormGroup;
  private riskFactorsForm: FormGroup;
  private conditionForm: FormGroup;
  public get ConditionForm(): FormGroup { return this.conditionForm; }
  private diabetesMellitusForm: FormGroup;
  private hyperlipidaemiaForm: FormGroup;
  private hypertensionForm: FormGroup;
  private asthmaForm: FormGroup;
  private medicationForm: FormGroup;
  private documentsForm: FormGroup;
  private comorbidForm: FormGroup;
  private requestedMedicationForm: FormGroup;
  private declarationForm: FormGroup;

  // Referral specs
  private _referralSpecs: SpecCodes[] = [];
  public get ReferralSpecs(): SpecCodes[] { return this._referralSpecs; }

  // Medicine Codes
  private _medicineCodes: MedicineCodesModel[] = [];
  public get MedicineCodes(): MedicineCodesModel[] { return this._medicineCodes; }

  // Condition Diag Codes
  private _medicationDiags: ChronicConditionDiagsModel[] = [];
  public get MedicationDiags(): ChronicConditionDiagsModel[] { return this._medicationDiags; }

  private _filteredMedicationDiags: ChronicConditionDiagsModel[] = [];
  public get FilteredMedicationDiags(): ChronicConditionDiagsModel[] { return this._filteredMedicationDiags; }

  private _conditionDiags: ChronicConditionDiagsModel[] = [];
  public get ConditionDiags(): ChronicConditionDiagsModel[] { return this._conditionDiags; }

  private _diabetesDiags: ChronicConditionDiagsModel[] = [];
  public get DiabetesDiags(): ChronicConditionDiagsModel[] { return this._diabetesDiags; }

  private _hyperLipDiags: ChronicConditionDiagsModel[] = [];
  public get HyperLipDiags(): ChronicConditionDiagsModel[] { return this._hyperLipDiags; }

  private _hypertensionDiags: ChronicConditionDiagsModel[] = [];
  public get HypertensionDiags(): ChronicConditionDiagsModel[] { return this._hypertensionDiags; }

  private _asthmaDiags: ChronicConditionDiagsModel[] = [];
  public get AsthmaDiags(): ChronicConditionDiagsModel[] { return this._asthmaDiags; }

  private _addisDiags: ChronicConditionDiagsModel[] = [];
  public get AddisDiags(): ChronicConditionDiagsModel[] { return this._addisDiags; }

  private _bipolDiags: ChronicConditionDiagsModel[] = [];
  public get BipolDiags(): ChronicConditionDiagsModel[] { return this._bipolDiags; }

  private _bronchDiags: ChronicConditionDiagsModel[] = [];
  public get BronchDiags(): ChronicConditionDiagsModel[] { return this._bronchDiags; }

  private _cardiacDiags: ChronicConditionDiagsModel[] = [];
  public get CardiacDiags(): ChronicConditionDiagsModel[] { return this._cardiacDiags; }

  private _cardiomDiags: ChronicConditionDiagsModel[] = [];
  public get CardiomDiags(): ChronicConditionDiagsModel[] { return this._cardiomDiags; }

  private _copdDiags: ChronicConditionDiagsModel[] = [];
  public get CopdDiags(): ChronicConditionDiagsModel[] { return this._copdDiags; }

  private _renalDiags: ChronicConditionDiagsModel[] = [];
  public get RenalDiags(): ChronicConditionDiagsModel[] { return this._renalDiags; }

  private _coronarDiags: ChronicConditionDiagsModel[] = [];
  public get CoronarDiags(): ChronicConditionDiagsModel[] { return this._coronarDiags; }

  private _crohnsDiags: ChronicConditionDiagsModel[] = [];
  public get CrohnsDiags(): ChronicConditionDiagsModel[] { return this._crohnsDiags; }

  private _diabinsipDiags: ChronicConditionDiagsModel[] = [];
  public get DiabinsipDiags(): ChronicConditionDiagsModel[] { return this._diabinsipDiags; }

  private _dysrhytDiags: ChronicConditionDiagsModel[] = [];
  public get DysrhytDiags(): ChronicConditionDiagsModel[] { return this._dysrhytDiags; }

  private _epilDiags: ChronicConditionDiagsModel[] = [];
  public get EpilDiags(): ChronicConditionDiagsModel[] { return this._epilDiags; }

  private _glaucomDiags: ChronicConditionDiagsModel[] = [];
  public get GlaucomDiags(): ChronicConditionDiagsModel[] { return this._glaucomDiags; }

  private _haemophilDiags: ChronicConditionDiagsModel[] = [];
  public get HaemophilDiags(): ChronicConditionDiagsModel[] { return this._haemophilDiags; }

  private _hypothDiags: ChronicConditionDiagsModel[] = [];
  public get HypothDiags(): ChronicConditionDiagsModel[] { return this._hypothDiags; }

  private _sclerosisDiags: ChronicConditionDiagsModel[] = [];
  public get SclerosisDiags(): ChronicConditionDiagsModel[] { return this._sclerosisDiags; }

  private _parkinsDiags: ChronicConditionDiagsModel[] = [];
  public get ParkinsDiags(): ChronicConditionDiagsModel[] { return this._parkinsDiags; }

  private _rheumaDiags: ChronicConditionDiagsModel[] = [];
  public get RheumaDiags(): ChronicConditionDiagsModel[] { return this._rheumaDiags; }

  private _schizoDiags: ChronicConditionDiagsModel[] = [];
  public get SchizoDiags(): ChronicConditionDiagsModel[] { return this._schizoDiags; }

  private _lupusDiags: ChronicConditionDiagsModel[] = [];
  public get LupusDiags(): ChronicConditionDiagsModel[] { return this._lupusDiags; }

  private _ulceraDiags: ChronicConditionDiagsModel[] = [];
  public get UlceraDiags(): ChronicConditionDiagsModel[] { return this._ulceraDiags; }

  // File upload variables
  private _referralLetterFiles: FileItemModel[] = [];
  public get ReferralLetterFiles(): FileItemModel[] { return this._referralLetterFiles; }

  private _diabPathResultFiles: FileItemModel[] = [];
  public get DiabPathResultFiles(): FileItemModel[] { return this._diabPathResultFiles; }

  private _hyperlipPathResultFiles: FileItemModel[] = [];
  public get HyperlipPathResultFiles(): FileItemModel[] { return this._hyperlipPathResultFiles; }

  private _asthmaPaedFiles: FileItemModel[] = [];
  public get AsthmaPaedFiles(): FileItemModel[] { return this._asthmaPaedFiles; }

  private _asthmaFlowVolFiles: FileItemModel[] = [];
  public get AsthmaFlowVolFiles(): FileItemModel[] { return this._asthmaFlowVolFiles; } 

  private _medPrescriptionFiles: FileItemModel[] = [];
  public get MedPrescriptionFiles(): FileItemModel[] { return this._medPrescriptionFiles; }

  private _additionalDocsFiles: FileItemModel[] = [];
  public get AdditionalDocsFiles(): FileItemModel[] { return this._additionalDocsFiles; }

  private _isFileUploadInProgress: boolean = false;
  public get IsFileUploadInProgress(): boolean { return this._isFileUploadInProgress; }

  // Medication   
  private _medicationItem: MedicationModel = null;
  public get MedicationItem(): MedicationModel { return this._medicationItem; }

  private _hypertensionMedList: MedicationModel[] = [];
  public get HypertensionMedList(): MedicationModel[] { return this._hypertensionMedList; }
  private _hyperlipidaemiaMedList: MedicationModel[] = [];
  public get HyperlipidaemiaMedList(): MedicationModel[] { return this._hyperlipidaemiaMedList; }
  private _requestedMedList: MedicationModel[] = [];
  public get RequestedMedList(): MedicationModel[] { return this._requestedMedList; }
  private _asthmaComorbidMedList: MedicationModel[] = [];
  public get AsthmaComorbidMedList(): MedicationModel[] { return this._asthmaComorbidMedList; }
  private _diabComorbidMedList: MedicationModel[] = [];
  public get DiabComorbidMedList(): MedicationModel[] { return this._diabComorbidMedList; }
  private _hyperlipComorbidMedList: MedicationModel[] = [];
  public get HyperlipComorbidMedList(): MedicationModel[] { return this._hyperlipComorbidMedList; }
  private _hypertensionComorbidMedList: MedicationModel[] = [];
  public get HypertensionComorbidMedList(): MedicationModel[] { return this._hypertensionComorbidMedList; }
  private _addisComorbidMedList: MedicationModel[] = [];
  public get AddisComorbidMedList(): MedicationModel[] { return this._addisComorbidMedList; }
  private _bipolComorbidMedList: MedicationModel[] = [];
  public get BipolComorbidMedList(): MedicationModel[] { return this._bipolComorbidMedList; }
  private _bronchComorbidMedList: MedicationModel[] = [];
  public get BronchComorbidMedList(): MedicationModel[] { return this._bronchComorbidMedList; }
  private _cardiacComorbidMedList: MedicationModel[] = [];
  public get CardiacComorbidMedList(): MedicationModel[] { return this._cardiacComorbidMedList; }
  private _cardiomComorbidMedList: MedicationModel[] = [];
  public get CardiomComorbidMedList(): MedicationModel[] { return this._cardiomComorbidMedList; }
  private _copdComorbidMedList: MedicationModel[] = [];
  public get CopdComorbidMedList(): MedicationModel[] { return this._copdComorbidMedList; }
  private _renalComorbidMedList: MedicationModel[] = [];
  public get RenalComorbidMedList(): MedicationModel[] { return this._renalComorbidMedList; }
  private _coronarComorbidMedList: MedicationModel[] = [];
  public get CoronarComorbidMedList(): MedicationModel[] { return this._coronarComorbidMedList; }
  private _crohnsComorbidMedList: MedicationModel[] = [];
  public get CrohnsComorbidMedList(): MedicationModel[] { return this._crohnsComorbidMedList; }
  private _diabinsipComorbidMedList: MedicationModel[] = [];
  public get DiabinsipComorbidMedList(): MedicationModel[] { return this._diabinsipComorbidMedList; }
  private _dysrhytComorbidMedList: MedicationModel[] = [];
  public get DysrhytComorbidMedList(): MedicationModel[] { return this._dysrhytComorbidMedList; }
  private _epilComorbidMedList: MedicationModel[] = [];
  public get EpilComorbidMedList(): MedicationModel[] { return this._epilComorbidMedList; }
  private _glaucomComorbidMedList: MedicationModel[] = [];
  public get GlaucomComorbidMedList(): MedicationModel[] { return this._glaucomComorbidMedList; }
  private _haemophilComorbidMedList: MedicationModel[] = [];
  public get HaemophilComorbidMedList(): MedicationModel[] { return this._haemophilComorbidMedList; }
  private _hypothComorbidMedList: MedicationModel[] = [];
  public get HypothComorbidMedList(): MedicationModel[] { return this._hypothComorbidMedList; }
  private _sclerosisComorbidMedList: MedicationModel[] = [];
  public get SclerosisComorbidMedList(): MedicationModel[] { return this._sclerosisComorbidMedList; }
  private _parkinsComorbidMedList: MedicationModel[] = [];
  public get ParkinsComorbidMedList(): MedicationModel[] { return this._parkinsComorbidMedList; }

  private _rheumaComorbidMedList: MedicationModel[] = [];
  public get RheumaComorbidMedList(): MedicationModel[] { return this._rheumaComorbidMedList; }

  private _schizoComorbidMedList: MedicationModel[] = [];
  public get SchizoComorbidMedList(): MedicationModel[] { return this._schizoComorbidMedList; }

  private _lupusComorbidMedList: MedicationModel[] = [];
  public get LupusComorbidMedList(): MedicationModel[] { return this._lupusComorbidMedList; }

  private _ulceraComorbidMedList: MedicationModel[] = [];
  public get UlceraComorbidMedList(): MedicationModel[] { return this._ulceraComorbidMedList; }



  // Other lists
  private _chronicConditionsAll: ChronicConditionCodesModel[] = [];
  public get ChronicConditionsAll(): ChronicConditionCodesModel[] { return this._chronicConditionsAll; }

  private _chronicConditionsSelectable: ChronicConditionCodesModel[] = [];
  public get ChronicConditionsSelectable(): ChronicConditionCodesModel[] { return this._chronicConditionsSelectable; }

  private _sumbittedApplication: ChronicConditionApplicationModel;
  public get SumbittedApplication(): ChronicConditionApplicationModel { return this._sumbittedApplication; }

  private _approvedApplication: ChronicConditionApplicationModel;
  public get ApprovedApplication(): ChronicConditionApplicationModel { return this._approvedApplication; }

  private _latestUpdate: ChronicConditionApplicationModel;
  public get LatestUpdate(): ChronicConditionApplicationModel { return this._latestUpdate; }

  private _hpContracts: HP_ContractsModel[] = [];
  public get HpContracts(): HP_ContractsModel[] { return this._hpContracts; }

  private _familyList: MemberEligibilityModel[] = [];
  public get FamilyList(): MemberEligibilityModel[] { return this._familyList; }

  // Modal Vars

  private _showMedicationModal: boolean = false;
  public get ShowMedicationModal(): boolean { return this._showMedicationModal; }
  public set ShowMedicationModal(value: boolean) { this._showMedicationModal = value; }

  private _busy: boolean = false;
  public get Busy(): boolean { return this._busy; }
  public set Busy(value: boolean) { this._busy = value; }

  private _error: boolean = false;
  public get Error(): boolean { return this._error; }

  private _errorMessageHeading: string;
  public get ErrorMessageHeading(): string { return this._errorMessageHeading; }

  private _errorMessage: string = "";
  public get ErrorMessage(): string { return this._errorMessage; }  

  private _errorArrayModal: boolean;
  public get ErrorArrayModal(): boolean { return this._errorArrayModal; }

  private _errorArrayHeading: string;
  public get ErrorArrayHeading(): string { return this._errorArrayHeading; }

  private _errorArrayMessage: string;
  public get ErrorArrayMessage(): string { return this._errorArrayMessage; }

  private _errorArrayArr: any = [];
  public get ErrorArrayArr(): any { return this._errorArrayArr; }
    
  private _success: boolean;
  public get Success(): boolean { return this._success; }
  public set Success(value: boolean) { this._error = this._success; }

  private _successMessageHeading: string;
  public get SuccessMessageHeading(): string { return this._successMessageHeading; }

  private _successMessage: string;
  public get SuccessMessage(): string { return this._successMessage; }

  private _successMessageBoldPart: string;
  public get SuccessMessageBoldPart(): string { return this._successMessageBoldPart; }

  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private fb: FormBuilder, private _loginService: LoginService, private _validationService: ValidationService, private _domSanitizer: DomSanitizer/*, private _membershipService: MembershipService*/) {

    this.memberForm = this.fb.group({
      'member': ['', Validators.required],
      'hp': ['', Validators.required],
      'dob': [null, Validators.required],
      'dependant': [null, Validators.required],
      'membEmail': [null, Validators.required]
    });

    this.providerForm = this.fb.group({
      'provid': [null, Validators.required],
      'provName': [null],
      'provContactNo': [null],
      'provEmail': [null, Validators.required],
      'referredYN': [null, Validators.required],
      'refProvSpec': [null, Validators.required],
      'refProvid': [null, Validators.required],
      'refProvSurname': [null, Validators.required],
      'refProvNotes': [null, Validators.maxLength(1000)],
      'referralLetter': [null],
    });

    this.providerForm.controls['provid'].disable();
    this.providerForm.controls['provName'].disable();
    this.providerForm.controls['provContactNo'].disable();
    //this.providerForm.controls['provEmail'].disable();

    // Make referral fields required if referredYN = 'Y'
    this.providerForm.get('referredYN').valueChanges
      .subscribe(value => {

        // Check value of diabetesType
        if (value == 'Y') {
          this.providerForm.get('refProvSpec').setValidators(Validators.required);
          this.providerForm.get('refProvid').setValidators(Validators.required);
          this.providerForm.get('refProvSurname').setValidators(Validators.required);
          //this.providerForm.get('refProvNotes').setValidators(Validators.required); //Notes are always optional
          this.providerForm.get('referralLetter').setValidators(Validators.required);
        }
        else {
          this.providerForm.get('refProvSpec').setValidators(null);
          this.providerForm.get('refProvSpec').setValue(null);
          this.providerForm.get('refProvSpec').reset();

          this.providerForm.get('refProvid').setValidators(null);
          this.providerForm.get('refProvid').setValue(null);
          this.providerForm.get('refProvid').reset();

          this.providerForm.get('refProvSurname').setValidators(null);
          this.providerForm.get('refProvSurname').setValue(null);
          this.providerForm.get('refProvSurname').reset();

          //this.providerForm.get('refProvNotes').setValidators(null);
          this.providerForm.get('refProvNotes').setValue(null);
          this.providerForm.get('refProvNotes').reset();

          this.providerForm.get('referralLetter').setValidators(null);
          this.providerForm.get('referralLetter').setValue(null);
          this.providerForm.get('referralLetter').reset();
          this._referralLetterFiles = [];
        }

        // Validate fields
        this.providerForm.get('refProvSpec').updateValueAndValidity();
        this.providerForm.get('refProvid').updateValueAndValidity();
        this.providerForm.get('refProvSurname').updateValueAndValidity();
        this.providerForm.get('referralLetter').updateValueAndValidity();
      });

    this.conditionForm = this.fb.group({
      'conditionCode': [null, Validators.required],
    });

    // Uncheck Comorbid checkbox for condition that is selected
    //this.conditionForm.get('conditionCode').valueChanges
    //  .subscribe(value => {

    //    // Check value of diabetesType
    //    if (value == 'ASTHMA') {
    //      this.comorbidForm.get('asthmaCmChk').setValue(false);          
    //    }
    //    else if (value == 'CHOL') {
    //      this.comorbidForm.get('hyperlipCmChk').setValue(false);
    //    }
    //    else if (value == 'HYPER') {
    //      this.comorbidForm.get('hypertensionCmChk').setValue(false);
    //    }

    //    if (value == 'DIAB') {
    //      this.comorbidForm.get('diabCmChk').setValue(false);

    //      // Also set diabetesYN to Y and disable radio
    //      this.biometricForm.get('diabetesYN').disable();
    //      this.biometricForm.get('diabetesYN').reset();
    //      this.biometricForm.get('diabetesYN').setValue('Y');
    //      this.biometricForm.get('diabetesYN').markAsTouched;
    //      this.biometricForm.get('diabetesYN').updateValueAndValidity();          

    //    } else {
    //      // Enable diabetes radio if selected condition is not diabetes
    //      this.biometricForm.get('diabetesYN').enable();
    //      this.biometricForm.get('diabetesYN').setValue(null);
    //      this.biometricForm.get('diabetesYN').reset();              
    //    }

    //  });

    this.biometricForm = new FormGroup({
      'height': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      'weight': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      'bmi': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      //'bloodPressureMmhg': new FormControl(null, [Validators.required, Validators.pattern("^[0-9]{1,3}/[0-9]{1,3}$")]), //pattern is for blood pressure (int + slash + int)
      'bloodPresSystolic': new FormControl(null, [Validators.required, Validators.max(2147483647), Validators.pattern("^[0-9]*$")]),  // max is SQL int max, pattern is for only positive int
      'bloodPresDiastolic': new FormControl(null, [Validators.required, Validators.max(2147483647), Validators.pattern("^[0-9]*$")]),  // max is SQL int max, pattern is for only positive int
      'cholesterolTotal': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)      
      'cholesterolHDL': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      'cholesterolLDL': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)     
      'cholesterolTRI': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      'cholesterolLdlHdlRatio': new FormControl(null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)

      'diabetesYN': new FormControl(null, [Validators.required]),
      'diabetesType': new FormControl(null, [Validators.required]),      
      //'fastingOrRandom': new FormControl(null),
      //'fastingOrRandomResult': new FormControl(null, [Validators.max(2147483647), Validators.pattern("^[0-9]*$")]), // max is SQL int max, pattern is for only positive int
      'ogttResult': new FormControl(null, [Validators.max(2147483647), Validators.pattern("^[0-9]*$")]), // max is SQL int max, pattern is for only positive int
      'fastingGlucoseResult': new FormControl(null, [Validators.max(2147483647), Validators.pattern("^[0-9]*$")]), // max is SQL int max, pattern is for only positive int
      'randomGlucoseResult': new FormControl(null, [Validators.max(2147483647), Validators.pattern("^[0-9]*$")]), // max is SQL int max, pattern is for only positive int
      'HbA1c': new FormControl(null, [Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]), // SQL decimal(6,2)
      'diabTestDate': new FormControl(null, this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date()))      
    })

    // Make diabetesType, fastingTestResult, diabTestDate required if diabetesYN = 'Y'
    // Make HbA1c required if diabetesYN = 'Y' and it is an update
    this.biometricForm.get('diabetesYN').valueChanges
      .subscribe(value => {

        // Declare validators that should always be on controls
        let fastingGlucoseResultValidators = [
          Validators.max(2147483647),
          Validators.pattern("^[0-9]*$")
        ];

        let HbA1cValidators = [
          Validators.max(9999.99),
          this._validationService.decimalOrIntCustomValidator.bind(this)
        ];

        let diabTestDateValidators = [
          this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date())
        ];

        // Check value of diabetesYN
        if (value == 'Y') {

          // Set always validators and add required validator
          this.biometricForm.get('diabetesType').setValidators(Validators.required);          

          this.biometricForm.get('diabTestDate').setValidators(diabTestDateValidators.concat(Validators.required));                 
        }
        else {
          // Set always validators without required validator
          this.biometricForm.get('diabetesType').setValidators(null);
          this.biometricForm.get('diabetesType').setValue(null);
          this.biometricForm.get('diabetesType').reset();

          this.biometricForm.get('diabTestDate').setValidators(diabTestDateValidators);
          this.biometricForm.get('diabTestDate').setValue(null);
          this.biometricForm.get('diabTestDate').reset();
          
          // Don't allow diab comorbid if value is not Y
          this.comorbidForm.get('diabCmChk').setValue(false);
        }

        // Make fastingGlucoseResult compulsory only if diabetesYN is Y and condition is DIAB and it is an Application (new diagnosis)
        if (value == 'Y' && this._currentPage == 'ChronicCondApp' && this.conditionForm.get('conditionCode').value == "DIAB") {

          // Set always validators and add required validator
          this.biometricForm.get('fastingGlucoseResult').setValidators(fastingGlucoseResultValidators.concat(Validators.required));
        }
        else {
          // Set always validators without required validator
          this.biometricForm.get('fastingGlucoseResult').setValidators(fastingGlucoseResultValidators);
          this.biometricForm.get('fastingGlucoseResult').setValue(null);
          this.biometricForm.get('fastingGlucoseResult').reset();
        }

        // Make HbA1c compulsory if diabetesYN is Y and condition is DIAB and it is an Update (monitoring)
        if (value == 'Y' && this._currentPage == 'ChronicCondUpdate' && this.conditionForm.get('conditionCode').value == "DIAB" ) {

           // Set always validators and add required validator
          this.biometricForm.get('HbA1c').setValidators(HbA1cValidators.concat(Validators.required));
        }
        else {
          // Set always validators without required validator
          this.biometricForm.get('HbA1c').setValidators(HbA1cValidators);
          this.biometricForm.get('HbA1c').setValue(null);
          this.biometricForm.get('HbA1c').reset();
        }

        // Validate controls that changed
        this.biometricForm.get('diabetesType').updateValueAndValidity();
        this.biometricForm.get('fastingGlucoseResult').updateValueAndValidity();
        this.biometricForm.get('HbA1c').updateValueAndValidity();
        this.biometricForm.get('diabTestDate').updateValueAndValidity();
      })
        
    // Make ogttResult required only if diabetesType = diabetesType1 and it is an new diagnosis (new application) for DIAB condition
    // Set diabetes diag codes list depending in which type of diabetes is selected
    this.biometricForm.get('diabetesType').valueChanges
      .subscribe(value => {

        // Declare validators that should always be on controls
        let ogttResultValidators = [
          Validators.max(2147483647),
          Validators.pattern("^[0-9]*$")
        ];

        // Check value of diabetesType and if application is for DIAB and if application or update
        if (value == 'diabetesType1' && this._currentPage == 'ChronicCondApp' && this.conditionForm.get('conditionCode').value == "DIAB") {

          // Set always validators and add required validator
          this.biometricForm.get('ogttResult').setValidators(ogttResultValidators.concat(Validators.required));

        } else {

          // Set always validators without required validator
          this.biometricForm.get('ogttResult').setValidators(ogttResultValidators);
          this.biometricForm.get('ogttResult').setValue(null);
          this.biometricForm.get('ogttResult').reset();
        }

        // Validate controls that changed
        this.biometricForm.get('ogttResult').updateValueAndValidity();

        ////

        // Set diabetes diag codes list depending in which type of diabetes is selected
        this._diabetesDiags = [];

        // Check value of diabetesYN
        if (value == 'diabetesType1') { this._diabetesDiags = this._conditionDiags.filter(x => x.conditionCode == "DIAB1"); }
        else if (value == 'diabetesType2') { this._diabetesDiags = this._conditionDiags.filter(x => x.conditionCode == "DIAB2"); }
        else {

          this._diabetesDiags = this._conditionDiags.filter(x => x.conditionCode == "DIAB1" || x.conditionCode == "DIAB2");

          // Get distinct
          this._diabetesDiags = this._diabetesDiags.filter(
            (thing, i, arr) => arr.findIndex(t => t.diagCode === thing.diagCode) === i
          );

          // Sort
          this._diabetesDiags.sort((a, b) => a.diagCode.localeCompare(b.diagCode));
        }

        // Reset control value and validation
        this.diabetesMellitusForm.get('diabIcd10').setValue(null);
        this.diabetesMellitusForm.get('diabIcd10').reset();
        this.diabetesMellitusForm.get('diabIcd10').updateValueAndValidity();

      });

    //// Make fastingOrRandom and fastingOrRandomResult required if diabetesType = 'diabetesType2'
    //this.biometricForm.get('diabetesType').valueChanges
    //  .subscribe(value => {

    //    // Declare validators that should always be on controls
    //    let fastingOrRandomResultValidators = [
    //      Validators.max(2147483647),
    //      Validators.pattern("^[0-9]*$")
    //    ];

    //    // Check value of diabetesType
    //    if (value == 'diabetesType2') {

    //      // Set required validator
    //      this.biometricForm.get('fastingOrRandom').setValidators(Validators.required);

    //      // Set always validatros and concat required 
    //      this.biometricForm.get('fastingOrRandomResult').setValidators(fastingOrRandomResultValidators.concat(Validators.required));
    //    }
    //    else {
    //      // Remove required validator
    //      this.biometricForm.get('fastingOrRandom').setValidators(null);
    //      this.biometricForm.get('fastingOrRandom').setValue(null);
    //      this.biometricForm.get('fastingOrRandom').reset();

    //      // Set always validators without required validator
    //      this.biometricForm.get('fastingOrRandomResult').setValidators(fastingOrRandomResultValidators);
    //      this.biometricForm.get('fastingOrRandomResult').setValue(null);
    //      this.biometricForm.get('fastingOrRandomResult').reset();
    //    }

    //    // Validate fastingOrRandom
    //    this.biometricForm.get('fastingOrRandom').updateValueAndValidity();
    //    this.biometricForm.get('fastingOrRandomResult').updateValueAndValidity();
    //  })

    //// Make diabTestDate required only if diabetesType = diabetesType1 OR diabetesType = diabetesType2 (Not when unselected/null)
    //this.biometricForm.get('diabetesType').valueChanges
    //  .subscribe(value => {

    //    let diabTestDateValidators = [
    //      this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date())
    //    ];

    //    // Check value of diabetesType
    //    if (value == 'diabetesType1' || value == 'diabetesType2') {
    //      // Set always validators and add required validator         
    //      this.biometricForm.get('diabTestDate').setValidators(diabTestDateValidators.concat(Validators.required));
    //    } else {
    //      // Set always validators without required validator       
    //      this.biometricForm.get('diabTestDate').setValidators(diabTestDateValidators);
    //      this.biometricForm.get('diabTestDate').setValue(null);
    //      this.biometricForm.get('diabTestDate').reset();
    //    }

    //    // Validate HbA1c and HbA1cDate       
    //    this.biometricForm.get('diabTestDate').updateValueAndValidity();
    //  })

    // Set diabetes diag codes list depending in which type of diabetes is selected
    //this.biometricForm.get('diabetesType').valueChanges
    //  .subscribe(value => {        
    //  });

    this.riskFactorsForm = this.fb.group({
      'alcoholYN': [null, Validators.required],
      'obesityYN': [null, Validators.required],
      'smokerYN': [null, Validators.required],
      'heartDiseaseYN': [null, Validators.required],
      'vascularDiseaseYN': [null, Validators.required],
      'strokeYN': [null, Validators.required],
      'patientSmokeInPast': [null],
      'cessationDate': [null, this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date())]
    })

    // Make patientSmokeInPast required only if smokerYN = N
    this.riskFactorsForm.get('smokerYN').valueChanges
      .subscribe(value => {

        // Check value of smokerYN
        if (value == 'N') {
          this.riskFactorsForm.get('patientSmokeInPast').setValidators(Validators.required);
        }
        else {
          this.riskFactorsForm.get('patientSmokeInPast').setValidators(null);
          this.riskFactorsForm.get('patientSmokeInPast').setValue(null);
          this.riskFactorsForm.get('patientSmokeInPast').reset();
        }

        // Validate fastingOrRandom
        this.riskFactorsForm.get('patientSmokeInPast').updateValueAndValidity();
      })

    // Make cessationDate required only if patientSmokeInPast = Y
    this.riskFactorsForm.get('patientSmokeInPast').valueChanges
      .subscribe(value => {

        let cessationDateValidators = [
          this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date())
        ];

        // Check value of smokerYN
        if (value == 'Y') {
          // Set always validators and add required validator          
          this.riskFactorsForm.get('cessationDate').setValidators(cessationDateValidators.concat(Validators.required));
        } else {
          // Set always validators without required validator          
          this.riskFactorsForm.get('cessationDate').setValidators(cessationDateValidators);
          this.riskFactorsForm.get('cessationDate').setValue(null);
          this.riskFactorsForm.get('cessationDate').reset();
        }

        // Validate fastingOrRandom
        this.riskFactorsForm.get('cessationDate').updateValueAndValidity();
      });

    this.diabetesMellitusForm = this.fb.group({
      'diabIcd10': [null, [Validators.required, Validators.maxLength(10)]],
      'diabPathResults': [null, Validators.required]
    })

    this.hyperlipidaemiaForm = this.fb.group({
      'hyperlipIcd10': [null, [Validators.required, Validators.maxLength(10)]],
      'hyperlipTherapyYN': [null, Validators.required],
      //'therapyDuration': [null, [Validators.max(2147483647), Validators.pattern("^[0-9]*$")]], // max is SQL int max, pattern is for only positive int
      'therapyDuration': [null, [Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]], // SQL decimal(6,2)
      'hyperlipidaemiaMedHist': [this._hyperlipidaemiaMedList[0]],
      'atheroscleroticYN': [null, Validators.required],
      'hyperlipPathResults': [null],
      'hypertensionTreatmentYN': [null, Validators.required],
      'geneticHyperlipidaemiaYN': [null, Validators.required],
      'maleBloodRelativeYN': [null, Validators.required],
      'femaleBloodRelativeYN': [null, Validators.required],
      'tendonXanthomaYN': [null, Validators.required]
    })

    // Make therapyDuration and Medhist required only if therapyYN = Y
    this.hyperlipidaemiaForm.get('hyperlipTherapyYN').valueChanges
      .subscribe(value => {

        // Declare validators that should always be on therapyDuration control
        let therapyDurationValidators = [
          Validators.max(9999.99),
          this._validationService.decimalOrIntCustomValidator.bind(this)
        ];

        // Check value of baselineOrTherapy
        if (value == 'Y') {
          // Set always validators and add required validator
          this.hyperlipidaemiaForm.get('therapyDuration').setValidators(therapyDurationValidators.concat(Validators.required));
          this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').setValidators(Validators.required);
        } else {
          // Set always validators without required validator
          this.hyperlipidaemiaForm.get('therapyDuration').setValidators(therapyDurationValidators);
          this.hyperlipidaemiaForm.get('therapyDuration').setValue(null);
          this.hyperlipidaemiaForm.get('therapyDuration').reset();
          this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').setValidators(null);
          this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').setValue(null);
          this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').reset();
          this._hyperlipidaemiaMedList = [];
        }

        // Validate therapyDuration
        this.hyperlipidaemiaForm.get('therapyDuration').updateValueAndValidity();
        this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').updateValueAndValidity();
      });

    // Make hyperlipPathResults required if biometricForm.diabetesYN = 'Y'
    this.biometricForm.get('diabetesYN').valueChanges
      .subscribe(value => {

        // Check value of diabetesYN
        if (value == 'Y') {
          this.hyperlipidaemiaForm.get('hyperlipPathResults').setValidators(Validators.required);
        }
        else {
          this.hyperlipidaemiaForm.get('hyperlipPathResults').setValidators(null);
          this.hyperlipidaemiaForm.get('hyperlipPathResults').setValue(null);
          this.hyperlipidaemiaForm.get('hyperlipPathResults').reset();
          this._hyperlipPathResultFiles = [];
        }

        // Validate fastingOrRandom 
        this.hyperlipidaemiaForm.get('hyperlipPathResults').updateValueAndValidity();
      });

    this.hypertensionForm = this.fb.group({
      'hypertIcd10': [null, [Validators.required, Validators.maxLength(10)]],
      'severity': [null, Validators.required],
      'hypertTherapyYN': [null, Validators.required],
      'hypertensionMedHist': [this._hypertensionMedList[0]],
    })

    // Make hypertensionMedHist required if therapyYN = 'Y'
    this.hypertensionForm.get('hypertTherapyYN').valueChanges
      .subscribe(value => {

        // Check value of type2DiabYN
        if (value == 'Y') {
          this.hypertensionForm.get('hypertensionMedHist').setValidators(Validators.required);
        }
        else {
          this.hypertensionForm.get('hypertensionMedHist').setValidators(null);
          this.hypertensionForm.get('hypertensionMedHist').setValue(null);
          this.hypertensionForm.get('hypertensionMedHist').reset();
          this._hypertensionMedList = [];
        }

        // Validate fastingOrRandom
        this.hypertensionForm.get('hypertensionMedHist').updateValueAndValidity();
      });

    this.asthmaForm = this.fb.group({
      'asthmaIcd10': [null, [Validators.required, Validators.maxLength(10)]],
      //'asthmaFlowVolume': [null, [Validators.required, Validators.maxLength(10)]], //Flow Volume test are used more to diagnose pulmonary conditions and it is a graph that is interpreted by the physician. Therefore the latter will also be a document that must be attached and accessible. I would suggest that we-as a start-only include the peak flow measurements for the GP Network as Flow volume tests require specialised equipment that not all GP's will have in their in rooms. I hope this makes sense to you?
      'asthmaPleakFlow': [null, [Validators.required, Validators.max(2147483647), Validators.pattern("^[0-9]*$")]], // max is SQL int max, pattern is for only positive int
      'asthmaYounger3YN': [null, Validators.required],
      'asthmaPaedReport': [null],
      'asthmaFlowVolFile': [null],
    })

    // Make asthmaPaedReport required only if asthmaYounger3YN = 'Y' and it is an Application (ie newly diagnosed)
    this.asthmaForm.get('asthmaYounger3YN').valueChanges
      .subscribe(value => {

        // Check value of asthmaYounger3YN
        if (value == 'Y' && this._currentPage == 'ChronicCondApp') {

          // Make required
          this.asthmaForm.get('asthmaPaedReport').setValidators(Validators.required);

          // Clear flow volume 
          this.asthmaForm.get('asthmaFlowVolFile').setValue(null);
          this.asthmaForm.get('asthmaFlowVolFile').reset();
          this._asthmaFlowVolFiles = [];
        }
        else if (value == 'N' && this._currentPage == 'ChronicCondApp') {

          // Clear Paed Report and make not required
          this.asthmaForm.get('asthmaPaedReport').setValidators(null);
          this.asthmaForm.get('asthmaPaedReport').setValue(null);
          this.asthmaForm.get('asthmaPaedReport').reset();
          this._asthmaPaedFiles = [];
        }
        else { // If not Application then it is not rewuired and wont be displayed in html

          // Clear Paed Report and make not required
          this.asthmaForm.get('asthmaPaedReport').setValidators(null);
          this.asthmaForm.get('asthmaPaedReport').setValue(null);
          this.asthmaForm.get('asthmaPaedReport').reset();
          this._asthmaPaedFiles = [];

          // Clear flow volume 
          this.asthmaForm.get('asthmaFlowVolFile').setValue(null);
          this.asthmaForm.get('asthmaFlowVolFile').reset();
          this._asthmaFlowVolFiles = [];
        }

        // Validate asthmaPaedReport and asthmaFlowVolFile
        this.asthmaForm.get('asthmaPaedReport').updateValueAndValidity();
        this.asthmaForm.get('asthmaFlowVolFile').updateValueAndValidity();
      });

    this.requestedMedicationForm = this.fb.group({
      'requestedMedList': [this._requestedMedList[0]],
      'medPrescription': [null]
    })

    // Make medPrescription required if requestedMedList != null
    this.requestedMedicationForm.get('requestedMedList').valueChanges
      .subscribe(value => {

        // Check value of type2DiabYN
        if (value != null) {
          this.requestedMedicationForm.get('medPrescription').setValidators(Validators.required);
          this.requestedMedicationForm.get('medPrescription').markAsTouched();
        }
        else {
          this.requestedMedicationForm.get('medPrescription').setValidators(null);
          this.requestedMedicationForm.get('medPrescription').setValue(null);
          this.requestedMedicationForm.get('medPrescription').reset();
          this._medPrescriptionFiles = [];
        }

        // Validate fastingOrRandom 
        this.requestedMedicationForm.get('medPrescription').updateValueAndValidity();
      });

    this.medicationForm = this.fb.group({
      'conditionCodeM': [null],
      'icd10': [null, [Validators.required, Validators.maxLength(10)]],
      'prescriptionSvcCode': [null],
      'svcCodeDescr': [null, [Validators.required, Validators.maxLength(200)]],
      'strength': [null, [Validators.required, Validators.maxLength(100)]],
      'dosage': [null, [Validators.required, Validators.maxLength(100)]],
      'qtyPerMonth': [null, [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]],  // SQL decimal(6,2)
      'numRepeats': [null, [Validators.required, Validators.max(2147483647), Validators.pattern("^[0-9]*$")]]    // max is SQL int max, pattern is for only positive int
    });

    this.documentsForm = this.fb.group({
      'additionalDocs': [null]
    })

    this.comorbidForm = this.fb.group({
      'asthmaCmChk': [false],
      'asthmaCmICD10': [null],
      'asthmaCmMedHist': [this._asthmaComorbidMedList[0]],

      'diabCmChk': [false],
      'diabCmICD10': [null],
      'diabCmMedHist': [this._diabComorbidMedList[0]],

      'hyperlipCmChk': [false],
      'hyperlipCmICD10': [null],
      'hyperlipCmMedHist': [this._hyperlipComorbidMedList[0]],

      'hypertensionCmChk': [false],
      'hypertensionCmICD10': [null],
      'hypertensionCmMedHist': [this._hypertensionComorbidMedList[0]],

      'addisCmChk': [false],
      'addisCmICD10': [null],
      'addisCmMedHist': [this._addisComorbidMedList[0]],

      'bipolCmChk': [false],
      'bipolCmICD10': [null],
      'bipolCmMedHist': [this._bipolComorbidMedList[0]],

      'bronchCmChk': [false],
      'bronchCmICD10': [null],
      'bronchCmMedHist': [this._bronchComorbidMedList[0]],

      'cardiacCmChk': [false],
      'cardiacCmICD10': [null],
      'cardiacCmMedHist': [this._cardiacComorbidMedList[0]],

      'cardiomCmChk': [false],
      'cardiomCmICD10': [null],
      'cardiomCmMedHist': [this._cardiomComorbidMedList[0]],

      'copdCmChk': [false],
      'copdCmICD10': [null],
      'copdCmMedHist': [this._copdComorbidMedList[0]],

      'renalCmChk': [false],
      'renalCmICD10': [null],
      'renalCmMedHist': [this._renalComorbidMedList[0]],

      'coronarCmChk': [false],
      'coronarCmICD10': [null],
      'coronarCmMedHist': [this._coronarComorbidMedList[0]],

      'crohnsCmChk': [false],
      'crohnsCmICD10': [null],
      'crohnsCmMedHist': [this._crohnsComorbidMedList[0]],

      'diabinsipCmChk': [false],
      'diabinsipCmICD10': [null],
      'diabinsipCmMedHist': [this._diabinsipComorbidMedList[0]],

      'dysrhytCmChk': [false],
      'dysrhytCmICD10': [null],
      'dysrhytCmMedHist': [this._dysrhytComorbidMedList[0]],

      'epilCmChk': [false],
      'epilCmICD10': [null],
      'epilCmMedHist': [this._epilComorbidMedList[0]],

      'glaucomCmChk': [false],
      'glaucomCmICD10': [null],
      'glaucomCmMedHist': [this._glaucomComorbidMedList[0]],

      'haemophilCmChk': [false],
      'haemophilCmICD10': [null],
      'haemophilCmMedHist': [this._haemophilComorbidMedList[0]],

      'hypothCmChk': [false],
      'hypothCmICD10': [null],
      'hypothCmMedHist': [this._hypothComorbidMedList[0]],

      'sclerosisCmChk': [false],
      'sclerosisCmICD10': [null],
      'sclerosisCmMedHist': [this._sclerosisComorbidMedList[0]],

      'parkinsCmChk': [false],
      'parkinsCmICD10': [null],
      'parkinsCmMedHist': [this._parkinsComorbidMedList[0]],

      'rheumaCmChk': [false],
      'rheumaCmICD10': [null],
      'rheumaCmMedHist': [this._rheumaComorbidMedList[0]],

      'schizoCmChk': [false],
      'schizoCmICD10': [null],
      'schizoCmMedHist': [this._schizoComorbidMedList[0]],

      'lupusCmChk': [false],
      'lupusCmICD10': [null],
      'lupusCmMedHist': [this._lupusComorbidMedList[0]],

      'ulceraCmChk': [false],
      'ulceraCmICD10': [null],
      'ulceraCmMedHist': [this._ulceraComorbidMedList[0]],

    });

    // Set asthmaCmICD10 and asthmaCmMedHist required only if asthmaCmChk checkbox is checked
    this.comorbidForm.get('asthmaCmChk').valueChanges
      .subscribe(value => {

        // Check value of asthmaCmChk
        if (value == true) {
          this.comorbidForm.get('asthmaCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('asthmaCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('asthmaCmICD10').setValidators(null);
          this.comorbidForm.get('asthmaCmICD10').setValue(null);
          this.comorbidForm.get('asthmaCmICD10').reset();
          this.comorbidForm.get('asthmaCmMedHist').setValidators(null);
          this.comorbidForm.get('asthmaCmMedHist').setValue(null);
          this.comorbidForm.get('asthmaCmMedHist').reset();
          this._asthmaComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('asthmaCmICD10').updateValueAndValidity();
        this.comorbidForm.get('asthmaCmMedHist').updateValueAndValidity();
      });

    // Set diabCmICD10 and diabCmMedHist required only if diabCmChk checkbox is checked
    this.comorbidForm.get('diabCmChk').valueChanges
      .subscribe(value => {

        // Check value of diabCmChk
        if (value == true) {
          this.comorbidForm.get('diabCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('diabCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('diabCmICD10').setValidators(null);
          this.comorbidForm.get('diabCmICD10').setValue(null);
          this.comorbidForm.get('diabCmICD10').reset();
          this.comorbidForm.get('diabCmMedHist').setValidators(null);
          this.comorbidForm.get('diabCmMedHist').setValue(null);
          this.comorbidForm.get('diabCmMedHist').reset();
          this._diabComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('diabCmICD10').updateValueAndValidity();
        this.comorbidForm.get('diabCmMedHist').updateValueAndValidity();
      });

    // Set hyperlipCmICD10 and hyperlipCmMedHist required only if hyperlipCmChk checkbox is checked
    this.comorbidForm.get('hyperlipCmChk').valueChanges
      .subscribe(value => {

        // Check value of hyperlipCmChk
        if (value == true) {
          this.comorbidForm.get('hyperlipCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('hyperlipCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('hyperlipCmICD10').setValidators(null);
          this.comorbidForm.get('hyperlipCmICD10').setValue(null);
          this.comorbidForm.get('hyperlipCmICD10').reset();
          this.comorbidForm.get('hyperlipCmMedHist').setValidators(null);
          this.comorbidForm.get('hyperlipCmMedHist').setValue(null);
          this.comorbidForm.get('hyperlipCmMedHist').reset();
          this._hyperlipComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('hyperlipCmICD10').updateValueAndValidity();
        this.comorbidForm.get('hyperlipCmMedHist').updateValueAndValidity();
      });

    // Set hypertensionCmICD10 and hypertensionCmMedHist required only if hypertensionCmChk checkbox is checked
    this.comorbidForm.get('hypertensionCmChk').valueChanges
      .subscribe(value => {

        // Check value of hyperlipCmChk
        if (value == true) {
          this.comorbidForm.get('hypertensionCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('hypertensionCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('hypertensionCmICD10').setValidators(null);
          this.comorbidForm.get('hypertensionCmICD10').setValue(null);
          this.comorbidForm.get('hypertensionCmICD10').reset();
          this.comorbidForm.get('hypertensionCmMedHist').setValidators(null);
          this.comorbidForm.get('hypertensionCmMedHist').setValue(null);
          this.comorbidForm.get('hypertensionCmMedHist').reset();
          this._hypertensionComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('hypertensionCmICD10').updateValueAndValidity();
        this.comorbidForm.get('hypertensionCmMedHist').updateValueAndValidity();
      });

    // Set addisCmICD10 and addisCmMedHist required only if addisCmChk checkbox is checked
    this.comorbidForm.get('addisCmChk').valueChanges
      .subscribe(value => {

        // Check value of addisCmChk
        if (value == true) {
          this.comorbidForm.get('addisCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('addisCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('addisCmICD10').setValidators(null);
          this.comorbidForm.get('addisCmICD10').setValue(null);
          this.comorbidForm.get('addisCmICD10').reset();
          this.comorbidForm.get('addisCmMedHist').setValidators(null);
          this.comorbidForm.get('addisCmMedHist').setValue(null);
          this.comorbidForm.get('addisCmMedHist').reset();
          this._addisComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('addisCmICD10').updateValueAndValidity();
        this.comorbidForm.get('addisCmMedHist').updateValueAndValidity();
      });

    // Set bipolCmICD10 and bipolCmMedHist required only if bipolCmChk checkbox is checked
    this.comorbidForm.get('bipolCmChk').valueChanges
      .subscribe(value => {

        // Check value of bipolCmChk
        if (value == true) {
          this.comorbidForm.get('bipolCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('bipolCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('bipolCmICD10').setValidators(null);
          this.comorbidForm.get('bipolCmICD10').setValue(null);
          this.comorbidForm.get('bipolCmICD10').reset();
          this.comorbidForm.get('bipolCmMedHist').setValidators(null);
          this.comorbidForm.get('bipolCmMedHist').setValue(null);
          this.comorbidForm.get('bipolCmMedHist').reset();
          this._bipolComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('bipolCmICD10').updateValueAndValidity();
        this.comorbidForm.get('bipolCmMedHist').updateValueAndValidity();
      });

    // Set bronchCmICD10 and bronchCmMedHist required only if bronchCmChk checkbox is checked
    this.comorbidForm.get('bronchCmChk').valueChanges
      .subscribe(value => {

        // Check value of bronchCmChk
        if (value == true) {
          this.comorbidForm.get('bronchCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('bronchCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('bronchCmICD10').setValidators(null);
          this.comorbidForm.get('bronchCmICD10').setValue(null);
          this.comorbidForm.get('bronchCmICD10').reset();
          this.comorbidForm.get('bronchCmMedHist').setValidators(null);
          this.comorbidForm.get('bronchCmMedHist').setValue(null);
          this.comorbidForm.get('bronchCmMedHist').reset();
          this._bronchComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('bronchCmICD10').updateValueAndValidity();
        this.comorbidForm.get('bronchCmMedHist').updateValueAndValidity();
      });

    // Set cardiacCmICD10 and cardiacCmMedHist required only if cardiacCmChk checkbox is checked
    this.comorbidForm.get('cardiacCmChk').valueChanges
      .subscribe(value => {

        // Check value of cardiacCmChk
        if (value == true) {
          this.comorbidForm.get('cardiacCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('cardiacCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('cardiacCmICD10').setValidators(null);
          this.comorbidForm.get('cardiacCmICD10').setValue(null);
          this.comorbidForm.get('cardiacCmICD10').reset();
          this.comorbidForm.get('cardiacCmMedHist').setValidators(null);
          this.comorbidForm.get('cardiacCmMedHist').setValue(null);
          this.comorbidForm.get('cardiacCmMedHist').reset();
          this._cardiacComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('cardiacCmICD10').updateValueAndValidity();
        this.comorbidForm.get('cardiacCmMedHist').updateValueAndValidity();
      });

    // Set cardiomCmICD10 and cardiomCmMedHist required only if cardiomCmChk checkbox is checked
    this.comorbidForm.get('cardiomCmChk').valueChanges
      .subscribe(value => {

        // Check value of cardiomCmChk
        if (value == true) {
          this.comorbidForm.get('cardiomCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('cardiomCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('cardiomCmICD10').setValidators(null);
          this.comorbidForm.get('cardiomCmICD10').setValue(null);
          this.comorbidForm.get('cardiomCmICD10').reset();
          this.comorbidForm.get('cardiomCmMedHist').setValidators(null);
          this.comorbidForm.get('cardiomCmMedHist').setValue(null);
          this.comorbidForm.get('cardiomCmMedHist').reset();
          this._cardiomComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('cardiomCmICD10').updateValueAndValidity();
        this.comorbidForm.get('cardiomCmMedHist').updateValueAndValidity();
      });

    // Set copdCmICD10 and copdCmMedHist required only if copdCmChk checkbox is checked
    this.comorbidForm.get('copdCmChk').valueChanges
      .subscribe(value => {

        // Check value of copdCmChk
        if (value == true) {
          this.comorbidForm.get('copdCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('copdCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('copdCmICD10').setValidators(null);
          this.comorbidForm.get('copdCmICD10').setValue(null);
          this.comorbidForm.get('copdCmICD10').reset();
          this.comorbidForm.get('copdCmMedHist').setValidators(null);
          this.comorbidForm.get('copdCmMedHist').setValue(null);
          this.comorbidForm.get('copdCmMedHist').reset();
          this._copdComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('copdCmICD10').updateValueAndValidity();
        this.comorbidForm.get('copdCmMedHist').updateValueAndValidity();
      });

    // Set renalCmICD10 and renalCmMedHist required only if renalCmChk checkbox is checked
    this.comorbidForm.get('renalCmChk').valueChanges
      .subscribe(value => {

        // Check value of renalCmChk
        if (value == true) {
          this.comorbidForm.get('renalCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('renalCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('renalCmICD10').setValidators(null);
          this.comorbidForm.get('renalCmICD10').setValue(null);
          this.comorbidForm.get('renalCmICD10').reset();
          this.comorbidForm.get('renalCmMedHist').setValidators(null);
          this.comorbidForm.get('renalCmMedHist').setValue(null);
          this.comorbidForm.get('renalCmMedHist').reset();
          this._renalComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('renalCmICD10').updateValueAndValidity();
        this.comorbidForm.get('renalCmMedHist').updateValueAndValidity();
      });

    // Set coronarCmICD10 and coronarCmMedHist required only if coronarCmChk checkbox is checked
    this.comorbidForm.get('coronarCmChk').valueChanges
      .subscribe(value => {

        // Check value of coronarCmChk
        if (value == true) {
          this.comorbidForm.get('coronarCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('coronarCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('coronarCmICD10').setValidators(null);
          this.comorbidForm.get('coronarCmICD10').setValue(null);
          this.comorbidForm.get('coronarCmICD10').reset();
          this.comorbidForm.get('coronarCmMedHist').setValidators(null);
          this.comorbidForm.get('coronarCmMedHist').setValue(null);
          this.comorbidForm.get('coronarCmMedHist').reset();
          this._coronarComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('coronarCmICD10').updateValueAndValidity();
        this.comorbidForm.get('coronarCmMedHist').updateValueAndValidity();
      });

    // Set crohnsCmICD10 and crohnsCmMedHist required only if crohnsCmChk checkbox is checked
    this.comorbidForm.get('crohnsCmChk').valueChanges
      .subscribe(value => {

        // Check value of crohnsCmChk
        if (value == true) {
          this.comorbidForm.get('crohnsCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('crohnsCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('crohnsCmICD10').setValidators(null);
          this.comorbidForm.get('crohnsCmICD10').setValue(null);
          this.comorbidForm.get('crohnsCmICD10').reset();
          this.comorbidForm.get('crohnsCmMedHist').setValidators(null);
          this.comorbidForm.get('crohnsCmMedHist').setValue(null);
          this.comorbidForm.get('crohnsCmMedHist').reset();
          this._crohnsComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('crohnsCmICD10').updateValueAndValidity();
        this.comorbidForm.get('crohnsCmMedHist').updateValueAndValidity();
      });

    // Set diabinsipCmICD10 and diabinsipCmMedHist required only if diabinsipCmChk checkbox is checked
    this.comorbidForm.get('diabinsipCmChk').valueChanges
      .subscribe(value => {

        // Check value of diabinsipCmChk
        if (value == true) {
          this.comorbidForm.get('diabinsipCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('diabinsipCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('diabinsipCmICD10').setValidators(null);
          this.comorbidForm.get('diabinsipCmICD10').setValue(null);
          this.comorbidForm.get('diabinsipCmICD10').reset();
          this.comorbidForm.get('diabinsipCmMedHist').setValidators(null);
          this.comorbidForm.get('diabinsipCmMedHist').setValue(null);
          this.comorbidForm.get('diabinsipCmMedHist').reset();
          this._diabinsipComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('diabinsipCmICD10').updateValueAndValidity();
        this.comorbidForm.get('diabinsipCmMedHist').updateValueAndValidity();
      });

    // Set dysrhytCmICD10 and dysrhytCmMedHist required only if dysrhytCmChk checkbox is checked
    this.comorbidForm.get('dysrhytCmChk').valueChanges
      .subscribe(value => {

        // Check value of dysrhytCmChk
        if (value == true) {
          this.comorbidForm.get('dysrhytCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('dysrhytCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('dysrhytCmICD10').setValidators(null);
          this.comorbidForm.get('dysrhytCmICD10').setValue(null);
          this.comorbidForm.get('dysrhytCmICD10').reset();
          this.comorbidForm.get('dysrhytCmMedHist').setValidators(null);
          this.comorbidForm.get('dysrhytCmMedHist').setValue(null);
          this.comorbidForm.get('dysrhytCmMedHist').reset();
          this._dysrhytComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('dysrhytCmICD10').updateValueAndValidity();
        this.comorbidForm.get('dysrhytCmMedHist').updateValueAndValidity();
      });

    // Set epilCmICD10 and epilCmMedHist required only if epilCmChk checkbox is checked
    this.comorbidForm.get('epilCmChk').valueChanges
      .subscribe(value => {

        // Check value of epilCmChk
        if (value == true) {
          this.comorbidForm.get('epilCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('epilCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('epilCmICD10').setValidators(null);
          this.comorbidForm.get('epilCmICD10').setValue(null);
          this.comorbidForm.get('epilCmICD10').reset();
          this.comorbidForm.get('epilCmMedHist').setValidators(null);
          this.comorbidForm.get('epilCmMedHist').setValue(null);
          this.comorbidForm.get('epilCmMedHist').reset();
          this._epilComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('epilCmICD10').updateValueAndValidity();
        this.comorbidForm.get('epilCmMedHist').updateValueAndValidity();
      });

    // Set glaucomCmICD10 and glaucomCmMedHist required only if glaucomCmChk checkbox is checked
    this.comorbidForm.get('glaucomCmChk').valueChanges
      .subscribe(value => {

        // Check value of glaucomCmChk
        if (value == true) {
          this.comorbidForm.get('glaucomCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('glaucomCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('glaucomCmICD10').setValidators(null);
          this.comorbidForm.get('glaucomCmICD10').setValue(null);
          this.comorbidForm.get('glaucomCmICD10').reset();
          this.comorbidForm.get('glaucomCmMedHist').setValidators(null);
          this.comorbidForm.get('glaucomCmMedHist').setValue(null);
          this.comorbidForm.get('glaucomCmMedHist').reset();
          this._glaucomComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('glaucomCmICD10').updateValueAndValidity();
        this.comorbidForm.get('glaucomCmMedHist').updateValueAndValidity();
      });

    // Set haemophilCmICD10 and haemophilCmMedHist required only if haemophilCmChk checkbox is checked
    this.comorbidForm.get('haemophilCmChk').valueChanges
      .subscribe(value => {

        // Check value of haemophilCmChk
        if (value == true) {
          this.comorbidForm.get('haemophilCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('haemophilCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('haemophilCmICD10').setValidators(null);
          this.comorbidForm.get('haemophilCmICD10').setValue(null);
          this.comorbidForm.get('haemophilCmICD10').reset();
          this.comorbidForm.get('haemophilCmMedHist').setValidators(null);
          this.comorbidForm.get('haemophilCmMedHist').setValue(null);
          this.comorbidForm.get('haemophilCmMedHist').reset();
          this._haemophilComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('haemophilCmICD10').updateValueAndValidity();
        this.comorbidForm.get('haemophilCmMedHist').updateValueAndValidity();
      });

    // Set hypothCmICD10 and hypothCmMedHist required only if hypothCmChk checkbox is checked
    this.comorbidForm.get('hypothCmChk').valueChanges
      .subscribe(value => {

        // Check value of hypothCmChk
        if (value == true) {
          this.comorbidForm.get('hypothCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('hypothCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('hypothCmICD10').setValidators(null);
          this.comorbidForm.get('hypothCmICD10').setValue(null);
          this.comorbidForm.get('hypothCmICD10').reset();
          this.comorbidForm.get('hypothCmMedHist').setValidators(null);
          this.comorbidForm.get('hypothCmMedHist').setValue(null);
          this.comorbidForm.get('hypothCmMedHist').reset();
          this._hypothComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('hypothCmICD10').updateValueAndValidity();
        this.comorbidForm.get('hypothCmMedHist').updateValueAndValidity();
      });

    // Set sclerosisCmICD10 and sclerosisCmMedHist required only if sclerosisCmChk checkbox is checked
    this.comorbidForm.get('sclerosisCmChk').valueChanges
      .subscribe(value => {

        // Check value of sclerosisCmChk
        if (value == true) {
          this.comorbidForm.get('sclerosisCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('sclerosisCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('sclerosisCmICD10').setValidators(null);
          this.comorbidForm.get('sclerosisCmICD10').setValue(null);
          this.comorbidForm.get('sclerosisCmICD10').reset();
          this.comorbidForm.get('sclerosisCmMedHist').setValidators(null);
          this.comorbidForm.get('sclerosisCmMedHist').setValue(null);
          this.comorbidForm.get('sclerosisCmMedHist').reset();
          this._sclerosisComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('sclerosisCmICD10').updateValueAndValidity();
        this.comorbidForm.get('sclerosisCmMedHist').updateValueAndValidity();
      });

    // Set parkinsCmICD10 and parkinsCmMedHist required only if parkinsCmChk checkbox is checked
    this.comorbidForm.get('parkinsCmChk').valueChanges
      .subscribe(value => {

        // Check value of parkinsCmChk
        if (value == true) {
          this.comorbidForm.get('parkinsCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('parkinsCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('parkinsCmICD10').setValidators(null);
          this.comorbidForm.get('parkinsCmICD10').setValue(null);
          this.comorbidForm.get('parkinsCmICD10').reset();
          this.comorbidForm.get('parkinsCmMedHist').setValidators(null);
          this.comorbidForm.get('parkinsCmMedHist').setValue(null);
          this.comorbidForm.get('parkinsCmMedHist').reset();
          this._parkinsComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('parkinsCmICD10').updateValueAndValidity();
        this.comorbidForm.get('parkinsCmMedHist').updateValueAndValidity();
      });

    // Set rheumaCmICD10 and rheumaCmMedHist required only if rheumaCmChk checkbox is checked
    this.comorbidForm.get('rheumaCmChk').valueChanges
      .subscribe(value => {

        // Check value of rheumaCmChk
        if (value == true) {
          this.comorbidForm.get('rheumaCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('rheumaCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('rheumaCmICD10').setValidators(null);
          this.comorbidForm.get('rheumaCmICD10').setValue(null);
          this.comorbidForm.get('rheumaCmICD10').reset();
          this.comorbidForm.get('rheumaCmMedHist').setValidators(null);
          this.comorbidForm.get('rheumaCmMedHist').setValue(null);
          this.comorbidForm.get('rheumaCmMedHist').reset();
          this._rheumaComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('rheumaCmICD10').updateValueAndValidity();
        this.comorbidForm.get('rheumaCmMedHist').updateValueAndValidity();
      });

    // Set schizoCmICD10 and schizoCmMedHist required only if schizoCmChk checkbox is checked
    this.comorbidForm.get('schizoCmChk').valueChanges
      .subscribe(value => {

        // Check value of schizoCmChk
        if (value == true) {
          this.comorbidForm.get('schizoCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('schizoCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('schizoCmICD10').setValidators(null);
          this.comorbidForm.get('schizoCmICD10').setValue(null);
          this.comorbidForm.get('schizoCmICD10').reset();
          this.comorbidForm.get('schizoCmMedHist').setValidators(null);
          this.comorbidForm.get('schizoCmMedHist').setValue(null);
          this.comorbidForm.get('schizoCmMedHist').reset();
          this._schizoComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('schizoCmICD10').updateValueAndValidity();
        this.comorbidForm.get('schizoCmMedHist').updateValueAndValidity();
      });

    // Set lupusCmICD10 and lupusCmMedHist required only if lupusCmChk checkbox is checked
    this.comorbidForm.get('lupusCmChk').valueChanges
      .subscribe(value => {

        // Check value of lupusCmChk
        if (value == true) {
          this.comorbidForm.get('lupusCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('lupusCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('lupusCmICD10').setValidators(null);
          this.comorbidForm.get('lupusCmICD10').setValue(null);
          this.comorbidForm.get('lupusCmICD10').reset();
          this.comorbidForm.get('lupusCmMedHist').setValidators(null);
          this.comorbidForm.get('lupusCmMedHist').setValue(null);
          this.comorbidForm.get('lupusCmMedHist').reset();
          this._lupusComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('lupusCmICD10').updateValueAndValidity();
        this.comorbidForm.get('lupusCmMedHist').updateValueAndValidity();
      });

    // Set ulceraCmICD10 and ulceraCmMedHist required only if ulceraCmChk checkbox is checked
    this.comorbidForm.get('ulceraCmChk').valueChanges
      .subscribe(value => {

        // Check value of ulceraCmChk
        if (value == true) {
          this.comorbidForm.get('ulceraCmICD10').setValidators(Validators.required);
          this.comorbidForm.get('ulceraCmMedHist').setValidators(Validators.required);
        }
        else {
          this.comorbidForm.get('ulceraCmICD10').setValidators(null);
          this.comorbidForm.get('ulceraCmICD10').setValue(null);
          this.comorbidForm.get('ulceraCmICD10').reset();
          this.comorbidForm.get('ulceraCmMedHist').setValidators(null);
          this.comorbidForm.get('ulceraCmMedHist').setValue(null);
          this.comorbidForm.get('ulceraCmMedHist').reset();
          this._ulceraComorbidMedList = [];
        }

        // Update and Validate fields
        this.comorbidForm.get('ulceraCmICD10').updateValueAndValidity();
        this.comorbidForm.get('ulceraCmMedHist').updateValueAndValidity();
      });

    this.declarationForm = this.fb.group({
      'declaration': [false, Validators.requiredTrue],
      'declarationDate': [null, [Validators.required, this._validationService.dateCustomValidator.bind(this, new Date("1900-01-01"), new Date())]]
    });

  }

  loadChronicConditionSetup() {
    // Only set values if not already set
    if (this._conditionDiags.length < 1) {

      let userName: ChronicConditionDiagsModel = new ChronicConditionDiagsModel();
      userName.user = this._loginService.CurrentLoggedInUser.username;

      let userN: SpecCodes = new SpecCodes();
      userN.user = this._loginService.CurrentLoggedInUser.username;

      let userM: MedicineCodesModel = new MedicineCodesModel();
      userM.user = this._loginService.CurrentLoggedInUser.username;

      this.getChronicConditions(userName);
      this.getConditionDiags(userName);
      this.getMedicationDiags(userName);
      this.getReferralSpecCodes(userN);
      this.getMedicineCodes(userM);
    }
  }

  getChronicConditions(userName: ChronicConditionDiagsModel) {

    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/ChronicConditions/GetChronicConditions', userName,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (results: ChronicConditionCodesModel[]) => {
        if (results.length > 0) {
          if (results[0].success) {
            this._chronicConditionsAll = results;
            this._chronicConditionsSelectable = results.filter(item => item.selectable == '1');
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Page load failed";
            this._errorMessage = "This page did not load correctly, please refresh." + results[0].message;
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._error = true;
        this._errorMessageHeading = "Page load failed";
        this._errorMessage = "This page did not load correctly, please refresh." + error.message;
      },
      () => { }
    )
  }

  getConditionDiags(userName: ChronicConditionDiagsModel) {
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/ChronicConditions/GetConditionDiags', userName,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (results: ChronicConditionDiagsModel[]) => {
        if (results.length > 0) {
          if (results[0].success) {
            this._conditionDiags = results;
            this._diabetesDiags = this._conditionDiags.filter(x => x.conditionCode == "DIAB1" || x.conditionCode == "DIAB2");
            this._hyperLipDiags = this._conditionDiags.filter(x => x.conditionCode == "CHOL");
            this._hypertensionDiags = this._conditionDiags.filter(x => x.conditionCode == "HYPER");
            this._asthmaDiags = this._conditionDiags.filter(x => x.conditionCode == "ASTHMA");
            this._addisDiags = this._conditionDiags.filter(x => x.conditionCode == "ADDIS");            
            this._bipolDiags = this._conditionDiags.filter(x => x.conditionCode == "BIPOL");
            this._bronchDiags = this._conditionDiags.filter(x => x.conditionCode == "BRONCH");
            this._cardiacDiags = this._conditionDiags.filter(x => x.conditionCode == "CARDIAC");
            this._cardiomDiags = this._conditionDiags.filter(x => x.conditionCode == "CARDIOM");
            this._copdDiags = this._conditionDiags.filter(x => x.conditionCode == "COPD");
            this._renalDiags = this._conditionDiags.filter(x => x.conditionCode == "RENAL");
            this._coronarDiags = this._conditionDiags.filter(x => x.conditionCode == "CORONAR");
            this._crohnsDiags = this._conditionDiags.filter(x => x.conditionCode == "CROHNS");
            this._diabinsipDiags = this._conditionDiags.filter(x => x.conditionCode == "DIABINSIP");
            this._dysrhytDiags = this._conditionDiags.filter(x => x.conditionCode == "DYSRHYT");
            this._epilDiags = this._conditionDiags.filter(x => x.conditionCode == "EPIL");
            this._glaucomDiags = this._conditionDiags.filter(x => x.conditionCode == "GLAUCOM");
            this._haemophilDiags = this._conditionDiags.filter(x => x.conditionCode == "HAEMOPHIL");
            this._hypothDiags = this._conditionDiags.filter(x => x.conditionCode == "HYPOTH");
            this._sclerosisDiags = this._conditionDiags.filter(x => x.conditionCode == "SCLEROSIS");            
            this._parkinsDiags = this._conditionDiags.filter(x => x.conditionCode == "PARKINS");
            this._rheumaDiags = this._conditionDiags.filter(x => x.conditionCode == "RHEUMA");
            this._schizoDiags = this._conditionDiags.filter(x => x.conditionCode == "SCHIZO");      
            this._lupusDiags = this._conditionDiags.filter(x => x.conditionCode == "LUPUS");
            this._ulceraDiags = this._conditionDiags.filter(x => x.conditionCode == "ULCERA");
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Page load failed";
            //this.errorMessage = results[0].message;
            this._errorMessage = "This page did not load correctly, please refresh.";
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._error = true;
        this._errorMessageHeading = "Page load failed";
        this._errorMessage = "This page did not load correctly, please refresh." + error.message;
      },
      () => { }
    )
  }

  getMedicationDiags(userName: ChronicConditionDiagsModel) {
    let token = localStorage.getItem("jwt");
    return this._http.post(this._baseUrl + 'api/ChronicConditions/GetMedicationDiags', userName,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (results: ChronicConditionDiagsModel[]) => {
        if (results.length > 0) {
          if (results[0].success) {
            this._medicationDiags = results;
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Page load failed";
            //this.errorMessage = results[0].message;
            this._errorMessage = "This page did not load correctly, please refresh.";
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._error = true;
        this._errorMessageHeading = "Page load failed";
        this._errorMessage = "This page did not load correctly, please refresh." + error.message;
      },
      () => { }
    )
  }

  getReferralSpecCodes(userName: SpecCodes) {
    let token = localStorage.getItem("jwt");
    return this._http.post(this._baseUrl + 'api/ChronicConditions/GetReferralSpecCodes', userName,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (results: SpecCodes[]) => {
        if (results.length > 0) {
          if (results[0].success) {
            this._referralSpecs = results;
            //this._referralSpecs.sort((a, b) => a.code.localeCompare(b.code));
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Page load failed";
            //this.errorMessage = results[0].message;
            this._errorMessage = "This page did not load correctly, please refresh.";
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._error = true;
        this._errorMessageHeading = "Page load failed";
        this._errorMessage = "This page did not load correctly, please refresh." + error.message;
      },
      () => { }
    )
  }

  getMedicineCodes(userName: MedicineCodesModel) {
    let token = localStorage.getItem("jwt");
    return this._http.post(this._baseUrl + 'api/ChronicConditions/GetMedicineCodes', userName,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (results: MedicineCodesModel[]) => {
        if (results.length > 0) {
          if (results[0].success) {
            this._medicineCodes = results;
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Page load failed";
            //this.errorMessage = results[0].message;
            this._errorMessage = "This page did not load correctly, please refresh.";
          }
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._error = true;
        this._errorMessageHeading = "Page load failed";
        this._errorMessage = "This page did not load correctly, please refresh." + error.message;
      },
      () => { }
    )
  }

  GetHpContractsForSchemeDropdown() {
    // Remove defaults from list (hpcode '000')
    this._hpContracts = this._loginService.MemberClaimsDropDown.filter((item) => {
      return item.hpcode !== '000' && item.chronicConditionsContract == true;
    });
  }

  SetProviderFormInfo() {
    // Only set values if not already set
    if (this.providerForm.get('provid').value == null) {
      this.providerForm.get('provid').setValue(this._loginService.CurrentLoggedInUser.provID);
      this.providerForm.get('provName').setValue(this._loginService.CurrentLoggedInUser.lastname);
      this.providerForm.get('provContactNo').setValue(this._loginService.CurrentLoggedInUser.contact);
      this.providerForm.get('provEmail').setValue(this._loginService.CurrentLoggedInUser.email);
    }
  }

  onMembidChange() {
    this._familyList = [];
    this.memberForm.get('dependant').reset();
    this.memberForm.get('membEmail').reset();

    this.doResets();
  }

  checkChange() {
    console.log(this.comorbidForm);
  }

  onSearchMemberClick() {
    this.memberForm.markAllAsTouched(); // to display error messages

    let doSearch: boolean = true;

    if (!this.memberForm.get('hp').valid) {
      doSearch = false;
    }

    if (!this.memberForm.get('member').valid || this.memberForm.get('member').value.trim() == '') {
      this.memberForm.get('member').reset(); // in case the user only entered a space or spaces
      this.memberForm.get('member').markAsTouched(); // to display error message
      doSearch = false;
    }

    if (!this.memberForm.get('dob').valid) {
      doSearch = false;
    }

    if (doSearch) {
      this.searchMember();
    }
  }

  searchMember() {

    this._familyList = [];
    this._busy = true;

    this._http.post(this._baseUrl + "api/Member/MemberDetails", {
      MEMBID: this.memberForm.get('member').value.trim(),
      FIRSTNM: '',
      LASTNM: '',
      BIRTH: this.memberForm.get('dob').value,
      HPCODE: this.memberForm.get('hp').value,
      LOBCODE: this._loginService.CurrentLoggedInUser.lobcode,
      USER: this._loginService.CurrentLoggedInUser.username,
      USERTYPE: this._loginService.CurrentLoggedInUser.userType
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._loginService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe(
        (results: MemberEligibilityModel[]) => {

          let currDate: Date = new Date();
          results.forEach((el) => {

            let opthrudt: Date = new Date(el.opthrudt);

            if (el.hpcode == this.memberForm.get('hp').value &&   // To not include old hpcodes
              (el.opthrudt == 'N/A' || opthrudt > currDate))    // Only currently active members
            {
              this._familyList.push(el);
            }
          })

          if (this._familyList.length < 1) {
            this._success = true;
            this._successMessageHeading = "Member not found";
            this._successMessage = "No member found. Please ensure the fund, family/policy number, and date of birth is correct.";
            this.clearAfterMemberSearchFail();
          }

        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._error = true;
          this._errorMessageHeading = "Unexpected error occured";
          //this._errorMessage = "An error occured while searching for the member";
          this._errorMessage = error.message;
          this._busy = false;
          this.clearAfterMemberSearchFail();
        },
        () => {
          this._busy = false;
        });
  }

  clearAfterMemberSearchFail() {
    this.doResets();
    this.memberForm.get('dependant').setValue(null);
    this.memberForm.get('membEmail').setValue(null);
    this._familyList = [];
  }

  dependantSelected() {
    let email: string = this._familyList.filter(x => x.dependant == this.memberForm.get('dependant').value)[0].email;
    if (email != undefined && email.trim() != "") {
      this.memberForm.get("membEmail").setValue(email);
    }
    this.doResets();
  }

  async onConditionChange() {

    // Check prior applications and updates (we might reset the conditioncode to null)    
    await this.checkPriorApplications();
    
    // Uncheck Comorbid checkbox for condition that is selected  
    if (this.conditionForm.get('conditionCode').value == 'ASTHMA') { this.comorbidForm.get('asthmaCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'CHOL') { this.comorbidForm.get('hyperlipCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'HYPER') { this.comorbidForm.get('hypertensionCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'DIAB') { this.comorbidForm.get('diabCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'ADDIS') { this.comorbidForm.get('addisCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'BIPOL') { this.comorbidForm.get('bipolCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'BRONCH') { this.comorbidForm.get('bronchCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'CARDIAC') { this.comorbidForm.get('cardiacCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'CARDIOM') { this.comorbidForm.get('cardiomCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'COPD') { this.comorbidForm.get('copdCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'RENAL') { this.comorbidForm.get('renalCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'CORONAR') { this.comorbidForm.get('coronarCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'CROHNS') { this.comorbidForm.get('crohnsCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'DIABINSIP') { this.comorbidForm.get('diabinsipCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'DYSRHYT') { this.comorbidForm.get('dysrhytCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'EPIL') { this.comorbidForm.get('epilCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'GLAUCOM') { this.comorbidForm.get('glaucomCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'HAEMOPHIL') { this.comorbidForm.get('haemophilCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'HYPOTH') { this.comorbidForm.get('hypothCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'SCLEROSIS') { this.comorbidForm.get('sclerosisCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'PARKINS') { this.comorbidForm.get('parkinsCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'RHEUMA') { this.comorbidForm.get('rheumaCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'SCHIZO') { this.comorbidForm.get('schizoCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'LUPUS') { this.comorbidForm.get('lupusCmChk').setValue(false); }
    else if (this.conditionForm.get('conditionCode').value == 'ULCERA') { this.comorbidForm.get('ulceraCmChk').setValue(false);
    }

    // Do diabetes 
    if (this.conditionForm.get('conditionCode').value == 'DIAB') {
      // Set diabetesYN to Y and disable radio     
      this.biometricForm.get('diabetesYN').reset();
      this.biometricForm.get('diabetesYN').setValue('Y');
      this.biometricForm.get('diabetesYN').markAsTouched;
      this.biometricForm.get('diabetesYN').updateValueAndValidity();
      //this.biometricForm.get('diabetesYN').disable(); // disable if mean control is not 'valid' because disabled is a state and the state is now disabled not valid (even if there is a valid value)

    } else {
      // Enable diabetes radio if selected condition is not diabetes      
      this.biometricForm.get('diabetesYN').reset();
      this.biometricForm.get('diabetesYN').setValue(null);      
      this.biometricForm.get('diabetesYN').updateValueAndValidity();
      //this.biometricForm.get('diabetesYN').enable();
    }

    // Clear comorbid form if neccessary
    if (this.conditionForm.get('conditionCode').value == null || this.conditionForm.get('conditionCode').value == "") {
      this.comorbidForm.reset();
    }
  }

  async checkPriorApplications() {

    let condCode: string = this.conditionForm.get('conditionCode').value;

    if (condCode != null && condCode != "") {

      // Reset vars
      this._approvedApplication = new ChronicConditionApplicationModel();
      this._latestUpdate = new ChronicConditionApplicationModel();

      // Info for backend
      let condInfo: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();
      condInfo.membId = this.FamilyList.filter(x => x.dependant == this.memberForm.get('dependant').value)[0].membid;
      condInfo.provId = this._loginService.CurrentLoggedInUser.provID;
      condInfo.conditionCode = condCode;

      // Check if there is already an approved or prending application condition for this member, if there is then don't allow to create another      
      if (this._currentPage == "ChronicCondApp") {

        //this._busy = true; //Caause error in console, can't figure out why

        let token = localStorage.getItem("jwt");
              
        //let promise = new Promise((resolve, reject) => {
        //var promis = await this._http.post(this._baseUrl + 'api/ChronicConditions/CheckExistingApplications', condInfo,
        await
          this._http.post(this._baseUrl + 'api/ChronicConditions/CheckExistingApplications', condInfo,
            {
              headers: new HttpHeaders({
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json",
                "Cache-Control": "no-store",
                "Pragma": "no-cache"
              })
            }
          ).toPromise()
            .then(
              (result: ChronicConditionApplicationModel) => { // Success
                
                if (result.success) {

                  // There is no approved or pending application for this member, so the user can proceed to capture a new application for this member and condition

                }
                else {

                  this._errorArrayModal = true;

                  if (result.message == "Prior existing application") {
                    this._errorArrayHeading = result.message;
                    this._errorArrayMessage = "There is already an existing application for this member and condition: ";
                    this._errorArrayArr.push("Application Number: " + result.conditionApplicationNumber);
                    this._errorArrayArr.push("Application Date: " + result.applicationDate);
                    this._errorArrayArr.push("Status: " + result.status);
                  }
                  else {
                    this._errorArrayMessage = result.message;
                  }

                  this.conditionForm.reset();
                }

                //this._busy = false;

              },
              (error: HttpErrorResponse) => {
                console.log(error)

                this._error = true;
                this._errorMessageHeading = "Unexpected error occured";
                this._errorMessage = error.message;

                //this._busy = false;            
              }
            );
        //});        
      }

      // Check if there is already an approved application for this member and condition, otherwise they can't do an update
      // If there is an approved condition, return it and the latest update so the provider can monitor progress 
      if (this._currentPage == "ChronicCondUpdate") {

        //this._busy = true;

        let token = localStorage.getItem("jwt");
        await
          this._http.post(this._baseUrl + 'api/ChronicConditions/GetApprovedApplicationAndLastUpdate', condInfo,
            {
              headers: new HttpHeaders({
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json",
                "Cache-Control": "no-store",
                "Pragma": "no-cache"
              })
            }
          ).toPromise()
            .then(
              (results: ChronicConditionApplicationModel[]) => {

                for (let i: number = 0; i < results.length; i++) {
                  if (results[i].success) {

                    if (results[i].status == 'APPROVED') {
                      this._approvedApplication = results[i];                      
                    }

                    if (results[i].status == 'UPDATE') {
                      this._latestUpdate = results[i];
                    }

                  }
                  else {
                    this._error = true;
                    this._errorMessageHeading = "No registered condition found to update.";
                    this._errorMessage = results[i].message;
                    this.conditionForm.reset();
                    break;
                  }
                }
              },
              (error: HttpErrorResponse) => {
                console.log(error)
                this._error = true;
                this._errorMessageHeading = "Unexpected error occured";
                this._errorMessage = error.message;
                //this._busy = false;
              }
            );
      }
    }
    
  }

  calcBmi() {
    this.biometricForm.get('bmi').reset();

    if (this.biometricForm.get('height').value != undefined &&
      this.biometricForm.get('weight').value != undefined &&
      this.biometricForm.get('height').value != null &&
      this.biometricForm.get('weight').value != null &&
      this.biometricForm.get('height').value.trim() != "" &&
      this.biometricForm.get('weight').value.trim() != "") {

      let height: number = this.biometricForm.get('height').value.trim();
      let weight: number = this.biometricForm.get('weight').value.trim();

      let bmi: number = weight / ((height / 100.0) * (height / 100.0));
      bmi = Math.round(bmi * 10) / 10;

      this.biometricForm.get('bmi').setValue(bmi.toString());

      this.biometricForm.get('bmi').updateValueAndValidity();
    }
    return;
  }

  calcLdlHdlRatio() {
    this.biometricForm.get('cholesterolLdlHdlRatio').reset();

    if (this.biometricForm.get('cholesterolLDL').value != undefined &&
      this.biometricForm.get('cholesterolHDL').value != undefined &&
      this.biometricForm.get('cholesterolLDL').value != null &&
      this.biometricForm.get('cholesterolHDL').value != null &&
      this.biometricForm.get('cholesterolLDL').value.trim() != "" &&
      this.biometricForm.get('cholesterolHDL').value.trim() != "") {

      let cholLDL: number = this.biometricForm.get('cholesterolLDL').value.trim();
      let cholHDL: number = this.biometricForm.get('cholesterolHDL').value.trim();

      let ratio: number = cholLDL / cholHDL;
      ratio = Math.round(ratio * 100) / 100;

      this.biometricForm.get('cholesterolLdlHdlRatio').setValue(ratio.toString());
      this.biometricForm.get('cholesterolLdlHdlRatio').updateValueAndValidity();
    }
    return;
  }

  onUploadFile(event, fileArray: FileItemModel[], form: FormGroup, controlName: string) {

    for (let i: number = 0; i < event.addedFiles.length; i++) {

      // Create item
      let eachFile: FileItemModel = new FileItemModel();

      // Convert File to string and assign to item
      this.fileToBase64(event.addedFiles[i])
        .then(result => {
          const base64String = result.replace('data:', '').replace(/^.+,/, ''); // To remove data url part


          eachFile.base64Str = base64String;

          // Set other item properties
          eachFile.name = event.addedFiles[i].name;
          eachFile.type = event.addedFiles[i].type;
          eachFile.fileDesc = controlName;
          eachFile.isEnabled = false;
          eachFile.isProgress = false;
          eachFile.isProgressValue = "0";

          // Add to applicable list
          fileArray.push(eachFile);

          // Do progress bar
          this.callInterval(fileArray);

          // Set form control
          if (fileArray.length > 0) {
            form.get(controlName).setValue(fileArray[0]); // Set formcontrol to first file
          } else {
            form.get(controlName).setValue(null);
          }

        });
    }
  }

  fileToBase64 = (file: File): Promise<string> => {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.toString());
      reader.onerror = error => reject(error);
    })
  }

  callInterval(fileArray: FileItemModel[]) {
    //for (let i = 0; i < this.files.length; i++) {
    for (let i = 0; i < fileArray.length; i++) {
      //let eachFile: any = this.files[i];
      let eachFile: any = fileArray[i];
      if (eachFile.isEnabled === false) {
        this._isFileUploadInProgress = true;
        eachFile.isProgress = true;
        let index: number = 0;
        let interval = setInterval(() => {
          index = index + 5;
          eachFile.isProgressValue = index.toString();
          //this.files[i] = eachFile;
          fileArray[i] = eachFile;
          if (index === 100) {
            this._isFileUploadInProgress = false;
            //this.files[i].isProgress = false;
            fileArray[i].isProgress = false;
            clearInterval(interval);
            //this.callInterval();
            this.callInterval(fileArray);
          }
        }, 200);
        eachFile.isEnabled = true;
        break;
      }
    }
  }

  deleteFile(index: number, fileArray: any, form: FormGroup, controlName: string) {

    //this.files.splice(index, 1);
    fileArray.splice(index, 1);

    if (fileArray.length > 0) {
      form.get(controlName).setValue(fileArray[0]); // Set formcontrol to first file
    } else {
      form.get(controlName).setValue(null);
    }

  }

  // This method gives the tabname and filename a random guid, so not ideal (but it handles download vs open in new tab by itself, which is nice)
  viewFile(vFile: FileItemModel) {
    const fileName = vFile.name;
    const fileBlob = this.dataURItoBlob(vFile.base64Str, vFile.type);
    const file = new File([fileBlob], fileName, { type: vFile.type });  //This step is not neccessary (URL can be created with fileBlob, filename is ignored when using blob)
    const url = URL.createObjectURL(file);
    //let selectedFileBLOB: string = this._domSanitizer.bypassSecurityTrustUrl(encodeURI(url));
    return this._domSanitizer.bypassSecurityTrustUrl(url);
  }

  // This method allows you to specify a filename when downloading. Opening in new tab still makes tab name a giud
  viewFileD(vFile: FileItemModel) {
    const fileName = vFile.name;
    const fileBlob = this.dataURItoBlob(vFile.base64Str, vFile.type);
    const file = new File([fileBlob], fileName, { type: vFile.type });  //This step is not neccessary (URL can be created with fileBlob, filename is ignored when using blob)
    const url = URL.createObjectURL(file);
    const safeUrl = this._domSanitizer.sanitize(SecurityContext.RESOURCE_URL, this._domSanitizer.bypassSecurityTrustResourceUrl(url));

    var anchor = document.createElement("a");
    anchor.href = safeUrl;

    if (vFile.type == 'image/jpeg' || vFile.type == 'image/png' || vFile.type == 'application/pdf') {
      anchor.target = '_blank';      // Open in new tab
    }
    else {
      anchor.download = vFile.name;  // Download
    }
    anchor.click();
  }

  dataURItoBlob(dataURI, fileType) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: fileType });
    return blob;
  }

  medicineSearchSelect() {

    let x: string = 'abc';

  }

  onNewMedicationItem(itemMedType: string) {

    this.medicationForm.reset();
    this._medicationItem = new MedicationModel();
    this._medicationItem.section = itemMedType;

    // Set default Condition dropdown value depending on which section new medicine added
    if (itemMedType == 'requestedMedication' || itemMedType == 'hypertensionOnTherapyMedHist' || itemMedType == 'hyperlipOnTherapyMedHist') {
      this.medicationForm.get('conditionCodeM').setValue(this.conditionForm.get('conditionCode').value);
    }
    else if (itemMedType == 'asthmaComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('ASTHMA'); }
    else if (itemMedType == 'diabComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('DIAB'); }
    else if (itemMedType == 'hyperlipComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('CHOL'); }
    else if (itemMedType == 'hypertensionComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('HYPER'); }
    else if (itemMedType == 'addisComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('ADDIS'); }
    else if (itemMedType == 'bipolComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('BIPOL'); }
    else if (itemMedType == 'bronchComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('BRONCH'); }
    else if (itemMedType == 'cardiacComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('CARDIAC'); }
    else if (itemMedType == 'cardiomComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('CARDIOM'); }
    else if (itemMedType == 'copdComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('COPD'); }
    else if (itemMedType == 'renalComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('RENAL'); }
    else if (itemMedType == 'coronarComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('CORONAR'); }
    else if (itemMedType == 'crohnsComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('CROHNS'); }
    else if (itemMedType == 'diabinsipComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('DIABINSIP'); }
    else if (itemMedType == 'dysrhytComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('DYSRHYT'); }
    else if (itemMedType == 'epilComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('EPIL'); }
    else if (itemMedType == 'glaucomComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('GLAUCOM'); }
    else if (itemMedType == 'haemophilComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('HAEMOPHIL'); }
    else if (itemMedType == 'hypothComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('HYPOTH'); }
    else if (itemMedType == 'sclerosisComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('SCLEROSIS'); }
    else if (itemMedType == 'parkinsComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('PARKINS'); }
    else if (itemMedType == 'rheumaComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('RHEUMA'); }
    else if (itemMedType == 'schizoComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('SCHIZO'); }
    else if (itemMedType == 'lupusComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('LUPUS'); }
    else if (itemMedType == 'ulceraComorbidMedHist') { this.medicationForm.get('conditionCodeM').setValue('ULCERA'); }


    // Filter ICD10 dropdown as per Condition dropdown value
    //this._filteredMedicationDiags = this._medicationDiags.filter(x => x.conditionCode == this.medicationForm.get('conditionCodeM').value);
    this.onMedicationConditionChange();

    // Show form/modal
    this._showMedicationModal = true;
  }

  onMedicationConditionChange() {

    if (this.medicationForm.get('conditionCodeM').value == 'DIAB') { // Conditions DIAB1 and DIAB2 in backend is only one condition in frontend

      if (this.biometricForm.get('diabetesType').value == 'diabetesType1') {

        this._filteredMedicationDiags = this._medicationDiags.filter(x => x.conditionCode == 'DIAB1');
      }
      else if (this.biometricForm.get('diabetesType').value == 'diabetesType2') {

        this._filteredMedicationDiags = this._medicationDiags.filter(x => x.conditionCode == 'DIAB2');
      }
      else {

        this._filteredMedicationDiags = this._medicationDiags.filter(x => x.conditionCode == 'DIAB1' || x.conditionCode == 'DIAB2');

        // Get distinct
        this._filteredMedicationDiags = this._filteredMedicationDiags.filter(
          (thing, i, arr) => arr.findIndex(t => t.diagCode === thing.diagCode) === i
        );

        // Sort
        this._filteredMedicationDiags.sort((a, b) => a.diagCode.localeCompare(b.diagCode));
      }
    }
    else { // All other conditions

      this._filteredMedicationDiags = this._medicationDiags.filter(x => x.conditionCode == this.medicationForm.get('conditionCodeM').value);
    }

    // Set control to first option in list
    //this.medicationForm.get('icd10').setValue(this._filteredMedicationDiags[0].diagCode);

    // Reset control value and validation
    this.medicationForm.get('icd10').setValue(null);
    this.medicationForm.get('icd10').reset();
    this.medicationForm.get('icd10').updateValueAndValidity();
  }

  onEditMedicationItem(itemToEdit: MedicationModel) {

    this.medicationForm.reset();
    this._medicationItem = itemToEdit;

    this.medicationForm.get('conditionCodeM').setValue(itemToEdit.condCode);
    this.medicationForm.get('icd10').setValue(itemToEdit.icd10);
    this.medicationForm.get('prescriptionSvcCode').setValue(itemToEdit.prescriptionSvcCode);
    this.medicationForm.get('svcCodeDescr').setValue(itemToEdit.svcCodeDescr);
    this.medicationForm.get('strength').setValue(itemToEdit.strength);
    this.medicationForm.get('dosage').setValue(itemToEdit.dosage);
    this.medicationForm.get('qtyPerMonth').setValue(itemToEdit.qtyPerMonth);
    this.medicationForm.get('numRepeats').setValue(itemToEdit.numRepeats);

    this._showMedicationModal = true;
  }

  onSaveMedicationItem() {

    this.medicationForm.markAllAsTouched(); // To display error messages

    // Only proceed if valid
    if (this.medicationForm.valid) {

      // Check if new item or item to edit
      if (this._medicationItem.arrayId == undefined) {  // new item

        // Set array id       
        this._medicationItem.arrayId = Date.now().toString();

        // Fill item from form
        this._medicationItem.condCode = this.medicationForm.get('conditionCodeM').value.trim();
        this._medicationItem.icd10 = this.medicationForm.get('icd10').value.trim();
        this._medicationItem.prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        this._medicationItem.svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        this._medicationItem.strength = this.medicationForm.get('strength').value.trim();
        this._medicationItem.dosage = this.medicationForm.get('dosage').value.trim();
        this._medicationItem.qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        this._medicationItem.numRepeats = this.medicationForm.get('numRepeats').value.trim();

        if (this._medicationItem.section == "hypertensionOnTherapyMedHist") {
          this._medicationItem.historyMed = true;
          this._medicationItem.comorbid = false;
          this._hypertensionMedList.push(this._medicationItem);
        }
        else if (this._medicationItem.section == "hyperlipOnTherapyMedHist") {
          this._medicationItem.historyMed = true;
          this._medicationItem.comorbid = false;
          this._hyperlipidaemiaMedList.push(this._medicationItem);
        }
        else if (this._medicationItem.section == "requestedMedication") {
          this._medicationItem.historyMed = false;
          this._medicationItem.comorbid = false;
          this._requestedMedList.push(this._medicationItem);
        }
        else if (this._medicationItem.section.includes('ComorbidMedHist')) {

          this._medicationItem.historyMed = false;
          this._medicationItem.comorbid = true;

          if (this._medicationItem.section == "asthmaComorbidMedHist") { this._asthmaComorbidMedList.push(this._medicationItem); }         
          else if (this._medicationItem.section == "diabComorbidMedHist") { this._diabComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == "hyperlipComorbidMedHist") { this._hyperlipComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == "hypertensionComorbidMedHist") { this._hypertensionComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'addisComorbidMedHist') { this._addisComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'bipolComorbidMedHist') { this._bipolComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'bronchComorbidMedHist') { this._bronchComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'cardiacComorbidMedHist') { this._cardiacComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'cardiomComorbidMedHist') { this._cardiomComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'copdComorbidMedHist') { this._copdComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'renalComorbidMedHist') { this._renalComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'coronarComorbidMedHist') { this._coronarComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'crohnsComorbidMedHist') { this._crohnsComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'diabinsipComorbidMedHist') { this._diabinsipComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'dysrhytComorbidMedHist') { this._dysrhytComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'epilComorbidMedHist') { this._epilComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'glaucomComorbidMedHist') { this._glaucomComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'haemophilComorbidMedHist') { this._haemophilComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'hypothComorbidMedHist') { this._hypothComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'sclerosisComorbidMedHist') { this._sclerosisComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'parkinsComorbidMedHist') { this._parkinsComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'rheumaComorbidMedHist') { this._rheumaComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'schizoComorbidMedHist') { this._schizoComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'lupusComorbidMedHist') { this._lupusComorbidMedList.push(this._medicationItem); }
          else if (this._medicationItem.section == 'ulceraComorbidMedHist') { this._ulceraComorbidMedList.push(this._medicationItem); }
        }

      }
      else { //edit existing item

        if (this._medicationItem.section == "hypertensionOnTherapyMedHist") { this.saveEditedMedItem(this._hypertensionMedList) }
        else if (this._medicationItem.section == "hyperlipOnTherapyMedHist") { this.saveEditedMedItem(this._hyperlipidaemiaMedList) }
        else if (this._medicationItem.section == "requestedMedication") { this.saveEditedMedItem(this._requestedMedList) }
        else if (this._medicationItem.section == "asthmaComorbidMedHist") { this.saveEditedMedItem(this._asthmaComorbidMedList) }
        else if (this._medicationItem.section == "diabComorbidMedHist") { this.saveEditedMedItem(this._diabComorbidMedList) }
        else if (this._medicationItem.section == "hyperlipComorbidMedHist") { this.saveEditedMedItem(this._hyperlipComorbidMedList) }
        else if (this._medicationItem.section == "hypertensionComorbidMedHist") { this.saveEditedMedItem(this._hypertensionComorbidMedList) }
        else if (this._medicationItem.section == 'addisComorbidMedHist') { this.saveEditedMedItem(this._addisComorbidMedList) }
        else if (this._medicationItem.section == 'bipolComorbidMedHist') { this.saveEditedMedItem(this._bipolComorbidMedList) }
        else if (this._medicationItem.section == 'bronchComorbidMedHist') { this.saveEditedMedItem(this._bronchComorbidMedList) }
        else if (this._medicationItem.section == 'cardiacComorbidMedHist') { this.saveEditedMedItem(this._cardiacComorbidMedList) }
        else if (this._medicationItem.section == 'cardiomComorbidMedHist') { this.saveEditedMedItem(this._cardiomComorbidMedList) }
        else if (this._medicationItem.section == 'copdComorbidMedHist') { this.saveEditedMedItem(this._copdComorbidMedList) }
        else if (this._medicationItem.section == 'renalComorbidMedHist') { this.saveEditedMedItem(this._renalComorbidMedList) }
        else if (this._medicationItem.section == 'coronarComorbidMedHist') { this.saveEditedMedItem(this._coronarComorbidMedList) }
        else if (this._medicationItem.section == 'crohnsComorbidMedHist') { this.saveEditedMedItem(this._crohnsComorbidMedList) }
        else if (this._medicationItem.section == 'diabinsipComorbidMedHist') { this.saveEditedMedItem(this._diabinsipComorbidMedList) }
        else if (this._medicationItem.section == 'dysrhytComorbidMedHist') { this.saveEditedMedItem(this._dysrhytComorbidMedList) }
        else if (this._medicationItem.section == 'epilComorbidMedHist') { this.saveEditedMedItem(this._epilComorbidMedList) }
        else if (this._medicationItem.section == 'glaucomComorbidMedHist') { this.saveEditedMedItem(this._glaucomComorbidMedList) }
        else if (this._medicationItem.section == 'haemophilComorbidMedHist') { this.saveEditedMedItem(this._haemophilComorbidMedList) }
        else if (this._medicationItem.section == 'hypothComorbidMedHist') { this.saveEditedMedItem(this._hypothComorbidMedList) }
        else if (this._medicationItem.section == 'sclerosisComorbidMedHist') { this.saveEditedMedItem(this._sclerosisComorbidMedList) }
        else if (this._medicationItem.section == 'parkinsComorbidMedHist') { this.saveEditedMedItem(this._parkinsComorbidMedList) }
        else if (this._medicationItem.section == 'rheumaComorbidMedHist') { this.saveEditedMedItem(this._rheumaComorbidMedList) }
        else if (this._medicationItem.section == 'schizoComorbidMedHist') { this.saveEditedMedItem(this._schizoComorbidMedList) }
        else if (this._medicationItem.section == 'lupusComorbidMedHist') { this.saveEditedMedItem(this._lupusComorbidMedList) }
        else if (this._medicationItem.section == 'ulceraComorbidMedHist') { this.saveEditedMedItem(this._ulceraComorbidMedList) }

        //  if (this._medicationItem.section == "hypertensionOnTherapyMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._hypertensionMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._hypertensionMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._hypertensionMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._hypertensionMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._hypertensionMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._hypertensionMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._hypertensionMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._hypertensionMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._hypertensionMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  //else if (this._medicationItem.hyperlipidaemiaTherapy == true) {
        //  else if (this._medicationItem.section == "hyperlipOnTherapyMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._hyperlipidaemiaMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._hyperlipidaemiaMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._hyperlipidaemiaMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._hyperlipidaemiaMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._hyperlipidaemiaMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value;
        //    this._hyperlipidaemiaMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._hyperlipidaemiaMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._hyperlipidaemiaMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._hyperlipidaemiaMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  else if (this._medicationItem.section == "requestedMedication") {
        //    // Get index of edited item 
        //    let foundIndex = this._requestedMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._requestedMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._requestedMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._requestedMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._requestedMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._requestedMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._requestedMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._requestedMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._requestedMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  else if (this._medicationItem.section == "asthmaComorbidMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._asthmaComorbidMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._asthmaComorbidMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._asthmaComorbidMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._asthmaComorbidMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  else if (this._medicationItem.section == "diabComorbidMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._diabComorbidMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._diabComorbidMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._diabComorbidMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._diabComorbidMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._diabComorbidMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._diabComorbidMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._diabComorbidMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._diabComorbidMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._diabComorbidMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  else if (this._medicationItem.section == "hyperlipComorbidMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._hyperlipComorbidMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._hyperlipComorbidMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._hyperlipComorbidMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._hyperlipComorbidMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
        //  else if (this._medicationItem.section == "hypertensionComorbidMedHist") {
        //    // Get index of edited item 
        //    let foundIndex = this._hypertensionComorbidMedList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

        //    // Update item values from form
        //    this._hypertensionComorbidMedList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
        //    this._hypertensionComorbidMedList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
        //    this._hypertensionComorbidMedList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
        //  }
      }

      // Set form controls for validation purposes
      this.setMedsFormControls();

      // Clear item
      this._medicationItem = new MedicationModel();

      // Clear from
      this.medicationForm.reset();

      // Close modal
      this._showMedicationModal = false;
    }
  }

  saveEditedMedItem(relevantList: MedicationModel[]) {
    // Get index of edited item 
    let foundIndex = relevantList.findIndex(x => x.arrayId == this._medicationItem.arrayId);

    // Update item values from form
    relevantList[foundIndex].condCode = this.medicationForm.get('conditionCodeM').value.trim();
    relevantList[foundIndex].icd10 = this.medicationForm.get('icd10').value.trim();
    relevantList[foundIndex].prescriptionSvcCode = this.medicationForm.get('prescriptionSvcCode').value;
    relevantList[foundIndex].svcCodeDescr = this.medicationForm.get('svcCodeDescr').value.trim();
    relevantList[foundIndex].strength = this.medicationForm.get('strength').value.trim();
    relevantList[foundIndex].dosage = this.medicationForm.get('dosage').value.trim();
    relevantList[foundIndex].qtyPerMonth = this.medicationForm.get('qtyPerMonth').value.trim();
    relevantList[foundIndex].numRepeats = this.medicationForm.get('numRepeats').value.trim();
  }

  onRemoveMedicationItem(itemToRemove: MedicationModel) {

    if (itemToRemove.section == "hypertensionOnTherapyMedHist") { this.RemoveMedItemFromRelevantList(this._hypertensionMedList, itemToRemove) }
    else if (itemToRemove.section == "hyperlipOnTherapyMedHist") { this.RemoveMedItemFromRelevantList(this._hyperlipidaemiaMedList, itemToRemove) }
    else if (itemToRemove.section == "requestedMedication") { this.RemoveMedItemFromRelevantList(this._requestedMedList, itemToRemove) }
    else if (itemToRemove.section == "asthmaComorbidMedHist") { this.RemoveMedItemFromRelevantList(this._asthmaComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == "diabComorbidMedHist") { this.RemoveMedItemFromRelevantList(this._diabComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == "hyperlipComorbidMedHist") { this.RemoveMedItemFromRelevantList(this._hyperlipComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == "hypertensionComorbidMedHist") { this.RemoveMedItemFromRelevantList(this._hypertensionComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'addisComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._addisComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'bipolComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._bipolComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'bronchComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._bronchComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'cardiacComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._cardiacComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'cardiomComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._cardiomComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'copdComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._copdComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'renalComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._renalComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'coronarComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._coronarComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'crohnsComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._crohnsComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'diabinsipComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._diabinsipComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'dysrhytComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._dysrhytComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'epilComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._epilComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'glaucomComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._glaucomComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'haemophilComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._haemophilComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'hypothComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._hypothComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'sclerosisComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._sclerosisComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'parkinsComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._parkinsComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'rheumaComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._rheumaComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'schizoComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._schizoComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'lupusComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._lupusComorbidMedList, itemToRemove) }
    else if (itemToRemove.section == 'ulceraComorbidMedHist') { this.RemoveMedItemFromRelevantList(this._ulceraComorbidMedList, itemToRemove) }

    //if (itemToRemove.section == "hypertensionOnTherapyMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._hypertensionMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._hypertensionMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "hyperlipOnTherapyMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._hyperlipidaemiaMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._hyperlipidaemiaMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "requestedMedication") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._requestedMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._requestedMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "asthmaComorbidMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._asthmaComorbidMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._asthmaComorbidMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "diabComorbidMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._diabComorbidMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._diabComorbidMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "hyperlipComorbidMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._hyperlipComorbidMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._hyperlipComorbidMedList.splice(foundIndex, 1);
    //  }
    //}
    //else if (itemToRemove.section == "hypertensionComorbidMedHist") {
    //  // Get index of item to be removed 
    //  let foundIndex = this._hypertensionComorbidMedList.findIndex(x => x.arrayId == itemToRemove.arrayId);

    //  // remove item
    //  if (foundIndex !== -1) {
    //    this._hypertensionComorbidMedList.splice(foundIndex, 1);
    //  }
    //}

    // Set form controls for validation purposes
    this.setMedsFormControls();
  }

  RemoveMedItemFromRelevantList(relevantList: MedicationModel[], itemToRemove: MedicationModel) {
    // Get index of item to be removed 
    let foundIndex = relevantList.findIndex(x => x.arrayId == itemToRemove.arrayId);
    // remove item
      if (foundIndex !== -1) {
        relevantList.splice(foundIndex, 1);
      }
  }

  setMedsFormControls() {
    // Set form controls for validation purposes
    this.hyperlipidaemiaForm.get('hyperlipidaemiaMedHist').setValue(this._hyperlipidaemiaMedList[0]);
    this.hypertensionForm.get('hypertensionMedHist').setValue(this._hypertensionMedList[0]);
    this.requestedMedicationForm.get('requestedMedList').setValue(this._requestedMedList[0]);
    this.comorbidForm.get('asthmaCmMedHist').setValue(this._asthmaComorbidMedList[0]);
    this.comorbidForm.get('diabCmMedHist').setValue(this._diabComorbidMedList[0]);
    this.comorbidForm.get('hyperlipCmMedHist').setValue(this._hyperlipComorbidMedList[0]);
    this.comorbidForm.get('hypertensionCmMedHist').setValue(this._hypertensionComorbidMedList[0]);
    this.comorbidForm.get('addisCmMedHist').setValue(this._addisComorbidMedList[0]);
    this.comorbidForm.get('bipolCmMedHist').setValue(this._bipolComorbidMedList[0]);
    this.comorbidForm.get('bronchCmMedHist').setValue(this._bronchComorbidMedList[0]);
    this.comorbidForm.get('cardiacCmMedHist').setValue(this._cardiacComorbidMedList[0]);
    this.comorbidForm.get('cardiomCmMedHist').setValue(this._cardiomComorbidMedList[0]);
    this.comorbidForm.get('copdCmMedHist').setValue(this._copdComorbidMedList[0]);
    this.comorbidForm.get('renalCmMedHist').setValue(this._renalComorbidMedList[0]);
    this.comorbidForm.get('coronarCmMedHist').setValue(this._coronarComorbidMedList[0]);
    this.comorbidForm.get('crohnsCmMedHist').setValue(this._crohnsComorbidMedList[0]);
    this.comorbidForm.get('diabinsipCmMedHist').setValue(this._diabinsipComorbidMedList[0]);
    this.comorbidForm.get('dysrhytCmMedHist').setValue(this._dysrhytComorbidMedList[0]);
    this.comorbidForm.get('epilCmMedHist').setValue(this._epilComorbidMedList[0]);
    this.comorbidForm.get('glaucomCmMedHist').setValue(this._glaucomComorbidMedList[0]);
    this.comorbidForm.get('haemophilCmMedHist').setValue(this._haemophilComorbidMedList[0]);
    this.comorbidForm.get('hypothCmMedHist').setValue(this._hypothComorbidMedList[0]);
    this.comorbidForm.get('sclerosisCmMedHist').setValue(this._sclerosisComorbidMedList[0]);
    this.comorbidForm.get('parkinsCmMedHist').setValue(this._parkinsComorbidMedList[0]);
    this.comorbidForm.get('rheumaCmMedHist').setValue(this._rheumaComorbidMedList[0]);
    this.comorbidForm.get('schizoCmMedHist').setValue(this._schizoComorbidMedList[0]);
    this.comorbidForm.get('lupusCmMedHist').setValue(this._lupusComorbidMedList[0]);
    this.comorbidForm.get('ulceraCmMedHist').setValue(this._ulceraComorbidMedList[0]);
  }

  onCancelMedItemForm() {
    this._showMedicationModal = false;
  }

  clearAll() {
    this.doResets();
    this._familyList = [];
    this.memberForm.reset();
  }

  doResets() {

    // Forms
    this.biometricForm.reset();
    this.providerForm.get('referredYN').reset();
    this.providerForm.get('refProvSpec').reset();
    this.providerForm.get('refProvid').reset();
    this.providerForm.get('refProvSurname').reset();
    this.providerForm.get('refProvNotes').reset();
    this.riskFactorsForm.reset();
    this.conditionForm.reset();
    this.diabetesMellitusForm.reset();
    this.hyperlipidaemiaForm.reset();
    this.hypertensionForm.reset();
    this.asthmaForm.reset();
    this.medicationForm.reset();
    this.requestedMedicationForm.reset();
    this.documentsForm.reset();
    this.comorbidForm.reset();
    this.declarationForm.reset();

    // Medication lists
    this._requestedMedList = [];
    this._hyperlipidaemiaMedList = [];
    this._hypertensionMedList = [];
    this._asthmaComorbidMedList = [];
    this._diabComorbidMedList = [];
    this._hyperlipComorbidMedList = [];
    this._hypertensionComorbidMedList = [];
    this._addisComorbidMedList = [];
    this._bipolComorbidMedList = [];
    this._bronchComorbidMedList = [];
    this._cardiacComorbidMedList = [];
    this._cardiomComorbidMedList = [];
    this._copdComorbidMedList = [];
    this._renalComorbidMedList = [];
    this._coronarComorbidMedList = [];
    this._crohnsComorbidMedList = [];
    this._diabinsipComorbidMedList = [];
    this._dysrhytComorbidMedList = [];
    this._epilComorbidMedList = [];
    this._glaucomComorbidMedList = [];
    this._haemophilComorbidMedList = [];
    this._hypothComorbidMedList = [];
    this._sclerosisComorbidMedList = [];
    this._parkinsComorbidMedList = [];
    this._rheumaComorbidMedList = [];
    this._schizoComorbidMedList = [];
    this._lupusComorbidMedList = [];
    this._ulceraComorbidMedList = [];

    // Files
    this._referralLetterFiles = [];
    this._diabPathResultFiles = [];
    this._hyperlipPathResultFiles = [];
    this._asthmaPaedFiles = [];
    this._asthmaFlowVolFiles = [];
    this._medPrescriptionFiles = [];
    this._additionalDocsFiles = [];
  }

  ViewSubmittedApplicationApi(application: ChronicConditionApplicationModel) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/ChronicConditions/ViewSubmittedApplication', application, { headers: headers });
  }

  ViewSubmittedApplication(application: ChronicConditionApplicationModel) {

    this._sumbittedApplication = application;

    this._busy = true;

    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/ChronicConditions/ViewSubmittedApplication', application,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (result: ChronicConditionApplicationModel) => {

        if (result.success) {
          this._sumbittedApplication = result;
          this._error = false;
        }
        else {
          this._sumbittedApplication = null;
          this._error = true;
          this._errorMessage = result.message;
        }

      },
      (error: HttpErrorResponse) => {
        console.log(error)
        this._sumbittedApplication = null;
        this._error = true;
        this._errorMessageHeading = "Unexpected error occured";
        this._errorMessage = error.message;
        this._busy = false;
      },
      () => {
        this._busy = false;
      }
    )
  }

  onSubmitFormData() {

    this._errorArrayArr = [];
    let SelectedConditions = [];

    // to display error messages
    this.memberForm.markAllAsTouched();
    this.providerForm.markAllAsTouched();
    this.biometricForm.markAllAsTouched();
    this.riskFactorsForm.markAllAsTouched();
    this.conditionForm.markAllAsTouched();
    this.requestedMedicationForm.markAllAsTouched();
    this.documentsForm.markAllAsTouched();
    this.comorbidForm.markAllAsTouched();
    this.declarationForm.markAllAsTouched();

    // Always validate these forms
    if (!this.memberForm.valid) {
      this._errorArrayArr.push("Patient");
    }
    //if (this.providerForm.get("provEmail").value.trim() == "") {
    //  // Do this to display error message on control
    //  this.providerForm.get('provEmail').setValue(null);
    //  this.providerForm.get('provEmail').updateValueAndValidity();
    //}
    if (!this.providerForm.valid) {
      this._errorArrayArr.push("Medical Practitioner");
    }
    if (!this.biometricForm.valid) {
      this._errorArrayArr.push("Base Biometric");
    }
    if (!this.riskFactorsForm.valid) {
      this._errorArrayArr.push("Risk Factors");
    }
    if (!this.conditionForm.valid) {
      this._errorArrayArr.push("Condition");
    }

    // Only validate these forms if they are selected
    if (this.conditionForm.get('conditionCode').value == 'DIAB') {

      this.diabetesMellitusForm.markAllAsTouched();

      SelectedConditions.push("DIAB");

      if (!this.diabetesMellitusForm.valid) {
        this._errorArrayArr.push("Diabetes Mellitus");
      }
    }

    if (this.conditionForm.get('conditionCode').value == 'CHOL') {

      this.hyperlipidaemiaForm.markAllAsTouched();

      SelectedConditions.push("CHOL");

      if (!this.hyperlipidaemiaForm.valid) {
        this._errorArrayArr.push("Hyperlipidaemia");
      }
    }

    if (this.conditionForm.get('conditionCode').value == 'HYPER') {

      this.hypertensionForm.markAllAsTouched();

      SelectedConditions.push("HYPER");

      if (!this.hypertensionForm.valid) {
        this._errorArrayArr.push("Hypertension");
      }
    }

    if (this.conditionForm.get('conditionCode').value == 'ASTHMA') {

      this.asthmaForm.markAllAsTouched();

      SelectedConditions.push("ASTHMA");

      if (!this.asthmaForm.valid) {
        this._errorArrayArr.push("Asthma");
      }
    }

    if (!this.requestedMedicationForm.valid) {
      this._errorArrayArr.push("Requested Medications");
    }
    if (!this.documentsForm.valid) {
      this._errorArrayArr.push("Additional Documents");
    }
    if (!this.comorbidForm.valid) {
      this._errorArrayArr.push("Comorbid Conditions");
    }
    if (!this.declarationForm.valid) {
      this._errorArrayArr.push("Declaration");
    }

    if (this._errorArrayArr.length > 0) {
      this._errorArrayModal = true;
      this._errorArrayHeading = "Invalid or missing data";
      this._errorArrayMessage = "The following sections have invalid or missing data:";
    }
    else {
      this.prepareApiData(SelectedConditions);
      //alert("Validation Successful");
    }
  }

  prepareApiData(SelectedConditions: string[]) {

    let formData: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();

    // New application or info Update
    formData.currentPage = this._currentPage;

    // Applicable to all conditions
    formData.conditionApplicationNumber = null;
    formData.membId = this.FamilyList.filter(x => x.dependant == this.memberForm.get('dependant').value)[0].membid;
    formData.membEmail = this.memberForm.get('membEmail').value;;
    formData.provId = this.providerForm.get('provid').value;
    formData.provEmail = this.providerForm.get('provEmail').value;
    formData.referredYN = this.providerForm.get('referredYN').value;
    formData.refProvSpec = this.providerForm.get('refProvSpec').value;
    formData.refProvid = this.providerForm.get('refProvid').value;
    formData.refProvSurname = this.providerForm.get('refProvSurname').value;
    formData.refProvNotes = this.providerForm.get('refProvNotes').value;
    formData.referralLetterFiles = this._referralLetterFiles;
    formData.applicationDate = "";
    //formData.status = "P";

    // Base Biometric
    formData.weight = this.biometricForm.get('weight').value;
    formData.height = this.biometricForm.get('height').value;
    formData.bmi = this.biometricForm.get('bmi').value;
    formData.bloodPresSystolic = this.biometricForm.get('bloodPresSystolic').value;
    formData.bloodPresDiastolic = this.biometricForm.get('bloodPresDiastolic').value;
    formData.cholesterolHDL = this.biometricForm.get('cholesterolHDL').value;
    formData.cholesterolLDL = this.biometricForm.get('cholesterolLDL').value;
    formData.cholesterolTRI = this.biometricForm.get('cholesterolTRI').value;
    formData.cholesterolTotal = this.biometricForm.get('cholesterolTotal').value;
    formData.cholesterolLdlHdlRatio = this.biometricForm.get('cholesterolLdlHdlRatio').value;
    formData.diabetesYN = this.biometricForm.get('diabetesYN').value;
    formData.diabetesType = this.biometricForm.get('diabetesType').value;
    formData.hba1c = this.biometricForm.get('HbA1c').value;
    formData.ogttResult = this.biometricForm.get('ogttResult').value;
    formData.fastingGlucoseResult = this.biometricForm.get('fastingGlucoseResult').value;
    formData.randomGlucoseResult = this.biometricForm.get('randomGlucoseResult').value;
    formData.diabTestDate = this.biometricForm.get('diabTestDate').value;

    // Risk Factors    
    formData.predisposingRiskFactors = null;
    formData.alcohol = this.riskFactorsForm.get('alcoholYN').value;
    formData.obesity = this.riskFactorsForm.get('obesityYN').value;
    formData.smoker = this.riskFactorsForm.get('smokerYN').value;
    formData.smokeInPast = this.riskFactorsForm.get('patientSmokeInPast').value;
    formData.cessationDate = this.riskFactorsForm.get('cessationDate').value;
    formData.preExistingComplications = null;
    formData.ischaemicHeartDisease = this.riskFactorsForm.get('heartDiseaseYN').value;
    formData.peripheralVascularDisease = this.riskFactorsForm.get('vascularDiseaseYN').value;
    formData.strokeAttacks = this.riskFactorsForm.get('strokeYN').value;

    // Condition Applied For
    formData.selectedConditions = SelectedConditions;

    // Diabetes Melutis
    formData.diabetesIcd10 = this.diabetesMellitusForm.get('diabIcd10').value;
    formData.diabPathResultFiles = this._diabPathResultFiles;

    // Hyperlipidemia
    formData.hyperlipidaemiaIcd10 = this.hyperlipidaemiaForm.get('hyperlipIcd10').value;
    formData.hyperlipidaemiatherapyYN = this.hyperlipidaemiaForm.get('hyperlipTherapyYN').value;
    formData.hyperlipidaemiaTherapyDuration = this.hyperlipidaemiaForm.get('therapyDuration').value;
    formData.hyperlipidaemiaMedHist = this._hyperlipidaemiaMedList;
    formData.hyperlipidaemiaAtheroscleroticYN = this.hyperlipidaemiaForm.get('atheroscleroticYN').value;
    formData.hyperlipPathResultFiles = this._hyperlipPathResultFiles;
    formData.hyperlipidaemiaHypertensionTreatmentYN = this.hyperlipidaemiaForm.get('hypertensionTreatmentYN').value;
    formData.hyperlipidaemiaGeneticHyperlipidaemiaYN = this.hyperlipidaemiaForm.get('geneticHyperlipidaemiaYN').value;
    formData.hyperlipidaemiaMaleBloodRelativeYN = this.hyperlipidaemiaForm.get('maleBloodRelativeYN').value;
    formData.hyperlipidaemiaFemaleBloodRelativeYN = this.hyperlipidaemiaForm.get('femaleBloodRelativeYN').value;
    formData.hyperlipidaemiaTendonXanthomaYN = this.hyperlipidaemiaForm.get('tendonXanthomaYN').value;

    // Hypertension
    formData.hypertensionIcd10 = this.hypertensionForm.get('hypertIcd10').value;
    formData.hypertensionSeverity = this.hypertensionForm.get('severity').value;
    formData.hypertensionTherapyYN = this.hypertensionForm.get('hypertTherapyYN').value;
    formData.hypertensionMedHist = this._hypertensionMedList;

    // Asthma
    formData.asthmaIcd10 = this.asthmaForm.get('asthmaIcd10').value;
    formData.asthmaPleakFlow = this.asthmaForm.get('asthmaPleakFlow').value;
    formData.asthmaYounger3YN = this.asthmaForm.get('asthmaYounger3YN').value;
    formData.asthmaPaedFiles = this._asthmaPaedFiles;
    formData.asthmaFlowVolFiles = this._asthmaFlowVolFiles;

    // Requested Medication
    formData.requestedMeds = this._requestedMedList;
    formData.medPrescriptionFiles = this._medPrescriptionFiles;

    // Additional Documents
    formData.additionalDocsFiles = this._additionalDocsFiles;

    // Comorbid
    formData.asthmaCmICD10 = this.comorbidForm.get('asthmaCmICD10').value;
    formData.asthmaCmMedList = this._asthmaComorbidMedList;
    formData.diabCmICD10 = this.comorbidForm.get('diabCmICD10').value;
    formData.diabCmMedList = this._diabComorbidMedList;
    formData.hyperlipCmICD10 = this.comorbidForm.get('hyperlipCmICD10').value;
    formData.hyperlipCmMedList = this._hyperlipComorbidMedList;
    formData.hypertensionCmICD10 = this.comorbidForm.get('hypertensionCmICD10').value;
    formData.hypertensionCmMedList = this._hypertensionComorbidMedList;
    formData.addisCmICD10 = this.comorbidForm.get('addisCmICD10').value;
    formData.addisCmMedList = this._addisComorbidMedList;
    formData.bipolCmICD10 = this.comorbidForm.get('bipolCmICD10').value;
    formData.bipolCmMedList = this._bipolComorbidMedList;
    formData.bronchCmICD10 = this.comorbidForm.get('bronchCmICD10').value;
    formData.bronchCmMedList = this._bronchComorbidMedList;
    formData.cardiacCmICD10 = this.comorbidForm.get('cardiacCmICD10').value;
    formData.cardiacCmMedList = this._cardiacComorbidMedList;
    formData.cardiomCmICD10 = this.comorbidForm.get('cardiomCmICD10').value;
    formData.cardiomCmMedList = this._cardiomComorbidMedList;
    formData.copdCmICD10 = this.comorbidForm.get('copdCmICD10').value;
    formData.copdCmMedList = this._copdComorbidMedList;
    formData.renalCmICD10 = this.comorbidForm.get('renalCmICD10').value;
    formData.renalCmMedList = this._renalComorbidMedList;
    formData.coronarCmICD10 = this.comorbidForm.get('coronarCmICD10').value;
    formData.coronarCmMedList = this._coronarComorbidMedList;
    formData.crohnsCmICD10 = this.comorbidForm.get('crohnsCmICD10').value;
    formData.crohnsCmMedList = this._crohnsComorbidMedList;
    formData.diabinsipCmICD10 = this.comorbidForm.get('diabinsipCmICD10').value;
    formData.diabinsipCmMedList = this._diabinsipComorbidMedList;
    formData.dysrhytCmICD10 = this.comorbidForm.get('dysrhytCmICD10').value;
    formData.dysrhytCmMedList = this._dysrhytComorbidMedList;
    formData.epilCmICD10 = this.comorbidForm.get('epilCmICD10').value;
    formData.epilCmMedList = this._epilComorbidMedList;
    formData.glaucomCmICD10 = this.comorbidForm.get('glaucomCmICD10').value;
    formData.glaucomCmMedList = this._glaucomComorbidMedList;
    formData.haemophilCmICD10 = this.comorbidForm.get('haemophilCmICD10').value;
    formData.haemophilCmMedList = this._haemophilComorbidMedList;
    formData.hypothCmICD10 = this.comorbidForm.get('hypothCmICD10').value;
    formData.hypothCmMedList = this._hypothComorbidMedList;
    formData.sclerosisCmICD10 = this.comorbidForm.get('sclerosisCmICD10').value;
    formData.sclerosisCmMedList = this._sclerosisComorbidMedList;
    formData.parkinsCmICD10 = this.comorbidForm.get('parkinsCmICD10').value;
    formData.parkinsCmMedList = this._parkinsComorbidMedList;
    formData.rheumaCmICD10 = this.comorbidForm.get('rheumaCmICD10').value;
    formData.rheumaCmMedList = this._rheumaComorbidMedList;
    formData.schizoCmICD10 = this.comorbidForm.get('schizoCmICD10').value;
    formData.schizoCmMedList = this._schizoComorbidMedList;
    formData.lupusCmICD10 = this.comorbidForm.get('lupusCmICD10').value;
    formData.lupusCmMedList = this._lupusComorbidMedList;
    formData.ulceraCmICD10 = this.comorbidForm.get('ulceraCmICD10').value;
    formData.ulceraCmMedList = this._ulceraComorbidMedList;

    this.submitToApi(formData); 
  }

  submitToApi(formData: ChronicConditionApplicationModel) {

    this._busy = true;

    this._http.post(this._baseUrl + 'api/ChronicConditions/SubmitApplicationOrUpdate', formData,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._loginService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe(
        (result: ChronicConditionApplicationModel) => {
          if (result.success) {

            this._success = true;
            this._successMessageHeading = "Submission Success";

            if (formData.currentPage == "ChronicCondApp") {

              this._successMessage = "Chronic condition application submitted sucessfully. You will be informed of the approval/rejection via email address ";
              this._successMessageBoldPart = result.provEmail;

            } else if (formData.currentPage == "ChronicCondUpdate") {

              this._successMessage = "Biometric updated sucessfully.";
            }

            //this.clearAll(); jaco
          }
          else {
            this._error = true;
            this._errorMessageHeading = "Submission Failed";
            this._errorMessage = result.message;
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error)
          this._error = true;
          this._errorMessageHeading = "Submission Failed";
          this._errorMessage = error.message;
          this._busy = false;
        },
        () => {
          this._busy = false;
        }
      )
  }

  //SubmitApplication(formData: ChronicConditionApplicationModel) {
  //  let token = localStorage.getItem("jwt");

  //  const headers = new HttpHeaders({
  //    "Authorization": "Bearer " + token,
  //    "Content-Type": "application/json",
  //    "Cache-Control": "no-store",
  //    "Pragma": "no-cache"
  //  });

  //  return this._http.post(this._baseUrl + 'api/ChronicConditions/SubmitApplication', formData, { headers: headers });
  //}

  GetSubmissionHistory(submissionHistory: ChronicConditionApplicationModel) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/ChronicConditions/GetSubmissionHistory', submissionHistory, { headers: headers });
  }

  GetConditionDiags(userName: ChronicConditionDiagsModel) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/ChronicConditions/GetConditionDiags', userName, { headers: headers });
  }

  //GetMedicationDiags(userName: ChronicConditionDiagsModel) {
  //  let token = localStorage.getItem("jwt");

  //  const headers = new HttpHeaders({
  //    "Authorization": "Bearer " + token,
  //    "Content-Type": "application/json",
  //    "Cache-Control": "no-store",
  //    "Pragma": "no-cache"
  //  });

  //  return this._http.post(this._baseUrl + 'api/ChronicConditions/GetMedicationDiags', userName, { headers: headers });
  //}

  closeErrorMessage() {
    this._error = false;
    this._errorMessageHeading = "";
    this._errorMessage = "";
  }

  closeErrorArrayModal() {
    this._errorArrayModal = false;
    this._errorArrayHeading = "";
    this._errorArrayMessage = "";
    this._errorArrayArr = [];
  }

  closeSuccessMessage() {
    this._success = false;
    this._successMessageHeading = "";
    this._successMessage = "";
    this._successMessageBoldPart = "";
  }
}

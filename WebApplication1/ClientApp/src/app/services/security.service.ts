import { Injectable, Inject } from '@angular/core';
import { LoginService } from './login.service';
import { SettingsService } from './settings.service';
import { ClaimHeadersModel } from '../models/ClaimHeadersModel';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { ClaimLineModel } from '../models/ClaimLineModel';
import { ClaimAdjustModel } from '../models/ClaimAdjustModel';
import { DetailLinesModel } from '../models/DetailLinesModel';
import { CheckRunDetailsModel } from '../models/CheckRunDetailsModel';
import { Diagcodes } from '../models/diagcodes';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { MemberNoModel } from '../models/MemberNoModel';
import { RejectClaimDetailModel } from '../models/RejectClaimDetaiModel';
import { ClaimReversalModel } from '../models/ClaimReversalModel';
import { AccountingBureauRemittancesModel } from '../models/AccountingBureauRemittancesModel';
import { ReturnedFileInformationModel } from '../models/ReturnedFileInformationModel';
import { ProviderRemittancesModel } from '../models/ProviderRemittancesModel';
import { ProviderDetailedRemittanceModel } from '../models/ProviderDetailedRemittanceModel';
import { AccountingBureauDetailedRemittancesModel } from '../models/AccountingBureauDetailedRemittancesModel';
import { SpecCodes } from '../models/SpecCodes';
import { Specmasters } from '../models/specmasters';
import { BenTrackCodes } from '../models/ben-track-codes';
import { Faq } from '../models/faq';
import { VendorInfoModel } from '../models/VendorInfoModel';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private _faqList: Faq[] = [];
  private _openFaq: boolean = false;
  private _selectedRemittance: AccountingBureauRemittancesModel = new AccountingBureauRemittancesModel()
  private _claimDetails: ClaimHeadersModel = new ClaimHeadersModel();
  private _invalidProvderidModal: boolean;
  private _hpContract: HP_ContractsModel = new HP_ContractsModel();
  private _listOfrequirements = [];
  private _testClaimcompleteness: boolean;
  private _emptyClaimform: boolean = false
  private _displayClaimtable: boolean = false;
  private _paidTo: string;
  private _validationSpinner: boolean = false;
  private _disableClaimssearch: boolean;
  private _subscribeClaimerror: boolean = false;
  private _busyReverse: boolean = false;
  private _listOfclaims: ClaimHeadersModel[] = [];
  private _historyOfclaims: ClaimHeadersModel[] = [];

  private _familyName: string;
  private _noClaimsfound: boolean = false
  private _contractMessage: string[] = [];
  private _familyNo: string;
  private _modelContractShow: boolean;
  private _showTabels: boolean = true;
  private _selectedClaim: ClaimHeadersModel = new ClaimHeadersModel();
  private _claimReversalinfo: ClaimReversalModel = new ClaimReversalModel();
  private _returnedClaimdetail = [Array, Array]
  private _selectedClaimdetail: ClaimLineModel[] = [];
  private _selectedClaimadjust: ClaimAdjustModel[] = [];
  private _details: CheckRunDetailsModel = new CheckRunDetailsModel();
  private _refProvInfo: DetailLinesModel = new DetailLinesModel();
  private _LoadingModel: boolean = false;
  private _claimInfo: DetailLinesModel[];
  private _vendorInfo: VendorInfoModel = new VendorInfoModel();
  private _diacodes: Diagcodes[] = [];
  private _visitCounts: number;
  private _authNo: string;
  private _AdjustInfo: DetailLinesModel[];
  private _showClaimdetail: boolean;
  private _rejectionNote: string;
  private _rejectModalShow: boolean = false;
  private _unreverseBtn: boolean = false;
  private _rejectionReason: string;
  private _hasRejections: boolean;
  private _rejectionReasons: ClaimAdjustModel[] = []
  private _claimNumber: string;
  private _claimpaidDate: string
  private _downloadClaimclicked: boolean
  private _remittancedownloadmodal: boolean = false;
  private _viewClaimSpinnerModel: boolean = false;
  private _parseddate: any;
  private _filename: string;
  private _filesource: string;
  private _errorRemittance: boolean = false;
  private _selectedProviderremittance: ProviderRemittancesModel = new ProviderRemittancesModel();
  private _providerDetailedremittanceModal: boolean = false;
  private _detailedProviderremittances: ProviderDetailedRemittanceModel[] = [];
  private _detailedProviderremittancesDownload: ProviderDetailedRemittanceModel[] = [];
  private _detailedRemittancemodal: boolean;
  public _detailedremittance: AccountingBureauDetailedRemittancesModel[] = [];
  private _stringamount: any;
  private _specList: SpecCodes[] = [];
  private _busy: boolean = false;
  private _existingBen: Specmasters[] = [];
  private _bencodes: BenTrackCodes[] = [];
  private _selectedList: SpecCodes[] = [];
  private _selectedSpecs: SpecCodes[] = [];
  private _displayGrid: boolean = false;
  private _procerror: boolean = false;

  constructor(private _loginService: LoginService, private _settingsService: SettingsService, private _router: Router,
    private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string) { }

  public get displayGrid(): boolean {
    return this._displayGrid;
  }
  public set displayGrid(value: boolean) {
    this._displayGrid = value;
  }
  public get selectedSpecs(): SpecCodes[] {
    return this._selectedSpecs;
  }
  public set selectedSpecs(value: SpecCodes[]) {
    this._selectedSpecs = value;
  }
  public get bencodes(): BenTrackCodes[] {
    return this._bencodes;
  }
  public set bencodes(value: BenTrackCodes[]) {
    this._bencodes = value;
  }
  public get selectedList(): SpecCodes[] {
    return this._selectedList;
  }
  public set selectedList(value: SpecCodes[]) {
    this._selectedList = value;
  }
  get DetailedProviderRemittance() {
    return this._detailedProviderremittances;
  }
  set DetailedProviderRemittance(detail: ProviderDetailedRemittanceModel[]) {
    this._detailedProviderremittances = detail;
  }
  get DetailRemittanceModal() {
    return this._detailedRemittancemodal
  }
  set DetailRemittanceModal(isValid) {
    this._detailedRemittancemodal = isValid
  }
  get ProviderRemittanceInfo() {
    return this._selectedProviderremittance;
  }
  set ProviderRemittanceInfo(remit: ProviderRemittancesModel) {
    this._selectedProviderremittance = remit;
  }
  get ProviderRemittancDetail() {
    return this._providerDetailedremittanceModal;
  }
  set ProviderRemittancDetail(isValid) {
    this._providerDetailedremittanceModal = isValid;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get existingBen(): Specmasters[] {
    return this._existingBen;
  }
  public set existingBen(value: Specmasters[]) {
    this._existingBen = value;
  }
  get DownloadRemittanceModel() {
    return this._remittancedownloadmodal
  }
  set DownloadRemittanceModel(isValid) {
    this._remittancedownloadmodal = isValid
  }
  get ViewClaimSpinnerModel() {
    return this._viewClaimSpinnerModel
  }
  set ViewClaimSpinnerModel(isValid) {
    this._viewClaimSpinnerModel = isValid
  }
  public get errorRemittance(): boolean {
    return this._errorRemittance;
  }
  public set errorRemittance(value: boolean) {
    this._errorRemittance = value;
  }
  get ClaimModal() {
    return this._claimReversalinfo
  }
  set ClaimModal(val) {
    this._claimReversalinfo = val
  }
  public get rejectionReason(): string {
    return this._rejectionReason;
  }
  public set rejectionReason(value: string) {
    this._rejectionReason = value;
  }
  public get rejectionNote(): string {
    return this._rejectionNote;
  }
  public set rejectionNote(value: string) {
    this._rejectionNote = value;
  }
  get ClaimInfo() {
    return this._showClaimdetail;
  }
  set ClaimInfo(isValid) {
    this._showClaimdetail = isValid;
  }
  get ClaimRequirements() {
    return this._listOfrequirements;
  }
  set ClaimRequirements(isValid) {
    this._listOfrequirements = isValid;
  }
  public get modelContractShow(): boolean {
    return this._modelContractShow;
  }
  public set modelContractShow(value: boolean) {
    this._modelContractShow = value;
  }
  public get showTabels(): boolean {
    return this._showTabels;
  }
  public set showTabels(value: boolean) {
    this._showTabels = value;
  }
  get EmptyClaimForm() {
    return this._emptyClaimform;
  }
  set EmptyClaimForm(isValid) {
    this._emptyClaimform = isValid;
  }
  get NoClaims() {
    return this._noClaimsfound;
  }
  set NoClaims(isValid) {
    this._noClaimsfound = isValid;
  }
  get DisplayClaimTable() {
    return this._displayClaimtable
  }
  set DisplayClaimTable(isValid) {
    this._displayClaimtable = isValid
  }
  get isMemberClaim(): boolean {
    let check: boolean;
    if (this._router.url == "/member-claims") {
      check = true
    } else {
      check = false
    }
    return check;
  }
  public get busyReverse(): boolean {
    return this._busyReverse;
  }
  public set busyReverse(value: boolean) {
    this._busyReverse = value;
  }
  public get contractMessage(): string[] {
    return this._contractMessage;
  }
  public set contractMessage(value: string[]) {
    this._contractMessage = value;
  }
  get ClaimValidationSpinner() {
    return this._validationSpinner
  }
  set ClaimValidationSpinner(isValid) {
    this._validationSpinner = isValid
  }
  get InvalidProvider() {
    return this._invalidProvderidModal
  }
  set InvalidProvider(isValid) {
    this._invalidProvderidModal = isValid;
  }
  get refProviderInfo() {
    return this._refProvInfo;
  }
  public get diacodes(): Diagcodes[] {
    return this._diacodes;
  }
  public set diacodes(value: Diagcodes[]) {
    this._diacodes = value;
  }
  public get LoadingModel(): boolean {
    return this._LoadingModel;
  }
  public set LoadingModel(value: boolean) {
    this._LoadingModel = value;
  }
  get DisableClaimSearchBtn() {
    return this._disableClaimssearch
  }
  set DisableClaimSearchBtn(isValid) {
    this._disableClaimssearch = isValid
  }
  public get familyName(): string {
    return this._familyName;
  }
  public set familyName(value: string) {
    this._familyName = value;
  }
  public get familyNo(): string {
    return this._familyNo;
  }
  public set familyNo(value: string) {
    this._familyNo = value;
  }
  get ClaimError() {
    return this._subscribeClaimerror
  }
  set ClaimError(val) {
    this._subscribeClaimerror = val
  }
  get RecievedClaims() {
    return this._listOfclaims
  }
  get RecievedHistoryClaims() {
    return this._historyOfclaims;
  }
  public get AdjustInfo() {
    return this._AdjustInfo;
  }
  get ClaimHeaders() {
    return this._claimDetails;
  }
  set ClaimHeaders(val: ClaimHeadersModel) {
    this._claimDetails = val
  }
  public get visitCounts(): number {
    return this._visitCounts;
  }
  public set visitCounts(value: number) {
    this._visitCounts = value;
  }
  get HP_Contracts() {
    return this._hpContract;
  }
  set HP_Contracts(val: HP_ContractsModel) {
    this._hpContract = val;
  }
  public get rejectModalShow(): boolean {
    return this._rejectModalShow;
  }
  public set rejectModalShow(value: boolean) {
    this._rejectModalShow = value;
  }
  get SelectedClaim() {
    return this._selectedClaim
  }
  set SelectedClaim(claim: ClaimHeadersModel) {
    this._selectedClaim = claim
  }
  get ShowClaimReversalModal() {
    return this._claimReversalinfo.showClaimModal
  }
  set ShowClaimReversalModal(isValid) {
    this._claimReversalinfo.showClaimModal = isValid
  }
  get ClaimInformation() {
    return this._selectedClaimdetail;
  }
  set ClaimInformation(claim: ClaimLineModel[]) {
    this._selectedClaimdetail = claim;
  }
  get RejectionReasons() {
    return this._rejectionReasons
  }
  set RejectionReasons(claim: ClaimAdjustModel[]) {
    this._rejectionReasons = claim
  }
  get ClaimHasRejections() {
    return this._hasRejections
  }
  set ClaimHasRejections(isValid) {
    this._hasRejections = isValid
  }
  get SpecificClaimDownload() {
    return this._downloadClaimclicked
  }
  set SpecificClaimDownload(isValid) {
    this._downloadClaimclicked = isValid
  }
  get vendorInfo() {
    return this._vendorInfo;
  }
  get SelectedRemittance() {
    return this._selectedRemittance
  }
  get claimInfo() {
    return this._claimInfo;
  }
  public get unreverseBtn(): boolean {
    return this._unreverseBtn;
  }
  public set unreverseBtn(value: boolean) {
    this._unreverseBtn = value;
  }
  public get authNo(): string {
    return this._authNo;
  }
  public set authNo(value: string) {
    this._authNo = value;
  }
  get details() {
    return this._details;
  } 
  get DetailedRemittance() {
    return this._detailedremittance
  }
  public get procerror(): boolean {
    return this._procerror;
  }
  public set procerror(value: boolean) {
    this._procerror = value;
  }
  public get specList(): SpecCodes[] {
    return this._specList;
  }
  public set specList(value: SpecCodes[]) {
    this._specList = value;
  }
  public get faqList(): Faq[] {
    return this._faqList;
  }
  public set faqList(value: Faq[]) {
    this._faqList = value;
  }
  public get openFaq(): boolean {
    return this._openFaq;
  }
  public set openFaq(value: boolean) {
    this._openFaq = value;
  }


  ValidateForm() {
    if (this._loginService.CurrentLoggedInUser.userType == 2) {
      this.CheckProviderID();
      this.BureauValidation();
    }
    if (this._loginService.CurrentLoggedInUser.userType == 1) {
      this.ProviderValidation();
    }
    if (this._loginService.CurrentLoggedInUser.userType == 3) {
      this.CheckAdministratorProviderID();
      this.AdministratorValidation();

    } if (this._loginService.CurrentLoggedInUser.userType == 5) {
      this.BrokerValidation();
    }
  }

  CloseNoClaims() {
    this.ClearAllInputFields();
    this._noClaimsfound = false
  }

  DisableDownloadBtn(x: string): boolean {
    let isDisabled: boolean;

    let inputDate: Date = new Date(x);
        
    if (x == "In Process") {
      isDisabled = true;
    }
    else if (isNaN(inputDate.getTime())) // Check if date is invalid
    {
      isDisabled = true;
    }
    else // Valid date
    {
      //Get delayDays
      let delayDaySetting: number;

      if (this._loginService.CurrentLoggedInUser.userType == 1) //Provider
      {
        delayDaySetting = Math.abs(parseInt(this._settingsService.GetSettingsDesc('payrunlag_prov')));
      }
      else if (this._loginService.CurrentLoggedInUser.userType == 2 || this._loginService.CurrentLoggedInUser.userType == 3 || this._loginService.CurrentLoggedInUser.userType == 5) //Bureau, Client, Broker
      {
        delayDaySetting = Math.abs(parseInt(this._settingsService.GetSettingsDesc('payrunlag')));
      }

      //let currentDate: Date = new Date(); //return datetime object
      //let currentDate = Date.now(); //returns Epoch date (milliseconds from 1 Jan 1970)
      //let testDate = inputDate.setDate(inputDate.getDate() + 3); //returns Epoch date (milliseconds from 1 Jan 1970)

      if (inputDate.setDate(inputDate.getDate() + delayDaySetting) > Date.now())
      {
        isDisabled = true;
      }
      else
      {
        isDisabled = false;
      }
    }

    return isDisabled
  }

  ShowClaimDetailModal(claim: ClaimHeadersModel) {

    this._viewClaimSpinnerModel = true;
    this._selectedClaim = claim

    //let token = localStorage.getItem("jwt"); // Jaco 2023-12-04 not used anymore    
    //this._http.post(this._baseUrl + "api/Security/ClaimInfo", {
    //  ClaimNo: this._selectedClaim.claimNo
    //},
    //  {
    //    headers: new HttpHeaders({
    //      "Authorization": "Bearer " + token,
    //      "Content-Type": "application/json",
    //      "Cache-Control": "no-store",
    //      "Pragma": "no-cache"

    //    })
    //  }).subscribe((ReturnedDetail: any) => {
    //    this._returnedClaimdetail = ReturnedDetail
    //    this._selectedClaimdetail = Object(this._returnedClaimdetail)["item1"]
    //    this._selectedClaimadjust = Object(this._returnedClaimdetail)["item2"]
    //  },
    //    (error) => {
    //      console.log(error);
    //      if (error.status == '401') {
    //        this._router.navigateByUrl('/login');
    //      }
    //      this._viewClaimSpinnerModel = false;
    //    },
    //    () => {          
    //      this.GetDetailsOfRefProv();
    //    })

    let token = localStorage.getItem("jwt"); // Jaco 2023-12-04    
    this._http.post(this._baseUrl + "api/Security/GetVendorInfo", claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (result: VendorInfoModel) => {
        this._vendorInfo = result;        
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {          
          this.GetDetailsOfRefProv();
        })    
  }

  GetDetailsOfRefProv() {

    this._details = new CheckRunDetailsModel();
    this._details.claimNo = this._selectedClaim.claimNo;
    let token = localStorage.getItem("jwt");

    this._http.post(this._baseUrl + 'api/Security/GetDetailsOfRefProv', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      })
      .subscribe((recievedData: DetailLinesModel) => {
        this._refProvInfo = new DetailLinesModel();
        this._refProvInfo.claimNo = recievedData.claimNo;
        this._refProvInfo.refProvId = recievedData.refProvId;
        this._refProvInfo.refProfName = recievedData.refProfName;
        this._refProvInfo.specCode = recievedData.specCode;
        this._refProvInfo.desc = recievedData.desc;
        this._refProvInfo.provid = recievedData.provid;
        this._refProvInfo.provname = recievedData.provname;
        this._refProvInfo.provspec = recievedData.provspec;
        this._refProvInfo.provdesc = recievedData.provdesc
        if (recievedData.claimOriginal === '') {
          this._refProvInfo.claimOriginal = recievedData.claimNo;
        } else {
          this._refProvInfo.claimOriginal = recievedData.claimOriginal;
        }
        if (recievedData.claimreverse === '') {
          this._refProvInfo.claimreverse = 'No Reversal';
        } else {
          this._refProvInfo.claimreverse = recievedData.claimreverse;
        }
        if (recievedData.reversedate === '') {
          this._refProvInfo.reversedate = 'No Reverse Date';
        } else {
          this._refProvInfo.reversedate = recievedData.reversedate;
        }
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {
          this.GetDetailsOfClaim();
        });
  }

  GetDetailsOfClaim() {
    this.LoadingModel = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Security/GetDetailsOfClaim', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel[]) => {        
        this._claimInfo = [];
        let x = recievedData;
        x.forEach((dl) => {
          this._claimInfo.push(dl);
        });
        //console.log(this._claimInfo);
        this._diacodes = [];
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {

          // Jaco 2024-08-27 This only gets the diag codes that are linked to a line, we will have to get all diags from CLAIM_DIAGS in a seperate call below
          //let temparr: Diagcodes[] = []

          //this._claimInfo.forEach((ifo) => {
          //  temparr.push({ code: ifo.diagcode, desc: ifo.diagdesc });
          //});

          //temparr.sort((a, b) => a.code < b.code ? -1 : 1);
          //let temp: Diagcodes = new Diagcodes();
          //temparr.forEach((tmp) => {
          //  if (tmp.code !== temp.code) {
          //    this._diacodes.push(tmp);
          //    temp.code = tmp.code;
          //  }
          //});

          //console.log(this.diacodes);
          this.GetClaimDiags();
        });
  }

  GetClaimDiags() {

    let token = localStorage.getItem("jwt");
    const params = new HttpParams().append('claimNo', this._details.claimNo);
    this._http.post(this._baseUrl + 'api/Security/GetClaimDiags', this._details.claimNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        }),
        params
      })
      .subscribe((recievedData: Diagcodes[]) => {
        let temparr: Diagcodes[] = [];

        recievedData.forEach((ifo) => {
          temparr.push(ifo);
        });

        temparr.sort((a, b) => a.code < b.code ? -1 : 1);
        let temp: Diagcodes = new Diagcodes();
        temparr.forEach((tmp) => {
          if (tmp.code !== temp.code) {
            this._diacodes.push(tmp);
            temp.code = tmp.code;
          }
        });        
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {
          this.GetAdjustOfClaim();
        });
  }

  GetAdjustOfClaim() {
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Security/GetAdjustOfClaim', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel[]) => {       
        this._AdjustInfo = [];
        let x = recievedData;
        let y: boolean = false;
        x.forEach((dl) => {
          this._AdjustInfo.forEach((ad) => {
            if (dl.adjCode2 === ad.adjCode2) {
              y = true;
            }
          });
          if (y === true) {
            y = false;
          } else {
            this._AdjustInfo.push(dl);
            y = false;
          }
        });
        //console.log(this._claimInfo);
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {

          this.GetVisitCounts();
        });
  }

  GetVisitCounts() {
    let token = localStorage.getItem("jwt")
    let memb: MemberNoModel = new MemberNoModel();
    memb.hpcode = this._loginService.CurrentLoggedInUser.username;
    memb.memberNo = this._selectedClaim.memberNo;
    if (this._loginService.CurrentLoggedInUser.userType !== 3) {
      memb.usertype = this._loginService.CurrentLoggedInUser.userType.toString();
    }
    this._http.post(this._baseUrl + 'api/Security/GetVisitClaims', memb,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      })
      .subscribe((recievedCount: MemberNoModel) => {
        let x: string = recievedCount.memberNo
        this.visitCounts = parseInt(x);
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {
          this.GetAuthNo();
        });

  }

  GetAuthNo() {
    let token = localStorage.getItem("jwt");
    let x: ValidationResultsModel = new ValidationResultsModel();
    x.message = this.details.claimNo;
    this._http.post(this._baseUrl + 'api/Security/GetAuthNo', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      })
      .subscribe((recievedData: ValidationResultsModel) => {
        this.authNo = recievedData.message;
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
          this._viewClaimSpinnerModel = false;
        },
        () => {
          this._showClaimdetail = true;
          this.LoadingModel = false;
          this._viewClaimSpinnerModel = false;
        });
  }

  DisableReversal(paid: string, negativeNumcheck: string, reverseDate: string, datePaid: string): boolean {
    let isDisabled: boolean
    if (negativeNumcheck.includes("-") || reverseDate != null || datePaid == 'In Process' || paid == '0.00' || paid == '0,00') {
      isDisabled = true
    } else {
      isDisabled = false
    }
    return isDisabled
  }

  ClearAllInputFields() {
    this.HP_Contracts.hpcode = "000";
    this._claimDetails.memberNo = "";
    this._claimDetails.memberId = "";
    this._claimDetails.providerId = "";
    this._claimDetails.memberDOB = null;
    this._claimDetails.serviceDate = null;
    this._claimDetails.datePaid = null;
  }

  CheckAdministratorProviderID() {
    this._paidTo = "P";
    this._loginService.CurrentLoggedInUser.provID = ""
    if (this._loginService.CurrentLoggedInUser.provID == "") {
      this._loginService.CurrentLoggedInUser.provID = this._claimDetails.providerId
    }
  }

  CheckProviderID() {
    let filterprovID = this._loginService.ProvidersLinkedToBureau.filter(x => x.vendorid === this._claimDetails.providerId)
    if (filterprovID.length != 0) {
      this._loginService.CurrentLoggedInUser.provID = "";
      filterprovID.forEach(el => {
        this._loginService.CurrentLoggedInUser.provID = el.vendorid
        this._invalidProvderidModal = false
      });
    } else {
      this._loginService.CurrentLoggedInUser.provID = this._claimDetails.providerId
      if (filterprovID.length == 0 && this._claimDetails.providerId.length > 0) {
        this._invalidProvderidModal = true
      }
    }
  }

  BureauValidation() {
    let tempArray = []
    let testBoolean: boolean;
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('ViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('ViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this.HP_Contracts.hpcode != "000") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberNumber');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter a Member Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberIDNumber');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter The Member's ID Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Member's Date Of Birth")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Claim Service Date")
        }
      }
    }
    if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
      testBoolean = true
    } else {
      tempArray.push("Enter a Practice Number")
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberPaidDate');


    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = tempArray;
    this._testClaimcompleteness = testBoolean;

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderidModal == true) {
      this._emptyClaimform = false;
      this._displayClaimtable = false;
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true;
        this._displayClaimtable = false;
      } else {
        this.SearchClaim();
      }
    }

  }

  SearchClaim() {
    this._validationSpinner = true
    this._disableClaimssearch = true;

    let token = localStorage.getItem('jwt')
    this._http.post(this._baseUrl + "api/Security/ProviderClaimSearch", {

      Plan: this.HP_Contracts.hpcode,
      MemberNo: this._claimDetails.memberNo,
      ServiceDate: this._claimDetails.serviceDate,
      DatePaid: this._claimDetails.datePaid,
      Practice: this._loginService.CurrentLoggedInUser.provID,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      userName: this._loginService.CurrentLoggedInUser.username,
      lobCode: this._loginService.CurrentLoggedInUser.lobcode,
      paidTo: this._paidTo,
      isMember: this.isMemberClaim,
      brokerId: this._loginService.CurrentLoggedInUser.brokerId,
      contracted: this._loginService.CurrentLoggedInUser.contract
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedclaimdetail: ClaimHeadersModel[]) => {

        this._listOfclaims = returnedclaimdetail


      }, (error) => {
        this._validationSpinner = false;
        this._disableClaimssearch = false;
        this._subscribeClaimerror = true;
      },
        () => {
          this._busyReverse = false;
          this.GetFamilyNameSearch();
        });
  }

  SearchClaimHistory() {
    this._validationSpinner = true
    this._disableClaimssearch = true;

    let token = localStorage.getItem('jwt')
    this._http.post(this._baseUrl + "api/Security/ProviderClaimHistorySearch", {

      Plan: this.HP_Contracts.hpcode,
      MemberNo: this._claimDetails.memberNo,
      ServiceDate: this._claimDetails.serviceDate,
      DatePaid: this._claimDetails.datePaid,
      Practice: this._loginService.CurrentLoggedInUser.provID,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      userName: this._loginService.CurrentLoggedInUser.username,
      lobCode: this._loginService.CurrentLoggedInUser.lobcode,
      paidTo: this._paidTo,
      isMember: this.isMemberClaim,
      brokerId: this._loginService.CurrentLoggedInUser.brokerId,
      contracted: this._loginService.CurrentLoggedInUser.contract
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedclaimdetail: ClaimHeadersModel[]) => {

        this._historyOfclaims = returnedclaimdetail
        if (this._historyOfclaims.length > 0) {
          if (this._loginService.CurrentLoggedInUser.userType == 1) {
            if (this._loginService.CurrentLoggedInUser.hospital == false) {
              this.CheckAllowedOptions();
            }
          }
          this._displayClaimtable = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
        } else {
          this._displayClaimtable = false;
          this._noClaimsfound = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
        }

      }, (error) => {
        this._validationSpinner = false;
        this._disableClaimssearch = false;
        this._subscribeClaimerror = true;
      },
        () => {
          this._busyReverse = false;

         // this.GetFamilyNameSearch();
        });
  }

  GetFamilyNameSearch() {
    let token = localStorage.getItem("jwt");

    this._http.post(this._baseUrl + 'api/Security/ProviderClaimSearchFamilyName', {

      Plan: this.HP_Contracts.hpcode,
      MemberNo: this._claimDetails.memberNo,
      ServiceDate: this._claimDetails.serviceDate,
      DatePaid: this._claimDetails.datePaid,
      Practice: this._loginService.CurrentLoggedInUser.provID,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      userName: this._loginService.CurrentLoggedInUser.username,
      lobCode: this._loginService.CurrentLoggedInUser.lobcode,
      paidTo: this._paidTo,
      isMember: this.isMemberClaim,
      brokerId: this._loginService.CurrentLoggedInUser.brokerId
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedFamily: ClaimHeadersModel) => {


        if (this._listOfclaims.length > 0) {
          if (this._loginService.CurrentLoggedInUser.userType == 1) {
            if (this._loginService.CurrentLoggedInUser.hospital == false) {
              this.CheckAllowedOptions();
            }
          }
          this._displayClaimtable = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
          this._familyName = "";
          this._familyName = "Main Policy Holder: " + returnedFamily.memberFirstName + " " + returnedFamily.memberLastName;
        } else {
          this._displayClaimtable = false;
          this._noClaimsfound = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
          this._familyName = "";
        }

      },
        (error) => {
          this._validationSpinner = false;
          this._disableClaimssearch = false;
          this._subscribeClaimerror = true;
        },
        () => {
          let c: string = this._claimDetails.memberNo;
          let y: boolean = c.includes('-');
          if (y) {
            let x: number = c.lastIndexOf('-');
            let v: number = c.length - (x + 1);
            if (v == 2) {
              let z = c.substring(0, x);
              this._familyNo = z;
            } else {
              this._familyNo = this._claimDetails.memberNo;
            }

          } else {
            this._familyNo = this._claimDetails.memberNo;
          }

        });
  }

  CheckAllowedOptions() {
    this._contractMessage = [];
    let list: string[] = [];
    let opt: string[] = [];
    this._settingsService.Settings.forEach((el) => {
      if (this._loginService.isDentist) {
        if (el.settingsId == 'ContractBoundOptionsDentist') {
          opt = el.description.split(',');
        }
      }
      if (this._loginService.isSpecialist) {
        if (el.settingsId == 'ContractBoundOptionsGP') {
          opt = el.description.split(',');
        }
      }
    });
    let p = this;
    if (this._loginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED') {
      p._listOfclaims.forEach((el) => {
        opt.forEach((op) => {
          // console.log(el);
          if (op == el.opt) {
            let found: boolean = false;
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                if (list[i].toUpperCase() == el.memberNo.toUpperCase()) {
                  found = true;
                  break;
                }
              }
            } else {
              list.push(el.memberNo);
              found = true;
            }

            if (found == false) {
              list.push(el.memberNo);

            }
          }
        });
      });
    }
    let d: string = "";

    this._contractMessage.push("The Following Member/s is bound to a network option:");
    list.forEach((el) => {
      this._contractMessage.push("• " + el);
    });
    this._contractMessage.push("and you are a non network provider, therefore you can not view the claims.")
    if ((list.length > 0) && (this._loginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED')) {
      this._modelContractShow = true;
      this._showTabels = this._settingsService.GetSettingEnabled('NetworkBoundShow');
    } else {
      this._showTabels = true;
    }

    if (this._loginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED') {
      this._settingsService.expSettings.forEach((el) => {
        p._listOfclaims.forEach((e) => {
          if (e.opt.toUpperCase() == el.opt) {
            let c: string[] = [];
            c = el.specs.split(',');
            c.forEach((l) => {
              if (l.trim() == this._loginService.CurrentLoggedInUser.specCode) {
                if (this._showTabels == false) {
                  this._showTabels = true;
                }

                this._modelContractShow = false;
              }
            })
          }
        });
      });
    }
  }

  ProviderValidation() {
    let temparray = []
    let TestBoolean: boolean
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaiMemberNumber');
    required = this._settingsService.GetSettingRequired('ProviderViewClaiMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter a Member Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this._hpContract.hpcode != "000") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderviewClaimmemberIDNumber');
    required = this._settingsService.GetSettingRequired('ProviderviewClaimmemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter The Member's ID Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Member's Date Of Birth")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Service Date")
        }
      }
    }
    if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
      TestBoolean = true
    } else {
      temparray.push("Enter a Practice Number")
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberPaidDate');

    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = temparray
    this._testClaimcompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderidModal == true) {
      this._emptyClaimform = false
      this._displayClaimtable = false
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true
        this._displayClaimtable = false
      } else {
        this.SearchClaim()
      }
    }
  }

  AdministratorValidation() {
    let temparray = []
    let TestBoolean: boolean
    let enabled: boolean;
    let required: boolean;



    if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
      TestBoolean = true
    } else {
      temparray.push("Enter a Practice Number")
    }
    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberNumber');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter a Member Number")
        }
      }
    }
    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this._hpContract.hpcode != "000") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorviewClaimmemberIDNumber');
    required = this._settingsService.GetSettingRequired('AdministratorviewClaimmemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter The Member's ID Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Member's Date Of Birth")
        }
      }
    }
    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Service Date")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberPaidDate');

    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = temparray
    this._testClaimcompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderidModal == true) {
      this._emptyClaimform = false
      this.DisplayClaimTable = false
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true
        this.DisplayClaimTable = false
      } else {
        this.SearchClaim()
      }
    }
  }

  BrokerValidation() {
    let temparray = []
    let TestBoolean: boolean = true;
    let required: boolean;

    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberNumber');
    if (required == true) {
      if (this._claimDetails.memberNo == "") {
        temparray.push("Enter a Member Number");
        TestBoolean = false;
      }
    }

    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberNumberID');
    if (required == true) {
      if (this._claimDetails.memberId == "") {
        temparray.push("Enter The Member's ID Number")
        TestBoolean = false;
      }
    }

    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberDOB');
    if (required == true) {
      if (this._claimDetails.memberDOB == null) {
        temparray.push("Enter the Member's Date Of Birth")
        TestBoolean = false;
      }

    }

    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberServicedate');
    if (required == true) {
      if (this._claimDetails.serviceDate == "") {
        temparray.push("Enter the Claim Service Date")
        TestBoolean = false;
      }
    }

    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberPaiddate');

    if (required == true) {
      if (this._claimDetails.datePaid == "") {
        temparray.push("Enter the Claim Payment Date")
        TestBoolean = false;
      }
    }


    required = this._settingsService.GetSettingRequired('BrokerViewClaimMemberHealthPlan');
    if (required == true) {
      if (this._claimDetails.plan == "") {
        temparray.push("Select a Health plan")
        TestBoolean = false;
      }
    }

    this._listOfrequirements = temparray
    this._testClaimcompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderidModal == true) {
      this._emptyClaimform = false
      this.DisplayClaimTable = false
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true
        this.DisplayClaimTable = false
      } else {
        this.SearchClaim()
      }
    }
  }

  RejectClaim(claimToBeRejected: RejectClaimDetailModel) {

    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Security/RejectClaim', claimToBeRejected,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData) => {

    },
      (error) => {

        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this._rejectModalShow = false;
        this.RefreshTable(claimToBeRejected);
        this.GetDetailsOfClaim();
      });
  }

  RefreshTable(claimToBeRejected: RejectClaimDetailModel) {

    let token = localStorage.getItem("jwt");

    this._http.post(this._baseUrl + 'api/Security/RefreshTabel', claimToBeRejected,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {



    },
      (error) => {
        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this.ValidateForm();
      });
  }

  VerifyRejection(claimToVerify: RejectClaimDetailModel) {

    let token = localStorage.getItem("jwt")

    this._http.post(this._baseUrl + 'api/Security/VerifyRejection', claimToVerify,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: ValidationResultsModel) => {

      if (recievedData.valid === true) {
        this._unreverseBtn = true;
      } else {
        this._unreverseBtn = false;
      }

    },
      (error) => {
        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => { this._rejectModalShow = true; });
  }

  CheckExistOnWebReverse(x: RejectClaimDetailModel) {
    let token = localStorage.getItem("jwt")
    let y: boolean = false;
    this.LoadingModel = true;
    this._http.post(this._baseUrl + 'api/Security/ConfirmReversal', x, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ValidationResultsModel) => {
      y = results.valid;
    },
      (error) => {
        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this.LoadingModel = false;
        return y
      }
    )
  }

  ClaimReversalModel(claim: ClaimHeadersModel) {
    this._claimReversalinfo.claimNumber = claim.claimNo;
    this._claimReversalinfo.memberNumber = claim.memberNo;
    this._claimReversalinfo.healthPlan = claim.plan;
    this._claimReversalinfo.serviceDate = claim.serviceDate;
    this._claimReversalinfo.recievedDate = claim.receivedDate;
    this._claimReversalinfo.chargedAmount = claim.charged;
    this._claimReversalinfo.paidDate = claim.datePaid;
    let amountTobeReplaced = this._claimReversalinfo.paidDate.match(/-/gi).length;
    for (var i = 0; i < amountTobeReplaced; i++) {
      this._claimReversalinfo.paidDate = this._claimReversalinfo.paidDate.replace("-", "/")
    }
    this._claimReversalinfo.paidAmount = claim.paid;
    this._claimReversalinfo.showClaimModal = true;
  }

  CloseClaimReversalModal() {
    this._claimReversalinfo.showClaimModal = false
  }

  ReverseClaim() {
    let token = localStorage.getItem("jwt");
    this._busyReverse = true;
    this._http.post(this._baseUrl + "api/Security/ClaimReversal", {
      claimNumber: this._claimReversalinfo.claimNumber
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json"

        })
      }).subscribe((claimReversal: ClaimReversalModel[]) => {
        this._claimReversalinfo.showClaimModal = false;
        this.SearchClaim()
      })
  }

  CloseClaimDetail() {
    this._showClaimdetail = false
    this._rejectionReasons = [];
    this._hasRejections = false;
  }

  ShowRejectionReason(selectedClaim: number) {
    let RjectionOfClaim = this._selectedClaimadjust.filter(x => selectedClaim.toString() == x.claimtblrow)
    this._rejectionReasons = RjectionOfClaim
    this._hasRejections = true

  }

  ClaimDownloadModel(selectedclaimnumber: string, paiddate: string) {
    this._claimNumber = selectedclaimnumber;
    this._claimpaidDate = paiddate;
    this._downloadClaimclicked = true;
  }

  DownloadClaimRemit(file: boolean) {
    this._selectedRemittance.file = file;
    this._downloadClaimclicked = false;
    this._remittancedownloadmodal = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + "api/Security/DownloadClaimRemitance", {
      Username: this._loginService.CurrentLoggedInUser.username,
      File: this._selectedRemittance.file,
      ClaimNumber: this._claimNumber,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      PaidDate: this._claimpaidDate

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }, ).subscribe(
        (specificremittance: ReturnedFileInformationModel) => {

          this._parseddate = specificremittance.parsedDate
          this._filename = specificremittance.fileName;

          //this._filesource = './../../PamcPortal/assets/Remittances/' + this._parseddate + '/' + this._filename;
          //this._filesource = './../../../assets/Remittances/' + this._parseddate + '/' + this._filename;

          var a = document.createElement('a');
          a.target = '_blank';
          a.setAttribute('type', 'hidden');
          //a.href = this._filesource
          a.href = 'assets/Remittances/' + this._parseddate + '/' + this._filename;
          a.download = this._filename;
          document.body.appendChild(a);
          a.click();
          a.remove();

          this._remittancedownloadmodal = false
        },
        (error) => {
          console.log(error)
          this._remittancedownloadmodal = false;
          this._errorRemittance = true;
        })

  }

  ProviderRemittanceModal(paidDate: Date, net: any, provId: string, prefix: number) {
    this._selectedProviderremittance.datepaid = paidDate;
    this._selectedProviderremittance.net = net.toLocaleString('en-US', { minimumFractionDigits: 2 });
    this._selectedProviderremittance.provid = provId
    this._selectedProviderremittance.chprefix = prefix
    this.DetailedProviderRemittancInformation();
    this._providerDetailedremittanceModal = true;


  }

  ProviderRemittanceDownload(paidDate: Date, net: any, provId: string, prefix: number, file: boolean) {
    this._selectedProviderremittance.datepaid = paidDate;
    this._selectedProviderremittance.net = net.toLocaleString('en-US', { minimumFractionDigits: 2 });
    this._selectedProviderremittance.provid = provId
    this._selectedProviderremittance.chprefix = prefix
    this.DetailedProviderRemittancInformationToDownload(provId, paidDate,file);



  }


  DetailedProviderRemittancInformation() {

    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + "api/Security/DetailedProviderRemittances", {
      provID: this._selectedProviderremittance.provid,
      transactionDate: this._selectedProviderremittance.datepaid,
      chprefix: this._selectedProviderremittance.chprefix,
      loginProvId: this._loginService.CurrentLoggedInUser.provID

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returneddetailremittance: ProviderDetailedRemittanceModel[]) => {
        this._detailedProviderremittances = returneddetailremittance
      });
  }

  DetailedProviderRemittancInformationToDownload(pracnum: string, paiddate: Date, file: boolean) {

    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + "api/Security/DetailedProviderRemittances", {
      ProvID: this._loginService.CurrentLoggedInUser.provID,
      TransactionDate: this._selectedProviderremittance.datepaid,
      CHPREFIX: this._selectedProviderremittance.chprefix

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returneddetailremittance: ProviderDetailedRemittanceModel[]) => {
        this._detailedProviderremittancesDownload = returneddetailremittance
      },
        (error) => {
          console.log(error);
        },
        () => {
          this.ProviderDownloadRemittanceClaim(pracnum, paiddate, file);
        });
  }

  OpenRemittanceModal(paidDate: Date, Totalamount: any, Totalpaymentsmade: number) {
    this._detailedRemittancemodal = true;
    this._selectedRemittance.datepaid = paidDate;
    this._selectedRemittance.amount = Totalamount.toLocaleString('en-US', { minimumFractionDigits: 2 });
    this._selectedRemittance.vendorid = Totalpaymentsmade
    this.DetailedRemittancInformation();

  }
  DetailedRemittancInformation() {

    let token = localStorage.getItem("jwt");

    this._http.post(this._baseUrl + "api/Security/DetailedRemittances", {
      Username: this._loginService.CurrentLoggedInUser.username,
      TransactionDate: this._selectedRemittance.datepaid,
      bureauId: this._loginService.CurrentLoggedInUser.bureauId

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returneddetailremittance: AccountingBureauDetailedRemittancesModel[]) => {
        this._detailedremittance = returneddetailremittance
        this._detailedremittance.forEach(el => {
          this._stringamount = el.amount.toLocaleString('en-US', { minimumFractionDigits: 2 })
        });
      });
  }
  ProviderDownloadRemittance(prefix: number,pracnum: string, paiddate: Date, file: boolean) {
    this._remittancedownloadmodal = true;
    this._selectedProviderremittance.provid = pracnum
    this._selectedProviderremittance.datepaid = paiddate
    this._selectedProviderremittance.file = file
    this._selectedProviderremittance.chprefix = prefix

    let token = localStorage.getItem("jwt")

    this._http.post(this._baseUrl + "api/Security/DownloadProviderRemitance", {
      File: this._selectedProviderremittance.file,
      PaidDate: this._selectedProviderremittance.datepaid,
      Username: this._loginService.CurrentLoggedInUser.username,
      ProvID: this._selectedProviderremittance.provid,
      BureauID: this._loginService.CurrentLoggedInUser.bureauId,
      prefix: this._selectedProviderremittance.chprefix,
      loginProvId: this._loginService.CurrentLoggedInUser.provID

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((downloadremittancestatus: ReturnedFileInformationModel) => {
        //   console.log("Returned Information:", downloadremittancestatus)
        this._parseddate = downloadremittancestatus.parsedDate
        //   console.log("Folder:", this._parseddate)
        this._filename = downloadremittancestatus.fileName
        //  console.log("File:", this._filename)
        //this._filesource = '../../PamcPortal/assets/Remittances/' + this._parseddate + '/' + this._filename
        this._filesource = 'assets/Remittances/' + this._parseddate + '/' + this._filename
        //console.log('ProviderDownloadRemittance');
        //console.log(this._filesource);


        var a = document.createElement('a');
        a.target = '_blank';
        a.setAttribute('type', 'hidden');
        a.href = this._filesource;
        a.download = this._filename;
        document.body.appendChild(a);
        a.click();
        a.remove();
        //console.log(a)

        //console.log(this._filesource);
        //console.log(this._filename);
        //console.log(this._parseddate);

        this._remittancedownloadmodal = false
      },
        (error) => {
          console.log(error)
          this._remittancedownloadmodal = false;
          this._errorRemittance = true;
        });
  }

  ProviderDownloadRemittanceClaim(pracnum: string, paiddate: Date, file: boolean) {

    this._detailedProviderremittancesDownload.forEach((el) => {
 
      this._remittancedownloadmodal = true;
      this._selectedProviderremittance.provid = pracnum
      this._selectedProviderremittance.datepaid = paiddate
      this._selectedProviderremittance.file = file
      this._selectedProviderremittance.claim = el.claimno;
      let token = localStorage.getItem("jwt")

      this._http.post(this._baseUrl + "api/Security/DownloadProviderRemitance", {
        File: this._selectedProviderremittance.file,
        PaidDate: this._selectedProviderremittance.datepaid,
        Username: this._loginService.CurrentLoggedInUser.username,
        ProvID: this._selectedProviderremittance.provid,
        BureauID: this._loginService.CurrentLoggedInUser.bureauId,
        claim: this._selectedProviderremittance.claim


      },
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }).subscribe((downloadremittancestatus: ReturnedFileInformationModel) => {
          //   console.log("Returned Information:", downloadremittancestatus)
          this._parseddate = downloadremittancestatus.parsedDate
          //   console.log("Folder:", this._parseddate)
          this._filename = downloadremittancestatus.fileName
          //  console.log("File:", this._filename)
          //this._filesource = '../../PamcPortal/assets/Remittances/' + this._parseddate + '/' + this._filename
          this._filesource = 'assets/Remittances/' + this._parseddate + '/' + this._filename
          //console.log('ProviderDownloadRemittanceClaim');
          //console.log(this._filesource);

          var a = document.createElement('a');
          a.target = '_blank';
          a.setAttribute('type', 'hidden');
          a.href = this._filesource
          a.download = this._filename;
          document.body.appendChild(a);
          a.click();
          a.remove();
          //console.log(a)


          this._remittancedownloadmodal = false


        },
          (error) => {
            console.log(error)
            this._remittancedownloadmodal = false;
            this._errorRemittance = true;
          });
    });



  }


  DownloadRemittance(paiddate: Date, file: boolean) {

    let token = localStorage.getItem("jwt");
    this._remittancedownloadmodal = true;
    this._selectedRemittance.datepaid = paiddate
    this._selectedRemittance.file = file
    this._http.post(this._baseUrl + "api/Security/DownloadRemitance", {
      PaidDate: this._selectedRemittance.datepaid,
      Username: this._loginService.CurrentLoggedInUser.username,
      BureauID: parseInt( this._loginService.CurrentLoggedInUser.bureauId),
      File: this._selectedRemittance.file

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((downloadremittancestatus: ReturnedFileInformationModel) => {
        this._parseddate = downloadremittancestatus.parsedDate;
        this._filename = downloadremittancestatus.fileName;

        //this._filesource = './../../PamcPortal/assets/Remittances/' + this._parseddate + '/' + this._filename
        this._filesource = 'assets/Remittances/' + this._parseddate + '/' + this._filename;
        //console.log('DownloadRemittance');
        //console.log(this._filesource);
      
        var a = document.createElement('a');
        a.target = '_blank';
        a.setAttribute('type', 'hidden');
        a.href = this._filesource
        a.download = this._filename;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._remittancedownloadmodal = false


      },
        (error) => {
          this._remittancedownloadmodal = false;
          this._errorRemittance = true;
          console.log(error);
        });
  }

  GetSpecCodes() {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Benefit/GetSPecCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: SpecCodes[]) => {
        this.specList = [];
        result.forEach((el) => {
          this.specList.push(el);
        });
      },
        (error) => { console.log(error) },
        () => {
          this._busy = false;
          this.GetBenTrackCodes();
        })
  }

  SubmitBenSpec(item: Specmasters) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceMasterSubmit', item,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: Specmasters) => {
        this._specList.forEach((el) => {
          if (el.selected == true) {
            el.id = result.id;
            el.phcode = item.phcode;

            el.user = this._loginService.CurrentLoggedInUser.username;
          } else {
            el.id = 0;
          }
        })
      },
        (error) => { console.log(error) },
        () => {
          this.SubmitBenSpecList();
          this._busy = false;
        });
  }

  UpdateBenSpec(item: Specmasters) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceMasterUpdate', item,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: Specmasters) => {
        this._specList.forEach((el) => {
          el.id = 0;
          if (el.selected == true) {
            el.id = item.id;
            el.user = this._loginService.CurrentLoggedInUser.username;
            el.phcode = item.phcode;
          }
        })
      },
        (error) => { console.log(error) },
        () => {
          this.UpdateBenSpecList();
          this._busy = false;
        });
  }

  DeleteBenSpec(item: Specmasters) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceMasterDelete', item,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: Specmasters) => {
      },
        (error) => { console.log(error) },
        () => {
          this.DeleteBenSpecList(item);
          this.GetExistingBenefits();
          this._busy = false;
        });
  }

  DeleteBenSpecList(item: Specmasters) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceDetailDelete', item,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: ValidationResultsModel) => {

      },
        (error) => { console.log(error) },
        () => {
          this._busy = false;
          this.GetExistingBenefits();
        });
  }

  UpdateBenSpecList() {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceDetailUpdate', this.specList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: ValidationResultsModel) => {

      },
        (error) => { console.log(error) },
        () => {
          this._busy = false;
          this.GetExistingBenefits();
        });
  }

  SubmitBenSpecList() {
    let token = localStorage.getItem("jwt");
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Benefit/InsuranceDetailSubmit', this.specList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: ValidationResultsModel) => {

      },
        (error) => { console.log(error) },
        () => {
          this._busy = false;
          this.GetExistingBenefits();
        });
  }

  GetExistingBenefits() {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/GetExistingBef', this._loginService.CurrentLoggedInUser,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Specmasters[]) => {
      this.existingBen = [];
      results.forEach((el) => {
        this.existingBen.push(el);
      })
    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
      })
  }

  GetBenTrackCodes() {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this.bencodes = [];
    this._http.get(this._baseUrl + 'api/Benefit/GetBenTypeCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: BenTrackCodes[]) => {
        results.forEach((el) => {
          this.bencodes.push(el);
        })
      },
        (error) => {
          console.log(error)
        },
        () => { this._busy = false; })
  }

  GetDetailsBen(data: Specmasters) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Security/GetDetailBen', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: SpecCodes[]) => {
      this.selectedList = [];
      this.selectedSpecs = [];
      results.forEach((el) => {
        if (el.code.length == 1) {
          el.code = "0" + el.code;
        }
        //  console.log(this._specList);
        this._specList.forEach((e) => {
          if (el.code == e.code) {
            e.selected = true;
            e.benCode = el.benCode;
            e.procCode = el.procCode;
            this.selectedList.push(e);
            this.selectedSpecs.push(e);
          }
          if ((el.code == '14') && (e.code == '15')) {
            e.selected = true;
            // this.selectedList.push(e);
            //this.selectedSpecs.push(e);
          }
          if ((el.code == '15') && (e.code == '14')) {
            e.selected = true;
            // this.selectedList.push(e);
            // this.selectedSpecs.push(e);
          }
        })
      })
    },
      (error) => { console.log(error); },
      () => { this._busy = false; })
  }


  ValidateProcCode(data: SpecCodes) {
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Benefit/ValidateProcCode',
      data, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((result: ValidationResultsModel) => {
      this._procerror = result.valid;
    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
      });
  }

  GetFaqs(data: Faq) {

    this.busy = true;

    this._http.post(this._baseUrl + 'api/Security/GetFaqs', data
    ).subscribe((result: Faq[]) => {
      this.faqList = [];
      this.faqList = result;
    },
      (error) => {
        console.log(error);
      },
      () => {
        this.busy = false;
        this.openFaq = true;

      });

  }

}

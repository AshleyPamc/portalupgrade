import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SecurityService } from './security.service';
import { SettingsService } from './settings.service';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { ClaimHeadersModel } from '../models/ClaimHeadersModel';
import { ClaimLineModel } from '../models/ClaimLineModel';
import { ClaimAdjustModel } from '../models/ClaimAdjustModel';
import { ReturnedFileInformationModel } from '../models/ReturnedFileInformationModel';
import { ClaimReversalModel } from '../models/ClaimReversalModel';
import { DetailLinesModel } from '../models/DetailLinesModel';
import { CheckRunDetailsModel } from '../models/CheckRunDetailsModel';
import { MemberNoModel } from '../models/MemberNoModel';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { RejectClaimDetailModel } from '../models/RejectClaimDetaiModel';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class MemberClaimService {
  private _hpContract: HP_ContractsModel = new HP_ContractsModel();
  private _claimDetails: ClaimHeadersModel = new ClaimHeadersModel();
  private _listOfrequirements = [];
  private _testClaimcompleteness: boolean;
  private _invalidProvderIdmodal: boolean;
  private _emptyClaimform: boolean = false;
  private _displayClaimtable: boolean = false;
  private _validationSpinner: boolean;
  private _disableClaimssearch: boolean;
  private _paidTo: string;
  private _listOfclaims: ClaimHeadersModel[] = [];
  private _noClaimsfound: boolean = false;
  private _claimNumber: string;
  private _claimpaidDate: string;
  private _downloadClaimclicked: boolean;
  private _selectedClaim: ClaimHeadersModel = new ClaimHeadersModel();
  private _returnedClaimdetail = [Array, Array];
  private _selectedClaimdetail: ClaimLineModel[] = [];
  private _selectedClaimadjust: ClaimAdjustModel[] = [];
  private _showClaimdetail: boolean;
  private _rejectionReasons: ClaimAdjustModel[] = [];
  private _hasRejections: boolean;
  private _parseddate: any;
  private _filename: string;
  private _filesource: string;
  private _remittancedownloadmodal: boolean = false;
  private _isMember: boolean;
  private _refProvInfo: DetailLinesModel = new DetailLinesModel();
  private _details: CheckRunDetailsModel = new CheckRunDetailsModel();
  private _claimInfo: DetailLinesModel[];
  private _paidInfo: DetailLinesModel;
  private _visitCounts: number;
  private _authNo: string;
  private _AdjustInfo: DetailLinesModel[];
  private _token: string;
  private _familyNo: string;
  public get familyNo(): string {
    return this._familyNo;
  }
  public set familyNo(value: string) {
    this._familyNo = value;
  }
  private _familyName: string;
  private _busyReverse: boolean = false;
  public get busyReverse(): boolean {
    return this._busyReverse;
  }
  public set busyReverse(value: boolean) {
    this._busyReverse = value;
  }







  constructor(private _router: Router, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _loginService: LoginService, private _securityService: SecurityService, private _settingsService: SettingsService) {
  }

  public get token(): string {
    return localStorage.getItem("jwt");
  }
  public set token(value: string) {
    localStorage.setItem("jwt",  value);
  }

  CloseNoClaims() {
    this.ClearAllInputFields();
    this._noClaimsfound = false
  }

  CloseClaimDetail() {
    this._showClaimdetail = false
    this._rejectionReasons = [];
    this._hasRejections = false

  }

  DisableDownloadBtn(x: string): boolean {
    let isDisabled: boolean;

    let inputDate: Date = new Date(x);

    if (x == "In Process") {
      isDisabled = true;
    }
    else if (isNaN(inputDate.getTime())) // Check if date is invalid
    {
      isDisabled = true;
    }
    else // Valid date
    {
      //Get delayDays
      let delayDaySetting: number;

      if (this._loginService.CurrentLoggedInUser.userType == 1) //Provider
      {
        delayDaySetting = Math.abs(parseInt(this._settingsService.GetSettingsDesc('payrunlag_prov')));
      }
      else if (this._loginService.CurrentLoggedInUser.userType == 2 || this._loginService.CurrentLoggedInUser.userType == 3 || this._loginService.CurrentLoggedInUser.userType == 5) //Bureau, Client, Broker
      {
        delayDaySetting = Math.abs(parseInt(this._settingsService.GetSettingsDesc('payrunlag')));
      }

      //let currentDate: Date = new Date(); //return datetime object
      //let currentDate = Date.now(); //returns Epoch date (milliseconds from 1 Jan 1970)
      //let testDate = inputDate.setDate(inputDate.getDate() + 3); //returns Epoch date (milliseconds from 1 Jan 1970)

      if (inputDate.setDate(inputDate.getDate() + delayDaySetting) > Date.now()) {
        isDisabled = true;
      }
      else {
        isDisabled = false;
      }
    }

    return isDisabled
  }

  ClaimDownload(selectedclaimnumber: string, paiddate: string, paidto: string, isMember: boolean) {
    this._claimNumber = selectedclaimnumber;
    this._claimpaidDate = paiddate;
    this._paidTo = paidto;
    this._remittancedownloadmodal = true;
    this._isMember = isMember

    this._http.post(this._baseUrl + "api/Security/DownloadClaimRemitance", {
      Username: this._loginService.CurrentLoggedInUser.username,
      ClaimNumber: this._claimNumber,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      PaidDate: this._claimpaidDate,
      paidTo: this._paidTo,
      isMember: this._isMember
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((specificremittance: ReturnedFileInformationModel) => {

        this._parseddate = specificremittance.parsedDate
        this._filename = specificremittance.fileName
        //this._filesource = '../../PamcPortal/assets/Remittances/' + this._parseddate + '/' + this._filename
        this._filesource = 'assets/Remittances/' + this._parseddate + '/' + this._filename

        var a = document.createElement('a');
        a.target = '_blank';
        a.setAttribute('type', 'hidden');
        a.href = this._filesource
        a.download = this._filename;
        document.body.appendChild(a);
        a.click();
        a.remove();



        this._remittancedownloadmodal = false
      },
        (error) => { console.log(error) })

  }

  ShowClaimDetailModal(claim: ClaimHeadersModel, billed: string, reversedate: string, paiddate: string) {
    this._selectedClaim = claim
    this._http.post(this._baseUrl + "api/Security/ClaimInfo", {
      ClaimNo: this._selectedClaim.claimNo
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((ReturnedDetail: any) => {
        this._returnedClaimdetail = ReturnedDetail
        this._selectedClaimdetail = Object(this._returnedClaimdetail)["item1"]
        this._selectedClaimadjust = Object(this._returnedClaimdetail)["item2"]

      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }

        },
        () => {
          this.GetDetailsOfRefProv();
        })


  }

  get refProviderInfo() {
    return this._refProvInfo;
  }

  get details() {
    return this._details;
  }

  GetDetailsOfRefProv() {
    this._details = new CheckRunDetailsModel();
    this._details.claimNo = this._selectedClaim.claimNo;

    this._http.post(this._baseUrl + 'api/Security/GetDetailsOfRefProv', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel) => {
        this._refProvInfo = new DetailLinesModel();
        this._refProvInfo.claimNo = recievedData.claimNo;
        this._refProvInfo.refProvId = recievedData.refProvId;
        this._refProvInfo.refProfName = recievedData.refProfName;
        this._refProvInfo.specCode = recievedData.specCode;
        this._refProvInfo.desc = recievedData.desc;
        if (recievedData.claimOriginal === '') {
          this._refProvInfo.claimOriginal = recievedData.claimNo;
        } else {
          this._refProvInfo.claimOriginal = recievedData.claimOriginal;
        }
        if (recievedData.claimreverse === '') {
          this._refProvInfo.claimreverse = 'No Reversal';
        } else {
          this._refProvInfo.claimreverse = recievedData.claimreverse;
        }
        if (recievedData.reversedate === '') {
          this._refProvInfo.reversedate = 'No Reverse Date';
        } else {
          this._refProvInfo.reversedate = recievedData.reversedate;
        }
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this.GetDetailsOfClaim();
        });
  }

  get paidInfo() {
    return this._paidInfo;
  }

  get claimInfo() {
    return this._claimInfo;
  }

  GetDetailsOfClaim() {
    this._securityService.LoadingModel = true;
    this._http.post(this._baseUrl + 'api/Security/GetDetailsOfClaim', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel[]) => {
        this._paidInfo = new DetailLinesModel();
        this._claimInfo = [];
        let x = recievedData;
        x.forEach((dl) => {
          this._claimInfo.push(dl);
        });
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this.GetAdjustOfClaim();
        });
  }

  public get AdjustInfo() {
    return this._AdjustInfo;
  }

  GetAdjustOfClaim() {
    this._http.post(this._baseUrl + 'api/Security/GetAdjustOfClaim', this._details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel[]) => {
        this._paidInfo = new DetailLinesModel();
        this._AdjustInfo = [];
        let x = recievedData;
        let temp: boolean;
        x.sort((a, b) => a.adjCode2 < b.adjCode ? 1 : -1);
        x.forEach((dl) => {
          temp = false;
          this.AdjustInfo.forEach((adj) => {
            if (adj.adjCode2 === dl.adjCode2) {
              temp = true;
            }
          });
          if (temp === false) {
            this._AdjustInfo.push(dl);
          }

        });
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this.GetVisitCounts();
        });
  }

  RejectClaim(claimToBeRejected: RejectClaimDetailModel) {

    this._http.post(this._baseUrl + 'api/Security/RejectClaim', claimToBeRejected,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData) => {

    },
      (error) => {

        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this.RefreshTable(claimToBeRejected);
        this.GetDetailsOfClaim();
      });
  }

  RefreshTable(claimToBeRejected: RejectClaimDetailModel) {

    this._http.post(this._baseUrl + 'api/Security/RefreshTabel', claimToBeRejected,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {



    },
      (error) => {
        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this.ValidateForm();
      });
  }
  public get visitCounts(): number {
    return this._visitCounts;
  }
  public set visitCounts(value: number) {
    this._visitCounts = value;
  }

  GetVisitCounts() {
    let memb: MemberNoModel = new MemberNoModel();
    memb.hpcode = this._loginService.CurrentLoggedInUser.username;
    memb.memberNo = this._selectedClaim.memberNo;
    this._http.post(this._baseUrl + 'api/Security/GetVisitClaims', memb,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedCount: MemberNoModel) => {
        let x: string = recievedCount.memberNo
        this.visitCounts = parseInt(x);
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this.GetAuthNo();
        });

  }

  public get authNo(): string {
    return this._authNo;
  }
  public set authNo(value: string) {
    this._authNo = value;
  }

  GetAuthNo() {
    let x: ValidationResultsModel = new ValidationResultsModel();
    x.message = this.details.claimNo;
    this._http.post(this._baseUrl + 'api/Security/GetAuthNo', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: ValidationResultsModel) => {
        this.authNo = recievedData.message;
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this._showClaimdetail = true;
          this._securityService.LoadingModel = false;
          this._securityService.rejectModalShow = false;
        });
  }

  ClearAllInputFields() {
    this._hpContract.hpcode = "000"
    this._claimDetails.memberNo = ""
    this._claimDetails.memberId = ""
    this._claimDetails.providerId = ""
    this._claimDetails.memberDOB = null
    this._claimDetails.serviceDate = null
    this._claimDetails.datePaid = null

  }

  AdministratorValidation() {
    let temparray = [];
    let TestBoolean: boolean;
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberNumber');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter a Member Number")
        }
      }
    }
    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this._hpContract.hpcode != "000") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorviewClaimmemberIDNumber');
    required = this._settingsService.GetSettingRequired('AdministratorviewClaimmemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter The Member's ID Number")
        }
      }
    }
    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Member's Date Of Birth")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Service Date")
        }
      }
    }


    enabled = this._settingsService.GetSettingEnabled('MemberViewClaimPracticeNumber');
    required = this._settingsService.GetSettingRequired('MemberViewClaimPracticeNumber');

    if (enabled == true) {
      if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
        TestBoolean = true
      } else {
        TestBoolean = true
        if (required == true) {
          temparray.push("Enter a Practice Number")
          TestBoolean = false
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('AdministratorViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('AdministratorViewClaimMemberPaidDate');

    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = temparray
    this._testClaimcompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderIdmodal == true) {
      this._emptyClaimform = false
      this._displayClaimtable = false
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true
        this._displayClaimtable = false
      } else {
        this.SearchClaim()
      }
    }
  }

  get isMemberClaim(): boolean {
    let check: boolean;
    if (this._router.url == "/member-claims") {
      check = true
    } else {
      check = false
    }
    return check;
  }

  CheckAdministratorProviderID() {
    this._paidTo = "M";
    this._loginService.CurrentLoggedInUser.provID = ""
    if (this._loginService.CurrentLoggedInUser.provID == "") {
      this._loginService.CurrentLoggedInUser.provID = this._claimDetails.providerId
    }
  }

  ProviderValidation() {
    let temparray = []
    let TestBoolean: boolean
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaiMemberNumber');
    required = this._settingsService.GetSettingRequired('ProviderViewClaiMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter a Member Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this._hpContract.hpcode != "000") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderviewClaimmemberIDNumber');
    required = this._settingsService.GetSettingRequired('ProviderviewClaimmemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter The Member's ID Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Member's Date Of Birth")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Service Date")
        }
      }
    }
    if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
      TestBoolean = true
    } else {
      temparray.push("Enter a Practice Number")
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('ProviderViewClaimMemberPaidDate');

    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        TestBoolean = true
      } else {
        TestBoolean = false
        if (required == true) {
          temparray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = temparray
    this._testClaimcompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderIdmodal == true) {
      this._emptyClaimform = false
      this._displayClaimtable = false
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true
        this._displayClaimtable = false
      } else {
        this.SearchClaim()
      }
    }
  }

  BureauValidation() {
    let tempArray = []
    let testBoolean: boolean;
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('ViewClaimSelectHealthPlan');
    required = this._settingsService.GetSettingRequired('ViewClaimSelectHealthPlan');

    if (enabled == true) {
      if (this._hpContract.hpcode != "000") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Select a Health plan")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberNumber');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberNumber');

    if (enabled == true) {
      if (this._claimDetails.memberNo != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter a Member Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberIDNumber');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberIDNumber');

    if (enabled == true) {
      if (this._claimDetails.memberId != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter The Member's ID Number")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberDOB');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberDOB');

    if (enabled == true) {
      if (this._claimDetails.memberDOB != null) {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Member's Date Of Birth")
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberServiceDate');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberServiceDate');

    if (enabled == true) {
      if (this._claimDetails.serviceDate != null && this._claimDetails.serviceDate != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Claim Service Date")
        }
      }
    }
    if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
      testBoolean = true
    } else {
      tempArray.push("Enter a Practice Number")
    }

    enabled = this._settingsService.GetSettingEnabled('ViewClaimMemberPaidDate');
    required = this._settingsService.GetSettingRequired('ViewClaimMemberPaidDate');


    if (enabled == true) {
      if (this._claimDetails.datePaid != null && this._claimDetails.datePaid != "") {
        testBoolean = true
      } else {
        testBoolean = false
        if (required == true) {
          tempArray.push("Enter the Claim Payment Date")
        }

      }
    }
    this._listOfrequirements = tempArray;
    this._testClaimcompleteness = testBoolean;

    if (this._listOfrequirements.length > 0) {
      this._testClaimcompleteness = false
    }
    if (this._invalidProvderIdmodal == true) {
      this._emptyClaimform = false;
      this._displayClaimtable = false;
    } else {
      if (this._testClaimcompleteness == false) {
        this._emptyClaimform = true;
        this._displayClaimtable = false;
      } else {
        this.SearchClaim();
      }
    }

  }

  CheckProviderID() {
    let filterprovID = this._loginService.ProvidersLinkedToBureau.filter(x => x.vendorid === this._claimDetails.providerId)
    if (filterprovID.length != 0) {
      this._loginService.CurrentLoggedInUser.provID = "";
      filterprovID.forEach(el => {
        this._loginService.CurrentLoggedInUser.provID = el.vendorid
        this._invalidProvderIdmodal = false
      });
    } else {
      this._loginService.CurrentLoggedInUser.provID = this._claimDetails.providerId
      if (filterprovID.length == 0 && this._claimDetails.providerId.length > 0) {
        this._invalidProvderIdmodal = true
      }
    }
  }

  ValidateForm() {
    if (this._loginService.CurrentLoggedInUser.userType == 2) {
      this.CheckProviderID();
      this.BureauValidation();
    }
    if (this._loginService.CurrentLoggedInUser.userType == 1) {
      this.ProviderValidation();
    }
    if (this._loginService.CurrentLoggedInUser.userType == 3) {
      this.CheckAdministratorProviderID();
      this.AdministratorValidation();
    }
  }

  SearchClaim() {
    this._validationSpinner = true
    this._disableClaimssearch = true;

    this._http.post(this._baseUrl + "api/Security/MemberClaimSearch", {

      Plan: this._hpContract.hpcode,
      MemberNo: this._claimDetails.memberNo,
      ServiceDate: this._claimDetails.serviceDate,
      DatePaid: this._claimDetails.datePaid,
      Practice: this._loginService.CurrentLoggedInUser.provID,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      userName: this._loginService.CurrentLoggedInUser.username,
      lobCode: this._loginService.CurrentLoggedInUser.lobcode,
      paidTo: this._paidTo,
      isMember: this.isMemberClaim

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedclaimdetail: ClaimHeadersModel[]) => {

        this._listOfclaims = returnedclaimdetail

        if (this._listOfclaims.length > 0) {
          this._displayClaimtable = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
        } else {
          this._displayClaimtable = false;
          this._noClaimsfound = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
        }

      },
        (error) => { console.log(error); },
        () => {
          this._busyReverse = false;
          this.GetFamilyNameSearch()
        });
  }

  public get familyName(): string {
    return this._familyName;
  }
  public set familyName(value: string) {
    this._familyName = value;
  }

  GetFamilyNameSearch() {
    this._http.post(this._baseUrl + 'api/Security/ProviderClaimSearchFamilyName', {

      Plan: this._hpContract.hpcode,
      MemberNo: this._claimDetails.memberNo,
      ServiceDate: this._claimDetails.serviceDate,
      DatePaid: this._claimDetails.datePaid,
      Practice: this._loginService.CurrentLoggedInUser.provID,
      UserType: this._loginService.CurrentLoggedInUser.userType,
      userName: this._loginService.CurrentLoggedInUser.username,
      lobCode: this._loginService.CurrentLoggedInUser.lobcode,
      paidTo: this._paidTo,
      isMember: this.isMemberClaim
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedFamily: ClaimHeadersModel) => {


        if (this._listOfclaims.length > 0) {
          this._displayClaimtable = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
          this._familyName = "";
          this._familyName = "Main Policy Holder: " + returnedFamily.memberFirstName + " " + returnedFamily.memberLastName;
        } else {
          this._displayClaimtable = false;
          this._noClaimsfound = true;
          this._validationSpinner = false;
          this._disableClaimssearch = false;
          this._familyName = "";
        }
      },
        (error) => {

        },
        () => {
          let c: string = this._claimDetails.memberNo;
          let y: boolean = c.includes('-');
          if (y) {
            let x: number = c.lastIndexOf('-');
            let v: number = c.length - (x + 1);
            if (v == 2) {
              let z = c.substring(0, x);
              this._familyNo = z;
            } else {
              this._familyNo = this._claimDetails.memberNo;
            }

          } else {
            this._familyNo = this._claimDetails.memberNo;
          }
        });
  }
  ReverseClaim() {
    this._busyReverse = true;
    this._http.post(this._baseUrl + "api/Security/ClaimReversal", {
      claimNumber: this._securityService.ClaimModal.claimNumber
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((claimReversal: ClaimReversalModel[]) => {
        this._securityService.ShowClaimReversalModal = false;
        this.SearchClaim()
      })
  }

  ShowRejectionReason(selectedClaim: number) {
    let RjectionOfClaim = this._selectedClaimadjust.filter(x => selectedClaim.toString() == x.claimtblrow)
    this._rejectionReasons = RjectionOfClaim
    this._hasRejections = true

  }

  DisableInfoBtn(billed: number, net: number, claimtblnummber: number): boolean {
    if (billed == net) {
      return true
    } else {
      return false
    }
  }

  get ClaimDetail() {
    return this._claimDetails;
  }
  set ClaimDetail(claim) {
    this._claimDetails = claim
  }

  get HP_Contracts() {
    return this._hpContract;
  }
  set HP_Contracts(val: HP_ContractsModel) {
    this._hpContract = val;
  }

  get DisableClaimSearchBtn() {
    return this._disableClaimssearch
  }
  set DisableClaimSearchBtn(isValid) {
    this._disableClaimssearch = isValid
  }

  get ClaimValidationSpinner() {
    return this._validationSpinner
  }
  set ClaimValidationSpinner(isValid) {
    this._validationSpinner = isValid
  }

  get NoClaims() {
    return this._noClaimsfound;
  }

  get RecievedClaims() {
    return this._listOfclaims
  }

  get EmptyClaimForm() {
    return this._emptyClaimform;
  }
  set EmptyClaimForm(isValid) {
    this._emptyClaimform = isValid;
  }

  get ClaimRequirements() {
    return this._listOfrequirements;
  }
  set ClaimRequirements(isValid) {
    this._listOfrequirements = isValid;
  }

  get DisplayClaimTable() {
    return this._displayClaimtable
  }
  set DisplayClaimTable(isValid) {
    this._displayClaimtable = isValid
  }

  get ClaimInfo() {
    return this._showClaimdetail;
  }
  set ClaimInfo(isValid) {
    this._showClaimdetail = isValid;
  }

  get SelectedClaim() {
    return this._selectedClaim
  }
  set SelectedClaim(claim: ClaimHeadersModel) {
    this._selectedClaim = claim
  }

  get ClaimInformation() {
    return this._selectedClaimdetail;
  }
  set ClaimInformation(claim: ClaimLineModel[]) {
    this._selectedClaimdetail = claim;
  }

  get ClaimHasRejections() {
    return this._hasRejections
  }
  set ClaimHasRejections(isValid) {
    this._hasRejections = isValid
  }

  get RejectionReasons() {
    return this._rejectionReasons
  }
  set RejectionReasons(claim: ClaimAdjustModel[]) {
    this._rejectionReasons = claim
  }

  get DownloadIndicator() {
    return this._remittancedownloadmodal;
  }
  set DownloadIndicator(isValid) {
    this._remittancedownloadmodal = isValid;
  }
}

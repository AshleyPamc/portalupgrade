import { Injectable } from '@angular/core';
import { FormControl, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  onlyDecimalInput(e, input) {

    // Allow copy cut paste (the angular or custom validator will then display a message if pasted value is invalid)
    if ((e.ctrlKey || e.metaKey) && (e.keyCode == 67 || e.keyCode == 86 || e.keyCode == 88)) {
      return;
    }

    // Array of allowed keys
    const allowedKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',', '.', 'Backspace', 'ArrowRight', 'ArrowLeft', 'Delete', 'Tab', 'Control', 'Shift'];

    // Only proceed if key is allowed 
    if (allowedKeys.indexOf(e.key) > -1) {

      //Change comma to dot and only one dot is allowed
      if (e.key == "," || e.key == ".") {
        if (input.value.indexOf(".") > -1) {
          e.preventDefault(); // Do not allow key - only one dot is allowed
        }
        else {
          e.preventDefault(); // Do not allow key - we will maullay add a dot
          input.value += ".";
        }
      }
    }
    else {
      e.preventDefault(); // Do not allow key
    }

    return;
  }

  onlyDecimalInputTwoDecimalPlaces(e, input) {

    // Allow copy cut paste (the angular or custom validator will then display a message if pasted value is invalid)
    if ((e.ctrlKey || e.metaKey) && (e.keyCode == 67 || e.keyCode == 86 || e.keyCode == 88)) {
      return;
    }

    // Array of allowed keys
    const allowedKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'Backspace', 'ArrowRight', 'ArrowLeft', 'Delete', 'Tab', 'Control', 'Shift'];

    // Only proceed if key is allowed 
    if (allowedKeys.indexOf(e.key) > -1) {

      // Only allow two decimals after the dot
      if (e.key == 0 || e.key == 1 || e.key == 2 || e.key == 3 || e.key == 4 || e.key == 5 || e.key == 6 || e.key == 7 || e.key == 8 || e.key == 9) {
        if (input.value.indexOf(".") > -1 && input.value.length - input.value.indexOf(".") >= 3) {
          e.preventDefault();
        }
      }

      //Only one dot is allowed
      if (e.key == ".") {
        if (input.value.indexOf(".") > -1) {
          e.preventDefault(); // Do not allow key - only one dot is allowed
        }       
      }

    }
    else {
      e.preventDefault(); // Do not allow key
    }

    return;
  }

  onlyIntInput(e, input) {

    // Allow copy cut paste (the angular or custom validator will then display a message if pasted value is invalid)
    if ((e.ctrlKey || e.metaKey) && (e.keyCode == 67 || e.keyCode == 86 || e.keyCode == 88)) {
      return;
    }

    // Array of allowed keys
    const allowedKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'Backspace', 'ArrowRight', 'ArrowLeft', 'Delete', 'Tab', 'Control', 'Shift'];

    // Only proceed if key is allowed 
    if (allowedKeys.indexOf(e.key) > -1) {

      return;
    }
    else {
      e.preventDefault(); // Do not allow key
    }

    return;
  }

  onlyBloodPressureInput(e, input) {

    // Allow copy cut paste (the angular or custom validator will then display a message if pasted value is invalid)
    if ((e.ctrlKey || e.metaKey) && (e.keyCode == 67 || e.keyCode == 86 || e.keyCode == 88)) {
      return;
    }

    // Array of allowed keys
    const allowedKeys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '/', 'Backspace', 'ArrowRight', 'ArrowLeft', 'Delete', 'Tab', 'Control', 'Shift'];

    // Only proceed if key is allowed 
    if (allowedKeys.indexOf(e.key) > -1) {

      //Only allow one slash
      if (e.key == "/") {
        if (input.value.indexOf("/") > -1) {
          e.preventDefault(); // Do not allow key - only one slash is allowed
        }
        else {
          return; // Allow key since it is the first slash
        }
      }
    }
    else {
      e.preventDefault(); // Do not allow key
    }

    return;
  }

  decimalOrIntCustomValidator(control: FormControl): { [s: string]: boolean } {

    if (control.value != undefined && control.value != null) {
      if (isNaN(Number(control.value))) {  // Check if not valid number 
        return { 'invalidValue': true };
      }
    }

    return null;
  }

  intCustomValidator(control: FormControl): { [s: string]: boolean } {

    if (control.value != undefined && control.value != null) {
      if (isNaN(Number(control.value))) {  // Check if not valid number 
        return { 'invalidValue': true };
      }
      else if (!Number.isInteger(control.value)) {  // Check if valid number is not integer
        return { 'invalidValue': true }; 
      }
    }

    return null;
  }

  dateCustomValidator(minDate: Date, maxDate: Date, control: FormControl): { [s: string]: boolean } {

    if (control.value != undefined && control.value != null) {

      let inputDate: Date = new Date(control.value);

      //console.log("inputDate:");
      //console.log(inputDate);

      //console.log("inputDate.getTime:");
      //console.log(inputDate.getTime());

      // Check if not valid date (will trigger when for example 31 Feb is entered) (must pass input value in html if not using clarity datetime picker)
      if (isNaN(inputDate.getTime())) {
        control.setValue(null);
        control.reset();
        return { 'invalidValue': true };
      }

      //console.log("getFullYear():");
      //console.log(inputDate.getFullYear());

      // Always do this check as this is the SQL max year (the input validates max day and month already)
      if (inputDate.getFullYear() > 9999) {
        return { 'invalidValue': true };
      }

      // Apply minimum date if param is passed
      if (minDate != undefined && minDate != null) {

        //let minDateDt: Date = new Date(minDate);

        //console.log("minDate:");
        //console.log(minDate);

        if (inputDate < minDate) {
          return { 'invalidValue': true };
        }
      }

      // Apply maximum date if param is passed
      if (maxDate != undefined && maxDate != null) {

        //console.log("maxDate:");
        //console.log(maxDate);

        if (inputDate > maxDate) {
          return { 'invalidValue': true };
        }
      }
    }
    
    return null;
  }

  dateValidatorSpecifyMinAndMax(minDate: Date, maxDate: Date): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {

      const value = control.value;

      if (!value) {
        return null;
      }

      if (Date.parse(control.value) < minDate.getTime() || Date.parse(control.value) > maxDate.getTime()) {
        return { invalidDate: true };
      }
      else {
        return null;
      }

    }
  }

  dateValidatorMaxDateToday(minDate: Date): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {

      const value = control.value;

      if (!value) {
        return null;
      }

      if (Date.parse(control.value) < minDate.getTime() || Date.parse(control.value) > new Date().getTime()) {
        return { invalidDate: true };
      }
      else {
        return null;
      }

    }
  }

}

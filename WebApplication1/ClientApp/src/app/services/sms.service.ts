import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SMSService {

  constructor(private _http: HttpClient) { }


  GetTokenAndSendSMS(message: string, contactNo: string) {
    let token: string = "";

    this._http.get(
      "https://rest.smsportal.com/v1/Authentication",
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer MDJjMjhjOTAtMTUxZC00MmJhLTlkMDctMWUzNDA4NjRhZDg2OklPSEMzckhwM0dCY0hZTkFOZmZWRlN3VEo3N0xleUQ1",
          "Content-Type": "application/json"
          

        })
      }
    ).subscribe((results: any) => {
      token = results.token;
      console.log(results);
    },
      (error) => {
        console.log(error);
      },
      () => {
        this.SendSMS(token, message, contactNo);
      })
  }

  SendSMS(token:string,message:string,contactNo:string) {
    this._http.post(
      "https://rest.smsportal.com/v1/bulkmessages", {
      "Messages": [
          {
            "Content": message,
          "Destination": contactNo
        }
      ]
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer "+ token,
          "Content-Type": "application/json"


        })
      }
    ).subscribe((results: any) => {
     // this.token = results.token;
      console.log(results)
    },
      (error) => {
        console.log(error);
      },
      () => {

      })
  }

}

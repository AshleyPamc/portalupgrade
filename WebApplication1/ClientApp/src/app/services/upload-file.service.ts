import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from './settings.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthFiles } from '../models/auth-files';
import { ValidationResultsModel } from '../models/ValidationResultsModel';

@Injectable({
  providedIn: 'root'
})
export class FileuploadService {

  public _valid: boolean = false;
  public _busy: boolean = false;
  public _popUp: boolean = false;
  public _success: boolean = false;
  public files: FormData = new FormData();
  public _totalSize: string;

  constructor(private _router: Router, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _settingsService: SettingsService) { }


  SendFiles(files: FormData) {
    let token = localStorage.getItem('jwt');
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/SendAuthFiles', files,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }

    ).subscribe((returned) => {


    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._popUp = true;
      },
      () => {
        this._busy = false;
        this._success = true;
      })
  }

  ValidateUser(data: AuthFiles) {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this._http.post(this._baseUrl + 'api/Authorization/ValidateUser', data, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: ValidationResultsModel) => {
      this._valid = results.valid;
    },
      (error) =>
      {        
        console.log(error);
        this._busy = false;
        this._popUp = true;        
      },
      () => {
        if (this._valid) {
          this.SendFiles(this.files);
        } else {
          this._busy = false;
          this._popUp = true;          
        }
      })
  }

}

import { Injectable, Inject } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SecurityService } from './security.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Recordtype1Model } from '../models/Recordtype1Model';
import { PlaceOfServiceModel } from '../models/PlaceOfServiceModel';
import { HealthplanModel } from '../models/HealthplanModel';
import { VendoridsModel } from '../models/VendoridsModel';
import { Recordtype2Model } from '../models/Recordtype2Model';
import { ServicetypesModel } from '../models/ServicetypesModel';
import { ModifierModel } from '../models/ModifierModel';
import { ToothNumbersModel } from '../models/ToothNumbersModel';
import { Recordtype5Model } from '../models/Recordtype5Model';
import { AdjustcodesModel } from '../models/AdjustcodesModel';
import { Recordtype8Model } from '../models/Recordtype8Model';
import { Recordtype4Model } from '../models/Recordtype4Model';
//import { TestClaimHeaderModel } from '../models/TestClaimHeaderModel';
//import { HealthplanComponent } from '../../admin/healthplan/healthplan.component';
import { Observable, from } from 'rxjs';
import { RejectionCodesModel } from '../models/RejectionCodesModel';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { ClaimReport } from '../models/ClaimReport';
import { tryParse } from 'selenium-webdriver/http';
import { BankingDetailModel } from '../models/BankingDetailModel';
import { Objectlog } from '../models/objectlog';
import { LoginService } from './login.service';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';
import { JoyrideService } from 'ngx-joyride';
import { ClaimMasterModel } from '../models/ClaimMasterModel';
import { ClaimDetailModel } from '../models/ClaimDetailModel';
import { ProviderInfoModel } from '../models/ProviderInfoModel';
import { ProviderAndVendorInfoModel } from '../models/ProviderAndVendorInfoModel';
import { MembershipService } from './membership.service';
import { MemberEligibilityModel } from '../models/MemberEligibilityModel';
import { ValidationService } from './validation.service';
import { Diagcodes } from '../models/diagcodes';
import { ProcCodeModel } from '../models/ProcCodeModel';
import { ModifierCodeModel } from '../models/ModifierCodeModel';


@Injectable({
  providedIn: 'root'
})
export class ClaimCaptureService {

  public auditMsg: ValidationResultsModel = new ValidationResultsModel();
  public _disableBack: boolean = false;
  private _wizardOpen: boolean = false;
  public _errorFound: boolean = false;

  // Declarations for ClaimHeader
  public errorMsgFound: string = "";
  public currentClaimHeader: Recordtype1Model = new Recordtype1Model();
  public plcSvcList: PlaceOfServiceModel[] = [];
  //public HpCodesList: HealthplanModel[] = new HealthplanModel();
  public vendorIds: VendoridsModel[] = [];
  public dataBaseMessageProv: string;
  public dataBaseMessageMemb: string;
  public dataBaseMessageDepMemb: string;
  public dataBaseMessageRefProv: string;
  public dataBaseMessageVend: string;
  public dataBaseMessageAuth: string;
  public phSelect: string[] = ['PROFESSIONAL', 'HOSPITAL'];
  public ctSelect: string[] = ['PROVIDER', 'MEMBER'];
  public btnClick = true;
  public vedorFieldVissible: boolean = true;
  public memberNumPrefix: string;
  //public tempMesgHolder: any; //Jaco comment out 2024-01-05
  public msgFamily: string = 'Searching';
  public msgDependent: string = 'Searching';
  public displayClinicalCodes: boolean = false;
  public toothNumString: string;
  public _busy: boolean = false;
  public _staleDateModel: boolean = false;
  public _stalePeriod: number;

  public _bankingPrompt: boolean = false;
  public _openBanking: boolean = false;
  public _load: boolean = false;
  public _incorrect: boolean = false;
  public _incorrectProc: boolean = false;
  public _success: boolean = false;
  public fileid: number;
  public rec1Id: number;
  public _disableCancel: boolean = false;

  //Declarations for ClaimDetail
  public currentClaimDetail: Recordtype2Model = new Recordtype2Model();
  public claimDetailList: Recordtype2Model[] = [];
  public billedAmount: number = 0;
  public dataBaseMessageProccode: string;
  public listOfServiceTypes: ServicetypesModel[] = [];
  public btnClaimDetailDone: boolean = false;
  public DefaultDate: any;
  public showModCode: boolean = false;
  public showToothNum: boolean = false;
  public showReject: boolean = false;
  public dataBaseMsgMod1: string;
  public dataBaseMsgMod2: string;
  public dataBaseMsgMod3: string;
  public dataBaseMsgMod4: string;
  public modCode: ModifierModel = new ModifierModel();
  public toothNum: ToothNumbersModel = new ToothNumbersModel();
  public dataBaseMessageTooth1: string;
  public dataBaseMessageTooth2: string;
  public dataBaseMessageTooth3: string;
  public dataBaseMessageTooth4: string;
  public dataBaseMessageTooth5: string;
  public dataBaseMessageTooth6: string;
  public dataBaseMessageTooth7: string;
  public dataBaseMessageTooth8: string;
  public showNotes: boolean = false;
  public newBankingDetails: BankingDetailModel = new BankingDetailModel();

  //Declarations for DiagCodes
  public diagnosticCodes: Recordtype5Model = new Recordtype5Model();
  public findSameDiagCode: boolean = false;
  public findSameDiagRef: boolean = false;
  public diagnosticCodesList: Recordtype5Model[] = [];
  public dataBaseMessageDiagC1: string;
  public dataBaseMessageDiagC2: string;
  public dataBaseMessageDiagC3: string;
  public dataBaseMessageDiagC4: string = "";
  public returnMsg: any;

  //Declarations for CaptureWizard
  public wizardOpen: boolean = false;
  public enableWizardNext: boolean;
  public pageCount: number = 1;
  //private _token: string;

  //Declarations for Service
  public _webApiUnavalable: boolean = false;
  public SavedIdRec1: number;
  public SavedIdRec2: number;
  public fileCreated: boolean = false;
  public messageSaved: string;
  public buttonShow: boolean = true;

  //Declaration for Rejection codes
  public defualtCodesList: AdjustcodesModel[] = [];
  public codeLine: AdjustcodesModel = new AdjustcodesModel();
  public adjustmentCode: Recordtype8Model = new Recordtype8Model();
  public RejectList: Recordtype8Model[] = [];
  public rejectCode: Recordtype8Model = new Recordtype8Model();
  public tempIdOnEdit: number;
  public lobCode: RejectionCodesModel = new RejectionCodesModel();
  public rejectionReasons: RejectionCodesModel[] = [];

  //Declaration for Notes
  public claimNotes: Recordtype4Model = new Recordtype4Model();
  public notesLists: Recordtype4Model[] = [];

  //View Captured claims
  private curPage: number = 1;
  public claimReportTotalPages: number = 0;
  public _ReportList: ClaimReport[] = [];
  //public _ReportListTemp: ClaimReport[] = [];
  public _ReportListDisplay: ClaimReport[];
  public _showCapturedClaimsSearchForm: boolean = false;
  public filterCountDetails: number = 0;
  public filterCountNet: number = 0;
  //public earliestdate: string = "";
  //public latestDate: string = "";
  private capturedClaimsForm: FormGroup;

  // Claim Capture
  private _claimMasterForm: FormGroup;
  public get ClaimMasterForm(): FormGroup { return this._claimMasterForm; }

  private _claimDetailForm: FormGroup;
  public get ClaimDetailForm(): FormGroup { return this._claimDetailForm; }

  private _validationForm: FormGroup;
  public get ValidationForm(): FormGroup { return this._validationForm; }

  public _claimDetailModal: boolean = false;

  private _listOfServiceTypesFiltered: ServicetypesModel[] = [];
  public get ListOfServiceTypesFiltered(): ServicetypesModel[] { return this._listOfServiceTypesFiltered; }


  public _busyLoading: boolean = false;
  public _errorModal: boolean = false;
  public _errorMessage: string = "";
  public _errorMessageHeading: string = "";

  public _successModal: boolean = false;
  public _successMessage: string = "";
  public _successMessageHeading: string = "";

  //public _openAndDisableToothPanel: boolean = false;
  //public get OpenAndDisableToothPanel(): boolean { return this._openAndDisableToothPanel; }

  private _bankingDetailsForm: FormGroup;
  public get BankingDetailsForm(): FormGroup { return this._bankingDetailsForm; }

  public _bankingPromptModal: boolean = false;
  public _bankingEditModal: boolean = false;
  public _bankingEditWarningModal: boolean = false;
  private _accTypeSelect = ["CHEQUE", "SAVINGS", "CREDIT CARD", "TRANSMISSION", "OTHER"];
  public get AccTypeSelect() { return this._accTypeSelect; }

  public _cancelDetailModal: boolean = false;
  public _clearClaimModal: boolean = false;
  public _confirmationModal: boolean = false;

  private _currentClaim: ClaimMasterModel = new ClaimMasterModel();
  public get CurrentClaim(): ClaimMasterModel { return this._currentClaim; }

  private _currentClaimDetail: ClaimDetailModel = new ClaimDetailModel();
  public get CurrentClaimDetail(): ClaimDetailModel { return this._currentClaimDetail; }

  private _vendorList: VendoridsModel[] = [];
  public get VendorList(): VendoridsModel[] { return this._vendorList; }

  public HpCodesList: HealthplanModel[] = [];

  private _diagCodesList: Diagcodes[] = [];
  public get DiagCodesList(): Diagcodes[] { return this._diagCodesList; }

  private _toothNumberList: ToothNumbersModel[] = [];
  public get ToothNumberList(): ToothNumbersModel[] { return this._toothNumberList; }

  private _rejectionCodeList: RejectionCodesModel[] = [];
  public get RejectionCodeList(): RejectionCodesModel[] { return this._rejectionCodeList; }

  private _rejectionCodeListFiltered: RejectionCodesModel[] = [];
  public get RejectionCodeListFiltered(): RejectionCodesModel[] { return this._rejectionCodeListFiltered; }

  private _modifierCodesList: ModifierCodeModel[] = [];
  public get ModifierCodesList(): ModifierCodeModel[] { return this._modifierCodesList; }

  private _familyList: MemberEligibilityModel[] = [];
  public get FamilyList(): MemberEligibilityModel[] { return this._familyList; }

  //private _familyOpt: string = '';
  //public get FamilyOpt(): string { return this._familyOpt; }

  //private _depOpthruDt: string = '';
  //public get DepOpthruDt(): string { return this._depOpthruDt; }

  private _claimTypeSelect = [["P", "PROVIDER"], ["M", "MEMBER"]];
  public get ClaimtypeSelect() { return this._claimTypeSelect; }

  private _profHospSelect = [["P", "PROFESSIONAL"], ["H", "HOSPITAL"]];
  public get ProfHospSelect() { return this._profHospSelect; }

  constructor(private _router: Router, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _securityService: SecurityService, private _loginService: LoginService, private _fb: FormBuilder, private readonly joyrideService: JoyrideService, private _membershipService: MembershipService, private _validationService: ValidationService, private _datePipe: DatePipe) {
         
    this.capturedClaimsForm = this._fb.group({
      'createdFrom': [null, Validators.required],
      'createdTo': [null, Validators.required],
      'capturedBy': [null, Validators.required]
    });

    this.ClaimMasterFormInit(); // Wrap the form creation in a method, because when we reset the form later, the values are set to null instead of '', so then we can call the method for initial values.
    this.BankingDetailsFormInit(); // Wrap the form creation in a method, because when we reset the form later, the values are set to null instead of '', so then we can call the method for initial values.
    this.ClaimDetailFormInit(); // Wrap the form creation in a method, because when we reset the form later, the values are set to null instead of '', so then we can call the method for initial values.
    this.ValidationFormInit(); // Wrap the form creation in a method, because when we reset the form later, the values are set to null instead of '', so then we can call the method for initial values.        
  }

  ClaimMasterFormInit() {
    this._claimMasterForm = this._fb.group({
      'hpCode': ['', Validators.required],
      'profHosp': ['', Validators.required],
      'provId': ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(7), Validators.maxLength(7)]],
      'vendor': ['', Validators.required],
      'refProvId': ['', [Validators.pattern("^[0-9]*$"), /*Validators.minLength(7),*/ Validators.maxLength(7)]],
      'provClaim': ['', [Validators.required, Validators.maxLength(20)]],
      'policyNo': ['', Validators.required],
      'dependant': ['', Validators.required],
      'dateReceived': ['', [Validators.required, this._validationService.dateValidatorMaxDateToday(new Date(this.GetMinDateStr()))]],
      'claimType': ['', Validators.required],
      'authNo': ['', Validators.maxLength(16)],
      'diag1': ['', [Validators.required, Validators.maxLength(10)]],
      'diag2': ['', Validators.maxLength(10)],
      'diag3': ['', Validators.maxLength(10)],
      'diag4': ['', Validators.maxLength(10)],
    });

    if (this._profHospSelect.length == 1) {
      this._claimMasterForm.get('profHosp').setValue(this._profHospSelect[0][0]);
      this._claimMasterForm.get('profHosp').markAsTouched();
    }
    
    if (this.HpCodesList.length == 1) {
      this._claimMasterForm.get('hpCode').setValue(this.HpCodesList[0].code);
      this._claimMasterForm.get('hpCode').markAsTouched();
      //this.LoadServiceTypes(); move to memb search to get option
    }
  }

  ClaimDetailFormInit() {
    this._claimDetailForm = this._fb.group({
      'procCode': ['', [Validators.required, Validators.maxLength(20)]],
      'phCode': ['', Validators.required],
      'svcDateFrom': ['', [Validators.required, this._validationService.dateValidatorMaxDateToday(new Date(this.GetMinDateStr()))]],
      'svcDateTo': ['', [Validators.required, this._validationService.dateValidatorMaxDateToday(new Date(this.GetMinDateStr()))]],
      'billed': ['', [Validators.required, Validators.max(99999999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]], // SQL decimal(10,2)
      'qty': ['', [Validators.required, Validators.max(9999.99), this._validationService.decimalOrIntCustomValidator.bind(this)]], // SQL decimal(6,2)
      'rejCode': ['', Validators.maxLength(10)],
      'diag': ['', Validators.required],
      'tooth1': ['', Validators.maxLength(2)],
      'tooth2': ['', Validators.maxLength(2)],
      'tooth3': ['', Validators.maxLength(2)],
      'tooth4': ['', Validators.maxLength(2)],
      'tooth5': ['', Validators.maxLength(2)],
      'tooth6': ['', Validators.maxLength(2)],
      'tooth7': ['', Validators.maxLength(2)],
      'tooth8': ['', Validators.maxLength(2)],
      'modifier1': ['', Validators.maxLength(7)],
      'modifier2': ['', Validators.maxLength(7)],
      'modifier3': ['', Validators.maxLength(7)],
      'modifier4': ['', Validators.maxLength(7)],
      'note': ['', Validators.maxLength(40)],
    });
  }

  ValidationFormInit() {
    this._validationForm = this._fb.group({
      'checked': [null, Validators.required],
    });
  }

  BankingDetailsFormInit() {
    this._bankingDetailsForm = this._fb.group({
      'bankName': ['', [Validators.required, Validators.maxLength(30)]],
      'branchCode': ['', [Validators.required, Validators.maxLength(10)]],
      'accountNo': ['', [Validators.required, Validators.maxLength(20)]],
      'accountType': ['', [Validators.required, Validators.maxLength(20)]]
    });
  }

  GetMinDateStr(): string {
    //locale = navigator.language; can't trust this, the browser locale is almost always en-US even though the OS is something else and even though the html date input uses the OS locale for input formatting
    //return new Date().toISOString().split('T')[0]; do NOT use toISOString, it uses UTC timezone (off by 8 hours or so)
    //return new Date("2000-01-01").toLocaleDateString(); this will use the current locale, which in edge uses the OS locale, and in chrome uses the browser locale (which is almost always en-US even though the OS is something else)
    //return new Date(2000, 0, 1).toLocaleDateString('fr-CA'); can also pass integers to Date constructor but then remember that months start with 0 for Jan, 1 for Feb etc.
    //return new Date("2000-01-01").toLocaleDateString('en-ZA'); problem with using en-ZA is that it returns yyyy/MM/dd. The html date input min and max attributes requires yyyy-MM-dd (dashes not slashes!)
    //console.log("getMinDate() = " + new Date("2000-01-01").toLocaleDateString('fr-CA'));
    let currYearMin5: number = (new Date()).getFullYear() - 5;
    return new Date(currYearMin5.toString() + "-01-01").toLocaleDateString('fr-CA')
  }

  GetTodayStr(): string {
    //console.log("getToday() = " + new Date().toLocaleDateString('fr-CA'));
    return new Date().toLocaleDateString('fr-CA');
  }

  async LoadClaimCaptureSetup() {
       
    if (this.HpCodesList.length < 1) {
      await this.HpCodes();
    }

    if (this._diagCodesList.length < 1) {
      this.LoadAllDiagCodes();
    }

    if (this._toothNumberList.length < 1) {
      this.LoadAllToothNumbers();
    }

    if (this._rejectionCodeList.length < 1) {
      this.LoadRejectionCodesByLobCode();
    }

    if (this._modifierCodesList.length < 1) {
      this.LoadAllModifierCodes();
    }

    // Jaco 2024-09-10 Hardcode for Africa Assist
    let lobCode: string = this._loginService.CurrentLoggedInUser.lobcode.replace(" ", "");
    if (lobCode.indexOf("J") > -1 && lobCode.indexOf("AU") > -1 && lobCode.indexOf("LOA") > -1) {
      this._profHospSelect = [["H", "HOSPITAL"]];  // Only HOSPITAL claim capture - not PROFESSIONAL
      this.HpCodesList = this.HpCodesList.filter(x => x.lobcode != "J"); // Only AUL and LION claim capture - not ESSE and SANLAM
      
      this.ClaimMasterFormInit();
    }

  }

  LoadAllModifierCodes() {

    this._http.get(this._baseUrl + 'api/ClaimCapture/LoadAllModifierCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).
      subscribe((returnedList: [ModifierCodeModel]) => {
        this._modifierCodesList = returnedList;
        if (this._modifierCodesList.length < 1) {
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = "0 Modifier Codes loaded";
        }
      },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = error.message;
        },
        () => { });
  }

  LoadRejectionCodesByLobCode() {
    const params = new HttpParams().append('lobCode', this._loginService.CurrentLoggedInUser.lobcode);
    this._http.get(this._baseUrl + 'api/ClaimCapture/GetRejectionCodesByLobCode',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        }),
        params
      }).
      subscribe((returnedList: [RejectionCodesModel]) => {
        this._rejectionCodeList = returnedList;
        if (this._rejectionCodeList.length < 1) {
          this._errorModal = true;
          this._errorMessageHeading = "Invalid setup, claims will not be captured correctly.";
          this._errorMessage = "No rejection reason codes or LOBCODE configured for this user.";
        }
      },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = error.message;
        },
        () => { });
  }

  LoadAllDiagCodes() {

    this._http.get(this._baseUrl + 'api/ClaimCapture/LoadAllDiagCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).
      subscribe((returnedList: [Diagcodes]) => {
        this._diagCodesList = returnedList;
        if (this._diagCodesList.length < 1) {
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = "0 Diagnosis Codes loaded";
        }
      },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = error.message;
        },
        () => { });
  }

  LoadAllToothNumbers() {
    this._http.get(this._baseUrl + 'api/ClaimCapture/LoadAllToothNumbers',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).
      subscribe((returnedList: [ToothNumbersModel]) => {
        this._toothNumberList = returnedList;
        if (this._toothNumberList.length < 1) {
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = "0 Tooth numbers loaded";
        }
      },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = error.message;
        },
        () => { });
  }

  TrimClaimMasterValue(inputValue: string) {
    this._claimMasterForm.get(inputValue).setValue(this._claimMasterForm.get(inputValue).value.trim());
  }

  ValidateDiagCode(controlName: string) {
    this._claimMasterForm.get(controlName).setValue(this._claimMasterForm.get(controlName).value.trim());

    let diagCodes: Diagcodes[] = this._diagCodesList.filter(x => x.code.toUpperCase() == this._claimMasterForm.get(controlName).value.toUpperCase());

    if (diagCodes.length > 0) {

      if (controlName == 'diag1') {
        this._claimMasterForm.get(controlName).setValue(diagCodes[0].code);
        this._currentClaim.diag1 = diagCodes[0].code;
        this._currentClaim.diag1desc = diagCodes[0].desc;
      } else if (controlName == 'diag2') {
        this._claimMasterForm.get(controlName).setValue(diagCodes[0].code);
        this._currentClaim.diag2 = diagCodes[0].code;
        this._currentClaim.diag2desc = diagCodes[0].desc;
      } else if (controlName == 'diag3') {
        this._claimMasterForm.get(controlName).setValue(diagCodes[0].code);
        this._currentClaim.diag3 = diagCodes[0].code;
        this._currentClaim.diag3desc = diagCodes[0].desc;
      } else if (controlName == 'diag4') {
        this._claimMasterForm.get(controlName).setValue(diagCodes[0].code);
        this._currentClaim.diag4 = diagCodes[0].code;
        this._currentClaim.diag4desc = diagCodes[0].desc;
      } //else if (controlName == 'diag5') {
      //  this._claimMasterForm.get(controlName).setValue(diagCodes[0].code);
      //  this._currentClaim.diag5 = diagCodes[0].code;
      //  this._currentClaim.diag5desc = diagCodes[0].description;
      //}
    } else {
      if (controlName == 'diag1') {
        this._currentClaim.diag1 = "";
        this._currentClaim.diag1desc = "";
        this._claimMasterForm.get(controlName).setErrors({ 'incorrect': true });
      } else if (controlName == 'diag2') {
        this._currentClaim.diag2 = "";
        this._currentClaim.diag2desc = "";
        if (this._claimMasterForm.get(controlName).value != '') {
          this._claimMasterForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } else if (controlName == 'diag3') {
        this._currentClaim.diag3 = "";
        this._currentClaim.diag3desc = "";
        if (this._claimMasterForm.get(controlName).value != '') {
          this._claimMasterForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } else if (controlName == 'diag4') {
        this._currentClaim.diag4 = "";
        this._currentClaim.diag4desc = "";
        if (this._claimMasterForm.get(controlName).value != '') {
          this._claimMasterForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } //else if (controlName == 'diag5') {
      //  this._currentClaim.diag5 = "";
      //  this._currentClaim.diag5desc = "";
      //  if (this._claimMasterForm.get(controlName).value != '') {
      //    this._claimMasterForm.get(controlName).setErrors({ 'incorrect': true });
      //  }
      //}
    }
  }

  ValidateToothNo(controlName: string) {

    this._claimDetailForm.get(controlName).setValue(this._claimDetailForm.get(controlName).value.trim()); //trim

    let toothNumbers: ToothNumbersModel[] = this._toothNumberList.filter(x => x.toothNum == this._claimDetailForm.get(controlName).value); //search list

    if (toothNumbers.length > 0) {

      if (controlName == 'tooth1') {
        this._currentClaimDetail.tooth1 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth1Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth2') {
        this._currentClaimDetail.tooth2 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth2Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth3') {
        this._currentClaimDetail.tooth3 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth3Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth4') {
        this._currentClaimDetail.tooth4 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth4Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth5') {
        this._currentClaimDetail.tooth5 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth5Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth6') {
        this._currentClaimDetail.tooth6 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth6Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth7') {
        this._currentClaimDetail.tooth7 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth7Desc = toothNumbers[0].toothNumDesc;
      } else if (controlName == 'tooth8') {
        this._currentClaimDetail.tooth8 = toothNumbers[0].toothNum;
        this._currentClaimDetail.tooth8Desc = toothNumbers[0].toothNumDesc;
      }
    } else {
      if (controlName == 'tooth1') {
        this._currentClaimDetail.tooth1 = "";
        this._currentClaimDetail.tooth1Desc = "";
      } else if (controlName == 'tooth2') {
        this._currentClaimDetail.tooth2 = "";
        this._currentClaimDetail.tooth2Desc = "";
      } else if (controlName == 'tooth3') {
        this._currentClaimDetail.tooth3 = "";
        this._currentClaimDetail.tooth3Desc = "";
      } else if (controlName == 'tooth4') {
        this._currentClaimDetail.tooth4 = "";
        this._currentClaimDetail.tooth4Desc = "";
      } else if (controlName == 'tooth5') {
        this._currentClaimDetail.tooth5 = "";
        this._currentClaimDetail.tooth5Desc = "";
      } else if (controlName == 'tooth6') {
        this._currentClaimDetail.tooth6 = "";
        this._currentClaimDetail.tooth6Desc = "";
      } else if (controlName == 'tooth7') {
        this._currentClaimDetail.tooth7 = "";
        this._currentClaimDetail.tooth7Desc = "";
      } else if (controlName == 'tooth8') {
        this._currentClaimDetail.tooth8 = "";
        this._currentClaimDetail.tooth8Desc = "";
      }

      if (this._claimDetailForm.get(controlName).value != '') {
        this._claimDetailForm.get(controlName).setErrors({ 'incorrect': true });
      }
    }

  }

  GetRefProvSpecAndName() {

    this.ClearRefProvHelpers();

    this._claimMasterForm.get("refProvId").setValue(this._claimMasterForm.get("refProvId").value.trim());

    if (this._claimMasterForm.get("refProvId").valid) {
      // We know now field is blank OR length is not more than 7 and it is a number, but we have to manually valuate the min length (if min length is handled in form declaration, then error message is displayed while user is typing)
      if (this._claimMasterForm.get("refProvId").value.length > 0 && this._claimMasterForm.get("refProvId").value.length < 7)
        this._claimMasterForm.get('refProvId').setErrors({ 'incorrect': true });
    }

    if (this._claimMasterForm.get("refProvId").valid && this._claimMasterForm.get("refProvId").value != "") {

      this._busyLoading = true;

      const params = new HttpParams().append('provid', this._claimMasterForm.get("refProvId").value);
      this._http.get(this._baseUrl + 'api/ClaimCapture/GetRefProvSpecAndName',
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"
          }),
          params
        }
      ).subscribe(
        (provInfo: ProviderInfoModel) => {
          if (provInfo.provid != null && provInfo.provid != "") {
            this._currentClaim.refProvName = provInfo.lastName;
            this._currentClaim.refProvSpec = provInfo.specCode;
            this._currentClaim.refProvSpecDescr = provInfo.specCodeDescr;
          } else {
            this._currentClaim.refProvName = this._claimMasterForm.get("refProvId").value;
            this._currentClaim.refProvSpec = "";
            this._currentClaim.refProvSpecDescr = "";
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._busyLoading = false;
          this._errorModal = true;
          this._errorMessageHeading = "Referring Provider validation failed";
          this._errorMessage = error.message;
        },
        () => {
          this._busyLoading = false;
        });
    }
  }

  ClearRefProvHelpers() {
    this._currentClaim.refProvName = "";
    this._currentClaim.refProvSpec = "";
    this._currentClaim.refProvSpecDescr = "";
  }

  GetProvSpecAndNameAndVendors() {

    this._claimMasterForm.get("provId").setValue(this._claimMasterForm.get("provId").value.trim());

    this.ClearTreatingProvHelpers();
    this._vendorList = [];
    this._claimMasterForm.get("vendor").reset();

    if (this._claimMasterForm.get("provId").valid) {

      this._busyLoading = true;

      const params = new HttpParams().append('provid', this._claimMasterForm.get("provId").value);
      this._http.get(this._baseUrl + 'api/ClaimCapture/GetProvSpecAndNameAndVendors',
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"
          }),
          params
        }
      ).subscribe(
        (provInfo: ProviderAndVendorInfoModel) => {

          if (provInfo.provid != null && provInfo.provid != "") {

            this._currentClaim.provName = provInfo.lastName;
            this._currentClaim.provSpec = provInfo.specCode;
            this._currentClaim.provSpecDescr = provInfo.specCodeDescr;
            this._vendorList = provInfo.vendors;

            if (this._vendorList.length == 1) {
              this._claimMasterForm.get('vendor').setValue(this._vendorList[0].vendor);
            }
            else if (this._vendorList.length < 1) {
              let vend: VendoridsModel = new VendoridsModel();
              vend.vendor = this._claimMasterForm.get("provId").value;
              vend.displayItem = this._claimMasterForm.get("provId").value;
              this._vendorList.push(vend);
              this._claimMasterForm.get('vendor').setValue(this._vendorList[0].vendor);
            }

          }
          else {
            this._currentClaim.provName = this._claimMasterForm.get("provId").value;
            let vend: VendoridsModel = new VendoridsModel();
            vend.vendor = this._claimMasterForm.get("provId").value;
            vend.displayItem = this._claimMasterForm.get("provId").value;
            this._vendorList.push(vend);
            this._claimMasterForm.get('vendor').setValue(this._vendorList[0].vendor);
          }
          //this._claimMasterForm.get('vendor').markAsTouched();
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._busyLoading = false;
          this._errorModal = true;
          this._errorMessageHeading = "Provider validation failed";
          this._errorMessage = error.message;
        },
        () => {
          this._busyLoading = false;
        });
    }
  }

  ClearTreatingProvHelpers() {
    this._currentClaim.provName = "";
    this._currentClaim.provSpec = "";
    this._currentClaim.provSpecDescr = "";
  }

  ValidatePolicyNumber() {

    this._claimMasterForm.get("policyNo").setValue(this._claimMasterForm.get("policyNo").value.trim());

    this._currentClaim.familyOpt = "";
    this._familyList = [];
    this._currentClaim.depOpthruDt = "";
    this._claimMasterForm.get("dependant").reset();

    if (this._claimMasterForm.get("policyNo").valid) {

      this._busyLoading = true;

      this._membershipService.SearchEligibleMemberApi(this._claimMasterForm.get("policyNo").value, '', '', "1900/01/01", this._claimMasterForm.get("hpCode").value)
        .subscribe(
          (results: MemberEligibilityModel[]) => {

            results.forEach((el) => {
              if (el.hpcode == this._claimMasterForm.get("hpCode").value) { // to not include DRUM members (or any old hpcodes)
                this._familyList.push(el);
              }
            })

            if (this._familyList.length > 0 /*&& this._familyList[0].hpcode == this._appService.ClaimCaptureService.currentClaimHeader.hpCode*/) {

              this._currentClaim.familyOpt = this._familyList[0].opt;
              this.LoadServiceTypes();
              this._claimMasterForm.get('policyNo').setErrors(null);

              if (this._familyList.length == 1) {
                this._claimMasterForm.get('dependant').setValue(this._familyList[0].dependant);
                this._claimMasterForm.get('dependant').markAsTouched();
                this.CheckDependantThruDate();
              }

            }
            else {
              this._claimMasterForm.get('policyNo').setErrors({ 'incorrect': true });
            }

          },
          (error: HttpErrorResponse) => {
            console.log(error)
            this._claimMasterForm.get('policyNo').setErrors({ 'incorrect': true });
            this._busyLoading = false;
            this._errorModal = true;
            this._errorMessageHeading = "Policy number validation failed";
            this._errorMessage = "Please retype the policy number and click away to re-validate. " + error.message;
          },
          () => {
            this._busyLoading = false;
          }
        )
    }
  }

  CheckDependantThruDate() {
    if (this._familyList.length > 0 && this._claimMasterForm.get('dependant').value != null) {
      if (this._familyList.filter(x => x.dependant == this._claimMasterForm.get('dependant').value)[0].opthrudt != "N/A") {
        this._currentClaim.depOpthruDt = "Termination Date: " + this._familyList.filter(x => x.dependant == this._claimMasterForm.get('dependant').value)[0].opthrudt;
      }
    }
  }

  ClaimTypeChanged() {

    this._claimMasterForm.get('claimType').markAsTouched(); // To prevent error in console

    if (this._claimMasterForm.get('claimType').value == 'P' && !this._claimMasterForm.get('vendor').valid) {
      this._errorModal = true;
      this._errorMessageHeading = "Invalid";
      this._errorMessage = "Please select a valid Group/Payment Practice first."
      this._claimMasterForm.get('claimType').reset();

    } else if (this._claimMasterForm.get('claimType').value == 'M' && !this._claimMasterForm.get('policyNo').valid) {
      this._errorModal = true;
      this._errorMessageHeading = "Invalid";
      this._errorMessage = "Please enter a valid Policy Number first."
      this._claimMasterForm.get('claimType').reset();

    } else if (this._claimMasterForm.get('claimType').value == 'P' || this._claimMasterForm.get('claimType').value == 'M') {

      this._bankingPromptModal = true;
    }
  }

  TrimBankingDetailsValue(inputValue: string) {
    this._bankingDetailsForm.get(inputValue).setValue(this._bankingDetailsForm.get(inputValue).value.trim());
  }

  UpdateBankingDetailsNew() {

    this._bankingEditWarningModal = false;
    this._busyLoading = true;

    this.newBankingDetails.user = this._loginService.CurrentLoggedInUser.username;
    this.newBankingDetails.hpcode = this._claimMasterForm.get('hpCode').value;
    this.newBankingDetails.claimType = this._claimMasterForm.get('claimType').value;
    this.newBankingDetails.bankName = this._bankingDetailsForm.get('bankName').value;
    this.newBankingDetails.branchCode = this._bankingDetailsForm.get('branchCode').value;
    this.newBankingDetails.accountNo = this._bankingDetailsForm.get('accountNo').value;
    this.newBankingDetails.accType = this._bankingDetailsForm.get('accountType').value;

    if (this._claimMasterForm.get('claimType').value == 'M') {
      this.newBankingDetails.mainPolicyNo = this._claimMasterForm.get('policyNo').value;
    }
    else if (this._claimMasterForm.get('claimType').value == 'P') {
      this.newBankingDetails.provId = this._claimMasterForm.get('vendor').value;
    }

    this._http.post(this._baseUrl + 'api/ClaimCapture/UpdateBankingDetails', this.newBankingDetails, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
      })
    }).subscribe((result: ValidationResultsModel) => {

      if (result.valid == true) {
        this._successModal = true;
        this._successMessageHeading = "Success";
        if (this._claimMasterForm.get('claimType').value == 'M') {
          this._successMessage = "Member banking details updated successfully.";
        }
        else {
          this._successMessage = "A request has been sent to update the provider's banking details.";
        }
      } else {
        this._errorModal = true;
        this._errorMessageHeading = "Error";
        this._errorMessage = "Banking details update failed: " + result.message;
      }

    },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._busyLoading = false;
        this._errorModal = true;
        this._errorMessageHeading = "Error";
        this._errorMessage = "Banking details update failed: " + error.message;
      },
      () => {
        this._busyLoading = false;
      });
  }

  ClearClaim() {
    this._vendorList = [];
    this._familyList = [];
    this._currentClaim = new ClaimMasterModel();
    this._currentClaimDetail = new ClaimDetailModel();
    this.DisableUsedDiagFields(); // Do before initializing the form, otherwise enable doesnt work. Todo: check why disabled controls are not enabled when the form is re-intialized
    this._claimMasterForm.reset();
    this.ClaimMasterFormInit();
    this.ValidationFormInit();    
    
    //// Set hpcode if there is only one - moved this to ClaimMasterFormInit()
    //if (this.HpCodesList.length == 1) {
    //  this._claimMasterForm.get('hpCode').setValue(this.HpCodesList[0].code);
    //  this._claimMasterForm.get('hpCode').markAsTouched();
    //  this.LoadServiceTypes(); // load ServiceTypes for this hpcode
    //}
    this._clearClaimModal = false;    
  }

  LoadServiceTypes() {
        
    const params = new HttpParams()
      .set('hpCode', this._claimMasterForm.get("hpCode").value)
      .set('opt', this._currentClaim.familyOpt);

    this._http.get(this._baseUrl + 'api/ClaimCapture/GetPhCodesByHpCodeAndOpt',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        }),
        params
      }).
      subscribe((returnedList: [ServicetypesModel]) => {
        this.listOfServiceTypes = returnedList;
        if (this.listOfServiceTypes.length < 1) {
          this._errorModal = true;
          this._errorMessageHeading = "Missing setup - no Service Types configured for this Fund.";
          this._errorMessage = "0 Service Types loaded";
        }
      },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._errorModal = true;
          this._errorMessageHeading = "Failed to load setup, claims will not be captured correctly. Please refresh your browser and log in again.";
          this._errorMessage = Object.keys(error.error.errors)[0] + ": " + error.error.errors[Object.keys(error.error.errors)[0]];
        },
        () => { });
  }

  NewEditClaimDetailLine(claimLine: ClaimDetailModel) {

    if (!this._claimMasterForm.valid) {

      //console.log(this._claimMasterForm);
      this._claimMasterForm.markAllAsTouched(); // to display error messages
      this._errorModal = true;
      this._errorMessageHeading = "Incomplete Claim Header";
      this._errorMessage = "Please ensure that all non-optional Claim Header fields contains valid data.";

    } else {

      // Initialise for new and edit
      this._claimDetailForm.reset();
      this.ClaimDetailFormInit();

      // Prevent the accordion's error on the clrAccordionPanelOpen directive
      this._claimDetailForm.get('rejCode').markAsTouched();      
      this._claimDetailForm.get('tooth1').markAsTouched();
      this._claimDetailForm.get('tooth2').markAsTouched();
      this._claimDetailForm.get('tooth3').markAsTouched();
      this._claimDetailForm.get('tooth4').markAsTouched();
      this._claimDetailForm.get('tooth5').markAsTouched();
      this._claimDetailForm.get('tooth6').markAsTouched();
      this._claimDetailForm.get('tooth7').markAsTouched();
      this._claimDetailForm.get('tooth8').markAsTouched();
      this._claimDetailForm.get('modifier1').markAsTouched();
      this._claimDetailForm.get('modifier2').markAsTouched();
      this._claimDetailForm.get('modifier3').markAsTouched();
      this._claimDetailForm.get('modifier4').markAsTouched();
      this._claimDetailForm.get('note').markAsTouched();

      // Filter list of service types based on HPCODE and PROF/HOSP 
      this._listOfServiceTypesFiltered = this.listOfServiceTypes.filter(x => x.type == this._claimMasterForm.get('profHosp').value || x.type == "A");

      // Filter list of rejection codes based on HPCODE selected
      let claimLobCode = this.HpCodesList.filter(x => x.code == this._claimMasterForm.get('hpCode').value);
      this._rejectionCodeListFiltered = this._rejectionCodeList.filter(x => x.lobCode.trim() == claimLobCode[0].lobcode);


      if (claimLine == undefined || claimLine == null) { // New line

        // Initialise
        this._currentClaimDetail = new ClaimDetailModel();
        
      }
      else { // Existing line
         
        this._currentClaimDetail = Object.assign({}, claimLine); //Pass by value (not by reference) so that we don't change the original detail line if the user cancel the edit
               
        setTimeout(() => { // Timeout to prevent accordion dirty check console error
          this._claimDetailForm.get('procCode').setValue(this._currentClaimDetail.procCode);
          this._claimDetailForm.get('phCode').setValue(this._currentClaimDetail.phCode);
          this._claimDetailForm.get('svcDateFrom').setValue(this._currentClaimDetail.svcDateFrom);
          this._claimDetailForm.get('svcDateTo').setValue(this._currentClaimDetail.svcDateTo);
          this._claimDetailForm.get('billed').setValue(this._currentClaimDetail.billed);
          this._claimDetailForm.get('qty').setValue(this._currentClaimDetail.qty);
          this._claimDetailForm.get('diag').setValue(this._currentClaimDetail.diag);
          this._claimDetailForm.get('rejCode').setValue(this._currentClaimDetail.rejCode);
          this._claimDetailForm.get('tooth1').setValue(this._currentClaimDetail.tooth1);
          this._claimDetailForm.get('tooth2').setValue(this._currentClaimDetail.tooth2);
          this._claimDetailForm.get('tooth3').setValue(this._currentClaimDetail.tooth3);
          this._claimDetailForm.get('tooth4').setValue(this._currentClaimDetail.tooth4);
          this._claimDetailForm.get('tooth5').setValue(this._currentClaimDetail.tooth5);
          this._claimDetailForm.get('tooth6').setValue(this._currentClaimDetail.tooth6);
          this._claimDetailForm.get('tooth7').setValue(this._currentClaimDetail.tooth7);
          this._claimDetailForm.get('tooth8').setValue(this._currentClaimDetail.tooth8);
          this._claimDetailForm.get('modifier1').setValue(this._currentClaimDetail.modifier1);
          this._claimDetailForm.get('modifier2').setValue(this._currentClaimDetail.modifier2);
          this._claimDetailForm.get('modifier3').setValue(this._currentClaimDetail.modifier3);
          this._claimDetailForm.get('modifier4').setValue(this._currentClaimDetail.modifier4);
          this._claimDetailForm.get('note').setValue(this._currentClaimDetail.note);
        }, 0);        

        this._claimDetailForm.markAllAsTouched();        
      }

      // Show form
      this._claimDetailModal = true;
    }
  }

  CancelDetailModal() {

    this._currentClaimDetail = new ClaimDetailModel(); // clear
    this._cancelDetailModal = false;
    this._claimDetailModal = false;

  }

  SaveClaimDetailLine() {

    this._claimDetailForm.markAllAsTouched(); // to display error messages

    if (!this._claimDetailForm.valid) {

      this._errorModal = true;
      this._errorMessageHeading = "Incomplete Claim Details";
      this._errorMessage = "Please ensure that all non-optional Claim Detail fields contains valid data.";

    } else
    if (this._claimDetailForm.get('svcDateFrom').value > this._claimDetailForm.get('svcDateTo').value) {
            
      this._errorModal = true;
      this._errorMessageHeading = "Incorrect Claim Details";
      this._errorMessage = "Service From Date cannot be after service Service To Date";

    } else {

      this._currentClaimDetail.procCode = this._claimDetailForm.get('procCode').value;
      //this._currentClaimDetail.procCodeDesc = this._currentClaimDetail.procCodeDesc;
      this._currentClaimDetail.phCode = this._claimDetailForm.get('phCode').value;
      this._currentClaimDetail.phCodeDesc = this.ListOfServiceTypesFiltered.filter(x => x.phCode == this._claimDetailForm.get('phCode').value)[0].phDesc;
      this._currentClaimDetail.svcDateFrom = this._claimDetailForm.get('svcDateFrom').value;
      this._currentClaimDetail.svcDateTo = this._claimDetailForm.get('svcDateTo').value;
      this._currentClaimDetail.billed = this._claimDetailForm.get('billed').value;
      this._currentClaimDetail.qty = this._claimDetailForm.get('qty').value;
      this._currentClaimDetail.diag = this._claimDetailForm.get('diag').value;

      if (this._claimDetailForm.get('diag').value == this._currentClaim.diag1 && this._currentClaim.diag1 != "") { this._currentClaimDetail.diagDesc = this._currentClaim.diag1desc; }
      else if (this._claimDetailForm.get('diag').value == this._currentClaim.diag2 && this._currentClaim.diag2 != "") { this._currentClaimDetail.diagDesc = this._currentClaim.diag2desc; }
      else if (this._claimDetailForm.get('diag').value == this._currentClaim.diag3 && this._currentClaim.diag3 != "") { this._currentClaimDetail.diagDesc = this._currentClaim.diag3desc; }
      else if (this._claimDetailForm.get('diag').value == this._currentClaim.diag4 && this._currentClaim.diag4 != "") { this._currentClaimDetail.diagDesc = this._currentClaim.diag4desc; }
      //else if (this._claimDetailForm.get('diag').value == this._currentClaim.diag5 && this._currentClaim.diag5 != "") { this._currentClaimDetail.diagDesc = this._currentClaim.diag5desc; }

      this._currentClaimDetail.rejCode = this._claimDetailForm.get('rejCode').value;
      if (this._currentClaimDetail.rejCode.trim() != "") {
        this._currentClaimDetail.rejCodeDesc = this.RejectionCodeList.filter(x => x.code == this._claimDetailForm.get('rejCode').value)[0].reason;
      }
      this._currentClaimDetail.tooth1 = this._claimDetailForm.get('tooth1').value.trim();
      //this._currentClaimDetail.tooth1Desc = this._currentClaimDetail.tooth1Desc;
      this._currentClaimDetail.tooth2 = this._claimDetailForm.get('tooth2').value.trim();
      //this._currentClaimDetail.tooth2Desc = this._currentClaimDetail.tooth2Desc ;
      this._currentClaimDetail.tooth3 = this._claimDetailForm.get('tooth3').value.trim();
      //this._currentClaimDetail.tooth3Desc = this._currentClaimDetail.tooth3Desc;
      this._currentClaimDetail.tooth4 = this._claimDetailForm.get('tooth4').value.trim();
      //this._currentClaimDetail.tooth4Desc = this._currentClaimDetail.tooth4Desc;
      this._currentClaimDetail.tooth5 = this._claimDetailForm.get('tooth5').value.trim();
      //this._currentClaimDetail.tooth5Desc = this._currentClaimDetail.tooth5Desc;
      this._currentClaimDetail.tooth6 = this._claimDetailForm.get('tooth6').value.trim();
      //this._currentClaimDetail.tooth6Desc = this._currentClaimDetail.tooth6Desc ;
      this._currentClaimDetail.tooth7 = this._claimDetailForm.get('tooth7').value.trim();
      //this._currentClaimDetail.tooth7Desc = this._currentClaimDetail.tooth7Desc;
      this._currentClaimDetail.tooth8 = this._claimDetailForm.get('tooth8').value.trim();
      //this._currentClaimDetail.tooth8Desc = this._currentClaimDetail.tooth8Desc;
      this._currentClaimDetail.modifier1 = this._claimDetailForm.get('modifier1').value.trim();
      //this._currentClaimDetail.modifier1Desc = this._currentClaimDetail.modifier1Desc;
      this._currentClaimDetail.modifier2 = this._claimDetailForm.get('modifier2').value.trim();
      //this._currentClaimDetail.modifier2Desc = this._currentClaimDetail.modifier2Desc;
      this._currentClaimDetail.modifier3 = this._claimDetailForm.get('modifier3').value.trim();
      //this._currentClaimDetail.modifier3Desc = this._currentClaimDetail.modifier3Desc;
      this._currentClaimDetail.modifier4 = this._claimDetailForm.get('modifier4').value.trim();
      //this._currentClaimDetail.modifier4Desc = this._currentClaimDetail.modifier4Desc;
      this._currentClaimDetail.note = this._claimDetailForm.get('note').value.trim();

      // Create toothNumString      
      this._currentClaimDetail.toothNumString = "";
      if (this._currentClaimDetail.tooth1 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth1 + ", "; }
      if (this._currentClaimDetail.tooth2 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth2 + ", "; }
      if (this._currentClaimDetail.tooth3 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth3 + ", "; }
      if (this._currentClaimDetail.tooth4 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth4 + ", "; }
      if (this._currentClaimDetail.tooth5 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth5 + ", "; }
      if (this._currentClaimDetail.tooth6 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth6 + ", "; }
      if (this._currentClaimDetail.tooth7 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth7 + ", "; }
      if (this._currentClaimDetail.tooth8 != "") { this._currentClaimDetail.toothNumString += this._currentClaimDetail.tooth8 + ", "; }
      // Check if last character is a comma then remove the last comma
      if (this._currentClaimDetail.toothNumString.substring(this._currentClaimDetail.toothNumString.length - 2, this._currentClaimDetail.toothNumString.length) == ", ") {
        this._currentClaimDetail.toothNumString = this._currentClaimDetail.toothNumString.substring(0, this._currentClaimDetail.toothNumString.length - 2)
      }

      // Create modCodesString
      this._currentClaimDetail.modCodesString = "";
      if (this._currentClaimDetail.modifier1 != "") { this._currentClaimDetail.modCodesString += this._currentClaimDetail.modifier1 + ", "; }
      if (this._currentClaimDetail.modifier2 != "") { this._currentClaimDetail.modCodesString += this._currentClaimDetail.modifier2 + ", "; }
      if (this._currentClaimDetail.modifier3 != "") { this._currentClaimDetail.modCodesString += this._currentClaimDetail.modifier3 + ", "; }
      if (this._currentClaimDetail.modifier4 != "") { this._currentClaimDetail.modCodesString += this._currentClaimDetail.modifier4 + ", "; }
      // Check if last character is a comma then remove the last comma
      if (this._currentClaimDetail.modCodesString.substring(this._currentClaimDetail.modCodesString.length - 2, this._currentClaimDetail.modCodesString.length) == ", ") {
        this._currentClaimDetail.modCodesString = this._currentClaimDetail.modCodesString.substring(0, this._currentClaimDetail.modCodesString.length - 2)
      }
      
      if (this._currentClaimDetail.arrayId == '') {

        // Add detail to master if it is a new line
        this._currentClaimDetail.arrayId = Date.now().toString();
        this._currentClaim.claimDetails.push(this._currentClaimDetail);

      } else {

        // Replace detail line in _currentClaim.claimDetails with _currentClaimDetail        
        let indexToUpdate = this._currentClaim.claimDetails.findIndex(item => item.arrayId === this._currentClaimDetail.arrayId);
        this._currentClaim.claimDetails[indexToUpdate] = this._currentClaimDetail;

        // some angular libraries require breaking the array reference
        // to pick up the update in the array and trigger change detection.
        // In that case, you can do following
        this._currentClaim.claimDetails = Object.assign([], this._currentClaim.claimDetails); // Break reference to _currentClaimDetail (just to be safe)

        this._currentClaimDetail = new ClaimDetailModel(); // clear         
      }

      // Sort by arrayId (date created) and do line numbers
      this._currentClaim.claimDetails.sort((a, b) => a.arrayId.localeCompare(b.arrayId));
      let lineCounter: number = 1
      this._currentClaim.claimDetails.forEach((el: ClaimDetailModel) => {
        el.lineNo = lineCounter;
        lineCounter++;
      })

      this.DisableUsedDiagFields();

      // Close detail form
      this._claimDetailModal = false;
    }
  }

  RemoveClaimDetailLine(claimLine: ClaimDetailModel) {

    // Remove line from claim details array
    let index = this._currentClaim.claimDetails.findIndex((element) => element["arrayId"] == claimLine.arrayId);
    this._currentClaim.claimDetails.splice(index, 1);

    // Sort by arrayId (date created) and do line numbers
    this._currentClaim.claimDetails.sort((a, b) => a.arrayId.localeCompare(b.arrayId));
    let lineCounter: number = 1
    this._currentClaim.claimDetails.forEach((el: ClaimDetailModel) => {
      el.lineNo = lineCounter;
      lineCounter++;
    })

    this.DisableUsedDiagFields();
  }

  DisableUsedDiagFields() {

    // First Enable All
    this._claimMasterForm.get('diag1').enable();
    this._claimMasterForm.get('diag2').enable();
    this._claimMasterForm.get('diag3').enable();
    this._claimMasterForm.get('diag4').enable();
    //this._claimMasterForm.get('diag5').enable();

    // Loop through detail lines and disable diags on ClaimMaster form if they are used on a detail line
    this._currentClaim.claimDetails.forEach((el: ClaimDetailModel) => {
      if (this._claimMasterForm.get('diag1').value == el.diag) { this._claimMasterForm.get('diag1').disable() }
      if (this._claimMasterForm.get('diag2').value == el.diag) { this._claimMasterForm.get('diag2').disable() }
      if (this._claimMasterForm.get('diag3').value == el.diag) { this._claimMasterForm.get('diag3').disable() }
      if (this._claimMasterForm.get('diag4').value == el.diag) { this._claimMasterForm.get('diag4').disable() }
      //if (this._claimMasterForm.get('diag5').value == el.diag) { this._claimMasterForm.get('diag5').disable() }
    })
  }

  ValidateProcCode() {

    this._claimDetailForm.get("procCode").setValue(this._claimDetailForm.get("procCode").value.trim());

    if (this._claimDetailForm.get("procCode").valid) {

      this._busyLoading = true;

      const params = new HttpParams().append('procCode', this._claimDetailForm.get("procCode").value);
      this._http.get(this._baseUrl + 'api/ClaimCapture/ValidateTarrifCode',
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"
          }),
          params
        }
      ).subscribe(
        (result: ProcCodeModel) => {
          if (result.procCodeDesc == null || result.procCodeDesc == "") {
            this._currentClaimDetail.procCodeDesc = "Unknown code";
            this._currentClaimDetail.procCodePadded = this._claimDetailForm.get("procCode").value;
          } else {            
            this._currentClaimDetail.procCodeDesc = result.procCodeDesc;
            this._currentClaimDetail.procCodePadded = result.procCode;
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._busyLoading = false;
          this._errorModal = true;
          this._errorMessageHeading = "Failed get tarrif code description.";
          this._errorMessage = Object.keys(error.error.errors)[0] + ": " + error.error.errors[Object.keys(error.error.errors)[0]];
        },
      () => { this._busyLoading = false; });
    }
  }

  SetServiceToDate() {

    if (this._claimDetailForm.get("svcDateFrom").valid) {

      if (this._claimDetailForm.get("svcDateTo").value == "") {
        this._claimDetailForm.get("svcDateTo").setValue(this._claimDetailForm.get("svcDateFrom").value);
        this._claimDetailForm.get("svcDateTo").markAsTouched();
      }

    }
  }

  ClearRejection() {
    this._claimDetailForm.get("rejCode").reset();
    this._claimDetailForm.get("rejCode").setValue("");
  }

  GetModifierCodeDescription(controlName: string) {

    this._claimDetailForm.get(controlName).setValue(this._claimDetailForm.get(controlName).value.trim());

    if (this._claimDetailForm.get(controlName).valid) {

      let modCodes: ModifierCodeModel[] = this._modifierCodesList.filter(x => x.modCode == this._claimDetailForm.get(controlName).value);

      if (modCodes.length > 0) {

        if (controlName == 'modifier1') {
          this._currentClaimDetail.modifier1 = modCodes[0].modCode;
          this._currentClaimDetail.modifier1Desc = modCodes[0].modCodeDesc;
        } else if (controlName == 'modifier2') {
          this._currentClaimDetail.modifier2 = modCodes[0].modCode;
          this._currentClaimDetail.modifier2Desc = modCodes[0].modCodeDesc;
        } else if (controlName == 'modifier3') {
          this._currentClaimDetail.modifier3 = modCodes[0].modCode;
          this._currentClaimDetail.modifier3Desc = modCodes[0].modCodeDesc;
        } else if (controlName == 'modifier4') {
          this._currentClaimDetail.modifier4 = modCodes[0].modCode;
          this._currentClaimDetail.modifier4Desc = modCodes[0].modCodeDesc;
        }
      } else {
        if (controlName == 'modifier1') {
          this._currentClaimDetail.modifier1 = this._claimDetailForm.get(controlName).value;
          this._currentClaimDetail.modifier1Desc = this._claimDetailForm.get(controlName).value;
        } else if (controlName == 'modifier2') {
          this._currentClaimDetail.modifier2 = this._claimDetailForm.get(controlName).value;
          this._currentClaimDetail.modifier2Desc = this._claimDetailForm.get(controlName).value;
        } else if (controlName == 'modifier3') {
          this._currentClaimDetail.modifier3 = this._claimDetailForm.get(controlName).value;
          this._currentClaimDetail.modifier3Desc = this._claimDetailForm.get(controlName).value;
        } else if (controlName == 'modifier4') {
          this._currentClaimDetail.modifier4 = this._claimDetailForm.get(controlName).value;
          this._currentClaimDetail.modifier4Desc = this._claimDetailForm.get(controlName).value;
        }
      }

    }
  }

  PrepareClaimSubmission() {

    this._claimMasterForm.markAllAsTouched(); // to display error messages

    this._currentClaim.dateReceived = this._claimMasterForm.get('dateReceived').value; // Need to do this now for dates validation check

    if (!this._claimMasterForm.valid) {

      this._errorModal = true;
      this._errorMessageHeading = "Incomplete Claim Header";
      this._errorMessage = "Please ensure that all non-optional Claim Header fields contains valid data.";

    } else if (this._currentClaim.claimDetails.length < 1) { // check that there are detail lines

      this._errorModal = true;
      this._errorMessageHeading = "Incomplete Claim";
      this._errorMessage = "There are no detail lines captured for this claim. At least one is required.";

    } else if (this._currentClaim.CheckValidDates().length > 0) { // check that service dates on detail lines is before claim receive date

      this._errorModal = true;
      this._errorMessageHeading = "Invalid dates";
      this._errorMessage = "Claim detail line(s) " + this._currentClaim.CheckValidDates() + " contains a service date(s) that is after the Date Received on the claim header.";

    } else {

      this._currentClaim.hpCode = this._claimMasterForm.get('hpCode').value;
      this._currentClaim.hpCodeDesc = this.HpCodesList.filter(x => x.code == this._currentClaim.hpCode)[0].name;
      this._currentClaim.provHosp = this._claimMasterForm.get('profHosp').value;
      this._currentClaim.provHospDesc = this._profHospSelect.filter(x => x[0] == this._currentClaim.provHosp)[0][1];
      this._currentClaim.provId = this._claimMasterForm.get('provId').value;
      this._currentClaim.vendor = this._claimMasterForm.get('vendor').value;
      this._currentClaim.vendorName = this._vendorList.filter(x => x.vendor == this._currentClaim.vendor)[0].displayItem;
      this._currentClaim.refProvId = this._claimMasterForm.get('refProvId').value;
      this._currentClaim.provClaim = this._claimMasterForm.get('provClaim').value;
      this._currentClaim.policyNo = this._claimMasterForm.get('policyNo').value;
      this._currentClaim.dependant = this._claimMasterForm.get('dependant').value;
      this._currentClaim.membId = this._familyList.filter(x => x.dependant == this._currentClaim.dependant)[0].membid;
      this._currentClaim.dependantFirstnm = this._familyList.filter(x => x.dependant == this._currentClaim.dependant)[0].firstnm;
      this._currentClaim.dependantLastnm = this._familyList.filter(x => x.dependant == this._currentClaim.dependant)[0].lastnm;
      this._currentClaim.dateReceived = this._claimMasterForm.get('dateReceived').value;
      this._currentClaim.claimType = this._claimMasterForm.get('claimType').value;
      this._currentClaim.claimTypeDesc = this._claimTypeSelect.filter(x => x[0] == this._currentClaim.claimType)[0][1];
      this._currentClaim.authNo = this._claimMasterForm.get('authNo').value;
      this._confirmationModal = true;
    }

  }

  SubmitClaim() {

    this._currentClaim.userName = this._loginService.CurrentLoggedInUser.username;

    if (!this._validationForm.get('checked').valid) {

      this._validationForm.markAllAsTouched(); // to display error message
      this._errorModal = true;
      this._errorMessageHeading = "Incomplete Validation";
      this._errorMessage = "Please check the box to confirm that the details captured are correct.";

    }
    else {

      this._currentClaim.clientCreateDate = this._datePipe.transform(new Date(), 'yyyy/MM/dd HH:mm:ss');
      this._busyLoading = true;

      this._http.post(this._baseUrl + 'api/ClaimCapture/SubmitClaim', this._currentClaim,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"
          })
        }
      ).subscribe(
        (result: ClaimMasterModel) => {            
          this._successMessageHeading = "Claim submitted successfully";
          this._successMessage = "Claim submission reference number: " + result.rec1Id;
          this._successModal = true;
          this._confirmationModal = false;
          this.ClearClaim();
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._busyLoading = false;
          this._errorModal = true;
          this._errorMessageHeading = "Failed to create claim.";          
          this._errorMessage = Object.keys(error.error.errors)[0] + ": " + error.error.errors[Object.keys(error.error.errors)[0]][0];
          this._currentClaim.exception = Object.keys(error.error.errors)[0] + ": " + error.error.errors[Object.keys(error.error.errors)[0]][0] + " " + error.error.errors[Object.keys(error.error.errors)[0]][1];
        },
      () => { this._busyLoading = false; });
    }
  }

  //Exposing of variables
  get OpenWizard() {
    return this._wizardOpen;
  }
  set OpenWizard(isValid) {
    this._wizardOpen = isValid;
  }
  public get token(): string {
    return localStorage.getItem("jwt");
  }
  public set token(value: string) {
    localStorage.setItem("jwt", value);
  }

  PopulateDefualtAdjCodes() {

    this.rejectionReasons.forEach((el) => {
      this.codeLine = new AdjustcodesModel();
      this.codeLine.code = el.code;
      this.codeLine.desc = el.reason;
      this.codeLine.notes = el.code.trim() + '-' + el.reason;
      this.defualtCodesList.push(this.codeLine);
    });

  }

  AddDiagCode() {
    this.findSameDiagCode = false;
    let p = this;
    p.diagnosticCodesList.forEach((el) => {
      if (el.DIAG === p.diagnosticCodes.DIAG) {
        p.findSameDiagCode = true;
        if (el.DIAGREF === p.diagnosticCodes.DIAGREF) {
          this.findSameDiagCode = false;
        }
      }
    });
    if (p.findSameDiagCode === false) {
      for (var i = 0; i < p.diagnosticCodesList.length; i++) {
        if (p.diagnosticCodesList[i].DIAGREF === p.diagnosticCodes.DIAGREF) {
          p.diagnosticCodesList[i].DIAG = p.diagnosticCodes.DIAG;
          p.diagnosticCodesList[i].DIAGREF = p.diagnosticCodes.DIAGREF;
          p.findSameDiagRef = true;
          break;
        } else {
          p.findSameDiagRef = false;
        }
      }
      if ((p.diagnosticCodesList.length === 0) || (p.findSameDiagRef === false)) {
        p.diagnosticCodesList.push(this.diagnosticCodes);
      }
      p.diagnosticCodes = new Recordtype5Model();
    }
  }

  NewClaimMaster() {
    this.currentClaimHeader = new Recordtype1Model();
    this.FindDefaultPlaceOfService();
    this.PHCodeSelection();
    this.ClaimTypeSelect()
    this.btnClick = true;
    this.enableWizardNext = false;
    this.FindDeFaultHPCode();
  }

  // Main main menu links are calling this method, see app.component.html - BE CAREFUL!
  ValidateWizard() {
    this.currentClaimHeader.lobCode = this._loginService.CurrentLoggedInUser.lobcode;
    this.currentClaimDetail.hpCode = this._loginService.CurrentLoggedInUser.firstname;
    this.PlacesOfService();
    this.GetRejectionReasons();
  }

  CancelClaimMaster() {
    this.btnClick = false;
  }

  SaveClaimMaster() {
    this.btnClick = false;
    this.enableWizardNext = true;
  }

  PlacesOfService() {
    const webApiPlaceOfService: Observable<Object> = this._http.get(
      this._baseUrl + 'api/ClaimCapture/GetPlacesOfService',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    );
    webApiPlaceOfService.subscribe(
      (returnedPlcSvc: [PlaceOfServiceModel]) => {
        this.plcSvcList = returnedPlcSvc;
      },
      (error) => {
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => {
        this.FindDefaultPlaceOfService();
        this.HpCodes();
      }
    );
  }

  async HpCodes() {  // Don't change, used by old claim capture as well
    let x: RejectionCodesModel;
    x = new RejectionCodesModel();
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    await
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).toPromise()
        .then((returnedList: [HealthplanModel]) => {
          this.HpCodesList = returnedList;          

          //if (this.HpCodesList.length == 1) { mover this to ClaimMasterFormInit()
          //  this._claimMasterForm.get('hpCode').setValue(this.HpCodesList[0].code);
          //  this._claimMasterForm.get('hpCode').markAsTouched();
          //  this.LoadServiceTypes();
          //}

          this.ClaimMasterFormInit();
      },
        (error) => {
          console.log(error);
          //if (error.status == '401') {
          //  this._router.navigateByUrl('/login');}
        }//,
        //() => { });
      );
  }

  FindDefaultPlaceOfService() {
    this.plcSvcList.forEach(
      (el) => {
        if (el.default_YN === true) {
          this.currentClaimHeader.PLACE = el.code;
        }
      });
  }

  FindDeFaultHPCode() {
    this.currentClaimHeader.hpCode = this.HpCodesList[0].code;
    this.currentClaimDetail.hpCode = this.HpCodesList[0].code;
  }

  PHCodeSelection() {
    this.currentClaimHeader.PHCODE = this.phSelect[0].charAt(0);
  }

  ClaimTypeSelect() {
    this.currentClaimHeader.CLAIMTYPE = this.ctSelect[0].charAt(0);
  }

  //DataBaseCheckEzcapProv(): string { // Jaco comment out 2024-03-04
  //  this._busy = true;
  //  this._http.post(this._baseUrl +
  //    'api/ClaimCapture/ValidateEzcapProv', this.currentClaimHeader,
  //    {
  //      headers: new HttpHeaders({
  //        "Authorization": "Bearer " + this.token,
  //        "Content-Type": "application/json",
  //        "Cache-Control": "no-store",
  //        "Pragma": "no-cache"

  //      })
  //    }
  //  ).subscribe((returnedValid) => {

  //    let v: any;
  //    v = returnedValid;
  //    this.dataBaseMessageProv = v.message;
  //  },
  //    (error) => {
  //      console.log(error);
  //      //if (error.status == '401') {
  //      //  this._router.navigateByUrl('/login');
  //      //}
  //    },
  //    () => {
  //      this._busy = false;
  //      this.LoadVendorIds();
  //    }
  //  );
  //  return this.dataBaseMessageProv;
  //}

  DataBaseCheckEzcapProv() { //Jaco 2024-03-04

    return this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateEzcapProv', this.currentClaimHeader,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    )
  }

  LoadVendorIds() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/GetVendorIDs', this.currentClaimHeader,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((returneVendorIds: [VendoridsModel]) => {
      this.vendorIds = returneVendorIds;
    },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => {

        this._busy = false;
      });
  }

  //DataBaseCheckEzcapMemb(): string { //Jaco comment out 2024-01-05
  //  this._busy = true;

  //  let x: TestClaimHeaderModel = new TestClaimHeaderModel();
  //  x.membNum = this.currentClaimHeader.EZCAPMEMB;
  //  x.hpCode = this.currentClaimHeader.hpCode;
  //  this._http.post(this._baseUrl +
  //    'api/ClaimCapture/ValidateEzcapMemb', x,
  //    {
  //      headers: new HttpHeaders({
  //        "Authorization": "Bearer " + this.token,
  //        "Content-Type": "application/json",
  //        "Cache-Control": "no-store",
  //        "Pragma": "no-cache"

  //      })
  //    }
  //  ).subscribe((returnedValid) => {
  //    this.dataBaseMessageMemb = '';
  //    this.dataBaseMessageMemb = 'Searching...';
  //    this.tempMesgHolder = returnedValid;
  //    this.dataBaseMessageMemb = this.tempMesgHolder.message;
  //  },
  //    (error) => {
  //      console.log(error);
  //      //if (error.status == '401') {
  //      //  this._router.navigateByUrl('/login');
  //      //}
  //    },
  //    () => {
  //      this._busy = false;
  //    });
  //  return this.dataBaseMessageMemb;
  //}

  //DataBaseCheckEzcapDepMemb(): string { //Jaco comment out 2024-01-05
  //  this._busy = true;
  //  let x: TestClaimHeaderModel = new TestClaimHeaderModel();
  //  x.membNum = this.currentClaimHeader.EZCAPMEMB;
  //  x.hpCode = this.currentClaimHeader.hpCode;
  //  this._http.post(this._baseUrl +
  //    'api/ClaimCapture/ValidateEzcapDepCode', x,
  //    {
  //      headers: new HttpHeaders({
  //        "Authorization": "Bearer " + this.token,
  //        "Content-Type": "application/json",
  //        "Cache-Control": "no-store",
  //        "Pragma": "no-cache"

  //      })
  //    }
  //  ).subscribe((returnedValid) => {
  //    this.dataBaseMessageDepMemb = '';
  //    let v: any;
  //    v = returnedValid;
  //    this.dataBaseMessageDepMemb = v.message;
  //  },
  //    (error) => {
  //      console.log(error);
  //      //if (error.status == '401') {
  //      //  this._router.navigateByUrl('/login');
  //      //}
  //    },
  //    () => {
  //      this._busy = false;
  //    });
  //  return this.dataBaseMessageDepMemb;
  //}

  DataBaseCheckRefProv() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateRefPRov', this.currentClaimHeader,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((returnedValid) => {
      let v: any;
      v = returnedValid;
      this.dataBaseMessageRefProv = v.message;
    },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => {
        this._busy = false;
      });
    return this.dataBaseMessageRefProv;
  }

  DataBaseChecEzcapAuth(): string {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateEzcapAuth', this.currentClaimHeader,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((returnedValid) => {
      let v: any;
      v = returnedValid;
      this.dataBaseMessageAuth = v.message;
    },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageAuth;
  }

  DataBaseCheckProccode() {
    this._busy = true;
    this.currentClaimDetail.DIAGREF = 1;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateProccode',
      this.currentClaimDetail,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.currentClaimDetail.PROCCODE_DESC = v.message;
      },
      (error) => {
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
        console.log(error);
      },
      () => { this._busy = false; });
  }

  ClickClaimDetailDone() {
    this.enableWizardNext = true;
  }

  FindDefualtPHCODE() {
    this.listOfServiceTypes.forEach(
      (el) => {
        if (el.phCode === 'P') {
          this.currentClaimDetail.PHCODE = el.phCode;
        }
      });
  }

  LoadPhCodes() {
    const webApiServiceType: Observable<Object> =
      this._http.post(this._baseUrl +
        'api/ClaimCapture/GetPhCodes', this.currentClaimHeader,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        });
    webApiServiceType.subscribe(
      (recievedPhCodes: [ServicetypesModel]) => {
        this.listOfServiceTypes = recievedPhCodes;
        //console.log(this.listOfServiceTypes)
      });
  }

  DataBaseCheckModCode1() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateModCodes',
      this.modCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMsgMod1 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMsgMod1
  }

  DataBaseCheckModCode2() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateModCodes',
      this.modCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMsgMod2 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMsgMod2;
  }

  DataBaseCheckModCode3() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateModCodes',
      this.modCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMsgMod3 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMsgMod3;
  }

  DataBaseCheckModCode4() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateModCodes',
      this.modCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMsgMod4 = v.message
      },
      (error) => {
        console.log(error); if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMsgMod4;
  }

  DataBaseCheckTooth1() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth1 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth1;
  }

  DataBaseCheckTooth2() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth2 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth2;
  }

  DataBaseCheckTooth3() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth3 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth3
  }

  DataBaseCheckTooth4() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth4 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth4;
  }

  DataBaseCheckTooth5() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth5 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth5;
  }

  DataBaseCheckTooth6() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth6 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth6;
  }

  DataBaseCheckTooth7() {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth7 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth7;
  }

  DataBaseCheckTooth8() {
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateToothNum',
      this.toothNum,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageTooth8 = v.message;
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; });
    return this.dataBaseMessageTooth8;
  }

  CalculateBilledAmount() {
    this.billedAmount = 0;
    this.claimDetailList.forEach((el) => {
      this.billedAmount = this.billedAmount + parseFloat(el.BILLED);
    });
    this.billedAmount = parseFloat(this.billedAmount.toFixed(2));
  }

  StoreClaimDetail(action: string) {
    if (action === "new") {
      this.currentClaimDetail.Rec2IDArr = this.claimDetailList.length + 1;
      this.claimDetailList.push(this.currentClaimDetail);
      this.CalculateBilledAmount();
      this.currentClaimDetail = new Recordtype2Model();
    } else {
      this.claimDetailList.forEach((el) => {
        if (this.currentClaimDetail.Rec2IDArr === el.Rec2IDArr) {
          el.PHCODE = this.currentClaimDetail.PHCODE;
          el.PROCCODE = this.currentClaimDetail.PROCCODE;
          el.FROMDATE = this.currentClaimDetail.FROMDATE;
          el.TODATE = this.currentClaimDetail.TODATE;
          el.BILLED = this.currentClaimDetail.BILLED;
          el.QTY = this.currentClaimDetail.QTY;
          el.DIAGREF = this.currentClaimDetail.DIAGREF;
          el.MODCODE = this.currentClaimDetail.MODCODE;
          el.MODCODE2 = this.currentClaimDetail.MODCODE2;
          el.MODCODE3 = this.currentClaimDetail.MODCODE3;
          el.MODCODE4 = this.currentClaimDetail.MODCODE4;
          el.CLINICALCODE1 = this.currentClaimDetail.CLINICALCODE1;
          el.CLINICALCODE2 = this.currentClaimDetail.CLINICALCODE2;
          el.CLINICALCODE3 = this.currentClaimDetail.CLINICALCODE3;
          el.CLINICALCODE4 = this.currentClaimDetail.CLINICALCODE4;
          el.CLINICALCODE5 = this.currentClaimDetail.CLINICALCODE5;
          el.CLINICALCODE6 = this.currentClaimDetail.CLINICALCODE6;
          el.CLINICALCODE7 = this.currentClaimDetail.CLINICALCODE7;
          el.CLINICALCODE8 = this.currentClaimDetail.CLINICALCODE8;
          el.toothNumString = this.currentClaimDetail.toothNumString;
          el.ModCodesString = this.currentClaimDetail.ModCodesString;
          this.CalculateBilledAmount();
        }
      });
    }
  }

  CancelCLaimDetail() {
    this.btnClick = false;
  }

  NewClaimDetail() {
    this.currentClaimDetail = new Recordtype2Model();
  }

  DataBaseDiagCode1Check(): string {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateDiagref',
      this.diagnosticCodes,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageDiagC1 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => {
        this._busy = false;
      });
    return this.dataBaseMessageDiagC1
  }

  DataBaseDiagCode2Check(): string {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateDiagref',
      this.diagnosticCodes,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageDiagC2 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMessageDiagC2
  }

  DataBaseDiagCode3Check(): string {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateDiagref', this.diagnosticCodes,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageDiagC3 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMessageDiagC3
  }

  DataBaseDiagCode4Check(): string {
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/ValidateDiagref',
      this.diagnosticCodes,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.dataBaseMessageDiagC4 = v.message
      },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => { this._busy = false; }
    );
    return this.dataBaseMessageDiagC4
  }

  WizardIncPageCount() {
    this.pageCount++;
    if (this.pageCount === 2) {
      this.GetStalePeriod();
    }
    if (this.pageCount === 3) {
      if (this.diagnosticCodesList.length > 0) {
        this.enableWizardNext = true;
      } else {
        this.enableWizardNext = false;
      }
    }
    if (this.pageCount === 4) {
      this.enableWizardNext = false;
    }
    if (this.pageCount === 5) {
      this.enableWizardNext = false;
    }
    if (this.pageCount === 6) {
      if (this.fileCreated === true) {
        this.currentClaimHeader.Rec0ID = this.SavedIdRec1;
        this.LogObject();
        this.SaveHeader();
      } else {
        //this.fileCreated = true;
        this.SaveRec0();
        this._disableCancel = true;
      }
    }
  }

  WizardDecPageCount() {
    this.pageCount--;
    if (this.pageCount === 3) {
      this.enableWizardNext = true;
    }
    if (this.pageCount === 2) {
      this.enableWizardNext = true;
    }
    if (this.pageCount === 4) {
      if (this.claimDetailList.length > 0) {
        this.enableWizardNext = true;
      } else {
        this.enableWizardNext = false;
      }
    }
    if (this.pageCount === 5) {
      this.enableWizardNext = false;
      console.log("CLAIM DETAIL", this.claimDetailList);
    }
  }


  SaveRec0() {
    this.LogObject();
    this._busy = true;
    let data = { claimHeader: this.currentClaimHeader, claimDetail: this.claimDetailList };
    this._http.get(this._baseUrl +
      'api/ClaimCapture/SaveRecords',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage: number) => {
        let v: any;
        v = RecievedMessage;
        this.SavedIdRec1 = v;
        this.fileid = this.SavedIdRec1;
        this.currentClaimHeader.Rec0ID = this.SavedIdRec1;

      },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {

        this._busy = false;
        this.SaveHeader();
      }
    );
  }

  SaveDetail() {
    this._busy = true;
    this.DetailDataCheck();
    this._http.post(this._baseUrl +
      'api/ClaimCapture/SaveFinalDetail', this.claimDetailList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage: Recordtype2Model[]) => {
        let v: Recordtype2Model[] = [];
        v = RecievedMessage;
        this.claimDetailList = v;
        let count: number = 0;
        this.claimDetailList.forEach((el) => {
          count++;
          el.rec2IDArr = count;
        });
        this.claimDetailList.forEach((el) => {
          this.RejectList.forEach((re) => {
            if (el.rec2IDArr === re.Rec2IDArr) {
              re.Rec2IDdb = el.rec2IDDb;
            }
          });
          this.notesLists.forEach((le) => {
            if (el.rec2IDArr === le.Rec2ArrID) {
              le.Rec2ID = el.rec2IDDb;
            }
          });
        });
      },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this.SaveRecord5();
      }
    );
  }

  SaveHeader() {
    this._busy = true;
    this.HeaderDataCheck();

    this._http.post(this._baseUrl +
      'api/ClaimCapture/SaveFinalHeader', this.currentClaimHeader,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (RecievedMessage) => {
        let v: any;
        v = RecievedMessage;
        this.SavedIdRec2 = v.message;
        this.rec1Id = this.SavedIdRec2;
        this.claimDetailList.forEach((el) => {
          el.Rec1ID = parseInt(this.SavedIdRec2.toString());
        });
        this.diagnosticCodesList.forEach((el) => {
          el.FieldID = parseInt(this.SavedIdRec2.toString());
        });

      },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this.SaveDetail();
        this._busy = false;
      });
  }

  HeaderDataCheck() {
    this.currentClaimHeader.createBy = this._loginService.CurrentLoggedInUser.username;
    this.currentClaimHeader.TYPE = 1;
    if (this.currentClaimHeader.PHCODE == "" || this.currentClaimHeader.PHCODE == null || this.currentClaimHeader.PHCODE == undefined) {
      this.currentClaimHeader.PHCODE = "";
    } else {
      this.currentClaimHeader.PHCODE = this.currentClaimHeader.PHCODE.toString();
    }
    if (this.currentClaimHeader.PROVCLAIM == "" || this.currentClaimHeader.PROVCLAIM == null || this.currentClaimHeader.PROVCLAIM == undefined) {
      this.currentClaimHeader.PROVCLAIM = "";
    } else {
      this.currentClaimHeader.PROVCLAIM = this.currentClaimHeader.PROVCLAIM.toString();
    }
    if (this.currentClaimHeader.EZCAPAUTH == "" || this.currentClaimHeader.EZCAPAUTH == null || this.currentClaimHeader.EZCAPAUTH == undefined) {
      this.currentClaimHeader.EZCAPAUTH = "";
    } else {
      this.currentClaimHeader.EZCAPAUTH = this.currentClaimHeader.EZCAPAUTH.toString();
    }
    if (this.currentClaimHeader.EZCAPPROV == "" || this.currentClaimHeader.EZCAPPROV == null || this.currentClaimHeader.EZCAPPROV == undefined) {
      this.currentClaimHeader.EZCAPPROV = "";
    } else {
      this.currentClaimHeader.EZCAPPROV = this.currentClaimHeader.EZCAPPROV.toString();
    }
    if (this.currentClaimHeader.EZCAPMEMB == "" || this.currentClaimHeader.EZCAPMEMB == null || this.currentClaimHeader.EZCAPMEMB == undefined) {
      this.currentClaimHeader.EZCAPMEMB = "";
    } else {
      this.currentClaimHeader.EZCAPMEMB = this.currentClaimHeader.EZCAPMEMB.toString();
    }
    if (this.currentClaimHeader.PLACE == "" || this.currentClaimHeader.PLACE == null || this.currentClaimHeader.PLACE == undefined) {
      this.currentClaimHeader.PLACE = "11";
    } else {
      this.currentClaimHeader.PLACE = this.currentClaimHeader.PLACE.toString();
    }
    if (this.currentClaimHeader.OUTCOME == "" || this.currentClaimHeader.OUTCOME == null || this.currentClaimHeader.OUTCOME == undefined) {
      this.currentClaimHeader.OUTCOME = "";
    } else {
      this.currentClaimHeader.OUTCOME = this.currentClaimHeader.OUTCOME.toString();
    }
    if (this.currentClaimHeader.EDIREF == "" || this.currentClaimHeader.EDIREF == null || this.currentClaimHeader.EDIREF == undefined) {
      this.currentClaimHeader.EDIREF = "";
    } else {
      this.currentClaimHeader.EDIREF = this.currentClaimHeader.EDIREF.toString();
    }
    if (this.currentClaimHeader.CASENUM == "" || this.currentClaimHeader.CASENUM == null || this.currentClaimHeader.CASENUM == undefined) {
      this.currentClaimHeader.CASENUM = "";
    } else {
      this.currentClaimHeader.CASENUM = this.currentClaimHeader.CASENUM.toString();
    }
    if (this.currentClaimHeader.UNITS == null || this.currentClaimHeader.UNITS == undefined) {
      this.currentClaimHeader.UNITS = 1;
    } else {
      this.currentClaimHeader.UNITS = parseInt(this.currentClaimHeader.UNITS.toString());
    }
    if (this.currentClaimHeader.CLAIMTYPE == "" || this.currentClaimHeader.CLAIMTYPE == null || this.currentClaimHeader.CLAIMTYPE == undefined) {
      this.currentClaimHeader.CLAIMTYPE = "";
    } else {
      this.currentClaimHeader.CLAIMTYPE = this.currentClaimHeader.CLAIMTYPE.toString();
    }
    if (this.currentClaimHeader.REFPROVID == "" || this.currentClaimHeader.REFPROVID == null || this.currentClaimHeader.REFPROVID == undefined) {
      this.currentClaimHeader.REFPROVID = "";
    } else {
      this.currentClaimHeader.REFPROVID = this.currentClaimHeader.REFPROVID.toString();
    }
    if (this.currentClaimHeader.VENDORID == "" || this.currentClaimHeader.VENDORID == null || this.currentClaimHeader.VENDORID == undefined) {
      this.currentClaimHeader.VENDORID = "";
    } else {
      this.currentClaimHeader.VENDORID = this.currentClaimHeader.VENDORID.toString();
    }
    if (this.currentClaimHeader.DATERECD == "" || this.currentClaimHeader.DATERECD == null || this.currentClaimHeader.DATERECD == undefined) {
      this.currentClaimHeader.DATERECD = "";
    } else {
      this.currentClaimHeader.DATERECD = this.currentClaimHeader.DATERECD.toString();
    }
    if (this.currentClaimHeader.BATCH_NO == "" || this.currentClaimHeader.BATCH_NO == null || this.currentClaimHeader.BATCH_NO == undefined) {
      this.currentClaimHeader.BATCH_NO = "";
    } else {
      this.currentClaimHeader.BATCH_NO = this.currentClaimHeader.BATCH_NO.toString();
    }
    if (this.currentClaimHeader.hpCode == "" || this.currentClaimHeader.hpCode == null || this.currentClaimHeader.hpCode == undefined) {
      this.currentClaimHeader.hpCode = "";
    } else {
      this.currentClaimHeader.hpCode = this.currentClaimHeader.hpCode.toString();
    }
    if (this.currentClaimHeader.Rec0ID == null || this.currentClaimHeader.Rec0ID == undefined) {
      this.currentClaimHeader.Rec0ID = 0;
    } else {
      this.currentClaimHeader.Rec0ID = parseInt(this.currentClaimHeader.Rec0ID.toString());
    }
    if (this.currentClaimHeader.Rec1ID == null || this.currentClaimHeader.Rec1ID == undefined) {
      this.currentClaimHeader.Rec1ID = 0;
    } else {
      this.currentClaimHeader.Rec1ID = parseInt(this.currentClaimHeader.Rec1ID.toString());
    }
    if (this.currentClaimHeader.lobCode == "" || this.currentClaimHeader.lobCode == null || this.currentClaimHeader.lobCode == undefined) {
      this.currentClaimHeader.lobCode = "";
    } else {
      this.currentClaimHeader.lobCode = this.currentClaimHeader.lobCode.toString();
    }
    if (this.currentClaimHeader.membDepCode == "" || this.currentClaimHeader.membDepCode == null || this.currentClaimHeader.membDepCode == undefined) {
      this.currentClaimHeader.membDepCode = "";
    } else {
      this.currentClaimHeader.membDepCode = this.currentClaimHeader.membDepCode.toString();
    }
  }

  DetailDataCheck() {
    let test = 1;
    this.claimDetailList.forEach((el) => {
      if (el.ModCodesString == "" || el.ModCodesString == null || el.ModCodesString == undefined) {
        el.ModCodesString = "";
      } else {
        el.ModCodesString = el.ModCodesString.toString();
      }

      if (el.toothNumString == "" || el.toothNumString == null || el.toothNumString == undefined) {
        el.toothNumString = "";
      } else {
        el.toothNumString = el.toothNumString.toString();
      }
      if (el.rec2IDArr == null || el.rec2IDArr == undefined) {
        el.rec2IDArr = 0;
      } else {
        el.rec2IDArr = parseInt(el.rec2IDArr.toString());
      }
      if (el.rec2IDDb == null || el.rec2IDDb == undefined) {
        el.rec2IDDb = 0;
      } else {
        el.rec2IDDb = parseInt(el.rec2IDDb.toString());
      }

      el.TYPE = 2;

      el.PHCODE = el.PHCODE.toString();
      el.FROMDATE = el.FROMDATE.toString();
      el.TODATE = el.TODATE.toString();
      el.PROCCODE = el.PROCCODE.toString();
      if (el.CURR_MODCODE == "" || el.CURR_MODCODE == null || el.CURR_MODCODE == undefined) {
        el.CURR_MODCODE = "";
      } else {
        el.CURR_MODCODE = el.CURR_MODCODE.toString();
      }
      if (el.CURR_MODAMOUNT == "" || el.CURR_MODAMOUNT == null || el.CURR_MODAMOUNT == undefined) {
        el.CURR_MODAMOUNT = "";
      } else {
        el.CURR_MODAMOUNT = el.CURR_MODAMOUNT.toString();
      }
      if (el.MODCODE == "" || el.MODCODE == null || el.MODCODE == undefined) {
        el.MODCODE = "";
      } else {
        el.MODCODE = el.MODCODE.toString();
      }
      if (el.MODCODE2 == "" || el.MODCODE2 == null || el.MODCODE2 == undefined) {
        el.MODCODE2 = "";
      } else {
        el.MODCODE2 = el.MODCODE2.toString();
      }

      if (el.MODCODE3 == "" || el.MODCODE3 == null || el.MODCODE3 == undefined) {
        el.MODCODE3 = "";
      } else {
        el.MODCODE3 = el.MODCODE3.toString();
      }
      if (el.MODCODE4 == "" || el.MODCODE4 == null || el.MODCODE4 == undefined) {
        el.MODCODE4 = "";
      } else {
        el.MODCODE4 = el.MODCODE4.toString();
      }
      if (el.MODAMOUNT == "" || el.MODAMOUNT == null || el.MODAMOUNT == undefined) {
        el.MODAMOUNT = "";
      } else {
        el.MODAMOUNT = el.MODAMOUNT.toString();
      }
      if (el.MODAMOUNT2 == "" || el.MODAMOUNT2 == null || el.MODAMOUNT2 == undefined) {
        el.MODAMOUNT2 = "";
      } else {
        el.MODAMOUNT2 = el.MODAMOUNT2.toString();
      }
      if (el.MODAMOUNT3 == "" || el.MODAMOUNT3 == null || el.MODAMOUNT3 == undefined) {
        el.MODAMOUNT3 = "";
      } else {
        el.MODAMOUNT3 = el.MODAMOUNT3.toString();
      }
      if (el.MODAMOUNT4 == "" || el.MODAMOUNT4 == null || el.MODAMOUNT4 == undefined) {
        el.MODAMOUNT4 = "";
      } else {
        el.MODAMOUNT4 = el.MODAMOUNT4.toString();
      }
      if (el.QTY == "" || el.QTY == null || el.QTY == undefined) {
        el.QTY = "1";
      } else {
        el.QTY = el.QTY.toString();
      }
      if (el.BILLED == "" || el.BILLED == null || el.BILLED == undefined) {
        el.BILLED = "1";
      } else {
        el.BILLED = el.BILLED.toString();
      }
      if (el.DIAGREF == null || el.DIAGREF == undefined) {
        el.DIAGREF = 0;
      } else {
        el.DIAGREF = parseInt(el.DIAGREF.toString());
      }
      if (el.BUNDLER == "" || el.BUNDLER == null || el.BUNDLER == undefined) {
        el.BUNDLER = "";
      } else {
        el.BUNDLER = el.BUNDLER.toString();
      }
      if (el.BUNDLERTYP == "" || el.BUNDLERTYP == null || el.BUNDLERTYP == undefined) {
        el.BUNDLERTYP = "";
      } else {
        el.BUNDLERTYP = el.BUNDLERTYP.toString();
      }
      if (el.COPAY == "" || el.COPAY == null || el.COPAY == undefined) {
        el.COPAY = "";
      } else {
        el.COPAY = el.COPAY.toString();
      }
      if (el.MANDISCOUNT == "" || el.MANDISCOUNT == null || el.MANDISCOUNT == undefined) {
        el.MANDISCOUNT = "";
      } else {
        el.MANDISCOUNT = el.MANDISCOUNT.toString();
      }
      if (el.TRANSACT_NO == "" || el.TRANSACT_NO == null || el.TRANSACT_NO == undefined) {
        el.TRANSACT_NO = "";
      } else {
        el.TRANSACT_NO = el.TRANSACT_NO.toString();
      }
      if (el.PROCCODE_DESC == "" || el.PROCCODE_DESC == null || el.PROCCODE_DESC == undefined) {
        el.PROCCODE_DESC = "";
      } else {
        el.PROCCODE_DESC = el.PROCCODE_DESC.toString();
      }
      if (el.CLINICALCODE1 == "" || el.CLINICALCODE1 == null || el.CLINICALCODE1 == undefined) {
        el.CLINICALCODE1 = "";
      } else {
        el.CLINICALCODE1 = el.CLINICALCODE1.toString();
      }
      if (el.CLINICALCODE2 == "" || el.CLINICALCODE2 == null || el.CLINICALCODE2 == undefined) {
        el.CLINICALCODE2 = "";
      } else {
        el.CLINICALCODE2 = el.CLINICALCODE2.toString();
      }
      if (el.CLINICALCODE3 == "" || el.CLINICALCODE3 == null || el.CLINICALCODE3 == undefined) {
        el.CLINICALCODE3 = "";
      } else {
        el.CLINICALCODE3 = el.CLINICALCODE3.toString();
      }
      if (el.CLINICALCODE4 == "" || el.CLINICALCODE4 == null || el.CLINICALCODE4 == undefined) {
        el.CLINICALCODE4 = "";
      } else {
        el.CLINICALCODE4 = el.CLINICALCODE4.toString();
      }
      if (el.CLINICALCODE5 == "" || el.CLINICALCODE5 == null || el.CLINICALCODE5 == undefined) {
        el.CLINICALCODE5 = "";
      } else {
        el.CLINICALCODE5 = el.CLINICALCODE5.toString();
      }
      if (el.CLINICALCODE6 == "" || el.CLINICALCODE6 == null || el.CLINICALCODE6 == undefined) {
        el.CLINICALCODE6 = "";
      } else {
        el.CLINICALCODE6 = el.CLINICALCODE6.toString();
      }
      if (el.CLINICALCODE7 == "" || el.CLINICALCODE7 == null || el.CLINICALCODE7 == undefined) {
        el.CLINICALCODE7 = "";
      } else {
        el.CLINICALCODE7 = el.CLINICALCODE7.toString();
      }
      if (el.CLINICALCODE8 == "" || el.CLINICALCODE8 == null || el.CLINICALCODE8 == undefined) {
        el.CLINICALCODE8 = "";
      } else {
        el.CLINICALCODE8 = el.CLINICALCODE8.toString();
      }
      if (el.MEMO1 == "" || el.MEMO1 == null || el.MEMO1 == undefined) {
        el.MEMO1 = "";
      } else {
        el.MEMO1 = el.MEMO1.toString();
      }
      if (el.MEMO10 == "" || el.MEMO10 == null || el.MEMO10 == undefined) {
        el.MEMO10 = "";
      } else {
        el.MEMO10 = el.MEMO10.toString();
      }
      if (el.MEMO2 == "" || el.MEMO2 == null || el.MEMO2 == undefined) {
        el.MEMO2 = "";
      } else {
        el.MEMO2 = el.MEMO2.toString();
      }
      if (el.MEMO3 == "" || el.MEMO3 == null || el.MEMO3 == undefined) {
        el.MEMO3 = "";
      } else {
        el.MEMO3 = el.MEMO3.toString();
      }
      if (el.MEMO4 == "" || el.MEMO4 == null || el.MEMO4 == undefined) {
        el.MEMO4 = "";
      } else {
        el.MEMO4 = el.MEMO4.toString();
      }
      if (el.MEMO5 == "" || el.MEMO5 == null || el.MEMO5 == undefined) {
        el.MEMO5 = "";
      } else {
        el.MEMO5 = el.MEMO5.toString();
      }
      if (el.MEMO6 == "" || el.MEMO6 == null || el.MEMO6 == undefined) {
        el.MEMO6 = "";
      } else {
        el.MEMO6 = el.MEMO6.toString();
      }
      if (el.MEMO7 == "" || el.MEMO7 == null || el.MEMO7 == undefined) {
        el.MEMO7 = "";
      } else {
        el.MEMO7 = el.MEMO7.toString();
      }
      if (el.MEMO8 == "" || el.MEMO8 == null || el.MEMO8 == undefined) {
        el.MEMO8 = "";
      } else {
        el.MEMO8 = el.MEMO8.toString();
      }
      if (el.MEMO9 == "" || el.MEMO9 == null || el.MEMO9 == undefined) {
        el.MEMO9 = "";
      } else {
        el.MEMO9 = el.MEMO9.toString();
      }




    })
  }

  SaveRecord5() {
    this._busy = true;
    this.DiagCodesDataCheck();
    this._http.post(this._baseUrl +
      'api/ClaimCapture/SaveFinalDiagCodes', this.diagnosticCodesList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((RecievedMessage) => {
      let v: any;
      v = RecievedMessage;
      this.messageSaved = v;
    },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this.SaveRecord8();
      }
    );
  }

  DiagCodesDataCheck() {
    let test = 1;
    this.diagnosticCodesList.forEach((el) => {

      el.TYPE = 5;
      if (el.DIAGREF == null || el.DIAGREF == undefined) {
        el.DIAGREF = 0;
      } else {
        el.DIAGREF = parseInt(el.DIAGREF.toString());
      }
      el.DIAG = el.DIAG.toString();
      if (el.FieldID == null || el.FieldID == undefined) {
        el.FieldID = 0;
      } else {
        el.FieldID = parseInt(el.FieldID.toString());
      }


    })
  }

  SaveRecord8() {
    this._busy = true;
    this.RejectionCodesDataCheck();
    this._http.post(this._baseUrl +
      'api/ClaimCapture/SaveFinalRejectCodes', this.RejectList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((RecievedMessage) => {
      let v: any;
      v = RecievedMessage;
      this.messageSaved = v;

    },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this.SaveRecord4();
      }
    );
  }

  RejectionCodesDataCheck() {
    let test = 1;
    this.RejectList.forEach((el) => {
      el.TYPE = "8";
      if (el.LINE == "" || el.LINE == null || el.LINE == undefined) {
        el.LINE = "";
      } else {
        el.LINE = el.LINE.toString();
      }
      if (el.ADJCODE == "" || el.ADJCODE == null || el.ADJCODE == undefined) {
        el.ADJCODE = "";
      } else {
        el.ADJCODE = el.ADJCODE.toString();
      }
      if (el.ADJUST == "" || el.ADJUST == null || el.ADJUST == undefined) {
        el.ADJUST = "";
      } else {
        el.ADJUST = el.ADJUST.toString();
      }
      if (el.COMMENT == "" || el.COMMENT == null || el.COMMENT == undefined) {
        el.COMMENT = "";
      } else {
        el.COMMENT = el.COMMENT.toString();
      }
      if (el.Rec2IDArr == null || el.Rec2IDArr == undefined) {
        el.Rec2IDArr = 0;
      } else {
        el.Rec2IDArr = parseInt(el.Rec2IDArr.toString());
      }
      if (el.Rec2IDdb == null || el.Rec2IDdb == undefined) {
        el.Rec2IDdb = 0;
      } else {
        el.Rec2IDdb = parseInt(el.Rec2IDdb.toString());
      }
    })
  }

  SaveRecord4() {
    this.ClaimNotesDataCheck();
    this._busy = true;
    this._http.post(this._baseUrl +
      'api/ClaimCapture/SaveFinalNotes', this.notesLists,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((RecievedMessage) => {
      let v: any;
      v = RecievedMessage;
      this.messageSaved = v;
    },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this._success = true;
        this._disableBack = true;
        this.DoAuditOnClaimsSaved();
      }
    );
  }

  ClaimNotesDataCheck() {
    let test = 1;
    this.notesLists.forEach((el) => {
      el.TYPE = 4;
      if (el.NOTE == "" || el.NOTE == null || el.NOTE == undefined) {
        el.NOTE = "";
      } else {
        el.NOTE = el.NOTE.toString();
      }
      if (el.LINE == null || el.LINE == undefined) {
        el.LINE = 0;
      } else {
        el.LINE = parseInt(el.LINE.toString());
      }
      if (el.Rec2ID == null || el.Rec2ID == undefined) {
        el.Rec2ID = 0;
      } else {
        el.Rec2ID = parseInt(el.Rec2ID.toString());
      }
      if (el.Rec2ArrID == null || el.Rec2ArrID == undefined) {
        el.Rec2ArrID = 0;
      } else {
        el.Rec2ArrID = parseInt(el.Rec2ArrID.toString());
      }
    })
  }

  GetRejectionReasons() {
    this._busy = true;
    this.lobCode = new RejectionCodesModel();
    this.lobCode.username = this._loginService.CurrentLoggedInUser.username;
    this._http.post(this._baseUrl + "api/ClaimCapture/GetRejectionCodes", this.lobCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedList: [RejectionCodesModel]) => {
      this.rejectionReasons = [];
      this.rejectionReasons = recievedList;

    },
      (error) => {
        console.log(error);
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => {
        this.PopulateDefualtAdjCodes();
        this._busy = false;
      });

  }

  GetStalePeriod() {

    // api/ClaimCapture/GetStalePeriod must get passed a different model containing HPCODE and OPT selected in claim capture in order to allow providers to capture claims
    //let x: RejectionCodesModel = new RejectionCodesModel();     // Jaco 2023-04-20
    //x.username = this._loginService.CurrentLoggedInUser.username; // Jaco 2023-04-20
    //x.userType = this._loginService.CurrentLoggedInUser.userType; // Jaco 2023-04-20
    let test = 1;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetStalePeriod', this._loginService.CurrentLoggedInUser, // Jaco  2023-04-20 this must change
      //this._http.post(this._baseUrl + 'api/ClaimCapture/GetStalePeriod', x,   // Jaco 2023-04-20 this must be used in future 
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: ValidationResultsModel) => {
      let x: string[];

      x = recievedData.message.split(":");
      //console.log(x);

      for (var i = 0; i < x.length; i++) {
        if (x[i] == this.currentClaimHeader.hpCode) {
          this._stalePeriod = parseInt(x[i + 1]);
          break;
        }
      }

      //console.log(this._stalePeriod);

    },
      (error) => {
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => { });
  }

  UpdateBankingDetails() {
    if (this.currentClaimHeader.CLAIMTYPE.toUpperCase() == 'M') {
      this.newBankingDetails.mainPolicyNo = this.currentClaimHeader.EZCAPMEMB;
      this.newBankingDetails.user = this._loginService.CurrentLoggedInUser.username;
      this.newBankingDetails.hpcode = this.currentClaimHeader.hpCode;
      this.newBankingDetails.claimType = this.currentClaimHeader.CLAIMTYPE;
    }
    if (this.currentClaimHeader.CLAIMTYPE.toUpperCase() == 'P') {
      this.newBankingDetails.provId = this.currentClaimHeader.EZCAPPROV;
      this.newBankingDetails.user = this._loginService.CurrentLoggedInUser.username;
      this.newBankingDetails.hpcode = this.currentClaimHeader.hpCode;
      this.newBankingDetails.claimType = this.currentClaimHeader.CLAIMTYPE;
    }

    this._busy = true;
    this._http.post(this._baseUrl + 'api/ClaimCapture/UpdateBankingDetails', this.newBankingDetails, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results) => { },
      (error) => {
        console.log(error);
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this._openBanking = false;
      });


  }

  GetHpCodesLagReport() {
    this._load = true;
    let x: RejectionCodesModel;
    x = new RejectionCodesModel();
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).
      subscribe((returnedList: [HealthplanModel]) => {
        this.HpCodesList = [];
        this.HpCodesList = returnedList;
      },
        (error) => {
          console.log(error);
          //if (error.status == '401') {
          //  this._router.navigateByUrl('/login');
          //}
        },
        () => { this.GenerateLagReport(this.HpCodesList[0].code) });
  }

  GenerateLagReport(hpcode: string) {
    let claim: ClaimReport = new ClaimReport();
    claim.user = this._loginService.CurrentLoggedInUser.username;
    claim.hpcode = hpcode;
    let reportname: string;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetLagReport', claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {
      let x: any;
      x = recieved;
      reportname = x.message;
    },
      (error) => {
        console.log(error);
        console.log(error);
      },
      () => {
        let today: Date = new Date;
        let dateString: string;
        dateString = today.toDateString() + '_' + hpcode + '_LAGREPORT.xlsx';
        var a = document.createElement('a');
        a.setAttribute('type', 'hidden');
        a.target = "_blank"
        //a.href = '../../PamcPortal/assets/Reports/' + reportname;
        a.href = 'assets/Reports/' + reportname;
        a.download = dateString;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._load = false;
      });
  }

  GetHpCodesAgeReport() {
    this._load = true;
    let x: RejectionCodesModel;
    x = new RejectionCodesModel();
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).
      subscribe((returnedList: [HealthplanModel]) => {
        this.HpCodesList = [];
        this.HpCodesList = returnedList;
      },
        (error) => {
          console.log(error); if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => { this.GenerateAgeReport(this.HpCodesList[0].code) });
  }

  GenerateAgeReport(hpcode: string) {
    let claim: ClaimReport = new ClaimReport();
    claim.user = this._loginService.CurrentLoggedInUser.username;

    claim.hpcode = hpcode;
    let reportname: string;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetAgeReport', claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {
      let x: any;
      x = recieved;
      reportname = x.message;
    },
      (error) => {
        console.log(error);
        console.log(error);
      },
      () => {
        let today: Date = new Date;
        let dateString: string;
        dateString = today.toDateString() + '_' + hpcode + '_AGEANALYSIS.xlsx';
        var a = document.createElement('a');
        a.setAttribute('type', 'hidden');
        a.target = "_blank"
        //a.href = '../../PamcPortal/assets/Reports/' + reportname;
        a.href = 'assets/Reports/' + reportname;
        a.download = dateString;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._load = false;
      });
  }

  DoAuditOnClaimsSaved() {
    this._busy = true;
    this.auditMsg = new ValidationResultsModel();
    this._http.post(this._baseUrl + 'api/ClaimCapture/DoAuditOnClaimsSaved', this.claimDetailList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }

    ).subscribe((results: ValidationResultsModel) => {

      this.auditMsg.valid = results.valid;
      this.auditMsg.message = results.message;
    },
      (error) => {
        this.errorMsgFound = "CODE: " + error.status + " Message " + error.message;
        this._errorFound = true;
      },
      () => {
        this._busy = false;
        this._success = true;
      });
  }

  LogObject() {
    this.HeaderDataCheck();
    this.DetailDataCheck();
    this.DiagCodesDataCheck();
    this.ClaimNotesDataCheck();
    this.RejectionCodesDataCheck();
    let objects: Objectlog = new Objectlog();
    objects.header = new Recordtype1Model();
    objects.detail = [];
    objects.diags = [];
    objects.notes = [];
    objects.rejects = [];
    objects.header = this.currentClaimHeader;
    objects.detail = this.claimDetailList;
    objects.diags = this.diagnosticCodesList;
    objects.notes = this.notesLists;
    objects.rejects = this.RejectList;

    this._http.post(this._baseUrl + 'api/ClaimCapture/LogObject', objects, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((el) => {

    },
      (error) => { console.log(error) },
      () => { })
  }

  RestoreClaim(claim: Objectlog) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/ClaimCapture/RestoreClaim', claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {
        let c: string[];
        c = results.message.split(',');
        this.fileid = parseInt(c[0]);
        this.rec1Id = parseInt(c[1]);
        this.auditMsg.message = c[2];
        this.auditMsg.valid = results.valid;

      },
        (error) => { console.log(error); },
        () => {

          this._busy = false;
          this._success = true;
        });
  }

  UpdateHeader() {
    this._busy = true;

    this._http.post(this._baseUrl + 'api/ClaimCapture/UpdateHeader', this.currentClaimHeader, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
      },
      () => {
        this._busy = false;
      })
  }

  SearchCapturedClaims() {

    this._ReportList.splice(0, this._ReportList.length);
    //this._ReportListTemp.splice(0, this._ReportListTemp.length);

    let claim: ClaimReport = new ClaimReport();
    claim.hpcode = '';
    claim.capturedFrom = this.capturedClaimsForm.get('createdFrom').value;
    claim.capturedTo = this.capturedClaimsForm.get('createdTo').value;
    claim.username = this._loginService.CurrentLoggedInUser.username;

    if (this.capturedClaimsForm.get('capturedBy').value == "Me") {
      claim.userClaimsOnly = true;
    } else {
      claim.userClaimsOnly = false;
    }

    if (this.HpCodesList.length > 1) {
      for (var i = 0; i < this.HpCodesList.length; i++) {
        if (i == this.HpCodesList.length - 1) {
          claim.hpcode = claim.hpcode + this.HpCodesList[i].code;
        } else {
          claim.hpcode = claim.hpcode + this.HpCodesList[i].code + ';';
        }
      }
    } else {
      claim.hpcode = this.HpCodesList[0].code;
    }
    this.GetAllClaims(claim);
  }

  GetAllClaims(claims: ClaimReport) {
    //this._showReport = false;
    this._busyLoading = true;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetAllClaims', claims, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
      })
    }).subscribe(
      (receivedData: ClaimReport[]) => {

        this._ReportList = [];
        let page = 1;
        let count = 1;
        receivedData.forEach((el) => {
          if (count == 20) {
            page++;
            count = 1;
          }
          el.memb = el.memb.toUpperCase();
          el.billed = parseFloat(el.billed).toFixed(2);
          el.page = page;
          count++;

          this._ReportList.push(el);
          //this._ReportListTemp.push(el);
        });
        this._ReportListDisplay = [];

        this.claimReportTotalPages = page;
        //let x: ClaimReport[];
        //x = [];
        //if (this._ReportList.length !== 0) {
        //  this._ReportList.sort((a, b) => a.dateCreated < b.dateCreated ? -1 : 1).forEach((el) => { x.push(el) });

        //  this.earliestdate = x[0].dateCreated;
        //  this.latestDate = x[x.length - 1].dateCreated;
        //} else {
        //  this.earliestdate = "";
        //  this.latestDate = "";
        //}

        this._ReportList.forEach((el) => {
          if (el.page == 1) {
            this._ReportListDisplay.push(el);
          }
        })
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._busyLoading = false;
        this._ReportList = [];
        this._ReportListDisplay = [];
        this._errorModal = true;
        this._errorMessageHeading = "Unexpected error ocurred";
        this._errorMessage = error.message;
        //if (error.status == '401') {
        //  this._router.navigateByUrl('/login');
        //}
      },
      () => {
        this._busyLoading = false;
        //this._showReport = true;
      }
    );
  }

  incPage() {
    if (this.curPage < this.claimReportTotalPages) {
      this.curPage++;
      this._ReportListDisplay = [];
      this._ReportList.forEach((el) => {
        if (el.page == this.curPage) {
          this._ReportListDisplay.push(el);
        }
      })
    }
  }

  decPage() {
    if (this.curPage > 1) {
      this.curPage--;
      this._ReportListDisplay = [];
      this._ReportList.forEach((el) => {
        if (el.page == this.curPage) {
          this._ReportListDisplay.push(el);
        }
      })
    }
  }

  //GridSortingDates() {

  //  this.filterCountDetails++;
  //  if (this.filterCountDetails === 1) {
  //    this._ReportList.sort((a, b) => b.dateCreated < a.dateCreated ? -1 : 1);
  //  } else {
  //    this.filterCountDetails = 0;
  //    this._ReportList.sort((a, b) => a.dateCreated < b.dateCreated ? -1 : 1);
  //  }

  //}

  //GridSortingUser() {
  //  this.filterCountNet++;
  //  if (this.filterCountNet === 1) {
  //    this._ReportList.sort((a, b) => a.user < b.user ? -1 : 1);
  //  } else {
  //    this.filterCountNet = 0;
  //    this._ReportList.sort((a, b) => b.user < a.user ? -1 : 1);
  //  }

  //}

  GridSortingDates() {
    this.filterCountDetails++;
    if (this.filterCountDetails === 1) {
      this._ReportList.sort((a, b) => b.dateCreated < a.dateCreated ? -1 : 1);
    } else {
      this.filterCountDetails = 0;
      this._ReportList.sort((a, b) => a.dateCreated < b.dateCreated ? -1 : 1);
    }
    let page = 1;
    let count = 1;
    this._ReportList.forEach(el => {
      if (count == 20) {
        page++;
        count = 1;
      }
      count++
      el.page = page;
    })
    this._ReportListDisplay = [];
    this._ReportList.forEach((el) => {
      if (el.page == 1) {
        this._ReportListDisplay.push(el);
      }
    });
    this.claimReportTotalPages = page;
    this.curPage = 1;
  }

  GridSortingUser() {
    this.filterCountNet++;
    if (this.filterCountNet === 1) {
      this._ReportList.sort((a, b) => a.user < b.user ? -1 : 1);
    } else {
      this.filterCountNet = 0;
      this._ReportList.sort((a, b) => b.user < a.user ? -1 : 1);
    }
    let page = 1;
    let count = 1;
    this._ReportList.forEach(el => {
      if (count == 20) {
        page++;
        count = 1;
      }
      count++
      el.page = page;
    })
    this._ReportListDisplay = [];
    this._ReportList.forEach((el) => {
      if (el.page == 1) {
        this._ReportListDisplay.push(el);
      }
    });
    this.claimReportTotalPages = page;
    this.curPage = 1;
  }

  ExportReportListExcel() {
    this._busyLoading = true;
    this._http.post(this._baseUrl + 'api/ClaimCapture/ExportCapturedClaimsToExcel', this._ReportList, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
      })
    }).subscribe(
      (result: ValidationResultsModel) => {
        if (result.valid) {
          // Download file in browser    
          var a = document.createElement('a');
          a.target = "_blank";
          a.setAttribute('type', 'hidden');
          a.href = 'assets/CapturedClaimsReports/' + result.message;
          a.download = result.message;
          document.body.appendChild(a);
          a.click();
          a.remove();
        } else {
          this._errorModal = true;
          this._errorMessageHeading = "Export Failed";
          this._errorMessage = result.message;
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
        this._busyLoading = false;
        this._errorModal = true;
        this._errorMessageHeading = "Export Failed";
        this._errorMessage = error.message;
      },
      () => {
        this._busyLoading = false;
      }
    );
  }
}

import { Injectable, Inject } from '@angular/core';
import { SettingsModel } from '../models/SettingsModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HeaderModel } from '../models/HeadersModel';
import { EmailsModel } from '../models/EmailsModel';
import { Router } from '@angular/router';
import { ExeptionSettings } from '../models/exeption-settings';


@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public busy: boolean = false;
  private _settings: SettingsModel[] = [];
  private _expSettings: ExeptionSettings[] = [];
  private _selectedSetting: SettingsModel = new SettingsModel;
  private _brands: HeaderModel = new HeaderModel();
  private _emails: EmailsModel = new EmailsModel();
  private _ProvIDAmount: number;
  public _headerAlert: boolean = false;
  public _headerInfo: string = '';
  private _provSpecs: string[] = [];
  public get provSpecs(): string[] {
    return this._provSpecs;
  }
  public set provSpecs(value: string[]) {
    this._provSpecs = value;
  }
  private _provContract: string;
  public get provContract(): string {
    return this._provContract;
  }
  public set provContract(value: string) {
    this._provContract = value;
  }
  private _surveyLink: string = "";
  private _showSurvey: boolean = false;
    public get showSurvey(): boolean {
        return this._showSurvey;
    }
    public set showSurvey(value: boolean) {
        this._showSurvey = value;
    }
    public get surveyLink(): string {
        return this._surveyLink;
    }
    public set surveyLink(value: string) {
        this._surveyLink = value;
    }


  /*Will be used when a subscribe runs into a error*/
  private _subscribeError: boolean = false;



  //Below variable that will show the logged in user the DRC contact details
  private _drcContactDetails: boolean = false;

  //Variable that will show the logged in user the PAMC contact details
  private _pamcContactDetails: boolean = false;

  //Variable that will show the logged in user the All details
  private _allContactDetails: boolean = false

  /*Will be used to load the images on the dashboard*/
  //Objects that holds all the properties for the images


  constructor(private http: HttpClient, private _router: Router, @Inject('BASE_URL') public baseUrl: string) {

  }

  /*Loads all the setting by doing a web call to the configuartion controller
   Loops through the recieved setting getting specific settings in variables.*/
  private _token: string;
  public get token(): string {
    return this._token;
  }
  public set token(value: string) {
    this._token = value;
  }
  public get expSettings(): ExeptionSettings[] {
    return this._expSettings;
  }
  public set expSettings(value: ExeptionSettings[]) {
    this._expSettings = value;
  }

  public LoadSettings() {
    this.busy = true;
    this._token = localStorage.getItem("jwt");
    this.http.get(this.baseUrl + "api/configuration/getSettings", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (RecievedSettings: SettingsModel[]) => {
        this._settings = RecievedSettings;
        this._settings.forEach(el => {
          if ((el.settingsId == "profTitle")) {
            this._brands.provider = el.description
          }
          if ((el.settingsId == "notLogin")) {
            this._brands.default = el.description
          }

          if ((el.settingsId == "accTitle")) {
            this._brands.accountingBureau = el.description
          }

          if (el.settingsId == "adminTitle") {
            this._brands.admin = el.description
          }
          if (el.settingsId == "administratorTitle") {
            this._brands.administrators = el.description
          }

          if (el.settingsId == "membTitle") {
            this._brands.member = el.description
          }
          if (el.settingsId == "ShowSurveyForProviders") {
            this._showSurvey = el.enabled;
          }
          if (el.settingsId == "SurveyLinkForProviders") {
            this._surveyLink = el.description
            //console.log("LINK: "+this._surveyLink);
          }
          if (el.settingsId == "BrokerTitle") {
            this._brands.broker = el.description
          }

          if (el.settingsId == "accReg") {
            this.Emails.bureauRegistration = el.description
          }
          if (el.settingsId == "AllowedProvSpecs") {
            this.provSpecs = el.description.split(',');
          }
          if (el.settingsId == "AllowedProvContracts") {
            this.provContract = el.description;
          }
          if (el.settingsId == "HeaderAlert") {
            this._headerAlert = el.enabled;
            this._headerInfo = el.description;
          }
        });
      }
      , (error) => {
        this._subscribeError = true;

        this.busy = false;
      },
      () => {
        this.busy = false;
        this.LoadProvExeptionSettings();
      }
    );

  }

  public LoadProvExeptionSettings() {
    this.busy = true;
    this._token = localStorage.getItem("jwt");
    this.http.get(this.baseUrl + "api/configuration/getProvExeptionSettings", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (RecievedSettings: ExeptionSettings[]) => {
        this._expSettings = RecievedSettings;
      }
      , (error) => {
        this._subscribeError = true;
        this.busy = false;

      },
      () => {
        this.busy = false;
      }
    );
  }

  //Gets all enabled settings: determines what needs to show or hide
  GetSettingEnabled(settingName: string) {
    let foundSettings = this._settings.filter(function (el) {
      return el.settingsId == settingName;
    })
    if (foundSettings.length == 1) {
      return foundSettings[0].enabled
    }

    return false;
  }

  //Checks whether a setting is required or not
  GetSettingRequired(settingId: string) {
    let foundSettings = this._settings.filter(function (el) {
      return el.settingsId == settingId;
    })
    if (foundSettings.length == 1) {
      return foundSettings[0].required
    }

    return false;
  }

  //Returns a description of specific setting
  GetSettingsDesc(settingsId: string) {
    let foundSettings = this._settings.filter(function (el) {
      return el.settingsId == settingsId;
    })
    if (foundSettings.length == 1) {
      return foundSettings[0].description
    }
  }

  //Will get all needed settings for the dashboards
  GetSettingsDashboard(settingId: string) {
    let foundSetting = this._settings.filter(function (el) {
      return el.settingsId == settingId
    })
    if (foundSetting.length == 1) {
      return foundSetting[0].description
    }
  }

  get ShowaccEmail(): boolean {
    let email = this._settings.filter(function (el) {
      return el.settingsId == "accEmail"
    })
    if (email.length == 1) {
      return email[0].enabled
    }
    return false;
  }

  get ShowaccaccPass(): boolean {
    let email = this._settings.filter(function (el) {
      return el.settingsId == "accPass"
    })
    if (email.length == 1) {
      return email[0].enabled
    }
    return false;
  }

  get accPrac(): boolean {
    let email = this._settings.filter(function (el) {
      return el.settingsId == "accPrac"
    })
    if (email.length == 1) {
      return email[0].enabled
    }
    return false;
  }

  get RegisterationTitle(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "regTitle"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }

  get ProvIDThreash(): boolean {
    let ProvIDThreash = this._settings.filter(function (el) {
      return el.settingsId == "ProvIDThreash"
    })
    if (ProvIDThreash.length == 1) {
      return ProvIDThreash[0].enabled
    }
    return false;
  }

  get ProvIDThreashDesc(): number {
    let ProvIDThreash = this._settings.filter(function (el) {
      return el.settingsId == "ProvIDThreash"
    })
    if (ProvIDThreash.length == 1) {
      return +ProvIDThreash[0].description
    }
  }

  get CheckRequiredProvIDThreash(): boolean {
    let CheckRequiredProvIDThreash = this._settings.filter(function (el) {
      return el.settingsId == "ProvIDThreash"
    })
    if (CheckRequiredProvIDThreash.length == 1) {
      return CheckRequiredProvIDThreash[0].required
    }
    return false;
  }

  get BureauRegisterationDisclaimer(): string {
    let disclaimer = this._settings.filter(function (el) {
      return el.settingsId == "BureauRegisterationDisclaimer"
    })
    if (disclaimer.length == 1) {
      return disclaimer[0].description
    }
  }

  get RegistrationDescription(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "regdesc"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }

  get ShowBureauRegistrationName(): boolean {
    let name = this._settings.filter(function (el) {
      return el.settingsId == "accName"
    })
    if (name.length == 1) {
      return name[0].enabled
    }
    return false
  }

  get ShowBureauRegistrationEmail(): boolean {
    let email = this._settings.filter(function (el) {
      return el.settingsId == "accEmail"
    })
    if (email.length == 1) {
      return email[0].enabled
    }
    return false;
  }

  get RegistrationHelpHeader(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "accHelp(H)"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }

  get RegistrationHelpBody(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "accHelp(B)"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }

  get PracticeNumberHead(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "pracNum(H)"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }

  get PracticeNumberBody(): string {
    let desc = this._settings.filter(function (el) {
      return el.settingsId == "pracNum(B)"
    })
    if (desc.length == 1) {
      return desc[0].description
    }
  }





  //Exposing Private variables

  /*Exposing the Emails so that It can be accessed in the security service*/
  get Emails(): EmailsModel {
    return this._emails;
  }

  /*Exposing the _ProvIDAmount to be used in the security controller*/
  get ProvAmount(): number {
    return this._ProvIDAmount;
  }

  /*Making the _ProvIDAmount a set aswell so that the value can be modified in the security controller */
  set ProvAmount(val) {
    this._ProvIDAmount = val;
  }

  /*Exposing the Program Title when a User is not logged in*/
  get NotLoggedInTitle() {
    return this._brands.default;
  }

  /*Exposing the page title when a bureau logs in*/
  get BureauTitle() {
    return this._brands.accountingBureau;

  }

  /*Exposing the admin page title*/
  get AdminTitle() {
    return this._brands.admin
  }

  /*Exposing the Provider Title*/
  get ProviderTitle() {
    return this._brands.provider;
  }

  /*Exposing the Administrator page title*/
  get AdministratorTitle() {
    return this._brands.administrators
  }

  /*Exposing the Member page title*/
  get MemberTitle() {
    return this._brands.member
  }

  /*Exposing the Broker title*/
  get BrokeTitle() {
    return this._brands.broker
  }

  /*Returning the the error if setting cant load*/
  get SettingsError() {
    return this._subscribeError;
  }

  //Exposing the DRC contact details
  get DRCDetails() {
    return this._drcContactDetails
  }
  set DRCDetails(isValid) {
    this._drcContactDetails = isValid
  }

  //Exposing PAMC contact details
  get PAMCDetails() {
    return this._pamcContactDetails
  }
  set PAMCDetails(isValid) {
    this._pamcContactDetails = isValid
  }

  //Exposing All contact details
  get AllDetails() {
    return this._allContactDetails
  }
  set AllDetails(isValid) {
    this._allContactDetails = isValid

  }


  //Exposing all recieved settings
  get Settings() {
    return this._settings;
  }

  //exposing the selected settings
  get SelectedSetting() {
    return this._selectedSetting;
  }
  set SelectedSetting(val: SettingsModel) {
    this._selectedSetting = val;
  }

}

import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PatientWellnessFormModel } from '../models/PatientWellnessFormModel';

@Injectable({
  providedIn: 'root'
})
export class PatientWellnessService {

  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string) { }

  CheckIfPatientExist(idNoData: PatientWellnessFormModel) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/PatientWellness/CheckIfPatientExist', idNoData, { headers: headers });
  }

  SubmitPatientWellnessForm(formData: PatientWellnessFormModel) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/PatientWellness/SubmitPatientWellnessForm', formData, { headers: headers });
  }

}

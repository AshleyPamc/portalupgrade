import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from './security.service';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { BatchDetailsModel } from '../models/BatchDetailsModel';
import { ClaimsModel } from '../models/ClaimsModel';
import { BankingDetailModel } from '../models/BankingDetailModel';
import { FamilyModel } from '../models/FamilyModel';
import { CheckrunModel } from '../models/CheckrunModel';
import { ModifiedClaimsArrayModel } from '../models/ModifiedClaimsArrayModel';
import { BordrowListModel } from '../models/BordrowListModel';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { PatientModel } from '../models/PatientModel';
import { HealthplanModel } from '../models/HealthplanModel';
import { Recordtype1Model } from '../models/Recordtype1Model';
import { RejectionCodesModel } from '../models/RejectionCodesModel';
import { ProviderList } from '../models/ProviderListModel';
import { CheckRunTypeModel } from '../models/CheckRunTypeModel';
import { CheckRunDetailsModel } from '../models/CheckRunDetailsModel';
import { DetailLinesModel } from '../models/DetailLinesModel';
import { MemberNoModel } from '../models/MemberNoModel';
import { ExcludedClaims } from '../models/Excludedclaims';
import { LoginService } from './login.service';
import { formatDate } from '@angular/common';
import { ClaimHeadersModel } from '../models/ClaimHeadersModel';
import { VendorInfoModel } from '../models/VendorInfoModel';
import { Diagcodes } from '../models/diagcodes';

@Injectable({
  providedIn: 'root'
})
export class CheckrunService {

  //Private Variables with getters and setters
  private _tempDiscription: string;
  private _displayData: boolean = false;
  private _lastRecDate: any;
  private _totalMembPayments: any;
  private _totalProvPayments: any;
  private _excludeBankingDetails: boolean = false;
  private _enableDownload: boolean = true;
  private _showText: boolean = false;
  private _loadBoardRow: boolean = false;
  private _userName: string;
  private _amountOfClaims: number;
  private _errorHandler: boolean = false;
  private _legendDisplay: boolean = false;
  private _validationMessage: string;
  private _openWarningPayReady: boolean = false;
  private _currentFilter: string = "No Filter Applied";
  private _currentProvFilter: string = "No Filter Applied";
  private _currentPage: number = 1;
  private _closeCheckRun: boolean = false;
  private _readyToGenerate: boolean = true;
  private _showBordRowComponents: boolean = false;
  private _updateBankDetails: boolean = false;
  private _ProcessingValidation: boolean = true;
  private _confirmPaymentReady: boolean = false;
  private _changeBankModel: boolean = false;
  private _displaySpinner: boolean;
  private _showList: boolean = false;
  private _vendorName: string;
  private _scrollFilter: string;
  private _currentView: string = "Member View";
  private _getUnreleasedBatches: boolean = false;
  private _getUnPaidBatches: boolean = false;
  private _gettingDetails: boolean = false;
  private _openDetailsModel: boolean = false;
  private _membNo: string;
  private _visitCounts: number;
  private _hideReleaseOpt: boolean = false;
  private _unpaidCount: number = 0;
  private _paidCount: number = 0;
  private _inprocessCOunt: number = 0;
  private _authNo: string;
  private _enableSearch: boolean = false;
  private _openEditBatch: boolean = false;
  private _enableNavButtons: boolean = true;
  private _enablesave: boolean = false;
  private _openUnEditBatch: boolean = false;
  private _lastpayedDateFound: boolean = false;
  private _provTotal: number = 0;
  private _membTotal: number = 0;
  private _openWarnigDate: boolean = false;
  private _token: string;
  public nobankingFound: boolean = false;
  public _rowId: string;
  public _rejectProv: string;
  public _rejectmembNo: string;
  public _expandLegend: boolean = false;
  public _expandFilter: boolean = false;
  private _vendorInfo: VendorInfoModel = new VendorInfoModel();




  //Public variables
  public amounts: ClaimsModel = new ClaimsModel();
  public _hpListShow: boolean = false;
  public message: string = 'Loading';
  public totalClaims: number;
  public totalBenClaims: number;
  public totalbenprov: any;
  public tottalbenmen: any;
  public count: number = 0;
  public batchNumber: string;
  public _totalPayableAmount: number;
  public _amountOfReversals: number;
  public _totalDetailLines: number = 0;
  public _totalClaimLines: number = 0;
  public _pageCounter: number = 0;
  public _pageScrollCounter: number = 0;
  public _pageNumber: number = 0;
  public _filtteredList: any;
  public _fillteredProvList: any;
  public _rejectionNote: string;
  public _rejectionReason: string;
  public _total: number;
  public _specificBatchNo: BatchDetailsModel;
  public _vendBankingDetails: BankingDetailModel = new BankingDetailModel();
  public newBankinDetails: BankingDetailModel = new BankingDetailModel();
  public existingDetails: BankingDetailModel = new BankingDetailModel();
  public excludingClaims: ExcludedClaims[] = [];
  public _paymentType: string;
  public _totalAmountToPay: number = 0;
  public _MembName: string = "";
  public _provId: string = "";
  public _provName: string = "";
  public _isProvider: boolean = false;
  public _providerGrouping: boolean = false;
  public _providerPageCount: number;
  public _scrollableView: boolean = false;
  public details: CheckRunDetailsModel;
  public _displayFetchModel: boolean = false
  public _refProvInfo: DetailLinesModel = new DetailLinesModel();
  public _paidInfo: DetailLinesModel = new DetailLinesModel();
  public _whereClauseBenefit: number = 0;
  public _whereCauseSpec: number = 0;
  public sheetName: string;
  public _lastEnteredDate: string = '';
  public _wizard: boolean = false;
  public _wizardNext: boolean = false;
  public _wizardProvSort: boolean = false;
  public _wizardDateSort: boolean = false;
  public _wizardMembSort: boolean = false;
  public _wizardClaimSort: boolean = false;
  public _wizardServiceSort: boolean = false;
  public _wizardpagecount: number = 0;
  public _largeAmountFound: boolean = false;
  public _markReady: number = 0;
  public ErrorRequets: boolean = false;
  private _submitCheckrunError: boolean = false;
  private _submitCheckrunErrorMsg: string = "";

  //Public Arrays & Lists
  public _patientsPerFamily = {};
  public _claimsPerUnique = [];
  public _detailsPerClaim = [];
  public _uniqueList: FamilyModel[] = [];
  public _duplicateList: CheckrunModel[] = [];
  public _uniqueListPerMember: CheckrunModel[] = [];
  public bordRowList: CheckrunModel[] = [];
  public initialTotalsBordRowList: CheckrunModel[] = [];
  public arrType: string[] = ['Member', 'Provider'];
  public amountsToDisplay: ClaimsModel[] = [];
  public benifitAmountsToDis: ClaimsModel[] = [];
  public gridData: ModifiedClaimsArrayModel[] = [];
  public benefitGridData: ModifiedClaimsArrayModel[] = [];
  public _unreleasedBatches: string[] = [];
  public _unreleasedBatchesType: CheckRunTypeModel[] = [];
  public _paidBatches: string[] = [];
  public _paidBatchesType: CheckRunTypeModel[] = [];
  public _unpaidBatches: string[] = [];
  public _unpaidBatchesType: CheckRunTypeModel[] = [];
  public _filteredPaidList: BordrowListModel[] = [];
  public _filteredUnreleasedList: BordrowListModel[] = [];
  public _filteredUnpaidList: BordrowListModel[] = [];
  public _paid = {};
  public _unpaid = {};
  public _unreleased = {};
  public initailSotingOfUniqueList: FamilyModel[] = [];
  public initialSortingOfProvList = {};
  public initialSortingOfProviderGroup: ProviderList[] = [];
  public initailSortingOfPatientList = {};
  public HpCodesList: HealthplanModel[] = [];
  public recordType1: Recordtype1Model;
  public rejectionReasons: RejectionCodesModel[] = [];
  public lobCode: RejectionCodesModel = new RejectionCodesModel();
  public _patientsPerProvider = {};
  public _uniqueProvList: ProviderList[] = [];
  public testLoop: number[] = [];
  public _filteredBordRowList: CheckrunModel[] = [];
  public _claimInfo: DetailLinesModel[] = [];
  public whereClause: string[] = [];
  public accTypes: string[] = ['CHEQUE', 'SAVINGS', 'CREDIT CARD', 'TRANSMISSION', 'OTHER'];
  private _diacodes: Diagcodes[] = [];


  constructor(private _router: Router, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _loginService: LoginService) {

    for (var i = 0; i < 30; i++) {
      this.testLoop[i] = (i + 1);
    }
    this._totalAmountToPay = 0;
  }

  //Getters and Setters
  public get token(): string {
    return localStorage.getItem("jwt");
  }
  public set token(value: string) {
    localStorage.setItem("jwt",value);
  }
  public get membTotal(): number {
    return this._membTotal;
  }
  public set membTotal(value: number) {
    this._membTotal = value;
  }
  public get provTotal(): number {
    return this._provTotal;
  }
  public set provTotal(value: number) {
    this._provTotal = value;
  }
  public get unpaidCount(): number {
    return this._unpaidCount;
  }
  public set unpaidCount(value: number) {
    this._unpaidCount = value;
  }
  public get paidCount(): number {
    return this._paidCount;
  }
  public set paidCount(value: number) {
    this._paidCount = value;
  }
  public get inprocessCOunt(): number {
    return this._inprocessCOunt;
  }
  public set inprocessCOunt(value: number) {
    this._inprocessCOunt = value;
  }
  public get hideReleaseOpt(): boolean {
    return this._hideReleaseOpt;
  }
  public set hideReleaseOpt(value: boolean) {
    this._hideReleaseOpt = value;
  }
  public get tempDiscription(): string {
    return this._tempDiscription;
  }
  public set tempDiscription(value: string) {
    this._tempDiscription = value;
  }
  public set displayData(value: boolean) {
    this._displayData = value;
  }
  public get displayData() {
    return this._displayData;
  }
  public get totalMembPayments(): any {
    return this._totalMembPayments;
  }
  public set totalMembPayments(value: any) {
    this._totalMembPayments = value;
  }
  public get totalProvPayments(): any {
    return this._totalProvPayments;
  }
  public set totalProvPayments(value: any) {
    this._totalProvPayments = value;
  }
  public get lastRecDate(): any {
    return this._lastRecDate;
  }
  public set lastRecDate(value: any) {
    this._lastRecDate = value;
  }
  public set excludeBankingDetails(value: boolean) {
    this._excludeBankingDetails = value;
  }
  public get excludeBankingDetails(): boolean {
    return this._excludeBankingDetails;
  }
  public get enableDownload(): boolean {
    return this._enableDownload;
  }
  public set enableDownload(value: boolean) {
    this._enableDownload = value;
  }
  public get showText(): boolean {
    return this._showText;
  }
  public set showText(value: boolean) {
    this._showText = value;
  }
  public get loadBoardRow(): boolean {
    return this._loadBoardRow;
  }
  public set loadBoardRow(value: boolean) {
    this._loadBoardRow = value;
  }
  public set userName(value: string) {
    this._userName = value;
  }
  public get userName(): string {
    return this._userName;
  }
  public get amountOfClaims(): number {
    return this._amountOfClaims;
  }
  public set amountOfClaims(value: number) {
    this._amountOfClaims = value;
  }
  public get errorHandler(): boolean {
    return this._errorHandler;
  }
  public set errorHandler(value: boolean) {
    this._errorHandler = value;
  }
  public get legendDisplay(): boolean {
    return this._legendDisplay;
  }
  public set legendDisplay(value: boolean) {
    this._legendDisplay = value;
  }
  public get validationMessage(): string {
    return this._validationMessage;
  }
  public set validationMessage(value: string) {
    this._validationMessage = value;
  }
  public get openWarningPayReady(): boolean {
    return this._openWarningPayReady;
  }
  public set openWarningPayReady(value: boolean) {
    this._openWarningPayReady = value;
  }
  public get openWarnigDate(): boolean {
    return this._openWarnigDate;
  }
  public set openWarnigDate(value: boolean) {
    this._openWarnigDate = value;
  }
  public get currentFilter() {
    return this._currentFilter;
  }
  public set currentFilter(value: string) {
    this._currentFilter = value;
  }
  public get currentProvFilter() {
    return this._currentProvFilter;
  }
  public set currentProvFilter(value: string) {
    this._currentProvFilter = value;
  }
  public get currentPage() {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get closeCheckRun(): boolean {
    return this._closeCheckRun;
  }
  public set closeCheckRun(value: boolean) {
    this._closeCheckRun = value;
  }
  public get readyToGenerate(): boolean {
    return this._readyToGenerate;
  }
  public set readyToGenerate(value: boolean) {
    this._readyToGenerate = value;
  }
  public get showBordRowComponents(): boolean {
    return this._showBordRowComponents;
  }
  public set showBordRowComponents(value: boolean) {
    this._showBordRowComponents = value;
  }
  public get updateBankDetails(): boolean {
    return this._updateBankDetails;
  }
  public set updateBankDetails(value: boolean) {
    this._updateBankDetails = value;
  }
  public get ProcessingValidation(): boolean {
    return this._ProcessingValidation;
  }
  public set ProcessingValidation(value: boolean) {
    this._ProcessingValidation = value;
  }
  public get confirmPaymentReady(): boolean {
    return this._confirmPaymentReady;
  }
  public set confirmPaymentReady(value: boolean) {
    this._confirmPaymentReady = value;
  }
  public get changeBankModel(): boolean {
    return this._changeBankModel;
  }
  public set changeBankModel(value: boolean) {
    this._changeBankModel = value;
  }
  public get displaySpinner(): boolean {
    return this._displaySpinner;
  }
  public set displaySpinner(value: boolean) {
    this._displaySpinner = value;
  }
  public get showList() {
    return this._showList;
  }
  public set showList(value: boolean) {
    this._showList = value;
  }
  public get scrollFilter(): string {
    return this._scrollFilter;
  }
  public set scrollFilter(value: string) {
    this._scrollFilter = value;
  }
  public get currentView(): string {
    return this._currentView;
  }
  public set currentView(value: string) {
    this._currentView = value;
  }
  public get getUnreleasedBatches(): boolean {
    return this._getUnreleasedBatches;
  }
  public set getUnreleasedBatches(value: boolean) {
    this._getUnreleasedBatches = value;
  }
  public get getUnPaidBatches(): boolean {
    return this._getUnPaidBatches;
  }
  public set getUnPaidBatches(value: boolean) {
    this._getUnPaidBatches = value;
  }
  public get gettingDetails(): boolean {
    return this._gettingDetails;
  }
  public set gettingDetails(value: boolean) {
    this._gettingDetails = value;
  }
  public get openDetailsModel(): boolean {
    return this._openDetailsModel;
  }
  public set openDetailsModel(value: boolean) {
    this._openDetailsModel = value;
  }
  public get membNo(): string {
    return this._membNo;
  }
  public set membNo(value: string) {
    this._membNo = value;
  }
  public get visitCounts(): number {
    return this._visitCounts;
  }
  public set visitCounts(value: number) {
    this._visitCounts = value;
  }
  public get authNo(): string {
    return this._authNo;
  }
  public set authNo(value: string) {
    this._authNo = value;
  }
  public get enableSearch(): boolean {
    return this._enableSearch;
  }
  public set enableSearch(value: boolean) {
    this._enableSearch = value;
  }
  public get openEditBatch(): boolean {
    return this._openEditBatch;
  }
  public set openEditBatch(value: boolean) {
    this._openEditBatch = value;
  }
  public get enableNavButtons(): boolean {
    return this._enableNavButtons;
  }
  public set enableNavButtons(value: boolean) {
    this._enableNavButtons = value;
  }
  public get enablesave(): boolean {
    return this._enablesave;
  }
  public set enablesave(value: boolean) {
    this._enablesave = value;
  }
  public get openUnEditBatch(): boolean {
    return this._openUnEditBatch;
  }
  public set openUnEditBatch(value: boolean) {
    this._openUnEditBatch = value;
  }
  public get lastpayedDateFound(): boolean {
    return this._lastpayedDateFound;
  }
  public set lastpayedDateFound(value: boolean) {
    this._lastpayedDateFound = value;
  }
  public get vendorInfo() {
    return this._vendorInfo;
  }
  public get SubmitCheckrunError() {
    return this._submitCheckrunError;
  }
  public set SubmitCheckrunError(value: boolean) {
    this._submitCheckrunError = value;
    if (value == false) {
      this._submitCheckrunErrorMsg = "";
    }
  }

  public get diacodes(): Diagcodes[] {
    return this._diacodes;
  }
  public set diacodes(value: Diagcodes[]) {
    this._diacodes = value;
  }

  GetAmounts() {
    this.legendDisplay = false;
    this.errorHandler = false;
    let route: any;
    this.enableDownload = true;
    this.whereClause = [];
    if ((this._excludeBankingDetails === false) || (this._excludeBankingDetails === null)) {
      route = 'GetClaims';
    } else {
      route = 'GetClaimsExcludeBanking'
    }
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    this.amounts.user = this._userName;
    this.amounts.serviceToDate = (new Date).toDateString();
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/' + route,
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.specCode = el.specCode;
          x.include = false;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0.00";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0.00";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if ((this.amounts.claimType == 'P')) {
            z.providerPayment = (parseFloat(el.sum) + parseFloat(z.providerPayment)).toFixed(2);
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.memberPayment = (parseFloat(el.sum) + parseFloat(z.memberPayment)).toFixed(2);;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        this.GetAmounts();
        this.ErrorRequets = true;
      },
      () => {
        this._whereClauseBenefit = 0;
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
        this.enableDownload = false;
        this.displaySpinner = false;
        this.readyToGenerate = false;
        this._displayFetchModel = false;
        this._wizardNext = true;
      });
  }

  GetAmountsProv() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetProvClaims',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.specCode = el.specCode;
          x.include = false;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0.00";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0.00";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if ((this.amounts.claimType == 'P')) {
            z.providerPayment = (parseFloat(el.sum) + parseFloat(z.providerPayment)).toFixed(2);
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.memberPayment = (parseFloat(el.sum) + parseFloat(z.memberPayment)).toFixed(2);;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this._whereClauseBenefit = 0;
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
        this.enableDownload = false;
        this.displaySpinner = false;
        this.readyToGenerate = false;
        this._displayFetchModel = false;
        this._wizardNext = true;
      });
  }

  GetAmountsMemb() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetMembClaims',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.include = false;
          x.specCode = el.specCode;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0.00";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0.00";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if ((this.amounts.claimType == 'P')) {
            z.providerPayment = (parseFloat(el.sum) + parseFloat(z.providerPayment)).toFixed(2);
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.memberPayment = (parseFloat(el.sum) + parseFloat(z.memberPayment)).toFixed(2);;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {

        this._whereClauseBenefit = this.benefitGridData.length;
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
        this.enableDownload = false;
        this.displaySpinner = false;
        this.readyToGenerate = false;
        this._displayFetchModel = false;
        this._wizardNext = true;
      });
  }

  GetAmountsClaims() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetClaimsNo',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.include = false;
          x.specCode = el.specCode;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            this._paymentType = "P";
            x.memberPayment = "0.00";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0.00";
            this._paymentType = "M";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if ((this.amounts.claimType == 'P')) {
            z.providerPayment = (parseFloat(el.sum) + parseFloat(z.providerPayment)).toFixed(2);
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.memberPayment = (parseFloat(el.sum) + parseFloat(z.memberPayment)).toFixed(2);;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this._whereClauseBenefit = 0;
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
        this.enableDownload = false;
        this.displaySpinner = false;
        this.readyToGenerate = false;
        this._displayFetchModel = false;
        this._wizardNext = true;
      });
  }

  GetAmountsService() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;

    this._http.post(this._baseUrl + 'api/CheckRun/GetClaimsService', this.amounts, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.specCode = el.specCode;
          x.include = false;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0.00";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0.00";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if ((this.amounts.claimType == 'P')) {
            z.providerPayment = (parseFloat(el.sum) + parseFloat(z.providerPayment)).toFixed(2);
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.memberPayment = (parseFloat(el.sum) + parseFloat(z.memberPayment)).toFixed(2);;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        this.GetAmounts();
        this.ErrorRequets = true;
      },
      () => {
        this._whereClauseBenefit = 0;
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
        this.enableDownload = false;
        this.displaySpinner = false;
        this.readyToGenerate = false;
        this._displayFetchModel = false;
        this._wizardNext = true;
      });
  }

  GetAmountsExcludeBanking() {
    this.legendDisplay = false;
    this.errorHandler = false;
    let route: any;
    this.enableDownload = true;
    this.whereClause = [];
    if ((this.excludeBankingDetails === false) || (this.excludeBankingDetails === null)) {
      route = 'GetClaims';
    } else {
      route = 'GetClaimsExcludeBanking'
    }
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/' + route,
      this.amounts
    ).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          x.include = false;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if (z.memberPayment === "0") {
            z.memberPayment = el.sum;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.providerPayment = el.sum;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        this.GetAmounts();
        this.ErrorRequets = true;
      },
      () => {


      });
  }

  GetExcludedClaims() {
    this.legendDisplay = false;
    this.errorHandler = false;
    let route: any;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    this.amounts.user = this._userName;
    this.amounts.serviceToDate = (new Date).toDateString();
    this.amounts.claimType = this._paymentType;

    this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsRec', this.amounts, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: [ExcludedClaims]) => {
      this.excludingClaims = [];
      results.forEach((el) => {
        this.excludingClaims.push(el);
      });
      if (this.excludingClaims.length > 5) {
        this._largeAmountFound = true;
      } else {
        this._largeAmountFound = false;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.GetAmounts();
      });
  }

  GetExcludedClaimsMembNo() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsMembNo', this.amounts, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: [ExcludedClaims]) => {
      this.excludingClaims = [];
      results.forEach((el) => {
        this.excludingClaims.push(el);
      });
      if (this.excludingClaims.length > 5) {
        this._largeAmountFound = true;
      } else {
        this._largeAmountFound = false;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.GetAmountsMemb();
      });
  }

  GetExcludedClaimsProv() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsProv', this.amounts, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: [ExcludedClaims]) => {
      this.excludingClaims = [];
      results.forEach((el) => {
        this.excludingClaims.push(el);
      });
      if (this.excludingClaims.length > 5) {
        this._largeAmountFound = true;
      } else {
        this._largeAmountFound = false;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.GetAmountsProv();
      });
  }

  GetExcludedClaimsServ() {
    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.claimType = this._paymentType;
    //this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsRec', this.amounts, { //Jaco 2023-11-24
    this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsSer', this.amounts, {   //Jaco 2023-11-24
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: [ExcludedClaims]) => {
      this.excludingClaims = [];
      results.forEach((el) => {
        this.excludingClaims.push(el);
      });
      if (this.excludingClaims.length > 5) {
        this._largeAmountFound = true;
      } else {
        this._largeAmountFound = false;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.GetAmountsService();
      });
  }

  GetExcludedClaimsClaimNo() {

    this.legendDisplay = false;
    this.errorHandler = false;
    this.enableDownload = true;
    this.whereClause = [];
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    let today: Date = new Date;
    this.amounts.endDate = today.toDateString();
    this.amounts.user = this._userName;
    this.amounts.serviceToDate = today.toDateString();
    this.amounts.claimType = this._paymentType;
    this._http.post(this._baseUrl + 'api/CheckRun/GetAllExcludedClaimsClaimNo', this.amounts, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((results: [ExcludedClaims]) => {
      this.excludingClaims = [];
      results.forEach((el) => {
        this.excludingClaims.push(el);
      });
      if (this.excludingClaims.length > 5) {
        this._largeAmountFound = true;
      } else {
        this._largeAmountFound = false;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.GetAmountsClaims();
      });
  }

  GetLastRecievedDate() {
    this._http.post(this._baseUrl + 'api/CheckRun/GetLastRecDate',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedDate) => {
      let x: any;
      x = recievedDate;
      this.lastRecDate = x.message;
    },
      (error) => {
        console.log(error);
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
      });
    return this.lastRecDate;
  }

  GetTotals() {
    this.totalClaims = 0;
    this.totalMembPayments = 0;
    this.totalProvPayments = 0;
    this.totalBenClaims = 0;
    this.totalbenprov = 0;
    this.tottalbenmen = 0;
    this.provTotal = 0;
    this.membTotal = 0;
    this.gridData.forEach((el) => {
      this.membTotal = this.membTotal + parseFloat(el.memberPayment);
      this.provTotal = this.provTotal + parseFloat(el.providerPayment);
      this.membTotal = +this.membTotal.toFixed(3);
      this.provTotal = +this.provTotal.toFixed(3);
    });
  }

  SetBatchDetails() {
    this._specificBatchNo = new BatchDetailsModel();
    this._specificBatchNo.batchNumber = this.batchNumber;
    this._specificBatchNo.hpCode = this._loginService.CurrentLoggedInUser.username;
    this._specificBatchNo.paymentType = this._paymentType;
    this._specificBatchNo.whereClause = "";
    let count: number = 0;
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this._specificBatchNo.whereClause = this._specificBatchNo.whereClause + wc + ",";
      } else {
        this._specificBatchNo.whereClause = this._specificBatchNo.whereClause + wc;
      }

    });
    this.amounts.sorting = 'spec';
    this._specificBatchNo.sorting = this.amounts.sorting;
    this.BordRowCreation();
  }

  GetMemberAmounts() {
    this.legendDisplay = false;
    this.errorHandler = false;
    let route: any
    if ((this.excludeBankingDetails === false) || (this.excludeBankingDetails === null)) {
      route = 'GetClaims';
    } else {
      route = 'GetClaimsExcludeBanking';
    }
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    this._http.post(this._baseUrl + 'api/CheckRun/' + route,
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0";
            x.amountOfClaims = "0";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if (z.memberPayment === "0") {
            z.memberPayment = el.sum;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.providerPayment = el.sum;
            z.amountOfClaims = (parseInt(z.amountOfClaims)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        console.log(error);
        this.message = 'Retrying to load data.';
        this.errorHandler = true;
        this.GetAmounts();
        this.ErrorRequets = true;
      },
      () => {
        this.displayData = true;
        this.message = "Loading";
        this.GetTotals();
      });
  }

  GetProviderAmounts() {
    this.legendDisplay = false;
    this.errorHandler = false;
    let route: any;
    if ((this.excludeBankingDetails === false) || (this.excludeBankingDetails === null)) {
      route = 'GetClaims';
    } else {
      route = 'GetClaimsExcludeBanking'
    }
    this.gridData.splice(0, this.gridData.length);
    this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
    this.displayData = false;
    this._http.post(this._baseUrl + 'api/CheckRun/' + route,
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: [ClaimsModel]) => {
      let x: any;
      this.amountsToDisplay = recievedData;
      this.amountsToDisplay.forEach((el) => {
        let x: ModifiedClaimsArrayModel = new ModifiedClaimsArrayModel();
        if (el.specCodeDesc !== this.tempDiscription) {
          this.tempDiscription = el.specCodeDesc;
          x.description = el.specCodeDesc;
          x.amountOfClaims = el.amountOfRecords;
          if (el.claimType === "P") {
            x.providerPayment = el.sum;
            x.memberPayment = "0";
          } else {
            x.memberPayment = el.sum;
            x.providerPayment = "0";
            x.amountOfClaims = "0";
          }
          this.gridData.push(x);
        } else {
          let y = this.gridData.length;
          let z = this.gridData[y - 1];
          if (z.memberPayment === "0") {
            z.memberPayment = el.sum;
            z.amountOfClaims = (parseInt(z.amountOfClaims) + parseInt(el.amountOfRecords)).toString();
          } else {
            z.providerPayment = el.sum;
            z.amountOfClaims = (parseInt(el.amountOfRecords)).toString();
          }
          this.gridData.pop();
          this.gridData.push(z);
        }
      });
    },
      (error) => {
        this.message = 'Retrying to load data.'
        this.errorHandler = true;
        this.GetAmounts();
        this.ErrorRequets = true;
      },
      () => {
        this.displayData = true;
        this.message = "Loading"
        this.GetTotals();
      });
  }

  GridSortingProvider() {
    this.count++;
    if (this.count === 1) {
      this.gridData.sort((a, b) => parseFloat(a.providerPayment) - parseFloat(b.providerPayment));
      this.benefitGridData.sort((a, b) => parseFloat(a.providerPayment) - parseFloat(b.providerPayment));
    } else {
      this.count = 0;
      this.gridData.sort((a, b) => parseFloat(b.providerPayment) - parseFloat(a.providerPayment));
      this.benefitGridData.sort((a, b) => parseFloat(b.providerPayment) - parseFloat(a.providerPayment));
    }
  }

  GridSortingMember() {
    this.count++;
    if (this.count === 1) {
      this.gridData.sort((a, b) => parseFloat(a.memberPayment) - parseFloat(b.memberPayment));
      this.benefitGridData.sort((a, b) => parseFloat(a.memberPayment) - parseFloat(b.memberPayment));
    } else {
      this.count = 0;
      this.gridData.sort((a, b) => parseFloat(b.memberPayment) - parseFloat(a.memberPayment));
      this.benefitGridData.sort((a, b) => parseFloat(a.memberPayment) - parseFloat(b.memberPayment));
    }
  }

  GridSortingDescription() {
    this.count++;
    if (this.count === 1) {
      this.benefitGridData.sort((a, b) => a.description < b.description ? -1 : 1);
      this.gridData.sort((a, b) => a.description < b.description ? -1 : 1);
    } else {
      this.count = 0;
      this.benefitGridData.sort((a, b) => b.description < a.description ? -1 : 1);
      this.gridData.sort((a, b) => b.description < a.description ? -1 : 1);
    }
  }

  GridSortingTotalClaims() {
    this.count++;
    if (this.count === 1) {
      this.gridData.sort((a, b) => parseFloat(a.amountOfClaims) - parseFloat(b.amountOfClaims));
      this.benefitGridData.sort((a, b) => parseFloat(a.amountOfClaims) - parseFloat(b.amountOfClaims));
    } else {
      this.count = 0;
      this.gridData.sort((a, b) => parseFloat(b.amountOfClaims) - parseFloat(a.amountOfClaims));
      this.benefitGridData.sort((a, b) => parseFloat(b.amountOfClaims) - parseFloat(a.amountOfClaims));
    }
  }

  BordRowCreation() {

    this.errorHandler = false;
    this.bordRowList.splice(0, this.bordRowList.length);
    this._uniqueList.splice(0, this._uniqueList.length);
    this._patientsPerFamily = {};
    this.loadBoardRow = true;
    this._http.post(this._baseUrl + 'api/CheckRun/BordRowCreation', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe(
        (recievedFile: [CheckrunModel]) => {
          this.initialTotalsBordRowList = [];
          this.bordRowList = recievedFile;
          this.bordRowList.forEach((br) => {
            this.initialTotalsBordRowList.push(br);
          });
          this.bordRowList.forEach((el) => {
            el.dateOfClaimReceived = el.dateOfClaimReceived.substr(0, 10);
            el.dateOfClaimPaid = el.dateOfClaimPaid.substr(0, 10);
            el.dateofIncident = el.dateofIncident.substr(0, 10);
            el.patientBirth = el.patientBirth.substr(0, 10);
          });
          this._pageCounter = 0;
          this._pageScrollCounter = 1;
          this.bordRowList.forEach((el) => {
            el.page = this._pageScrollCounter;
            this._pageCounter++;
            if (this._pageCounter === 16) {
              this._pageCounter = 0;
              this._pageScrollCounter++;
            }
          });
        },
        (error) => {
          this.errorHandler = true;
          console.log(error);
          this.ErrorRequets = true;
        },
        () => {
          this._totalPayableAmount = 0;
          this.CalculateAmountsForBordRow();
          this.GenerateUniqueList();
          this.GenerateUniqueProviderList();
          this.FilterBordRowOnIncrease(1);
          this.loadBoardRow = false;
          this._paymentType = this.initialTotalsBordRowList[0].type.toUpperCase();
        });
  }

  GenerateBatchNumber() {
    this.amounts.user = this._loginService.CurrentLoggedInUser.username;
    this.amounts.claimType = this._paymentType;
    let count: number = 0;
    this.amounts.whereClause = ""
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this.amounts.whereClause = this.amounts.whereClause + wc + ",";
      } else {
        this.amounts.whereClause = this.amounts.whereClause + wc;
      }

    });

    this.amounts.sorting = "spec";
    this.loadBoardRow = true;
    this.errorHandler = false;
    this.enableDownload = true;
    this._http.post(this._baseUrl + 'api/CheckRun/GenerateBatchNumber', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (recievedData: ValidationResultsModel) => {
        let v: any;
        v = recievedData;
        this.batchNumber = v.message;
      },
      (error) => {
        console.log(error);
        this.enableDownload = true;
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.enableDownload = false;
        this.SetBatchDetails();
        this._wizardNext = true;
      });
  }

  GenerateBatchNumberService() {
   let  e = new Date(this.amounts.serviceToDate )
    this.amounts.serviceToDate = formatDate(e.toDateString(), "yyyy/MM/dd", 'en-za', '+0200').toString();
    this.amounts.user = this._loginService.CurrentLoggedInUser.username;
    this.amounts.claimType = this._paymentType;
    let count: number = 0;
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    this.amounts.whereClause = ""
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this.amounts.whereClause = this.amounts.whereClause + wc + ",";
      } else {
        this.amounts.whereClause = this.amounts.whereClause + wc;
      }

    });

    this.amounts.sorting = "spec";
    this.loadBoardRow = true;
    this.errorHandler = false;
    this.enableDownload = true;
    this._http.post(this._baseUrl + 'api/CheckRun/GenerateBatchNumberServiceDate', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (recievedData: ValidationResultsModel) => {
        let v: any;
        v = recievedData;
        this.batchNumber = v.message;
      },
      (error) => {
        console.log(error);
        this.enableDownload = true;
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.enableDownload = false;
        this.SetBatchDetails();
        this._wizardNext = true;
      });
  }

  GenerateBatchNumberProv() {
    this.amounts.user = this._loginService.CurrentLoggedInUser.username;
    this.amounts.claimType = this._paymentType;
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    let count: number = 0;
    this.amounts.whereClause = ""
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this.amounts.whereClause = this.amounts.whereClause + wc + ",";
      } else {
        this.amounts.whereClause = this.amounts.whereClause + wc;
      }

    });
    this.loadBoardRow = true;
    this.errorHandler = false;
    this.enableDownload = true;
    this.amounts.sorting = "spec";
    this._http.post(this._baseUrl + 'api/CheckRun/GenerateBatchNumberProvider', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (recievedData: ValidationResultsModel) => {
        let v: any;
        v = recievedData;
        this.batchNumber = v.message;
      },
      (error) => {
        console.log(error);
        this.enableDownload = true;
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.enableDownload = false;
        this.SetBatchDetails();
      });
  }

  GenerateBatchNumberMemb() {
    this.amounts.user = this._loginService.CurrentLoggedInUser.username;
    this.amounts.claimType = this._paymentType;
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    let count: number = 0;
    this.amounts.whereClause = ""
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this.amounts.whereClause = this.amounts.whereClause + wc + ",";
      } else {
        this.amounts.whereClause = this.amounts.whereClause + wc;
      }

    });
    this.loadBoardRow = true;
    this.errorHandler = false;
    this.enableDownload = true;
    this.amounts.sorting = "spec";
    this._http.post(this._baseUrl + 'api/CheckRun/GenerateBatchNumberMember', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (recievedData: ValidationResultsModel) => {
        let v: any;
        v = recievedData;
        this.batchNumber = v.message;
      },
      (error) => {
        console.log(error);
        this.enableDownload = true;
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.enableDownload = false;
        this.SetBatchDetails();
      });
  }

  GenerateBatchNumberClaim() {
    this.amounts.user = this._loginService.CurrentLoggedInUser.username;
    this.amounts.claimType = this._paymentType;
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    let count: number = 0;
    this.amounts.whereClause = ""
    this.whereClause.forEach((wc) => {
      count++;
      if (count < this.whereClause.length) {
        this.amounts.whereClause = this.amounts.whereClause + wc + ",";
      } else {
        this.amounts.whereClause = this.amounts.whereClause + wc;
      }

    });
    this.loadBoardRow = true;
    this.errorHandler = false;
    this.enableDownload = true;
    this.amounts.sorting = "spec";
    this._http.post(this._baseUrl + 'api/CheckRun/GenerateBatchNumberClaim', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe(
      (recievedData: ValidationResultsModel) => {
        let v: any;
        v = recievedData;
        this.batchNumber = v.message;
      },
      (error) => {
        console.log(error);
        this.enableDownload = true;
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.enableDownload = false;
        this.SetBatchDetails();
      });
  }

  CalculateAmountsForBordRow() {
    this._totalPayableAmount = 0;
    this._amountOfClaims = this.bordRowList.length;
    this._amountOfReversals = 0
    this.bordRowList.forEach((el) => {
      this._totalPayableAmount = this._totalPayableAmount + parseFloat(el.amountOfClaimApproved);
      if (parseFloat(el.amountOfClaimApproved) < 0) {
        this._amountOfReversals++;
      }
    });
  }

  GenerateUniqueList() {
    let x: string;
    this.bordRowList.sort((a, b) => a.mainPolicyNumber < b.mainPolicyNumber ? -1 : 1);
    this._pageCounter = 1;
    this._pageNumber = 1;
    this.bordRowList.forEach((el) => {
      if (el.mainPolicyNumber !== x) {
        x = el.mainPolicyNumber;
        el.mainLineId = this._uniqueList.length + 1;
        this._uniqueList.push(
          {
            mainPolicyNumber: el.mainPolicyNumber,
            nameSurnamePrincipleMember: el.nameSurnamePrincipleMember,
            bankAccountName: el.bankAccountName,
            bankAccountNumber: el.bankAccountNumber,
            bankAccountBranchCode: el.bankAccountBranchCode,
            page: this._pageNumber,
            validated: el.validated,
            exclude: el.excluded,
            rejected: el.rejected,
          });
        this._pageCounter++;
        if (this._pageCounter === 16) {
          this._pageCounter = 1;
          this._pageNumber++;
        }
      }
    });
    this._patientsPerFamily = {};
    this._uniqueList.forEach((family) => {
      let valuePatientArray = this.FindPatientsForPriciple(family.mainPolicyNumber);
      let key = family.mainPolicyNumber;
      if (key in this._patientsPerFamily) {
        this._patientsPerFamily[key] += valuePatientArray;
      } else {
        this._patientsPerFamily[key] = valuePatientArray;
      }
    });
    this.FilterDictOnIncrease(1);
    this.initailSotingOfUniqueList = [];
    this._uniqueList.forEach((ul) => {
      this.initailSotingOfUniqueList.push(ul);
    });
    this.initailSortingOfPatientList = this._patientsPerFamily;
  }

  GenerateUniqueProviderList() {
    let x: string;
    this.bordRowList.sort((a, b) => a.provId < b.provId ? -1 : 1);
    this._pageCounter = 1;
    this._providerPageCount = 1;
    this.bordRowList.forEach((el) => {
      if (el.provId !== x) {
        x = el.provId;
        el.mainLineId = this._uniqueProvList.length + 1;
        this._uniqueProvList.push(
          {
            bankName: el.bankAccountName,
            accNo: el.bankAccountNumber,
            branch: el.bankAccountBranchCode,
            providerID: el.provId,
            refProvId: el.refProvId,
            practiceName: el.providerName,
            page: this._providerPageCount,
            validated: el.validated,
            excluded: el.excluded,
            rejected: el.rejected,
            hasBanking: false,
            show: true

          });
        this._pageCounter++;
        if (this._pageCounter === 16) {
          this._pageCounter = 1;
          this._providerPageCount++;
        }
      }
    });

    this._patientsPerProvider = {};
    this._uniqueProvList.forEach((provider) => {
      let valuePatientArray = this.FindPatientsForProvider(provider.providerID);
      let key = provider.providerID;
      if (key in this._patientsPerProvider) {
        this._patientsPerProvider[key] += valuePatientArray;
      } else {
        this._patientsPerProvider[key] = valuePatientArray;
      }
    });

    this.initialSortingOfProvList = this._patientsPerProvider;
    this.FilterProvDictOnIncrease(1);
    this.initialSortingOfProviderGroup = []
    this._uniqueProvList.forEach((ul) => {
      this.initialSortingOfProviderGroup.push(ul);
    });
    this.showText = true;
    this.loadBoardRow = false;
    this.displayData = false;
    this.legendDisplay = true;

    this._uniqueProvList.forEach((prov) => {

      let t = this._patientsPerProvider[prov.providerID];
      prov.rejected
      t.forEach((clm) => {
        if ((clm.rejected === true) && (clm.amountOfClaimPaid === '0.00')) {
          clm.rejected = true;
        } else {
          clm.rejected = false;
        }
      })

    });
    this._totalAmountToPay = 0.00;
    this._uniqueProvList.forEach((prov) => {

      let t = this._patientsPerProvider[prov.providerID];

      t.forEach((el) => {
        if ((el.validated == true) && (el.excluded == false) && (el.rejected == false)) {
          this._totalAmountToPay = this._totalAmountToPay + parseFloat(el.amountOfClaimApproved);
        }

      })


    });

    this._uniqueProvList.forEach((pl) => {
      if ((pl.bankName !== "") && (pl.bankName !== undefined) && (pl.bankName !== undefined)) {
        let p: string = "";
        pl.hasBanking = true;
        pl.bankName = p.padEnd(pl.bankName.length, '*');
        p = "";
        pl.accNo = p.padEnd(pl.accNo.length, '*');
        p = "";
        pl.branch = p.padEnd(pl.branch.length, '*');
      } else {
        pl.hasBanking = false;
      }
    });
  }

  FindPatientsForProvider(provId: string): Array<CheckrunModel> {
    let patients: CheckrunModel[] = [];
    patients = this.bordRowList.filter((bordrow) => {
      return bordrow.provId == provId;
    });
    return patients;

  }

  FindPatientsForPriciple(mainPolicyNumber: string): Array<PatientModel> {
    let patients: Array<CheckrunModel> = this.bordRowList.filter((bordRow) => {
      return bordRow.principleMainMemberNumber == mainPolicyNumber;
    });
    let count = 0;
    let uniquePats: Array<PatientModel> = [];
    patients = patients.sort((a, b) => b.patientMembNo < a.patientMembNo ? -1 : 1);
    patients.forEach((el) => {
      let newPat = true;
      uniquePats.forEach((u) => {
        if (el.patientMembNo === u.patientMembNo) {
          newPat = false;
        }
      });
      let details: Array<CheckrunModel> = this.bordRowList.filter((bordRow) => {
        return bordRow.patientMembNo == el.patientMembNo;
      });
      if (newPat) {
        uniquePats.push(
          {
            patientMembNo: el.patientMembNo,
            idNumberofMember: el.idNumberofMember,
            patientName: el.patientName,
            patientBirth: el.patientBirth,
            claimNo: el.claimNumber,
            amountToPay: el.amountOfClaimApproved,
            amoutPayed: el.amountOfClaimPaid,
            detailLineArray: details,
            validated: el.validated,
            exclude: el.excluded,
            reject: el.rejected,
            rowCount: count
          }
        );

        count++
      }
    });
    uniquePats.forEach((up) => {
      up.detailLineArray.forEach((dl) => {
        if ((dl.validated === true) && ((dl.excluded === false) && (dl.rejected === false))) {
          this._totalAmountToPay = this._totalAmountToPay + parseFloat(dl.amountOfClaimApproved);
        }
      });
    });
    return uniquePats;
  }

  FindClaimPerPatient(mainMembNo: string, membId: string): [CheckrunModel] {
    let y = this._patientsPerFamily[mainMembNo];
    let x = y[0].detailLineArray;
    let count = 0;
    y.forEach((el) => {
      if (el.patientMembNo === membId) {
        el.detailLineArray.sort((a, b) => a.claimNumber - b.claimNumber);
        el.detailLineArray.forEach((dl) => {
          dl.rowCount = count;
          count++;
        });
        x = el.detailLineArray;
      }
    });
    return x;
  }

  FilterDictOnIncrease(pageNum: number) {
    let u = this._uniqueList.filter(el =>
      el.page === pageNum
    );
    this._filtteredList = u;
  }

  FilterProvDictOnIncrease(pageNum: number) {
    let p = this._uniqueProvList.filter(pl =>
      pl.page === pageNum
    );
    this._fillteredProvList = p;
    this._currentPage = 1;
  }

  RejectClaim(rejectClaimList: CheckrunModel[]) {

    rejectClaimList.forEach((el) => {
      el.amountOfClaimPaid = el.amountOfClaimPaid.toString();
      /*el.amountOfClaimApproved = el.amountOfClaimApproved.toString();
      el.amountOfClaimReported = el.amountOfClaimReported.toString();
      el.bankAccountBranchCode = el.bankAccountBranchCode.toString();
      el.bankAccountName = el.bankAccountName.toString();
      el.bankAccountNumber = el.bankAccountNumber.toString();
      el.batchNo = el.batchNo.toString();
      el.claimAmountOutstanding = el.claimAmountOutstanding.toString();
      el.claimNumber = el.claimNumber.toString();
      el.claimStatus = el.claimStatus.toString();
      el.claimType = el.claimType.toString();
      el.dateOfClaimPaid = el.dateOfClaimPaid.toString();
      el.dateOfClaimReceived = el.dateOfClaimReceived.toString();
      el.dateofIncident = el.dateofIncident.toString();
      el.idNumberofDeceased = el.idNumberofDeceased.toString();
      el.idNumberofMember = el.idNumberofMember.toString();
      el.mainLineId = el.mainLineId.toString();
      el.mainPolicyNumber = el.mainPolicyNumber.toString();
      el.nameSurnamePrincipleMember = el.nameSurnamePrincipleMember.toString();
      el.namSurnameofdeceased = el.namSurnameofdeceased.toString();*/
      el.reverse = el.reverse.toString();

    })
    let f = this;
    rejectClaimList.forEach((rl) => {
      rl.note = this._rejectionNote;
      rl.rejReason = this._rejectionReason;
      rl.batchNo = this.batchNumber;
    });
    this._http.post(this._baseUrl + "api/CheckRun/RejectClaim", rejectClaimList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((RecievedResult: ValidationResultsModel) => {
      let c: any = this._patientsPerProvider[this._rejectProv];
      c.forEach((el) => {
        if ((el.patientMembNo == f._rejectmembNo) && (el.rowId == f._rowId)) {
          el.amountOfClaimPaid = RecievedResult.message;
          el.amountOfClaimApproved = RecievedResult.message;
          if ((el.validated == true) || (el.validate == true)) {
            f._totalAmountToPay = f._totalAmountToPay + parseFloat(el.amountOfClaimApproved);
          }
        }
      });
    },
      (error) => { console.log(error) });
  }

  ExcludeClaim(excludeClaimList: CheckrunModel[]) {
    excludeClaimList.forEach((el) => {
      el.batchNo = this.batchNumber;
    });
    this._http.post(this._baseUrl + "api/CheckRun/ExcludeClaim", excludeClaimList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json"

        })
      }
    ).subscribe((RecievedResult: ValidationResultsModel) => {
    });
  }

  ValidateClaim(ValidatedList: CheckrunModel[]) {
    ValidatedList.forEach((vl) => {
      vl.batchNo = this.batchNumber;
    });
    this._http.post(this._baseUrl + "api/CheckRun/ValidateClaim", ValidatedList,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"


        })
      }
    ).subscribe((RecievedResult: ValidationResultsModel) => {
    });
  }

  GetListOfPaidBatches() {
    this.showList = true;
    this.showBordRowComponents = false;
    this._paidBatches = [];
    let y: Date;
    y = new Date;
    this.amounts.endDate = formatDate(y.toDateString(), "yyyy/MM/dd", 'en-za', '+0200').toString();
    this.amounts.serviceToDate = formatDate(y.toDateString(), "yyyy/MM/dd", 'en-za', '+0200').toString();

    if (this.amounts.endDate == null || this.amounts.endDate == undefined || this.amounts.endDate == "") {
      this.amounts.endDate = "";
    }
    this._http.post(this._baseUrl + 'api/CheckRun/GetPaidBatches',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json"

        })
      }
    ).subscribe((recievedList: CheckRunTypeModel[]) => {
      this._filteredPaidList = [];
      this._paid = {};
      let v = recievedList;
      this._paidBatchesType = v;
      this._paidBatchesType.forEach((pb) => {
        let key2: string = pb.batchNo.substr(0, 6);
        let contains: boolean = this._filteredPaidList.some(y => y.yearMont === key2);
        if (contains) {
          this._filteredPaidList.forEach((fl) => {
            if (fl.yearMont === key2) {
              let include = fl.list.some(y => y === pb);
              if (include === false) {
                fl.list.push(pb);
              }
            }
          });
        } else {
          let d: CheckRunTypeModel[] = [];
          d.push(pb);
          this._filteredPaidList.push({
            yearMont: key2,
            list: d
          });
        }
      });
      this._paid = {};
      this._filteredPaidList.forEach((flp) => {
        let batchNumbers = flp.list;
        let indexer = flp.yearMont;
        this._paid[indexer] = batchNumbers

      });
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {

        this.showBordRowComponents = true;
        this.CheckCheckRunTypeArrayPaid(this._paidBatchesType);
        this.GetUnreleasedBatches()
      });
  }

  GetUnreleasedBatches() {
    this.getUnreleasedBatches = true;
    this._unreleasedBatches = [];
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    this.amounts.serviceToDate = y.toDateString();
    this._http.post(this._baseUrl + 'api/CheckRun/GetUnreleasedBatches',
      this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedList: CheckRunTypeModel[]) => {
      let v = recievedList;
      this._unreleasedBatchesType = v;
      this.inprocessCOunt = this._unreleasedBatchesType.length;

    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.showBordRowComponents = true;
        this.showList = false;
        this.getUnreleasedBatches = false;
        this.GetUnpaidBatches();
      });
  }

  GetUnpaidBatches() {
    this.getUnPaidBatches = true;
    this._unpaidBatches = [];
    let y: Date;
    y = new Date;
    this.amounts.endDate = y.toDateString();
    this.amounts.serviceToDate = y.toDateString();
    this._http.post(this._baseUrl + 'api/CheckRun/GetUnPaidBatches', this.amounts,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedList: CheckRunTypeModel[]) => {
      let v = recievedList;
      this._unpaidBatchesType = v;
      this.unpaidCount = this._unpaidBatchesType.length;
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.showBordRowComponents = true;
        this.showList = false;
        this.getUnPaidBatches = false;
      });
  }

  GenerateBordRowWithSpecificBatchNo(batchNo: string) {
    this._specificBatchNo = new BatchDetailsModel();
    this._unreleasedBatchesType.forEach((bc) => {
      if (bc.batchNo == batchNo) {
        this._specificBatchNo.paymentType = bc.xtype;
        this._paymentType = bc.xtype;
      }
    })
    this.displayData = false;
    this.displaySpinner = true;
    this.batchNumber = batchNo;
    this.bordRowList.splice(0, this.bordRowList.length);
    this._specificBatchNo.batchNumber = batchNo;
    this._specificBatchNo.hpCode = this._loginService.CurrentLoggedInUser.username;
    this._http.post(this._baseUrl + 'api/CheckRun/GetSpecificBatch', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedList: CheckrunModel[]) => {
        this.initialTotalsBordRowList = [];
        this.bordRowList = recievedList;
        this.bordRowList.forEach((br) => {
          this.initialTotalsBordRowList.push(br);
        });
        this.bordRowList.forEach((el) => {
          el.dateOfClaimReceived = el.dateOfClaimReceived.substr(0, 10);
          el.dateOfClaimPaid = el.dateOfClaimPaid.substr(0, 10);
          el.dateofIncident = el.dateofIncident.substr(0, 10);
          el.patientBirth = el.patientBirth.substr(0, 10);
        });
        this._pageCounter = 0;
        this._pageScrollCounter = 1;
        this.bordRowList.forEach((el) => {
          el.page = this._pageScrollCounter;
          this._pageCounter++;
          if (this._pageCounter === 16) {
            this._pageCounter = 0;
            this._pageScrollCounter++;
          }
        });
      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {

          this._totalPayableAmount = 0;
          this.CalculateAmountsForBordRow();
          this.CheckCheckRunType();
          this.GenerateUniqueList();
          this.GenerateUniqueProviderList();
          this.FilterBordRowOnIncrease(1);
          this._displayFetchModel = false;
          this.GetLastEnteredPayDate();
          this.GetExistingBankingDetails();
        });
  }

  GetExistingBankingDetails() {

    this._http.post(this._baseUrl + 'api/CheckRun/CheckUpdatedBankingDetails', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })

      }).subscribe((returnedList: string[]) => {

        let provList: string[] = [];

        if (returnedList.length !== 0) {
          returnedList.forEach((el) => {
            this.bordRowList.forEach((pl) => {

              if (el == pl.claimNumber) {
                provList.push(pl.provId);
              }
            })
          });

          provList.forEach((ll) => {
            this._uniqueProvList.forEach((ul) => {
              if (ll == ul.providerID) {
                ul.hasBanking = true;
              }
            })
          });

          provList.forEach((ll) => {
            this._fillteredProvList.forEach((ul) => {
              if (ll == ul.providerID) {
                ul.hasBanking = true;
              }
            })
          });
        }


        // console.log(provList);
      });
  }

  ChangeBankingDetail() {
    let p = this;
    this.updateBankDetails = true;
    this.newBankinDetails.user = this._loginService.CurrentLoggedInUser.username;
    this._http.post(this._baseUrl + "api/CheckRun/ChangeBankingDetails",
      this.newBankinDetails,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.changeBankModel = false;
        this.updateBankDetails = false;
        this._uniqueProvList.forEach((el) => {
          if (el.providerID === p._provId) {
            el.hasBanking = true;
            el.show = false;

          }
        });
        this._fillteredProvList.forEach((el) => {
          if (el.providerID === p._provId) {
            el.hasBanking = true;
            el.show = false;

          }
        });

      });
  }

  CheckIfAllClaimsAreValidated() {
    this.ProcessingValidation = true;
    this._specificBatchNo.batchNumber = this.batchNumber;
    this._http.post(this._baseUrl + 'api/CheckRun/CheckValidation',
      this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {

      if (results.valid) {
        this.validationMessage = "You Are About To Mark This Batch Ready For Payment";
        this.confirmPaymentReady = false;
      } else {
        this.validationMessage =
          "Your Request Could Not Be Proccessed, Make Sure All Claims in This Batch is Validated...";
        this.confirmPaymentReady = true;
      }
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {
        this.ProcessingValidation = false;
      });
  }

  async MarkBatchAsReadyToPay() {
    this._displayFetchModel = true;
    await
      this._http.post(this._baseUrl + 'api/CheckRun/MarkBatchReady', this._specificBatchNo,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }
      ).toPromise()
        .then(
          (result: ValidationResultsModel) => {
            if (!result.valid) {
              this._submitCheckrunError = true;
              this._submitCheckrunErrorMsg = result.message;
            }
            else {
              this.openWarningPayReady = false;
              this.bordRowList.splice(0, this.bordRowList.length);
              this._uniqueList.splice(0, this._uniqueList.length);
              this._filtteredList = [];
              this._patientsPerFamily = {};
              this.showText = false;
              this.tempDiscription = '';
              this.displaySpinner = true;
              this.gridData.splice(0, this.gridData.length);
              this.amountsToDisplay.splice(0, this.amountsToDisplay.length);
              this.GetAmounts();
              this._paidBatches = [];
              this._paid = {};
              this._unreleasedBatches = [];
              this._unpaidBatches = [];
              this.GetListOfPaidBatches();
              this.closeCheckRun = true;
              this._filtteredList.splice(0, this._filtteredList.length);
              this._patientsPerFamily = {};
              this._uniqueList.splice(0, this._uniqueList.length);
              this.displayData = false;
              this.showText = false;
              this._totalAmountToPay = 0;
              this._paymentType = "";
              this._providerGrouping = false;
              this._fillteredProvList.splice(0, this._fillteredProvList.length);
              this._uniqueProvList.splice(0, this._uniqueProvList.length);
              this._patientsPerProvider = {};
              this._filteredBordRowList.splice(0, this._filteredBordRowList.length);
              this._scrollableView = false;
              this._isProvider = false;
              this.displaySpinner = false;              
            }
          },
          (error: HttpErrorResponse) => {
            console.log(error);
            //this.ErrorRequets = true;
            this._submitCheckrunError = true;
            this._submitCheckrunErrorMsg = error.message;
          }
      );

    this._displayFetchModel = false;
  }  

  GetExistingBankDetails() {

    this._http.post(this._baseUrl + 'api/CheckRun/GetEditedBankingDetails', this.newBankinDetails,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: BankingDetailModel) => {
      this.existingDetails = new BankingDetailModel();
      this.existingDetails.bankName = recievedData.bankName;
      this.existingDetails.branchCode = recievedData.branchCode;
      this.existingDetails.accountNo = recievedData.accountNo;
    },
      (error) => {
        console.log(error);
        this.ErrorRequets = true;
      },
      () => {

      });

  }

  private _currVenDetails: BankingDetailModel = new BankingDetailModel();
  public get currVenDetails(): BankingDetailModel {
    return this._currVenDetails;
  }
  public set currVenDetails(value: BankingDetailModel) {
    this._currVenDetails = value;
  }

  GetVendorName(claimType: string, claimNo: string, provId: string, bankName: string, accNr: string, branchCode: string) {

    this._vendBankingDetails = new BankingDetailModel();

    this._vendBankingDetails.claimType = claimType;
    this._vendBankingDetails.provId = provId;
    this._vendBankingDetails.claimno = claimNo;
    this._vendBankingDetails.bankName = bankName;
    this._vendBankingDetails.accountNo = accNr;
    this._vendBankingDetails.branchCode = branchCode;

    if (claimType.toUpperCase() == 'P') {
      this._http.post(this._baseUrl + 'api/CheckRun/FindProvider', this._vendBankingDetails,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + this.token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }
      ).subscribe((recievedData: BankingDetailModel) => {
        this._currVenDetails = new BankingDetailModel();
        this._currVenDetails = recievedData;



      },
        (error) => {
          console.log(error);
          this.ErrorRequets = true;
        },
        () => {
          this.changeBankModel = true;
        });
    } else {
      this.changeBankModel = true;
    }
  }

  AllValidatedFilter() {
    this.currentFilter = "Show all validated claims";
    this._pageNumber = 1;
    this._pageCounter = 1;
    this._uniqueList.sort((a, b) => {
      return (a.validated === b.validated) ? 0 : a.validated ? -1 : 1;
    });
    this._uniqueList.forEach((ul) => {
      ul.page = this._pageNumber;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageNumber++;
      }
    });
    for (var pat in this._patientsPerFamily) {
      this._patientsPerFamily[pat].sort((a, b) => {
        return (a.validated === b.validated) ? 0 : a.validated ? -1 : 1;
      });
    }
    this._filtteredList = this._uniqueList;
    this.FilterDictOnIncrease(1);
  }

  AllValidatedFilterProvider() {
    this.currentProvFilter = "Show all validated claims";
    this._providerPageCount = 1;
    this._pageCounter = 1;
    this._uniqueProvList.sort((a, b) => a.validated > b.validated ? -1 : 1);
    this._uniqueProvList.forEach((ul) => {
      ul.page = this._providerPageCount;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._providerPageCount++;
      }
    });
    for (var pat in this._patientsPerProvider) {
      this._patientsPerProvider[pat].sort((a, b) => a.validated > b.validated ? -1 : 1);
    }
    this._fillteredProvList = this._uniqueProvList;
    this.FilterProvDictOnIncrease(1);
  }

  AllValidatedFilterScroll() {
    this.currentProvFilter = "Show all validated claims";
    this._pageScrollCounter = 1;
    this._pageCounter = 1;
    this.bordRowList.sort((a, b) => {
      return (a.validated === b.validated) ? 0 : a.validated ? -1 : 1;
    });
    this.bordRowList.forEach((ul) => {
      ul.page = this._pageScrollCounter;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageScrollCounter++;
      }
    });
    this._filteredBordRowList = this.bordRowList;
    this.FilterBordRowOnIncrease(1);
  }

  AllExcludedFilter() {
    this.currentFilter = "Show all Excluded claims";
    this._pageNumber = 1;
    this._pageCounter = 1;
    this._uniqueList.sort((a, b) => {
      return (a.exclude === b.exclude) ? 0 : a.exclude ? -1 : 1;
    });
    this._uniqueList.forEach((ul) => {
      ul.page = this._pageNumber;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageNumber++;
      }
    });
    this._filtteredList = this._uniqueList;
    this.FilterDictOnIncrease(1);
  }

  AllexcludedFilterProvider() {
    this.currentProvFilter = "Show all Excluded claims";
    this._providerPageCount = 1;
    this._pageCounter = 1;
    this._uniqueProvList.sort((a, b) => a.excluded > b.excluded ? -1 : 1);
    this._uniqueProvList.forEach((ul) => {
      ul.page = this._providerPageCount;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._providerPageCount++;
      }
    });
    this._fillteredProvList = this._uniqueProvList;
    this.FilterProvDictOnIncrease(1);
  }

  AllexcludedFilterScroll() {
    this.currentProvFilter = "Show all Excluded claims";
    this._pageScrollCounter = 1;
    this._pageCounter = 1;
    this.bordRowList.sort((a, b) => {
      return (a.excluded === b.excluded) ? 0 : a.excluded ? -1 : 1;
    });
    this.bordRowList.forEach((ul) => {
      ul.page = this._pageScrollCounter;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageScrollCounter++;
      }
    });
    this._filteredBordRowList = this.bordRowList;
    this.FilterBordRowOnIncrease(1);
  }

  AllRejectedFilter() {
    this.currentFilter = "Show all rejected claims";
    this._pageNumber = 1;
    this._pageCounter = 1;
    this._uniqueList.sort((a, b) => {
      return (a.rejected === b.rejected) ? 0 : a.rejected ? -1 : 1;
    });
    this._uniqueList.forEach((ul) => {
      ul.page = this._pageNumber;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageNumber++;
      }
    });
    this._filtteredList = this._uniqueList;
    this.FilterDictOnIncrease(1);
  }

  AllRejectedFilterProvider() {
    this.currentProvFilter = "Show all rejected claims";
    this._providerPageCount = 1;
    this._pageCounter = 1;
    this._uniqueProvList.sort((a, b) => a.rejected > b.rejected ? -1 : 1);
    this._uniqueProvList.forEach((ul) => {
      ul.page = this._providerPageCount;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._providerPageCount++;
      }
    });
    this._fillteredProvList = this._uniqueProvList;
    this.FilterProvDictOnIncrease(1);
  }

  AllRejectedFilterScroll() {
    this.currentProvFilter = "Show all rejected claims";
    this._pageScrollCounter = 1;
    this._pageCounter = 1;
    this.bordRowList.sort((a, b) => {
      return (a.rejected === b.rejected) ? 0 : a.rejected ? -1 : 1;
    });
    this.bordRowList.forEach((ul) => {
      ul.page = this._pageScrollCounter;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageScrollCounter++;
      }
    });
    this._filteredBordRowList = this.bordRowList;
    this.FilterBordRowOnIncrease(1);
  }

  AllNonValidatedFilter() {
    this.currentFilter = "Show all unvalidated claims";
    this._pageNumber = 1;
    this._pageCounter = 1;
    this._uniqueList.sort((a, b) => {
      return (a.validated === b.validated) ? 0 : a.validated ? 1 : -1;
    });
    this._uniqueList.forEach((ul) => {
      ul.page = this._pageNumber;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageNumber++;
      }
    });
    this._filtteredList = this._uniqueList;
    this.FilterDictOnIncrease(1);
  }

  AllNonValidatedFilterProvider() {
    this.currentProvFilter = "Show all validated claims";
    this._providerPageCount = 1;
    this._pageCounter = 1;
    this._uniqueProvList.sort((a, b) => a.validated < b.validated ? -1 : 1);
    this._uniqueProvList.forEach((ul) => {
      ul.page = this._providerPageCount;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._providerPageCount++;
      }
    });

    this._fillteredProvList = this._uniqueProvList;
    this.FilterProvDictOnIncrease(1);
  }

  AllNonValidatedFilterScroll() {
    this.currentProvFilter = "Show all validated claims";
    this._pageScrollCounter = 1;
    this._pageCounter = 1;
    this.bordRowList.sort((a, b) => {
      return (a.validated === b.validated) ? 0 : a.validated ? 1 : -1;
    });
    this.bordRowList.forEach((ul) => {
      ul.page = this._pageScrollCounter;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageScrollCounter++;
      }
    });

    this._fillteredProvList = this._uniqueProvList;
    this.FilterProvDictOnIncrease(1);
  }

  RemoveFilters() {
    this.currentFilter = "No Filter Applied";
    let tempArr: FamilyModel[] = [];
    this._pageNumber = 1;
    this._pageCounter = 1;

    this.initailSotingOfUniqueList.forEach((iul) => {
      this._uniqueList.forEach((ul) => {
        if (ul.mainPolicyNumber === iul.mainPolicyNumber) {
          tempArr.push(ul);
        }
      });
    });
    tempArr.forEach((ta) => {
      ta.page = this._pageNumber;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageNumber++;
      }
    });
    this._uniqueList = [];
    tempArr.forEach((ta) => {
      this._uniqueList.push(ta);
    });
    this.FilterDictOnIncrease(1);
  }

  RemoveFiltersProvider() {
    this.currentProvFilter = "No Filter Applied";
    let tempArr: ProviderList[] = [];
    this._providerPageCount = 1;
    this._pageCounter = 1;

    this.initialSortingOfProviderGroup.forEach((iul) => {
      this._uniqueProvList.forEach((ul) => {
        if (ul.providerID === iul.providerID) {
          tempArr.push(ul);
        }
      });
    });
    tempArr.forEach((ta) => {
      ta.page = this._providerPageCount;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._providerPageCount++;
      }
    });
    this._uniqueProvList = [];
    tempArr.forEach((ta) => {
      this._uniqueProvList.push(ta);
    });
    this.FilterProvDictOnIncrease(1);
  }

  RemoveFiltersScroll() {
    this.currentProvFilter = "No Filter Applied";
    let tempArr: CheckrunModel[] = [];
    this._pageScrollCounter = 1;
    this._pageCounter = 1;

    this.initialTotalsBordRowList.forEach((iul) => {
      this.bordRowList.forEach((ul) => {
        if ((ul.mainPolicyNumber === iul.mainPolicyNumber) && (ul.claimNumber === iul.claimNumber) && (ul.patientMembNo === iul.patientMembNo)) {
          tempArr.push(ul);
        }
      });
    });
    tempArr.forEach((ta) => {
      ta.page = this._pageScrollCounter;
      this._pageCounter++;
      if (this._pageCounter === 16) {
        this._pageCounter = 1;
        this._pageScrollCounter++;
      }
    });
    this.FilterBordRowOnIncrease(1);
  }

  ClearAllOnNew() {
    this.bordRowList.splice(0, this.bordRowList.length);
    this._patientsPerFamily = {};
    this._paid = {};
    this._unreleased = {};
    this._unpaid = {};
    this._paidBatches.splice(0, this._paidBatches.length);
    this._unreleasedBatches.splice(0, this._unreleasedBatches.length);
    this.batchNumber = "";
    this._currentFilter = "No Filter Applied";
    this.newBankinDetails = new BankingDetailModel();
    this.existingDetails = new BankingDetailModel();
    this.GetListOfPaidBatches();
    this.gridData.splice(0, this.gridData.length);
    this.lastRecDate = '';
    this.totalMembPayments = 0;
    this.totalProvPayments = 0;
    this.showText = false;
    this.showList = false;

  }
  
  HpCodes() {
    this._hpListShow = false;
    let token = localStorage.getItem("jwt");
    let x: RejectionCodesModel;
    x = new RejectionCodesModel();
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })

      }).
      subscribe((returnedList: [HealthplanModel]) => {
        this.HpCodesList = [];
        this.HpCodesList = returnedList;

      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {
          this.userName = this._loginService.CurrentLoggedInUser.username;
          this.amounts.user = this.userName;
          this.amounts.hpCode = this.HpCodesList[0].code;

          if (this.HpCodesList.length > 1) {
            this._hpListShow = true;
          } else {
            this._hpListShow = false;
            this.amounts.hpCode = this.HpCodesList[0].code;
            //console.log(this.amounts.hpCode);
          }
          //console.log(this._hpListShow);
          this.GetListOfPaidBatches();
        });
  }

  GetRejectionReasons() {
    this.lobCode = new RejectionCodesModel();
    this.lobCode.username = this._loginService.CurrentLoggedInUser.username;
    this._http.post(this._baseUrl + "api/CheckRun/GetRejectionCodes", this.lobCode,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedList: [RejectionCodesModel]) => {
      this.rejectionReasons = [];
      this.rejectionReasons = recievedList;

    },
      (error) => {
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
      });

  }

  DownloadUserGuide() {

    this._http.get(this._baseUrl + "assets/CheckRun_User_Guide.pdf", { responseType: "arraybuffer" }).subscribe((recievedFile) => { });
  }

  CheckCheckRunType() {

    this._http.post(this._baseUrl + 'api/CheckRun/CheckCheckRunType', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: boolean) => {
      let x: boolean;
      x = recievedData;
      this._isProvider = x;
    },
      (error) => {
        this._errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        this.showText = true;
        this.loadBoardRow = false;
        this.displayData = false;
        this.legendDisplay = true;
        this.displaySpinner = false;
      });
  }

  CheckCheckRunTypeArrayPaid(array: CheckRunTypeModel[]) {

    this._http.post(this._baseUrl + 'api/CheckRun/CheckCheckRunTypeArray', array,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recievedData: CheckRunTypeModel[]) => {
      let x: CheckRunTypeModel[] = [];
      x = recievedData;
      this._paidBatchesType = x;
    },
      (error) => {
        this._errorHandler = true;
        this.ErrorRequets = true;
        this.ErrorRequets = true;
      },
      () => {

      });
  }

  ValidateEntireBatch() {
    this._http.post(this._baseUrl + "api/CheckRun/ValidateEntireBatch", this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved: ValidationResultsModel) => {

    },
      (error) => {
        this.errorHandler = true;
      },
      () => { });
  }

  UnvalidateEntireBatch() {
    this._http.post(this._baseUrl + "api/CheckRun/UnvalidateEntireBatch", this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved: ValidationResultsModel) => {

    },
      (error) => {
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => { });
  }

  FilterBordRowOnIncrease(page: number) {
    let u: CheckrunModel[];
    u = [];

    u = this.bordRowList.filter(ul =>
      ul.page === page
    );

    this._filteredBordRowList = u;
  }

  GetDetailsOfRefProv() {
    this.gettingDetails = true;
    this._http.post(this._baseUrl + 'api/CheckRun/GetDetailsOfRefProv', this.details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel) => {
        this._refProvInfo = new DetailLinesModel();
        this._refProvInfo.claimNo = recievedData.claimNo;
        this._refProvInfo.refProvId = recievedData.refProvId;
        this._refProvInfo.refProfName = recievedData.refProfName;
        this._refProvInfo.specCode = recievedData.specCode;
        this._refProvInfo.desc = recievedData.desc;
      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {
          this.GetDetailsOfClaim();
        });
  }

  GetDetailsOfClaim() {
    this._http.post(this._baseUrl + 'api/CheckRun/GetDetailsOfClaim', this.details,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: DetailLinesModel[]) => {
        this._paidInfo = new DetailLinesModel();
        this._claimInfo = [];
        this._diacodes = [];
        let x = recievedData.length;
        for (var i = 0; i < x; i++) {
          if (i < 1) {
            this._paidInfo.billed = recievedData[i].billed;
            this._paidInfo.coPay = recievedData[i].coPay;
            this._paidInfo.dateRec = recievedData[i].dateRec.substr(0, 10);
            this._paidInfo.adjust = recievedData[i].adjust;
            this._paidInfo.adjCode = recievedData[i].adjCode;
            this._paidInfo.tarrif = recievedData[i].tarrif;
            this._paidInfo.net = recievedData[i].net;
            this._paidInfo.fromDate = recievedData[i].fromDate;
            this._paidInfo.tooDate = recievedData[i].tooDate;
            this._paidInfo.srvCode = recievedData[i].srvCode;
            this._paidInfo.srvCodeDesc = recievedData[i].srvCodeDesc;
            this._paidInfo.provClaim = recievedData[i].provClaim;
            this._paidInfo.diagcode = recievedData[i].diagcode;
            this._paidInfo.diagdesc = recievedData[i].diagdesc;
            this._paidInfo.clinicalCodes = recievedData[i].clinicalCodes;
          } else {
            this._claimInfo.push(recievedData[i]);
          }
        }
      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {
          this.GetClaimDiags();
        });
  }

  GetClaimDiags() {
    
    const params = new HttpParams().append('claimNo', this.details.claimNo);
    this._http.post(this._baseUrl + 'api/Security/GetClaimDiags', this.details.claimNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        }),
        params
      })
      .subscribe((recievedData: Diagcodes[]) => {
        let temparr: Diagcodes[] = [];

        recievedData.forEach((ifo) => {
          temparr.push(ifo);
        });

        temparr.sort((a, b) => a.code < b.code ? -1 : 1);
        let temp: Diagcodes = new Diagcodes();
        temparr.forEach((tmp) => {
          if (tmp.code !== temp.code) {
            this._diacodes.push(tmp);
            temp.code = tmp.code;
          }
        });

      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }          
        },
        () => {
          this.GetVisitCounts();
        });
  }

  GetVisitCounts() {
    let memb: MemberNoModel = new MemberNoModel();
    memb.hpcode = this._loginService.CurrentLoggedInUser.username;
    memb.memberNo = this._membNo;
    this._http.post(this._baseUrl + 'api/CheckRun/GetVisitClaims', memb,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedCount: MemberNoModel) => {
        let x: string = recievedCount.memberNo
        this.visitCounts = parseInt(x);
      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {
          this.GetAuthNo();
        });

  }

  GetAuthNo() {
    let x: ValidationResultsModel = new ValidationResultsModel();
    x.message = this.details.claimNo;
    this._http.post(this._baseUrl + 'api/CheckRun/GetAuthNo', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      })
      .subscribe((recievedData: ValidationResultsModel) => {
        this.authNo = recievedData.message;
      },
        (error) => {
          this.errorHandler = true;
          console.log(error);
          this.ErrorRequets = true;
        },
        () => {
          //this.gettingDetails = false; // Jaco 2023-12-18
          this.GetVendorInfo();          // Jaco 2023-12-18
        });
  }

  GetVendorInfo() { // Jaco 2023-12-18

    let claim = new ClaimHeadersModel();
    claim.claimNo = this.details.claimNo;
    
    this._http.post(this._baseUrl + "api/Security/GetVendorInfo", claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe(
      (result: VendorInfoModel) => {
        this._vendorInfo = result;
      },
      (error) => {
        console.log(error);
        this.errorHandler = true;
        this.ErrorRequets = true;        
      },
      () => {
        this.gettingDetails = false;
      })
  }

  GenerateBatchForPrinting() {
    this._displayFetchModel = true;
    this._http.post(this._baseUrl + 'api/CheckRun/PrintCurrentBatch', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {
      let x: any;
      x = recieved;
      this.sheetName = x.message;
    },
      (error) => {
        console.log(error);
        this.errorHandler = true;
        this.ErrorRequets = true;
      },
      () => {
        let today: Date = new Date;
        let dateString: string;

        dateString = today.toDateString() + '_CheckRun.xlsx';
        var a = document.createElement('a');
        a.target = "_blank";
        a.setAttribute('type', 'hidden');
        //a.href = '../../PamcPortal/assets/CheckRunSheet/' + this.sheetName;
        a.href = 'assets/CheckRunSheet/' + this.sheetName;
        a.download = dateString;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._displayFetchModel = false;


        //this._filesource = '../../assets/Remittances/' + this._parseddate + '/' + this._filename


        //var a = document.createElement('a');
        //a.target = '_blank';
        //a.setAttribute('type', 'hidden');
        //a.href = this._filesource
        //a.download = this._filename;
        //document.body.appendChild(a);
        //a.click();
        //a.remove();
      });
  }

  FilterReleasedBatch(membNo: string, provider: string) {
    let found: boolean = false;
    if ((provider !== '') && (provider !== null)) {
      let p = this._uniqueProvList.filter(pl =>
        pl.providerID === provider
      );
      this._fillteredProvList = p;
    }

    if ((membNo !== '') && (membNo !== null)) {
      let x: ProviderList[] = [];
      this._uniqueProvList.forEach((ul) => {
        found = false;
        let y = this._patientsPerProvider[ul.providerID];
        y.forEach((pp) => {
          if (pp.mainPolicyNumber === membNo) {
            x.forEach((xl) => {
              if (xl.providerID === ul.providerID) {
                found = true;
              }
            });
            if (found !== true) {
              x.push(ul);
            }

          }
        });
      });
      this._fillteredProvList = x;
    }

    this.openEditBatch = false;
    this.enableNavButtons = false;
  }

  async UpdatePaymentDate() {    
    await
    this._http.post(this._baseUrl + 'api/CheckRun/UpdatePaymentDate', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).toPromise()
      .then(
        (recievedData) => { }
    );    
  }

  GetLastEnteredPayDate() {

    this._http.post(this._baseUrl + 'api/CheckRun/GetLastPayedDate', this._specificBatchNo,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe
      ((recieveddata: ValidationResultsModel) => {
        let x: ValidationResultsModel;
        x = recieveddata;
        this._lastEnteredDate = x.message.substring(0, 10);
        if (this._lastEnteredDate !== '') {
          this.lastpayedDateFound = true;
        } else {
          this.lastpayedDateFound = false;
        }
      },
        (error) => {
          console.log(error);
          this.errorHandler = true;
          this.ErrorRequets = true;
        },
        () => {

        });
  }

  FilterUnReleasedBatch(membNo: string, provider: string, claim: string) {
    let found: boolean = false;
    if ((provider !== '') && (provider !== null)) {
      let p = this._uniqueProvList.filter(pl =>
        pl.providerID === provider
      );
      this._fillteredProvList = p;
    }

    if ((membNo !== '') && (membNo !== null)) {
      let x: ProviderList[] = [];
      this._uniqueProvList.forEach((ul) => {
        found = false;
        let y = this._patientsPerProvider[ul.providerID];
        y.forEach((pp) => {
          if (pp.mainPolicyNumber === membNo) {
            x.forEach((xl) => {
              if (xl.providerID === ul.providerID) {
                found = true;
              }
            });
            if (found !== true) {
              x.push(ul);
            }

          }
        });
      });
      this._fillteredProvList = x;
    }
    if ((claim !== '') && (claim !== null)) {
      let x: ProviderList[] = [];
      this._uniqueProvList.forEach((ul) => {
        found = false;
        let y = this._patientsPerProvider[ul.providerID];
        y.forEach((pp) => {
          if (pp.claimNumber === claim) {
            x.forEach((xl) => {
              if (xl.providerID === ul.providerID) {
                found = true;
              }
            });
            if (found !== true) {
              x.push(ul);
            }

          }
        });
      });
      this._fillteredProvList = x;
    }

    this.openUnEditBatch = false;
  }

  NoBankingDetailsProvider() {
    this.nobankingFound = false;
    this._uniqueProvList.forEach((ul) => {
      if (ul.hasBanking == false) {
        ul.show = true;
      } else {
        ul.show = false;
      }
    });

    this._uniqueProvList.sort((a, b) => a.show > b.show ? -1 : 1);
    this._pageCounter = 1;
    this._providerPageCount = 1;

    this._uniqueProvList.forEach((el) => {

      el.page = this._providerPageCount;
      this._pageCounter++
      if (this._pageCounter === 16) {
        this._providerPageCount++;
        this._pageCounter = 1;
      }
    });
    this.FilterProvDictOnIncrease(1);
  }
}

import { Injectable, Inject } from '@angular/core';
import { AuthModel } from '../models/AuthModel';
import { Specmasters } from '../models/specmasters';
import { HealthplanModel } from '../models/HealthplanModel';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { MemberEligibilityModel } from '../models/MemberEligibilityModel';
import { Router } from '@angular/router';
import { SpecCodes } from '../models/SpecCodes';
import { LoginService } from './login.service';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { AuthHeadersModel } from '../models/AuthHeadersModel';
import { ClaimHeadersModel } from '../models/ClaimHeadersModel';
import { SettingsService } from './settings.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { RejectionCodesModel } from '../models/RejectionCodesModel';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private _modelContractShow: boolean;
  private _noAuthsfound: boolean = false
  private _listOfrequirements = [];
  private _authSetList: Specmasters[] = [];
  private _busy: boolean = false;
  private _showAuth: boolean = false;
  //private _AuthMembMsg: ValidationResultsModel = new ValidationResultsModel();
  //private _familyList: MemberEligibilityModel[] = [];
  private _AuthProvMsg: ValidationResultsModel = new ValidationResultsModel();
  //private _selectedAuthSetId: string = "";
  private _remainingAmountMessage: string = "";
  private _remainingAmount: string = "";
  private _newAuthNo: string;
  private _defaultProc: string;
  private _displayGrid: boolean = false;
  private _selectedSpecs: SpecCodes[] = [];
  private _selectedSpec: SpecCodes = new SpecCodes();
  //private _selectedList: SpecCodes[] = [];
  //private _specList: SpecCodes[] = [];
  private _displaySearchGrid: boolean = false;
  private _oldAuths: AuthModel[] = [];
  private _detailedAuth: AuthModel[] = [];
  private _hpContracts: HP_ContractsModel = new HP_ContractsModel();
  private _authDetail: AuthModel = new AuthModel();
  private _authDetails: AuthHeadersModel = new AuthHeadersModel();
  private _claimDetails: ClaimHeadersModel = new ClaimHeadersModel();
  private _testCompleteness: boolean;
  private _invalidProvderidModal: boolean;
  private _emptyFormerror: boolean = false;
  private _validationSpinner: boolean
  private _disableAuthssearch: boolean
  private _listOfauths: AuthHeadersModel[] = []
  private _displayAuthtable: boolean = false
  private _contractMessage: string[] = [];
  //private _enableSubmit: boolean = false;
  //public _memberror: any;
  private _membValid: boolean = false;
  private _membMessage: string;
  //public MembMessage: string;
  //public errMsg: ValidationErrors;
  private _provContract: string = "";

  //public _provError: boolean = false;

  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, private _router: Router,
    private _loginService: LoginService, private _settingsService: SettingsService) {
    //this.errMsg = {
    //  'required': 'This field is required!',
    //  'invalid': 'Invalid Member Number!',
    //  'invalid1': 'Invalid Provider Number!',

    //}
  }


  get AllAuths() {
    return this._listOfauths
  }
  set AllAuths(auths: AuthHeadersModel[]) {
    this._listOfauths = auths
  }
  //public get enableSubmit(): boolean {
  //  return this._enableSubmit;
  //}
  //public set enableSubmit(value: boolean) {
  //  this._enableSubmit = value;
  //}
  get DisableSearch() {
    return this._disableAuthssearch
  }
  set DisableSearch(isValid) {
    this._disableAuthssearch = isValid
  }
  get AuthTable() {
    return this._displayAuthtable
  }
  set AuthTable(isValid) {
    this._displayAuthtable = isValid
  }
  public get authDetail(): AuthModel {
    return this._authDetail;
  }
  public set authDetail(value: AuthModel) {
    this._authDetail = value;
  }
  get AuthDetail() {
    return this._authDetails
  }
  set AuthDetail(detail: AuthHeadersModel) {
    this.AuthDetail = detail
  }
  public get authSetList(): Specmasters[] {
    return this._authSetList;
  }
  public set authSetList(value: Specmasters[]) {
    this._authSetList = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get showAuth(): boolean {
    return this._showAuth;
  }
  public set showAuth(value: boolean) {
    this._showAuth = value;
  }
  //public get AuthMembMsg(): ValidationResultsModel {
  //  return this._AuthMembMsg;
  //}
  //public set AuthMembMsg(value: ValidationResultsModel) {
  //  this._AuthMembMsg = value;
  //}
  //public get familyList(): MemberEligibilityModel[] {
  //  return this._familyList;
  //}
  //public set familyList(value: MemberEligibilityModel[]) {
  //  this._familyList = value;
  //}
  public get AuthProvMsg(): ValidationResultsModel {
    return this._AuthProvMsg;
  }
  public set AuthProvMsg(value: ValidationResultsModel) {
    this._AuthProvMsg = value;
  }
  //public get selectedAuthSetId(): string {
  //  return this._selectedAuthSetId;
  //}
  //public set selectedAuthSetId(value: string) {
  //  this._selectedAuthSetId = value;
  //}
  public get remainingAmountMessage(): string {
    return this._remainingAmountMessage;
  }
  public set remainingAmountMessage(value: string) {
    this._remainingAmountMessage = value;
  }
  public get remainingAmount(): string {
    return this._remainingAmount;
  }
  public set remainingAmount(value: string) {
    this._remainingAmount = value;
  }
  public get newAuthNo(): string {
    return this._newAuthNo;
  }
  public set newAuthNo(value: string) {
    this._newAuthNo = value;
  }
  public get defaultProc(): string {
    return this._defaultProc;
  }
  public set defaultProc(value: string) {
    this._defaultProc = value;
  }
  public get selectedSpec(): SpecCodes {
    return this._selectedSpec;
  }
  public set selectedSpec(value: SpecCodes) {
    this._selectedSpec = value;
  }
  public get displayGrid(): boolean {
    return this._displayGrid;
  }
  public set displayGrid(value: boolean) {
    this._displayGrid = value;
  }
  public get selectedSpecs(): SpecCodes[] {
    return this._selectedSpecs;
  }
  public set selectedSpecs(value: SpecCodes[]) {
    this._selectedSpecs = value;
  }
  //public get selectedList(): SpecCodes[] {
  //  return this._selectedList;
  //}
  //public set selectedList(value: SpecCodes[]) {
  //  this._selectedList = value;
  //}
  //public get specList(): SpecCodes[] {
  //  return this._specList;
  //}
  //public set specList(value: SpecCodes[]) {
  //  this._specList = value;
  //}
  public get oldAuths(): AuthModel[] {
    return this._oldAuths;
  }
  public set oldAuths(value: AuthModel[]) {
    this._oldAuths = value;
  }
  public get displaySearchGrid(): boolean {
    return this._displaySearchGrid;
  }
  public set displaySearchGrid(value: boolean) {
    this._displaySearchGrid = value;
  }
  public get detailedAuth(): AuthModel[] {
    return this._detailedAuth;
  }
  public set detailedAuth(value: AuthModel[]) {
    this._detailedAuth = value;
  }
  get Healthplan() {
    return this._hpContracts
  }
  set Healthplan(plan: HP_ContractsModel) {
    this._hpContracts = plan
  }
  get Required() {
    return this._listOfrequirements
  }
  set Required(isValid) {
    this._listOfrequirements = isValid
  }
  get EmptyForm() {
    return this._emptyFormerror
  }
  set EmptyForm(isValid) {
    this._emptyFormerror = isValid
  }
  get Spinner() {
    return this._validationSpinner
  }
  set Spinner(isValid) {
    this._validationSpinner = isValid
  }
  get NoAuthsFound() {
    return this._noAuthsfound
  }
  set NoAuthsFound(isValid) {
    this._noAuthsfound = isValid
  }
  public get contractMessage(): string[] {
    return this._contractMessage;
  }
  public set contractMessage(value: string[]) {
    this._contractMessage = value;
  }
  public get modelContractShow(): boolean {
    return this._modelContractShow;
  }
  public set modelContractShow(value: boolean) {
    this._modelContractShow = value;
  }
  public get provContract(): string {
    return this._provContract;
  }
  public set provContract(value: string) {
    this._provContract = value;
  }

  public HpCodesList: HealthplanModel[] = [];

  //public get ProvError(): boolean {
  //  return this._provError;
  //}
  //public set ProvError(value: boolean) {
  //  this._provError = value;
  //}

  async HpCodes() {
    let x = new RejectionCodesModel();    
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    let token = localStorage.getItem("jwt");
    await
      this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }).toPromise()
        .then((returnedList: [HealthplanModel]) => {
          this.HpCodesList = returnedList;
        },
          (error) => {
            console.log(error);           
          }
        );
  }

  // Jaco 2024-10-28
  async OutOfNetGpVisitCheck(subssn: string) {

    const params = new HttpParams().append('subssn', subssn);    
    let token = localStorage.getItem("jwt");
    await
      this._http.get(this._baseUrl + 'api/Authorization/GetOutOfNetGpVisitCount',
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"
          }),
          params
        }).toPromise()
        .then((result: number) => {
          if (result >= 3) { //AUL out of network GP limit is 3
            this.authDetail.reject = true;
          }
        },
          (error: HttpErrorResponse) => {
            console.log(error);
          }
        );
  }

  SearchAuth() {
    this.ValidateForm()
  }

  ValidateForm() {
    if (this._loginService.CurrentLoggedInUser.userType == 1) {
      this.ProviderValidation()
    }
  }

  ProviderValidation() {
    let temparray = []
    let TestBoolean: boolean
    let enabled: boolean;
    let required: boolean;

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthMemberNumber');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthMemberNumber');

    if (enabled == true) {
      if (this._authDetails.memberno != "") {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Enter a Member Number")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthHealthPlan');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthHealthPlan');

    if (enabled == true) {
      if (this._hpContracts.hpcode != "000") {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Select a Health plan")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthServiceDate');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthServiceDate');

    if (enabled == true) {
      if (this._authDetails.servicedate != null && this._authDetails.servicedate != "") {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Enter the Auth Service Date")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthMemberID');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthMemberID');

    if (enabled == true) {
      if (this._authDetails.memberid != "") {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Enter The Member's ID Number")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthMemberDOB');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthMemberDOB');

    if (enabled == true) {
      if (this._authDetails.memberdob != null) {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Enter the Member's Date Of Birth")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    enabled = this._settingsService.GetSettingEnabled('ProviderViewAuthProviderPracticeNumber');
    required = this._settingsService.GetSettingRequired('ProviderViewAuthProviderPracticeNumber');

    if (enabled == true) {
      if (this._loginService.CurrentLoggedInUser.provID != null && this._loginService.CurrentLoggedInUser.provID != "") {
        TestBoolean = true
      } else {
        if (required == true) {
          temparray.push("Enter a Practice Number")
          TestBoolean = false
        } else {
          TestBoolean = true
        }
      }
    }

    this._listOfrequirements = temparray
    this._testCompleteness = TestBoolean

    if (this._listOfrequirements.length > 0) {
      this._testCompleteness = false;
    }
    if (this._invalidProvderidModal == true) {
      this._emptyFormerror = false;

    } else {
      if (this._testCompleteness == false) {
        this._emptyFormerror = true;
      } else {
        this.SearchSpecificAuthorization();
      }

    }
  }

  SearchSpecificAuthorization() {
    this._validationSpinner = true
    this._disableAuthssearch = true
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + "api/Authorization/SearchSpecificAuth", {
      Practice: this._loginService.CurrentLoggedInUser.provID,
      MemberNo: this._authDetails.memberno,
      Plan: this._hpContracts.hpcode,
      ServiceDate: this._authDetails.servicedate,
      userName: this._loginService.CurrentLoggedInUser.username
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedauths: AuthHeadersModel[]) => {
        this._listOfauths = returnedauths
        if (this._listOfauths.length > 0) {
          this._displayAuthtable = true;
          this._validationSpinner = false;
          this._disableAuthssearch = false;
        } else {
          this._displayAuthtable = false;
          this._noAuthsfound = true;
          this._validationSpinner = false;
          this._disableAuthssearch = false;
        }

      },

        (error) => { console.log(error) },
        () => {
          if (this._loginService.CurrentLoggedInUser.userType == 1) {
            this.CheckAllowedOptions();
          }
        })
  }

  CheckAllowedOptions() {
    this._contractMessage = [];
    let list: string[] = [];
    let opt: string[] = [];
    this._settingsService.Settings.forEach((el) => {
      if (this._loginService.isDentist) {
        if (el.settingsId == 'ContractBoundOptions') {
          opt = el.description.split(',');
        }
      }
      if (this._loginService.isSpecialist) {
        if (el.settingsId == 'ContractBoundOptionsGP') {
          opt = el.description.split(',');
        }
      }
    });
    let p = this;
    if (this._loginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED') {
      p._listOfauths.forEach((el) => {
        opt.forEach((op) => {
          // console.log(el);
          if (op == el.plan) {
            let found: boolean = false;
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                if (list[i].toUpperCase() == el.memberno.toUpperCase()) {
                  found = true;
                  break;
                }
              }
            } else {
              list.push(el.memberno);
              found = true;
            }

            if (found == false) {
              list.push(el.memberno);

            }
          }
        });
      });
    }
    let d: string = "";


    this._contractMessage.push("The Following Member/s is bound to a network option:");
    list.forEach((el) => {
      this._contractMessage.push("• " + el);
    });
    this._contractMessage.push("and you are a non network provider, therefore you can not see this member/s details.")
    if ((list.length > 0) && (this._loginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED')) {
      this._modelContractShow = true;
      this._displayAuthtable = this._settingsService.GetSettingEnabled('NetworkBoundShow');
    } else {
      this._displayAuthtable = true;
    }
  }

  GetAuthSetListForHp(hp: HealthplanModel) {
    this.authSetList = [];
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Authorization/GetBenifitListForHp', hp,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Specmasters[]) => {      
      results.forEach((el) => {
        this.authSetList.push(el);
      })
    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
      })
  }

  GetBenTypeDescAuth() {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/Authorization/GetBenTypeDescAuth', this.authDetail, { headers: headers });
  }

  async GetBenefitLimit(data: AuthModel) {
    this.remainingAmountMessage = "";
    this.remainingAmount = "";
    //this.enableSubmit = false;
    this._busy = true;
    let token = localStorage.getItem("jwt")
    await
      this._http.post(this._baseUrl + 'api/Authorization/GetBenefitLimit', data, {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).toPromise()
        .then((results: ValidationResultsModel) => {
          if (results.valid) {
            let c: string[] = [];
            c = results.message.split('|');

            this.remainingAmountMessage = "AVAILABLE LIMIT: R " + c[0];
            this.remainingAmount = c[0];
            this.authDetail.benType = c[1];
            this.authDetail.benDesc = c[2];
            //this.enableSubmit = true;

            //console.log('limit: \n' + this.remainingAmountMessage);
            this._busy = false;
          }
        },
          (error) => {
            console.log(error);
            this._busy = false;

          },
        );
      //() => {
      //  this._busy = false;
      //});
  }

  ValidateMemberOld() {
    //this.familyList = [];
    //this.showAuth = false;
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Member/MemberValidation', this.authDetail,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      //this._AuthMembMsg = results;
      //this._memberror = this.AuthMembMsg.valid;
      this._membValid = results.valid;
      if (results.valid) {
        this._membMessage = results.message;
        this.GetMemberFamily();
      } else {
        //this._membMessage = this.errMsg['invalid'];
        //this.familyList = []; // Clear family list
      }

    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
        //if (this.AuthMembMsg.valid) {
        //  this.MembMessage = this.AuthMembMsg.message;
        //  this.GetMemberFamily();
        //} else {
        //  this.MembMessage = this.errMsg['invalid'];
        //  this.familyList = []; // Clear family list
        //}
      })
  }

  ValidateMember() {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/Member/MemberValidation', this.authDetail, { headers: headers });
  }

  GetMemberFamily() {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/Member/MemberValidationFamily', this.authDetail, { headers: headers });
  }

  GetMemberFamilyOld() {
    //this.familyList = [];
    this.showAuth = false;
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Member/MemberValidationFamily', this.authDetail,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: MemberEligibilityModel[]) => {
      let c: MemberEligibilityModel = new MemberEligibilityModel();
      //c.firstnm = "SELECT";      //Jaco comment out 2022-08-10
      //c.dependant = "DEPENDANT"; //Jaco comment out 2022-08-10
      //this.familyList.push(c);   //Jaco comment out 2022-08-10
      results.forEach((el) => {
        //this.familyList.push(el);
      })
    },
      (error) => {
        console.log(error);
      },
      () => {
        this._busy = false;
      })
  }

  ValidateProvider() {
    this.provContract = "";
    this.showAuth = false;
    let q = this;
    this._busy = true;
    let token = localStorage.getItem('jwt');
    let a = "/Single-Auth-Submision";
    if (this._router.url === a) {
      this._http.post(this._baseUrl + 'api/Authorization/ValidateProv', this.authDetail,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }
      ).subscribe((results: ValidationResultsModel) => {
        q._AuthProvMsg = new ValidationResultsModel();

        let c: string[] = [];
        c = results.message.split(',');

        q._AuthProvMsg.message = c[0];

        if (c[1] == '2') {
          this.provContract = "CONTRACTED";
        } else {
          this.provContract = "NON-CONTRACTED";
        }
        q._AuthProvMsg.valid = results.valid;
        console.log('service');


      },
        (error) => { console.log(error) },
        () => {
          console.log('service');
          this._busy = false;
        })
    } else {
      this._http.post(this._baseUrl + 'api/Authorization/ValidateProvWithSpec', this.authDetail,
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }
      ).subscribe((results: ValidationResultsModel) => {
        q._AuthProvMsg = new ValidationResultsModel();
        let c: string[] = [];
        c = results.message.split(',');

        q._AuthProvMsg.message = c[0];

        if (c[1] == '2') {
          this.provContract = "CONTRACTED";
        } else {
          this.provContract = "NON-CONTRACTED";
        }
        q._AuthProvMsg.valid = results.valid;

        console.log('service');
      },
        (error) => { console.log(error) },
        () => {
          console.log('service');
          this._busy = false;
        })
    }

  }

  ValidateProvWithSpec() {

    let token = localStorage.getItem('jwt');

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/Authorization/ValidateProvWithSpec', this.authDetail, { headers: headers });
  }

  SubmitAuth() {
    this.showAuth = false;
    let token = localStorage.getItem('jwt');
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/SubmitAuthorization', this.authDetail,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe(
        (resutls: ValidationResultsModel) => {
          this._newAuthNo = resutls.message;
        },
        (error) => {
          console.log(error);
        },
        () => {
          this._busy = false;
          this.showAuth = true;
        })
  }

  GetDetailsBen(data: Specmasters) {
    this.selectedSpecs = [];
    this._busy = true;
    let token = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Authorization/GetDetailBen', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }
    ).subscribe((results: SpecCodes[]) => {
      //this.selectedList = [];      
      results.forEach((el) => {
        if (el.code.length == 1) {
          el.code = "0" + el.code;
        }
        //  console.log(this._specList);
        this._loginService.specList.forEach((e) => {
          if (el.code == e.code) {
            el.selected = true;
            el.desc = e.desc;
            //e.benCode = el.benCode; // Jaco 2022-08-03
            //e.procCode = el.procCode; // Jaco 2022-08-03
            //e.specDetail = el.specDetail; // Jaco 2022-08-03
            //this.selectedList.push(el);

            el.provId = "";
            el.srvDate = "";
            el.amount = "R";
            el.selectedBenCode = "";
            el.selectedProcCode = "";

            //if ((el.code == '14') || (e.code == '15')) {
            //  el.amount = "R";
            //}

            this.selectedSpecs.push(el);
          }

          //if ((el.code == '14') && (e.code == '15')) {
          //  e.selected = true;
          // this.selectedList.push(e);
          // this.selectedSpecs.push(e);
          //}
          //if ((el.code == '15') && (e.code == '14')) {
          //e.selected = true;
          // this.selectedList.push(e);
          // this.selectedSpecs.push(e);
          //}
        })
      })
    },
      (error) => {
        console.log(error);
      },
      () => {
        this._busy = false;
        this.displayGrid = true;
        this.SubmitAuthHeader(this.authDetail);
      })
  }

  SubmitAuthHeader(data: AuthModel) {
    let token = localStorage.getItem('jwt');
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/SubmitAuthorizationHeader', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"
        })
      }).subscribe((resutls: ValidationResultsModel) => {
        this.detailedAuth = [];
        let c: AuthModel = new AuthModel();
        this.selectedSpecs.forEach((el) => {
          let c: AuthModel = new AuthModel();
          c.desc = el.desc;
          c.id = resutls.message; // Auth Master (header) rowid
          c.spec = el.code;
          c.username = this._loginService.CurrentLoggedInUser.username;
          this.detailedAuth.push(c);
        })
      },
        (error) => { console.log(error); },
        () => {
          this._busy = false;
          //this.SubmitAuthDetails(); // Comment out jaco 2022-07-07 - we now create details on the modal
        })
  }

  SubmitAuthDetails() {
    let token = localStorage.getItem('jwt');
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/SubmitAuthorizationDetail', this.detailedAuth,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((resutls: AuthModel[]) => {
        resutls.forEach((el) => {
          this.detailedAuth.forEach((a) => {
            if (el.spec == a.spec) {
              a.id = el.id;
            }
          })
        })
      },
        (error) => { console.log(error); },
        () => {
          this._busy = false;
          console.log(this.detailedAuth);
        })
  }

  UpdateAuthDetails(data: AuthModel) {
    let token = localStorage.getItem('jwt');
    this._busy = true;
    this._showAuth = false;
    this._http.post(this._baseUrl + 'api/Authorization/UpdateAuthorizationDetail', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: ValidationResultsModel) => {

        this._newAuthNo = results.message;
        
      },
        (error) => {
          console.log(error);
        },
        () => {
          this._busy = false;
          this._showAuth = true;
        })
  }

  SearchSpecificAuth(data: AuthModel) {
    let token = localStorage.getItem('jwt');
    this._displaySearchGrid = false;
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/SearchAuthorizationDetail', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((resutls: AuthModel[]) => {
        this._oldAuths = [];
        resutls.forEach((el) => {
          if (el.amount.trim() == '') {
            el.amount = "(SYSTEM AMOUNT)";
          }
          this._oldAuths.push(el);
        })
      },
        (error) => { console.log(error); },
        () => {
          this._displaySearchGrid = true;
          this._busy = false;
        })
  }

  ClearAllInputFields() {
    this._hpContracts.hpcode = "000"
    this._authDetails.memberno = ""
    this._claimDetails.memberId = ""
    this._claimDetails.providerId = ""
    this._claimDetails.memberDOB = null
    this._claimDetails.serviceDate = null
    this._claimDetails.datePaid = null

  }

}

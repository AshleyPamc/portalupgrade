import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { ClaimHeadersModel } from '../models/ClaimHeadersModel';
import { SettingsService } from './settings.service';
import { SecurityService } from './security.service';
import { MemberEligibilityModel } from '../models/MemberEligibilityModel';
import { LoginService } from './login.service';
import { BenifitRights } from '../models/benifit-rights';
import { BenifitResults } from '../models/benifit-results';
import { HealthPlanSetupModel } from '../models/HealthPlanSetupModel';
import { BenHist } from '../models/ben-hist';
import { ToastrService } from 'ngx-toastr';
import { formatDate } from '@angular/common';
import { ChronicConditionApplicationModel } from '../models/ChronicConditionApplicationModel';
import { ChronicConditionService } from './chronic-condition.service';


@Injectable({
  providedIn: 'root'
})
export class MembershipService {
  private _memberDetails: MemberEligibilityModel = new MemberEligibilityModel();
  private _memberDetailsBenefitScreen: MemberEligibilityModel = new MemberEligibilityModel();
  private hpContract: HP_ContractsModel = new HP_ContractsModel()
  private hpContractBenScreen: HP_ContractsModel = new HP_ContractsModel()
  private _testCompleteness: boolean
  private _listOfrequiredFields = []
  private _emptyFormerror: boolean = false
  private _displayMembertable: boolean = false
  private _displayChronicConditionstable: boolean = false
  private _ValidationSpinner: boolean
  private _disableValidatebtn: boolean
  private _noMembersfound: boolean = false
  private _listOfmembers: MemberEligibilityModel[] = [];
  private _listOfmembersBenScreen: MemberEligibilityModel[] = [];
  private _token: string;
  private _showTabels: boolean = true;
  private _moreInfo: boolean = false;
  public get moreInfo(): boolean {
    return this._moreInfo;
  }
  public set moreInfo(value: boolean) {
    this._moreInfo = value;
  }
  public get showTabels(): boolean {
    return this._showTabels;
  }
  public set showTabels(value: boolean) {
    this._showTabels = value;
  }
  private _busy: boolean = false;
  private _benRights: BenifitRights[] = [];
  private _benHistOpen: boolean = false;
  private _allHist: boolean = false;
  private _specificHist: boolean = false;
  private _benHisList: BenHist[] = [];
  private _orgHistLIst: BenHist[] = [];
  private _openBenresult: boolean = false;
  private _currMember: string = "";
  private _currHpCode: string = "";
  private _chronicConditionsubmissionHistory: ChronicConditionApplicationModel[] = [];

  constructor(private _router: Router, public _toastService: ToastrService, private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _loginService: SettingsService, private _securityService: LoginService, private _chronicConditionService: ChronicConditionService) {
    this._token = localStorage.getItem("jwt");
  }

  ClearAllInputFields() {
    this._memberDetails.membid = ""
    this.hpContract.hpcode = "000"
    this._memberDetails.firstnm = ""
    this._memberDetails.lastnm = ""
    this._memberDetails.birth = null

  }

  public get currHpCode(): string {
    return this._currHpCode;
  }
  public set currHpCode(value: string) {
    this._currHpCode = value;
  }
  public get currMember(): string {
    return this._currMember;
  }
  public set currMember(value: string) {
    this._currMember = value;
  }
  get MemberEligibilityID(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityID" && type == 3) {
        check = el.administrator
      }
      if (el.settingsId === "MemberEligibilityID" && type == 1) {
        check = el.provider
      }
    });

    return check;

  }
  public get benHistOpen(): boolean {
    return this._benHistOpen;
  }
  public set benHistOpen(value: boolean) {
    this._benHistOpen = value;
  }
  get CheckRequiredMemberEligibilityID(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityID" && type == 3) {
        check = el.administratorRequired
      }
      if (el.settingsId === "MemberEligibilityID" && type == 1) {
        check = el.providerRequired
      }
    });

    return check;
  }

  get MemberEligibilitySelectHealthPlan(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilitySelectHealthPlan" && type == 3) {
        check = el.administrator
      }
      if (el.settingsId === "MemberEligibilitySelectHealthPlan" && type == 1) {
        check = el.provider
      }
    });

    return check;
  }

  get CheckRequiredMemberEligibilitySelectHealthPlan(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilitySelectHealthPlan" && type == 3) {
        check = el.administratorRequired
      }
      if (el.settingsId === "MemberEligibilitySelectHealthPlan" && type == 1) {
        check = el.providerRequired
      }
    });

    return check;

  }

  get MemberEligibilityName(): boolean {

    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityName" && type == 3) {
        check = el.administrator
      }
      if (el.settingsId === "MemberEligibilityName" && type == 1) {
        check = el.provider
      }
    });

    return check;

  }

  get CheckRequiredMemberEligibilityName(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityName" && type == 3) {
        check = el.administratorRequired
      }
      if (el.settingsId === "MemberEligibilityName" && type == 1) {
        check = el.providerRequired
      }
    });

    return check;
  }

  get MemberEligibilityLastName(): boolean {

    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilitySurname" && type == 3) {
        check = el.administrator
      }
      if (el.settingsId === "MemberEligibilitySurname" && type == 1) {
        check = el.provider
      }
    });

    return check;

  }

  get CheckRequiredMemberEligibilityLastName(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilitySurname" && type == 3) {
        check = el.administratorRequired
      }
      if (el.settingsId === "MemberEligibilitySurname" && type == 1) {
        check = el.providerRequired
      }
    });

    return check;

  }
  public get openBenresult(): boolean {
    return this._openBenresult;
  }
  public set openBenresult(value: boolean) {
    this._openBenresult = value;
  }
  get MemberEligibilityDOB(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityDOB" && type == 3) {
        check = el.administrator
      }
      if (el.settingsId === "MemberEligibilityDOB" && type == 1) {
        check = el.provider
      }
    });

    return check;
  }
  public get orgHistLIst(): BenHist[] {
    return this._orgHistLIst;
  }
  public set orgHistLIst(value: BenHist[]) {
    this._orgHistLIst = value;
  }
  get CheckRequiredMemberEligibilityDOB(): boolean {
    let type = this._securityService.CurrentLoggedInUser.userType;
    let check: boolean;
    this._loginService.Settings.forEach(el => {
      if (el.settingsId == "MemberEligibilityDOB" && type == 3) {
        check = el.administratorRequired
      }
      if (el.settingsId === "MemberEligibilityDOB" && type == 1) {
        check = el.providerRequired
      }
    });

    return check;

  }
  public get benHisList(): BenHist[] {
    return this._benHisList;
  }
  public set benHisList(value: BenHist[]) {
    this._benHisList = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get allHist(): boolean {
    return this._allHist;
  }
  public set allHist(value: boolean) {
    this._allHist = value;
  }
  public get token(): string {
    return localStorage.getItem("jwt");
  }
  public set token(value: string) {
    localStorage.setItem("jwt", value);
  }
  public get specificHist(): boolean {
    return this._specificHist;
  }
  public set specificHist(value: boolean) {
    this._specificHist = value;
  }

  /*Member elgibility check done from membership screen for providers*/
  ValidateForm() {
    let temparray = []
    let testboolean: boolean
    let enabled: boolean;
    let required: boolean;

    enabled = this._loginService.GetSettingEnabled('ViewClaimSelectHealthPlan');
    required = this._loginService.GetSettingRequired('ViewClaimSelectHealthPlan');

    if (this.MemberEligibilityID == true) {
      if (this._memberDetails.membid != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityID == true) {
          temparray.push("Enter a Member Number")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilitySelectHealthPlan == true) {
      if (this.hpContract.hpcode != "000") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilitySelectHealthPlan == true) {
          temparray.push("Select a Healthplan")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityName == true) {
      if (this._memberDetails.firstnm != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityName == true) {
          temparray.push("Enter a Member's First Name")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityLastName == true) {
      if (this._memberDetails.lastnm != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityLastName == true) {
          temparray.push("Enter a Member's Last Name")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityDOB == true) {
      if (this._memberDetails.birth != null && this._memberDetails.birth != "") {

        if (this.ValidateDOB(this._memberDetails.birth)) {
          testboolean = true
        } else {
          temparray.push("Incorrect date or date format. Enter a valid date in the yyyy/mm/dd format e.g. 2005/01/28")
          testboolean = false
        }

      } else {
        if (this.CheckRequiredMemberEligibilityDOB == true) {
          temparray.push("Enter Member's Date Of Birth")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }

    this._testCompleteness = testboolean
    this._listOfrequiredFields = temparray

    if (this._listOfrequiredFields.length == 0) {
      this._testCompleteness = true
    }

    if (this._listOfrequiredFields.length > 0) {
      this._testCompleteness = false
    }
    if (this._testCompleteness == false) {
      this._emptyFormerror = true
      this._displayMembertable = false
      this._displayChronicConditionstable = false
    } else {
      this.SearchEligibleMember()
    }
  }

  ValidateDOB(dob: string): boolean {

    //Check that two "/" exist
    var splitArray = dob.split("/");
    if (splitArray.length != 3) return false;

    //Check if year is valid
    let curYear = new Date().getFullYear();
    if (!(Number(splitArray[0]) >= 1900 && Number(splitArray[0]) <= Number(curYear))) return false;

    //Check if month is valid
    if (!(Number(splitArray[1]) >= 1 && Number(splitArray[1]) <= 12)) return false;

    //Validate day and month combination (31 day months)
    //if ((Number(splitArray[1]) % 2 != 0) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false; // Jaco 2023-04-19 wrong

    //Validate day and month combination (30 day months)
    //if ((Number(splitArray[1]) % 2 == 0) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 30)) return false; // Jaco 2023-04-19 wrong

    //Validate day and month combination // Jaco 2023-04-19
    if ((Number(splitArray[1]) == 1) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 2) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 28)) return false;
    if ((Number(splitArray[1]) == 3) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 4) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 30)) return false;
    if ((Number(splitArray[1]) == 5) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 6) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 30)) return false;
    if ((Number(splitArray[1]) == 7) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 8) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 9) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 30)) return false;
    if ((Number(splitArray[1]) == 10) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;
    if ((Number(splitArray[1]) == 11) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 30)) return false;
    if ((Number(splitArray[1]) == 12) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 31)) return false;

    //Check if birth leap year
    //let boolLeapYr = 0;
    //if ((curYear % 400) == 0) boolLeapYr = 1
    //else if ((curYear % 100) == 0) boolLeapYr = 0
    //else if ((curYear % 4) == 0) boolLeapYr = 1
    //else boolLeapYr = 0
    //if ((Number(splitArray[1]) == 2) && (boolLeapYr == 1) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 29)) return false;
    //if ((Number(splitArray[1]) == 2) && (boolLeapYr == 0) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 28)) return false;
    if ((Number(splitArray[1]) == 2) && !(Number(splitArray[2]) >= 1 && Number(splitArray[2]) <= 28)) return false;

    //Check if date is in future
    let dobDt = new Date(dob);
    let currDate = new Date(formatDate(new Date(), 'yyyy/MM/dd', 'en'));
    if (dobDt > currDate) return false;

    return true;
  }

  /*Member elgibility check done from benefit lookup screen for providers*/
  ValidateFormVaiBenefitScreen() {
    let temparray = []
    let testboolean: boolean
    let enabled: boolean;
    let required: boolean;

    enabled = this._loginService.GetSettingEnabled('ViewClaimSelectHealthPlan');
    required = this._loginService.GetSettingRequired('ViewClaimSelectHealthPlan');

    if (this.MemberEligibilityID == true) {
      if (this._memberDetailsBenefitScreen.membid != "") {
        let c: string[] = [];
        c = this._memberDetailsBenefitScreen.membid.split("-");
        if (c.length < 2) {
          temparray.push("Enter a Member Number With Dependant Code")
          testboolean = false
        } else if (c[c.length - 1].length == 2) {
          testboolean = true;
        } else {
          temparray.push("Enter a Member Number With Dependant Code")
          testboolean = false
        }

      } else {
        if (this.CheckRequiredMemberEligibilityID == true) {
          temparray.push("Enter a Member Number")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilitySelectHealthPlan == true) {
      if (this.hpContractBenScreen.hpcode != "000") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilitySelectHealthPlan == true) {
          temparray.push("Select a Healthplan")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityName == true) {
      if (this._memberDetailsBenefitScreen.firstnm != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityName == true) {
          temparray.push("Enter a Member's First Name")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityLastName == true) {
      if (this._memberDetailsBenefitScreen.lastnm != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityLastName == true) {
          temparray.push("Enter a Member's Last Name")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }
    if (this.MemberEligibilityDOB == true) {
      if (this._memberDetailsBenefitScreen.birth != null && this._memberDetails.birth != "") {
        testboolean = true
      } else {
        if (this.CheckRequiredMemberEligibilityDOB == true) {
          temparray.push("Enter Member's Date Of Birth")
          testboolean = false
        } else {
          testboolean = true
        }
      }
    }

    this._testCompleteness = testboolean
    this._listOfrequiredFields = temparray

    if (this._listOfrequiredFields.length == 0) {
      this._testCompleteness = true
    }

    if (this._listOfrequiredFields.length > 0) {
      this._testCompleteness = false
    }
    if (this._testCompleteness == false) {
      this._emptyFormerror = true
      this._displayMembertable = false
      this._displayChronicConditionstable = false
    } else {
      this.SearchEligibleMemberVaiBenefitScreen()
    }
  }

  // Return observable to calling controller
  SearchEligibleMemberApi(membid: string, firstnm: string, lastnm: string, birth: string, hpcode: string) {
    let token = localStorage.getItem("jwt");

    const headers = new HttpHeaders({
      "Authorization": "Bearer " + token,
      "Content-Type": "application/json",
      "Cache-Control": "no-store",
      "Pragma": "no-cache"
    });

    return this._http.post(this._baseUrl + 'api/Member/MemberDetails', {
      MEMBID: membid,
      FIRSTNM: firstnm,
      LASTNM: lastnm,
      BIRTH: birth,
      HPCODE: hpcode,
      LOBCODE: this._securityService.CurrentLoggedInUser.lobcode,
      USER: this._securityService.CurrentLoggedInUser.username,
      USERTYPE: this._securityService.CurrentLoggedInUser.userType
    }, { headers: headers });
  }

  SearchEligibleMember() {
    this._ValidationSpinner = true
    this._disableValidatebtn = true
    if (this._memberDetails.birth === null) {
      this._memberDetails.birth = "1900/01/01";
    }
    this._http.post(this._baseUrl + "api/Member/MemberDetails", {
      MEMBID: this._memberDetails.membid,
      FIRSTNM: this._memberDetails.firstnm,
      LASTNM: this._memberDetails.lastnm,
      BIRTH: this._memberDetails.birth,
      HPCODE: this.hpContract.hpcode,
      LOBCODE: this._securityService.CurrentLoggedInUser.lobcode,
      USER: this._securityService.CurrentLoggedInUser.username,
      USERTYPE: this._securityService.CurrentLoggedInUser.userType
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._securityService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedmemberdetail: MemberEligibilityModel[]) => {
        this._listOfmembers = returnedmemberdetail
        //  console.log("returned member:", this._listOfmembers )

        if (this._listOfmembers.length > 0) {
          //this.MemberDetailsBenefitScreen.membid = this._memberDetails.membid;
          //this.MemberDetailsBenefitScreen.birth = this._memberDetails.birth;          
          this._displayMembertable = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
          this.currMember = this._memberDetails.membid;
          this._currHpCode = this.hpContract.hpcode;

          this.SearchEligibleMemberChronicConditions(this._listOfmembers);

        } else {
          this._displayMembertable = false
          this._noMembersfound = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
        }

      },
        (error) => {
          console.log(error);
        },
        () => {

          if (this._securityService.CurrentLoggedInUser.userType == 1) {
            if (this._securityService.CurrentLoggedInUser.hospital == false) {
              this.CheckAllowedOptions();
            }
          }

        });
  }

  SearchEligibleMemberChronicConditions(listofMembers: MemberEligibilityModel[]) {

    //this.busy = true;

    this._ValidationSpinner = true
    this._disableValidatebtn = true

    let submissionHist: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();
    submissionHist.provId = this._securityService.CurrentLoggedInUser.provID;
    submissionHist.familyMembers = listofMembers;

    this._chronicConditionService.GetSubmissionHistory(submissionHist)
      .subscribe(
        (results: ChronicConditionApplicationModel[]) => {

          if (results.length > 0) {

            if (results[0].success) {
              //results.forEach((el) => {
              //  this.submissionHistory.push(el);
              //})
              this._chronicConditionsubmissionHistory = results;
              
              this._ValidationSpinner = false
              this._disableValidatebtn = false

              this._displayChronicConditionstable = true;
              console.log(this.ChronicConditionstable)
            }
            else {
              //this.error = true;
              //this.errorMessageHeading = "Could not get submission history";
              //this.errorMessage = results[0].message;
              this._displayChronicConditionstable = false;
            }

          }
        },
        (error) => {
          //console.log(error)
          //this.error = true;
          //this.errorMessageHeading = "Could not get submission history";
          //this.errorMessage = "Unexpected error occured";
          //this.busy = false;

          this._ValidationSpinner = false
          this._disableValidatebtn = false
          this._displayChronicConditionstable = false;
        },
        () => {
          //this.busy = false;

          this._ValidationSpinner = false
          this._disableValidatebtn = false
        }
      )
  }
   

  SearchEligibleMemberVaiBenefitScreen() {
    this._ValidationSpinner = true
    this._disableValidatebtn = true
    if (this._memberDetailsBenefitScreen.birth === null) {
      this._memberDetailsBenefitScreen.birth = "1900/01/01";
    }
    this._http.post(this._baseUrl + "api/Member/MemberDetailsBenefitScreen", {
      MEMBID: this._memberDetailsBenefitScreen.membid,
      FIRSTNM: this._memberDetailsBenefitScreen.firstnm,
      LASTNM: this._memberDetailsBenefitScreen.lastnm,
      BIRTH: this._memberDetailsBenefitScreen.birth,
      HPCODE: this.hpContractBenScreen.hpcode,
      LOBCODE: this._securityService.CurrentLoggedInUser.lobcode,
      USER: this._securityService.CurrentLoggedInUser.username,
      USERTYPE: this._securityService.CurrentLoggedInUser.userType
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._securityService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedmemberdetail: MemberEligibilityModel[]) => {
        this._listOfmembersBenScreen = returnedmemberdetail
        //  console.log("returned member:", this._listOfmembers )

        if (this._listOfmembersBenScreen.length > 0) {
          this._displayMembertable = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
          //  this.currMember = this._memberDetails.membid;
          this._currHpCode = this.hpContract.hpcode;
        } else {
          this._displayMembertable = false
          this._noMembersfound = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
        }


      },
        (error) => {
          console.log(error);
        },
        () => {


          if (this._securityService.CurrentLoggedInUser.userType == 1) {
            if (this._securityService.CurrentLoggedInUser.hospital == false) {
              this.CheckAllowedOptions();
            }
          }
        });
  }

  SearchEligibleMemberLastServDate() {
    this._ValidationSpinner = true
    this._disableValidatebtn = true
    if (this._memberDetails.birth === null) {
      this._memberDetails.birth = "1900/01/01";
    }
    this._http.post(this._baseUrl + "api/Member/GetLastServDatePerMemb",
      this._listOfmembers
      ,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._securityService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedmemberdetail: MemberEligibilityModel[]) => {
        this._listOfmembers = [];
        this._listOfmembers = returnedmemberdetail
        if (this._listOfmembers.length > 0) {
          this._displayMembertable = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
        } else {
          this._displayMembertable = false
          this._noMembersfound = true
          this._ValidationSpinner = false
          this._disableValidatebtn = false
        }
        //  console.log("returned member:", this._listOfmembers )
      },
        (error) => {
          console.log(error);
        },
        () => {

        });
  }
  private _contractMessage: string[] = [];
  private _modelContractShow: boolean;

  public get modelContractShow(): boolean {
    return this._modelContractShow;
  }
  public set modelContractShow(value: boolean) {
    this._modelContractShow = value;
  }
  public get contractMessage(): string[] {
    return this._contractMessage;
  }
  public set contractMessage(value: string[]) {
    this._contractMessage = value;
  }

  CheckAllowedOptions() {
    this._contractMessage = [];
    let list: string[] = [];
    let opt: string[] = [];
    this._loginService.Settings.forEach((el) => {
      if (this._securityService.isDentist) {
        if (el.settingsId == 'ContractBoundOptionsDentist') {
          opt = el.description.split(',');
        }
      }
      if (this._securityService.isSpecialist) {
        if (el.settingsId == 'ContractBoundOptionsGP') {
          opt = el.description.split(',');
        }
      }
    });
    let p = this;
    if (this._securityService.CurrentLoggedInUser.contract == 'NON-CONTRACTED') {
      p._listOfmembers.forEach((el) => {
        opt.forEach((op) => {
          // console.log(el);
          if (op == el.opt) {
            let found: boolean = false;
            if (list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                if (list[i].toUpperCase() == el.membid.toUpperCase()) {
                  found = true;
                  break;
                }
              }
            } else {
              list.push(el.membid);
              found = true;
            }

            if (found == false) {
              list.push(el.membid);

            }
          }
        });
      });
    }
    let d: string = "";


    this._contractMessage.push("The Following Member/s is bound to a network option:");
    list.forEach((el) => {
      this._contractMessage.push("• " + el);
    });
    this._contractMessage.push("and you are a non network provider, therefore you can not see this member/s details.")
    if ((list.length > 0) && (this._securityService.CurrentLoggedInUser.contract == 'NON-CONTRACTED')) {
      this._modelContractShow = true;
      this._showTabels = this._loginService.GetSettingEnabled('NetworkBoundShow');
    } else {
      this._showTabels = true;
    }

    if (this._securityService.CurrentLoggedInUser.contract == 'NON-CONTRACTED') {
      this._loginService.expSettings.forEach((el) => {
        p._listOfmembers.forEach((e) => {
          if (e.opt.toUpperCase() == el.opt) {
            let c: string[] = [];
            c = el.specs.split(',');
            c.forEach((l) => {
              if (l.trim() == this._securityService.CurrentLoggedInUser.specCode) {
                if (this._showTabels == false) {
                  this._showTabels = true;
                }
                this._modelContractShow = false;
              }
            })
          }
        });
      });
    }
  }

  SubmitVaiBenefit() {
    this.ValidateFormVaiBenefitScreen();
  }

  Submit() {
    this.ValidateForm();
  }

  //exposing the member details
  get MemberDetails() {
    return this._memberDetails
  }
  set MemberDetails(details: MemberEligibilityModel) {
    this._memberDetails = details
  }

  get MemberDetailsBenefitScreen() {
    return this._memberDetailsBenefitScreen
  }
  set MemberDetailsBenefitScreen(details: MemberEligibilityModel) {
    this._memberDetailsBenefitScreen = details
  }

  //exposing the HealthPlans
  get HealthplanContracts() {
    return this.hpContract
  }
  set HealthplanContracts(healthplan: HP_ContractsModel) {
    this.hpContract = healthplan
  }

  get HealthplanContractsBenScreen() {
    return this.hpContractBenScreen
  }
  set HealthplanContractsBenScreen(healthplan: HP_ContractsModel) {
    this.hpContractBenScreen = healthplan
  }

  //exposing the validation spinner
  get Validation() {
    return this._ValidationSpinner
  }
  set Validation(isValid) {
    this._ValidationSpinner = isValid
  }

  //disable submit button
  get DisableSubmitBtn() {
    return this._disableValidatebtn
  }
  set DisableSubmitBtn(isValid) {
    this._disableValidatebtn = isValid
  }

  public get benRights(): BenifitRights[] {
    return this._benRights;
  }
  public set benRights(value: BenifitRights[]) {
    this._benRights = value;
  }

  //boolean to display the member table
  get MemberTable() {
    return this._displayMembertable
  }
  set MemberTable(isValid) {
    this._displayMembertable = isValid
  }

  get ChronicConditionstable() {
    return this._displayChronicConditionstable
  }
  set ChronicConditionstable(isValid) {
    this._displayChronicConditionstable = isValid
  }

  get AllMembersBenScreen() {
    return this._listOfmembersBenScreen
  }
  set AllMembersBenScreen(member: MemberEligibilityModel[]) {
    this._listOfmembersBenScreen = member
  }

  get AllMembers() {
    return this._listOfmembers
  }
  set AllMembers(member: MemberEligibilityModel[]) {
    this._listOfmembers = member
  }

  get EmptyForm() {
    return this._emptyFormerror
  }
  set EmptyForm(isValid) {
    this._emptyFormerror = isValid
  }

  get Requirements() {
    return this._listOfrequiredFields
  }

  get NoMembersFound() {
    return this._noMembersfound
  }
  set NoMembersFound(isValid) {
    this._noMembersfound = isValid
  }

  private _returnBenSet: BenifitRights = new BenifitRights();

  public get returnBenSet(): BenifitRights {
    return this._returnBenSet;
  }
  public set returnBenSet(value: BenifitRights) {
    this._returnBenSet = value;
  }

  GetBenRights(data: MemberEligibilityModel) {
    let token = localStorage.getItem("jwt");
    data.type = this._securityService.CurrentLoggedInUser.userType;
    data.dentist = this._securityService.CurrentLoggedInUser.dentist;
    if (this._securityService.isDentist) {
      data.dentist = true;
    } else {
      data.dentist = false;
    }
    this.benRights = []
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Benefit/GetBenRights', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: BenifitRights[]) => {
      this.benRights = results;


    },
      (error) => {
        console.log(error);
      },
      () => {
        this._busy = false
      });

  }

  public resultSetDisplay: BenifitResults[] = [];

  GetBenifitResult(data: BenifitRights) {
    let token = localStorage.getItem("jwt");
    let error: boolean = false;
    this.busy = true;
    this.resultSetDisplay = [];
    data.dentice = this._securityService.CurrentLoggedInUser.dentist;
    this._http.post(this._baseUrl + 'api/Benefit/BenifitResult', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((result: BenifitRights) => {
      this.returnBenSet = result;

      if (data.defaultProc) {
        this.returnBenSet.results.forEach((el) => {
          let c: string[] = el.split(";");
          let d: BenifitResults = new BenifitResults();
          d.indicate = c[0];
          if (d.indicate == 'Y') {
            error = false;
          }
          if (d.indicate == 'N') {
            error = true;
          }
          d.ageMessage = c[1];
          d.age = c[2];
          d.provid = c[3];
          if (c[4] !== 'N/A') {
            d.svcDate = c[4].substr(0, 10);
          } else {
            d.svcDate = c[4];
          }

          this.resultSetDisplay.push(d);
        });
      } else {
        this.returnBenSet.results.forEach((el) => {
          let c: string[] = el.split(";");
          let d: BenifitResults = new BenifitResults();
          d.provid = c[0];
          d.tootno = c[1];
          d.svcDate = c[2];

          if (c.length == 4) {
            d.months = +c[3];
          }



          this.resultSetDisplay.push(d);

          if (d.tootno == "N/A") {
            error = true;
          }
        });
      }


    },
      (error) => {
        console.log(error);
      },
      () => {
        this._busy = false;
        let m: string = `Reference No: ${this.returnBenSet.reff}   <br/>     Member No: ${this.returnBenSet.membid}  <br/>
Benefit Type: ${this.returnBenSet.benName}`;
        if (error) {
          this._toastService.error(m, "BENEFIT CONFORMATION", {
            timeOut: 1200000,
            enableHtml: true,
            closeButton: true

          });
        } else {
          this._toastService.success(m, "BENEFIT CONFORMATION", {
            timeOut: 1200000,
            enableHtml: true,
            closeButton: true

          });
        }

        this.openBenresult = true;

      })
  }

  GetAllBeifitSearchHist() {
    let token = localStorage.getItem("jwt");
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Benefit/EntireBenifitHistSearch', this._securityService.CurrentLoggedInUser,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: BenHist[]) => {
        this.benHisList = [];
        this.benHisList = results;
        this.orgHistLIst = results;
      },
        (error) => {
          console.log(error);
        },
        () => {
          this.busy = false;
        })

  }

}

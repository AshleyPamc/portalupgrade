import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { SettingsService } from './settings.service';
import { SecurityService } from './security.service';
import { GraphsService } from './graphs.service';
import { MembershipService } from './membership.service';
import { CheckrunService } from './checkrun.service';
import { MemberClaimService } from './member-claim.service';
import { ClaimCaptureService } from './claim-capture.service';
import { AuthorizationService } from './authorization.service';
import { FileuploadService } from './upload-file.service';
import { MedivouchService } from './medivouch.service';
import { VoucherAdminService } from './voucher-admin.service';
import { PatientWellnessService } from './patient-wellness.service';
import { ChronicConditionService } from './chronic-condition.service';
import { ValidationService } from './validation.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public openSurvey: boolean = false;

  constructor(private _loginService: LoginService, private _settingService: SettingsService, private _securityService: SecurityService,
    private _grapgService: GraphsService, private _membershipService: MembershipService, private _checkRunService: CheckrunService,
    private _memberClaimService: MemberClaimService, private _claimCaptureService: ClaimCaptureService,
    private _authService: AuthorizationService, private _uploadFileService: FileuploadService,
    private _medivouchService: MedivouchService, private _voucherAdminService: VoucherAdminService,
    private _patientWelnessService: PatientWellnessService, private _chronicConditionService: ChronicConditionService,
    private _validationService: ValidationService ) { }



  public get LoginService(): LoginService {
    return this._loginService;
  }
  public get SettingService(): SettingsService {
    return this._settingService;
  }
  public get SecurityService(): SecurityService {
    return this._securityService;
  }
  public get GraphsService(): GraphsService {
    return this._grapgService;
  }
  public get MembershipService(): MembershipService {
    return this._membershipService;
  }
  public get CheckRunService(): CheckrunService {
    return this._checkRunService;
  }

  public get MedivouchService(): MedivouchService {
    return this._medivouchService;
  }

  public get MemberClaimService(): MemberClaimService {
    return this._memberClaimService;
  }
  public get ClaimCaptureService(): ClaimCaptureService {
    return this._claimCaptureService;
  }

  public get AuthorizationService(): AuthorizationService {
    return this._authService;
  }

  public get UploadFileService(): FileuploadService {
    return this._uploadFileService;
  }

  public get VoucherAdminService(): VoucherAdminService {
    return this._voucherAdminService;
  }

  public get PatientWellnessService(): PatientWellnessService {
    return this._patientWelnessService;
  }

  public get ChronicConditionService(): ChronicConditionService {
    return this._chronicConditionService;
  }

  public get ValidationService(): ValidationService {
    return this._validationService;
  }

}

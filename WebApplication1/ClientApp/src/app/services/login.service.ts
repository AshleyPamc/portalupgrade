import { Injectable, Inject, HostListener } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { HealthPlanSetupModel } from '../models/HealthPlanSetupModel';
import { HP_ContractsModel } from '../models/HP_ContractsModel';
import { SettingsService } from './settings.service';
import { LoginDetailsModel } from '../models/LoginDetailsModel';
import { Router } from '@angular/router';
import { ProviderRemittancesModel } from '../models/ProviderRemittancesModel';
import { ImagespecModel } from '../models/ImagespecModel';
import { SpecCodes } from '../models/SpecCodes';
import { BenTrackCodes } from '../models/ben-track-codes';
import { Specmasters } from '../models/specmasters';
import { AccountingBureauRemittancesModel } from '../models/AccountingBureauRemittancesModel';
import { BureauProvidersModel } from '../models/BureauProvidersModel';
import { MemberInfoModel } from '../models/MemberInfoModel';
import { UserGridModel } from '../models/UserGridModel';
import { LOBcodesModel } from '../models/LOBcodesModel';
import { UsertypeModel } from '../models/UsertypeModel';
import { UserRightsModel } from '../models/UserRightsModel';
import { ManualsModel } from '../models/ManualsModel';
import { PasswordChange } from '../models/PasswordChangeModel';
import { PopUpModel } from '../models/PopUpModel';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //PRIVATE VARIABLES
  private _screenWidth: number;
  private _webAPIavailable: boolean = false;
  private _server: string;
  private _busy: boolean;
  private _subscribeError: boolean = false;
  private _healthPlansetup: HealthPlanSetupModel[] = [];
  private _memberRegistrationhealthPlans: HP_ContractsModel[] = [];
  private _brokerRegistrationhealthPlans: HP_ContractsModel[] = [];
  private _token: string;
  private _userName: string;
  private _password: string;
  private show_hide_error: boolean = false;
  private _isLoggedin: boolean = false;
  private _isSpecialist: boolean = false;
  private _listOfDentistSpecCodes: string[] = [];
  private _listOfHospitalSpecCodes: string[] = [];
  private _listOfPharmaciesSpecCodes: string[] = [];
  private _loggedInuser: LoginDetailsModel;
  private _isDentist: boolean = false;
  private _isPharmacy: boolean = false;
  private _listOfhealthplansMember: HP_ContractsModel[] = [];
  private _listOfhealthplansMemberBenScreen: HP_ContractsModel[] = [];
  private _listOfhealthplansClaim: HP_ContractsModel[] = [];
  private _providerRemittances: ProviderRemittancesModel[] = [];
  private _selectedimage: ImagespecModel = new ImagespecModel();
  private _specList: SpecCodes[] = [];
  private _bencodes: BenTrackCodes[] = [];
  private _existingBen: Specmasters[] = [];
  private _remittances: AccountingBureauRemittancesModel[] = [];
  private _remittanceLoader: boolean = false;
  private _listOfProvider: BureauProvidersModel[] = [];
  private _memberInfo: MemberInfoModel[] = [];
  private _originalListofUsers: UserGridModel[] = []
  private _listOfusers: UserGridModel[] = [];
  private _lobCodes: LOBcodesModel[] = [];
  private _userTypesarray: UsertypeModel[] = [];
  private _userRegistration: any[] = [];
  private _userRights: UserRightsModel = new UserRightsModel();
  private _manuals: ManualsModel[] = [];
  private _manuals2020: ManualsModel[] = [];
  private _manuals2019: ManualsModel[] = [];
  private _guides2020: ManualsModel[] = [];
  private _guides2019: ManualsModel[] = [];
  private _poptcModel: boolean = false;
  private _passwordChange: PasswordChange = new PasswordChange();
  private _oldPasswordError: string;
  private _oldPasswordHasError: boolean;
  private _disableConfirm: boolean = true;
  private _newPasswordError: string = "";
  private _newPasswordHasError: boolean;
  private _confirmPasswordError: string = "";
  private _confirmPasswordHasError: boolean;
  private _showPasswordChange: boolean = false;
  private _confirmationMessage: string;
  private _showConfirmationMsg: boolean = false;
  private _bureauRecover: boolean = false;
  private _newPasswordrecover = "";
  private _confirmNewpasswordrecover = "";
  private _accUpdateerror: boolean = false;
  private _recoverAccount: boolean = false;
  private _accemailValidationregistermodal: boolean = false;
  private _accUpdatesuccess: boolean = false;
  private _showHideforgetPassword: boolean = false;
  private _names = new Array();
  private _providerIdBureaurecover: string = "";
  private _enableAddProvider: boolean = false
  private _passwordErrorlabel = "";
  private _isHospital: boolean = false;
  private _smallPopModal: boolean = false;
  private _mediumPopModal: boolean = false;
  private _largePopModal: boolean = false;
  private _popupList: PopUpModel[] = [];
  private _currPopMsg: number = 0;


  


  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _settingsService: SettingsService, private _router: Router) {
    //this._baseUrl = 'http://localhost:4200'
    this.TestConnection();

    this.ScreenWidth = window.innerWidth;
  }

  //GETTER AND SETTER
  public get currPopMsg(): number {
    return this._currPopMsg;
  }
  public set currPopMsg(value: number) {
    this._currPopMsg = value;
  }
  public get smallPopModal(): boolean {
    return this._smallPopModal;
  }
  public set smallPopModal(value: boolean) {
    this._smallPopModal = value;
  }
  public get mediumPopModal(): boolean {
    return this._mediumPopModal;
  }
  public set mediumPopModal(value: boolean) {
    this._mediumPopModal = value;
  }
  public get largePopModal(): boolean {
    return this._largePopModal;
  }
  public set largePopModal(value: boolean) {
    this._largePopModal = value;
  }
  public get ScreenWidth(): number {
    return this._screenWidth;
  }
  public set ScreenWidth(value: number) {
    this._screenWidth = value;
  }
  public get InvaildUsernameOrPassword() {
    return this.show_hide_error
  }
  public get SQLConnectionStatus() {
    return this._webAPIavailable
  }
  public get server(): string {
    return this._server;
  }
  public set server(value: string) {
    this._server = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get ErrorOccurance() {
    return this._subscribeError
  }
  public get HealthPlans() {
    return this._healthPlansetup;
  }
  public set HealthPlans(val: HealthPlanSetupModel[]) {
    this._healthPlansetup = val;
  }
  public get MemberRegHealthPlans() {
    return this._memberRegistrationhealthPlans
  }
  public get BrokerHealthPlans() {
    return this._brokerRegistrationhealthPlans
  }
  public get token(): string {
    return this._token;
  }
  public set token(value: string) {
    this._token = value;
  }
  public get Username() {
    return this._userName
  }
  public set Username(name) {
    this._userName = name
  }
  public get Password() {
    return this._password
  }
  public set Password(pass) {
    this._password = pass
  }
  public get CurrentLoggedInUser() {
    return this._loggedInuser
  }
  public get IsLoggedIn(): boolean {
    return this._isLoggedin;
  }
  public set IsLoggedIn(value: boolean) {
    this._isLoggedin = value;
  }
  public get isSpecialist(): boolean {
    return this._isSpecialist;
  }
  public set isSpecialist(value: boolean) {
    this._isSpecialist = value;
  }
  public get ProviderRemittance() {
    return this._providerRemittances
  }
  public get isDentist(): boolean {
    return this._isDentist;
  }
  public set isDentist(value: boolean) {
    this._isDentist = value;
  }
  public get isPharmacy(): boolean {
    return this._isPharmacy;
  }
  public set isPharmacy(value: boolean) {
    this._isPharmacy = value;
  }
  public get isHospital(): boolean {
    return this._isHospital;
  }
  public set isHospital(value: boolean) {
    this._isHospital = value;
  }
  public get ClaimsHealthPlanDropDown() {
    return this._listOfhealthplansClaim
  }
  public get SelectedImage() {
    return this._selectedimage
  }
  public get MemberClaimsDropDown() {
    return this._listOfhealthplansMember
  }
  public get MemberClaimsDropDownBenScreen() {
    return this._listOfhealthplansMemberBenScreen
  }
  public get specList(): SpecCodes[] {
    return this._specList;
  }
  public set specList(value: SpecCodes[]) {
    this._specList = value;
  }
  public get bencodes(): BenTrackCodes[] {
    return this._bencodes;
  }
  public set bencodes(value: BenTrackCodes[]) {
    this._bencodes = value;
  }
  public get existingBen(): Specmasters[] {
    return this._existingBen;
  }
  public set existingBen(value: Specmasters[]) {
    this._existingBen = value;
  }
  public get Remittances() {
    return this._remittances
  }
  public get RemittanceLoader() {
    return this._remittanceLoader;
  }
  public get ProvidersLinkedToBureau() {
    return this._listOfProvider
  }
  public get MemberInformation() {
    return this._memberInfo;
  }
  public get AllUsers() {
    return this._listOfusers
  }
  public set AllUsers(val: UserGridModel[]) {
    this._listOfusers = val;
  }
  public get LOBcodes() {
    return this._lobCodes
  }
  public get UserRegistrationArray() {
    return this._userRegistration
  }
  public get userRights(): UserRightsModel {
    return this._userRights;
  }
  public set userRights(value: UserRightsModel) {
    this._userRights = value;
  }
  public get Manuals() {
    return this._manuals
  }
  public get ProviderGuides2020() {
    return this._guides2020;
  }
  public get ProviderGuides2019() {
    return this._guides2019;
  }
  public get OosManuls2020() {
    return this._manuals2020
  }
  public get OosManuls2019() {
    return this._manuals2019
  }
  public get poptcModel(): boolean {
    return this._poptcModel;
  }
  public set poptcModel(value: boolean) {
    this._poptcModel = value;
  }
  public get passwordChange(): PasswordChange {
    return this._passwordChange;
  }
  public set passwordChange(value: PasswordChange) {
    this._passwordChange = value;
  }
  public get OldPasswordErrorMsg() {
    return this._oldPasswordError;
  }
  public set OldPasswordErrorMsg(msg) {
    this._oldPasswordError = msg
  }
  public get OldPasswordNotValid() {
    return this._oldPasswordHasError
  }
  public set OldPasswordNotValid(isValid) {
    this._oldPasswordHasError = isValid

  }
  public get DisableConfirmbtn() {
    return this._disableConfirm
  }
  public set DisableConfirmbtn(isValid) {
    this._disableConfirm = isValid
  }
  public get NewPasswordErrorMsg() {
    return this._newPasswordError
  }
  public set NewPasswordErrorMsg(msg) {
    this._newPasswordError = msg;
  }
  public get NewPasswordNotValid() {
    return this._newPasswordHasError;
  }
  public set NewPasswordNotValid(isValid) {
    this._newPasswordHasError = isValid;
  }
  public get ConfirmPasswordErrorMsg() {
    return this._confirmPasswordError;
  }
  public set ConfirmPasswordErrorMsg(msg) {
    this._confirmPasswordError = msg;
  }
  public get ConfirmPasswordNotValid() {
    return this._confirmPasswordHasError
  }
  public set ConfirmPasswordNotValid(isValid) {
    this._confirmPasswordHasError = isValid
  }
  public get AllowChangePassword() {
    return this._showPasswordChange;
  }
  public set AllowChangePassword(change) {
    this._showPasswordChange = change
    this._disableConfirm = true
  }
  public get PasswordUpdateMsg() {
    return this._confirmationMessage
  }
  public get ConfirmPasswordUpdate() {
    return this._showConfirmationMsg
  }
  public set ConfirmPasswordUpdate(isValid) {
    this._showConfirmationMsg = isValid
  }
  public get BureauRecovery() {
    return this._bureauRecover;
  }
  public set BureauRecovery(isValid) {
    this._bureauRecover = isValid
  }
  public get ValidateAccountingBuroRecovery(): boolean {
    let isValid: boolean = true;

    let passregexp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    /*     if (emailregexp.test(this.AccEmail) == false) {
        isValid = false
      } */

    if (this._names.length == 0) {
      isValid = false;
    }

    if (this._newPasswordrecover != this._confirmNewpasswordrecover) {
      isValid = false;
      this._passwordErrorlabel = "Ensure Passwords Match"

    }

    if (passregexp.test(this._newPasswordrecover) == false) {
      isValid = false
    }

    if (this._newPasswordrecover.length == 0) {
      isValid = false
    }
    if (this._confirmNewpasswordrecover.length == 0) {
      isValid = false
    }


    return isValid

  }
  public get NewRecoveryPassword() {
    return this._newPasswordrecover
  }
  public set NewRecoveryPassword(pass) {
    this._newPasswordrecover = pass
  }
  public get ConfirmRecoveryPassword() {
    return this._confirmNewpasswordrecover
  }
  public set ConfirmRecoveryPassword(pass) {
    this._confirmNewpasswordrecover = pass
  }
  public get FailedUpdateError() {
    return this._accUpdateerror

  }
  public set FailedUpdateError(isValid) {
    this._accUpdateerror = isValid
  }
  public get SuccessfulUpdate() {
    return this._accUpdatesuccess
  }
  public set SuccessfulUpdate(isValid) {
    this._accUpdatesuccess = isValid
  }
  public get ForgetPassword() {
    return this._showHideforgetPassword
  }
  public set ForgetPassword(isValid) {
    this._showHideforgetPassword = isValid
  }
  public get ProviderIds() {
    return this._providerIdBureaurecover
  }
  public set ProviderIds(name) {
    this._providerIdBureaurecover = name
  }
  public get btnClear(): boolean {
    let isEmpty: boolean = true

    if (this._names.length > 0) {
      isEmpty = false
    }
    return isEmpty
  }
  public get ErrorLabel() {

    return this._passwordErrorlabel
  }
  public get PasswordLabel(): string {
    return this._passwordErrorlabel
  }  
  public get popupList(): PopUpModel[] {
    return this._popupList;
  }
  public set popupList(value: PopUpModel[]) {
    this._popupList = value;
  }

  //The testconnection property will be used to check if a SQL connection has been established
  TestConnection() {
    this._http.get(this._baseUrl + "api/Login/validConnection").subscribe(
      (connectionStatus: ValidationResultsModel) => {
        this._webAPIavailable = connectionStatus.valid;
        if (this._webAPIavailable == true) {
          this.LoadHealthPlanSetUp();
          this._server = connectionStatus.message;
          //this.LoadMemberRegistrationHealthPlans();
        }
      }, (error) => {
        this._subscribeError = true
        this._busy = false;

        if (this._settingsService.SettingsError == true) {
          this._subscribeError = true
        }
      },
      () => {

      }

    )




  }

  LoadHealthPlanSetUp() {
    this._busy = true
    let tempmembarr: any[] = []
    let tempbrokerArr: any[] = []
    let defaultval: HealthPlanSetupModel = new HealthPlanSetupModel()

    defaultval.hpCode = "000"
    defaultval.hpName = "Select a Health Plan"

    tempmembarr.push(defaultval);
    tempbrokerArr.push(defaultval);

    this._http.get(this._baseUrl + "api/Security/LoadHealthPlans", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (returnedHealthplans: HealthPlanSetupModel[]) => {
        this._healthPlansetup = returnedHealthplans
        //  console.log("All healthplans:", this._healthPlansetup)
        this._healthPlansetup.forEach(function (el) {
          if (el.membRegistration == true) {
            tempmembarr.push(el);
          }
          if (el.isEnabledBroker == true) {
            tempbrokerArr.push(el);
          }
        })
        this._memberRegistrationhealthPlans = tempmembarr;
        this._brokerRegistrationhealthPlans = tempbrokerArr;
        this._busy = false
      }, (error) => {
        this._subscribeError = true
        this._busy = false
      }
    )
  }

  public PageReload() {
    window.location.reload();
  }

  /*Login Method that will be called when the user clicks on the sign in button login page */
  Login() {
    this._busy = true
    this._http
      .post(this._baseUrl + "api/Login/LoginDetails", {
        Username: this.Username,
        Password: this.Password
      }).subscribe((webresult: LoginDetailsModel) => {
        if (webresult.success == false) {
          this.show_hide_error = true
          this._userName = "";
          this._password = "";
          this._isLoggedin = false;
          this._loggedInuser = new LoginDetailsModel();
        } else {
          this._loggedInuser = webresult;
          this.IsLoggedIn = true;
          this.GetPopMessage();
          this.Images();
          setTimeout((e) => {
            if (this._loggedInuser.userType == 0) {
              this._router.navigate(["admin-dash"])
            }
            else if (this._loggedInuser.userType == 1) {
              this._loggedInuser.provID = webresult.provID;
              this._loggedInuser.description = webresult.description;
              this._loggedInuser.username = webresult.username;
              this._loggedInuser.firstname = webresult.firstname;
              this._loggedInuser.lastname = webresult.lastname;
              this._loggedInuser.contact = webresult.contact;
              // Set local storage params for Whatsapp web chat to display
              if (this._settingsService.GetSettingEnabled('WebChat')) {
                localStorage.setItem("type", this._loggedInuser.userType.toString());
                localStorage.setItem("typeId", webresult.provID);
                localStorage.setItem("typeEm", webresult.username);
                localStorage.setItem("cEnabled", "1");               
              }              
              if (!this._loggedInuser.pharmacy) {
                this._router.navigate(["membership"])
              } else {
                this._router.navigate(["provider-dash"])
              }
            }
            else if (this._loggedInuser.userType == 2) {
              this._router.navigate(["bureau-dash"])
            }
            else if (this._loggedInuser.userType == 3) {
              this._router.navigate(["membership"]);
            }
            else if (this._loggedInuser.userType == 4) {
              this._router.navigate(["member-dashboard"]);
            }
            else if (this._loggedInuser.userType == 5) {
              this._router.navigate(["broker-dashboard"]);
            }
            else if (this._loggedInuser.userType == 6) // Wellness providers
            {
              this._loggedInuser.provID = webresult.provID;
              this._loggedInuser.description = webresult.description;
              this._loggedInuser.username = webresult.username;
              this._loggedInuser.firstname = webresult.firstname;
              this._loggedInuser.lastname = webresult.lastname;
              this._loggedInuser.contact = webresult.contact;
              this._router.navigate(["patient-wellness-form"]);
            }
            else if (this._loggedInuser.userType == 7) // Voucher Amdmin
            {
              this._router.navigate(["voucher-clients"]);
            }
          }, 2500)
        }
        
      }, (error) => {
        //todo: handle error propperly. What will the user see if this error fires???
        console.log(error);

      },
        () => {
          if (this._loggedInuser.success === true) {
            
            this.GetTokenForUser();
          }
          this._busy = false

        });

  }



  GetTokenForUser() {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Token/token', this.CurrentLoggedInUser, { responseType: "text" })
      .subscribe((results: string) => {
        let jwt: string;
        jwt = results;
        localStorage.setItem("jwt", jwt);
        this.token = results;
      },
        (error) => {
          console.log(error);
          if (error.status == '401') {
            this._router.navigateByUrl('/login');
          }
        },
        () => {
          this._token = localStorage.getItem("jwt");
          this.LoggedInUserType(this._loggedInuser.specCode);
          this._settingsService.LoadSettings();
          this.GetUserRights();
          this.LoadManuals();
          this.busy = true;
          setTimeout((a) => {
            if (this._loggedInuser.userType == 1) {
              this.CheckTCAccepted();
              this.CheckHealthPlanSetUp();
              this.LoadProviderRemittances();
            }
            if (this._loggedInuser.userType == 3) {
              this.CheckTCAccepted();
              this.CheckHealthPlanSetUp();
              this.GetSpecCodes(); // Jaco 2022-09-12
              //this.GetExistingBenefits(); // Jaco 2022-09-12
            }
            if (this._loggedInuser.userType == 2) {
              this.CheckTCAccepted();
              this.Loadremittances();
              this.ListOfProviders();
              this.CheckHealthPlanSetUp();
            }
            if (this._loggedInuser.userType == 4) {
              this.CheckTCAccepted();
              this.MemberDetails()
            }
            if (this._loggedInuser.userType == 5) {
              this.CheckTCAccepted();
            }
            if (this._loggedInuser.userType == 0) {
              this.CheckTCAccepted();
              this.LoadUsers(false);
              this.LoadLobCodes();
              this.LoadUserTypes()
            }
            this.busy = false;
          }, 2500)


          
          this._busy = false;
        })
  }

  LoggedInUserType(loggedinspec: string) {
    let SpecCode = loggedinspec;

    this._listOfDentistSpecCodes.push("54", "95", "62", "64", "92", "93", "94", "96", "113")
    this._listOfHospitalSpecCodes.push("57", "58", "59", "76", "77", "56", "51")
    this._listOfPharmaciesSpecCodes.push("60");
    let SearchArray = this._listOfDentistSpecCodes.filter(x => x === SpecCode);
    let SearchArrayPharmacy = this._listOfPharmaciesSpecCodes.filter(x => x === SpecCode);
    let SearchArrayHosp = this._listOfHospitalSpecCodes.filter(x => x === SpecCode);
    if (SearchArrayHosp.length > 0) {
      this._isSpecialist = false
      this._isDentist = false
      this._isHospital = true;
      this._isPharmacy = false;
      this.CurrentLoggedInUser.dentist = this._isDentist;
      this.CurrentLoggedInUser.specialist = this._isSpecialist;
      this.CurrentLoggedInUser.hospital = this._isHospital;
      this.CurrentLoggedInUser.pharmacy = this._isPharmacy;

    }
    else if (SearchArrayPharmacy.length > 0) {
      this._isSpecialist = false
      this._isDentist = false
      this._isHospital = false;
      this._isPharmacy = true;
      this.CurrentLoggedInUser.dentist = this._isDentist;
      this.CurrentLoggedInUser.specialist = this._isSpecialist;
      this.CurrentLoggedInUser.hospital = this._isHospital;
      this.CurrentLoggedInUser.pharmacy = this._isPharmacy;
    
    }
    else if (SearchArray.length == 0) {
      this._isSpecialist = true
      this._isDentist = false
      this._isHospital = false;
      this._isPharmacy = false;
      this.CurrentLoggedInUser.dentist = this._isDentist;
      this.CurrentLoggedInUser.specialist = this._isSpecialist;
      this.CurrentLoggedInUser.hospital = this._isHospital;
      this.CurrentLoggedInUser.pharmacy = this._isPharmacy;

    }else {
      this._isDentist = true;
      this._isSpecialist = false;
      this._isHospital = false;
      this._isPharmacy = false;
      this.CurrentLoggedInUser.dentist = this._isDentist;
      this.CurrentLoggedInUser.specialist = this._isSpecialist;
      this.CurrentLoggedInUser.hospital = this._isHospital;
      this.CurrentLoggedInUser.pharmacy = this._isPharmacy;
    }

  }

  CheckHealthPlanSetUp() {
    this._healthPlansetup.forEach(el => {
      if (el.isEnabledMember == true) {
        this.GetHPContractsForDropDownMember(el.isEnabledMember)
      }
      if (el.isEnabledClaim == true) {
        this.GetHPContractsForDropDownClaim(el.isEnabledClaim)
      }

    });
  }

  GetHPContractsForDropDownMember(val: boolean) {
    let defaultval: HP_ContractsModel = new HP_ContractsModel()
    let c: boolean = false;
    if (this._loggedInuser.contract == 'CONTRACTED') {
      c = true;
    } else {
      c = false;
    }
    this._http.post(this._baseUrl + "api/Security/MemberDropdown", {
      isEnabledMember: val,
      isDentist: this._isDentist,
      isSpecialist: this._isSpecialist,
      isHospital: this._isHospital,
      contracted: c
    }, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((returenedhealthplansmember: HP_ContractsModel[]) => {
      this._listOfhealthplansMember = returenedhealthplansmember
      this._listOfhealthplansMemberBenScreen = returenedhealthplansmember
      defaultval.hpcode = "000"
      defaultval.hpname = "Select a Health Plan"
      this._listOfhealthplansMember.push(defaultval)
      this._listOfhealthplansMemberBenScreen.push(defaultval)
    })
  }

  GetPopMessage() {
    this.busy = true;
    this._http.post(this._baseUrl + "api/Login/GetPopMessage", this.CurrentLoggedInUser,
      {
      headers: new HttpHeaders({
        
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
      }).subscribe((popups: PopUpModel[]) => {
        this.popupList = [];
        this.popupList = popups;

    },

      (error) => { console.log(error); this.busy = false;},
      () => {
        this.busy = false;
        if (this.popupList.length > 0) {
          this.PopModals(0);
        }
        
        
      })
  }

  GetHPContractsForDropDownClaim(val: boolean) {
    let c: boolean = false;
    if (this._loggedInuser.contract == 'CONTRACTED') {
      c = true;
    } else {
      c = false;
    }
    let defaultval: HP_ContractsModel = new HP_ContractsModel()
    this._http.post(this._baseUrl + "api/Security/ClaimDropdown", {
      hpcode: "",
      hpname: "",
      isEnabledClaim: val,
      isEnabledMember: !val,
      userType: this._loggedInuser.userType,
      isDentist: this._isDentist,
      isSpecialist: this._isSpecialist,
      isHospital: this._isHospital,
      isPharmacy: this._isPharmacy,
      contracted: c
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returenedhealthplansmember: HP_ContractsModel[]) => {
        this._listOfhealthplansClaim = returenedhealthplansmember
        defaultval.hpcode = "000"
        defaultval.hpname = "Select a Health Plan"
        this._listOfhealthplansClaim.push(defaultval)
      },
        (error) => {
          console.log(error);
        })
  }

  public LoadProviderRemittances() {
    this._http.post(this._baseUrl + "api/Security/GetProviderRemittances", {
      ProvID: this._loggedInuser.provID

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedremittance: ProviderRemittancesModel[]) => {
        this._providerRemittances = returnedremittance
      }, (error) => {
        this._subscribeError = true
        this._busy = false

        if (this._settingsService.SettingsError == true) {
          this._subscribeError = true
        }
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      });
  }

  Images() {
    let dentalSpecCodes: string[] = ["54", "62", "64", "92", "93", "94", "95", "96", "98", "113"];

    if (this.CurrentLoggedInUser.userType == 1) {
      let found: boolean = false;
      for (var i = 0; i < dentalSpecCodes.length; i++) {
        if (dentalSpecCodes[i] == this.CurrentLoggedInUser.specCode) {
          found = true;
          break;
        }
      }
      if (found == true) {
        this._settingsService.DRCDetails = true;
        this._selectedimage.imgDRC = "DRC_Logo.png"
        this._selectedimage.widthDRC = 300
        this._selectedimage.heightDRC = 264
      } else {
        this._settingsService.PAMCDetails = true;
        this._selectedimage.imgPAMC = "PAMC - Logo.png"
        this._selectedimage.widthPAMC = 355
        this._selectedimage.heightPAMC = 191
      }
    }
    if (this.CurrentLoggedInUser.userType == 0) {

      this._settingsService.PAMCDetails = true;
      this._selectedimage.imgCombined = "DRC_PAMC_Logo.png"
      this._selectedimage.widthCombined = 900
      this._selectedimage.heightCombined = 270

    }
    if (this.CurrentLoggedInUser.userType == 2) {
      this._settingsService.PAMCDetails = true;
      this._selectedimage.imgPAMC = "PAMC - Logo.png"
      this._selectedimage.widthPAMC = 355
      this._selectedimage.heightPAMC = 191


      this._settingsService.DRCDetails = true;
      this._selectedimage.imgDRC = "DRC_Logo.png"
      this._selectedimage.widthDRC = 300
      this._selectedimage.heightDRC = 264

    }
    if (this.CurrentLoggedInUser.userType == 3) {
      if (this._loggedInuser.client.toUpperCase().trim() == "DRC") {
        this._settingsService.DRCDetails = true;
        this._selectedimage.imgDRC = "DRC_Logo.png"
        this._selectedimage.widthDRC = 300
        this._selectedimage.heightDRC = 264
      }
      if (this._loggedInuser.client.toUpperCase().trim() == "PAMC") {
        this._settingsService.PAMCDetails = true;
        this._selectedimage.imgPAMC = "PAMC - Logo.png"

        this._selectedimage.widthPAMC = 300
        this._selectedimage.heightPAMC = 162

      }
      if (this._loggedInuser.client.toUpperCase().trim() == "ALL") {
        this._settingsService.AllDetails = true;

        this._settingsService.PAMCDetails = true;
        this._selectedimage.imgPAMC = "PAMC - Logo.png"
        this._selectedimage.widthPAMC = 355
        this._selectedimage.heightPAMC = 191


        this._settingsService.DRCDetails = true;
        this._selectedimage.imgDRC = "DRC_Logo.png"
        this._selectedimage.widthDRC = 300
        this._selectedimage.heightDRC = 264
      }

    }
    return this._selectedimage
  }

  GetSpecCodes() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Authorization/GetSPecCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: SpecCodes[]) => {
        this.specList = [];
        result.forEach((el) => {
          this.specList.push(el);
        });
      },
        (error) => { console.log(error) },
        () => {
          this._busy = false;
          //this.GetBenTrackCodes(); //Jaco 2022-09-12
        })
  }

  GetBenTrackCodes() {
    this._busy = true;
    let token = localStorage.getItem('jwt');
    this.bencodes = [];
    this._http.get(this._baseUrl + 'api/Authorization/GetBenTypeCodes',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((results: BenTrackCodes[]) => {
        results.forEach((el) => {
          this.bencodes.push(el);
        })
      },
        (error) => {
          console.log(error)
        },
        () => { this._busy = false; })
  }

  GetExistingBenefits() {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Authorization/GetExistingBef', this.CurrentLoggedInUser,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Specmasters[]) => {
      this.existingBen = [];
      results.forEach((el) => {
        this.existingBen.push(el);
      })
    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
      })
  }

  public Loadremittances() {
    this._busy = true;
    this._http.post(this._baseUrl + "api/Security/GetAccountingBureauRemittances", {
      bureauId: this._loggedInuser.bureauId
    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedremittance: AccountingBureauRemittancesModel[]) => {
        this._remittances = returnedremittance
        this._busy = false;
        if (returnedremittance.length != 0) {
          this._remittanceLoader = false;
        } else {
          this._remittanceLoader = true;
        }

      }, (error) => {
        this._subscribeError = true
        this._busy = false;

        if (this._settingsService.SettingsError == true) {
          this._subscribeError = true
        }
      });
  }

  public ListOfProviders() {
    this._http.post(this._baseUrl + "api/Security/getProviders", {
      bureauId: this._loggedInuser.bureauId

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((returnedProviders: BureauProvidersModel[]) => {
        this._listOfProvider = returnedProviders
      });
  }

  /*Adds the entered provider ID's to the array when the user clicks on the "+" incon on the registration page*/
  AddToArray() {
    this._names.push(this._providerIdBureaurecover)
    this._providerIdBureaurecover = "";

  }

  /*Clears the Array of Provider Id's when the user clicks on the trash can*/
  ClearingProviderArray() {
    this._names = [];
  }



  MemberDetails() {
    this._http.post(this._baseUrl + "api/Member/LoginMemberInfo", {
      memberIdNumber: this._loggedInuser.memberIdNumber

    },
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((memberInfo: MemberInfoModel[]) => {
      this._memberInfo = memberInfo;
    });
  }

  
  ValidateAccountingBuroProvider() {
    if ((this._providerIdBureaurecover.length > 0)) {
      this._enableAddProvider = true;
      return this._enableAddProvider;
    }
  }

  LoadUsers(data: boolean) {
    this._http.get(this._baseUrl + "/api/Security/LoadUsers", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (ReturnedUsers: UserGridModel[]) => {
        this._originalListofUsers = ReturnedUsers
        this._listOfusers = this._originalListofUsers
        this._listOfusers.forEach((el) => {
          let c: number = el.password.length;
          let d: string = "";
          el.hiddenPass = d.padEnd(c, "*");
        })
      }
    );
  }

  LoadLobCodes() {
    this._http.get(this._baseUrl + "api/Security/LoadLobCodes", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (ReturnedLobCodes: LOBcodesModel[]) => {
        this._lobCodes = ReturnedLobCodes
        //  console.log("Retruned LOB codes:", this._lobCodes)
        let code: LOBcodesModel = new LOBcodesModel();
        code.code = "000";
        code.descr = "Select Lob Code";
        this._lobCodes.push(code)
      }
    )
  }

  LoadUserTypes() {
    let userReg = []
    let defaultValue: UsertypeModel = new UsertypeModel();

    defaultValue.userTypeId = "000"
    defaultValue.userType = "Please Select Option"

    userReg.push(defaultValue);

    this._http.get(this._baseUrl + "api/Security/LoadUserTypes", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (RecievedUserTypes: UsertypeModel[]) => {
        this._userTypesarray = RecievedUserTypes
        this._userTypesarray.forEach(function (x) {
          if (x.userRegistration == true) {
            userReg.push(x);
          }
        })
        this._userRegistration = userReg

      }
    )
  }


  GetUserRights() {
    this._http.post(this._baseUrl + 'api/Login/GetUserRights', this._loggedInuser,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: UserRightsModel) => {
      this.userRights = results;


    },
      (error) => {
        console.log(error);

      },
      () => {
        this._busy = false
      });

  }

  LoadManuals() {
    let currYear = new Date().getFullYear().toString();
    let lastYear = (new Date().getFullYear() - 1).toString();
    let opsManuals = [];
    let providerGuides = [];

    this._http.get(this._baseUrl + "api/configuration/GetManuals", {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe(
      (RecievedManuals: ManualsModel[]) => {
        this._manuals = RecievedManuals
        if ((this.CurrentLoggedInUser.contract == 'NON-CONTRACTED') && (this.CurrentLoggedInUser.dentist)) {
          opsManuals = this._manuals.filter(x => x.fileName.includes("Ops Manual"));
          let ops2020 = opsManuals.filter(x => x.fileLink.includes(currYear))
          let ops2019 = opsManuals.filter(x => x.fileLink.includes(lastYear))
          ops2019 = ops2019.filter(x => x.fileLink.includes("Non_Contracted"))
          ops2020 = ops2020.filter(x => x.fileLink.includes("Non_Contracted"))
          this._manuals2020 = ops2020
          this._manuals2019 = ops2019

          //if (this.CurrentLoggedInUser.provClass == '5') {

          //  let c: number = 0;                         
          //  let count: number = 0;
          //  this._manuals2019.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  if (count == 0) {
          //    if (this._manuals2019[0].fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      this._manuals2019.splice(count, 1);
          //    }
          //  } else {
          //    this._manuals2019.splice(count, 1);
          //  }
            
          //  c = 0;                                   
          //  count = 0;
          //  this._manuals2020.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  if (count == 0) {
          //    if (this._manuals2020[0].fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      this._manuals2020.splice(count, 1);
          //    }
          //  } else {
          //    this._manuals2020.splice(count, 1);
          //  }
            
          //}

          //else {
                          
          //    let c: number = 0;                     
          //    let count: number = 0;
          //    this._manuals2019.forEach((el) => {
          //      if (el.fileName.includes("Ops Manual A - Managed Providers")) {
          //        count = c;
          //      }
          //      c++;
          //    });
          //  if (count == 0) {
          //    if (this._manuals2019[0].fileName.includes("Ops Manual A - Managed Providers")) {
          //      this._manuals2019.splice(count, 1);
          //    }
          //  } else {
          //    this._manuals2019.splice(count, 1);
          //  }
                        
          //    c = 0;                           
          //    count = 0;
          //    this._manuals2020.forEach((el) => {
          //      if (el.fileName.includes("Ops Manual A - Managed Providers")) {
          //        count = c;
          //      }
          //      c++;
          //    });
          //  if (count == 0) {
          //    if (this._manuals2020[0].fileName.includes("Ops Manual A - Managed Providers")) {
          //      this._manuals2020.splice(count, 1);
          //    }
          //  } else {
          //    this._manuals2020.splice(count, 1);
          //  }

          //}
        }

        if ((this.CurrentLoggedInUser.contract == 'CONTRACTED') && (this.CurrentLoggedInUser.dentist)) {
          opsManuals = this._manuals.filter(x => x.fileName.includes("Ops Manual"));
          let ops2020 = opsManuals.filter(x => x.fileLink.includes(currYear) && !x.fileLink.includes("Non_Contracted"))          
          let ops2019 = opsManuals.filter(x => x.fileLink.includes(lastYear) && !x.fileLink.includes("Non_Contracted"))
          this._manuals2020 = ops2020
          this._manuals2019 = ops2019

          //if (this.CurrentLoggedInUser.provClass == '5') {

          //  let c: number = 0;                 
          //  let count: number = 0;
          //  this._manuals2019.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  this._manuals2019.splice(count, 1);
               
          //  c = 0;                            
          //  count = 0;
          //  this._manuals2020.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  this._manuals2020.splice(count, 1);

          //}

          //else {
            
          //  let c: number = 0;        
          //  let count: number = 0;
          //  this._manuals2019.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - Managed Providers")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  this._manuals2019.splice(count, 1);

            

          //  c = 0;                   
          //  count = 0;
          //  this._manuals2020.forEach((el) => {
          //    if (el.fileName.includes("Ops Manual A - Managed Providers")) {
          //      count = c;
          //    }
          //    c++;
          //  });
          //  this._manuals2020.splice(count, 1);
          //}
        }

        if (this.CurrentLoggedInUser.provClass == '5') {
          this._manuals2019 = this._manuals2019.filter(x => !x.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")); //Jaco 2022-03-31
          this._manuals2020 = this._manuals2020.filter(x => !x.fileName.includes("Ops Manual A - General Providers - Capitation Agreement")); //Jaco 2022-03-31
        }

        else {
          this._manuals2019 = this._manuals2019.filter(x => !x.fileName.includes("Ops Manual A - Managed Providers - Capitation Agreement")); //Jaco 2022-03-31
          this._manuals2020 = this._manuals2020.filter(x => !x.fileName.includes("Ops Manual A - Managed Providers - Capitation Agreement")); //Jaco 2022-03-31
        }

        providerGuides = this._manuals.filter(x => x.fileName.includes("PAMC"));
        let provGuides2020 = providerGuides.filter(x => x.fileLink.includes(currYear))
        let provGuides2019 = providerGuides.filter(x => x.fileLink.includes(lastYear))

        this._guides2020 = provGuides2020
        this._guides2019 = provGuides2019

      }
    )
  }

  CheckTCAccepted() {

    this._http.post(this._baseUrl + 'api/Login/CheckTandC', this.CurrentLoggedInUser, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((recievedValue: boolean) => {
      this.poptcModel = !recievedValue;
    },
      (error) => {
        console.log(error);
      })
  }

  /*Validates the Change Password section to make sure user has chosen a valid password*/
  CheckPasswordOld() {
    let passregexp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");

    if (this._passwordChange.oldPassword == "" || this._passwordChange.oldPassword == undefined) {
      this._oldPasswordError = "Please Enter Your Current Password"
      this._oldPasswordHasError = true;
      this._disableConfirm = true
    }
    else {
      if (this._passwordChange.oldPassword != this._loggedInuser.password) {
        this._oldPasswordError = "The Password Is Incorrect, Please Try Again"
        this._oldPasswordHasError = true;
        this._disableConfirm = true
      } else {
        this._oldPasswordHasError = false;
      }
    }
  }

  CheckPasswordNew() {
    let passregexp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");
    if (this._passwordChange.newPassword == "" || this._passwordChange.newPassword == undefined) {
      this._newPasswordError = "Please Enter Your New Password"
      this._newPasswordHasError = true;
      this._disableConfirm = true
    } else {
      if (passregexp.test(this._passwordChange.newPassword) == false) {
        this._newPasswordError = "Password needs to contain 6 or more characters, 1 Capital and 1 Special Character."
        this._newPasswordHasError = true;
        this._disableConfirm = true
      } else {
        this._newPasswordHasError = false;
      }
    }
  }

  CheckPasswordConfrim() {
    let passregexp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})");
    if (this._passwordChange.confirmPassword == "" || this._passwordChange.confirmPassword == undefined) {
      this._confirmPasswordError = "Please Confirm Your Password"
      this._confirmPasswordHasError = true;
      this._disableConfirm = true
    } else {
      if (passregexp.test(this._passwordChange.confirmPassword) == false) {
        this._confirmPasswordError = "Password needs to contain 6 or more characters, 1 Capital and 1 Special Character."
        this._confirmPasswordHasError = true;
        this._disableConfirm = true
      } else {
        if (this._passwordChange.newPassword != this._passwordChange.confirmPassword) {
          this._confirmPasswordError = "Ensure Passwords Match."
          this._confirmPasswordHasError = true;
          this._disableConfirm = true
        } else {
          this._confirmPasswordHasError = false;
          this._disableConfirm = false
        }
      }

    }
  }


  UpdateTCAccepted() {
    this._http.post(this._baseUrl + 'api/Login/UpdateTCAccepted', this.CurrentLoggedInUser, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((recievedDate) => {

    },
      (error) => {
        console.log(error);
        if (error.status == '401') {
          this._router.navigateByUrl('/login');
        }
      },
      () => { /*this.poptcModel = false; */ });
  }

  UpdatePasswordRequest() {

    if (this.CurrentLoggedInUser.userType == 1) {
      this._http.post(this._baseUrl + "api/Security/ChangePassword", {
        Email: this._loggedInuser.username,
        NewPassword: this._passwordChange.newPassword,
        provPracNum: this.CurrentLoggedInUser.provID,
        ChangeBy: this._loggedInuser.username,
        ChangeDate: '2021/08/06',
      }, {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
        , responseType: "text"
      }
      ).subscribe((results: string) => {
        this._confirmationMessage = results;
        this._showPasswordChange = false;
        this._showConfirmationMsg = true;

      });
    } else if (this.CurrentLoggedInUser.userType == 2) {
      this._http.post(this._baseUrl + "api/Security/ChangePassword", {
        Email: this._loggedInuser.username,
        NewPassword: this._passwordChange.newPassword
      }, {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
        , responseType: "text"
      }
      ).subscribe((results: string) => {
        this._confirmationMessage = results;
        this._showPasswordChange = false;
        this._showConfirmationMsg = true;

      });
    } else if (this.CurrentLoggedInUser.userType == 3) {
      this._http.post(this._baseUrl + "api/Security/UpdateClientPassword", {
        Password: this._passwordChange.newPassword,
        ChangeBy: this._loggedInuser.username,
        ChangeDate: '2021/08/06',
        Email: this._loggedInuser.username

      }, {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
        , responseType: "text"
      }
      ).subscribe((results: string) => {
        this._confirmationMessage = results;
        this._showPasswordChange = false;
        this._showConfirmationMsg = true;

      });
    }


  }

  ChangePassword() {
    this._showPasswordChange = true
  }

  /*Clearing all fields in the Accounting bureau recovery*/
  ClearBureauRecovery() {
    this._newPasswordrecover = "";
    this._confirmNewpasswordrecover = "";
    this.ClearingProviderArray()
  }

  UpdatePassword() {
    this._http.post(this._baseUrl + "api/Security/NewPassword", {
      Username: this._userName,
      NewPassword: this._newPasswordrecover,
      ListofProviders: this._names,
      createBy: this._loggedInuser.username,
      ChangeBy: this._loggedInuser.username
    }, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).subscribe((updatedlogindetails: LoginDetailsModel) => {

      this._loggedInuser.success = updatedlogindetails.success

      this._loggedInuser.errorMessage = updatedlogindetails.errorMessage

      if (this._loggedInuser.success == false) {
        this._accUpdateerror = true
        this._recoverAccount = false;
        this._accemailValidationregistermodal = false;
        this._bureauRecover = false;
        this.ClearBureauRecovery()
      }
      if (this._loggedInuser.success == true) {
        this._accUpdatesuccess = true
        this._recoverAccount = false;
        this._accemailValidationregistermodal = false;
        this._bureauRecover = false;
        this.ClearBureauRecovery()
      }


    }, (error) => {
      if (this._loggedInuser.success == false) {


      }
      if (error.status == '401') {
        this._router.navigateByUrl('/login');
      }
    });
  }
  ForgetPasswordModal() {
    this._showHideforgetPassword = true
    this._recoverAccount = true;
  }


  ClearLoginFields() {
    this._userName = "";
    this._password = "";
  }

  PopModals(count: number) {
    let p: PopUpModel = this.popupList[count];
    this.currPopMsg = count;
    if (p.height <= 324 && p.width <= 576 && p.filetype == 2) {
      this.smallPopModal = true;
      this.mediumPopModal = false;
      this.largePopModal = false;
    }
    else if ((p.height >= 324 && p.width >= 576) && (p.height <= 486 && p.width <= 864) && p.filetype == 2) {
      this.smallPopModal = false;
      this.mediumPopModal = true;
      this.largePopModal = false;
    }
    else if ((p.height >= 486 && p.width >= 864) && p.filetype == 2) {
      this.smallPopModal = false;
      this.mediumPopModal = false;
      this.largePopModal = true;
    } else if (p.filetype == 1) {
      this.smallPopModal = true;
      this.mediumPopModal = false;
      this.largePopModal = false;
    }
  }

  MarkPopUpAsAccepted(p: PopUpModel) {
    this.busy = true;
    this._http.post(this._baseUrl + "api/Login/MarkPopUpAsAccepted", p,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).subscribe((result: ValidationResultsModel) => {
   
      },

        (error) => { console.log(error); this.busy = false; },
        () => {
          this.busy = false;
          if (this.popupList.length > (this.currPopMsg+1)) {
            this.PopModals(this.currPopMsg + 1);
          }

        })
  }
}

import { Injectable, Inject } from '@angular/core';
import { ClaimReport } from '../models/ClaimReport';
import { HealthplanModel } from '../models/HealthplanModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SecurityService } from './security.service';
import { RejectionCodesModel } from '../models/RejectionCodesModel';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class GraphsService {

  public _claimsData: any[] = [];
  public _paidGraphOptions: any;
  public _paidUnpaidSplitGraphOptions: any;
  private _monthType: string = "paidMonth"
  private _yearType: string = "paidYear"
  private _paidYears: number[] = []
  private _serviceYears: number[] = []
  private _serviceYMs: string[] = []
  private _serviceDatePaidDate: boolean = false;
  private _hpcodes: ClaimReport;
  public _hpcodeList: HealthplanModel[] = [];
  public _busy: boolean = true;
  public _showGraph: boolean = false;

  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _loginService: LoginService) { }


  get serviceDatePaidDate(): boolean {
    return this._serviceDatePaidDate;
  }
  set serviceDatePaidDate(value: boolean) {
    if (value === undefined) throw 'Please supply Paid/ Unpaid value';
    this._serviceDatePaidDate = value;
    if (value) {
      this._monthType = "paidMonth"
      this._yearType = "paidYear"
    } else {
      this._monthType = "serviceMonth"
      this._yearType = "serviceYear"
    }
    this.GeneratePaidGraphOptions();
  }

  HpCodes() {
    let p = this;
    p._busy = true;
    let x: RejectionCodesModel;
    x = new RejectionCodesModel();
    x.username = this._loginService.CurrentLoggedInUser.username;
    x.userType = this._loginService.CurrentLoggedInUser.userType;
    this._http.post(this._baseUrl + 'api/ClaimCapture/GetHpCodes', x,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._loginService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }).
      subscribe((returnedList: [HealthplanModel]) => {
        this._hpcodeList = [];
        this._hpcodeList = returnedList;
        this._hpcodes = new ClaimReport();
        if (this._hpcodeList.length > 1) {
          for (var i = 0; i < this._hpcodeList.length; i++) {
            if (i == 0) {
              this._hpcodes.hpcode = this._hpcodeList[i].code;
            } else {
              if (i == this._hpcodeList.length - 1) {
                this._hpcodes.hpcode = this._hpcodes.hpcode + ';' + this._hpcodeList[i].code;
              } else {
                this._hpcodes.hpcode = this._hpcodes.hpcode + this._hpcodeList[i].code + ';';
              }

            }
          }
        } else {

          this._hpcodes.hpcode = this._hpcodeList[0].code;
        }

      },
        (error) => {
          console.log(error);
        },
        () => {
          this.GetClaimsData(this._hpcodeList[0].code);
          p._showGraph = true;
        });
  }

  public GetClaimsData(hpcode: string) {

    this._hpcodeList.forEach((el) => {
      if (el.code == hpcode) {
        el.activeTab = true;
      } else {
        el.activeTab = false;
      }
    });
    this._busy = true;
    let p = this;
    this._hpcodes.hpcode = hpcode;
    this._claimsData.splice(0, this._claimsData.length);
    this._http.post(this._baseUrl + 'api/Report/GetGraphReport', this._hpcodes, {
      headers: new HttpHeaders({
        "Authorization": "Bearer " + this._loginService.token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }).
      subscribe((returnedList: any[]) => {


        returnedList.forEach((el) => {
          p._claimsData.push(el);
        })


        this.GetYearsFromData();
        this.GeneratePaidGraphOptions();
        this.GeneratePaidUnpaidSplit();
        this._busy = false;


      },
        (error) => { console.log(error); },
        () => {
          this._busy = false;
          p._showGraph = true;
        });
  }


  private GetYearsFromData() {
    let p = this;

    this._claimsData.forEach(
      (item) => {
        let svcYM: string = item["serviceYM"].toString();;
        let paidYear: string = item["paidYear"].toString();
        let svcYear: string = item["serviceYear"].toString();

        let tmpPaidYear: number;
        let tmpSvcYear: number;

        tmpPaidYear = parseInt(paidYear)
        tmpSvcYear = parseInt(svcYear)

        let ind = this._paidYears.indexOf(tmpPaidYear)
        if (ind < 0) {
          this._paidYears.push(tmpPaidYear);
        }

        ind = this._serviceYears.indexOf(tmpSvcYear)
        if (ind < 0) {
          this._serviceYears.push(tmpSvcYear);
        }

        ind = this._serviceYMs.indexOf(svcYM);
        if (ind < 0) {
          this._serviceYMs.push(svcYM);
        }
      }
    );
  }

  private GeneratePaidGraphOptions() {
    let p = this;

    let xAxisData = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    let seriesList = [];
    let seriesDataList = [];

    let seriesDict = [];
    //*************************** */
    if (p.serviceDatePaidDate) {
      seriesList = p._paidYears;
    } else {
      seriesList = p._serviceYears;
    }

    seriesList = seriesList.sort((el, el2) => {
      if (Number(el) > Number(el2)) {
        return -1;
      } else {
        return 1;
      }
    })
    seriesList.forEach(element => {


      let seriesDataPoints = p._claimsData.filter((item) => {
        if (item.paidYear == element) {
          return item;
        }
      });

      let paidAmounts = [];

      for (let index = 1; index < 13; index++) {

        let monthData = seriesDataPoints.filter(element => {
          if (element.paidMonth == index) {
            return element;
          }
        });


        let paidAmount = 0.0;
        monthData.forEach(month => {
          if ((month.isRiskOpt == "NonRisk") && (month.status == "9")) {
            paidAmount = paidAmount + month.net;
          }

        });

        paidAmounts.push(paidAmount.toFixed(2));

      }

      let seriesData = {
        name: element,
        type: 'line',
        data: paidAmounts,
        animationDelay: function (idx) {
          return idx * 10;
        }
      };

      seriesDict[element] = seriesData;
      seriesDataList.push(seriesData);
    });
    p._paidGraphOptions = {
      title: {
        text: 'Paid Claims',
        subtext: this._serviceDatePaidDate ? 'Grouped by Paid Dates' : 'Grouped by Service Dates',
        x: 'center',
        visible: false
      },
      legend: {
        data: seriesList,
        align: 'left',
        x: 'left',
        y: 'bottom'
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false
        }
      },
      yAxis: {
      },
      series: seriesDataList,
      animationEasing: 'elasticOut',
      animationDelayUpdate: function (idx) {
        return idx * 5;
      }
    };
  }

  private GeneratePaidUnpaidSplit() {
    let thisInstance = this;
    let xAxisData = this._serviceYMs;
    console.log('Sort xAxis')
    xAxisData.sort(
      //(el, el2) => {
      //if (Number(el) > Number(el2)) {
      //  return -1;
      //} else {
      //  return 1;
      //}
      //}

      (a, b) => a < b ? -1 : 1
    )

    let paidAmounts = []
    let unPaidAmounts = []
    xAxisData.forEach(
      itemYM => {
        let paidAmount = 0.0
        let unPaidAmount = 0.0
        let monthData = this._claimsData.filter(element => {
          return element["serviceYM"] == itemYM;
        });
        monthData.forEach(
          svcMonthItem => {
            if (svcMonthItem["status"].toString() == "9") {
              paidAmount += parseInt(svcMonthItem.net);
            } else {
              unPaidAmount += parseInt(svcMonthItem.net);
            }
          }
        );
        paidAmounts.push(paidAmount);
        if (unPaidAmount < 0) {
          unPaidAmount = 0;
        }
        unPaidAmounts.push(unPaidAmount);


      }
    );


    var upColor = '#00da3c';
    var downColor = '#ec0000';
    thisInstance._paidUnpaidSplitGraphOptions = {
      title: {
        text: 'Paid/Unpaid Claims'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          label: {
            backgroundColor: '#6a7985'
          }
        }
      },
      legend: {
        data: ['Unpaid Claims', 'Paid Claims']
      },
      grid: {
        left: '5%',
        right: '6%',
        bottom: '5%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {
            title: "Download Image of Chart"

          },
          dataZoom: {
            yAxisIndex: false,
            title: { "zoom": "Zoom Chart", "back": "Remove Zoom" }
          }
        }
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: false,
          data: xAxisData

        }
      ],
      yAxis: [
        {
          type: 'value'

        }
      ],
      series: [

        {
          name: 'Paid Claims',
          type: 'line',
          stack: 'WWWWWWWW',
          label: {
            position: 'TopLeft',
            normal: {

              show: true,
              formatter: function (data) {
                return thisInstance.GetFormattedValue(data);
              },
              color: '#151515'
            }
          },
          areaStyle: { normal: {} },
          data: paidAmounts
        },
        {
          name: 'Unpaid Claims',
          type: 'line',
          stack: 'WWWWWWWW',
          label: {
            normal: {
              show: true,
              formatter: function (data) {

                return thisInstance.GetFormattedValue(data);
              },
              position: 'BottomRight',
              color: '#151515',
              offset: [0, -15],
            }

          },
          areaStyle: { normal: {} },
          data: unPaidAmounts

        }
      ]
    };
    thisInstance._busy = false;
    thisInstance._showGraph = true;
  }

  public counter = 1;
  public GetFormattedValue(data: any): any {
    var v = data.value;
    if (Number.isNaN(v)) {
      this._showGraph = true;
      return '';
    }

    if (v < 1000) {
      this._showGraph = true;
      return '';
    }
    let suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
    let rounded = 0.0;
    let exp = Math.floor(Math.log(v) / Math.log(1000));

    if (this.counter == 1) {
      this._showGraph = true;
      this.counter++;
      return (v / Math.pow(1000, exp)).toFixed(2) + suffixes[exp - 1] + "\n\n";

    }
    if (this.counter == 2) {
      this._showGraph = true;
      this.counter = 1;
      return "\n\n" + (v / Math.pow(1000, exp)).toFixed(2) + suffixes[exp - 1];

    }

  }

  GenerateAgeReport(hpcode: string) {
    let claim: ClaimReport = new ClaimReport();
    claim.user = this._loginService.CurrentLoggedInUser.username;

    claim.hpcode = hpcode;
    let reportname: string;
    this._http.post(this._baseUrl + 'api/Report/GetAgeReport', claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._loginService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {
      let x: any;
      x = recieved;
      reportname = x.message;
    },
      (error) => {
        console.log(error);
        console.log(error);
      },
      () => {
        let today: Date = new Date;
        let dateString: string;
        dateString = today.toDateString() + '_' + hpcode + '_AGEANALYSIS.xlsx';
        var a = document.createElement('a');
        a.setAttribute('type', 'hidden');
        a.target = "_blank"
        //a.href = '../../PamcPortal/assets/Reports/' + reportname;
        a.href = 'assets/Reports/' + reportname;
        a.download = dateString;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._busy = false;
      });
  }


  GenerateLagReport(hpcode: string) {
    let claim: ClaimReport = new ClaimReport();
    claim.user = this._loginService.CurrentLoggedInUser.username;
    claim.hpcode = hpcode;
    let reportname: string;
    this._http.post(this._baseUrl + 'api/Report/GetLagReport', claim,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + this._loginService.token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((recieved) => {
      let x: any;
      x = recieved;
      reportname = x.message;
    },
      (error) => {
        console.log(error);
        console.log(error);
      },
      () => {
        let today: Date = new Date;
        let dateString: string;
        dateString = today.toDateString() + '_' + hpcode + '_LAGREPORT.xlsx';
        var a = document.createElement('a');
        a.setAttribute('type', 'hidden');
        a.target = "_blank"
        //a.href = '../../PamcPortal/assets/Reports/' + reportname;
        a.href = 'assets/Reports/' + reportname;
        a.download = dateString;
        document.body.appendChild(a);
        a.click();
        a.remove();
        this._busy = false;
      });
  }

}

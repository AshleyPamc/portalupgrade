import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chronic-cond-update',
  templateUrl: './chronic-cond-update.component.html',
  styleUrls: ['./chronic-cond-update.component.css']
})
export class ChronicCondUpdateComponent implements OnInit {

  public _proceedPrompt: boolean = false;

  constructor(public _appService: AppService, private router: Router) { }

  ngOnInit() {

    this._appService.ChronicConditionService.loadChronicConditionSetup();

    // Check if the user is coming to update from an application
    if (this._appService.ChronicConditionService.CurrentPage == 'ChronicCondApp' &&
      this._appService.ChronicConditionService.ConditionForm.get('conditionCode').value != null &&
      this._appService.ChronicConditionService.ConditionForm.get('conditionCode').value.trim() != '')
    {

      this._proceedPrompt = true;

    } else {

      this._appService.ChronicConditionService.CurrentPage = 'ChronicCondUpdate';

    }

  }

  proceedToUpdateFromApplication() {

    this._appService.ChronicConditionService.CurrentPage = 'ChronicCondUpdate';
    this._appService.ChronicConditionService.doResets();
    this._proceedPrompt = false;
  }

  returnToApplication() {

    this._appService.ChronicConditionService.CurrentPage = 'ChronicCondApp';
    this.router.navigate(['/chronic-cond-app']);
    this._proceedPrompt = false;
  }

}

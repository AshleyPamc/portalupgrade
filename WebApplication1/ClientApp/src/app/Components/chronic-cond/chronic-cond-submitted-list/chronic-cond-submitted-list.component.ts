import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { ChronicConditionApplicationModel } from '../../../models/ChronicConditionApplicationModel';

@Component({
  selector: 'app-chronic-cond-submitted-list',
  templateUrl: './chronic-cond-submitted-list.component.html',
  styleUrls: ['./chronic-cond-submitted-list.component.css']
})
export class ChronicCondSubmittedListComponent implements OnInit {

  private submissionHistory: ChronicConditionApplicationModel[] = [];

  private showSubmittedAppModal: boolean = false;

  private busy: boolean;
  private error: boolean;
  private errorMessageHeading: string;
  private errorMessage: string;

  constructor(public _appService: AppService) { }

  ngOnInit() {
    this.refreshSubmittedList();
  }

  refreshSubmittedList() {

    this.busy = true;

    let submissionHist: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();
    submissionHist.provId = this._appService.LoginService.CurrentLoggedInUser.provID;

    this._appService.ChronicConditionService.GetSubmissionHistory(submissionHist)
      .subscribe(
        (results: ChronicConditionApplicationModel[]) => {

          if (results.length > 0) {

            if (results[0].success) {
              //results.forEach((el) => {
              //  this.submissionHistory.push(el);
              //})
              this.submissionHistory = results;
            }
            else {
              this.error = true;
              this.errorMessageHeading = "Could not get submission history";
              this.errorMessage = results[0].message;
            }

          }
        },
        (error) => {
          console.log(error)
          this.error = true;
          this.errorMessageHeading = "Could not get submission history";
          this.errorMessage = "Unexpected error occured";
          this.busy = false;
        },
        () => {
          this.busy = false;
        }
      )
  }

  viewSubmittedApplication(condAppNo: string) {

    let applicationNo: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();
    applicationNo.conditionApplicationNumber = condAppNo;

    this._appService.ChronicConditionService.ViewSubmittedApplication(applicationNo);
    this.showSubmittedAppModal = true;    
  }

  closeErrorMessage() {
    this.error = false;
    this.errorMessageHeading = "";
    this.errorMessage = "";
  }
  

}

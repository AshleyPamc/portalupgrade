import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-hypertension-form',
  templateUrl: './hypertension-form.component.html',
  styleUrls: ['./hypertension-form.component.css']
})
export class HypertensionFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-risk-factors-form',
  templateUrl: './risk-factors-form.component.html',
  styleUrls: ['./risk-factors-form.component.css']
})
export class RiskFactorsFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

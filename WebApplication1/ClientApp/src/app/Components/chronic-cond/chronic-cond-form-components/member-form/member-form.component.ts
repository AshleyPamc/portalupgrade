import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.css']
})
export class MemberFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
    this._appService.ChronicConditionService.GetHpContractsForSchemeDropdown();
  }

}

import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-asthma-form',
  templateUrl: './asthma-form.component.html',
  styleUrls: ['./asthma-form.component.css']
})
export class AsthmaFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

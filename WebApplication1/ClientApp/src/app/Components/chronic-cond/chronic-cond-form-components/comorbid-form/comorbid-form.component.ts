import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-comorbid-form',
  templateUrl: './comorbid-form.component.html',
  styleUrls: ['./comorbid-form.component.css']
})
export class ComorbidFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

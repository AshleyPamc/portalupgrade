import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-provider-form',
  templateUrl: './provider-form.component.html',
  styleUrls: ['./provider-form.component.css']
})
export class ProviderFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
    this._appService.ChronicConditionService.SetProviderFormInfo();
  }

}

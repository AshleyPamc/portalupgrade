import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-requested-medication-form',
  templateUrl: './requested-medication-form.component.html',
  styleUrls: ['./requested-medication-form.component.css']
})
export class RequestedMedicationFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

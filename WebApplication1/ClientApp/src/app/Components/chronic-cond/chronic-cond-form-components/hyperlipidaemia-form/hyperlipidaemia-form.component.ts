import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-hyperlipidaemia-form',
  templateUrl: './hyperlipidaemia-form.component.html',
  styleUrls: ['./hyperlipidaemia-form.component.css']
})
export class HyperlipidaemiaFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

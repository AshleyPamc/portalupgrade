import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-diabetes-form',
  templateUrl: './diabetes-form.component.html',
  styleUrls: ['./diabetes-form.component.css']
})
export class DiabetesFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

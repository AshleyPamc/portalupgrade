import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-chronic-cond-detail-view',
  templateUrl: './chronic-cond-detail-view.component.html',
  styleUrls: ['./chronic-cond-detail-view.component.css']
})
export class ChronicCondDetailViewComponent implements OnInit {

  constructor(public _appService: AppService) { }

  ngOnInit() {
  }

}

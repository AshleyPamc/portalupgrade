import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chronic-cond-app',
  templateUrl: './chronic-cond-app.component.html',
  styleUrls: ['./chronic-cond-app.component.css']
})
export class ChronicCondAppComponent implements OnInit {

  public _proceedPrompt: boolean = false;   

  constructor(public _appService: AppService, private router: Router) { }

  ngOnInit() {

    this._appService.ChronicConditionService.loadChronicConditionSetup();

    // Check if the user is coming to application from an update
    if (this._appService.ChronicConditionService.CurrentPage == 'ChronicCondUpdate' &&
      this._appService.ChronicConditionService.ConditionForm.get('conditionCode').value != null &&
      this._appService.ChronicConditionService.ConditionForm.get('conditionCode').value.trim() != '')
    {

      this._proceedPrompt = true;

    } else {

      this._appService.ChronicConditionService.CurrentPage = 'ChronicCondApp';

    }

  }

  proceedToApplicationFromUpdate() {

    this._appService.ChronicConditionService.CurrentPage = 'ChronicCondApp';
    this._appService.ChronicConditionService.doResets();
    this._proceedPrompt = false;
  }

  returnToUpdate() {

    this._appService.ChronicConditionService.CurrentPage = 'ChronicCondUpdate';
    this.router.navigate(['/chronic-cond-update']);
    this._proceedPrompt = false;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { ProvdashComponent } from './provdash/provdash.component';
import { SharedModule } from './../../../app/shared.module';
//import { DownloadsComponent } from './../shared/downloads/downloads.component';

const routes: Routes = [{

  path: '',
  component: ProvdashComponent ,
  children: [
    { pathMatch: 'full', path: 'provider-dash', component: ProvdashComponent },
  ]
}];

@NgModule({
  declarations: [ProvdashComponent/*, DownloadsComponent*/],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class ProviderModule { }

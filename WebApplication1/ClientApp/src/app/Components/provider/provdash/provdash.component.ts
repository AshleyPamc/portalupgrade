import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { JoyrideService } from 'ngx-joyride';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-provdash',
  templateUrl: './provdash.component.html',
  styleUrls: ['./provdash.component.css']
})
export class ProvdashComponent implements OnInit {

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) { }


  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { SharedModule } from './../../../../app/shared.module';
import { ProviderFileUploadComponent } from './provider-file-upload.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { JoyrideModule } from 'ngx-joyride'


const routes: Routes = [{

  path: '',
  component: ProviderFileUploadComponent,
  children: [
    { pathMatch: 'full', path: 'UploadAuths', component: ProviderFileUploadComponent }
  ]

}];

@NgModule({
  declarations: [ProviderFileUploadComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    JoyrideModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule,
    NgxDropzoneModule
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class ProviderFileUploadModule { }

import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { AppService } from './../../../services/app.service';
import * as Tiff from 'tiff.js';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthFiles } from './../../../models/auth-files';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';




@Component({
  selector: 'app-provider-file-upload',
  templateUrl: './provider-file-upload.component.html',
  styleUrls: ['./provider-file-upload.component.css']
})
export class ProviderFileUploadComponent implements OnInit {

  @Input() memberForm: FormGroup;
  @Input() textForm: FormGroup;
  public _view: boolean = false;
  public openModelImgUrl: string;
  public progress: number;
  public message: string;
  public formData = new FormData();
  public files: File[] = [];
  public imageHeight: number = 0;
  public imageWidth: number = 0;
  public imageFile: boolean = false;
  public pdfUri: string = "";
  public pdfFile: boolean = false;
  public _unSupportedFormat: boolean = false;
  public _fileSize: string = "0.00";
  public outOfNetwork: boolean = false;
  public outOfNetworkMsg: string = "";

  @ViewChild('canvas', {static:false}) canvas: ElementRef;

  public url: string = '';

  public openSurvey: boolean = false;
  public urlI: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(private _appService: AppService, private fb: FormBuilder, private fbt: FormBuilder, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    //this.urlI = this._appService.SettingService.surveyLink;
    //this.urlI = this.urlI + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.urlI));

    this.memberForm = this.fb.group({
      options: ['', Validators.required],
      memb: ['', Validators.required]

    });
    this.textForm = this.fbt.group({
      addText: []
    });
  }

  getSafeUrl(): SafeResourceUrl {
    this.urlI = this._appService.SettingService.surveyLink;
    this.urlI = this.urlI + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.urlI));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }

  upload(event) {

    this.files.length

    event.addedFiles.forEach((el) => {
      if (el.type.includes('image')) {
        if (el.type == 'image/tiff') {
          let filereader = new FileReader();
          let canvasEl = document.getElementById("canvas") as HTMLCanvasElement;
          let cx = canvasEl.getContext('2d')!;
          let url: string;
          filereader.onload = (e) => {
            let tiff = new Tiff({ buffer: filereader.result });
            let ff = tiff.countDirectory();
            if (ff > 1) {
              for (var i = 0; i < ff; i++) {
                tiff.setDirectory(i);
                canvasEl = tiff.toCanvas();
                url = canvasEl.toDataURL('image/jpeg', 0.7);
                const imagefile = this.GetBlob(url);
                const newFile = new File([imagefile], i.toString() + "_" + el.name.replace('.TIF', '.jpeg').replace('.tiff', '.jpeg').replace('.tif', '.jpeg'), { type: 'image/jpeg' });
                this.files.push(newFile);
                let jpfile = this.files[this.files.length - 1];
                let reader = new FileReader();
                reader.onload = (e) => {
                  let c = document.getElementById(jpfile.name) as HTMLImageElement;
                  c.src = reader.result as string;
                }
                reader.readAsDataURL(jpfile)
              }
            } else {
              canvasEl = tiff.toCanvas();
              url = canvasEl.toDataURL('image/jpeg', 0.7);
              const imagefile = this.GetBlob(url);
              const newFile = new File([imagefile], el.name.replace('.TIF', '.jpeg').replace('.tiff', '.jpeg').replace('.tif', '.jpeg').replace('.TIFF', '.jpeg'), { type: 'image/jpeg' });
              this.files.push(newFile);

              let jpfile = this.files[this.files.length - 1];
              let reader = new FileReader();
              reader.onload = (e) => {
                let c = document.getElementById(jpfile.name) as HTMLImageElement;
                c.src = reader.result as string;
              }
              reader.readAsDataURL(jpfile)
            }


          }
          filereader.readAsArrayBuffer(el);

        } else {
          this.files.push(el);
          let canvasEl = document.getElementById("canvas") as HTMLCanvasElement;
          let cx = canvasEl.getContext('2d')!;
          let image = new Image();
          canvasEl.width = 100;
          canvasEl.height = 110;

          let reader = new FileReader();
          image.onload = () => {
            cx.drawImage(image, 0, 0, 100, 110);
          }
          reader.readAsDataURL(el);

          reader.onload = (event) => {
            let c = document.getElementById(el.name) as HTMLImageElement;
            c.src = reader.result as string;
            image.src = reader.result as string;
            //console.log(reader.result as string);
          }
        }

      }
      if (el.type.includes('application')) {
        this.files.push(el);
        let filereader = new FileReader();
        let url: string;
        filereader.onload = (e) => {
          url = filereader.result as string;
          let c = document.getElementById(el.name) as HTMLObjectElement;
          c.data = url;
        }

        filereader.readAsDataURL(el);


      }
      if (el.type.includes('text')) {
        this.files.push(el);
        let filereader = new FileReader();
        let url: string;
        filereader.onload = (e) => {
          url = filereader.result as string;
          let c = document.getElementById(el.name) as HTMLObjectElement;
          c.data = url;
        }

        filereader.readAsDataURL(el);


      }


    });

    setTimeout(() => {
      this.GetTotalSize();
    }, 1000)

  }

  GetTotalSize() {
    let p = this;
    p._fileSize = "0.00";
    p.files.forEach((el) => {
      p._fileSize = (parseFloat(p._fileSize) + (el.size / Math.pow(1024, 2))).toFixed(2);
    });
    //  console.log(this._fileSize);
  }

  GetBlob(url: string) {
    let uri = url.replace(/^data:image\/(jpeg|jpg);base64,/, "");

    const byteString = window.atob(uri);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  Remove(name: string, type: string) {
    let count: number = 0;
    let index: number = 0;
    let found: boolean = false;
    this.files.forEach((el) => {
      if ((el.name == name) && (el.type == type)) {
        index = count;
        found = true;
      }
      count++;
    });

    if (found) {
      this.files.splice(index, 1);
    }

    this.GetTotalSize();

  }

  Submit() {
    let data: AuthFiles = new AuthFiles();
    data.membid = this.memberForm.get('memb').value;
    data.hpcode = this.memberForm.get('options').value;
    data.provid = this._appService.LoginService.CurrentLoggedInUser.provID;
    data.username = this._appService.LoginService.CurrentLoggedInUser.username;
    data.AddText = this.textForm.get('addText').value;
    this._appService.UploadFileService.ValidateUser(data);
    this._appService.UploadFileService.files = new FormData;
    this.files.forEach((el) => {
      this._appService.UploadFileService.files.append(el.name, el);
    });
    let total: number = 0;
    this.files.forEach((el) => {
      total = total + el.size;

    });
    this._appService.UploadFileService._totalSize = (total / Math.pow(1024, 2)).toFixed(2);
  }

  SizeCalc(size: number): string {
    return (size / Math.pow(1024, 2)).toFixed(2);
  }

  ViewImage(name: File) {

    if ((name.type == 'application/pdf') || (name.type == 'image/tiff') || (name.type == 'text/plain') || (name.type == 'image/jpeg')
      || (name.type == 'image/jpg') || (name.type == 'image/png')) {

      this._view = true;
      if (name.type.includes('image')) {
        this.imageFile = true;
        this.pdfFile = false;
        this.files.forEach((el) => {
          if (el.name == name.name) {
            if ((name.name.includes('.tiff')) || (name.name.includes('.TIF'))) {
              let filereader = new FileReader();
              let canvasEl = document.getElementById("canvas") as HTMLCanvasElement;
              let cx = canvasEl.getContext('2d')!;
              filereader.onload = (e) => {
                // var Tiff = require('tiff.js');
                var img = new Image();
                img.onload = () => {
                  this.imageWidth = img.width / 2;
                  this.imageHeight = img.height / 2;
                };
                let tiff = new Tiff({ buffer: filereader.result });
                // console.log(tiff);
                canvasEl = tiff.toCanvas();
                img.src = canvasEl.toDataURL('image/jpeg', 0.7);
                this.openModelImgUrl = canvasEl.toDataURL('image/jpeg', 0.7);
              }
              filereader.readAsArrayBuffer(el);
            } else {
              let reader = new FileReader();

              reader.onload = (event) => {

                var img = new Image();
                img.onload = () => {
                  this.imageWidth = img.width / 2;
                  this.imageHeight = img.height / 2;
                };
                img.src = reader.result as string;
                this.openModelImgUrl = reader.result as string;
              }
              reader.readAsDataURL(el);
            }
          }
        });
      }
      if (name.type.includes('application')) {
        this.imageFile = false;
        this.pdfFile = true;
        let filereader = new FileReader();
        filereader.onload = () => {
          setTimeout((e) => {
            let r = document.getElementById("popup") as HTMLObjectElement;
            r.type = name.type;
            this.pdfUri = filereader.result as string;
            r.data = this.pdfUri;
            // console.log(this.pdfUri);
          }, 1000);

        }
        filereader.readAsDataURL(name);

      }
      if (name.type.includes('text')) {
        this.imageFile = false;
        this.pdfFile = true;
        let filereader = new FileReader();
        filereader.onload = () => {
          setTimeout((e) => {
            let r = document.getElementById("popup") as HTMLObjectElement;
            r.type = name.type;
            this.pdfUri = filereader.result as string;
            r.data = this.pdfUri;
            // console.log(this.pdfUri);
          }, 1000);

        }
        filereader.readAsDataURL(name);
      }

    } else {

      this._unSupportedFormat = true;

    }
  }

  ClearAll() {
    this.files = [];
    this.memberForm.reset();
    this.pdfFile = false;
    this.imageFile = false;
    this._unSupportedFormat = false;
    this.openModelImgUrl = "";
    this._appService.UploadFileService._success = false;
    this.textForm.reset();
    this._fileSize = "0.00";
  }

  onClick() {

    if (this.files.length > 0) { // Some controls are not displayed when there are no files uploaded - we don't want to display joyride for controls that aren't visible.

      this.joyrideService.startTour({
        steps: ['upload_3', 'upload_4', 'upload_5', 'upload_6', 'upload_2', 'upload_7', 'upload_8', 'upload_9'],
        themeColor: '#313131'
      })

    }
    else {

      this.joyrideService.startTour({
        steps: ['upload_3', 'upload_4', 'upload_5', 'upload_6', 'upload_8', 'upload_9'],
        themeColor: '#313131'
      })

    }
  }

  CheckOutOfNetBenefits() {

    let optAllowed: string[] = [];
    let optNotAllowed: string[] = [];
    let hpcode: string = this.memberForm.get('options').value;

    if (this._appService.LoginService.CurrentLoggedInUser.contract == 'NON-CONTRACTED' && this._appService.LoginService.isDentist) {

      this._appService.SettingService.Settings.forEach((el) => {


        if (el.settingsId == 'ProvAuthOutOfNetBen') {
          optAllowed = el.description.split(',');
        }
        if (el.settingsId == 'ProvAuthNonOutOfNetBen') {
          optNotAllowed = el.description.split(',');
        }
      });

      let found: boolean = false;

      optAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this.outOfNetworkMsg = "Provider not registered on the network. Please make sure the member do have “out of network” benefits.";
        }
      });
      optNotAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this.outOfNetworkMsg = "Out of Network provider member do not have benefits ";
        }
      });
      this.outOfNetwork = found;
    }
  }

}

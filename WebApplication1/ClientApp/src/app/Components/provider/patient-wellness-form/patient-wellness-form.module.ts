import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { PatientWellnessFormComponent } from './patient-wellness-form.component';

const routes: Routes = [{

  path: '',
  component: PatientWellnessFormComponent,
  children: [
    { pathMatch: 'full', path: 'patient-wellness-form', component: PatientWellnessFormComponent }
  ]

}];

@NgModule({
  declarations: [PatientWellnessFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class PatientWellnessFormModule { }

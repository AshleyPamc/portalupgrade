import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, ValidationErrors } from '@angular/forms';
import { AppService } from '../../../services/app.service';
import { PatientWellnessFormModel } from '../../../models/PatientWellnessFormModel';

@Component({
  selector: 'app-patient-wellness-form',
  templateUrl: './patient-wellness-form.component.html',
  styleUrls: ['./patient-wellness-form.component.css']
})
export class PatientWellnessFormComponent implements OnInit {

  constructor(public _appService: AppService) { }

  private arDepCode = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09'];
  private arMedAidYesNo = [["medAidYes", "Yes"], ["medAidNo", "No"]];
  private arAssessConsentYesNo = [["assessConsentYes", "Yes"], ["assessConsentNo", "No"]];
  private arHivConsentYesNo = [["hivConsentYes", "Yes"], ["hivConsentNo", "No"]];
  private arGender = ['Male', 'Female'];
  private arSexPastSixMonths = ['Never', 'A few times or less', 'A few times each month', 'Once or more each week'];
  private arCondomPastSixMonths = ['I have not had sex in the past 6 months', 'All the time', 'Most of the time', 'Some of the time', 'None of the time'];
  private arGetAidsWorry = ['Not at all', 'Slightly', 'Moderately', 'Considerably', 'Extremely'];
  private arExposedAidsWorry = ['Not at all', 'Slightly', 'Moderately', 'Considerably', 'Extremely'];
  private arToldAids = ['No', 'Yes', 'I never got the results'];
  private months = [[1, "January"], [2, "February"], [3, "March"], [4, "April"], [5, "May"], [6, "June"], [7, "July"], [8, "August"], [9, "September"], [10, "October"], [11, "November"], [12, "December"]];
  private years = [];

  patientWellnessForm: FormGroup;

  private busy: boolean;
  private error: boolean;
  private errorMessageHeading: string;
  private errorMessage: string;
  private success: boolean;
  private successMessageHeading: string;
  private successMessage: string;
  private successMessageBoldPart: string;

  ngOnInit() {
    this.patientWellnessForm = new FormGroup({
      'idNo': new FormControl(null, Validators.required),
      'name': new FormControl(null, Validators.required),
      'surname': new FormControl(null, Validators.required),
      'tel': new FormControl(null),
      'cell': new FormControl(null),
      'gender': new FormControl(null),
      'address': new FormControl(null),
      'email': new FormControl(null, Validators.email),
      'medicalAidYN': new FormControl(null),
      'medicalAidPlan': new FormControl(null),
      'medicalAidNo': new FormControl(null),
      'medicalAidDepCode': new FormControl(null),
      'screeningDate': new FormControl((new Date()).toISOString().substring(0, 10)),
      'assessmentConsent': new FormControl(null),
      'chronicConditions': new FormArray([new FormControl(null)]),
      'medications': new FormArray([new FormControl(null)]),
      'height': new FormControl(null),
      'weight': new FormControl(null),
      'bmi': new FormControl(null),
      'waist': new FormControl(null),
      'bloodPressureSystolic': new FormControl(null),
      'bloodPressureDiastolic': new FormControl(null),
      'cholesterolHDL': new FormControl(null),
      'cholesterolTRI': new FormControl(null),
      'cholesterolLDL': new FormControl(null),
      'cholesterolTotal': new FormControl(null),
      'bloodSugarMmols': new FormControl(null),
      'hivConsent': new FormControl(null),
      'sexPastSixMonths': new FormControl(null),
      'condomPastSixMonths': new FormControl(null),
      'getAidsWorry': new FormControl(null),
      'exposedAidsWorry': new FormControl(null),
      'monthLastAidsTest': new FormControl(null),
      'yearLastAidsTest': new FormControl(null),
      'toldAids': new FormControl(null),
    });

    this.getYears();
  }

  getYears() {
    let curryear: number = new Date().getFullYear() + 1;
    while (curryear > 1980) {
      curryear = curryear - 1;
      this.years.push(curryear);
    }
  }

  checkIfExist() {

    if (this.patientWellnessForm.get('idNo').value.trim() != "") {

      let formData: PatientWellnessFormModel = new PatientWellnessFormModel();
      formData.idNo = this.patientWellnessForm.get('idNo').value.trim();


      // Api Call
      this.busy = true;
      this._appService.PatientWellnessService.CheckIfPatientExist(formData)
        .subscribe(
          (result: PatientWellnessFormModel) => {

            if (result.success) {
              if (result.message == "found") {
                this.success = true;
                this.successMessageHeading = "Existing Patient Found";
                this.successMessage = "A patient with this ID number already exist on the system, any changes made will be saved.";
                this.populateFormWithExistingPatient(result);
              }
            }
            else {
              this.error = true;
              this.errorMessageHeading = "An error occured while doing an ID no lookup";
              this.errorMessage = result.message;
            }
          },
          (error) => {
            console.log(error)
            this.error = true;
            this.errorMessageHeading = "An error occured while doing an ID no lookup";
            this.errorMessage = "Unexpected error occured";
            this.busy = false;
          },
          () => {
            this.busy = false;
          }
        )
    }
  }

  populateFormWithExistingPatient(patientInfo: PatientWellnessFormModel) {
    this.patientWellnessForm.controls['name'].setValue(patientInfo.name);
    this.patientWellnessForm.controls['surname'].setValue(patientInfo.surname);
    this.patientWellnessForm.controls['tel'].setValue(patientInfo.tel);
    this.patientWellnessForm.controls['cell'].setValue(patientInfo.cell);
    this.patientWellnessForm.controls['gender'].setValue(patientInfo.gender);
    this.patientWellnessForm.controls['address'].setValue(patientInfo.address);
    this.patientWellnessForm.controls['email'].setValue(patientInfo.email);
    this.patientWellnessForm.controls['medicalAidYN'].setValue(patientInfo.medicalAidYN);
    //if (patientInfo.medicalAidYN == true) {
    //  this.patientWellnessForm.controls['medicalAidYN'].setValue('Yes');
    //} else if (patientInfo.medicalAidYN == false) {
    //  this.patientWellnessForm.controls['medicalAidYN'].setValue('No');
    //}
    this.patientWellnessForm.controls['medicalAidPlan'].setValue(patientInfo.medicalAidPlan);
    this.patientWellnessForm.controls['medicalAidNo'].setValue(patientInfo.medicalAidNo);
    this.patientWellnessForm.controls['medicalAidDepCode'].setValue(patientInfo.medicalAidDepCode);
    this.patientWellnessForm.controls['screeningDate'].setValue(patientInfo.screeningDate);
    this.patientWellnessForm.controls['assessmentConsent'].setValue(patientInfo.assessmentConsent);
    //if (patientInfo.assessmentConsent == true) {
    //  this.patientWellnessForm.controls['assessmentConsent'].setValue('Yes');
    //} else if (patientInfo.assessmentConsent == false) {
    //  this.patientWellnessForm.controls['assessmentConsent'].setValue('No');
    //}

    if (patientInfo.chronicConditions != null) {
      let conditions: string[] = patientInfo.chronicConditions.split('|');
      (<FormArray>this.patientWellnessForm.get('chronicConditions')).clear();
      for (let condition of conditions) {
        let control = new FormControl(null);
        control.setValue(condition);
        (<FormArray>this.patientWellnessForm.get('chronicConditions')).push(control);
      }
    }

    if (patientInfo.medications != null) {
      let medications: string[] = patientInfo.medications.split('|');
      (<FormArray>this.patientWellnessForm.get('medications')).clear();
      for (let medication of medications) {
        let control = new FormControl(null);
        control.setValue(medication);
        (<FormArray>this.patientWellnessForm.get('medications')).push(control);
      }
    }

    this.patientWellnessForm.controls['height'].setValue(patientInfo.height);
    this.patientWellnessForm.controls['weight'].setValue(patientInfo.weight);
    this.patientWellnessForm.controls['bmi'].setValue(patientInfo.bmi);
    this.patientWellnessForm.controls['waist'].setValue(patientInfo.waist);
    this.patientWellnessForm.controls['bloodPressureSystolic'].setValue(patientInfo.bloodPressureSystolic);
    this.patientWellnessForm.controls['bloodPressureDiastolic'].setValue(patientInfo.bloodPressureDiastolic);
    this.patientWellnessForm.controls['cholesterolHDL'].setValue(patientInfo.cholesterolHDL);
    this.patientWellnessForm.controls['cholesterolTRI'].setValue(patientInfo.cholesterolTRI);
    this.patientWellnessForm.controls['cholesterolLDL'].setValue(patientInfo.cholesterolLDL);
    this.patientWellnessForm.controls['cholesterolTotal'].setValue(patientInfo.cholesterolTotal);
    this.patientWellnessForm.controls['bloodSugarMmols'].setValue(patientInfo.bloodSugarMmols);
    this.patientWellnessForm.controls['hivConsent'].setValue(patientInfo.hivConsent);
    //if (patientInfo.hivConsent == 'Yes') {
    //  this.patientWellnessForm.controls['hivConsent'].setValue('Yes');
    //} else if (patientInfo.hivConsent == false) {
    //  this.patientWellnessForm.controls['hivConsent'].setValue('No');
    //}
    this.patientWellnessForm.controls['sexPastSixMonths'].setValue(patientInfo.sexPastSixMonths);
    this.patientWellnessForm.controls['condomPastSixMonths'].setValue(patientInfo.condomPastSixMonths);
    this.patientWellnessForm.controls['getAidsWorry'].setValue(patientInfo.getAidsWorry);
    this.patientWellnessForm.controls['exposedAidsWorry'].setValue(patientInfo.exposedAidsWorry);
    this.patientWellnessForm.controls['monthLastAidsTest'].setValue(patientInfo.monthLastAidsTest);
    this.patientWellnessForm.controls['yearLastAidsTest'].setValue(patientInfo.yearLastAidsTest);
    this.patientWellnessForm.controls['toldAids'].setValue(patientInfo.toldAids);

  }

  onSubmit() {
    console.log(this.patientWellnessForm);

    let formData: PatientWellnessFormModel = new PatientWellnessFormModel();

    formData.idNo = this.patientWellnessForm.get('idNo').value;
    if (formData.idNo != null) { formData.idNo = formData.idNo.trim(); }

    formData.name = this.patientWellnessForm.get('name').value;
    if (formData.name != null) { formData.name = formData.name.trim(); }

    formData.surname = this.patientWellnessForm.get('surname').value;
    if (formData.surname != null) { formData.surname = formData.surname.trim(); }

    formData.tel = this.patientWellnessForm.get('tel').value;
    if (formData.tel != null) { formData.tel = formData.tel.trim(); }

    formData.cell = this.patientWellnessForm.get('cell').value;
    if (formData.cell != null) { formData.cell = formData.cell.trim(); }

    formData.gender = this.patientWellnessForm.get('gender').value;
    if (formData.gender == 'Male') {
      formData.gender = 'M';
    }
    if (formData.gender == 'Female') {
      formData.gender = 'F';
    }

    formData.address = this.patientWellnessForm.get('address').value;
    if (formData.address != null) { formData.address = formData.address.trim(); }

    formData.email = this.patientWellnessForm.get('email').value;
    if (formData.email != null) { formData.email = formData.email.trim(); }

    formData.medicalAidYN = this.patientWellnessForm.get('medicalAidYN').value;
    //if (this.patientWellnessForm.get('medicalAidYN').value == 'Yes') {
    //  formData.medicalAidYN = true;
    //} else if (this.patientWellnessForm.get('medicalAidYN').value == 'No') {
    //  formData.medicalAidYN = false;
    //}

    formData.medicalAidPlan = this.patientWellnessForm.get('medicalAidPlan').value;
    if (formData.medicalAidPlan != null) { formData.medicalAidPlan = formData.medicalAidPlan.trim(); }

    formData.medicalAidNo = this.patientWellnessForm.get('medicalAidNo').value;
    if (formData.medicalAidNo != null) { formData.medicalAidNo = formData.medicalAidNo.trim(); }

    formData.medicalAidDepCode = this.patientWellnessForm.get('medicalAidDepCode').value;
    if (formData.medicalAidDepCode != null) { formData.medicalAidDepCode = formData.medicalAidDepCode.trim(); }

    formData.screeningDate = this.patientWellnessForm.get('screeningDate').value;
    //if (formData.screeningDate != null) { formData.screeningDate = formData.screeningDate.trim(); }

    formData.assessmentConsent = this.patientWellnessForm.get('assessmentConsent').value;
    //if (this.patientWellnessForm.get('assessmentConsent').value == 'Yes') {
    //  formData.assessmentConsent = true;
    //} else if (this.patientWellnessForm.get('assessmentConsent').value == 'No') {
    //  formData.assessmentConsent = false;
    //}

    // Handle conditions array
    formData.chronicConditions = '';
    for (let condition of this.patientWellnessForm.get('chronicConditions').value) {
      if (condition != null && condition.trim() != '') {
        formData.chronicConditions += condition.trim() + '|';
      }
    }
    if (formData.chronicConditions.trim().length > 0) {
      formData.chronicConditions = formData.chronicConditions.substring(0, formData.chronicConditions.length - 1); // remove last delimter from delimited string
    } else {
      formData.chronicConditions = null;
    }

    //Handle medications array
    formData.medications = '';
    for (let medication of this.patientWellnessForm.get('medications').value) {
      if (medication != null && medication.trim() != '') {
        formData.medications += medication.trim() + '|';
      }
    }
    if (formData.medications.trim().length > 0) {
      formData.medications = formData.medications.substring(0, formData.medications.length - 1); // remove last delimter from delimited string
    } else {
      formData.medications = null;
    }

    formData.height = this.patientWellnessForm.get('height').value;
    if (formData.height != null) { formData.height = formData.height.trim(); }

    formData.weight = this.patientWellnessForm.get('weight').value;
    if (formData.weight != null) { formData.weight = formData.weight.trim(); }

    formData.bmi = this.patientWellnessForm.get('bmi').value;
    if (formData.bmi != null) { formData.bmi = formData.bmi.trim(); }

    formData.waist = this.patientWellnessForm.get('waist').value;
    if (formData.waist != null) { formData.waist = formData.waist.trim(); }

    formData.bloodPressureSystolic = this.patientWellnessForm.get('bloodPressureSystolic').value;
    if (formData.bloodPressureSystolic != null) { formData.bloodPressureSystolic = formData.bloodPressureSystolic.trim(); }

    formData.bloodPressureDiastolic = this.patientWellnessForm.get('bloodPressureDiastolic').value;
    if (formData.bloodPressureDiastolic != null) { formData.bloodPressureDiastolic = formData.bloodPressureDiastolic.trim(); }

    formData.cholesterolHDL = this.patientWellnessForm.get('cholesterolHDL').value;
    if (formData.cholesterolHDL != null) { formData.cholesterolHDL = formData.cholesterolHDL.trim(); }

    formData.cholesterolTRI = this.patientWellnessForm.get('cholesterolTRI').value;
    if (formData.cholesterolTRI != null) { formData.cholesterolTRI = formData.cholesterolTRI.trim(); }

    formData.cholesterolLDL = this.patientWellnessForm.get('cholesterolLDL').value;
    if (formData.cholesterolLDL != null) { formData.cholesterolLDL = formData.cholesterolLDL.trim(); }

    formData.cholesterolTotal = this.patientWellnessForm.get('cholesterolTotal').value;
    if (formData.cholesterolTotal != null) { formData.cholesterolTotal = formData.cholesterolTotal.trim(); }

    formData.bloodSugarMmols = this.patientWellnessForm.get('bloodSugarMmols').value;
    if (formData.bloodSugarMmols != null) { formData.bloodSugarMmols = formData.bloodSugarMmols.trim(); }

    formData.hivConsent = this.patientWellnessForm.get('hivConsent').value;
    //if (this.patientWellnessForm.get('hivConsent').value == 'Yes') {
    //  formData.hivConsent = true;
    //} else if (this.patientWellnessForm.get('hivConsent').value == 'No') {
    //  formData.hivConsent = false;
    //}

    formData.sexPastSixMonths = this.patientWellnessForm.get('sexPastSixMonths').value;
    formData.condomPastSixMonths = this.patientWellnessForm.get('condomPastSixMonths').value;
    formData.getAidsWorry = this.patientWellnessForm.get('getAidsWorry').value;
    formData.exposedAidsWorry = this.patientWellnessForm.get('exposedAidsWorry').value;
    formData.monthLastAidsTest = this.patientWellnessForm.get('monthLastAidsTest').value;
    formData.yearLastAidsTest = this.patientWellnessForm.get('yearLastAidsTest').value;
    formData.toldAids = this.patientWellnessForm.get('toldAids').value;
    formData.user = this._appService.LoginService.CurrentLoggedInUser.username;

    // Api Call
    this.busy = true;
    this._appService.PatientWellnessService.SubmitPatientWellnessForm(formData)
      .subscribe(
        (result: PatientWellnessFormModel) => {
          if (result.success) {

            this.success = true;
            this.successMessageHeading = "Submission Success";
            this.successMessage = "Successfully saved data for patient";
            this.successMessageBoldPart = result.name + " " + result.surname;
            this.patientWellnessForm.reset();
          }
          else {
            /*this._busy = false;*/
            this.error = true;
            this.errorMessageHeading = "Submission Failed";
            this.errorMessage = result.message;
          }
        },
        (error) => {
          console.log(error)
          this.error = true;
          this.errorMessageHeading = "Submission Failed";
          this.errorMessage = "Unexpected error occured";
          this.busy = false;
        },
        () => {
          this.busy = false;
        }
      )
  }

  onAddChronicCondition() {
    const control = new FormControl(null);
    (<FormArray>this.patientWellnessForm.get('chronicConditions')).push(control);
  }

  onAddMedication() {
    const control = new FormControl(null);
    (<FormArray>this.patientWellnessForm.get('medications')).push(control);
  }

  calcBmi() {
    this.patientWellnessForm.get('bmi').reset();

    if (this.patientWellnessForm.get('height').value.trim() != "" && this.patientWellnessForm.get('weight').value.trim() != "") {

      let height: number = this.patientWellnessForm.get('height').value.trim();
      let weight: number = this.patientWellnessForm.get('weight').value.trim();

      let bmi: number = weight / ((height / 100.0) * (height / 100.0));
      bmi = Math.round(bmi * 10) / 10;

      this.patientWellnessForm.get('bmi').setValue(bmi.toString());
    }
  }

  closeErrorMessage() {
    this.error = false;
    this.errorMessageHeading = "";
    this.errorMessage = "";
  }

  closeSuccessMessage() {
    this.success = false;
    this.successMessageHeading = "";
    this.successMessage = "";
    this.successMessageBoldPart = "";
  }

}

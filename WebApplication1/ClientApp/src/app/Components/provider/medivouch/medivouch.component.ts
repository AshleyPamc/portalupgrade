import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { ClaimCodesModel } from '../../../models/ClaimCodesModel';
import { AppService } from '../../../services/app.service';
import { formatDate } from '@angular/common';
import { DefaultCodesModel } from '../../../models/DefaultCodesModel';
import { VoucherHistory } from '../../../models/history';
import { JoyrideService } from 'ngx-joyride';
import { Diagcodes } from '../../../models/diagcodes';
import { Recordtype5Model } from '../../../models/Recordtype5Model';
import { DependantsInfoModel } from '../../../models/DependantsInfoModel';

@Component({
  selector: 'app-medivouch',
  templateUrl: './medivouch.component.html',
  styleUrls: ['./medivouch.component.css']
})
export class MedivouchComponent implements OnInit {

  @Input() VoucherForm: FormGroup;
  @Input() BeneficiaryFrm: FormGroup;
  @Input() ToothFrm: FormGroup;
  @Input() DiagForm: FormGroup;

  private _errMsg: ValidationErrors;
  private _membError: string;
  private _numError: string;
  public procError: string;
  public diagError: string;
  public qtyError: string;
  public priceError: string;
  public openTansaction: boolean = false;
  public openEditCodeModal: boolean = false;
  public selectedEditCode: ClaimCodesModel = new ClaimCodesModel();
  public openOtpTansaction: boolean = false;
  public OtpError: string;
  public alertBalance: boolean = true;
  public popupBalance: boolean = false;
  public _date: string = formatDate(new Date, "yyyy/MM/dd", 'en-za', '+0200');
  public proceed: boolean = false;
  public showAddCodeForm: boolean = false;
  public openNewModel: boolean = false;
  public diagCorrect: boolean = false;
  public linesCorrect: boolean = false;
  public remaining: number = 0.00;
  public memberLiable: string;
  public providerLiable: string;
  public confirming: boolean = false;
  public total: string = ""
  public showInputEmail: boolean = false;
  public detailsOfBasket: DefaultCodesModel[] = [];
  public diagCodeError: boolean = false;
  public toothError: string = "";
  public toothNumberError: boolean = false;
  public busy: boolean = false;
  public showHistory: boolean = false;
  public enableRedeem: boolean = true;

  constructor(private readonly joyrideService: JoyrideService, private fb: FormBuilder, private fbBenForm: FormBuilder, public _appService: AppService, private fbTooth: FormBuilder, private fbDiag: FormBuilder) {

    this._errMsg = {
      'required': 'This field is required',
      'invalid': 'This field is incorrect',
      'length': 'Invalid voucher number'
    }

    // Get Spec code to determine HP Code for Internal Vouchers
    //if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
    //  this._appService.MedivouchService.Prefix = 'MV';
    //  this._appService.MedivouchService.HpCode = 'MDV';
    //  this._appService.MedivouchService.Option = 'Medivouch';
    //}
    //else
    // Set defaults

    //this._appService.MedivouchService.InsuranceVoucher = false;
    //this._appService.MedivouchService.HpCode = "";
    //this._appService.MedivouchService.Prefix = "";
    //this._appService.MedivouchService.Option = "";
     
    //if (this._appService.LoginService.CurrentLoggedInUser.userType == 6) {  //internal provider
    //  this._appService.MedivouchService.Prefix = 'DRC'
    //  this._appService.MedivouchService.HpCode = 'DRCV';
    //  this._appService.MedivouchService.Option = 'ORAL';
    //  this.VoucherForm.controls['hp'].setValue(this._appService.MedivouchService.HpCode);
    //} else {
    //  this._appService.MedivouchService.GetBasketHpCodes(this._appService.LoginService.CurrentLoggedInUser.specCode, this._appService.LoginService.CurrentLoggedInUser.provClass);
    //}
       
    this._appService.MedivouchService.GetDiagCodes();
    //this._appService.MedivouchService.success = true;        
  }

  ngOnInit() {
    this.VoucherForm = this.fb.group({
      'hp': ['', null],
      'membVoucher': ['', Validators.required]
    })
    this.BeneficiaryFrm = this.fbBenForm.group({
      'IDNo': ['', Validators.required],
      'FirstName': ['', Validators.required],
      'LastName': ['', Validators.required],
      'contact': ['', Validators.required],
      'dependant': ['', Validators.required]
    })

    // Set hpcode if it is not insurance voucher
    //if (this._appService.MedivouchService.HpCode != "") {
    //  this.VoucherForm.controls['hp'].setValue(this._appService.MedivouchService.HpCode);
    //}

    if (this._appService.LoginService.CurrentLoggedInUser.specialist) {
      this.ToothFrm = this.fbTooth.group({
        'tooth': []
      });


    } else {
      this.ToothFrm = this.fbTooth.group({
        'tooth': ['', Validators.required]
      });
    }

    this.DiagForm = this.fbDiag.group({
      'DiagCode': ['', Validators.required]
    })
       
  }

  /*Getters And Setters*/

  public get membError(): string {
    return this._membError;
  }
  public set membError(value: string) {
    this._membError = value;
  }
  public get numError(): string {
    return this._numError;
  }
  public set numError(value: string) {
    this._numError = value;
  }

  onClick() {
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1 || this._appService.LoginService.CurrentLoggedInUser.userType == 6) {
      this.joyrideService.startTour({
        steps: ['vouch_1', 'vouch_2', 'vouch_3', 'vouch_4', 'vouch_5', 'vouch_6'],
        themeColor: '#313131'
      })
    }
  }

  HpChange() {
    let selectedOpt: string = this.VoucherForm.get('hp').value;
    this._appService.MedivouchService.HpCode = this._appService.MedivouchService.VoucherHpList.find(x => x.opt == selectedOpt).hpCode;
    this._appService.MedivouchService.Option = this._appService.MedivouchService.VoucherHpList.find(x => x.opt == selectedOpt).opt;
    this._appService.MedivouchService.Prefix = this._appService.MedivouchService.VoucherHpList.find(x => x.opt == selectedOpt).prefix;
  }

  DependantChange() {
    let selectedDep: string = this.BeneficiaryFrm.get('dependant').value;
    this._appService.MedivouchService.benName = this._appService.MedivouchService.dependantList.find(x => x.dependant == selectedDep).firstname;
    this._appService.MedivouchService.benSurname = this._appService.MedivouchService.dependantList.find(x => x.dependant == selectedDep).lastname;
    this._appService.MedivouchService.benContact = this._appService.MedivouchService.dependantList.find(x => x.dependant == selectedDep).cell;
    this._appService.MedivouchService.Option = this._appService.MedivouchService.dependantList.find(x => x.dependant == selectedDep).option;
    this._appService.MedivouchService.benDep = selectedDep;
  }

  get ToothFrmValidator() {
    let c: boolean = false;
    this._appService.MedivouchService.groupDetail.forEach((el) => {
      if (el.enterTooth) {
        c = true;
      }
    })
    if (c) {
      let tooth = this.ToothFrm.controls['tooth']
      let temp = this.ToothFrm.controls['tooth'].value;
      this.ToothFrm.controls['tooth'].setValue('');
      const validator = this.ToothFrm.controls['tooth'].validator(tooth);
      //  console.log(validator);
      if (validator && validator.required) {
        this.ToothFrm.controls['tooth'].setValue(temp);
        return true;
      } else {
        this.ToothFrm.controls['tooth'].setValue(temp);
        return false;
      }
    } else {
      let temp = this.ToothFrm.controls['tooth'].value;
      this.ToothFrm.controls['tooth'].setValue('');
      let tooth = this.ToothFrm.controls['tooth']
      if (this.ToothFrm.controls['tooth'].validator != null) {
        const validator = this.ToothFrm.controls['tooth'].validator(tooth);
        //  console.log(validator);
        if (validator && validator.required) {
          this.ToothFrm.controls['tooth'].setValue(temp);
          return false;
        } else {
          this.ToothFrm.controls['tooth'].setValue(temp);
          return true;
        }
      } else {
        return false;
      }
    }
  }

  public get ToothFrmValid() {
    let c: boolean;
    if (this.ToothFrm.valid == true && this._appService.MedivouchService.toothCorrect == false) {
      return false;
    } else {
      return true;
    }
  }

  /*Clear all fields*/
  Clear() {
    this.VoucherForm.reset();
    this._membError = "";
    this._numError = "";

    //if (!this._appService.MedivouchService.InsuranceVoucher) {
    //  this.VoucherForm.controls['hp'].setValue(this._appService.MedivouchService.HpCode);
    //}
  }

  /*Process balance check*/
  VoucherRedemption() {

    this._appService.ClaimCaptureService.dataBaseMessageDiagC1 = "";
    this.DiagForm.reset();
    this.BeneficiaryFrm.reset();
    this.ToothFrm.reset();

    let minLength: number = 0;
    //if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
    if (this._appService.MedivouchService.HpCode == "MDV") {
      minLength = 16; // Medivouch voucher length
      //} else if (this._appService.LoginService.CurrentLoggedInUser.userType == 6) {
    } else {
      minLength = 5;  // Internal voucher length
    }

    let c: string = this.VoucherForm.get('membVoucher').value;

    if ((c == "") || (c == undefined) || (c == null)) {
      this.membError = this._errMsg['required'];
      this.VoucherForm.controls['membVoucher'].setErrors(this._errMsg['required']);
    } else if (c.length < minLength) {
      this.membError = this._errMsg['length'];
      this.VoucherForm.controls['membVoucher'].setErrors(this._errMsg['length']);
    } else {
      //this._appService.LoginService.CurrentLoggedInUser.provID
      this._appService.MedivouchService.memberDetails = c;
      this._appService.MedivouchService.cardnumber = c;
      this._appService.MedivouchService.voucherNo = c;
      this._appService.MedivouchService.VoucherValidation();
      this.busy = true;
      setTimeout(() => {
        this.busy = false;
      }, 3000)
    }
  }

  /*Get total claim amount*/
  GetTotal(): string {
    let count: number = 0;
    let code: number = 0;
    this.remaining = +this._appService.MedivouchService.voucherNo;
    this._appService.MedivouchService.codeList.forEach((el) => {
      code++;
      if ((el.total == '') || (el.total == null) || (el.total == undefined)) {
        el.total = '0.00';
      }


      if (code == 1) {


        ////console.log("code if " + this.remaining.toFixed(2));
        this.remaining = this.remaining - +el.total;
        if (el.total !== "0.00") {
          if (this.remaining == 0) {
            el.provpay = el.total;
          } else if (this.remaining < 0) {
            let c = 0 - this.remaining;
            el.provpay = (+el.total - c).toFixed(2);
            el.membpay = (c).toFixed(2);
          } else if (this.remaining > 0) {
            el.provpay = el.total;
            // el.provpay = "";
          }
        }
      } else {
        if (el.total !== "0.00") {
          this.remaining = this.remaining - +el.total;
          if (this.remaining > +el.total) {
            // this.remaining = this.remaining - +el.total;
            el.membpay = "0.00";
            el.provpay = el.total;
          } else if (this.remaining < +el.total && this.remaining > 0) {
            el.provpay = el.total;
            // let c = 0 - (this.remaining - +el.total);
            el.membpay = "0.00";
            //this.remaining = this.remaining - +el.total;
          } else if (this.remaining <= 0) {
            // el.membpay = el.total;
            let c = 0 - this.remaining;
            if (c >= +el.total) {
              el.membpay = el.total
            } else if (c < +el.total) {
              el.provpay = (+el.total - c).toFixed(2);
              el.membpay = (c).toFixed(2);
            }
            // this.remaining = this.remaining - +el.total;
          }
        }

        //  //console.log("code else " + this.remaining.toFixed(2));

      }




      count = count + (+el.total);
    });

    this.memberLiable = "0.00";
    this.providerLiable = "0.00";

    this._appService.MedivouchService.codeList.forEach((el) => {
      this.memberLiable = (+this.memberLiable + +el.membpay).toFixed(2);
      this.providerLiable = (+this.providerLiable + +el.provpay).toFixed(2);
    });
    //console.log(this._appService.MedivouchService.codeList)
    return count.toFixed(2);
  }

  ConfirmID() {

    // Reset Values
    this.BeneficiaryFrm.get('FirstName').reset();
    this.BeneficiaryFrm.get('LastName').reset();
    this.BeneficiaryFrm.get('contact').reset();
    this.BeneficiaryFrm.get('dependant').reset();
    this._appService.MedivouchService.benName = "";
    this._appService.MedivouchService.benSurname = "";
    this._appService.MedivouchService.benContact = "";
    this._appService.MedivouchService.benDep = "";
    this._appService.MedivouchService.allowSkip = false;
    this._appService.MedivouchService.isSchemeMember = false;

    if (this.BeneficiaryFrm.get('IDNo').value != "" && this.BeneficiaryFrm.get('IDNo').value != null) {

      let c: DefaultCodesModel = new DefaultCodesModel();
      c.hpCode = this._appService.MedivouchService.HpCode;
      c.depCode = "00";
      c.prefix = this._appService.MedivouchService.Prefix;
      c.membIDNo = this.BeneficiaryFrm.get('IDNo').value;
      if (this._appService.SettingService.GetSettingEnabled('MediVouchIDValidation')) {

        if (c.membIDNo.length < 13 || c.membIDNo.length > 13) {                          //validation for only SA id numbers
          this.BeneficiaryFrm.get('IDNo').setErrors(this._errMsg['invalid']);
        } else {
          this._appService.MedivouchService.ValidateIDExist(c, this.BeneficiaryFrm);
        }

      } else {
        this._appService.MedivouchService.ValidateIDExist(c, this.BeneficiaryFrm);
      }
    }
  }

  GetHeadDesc(): string {
    if (this._appService.MedivouchService.groupDetail.length > 0) {
      return this._appService.MedivouchService.groupDetail[0].headDesc;
    } else {
      return ' ';
    }
  }

  RedeemVoucher() {
    let di = this.DiagForm.controls['DiagCode'].value;

    if (di != '' && di != undefined && di != null) {

      this._appService.MedivouchService.benName = this.BeneficiaryFrm.get('FirstName').value;
      this._appService.MedivouchService.benSurname = this.BeneficiaryFrm.get('LastName').value;
      this._appService.MedivouchService.benContact = this.BeneficiaryFrm.get('contact').value;
      this._appService.MedivouchService.benIDNo = this.BeneficiaryFrm.get('IDNo').value;
      this._appService.MedivouchService.benDep = this.BeneficiaryFrm.get('dependant').value;
      this._appService.MedivouchService.diagCode = this.DiagForm.get('DiagCode').value;
      this._appService.MedivouchService.RedeemVoucher();
      this._appService.MedivouchService.voucherCodes = false;
      this._appService.MedivouchService.balanceFail = false;
      this._appService.MedivouchService.balanceSuccess = false;
      this._appService.MedivouchService.BenDetails = false;

      this.busy = true;

      setTimeout(() => {
        this.busy = false;

      }, 6000)

    } else {
      this.diagCodeError = true;
    }
  }

  CalculateTotal(): string {

    let c: string = "0";
    this._appService.MedivouchService.groupDetail.forEach((el) => {
      c = ((+c) + (+el.total)).toFixed(2);
    });

    return c;
  }

  AddToHistory() {
    if (this._appService.MedivouchService.balanceSuccess) {
      let c: VoucherHistory = new VoucherHistory();
      c.voucherNo = this._appService.MedivouchService.voucherNo.substr(0, this._appService.MedivouchService.voucherNo.indexOf('_'));
      c.voucherName = this._appService.MedivouchService.groupDetail[0].headDesc;
      c.voucherBasket = this._appService.MedivouchService.voucherNo.substr(this._appService.MedivouchService.voucherNo.indexOf('_') + 1,
        this._appService.MedivouchService.voucherNo.length - (this._appService.MedivouchService.voucherNo.indexOf('_') + 1))
      c.success = true;
      let date: Date = new Date;
      c.time = formatDate(date, "HH:mm:ss", 'en-za', '+0200')
      this._appService.MedivouchService.HistoryList.push(c);
    }
  }

  ToothBlur(code: string) {


    let c: string = this.ToothFrm.get('tooth').value;
    this._appService.MedivouchService.groupDetail.forEach((el) => {
      if (el.code == code) {
        let d: string[] = c.split(',');

        if (c == '' || c == undefined) {
          this.toothError = "Please enter a Toothnumber";
          this.toothNumberError = true;
          this.ToothFrm.get('tooth').setErrors(this._errMsg['invalid']);
        } else if (d.length > el.defaultQty) {
          this.ToothFrm.get('tooth').setErrors(this._errMsg['invalid']);
          this.toothError = "To many tooth numbers enter.";
          this.toothNumberError = true;
        } else if (d.length < el.defaultQty) {
          this.ToothFrm.get('tooth').setErrors(this._errMsg['invalid']);
          this.toothError = "Not enough tooth numbers entered.";
          this.toothNumberError = true;

        } else {
          let d: ClaimCodesModel = new ClaimCodesModel();
          d.toothNo = c;
          this._appService.MedivouchService.ValidateToothNo(d);
          el.toothNo = c;

        }

      }
    });
    this.enableRedeem = !this.ToothFrmValid;
    console.log(this.enableRedeem);
  }

  GetBasketDetails(id: string) {
    this._appService.MedivouchService.busy = true;
    this.detailsOfBasket = [];

    this._appService.MedivouchService.basketDetailLists.forEach((el) => {
      if (el.id == id) {
        this.detailsOfBasket.push(el);
      }
    });

    this._appService.MedivouchService.busy = false;

  }

  GetTotalView(data: string): string {
    let count: number = 0;
    let code: number = 0;
    this._appService.MedivouchService.basketDetailLists.forEach((el) => {
      if (el.id == data) {
        count = count + (+el.total);
      }
    })
    //console.log(this._appService.MedivouchService.codeList)
    return count.toFixed(2);
  }

  DiagBlur() {
    this._appService.MedivouchService.busy = true;
    let d: Diagcodes = new Diagcodes();
    d.code = this.DiagForm.get('DiagCode').value;
    this._appService.ClaimCaptureService.diagnosticCodes = new Recordtype5Model()
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = d.code;
    setTimeout(() => {
      this._appService.ClaimCaptureService.DataBaseDiagCode1Check();

    }, 1000)
    this._appService.MedivouchService.busy = false;
  }

  GetVoucherRedemtionHistory() {
       
    this._appService.MedivouchService.GetVoucherRedemtionHistory();
    this.showHistory = true;
  }

  GetVoucherCode(): string {
    let a: string = "";

    a = this._appService.MedivouchService.voucherNo.substr(0, (this._appService.MedivouchService.voucherNo.indexOf('_') - 1) + 1
    );

    return a;
  }

  CancelPatientDetails() {
    this._appService.MedivouchService.BenDetails = false;
    this._appService.MedivouchService.allowSkip = false;
    this._appService.MedivouchService.isSchemeMember = false;
    this.BeneficiaryFrm.reset();

    this._appService.MedivouchService.benName = "";
    this._appService.MedivouchService.benSurname = "";
    this._appService.MedivouchService.benContact = "";
    this._appService.MedivouchService.benDep = "";

  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { SharedModule } from './../../../../app/shared.module';
import { MedivouchComponent } from './medivouch.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { JoyrideModule } from 'ngx-joyride'

const routes: Routes = [{

  path: '',
  component: MedivouchComponent,
  children: [
    { pathMatch: 'full', path: 'Medivouch', component: MedivouchComponent }
  ]

}];

@NgModule({
  declarations: [MedivouchComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    JoyrideModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule,
    NgxDropzoneModule
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class MedivouchModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { JoyrideModule } from 'ngx-joyride';
import { SharedModule } from './../../../../app/shared.module';
import { BenefitLookupComponent } from '../benefit-lookup/benefit-lookup.component';

const routes: Routes = [{

  path: '',
  component: BenefitLookupComponent,
  children: [
    { pathMatch: 'full', path: 'benefits', component: BenefitLookupComponent },
  ]
}];

@NgModule({
  declarations: [BenefitLookupComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule,
    JoyrideModule.forRoot()
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class BenefitlookupModule { }

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { JoyrideService } from 'ngx-joyride';
import { BenHist } from '../../../models/ben-hist';
import { BenifitRights } from '../../../models/benifit-rights';
import { MemberEligibilityModel } from '../../../models/MemberEligibilityModel';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-benefit-lookup',
  templateUrl: './benefit-lookup.component.html',
  styleUrls: ['./benefit-lookup.component.css']
})
export class BenefitLookupComponent implements OnInit {

  public selectedMember: MemberEligibilityModel = new MemberEligibilityModel();
  public searchClick: boolean = false;
  public load: boolean = false;
  public newMemberSearched: boolean = false;

  public openSurvey: boolean = false;
  public url: string = "";
 
  public urlSafe: SafeResourceUrl;

  constructor(public _appService: AppService, private fb: FormBuilder, private fbSearch: FormBuilder, private readonly joyrideService: JoyrideService,
   public sanitizer: DomSanitizer) {
    this.searchClick = false;
    this.NewSearch();
    
    
  }

  @Input() MembBenFrm: FormGroup;
  @Input() searchForm: FormGroup;
  public orginalHist: BenHist[] = [];


  ngOnInit() {
    this.MembBenFrm = this.fb.group({
      'members': ['', Validators.required]
    });
    this.searchForm = this.fbSearch.group({
      'search': []
    });
    this.selectedMember = this._appService.MembershipService.AllMembers[0];
    this.MembBenFrm.get('members').setValue(this._appService.MembershipService.AllMembers[0]);
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
   // this._appService.MembershipService.AllMembers != undefined
  }

  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }

  GetBenifitResults(data: BenifitRights) {
    let c: BenifitRights = new BenifitRights();

    c.provid = this._appService.LoginService.CurrentLoggedInUser.provID;
    c.membid = this.selectedMember.subssn.substr(1, this.selectedMember.subssn.length - 1) + "-" + this.selectedMember.dependant;
    c.hp = this._appService.MembershipService.HealthplanContracts.hpcode;
    c.benName = data.benName;
    c.id = data.id;
    c.defaultProc = data.defaultProc;
    c.preAuth = data.preAuth;
    this._appService.MembershipService.GetBenifitResult(c);
  }

  OpenMoreInfoModalNewMember() {
    this.newMemberSearched = true;
    this.load = true;
    this._appService.MembershipService.SubmitVaiBenefit();
    if (this._appService.MembershipService.EmptyForm == false && this._appService.MembershipService.NoMembersFound == false) {
      setTimeout(() => {
        this.searchClick = true;
        let data: MemberEligibilityModel = new MemberEligibilityModel();
        this._appService.MembershipService.AllMembersBenScreen.forEach((el) => {
          if (el.membid.toUpperCase() == this._appService.MembershipService.MemberDetailsBenefitScreen.membid.toUpperCase()) {
            data = el;
          } else if (this._appService.MembershipService.MemberDetailsBenefitScreen.membid.toUpperCase().includes(el.membid.toUpperCase())) {
            data = el;
          } else if (el.membid.toUpperCase().includes(this._appService.MembershipService.MemberDetailsBenefitScreen.membid.toUpperCase())) {
            data = el;
          }
        },150)
        this.selectedMember = data;
        this._appService.MembershipService.returnBenSet = new BenifitRights();
        //  this._appService.MembershipService.moreInfo = true;
        this._appService.MembershipService.resultSetDisplay = [];
        console.log(this._appService.MembershipService.benRights.length);
        this._appService.MembershipService.benRights = [];
        //this.searchClick = false;
        let i = this._appService.MembershipService._toastService.currentlyActive;
        this._appService.MembershipService._toastService.clear();
        this._appService.MembershipService.GetBenRights(data);
        this.load = false;


      }, 3000)
      

    } else {
      this.load = false;
    }
  }

  OpenMoreInfoModal() {
    this.newMemberSearched = false;
    let data: MemberEligibilityModel = new MemberEligibilityModel();
    data = this.MembBenFrm.get('members').value;
    this.selectedMember = data;
    this._appService.MembershipService.returnBenSet = new BenifitRights();
    //  this._appService.MembershipService.moreInfo = true;
    this._appService.MembershipService.resultSetDisplay = [];
    console.log(this._appService.MembershipService.benRights.length);
    this._appService.MembershipService.benRights = [];
    this.searchClick = true;
    let i = this._appService.MembershipService._toastService.currentlyActive;
    this._appService.MembershipService._toastService.clear();
    this._appService.MembershipService.GetBenRights(data);

  }

  NewSearch() {
    this._appService.MembershipService.benRights = [];
    this.searchClick = false;
    this._appService.MembershipService._toastService.clear();
    this._appService.MembershipService.resultSetDisplay = [];

   // this._appService.MembershipService._toastService
  }

  onClick() {


      this.joyrideService.startTour({
        steps: ['look1', 'look2'],
        themeColor: '#313131'
      });
    



  }

  ShowAllSeacrhHist() {
    this._appService.MembershipService.allHist = true;
    this._appService.MembershipService.specificHist = false;
    this._appService.MembershipService.benHistOpen = false;
    this._appService.MembershipService.busy
    this._appService.MembershipService.GetAllBeifitSearchHist();
  }

  SplitResults(data: string): string[] {

    if (data != '') {
      let c: string[] = data.split(";");

      //console.log(data);

      return c;
    }

  }

  SearchBenHist(data: string) {
    if (data == "" || data == null || data == undefined) {
      this._appService.MembershipService.benHisList = [];
      this._appService.MembershipService.benHisList = this._appService.MembershipService.orgHistLIst;
    } else {
      let c: string = data.toLowerCase();
      this._appService.MembershipService.benHisList = this._appService.MembershipService.orgHistLIst.filter((a) => {

        if (a.logDate.includes(c)) {
          return a;
        }
        if (a.membId.toLowerCase().includes(c)) {
          return a;
        }
        if (a.hp.toLowerCase().includes(c)) {
          return a;
        }
        if (a.benName.toLowerCase().includes(c)) {
          return a;
        }
        if (a.reff.toLowerCase().includes(c)) {
          return a;
        }
      })
    }

  }

}

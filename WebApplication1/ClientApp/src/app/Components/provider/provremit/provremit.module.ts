import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { SharedModule } from './../../../../app/shared.module';
import { ProvremitComponent } from './provremit.component';
import { JoyrideModule } from 'ngx-joyride'

const routes: Routes = [{

  path: '',
  component: ProvremitComponent,
  children: [
    { pathMatch: 'full', path: 'provider-remittances', component: ProvremitComponent }
  ]

}];

@NgModule({
  declarations: [ProvremitComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    JoyrideModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class ProvremitModule { }

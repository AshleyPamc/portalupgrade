import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-provremit',
  templateUrl: './provremit.component.html',
  styleUrls: ['./provremit.component.css']
})
export class ProvremitComponent implements OnInit {

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  onClick() {
    this.joyrideService.startTour({
      steps: ['provRem_1', 'provRem_2', 'provRem_3', 'provRem_4'],
      themeColor: '#313131'
    })





  }
}

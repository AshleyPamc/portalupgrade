import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  constructor(public _appService: AppService, private readonly joyrideService: JoyrideService) {
    this.serviceDatePaidDate = true;
  }

  ngOnInit() {
  }

  get serviceDatePaidDate(): boolean {
    return this._appService.GraphsService.serviceDatePaidDate;
  }
  set serviceDatePaidDate(value: boolean) {
    this._appService.GraphsService.serviceDatePaidDate = value;
  }
  PaidUnPaidChange() {
    console.log(this.serviceDatePaidDate);
  }

  onClick() {
    this.joyrideService.startTour({
      steps: ['Report_1', 'Report_2', 'Report_3'],
      themeColor: '#313131'
    })
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { ReportsComponent } from './reports.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { JoyrideModule } from 'ngx-joyride';


const routes: Routes = [{

  path: '',
  component: ReportsComponent,
  children: [
    { pathMatch: 'full', path: 'Report', component: ReportsComponent }
  ]

}];

@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    JoyrideModule.forRoot(),
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    NgxEchartsModule
  ],
  exports: [FormsModule,
    ReactiveFormsModule]
})
export class ReportsModule {
}

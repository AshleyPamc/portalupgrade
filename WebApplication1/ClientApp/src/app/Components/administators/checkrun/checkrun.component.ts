import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { ValidationErrors, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { EditBankingDetailComponent } from './edit-banking-detail/edit-banking-detail.component';
import { ClrWizard } from '@clr/angular';
import { parse } from 'querystring';
import { ProviderViewComponent } from './provider-view/provider-view.component';
import { JoyrideService } from 'ngx-joyride'

@Component({
  selector: 'app-checkrun',
  templateUrl: './checkrun.component.html',
  styleUrls: ['./checkrun.component.css']
})
export class CheckrunComponent implements OnInit {

  @Input() cSelection: FormGroup;
  @Input() all: FormGroup;
  @Input() cCheckForm: FormGroup;
  @Input() specialtyForm: FormGroup;
  @Input() benefitForm: FormGroup;
  @Input() cCheckProvForm: FormGroup;
  @Input() cCheckMembForm: FormGroup;
  @Input() cCheckClaimForm: FormGroup;
  @Input() cServiceForm: FormGroup;

  @ViewChild('bankingDetails', { static: false }) bankingDetails: EditBankingDetailComponent;

  //Private variables with getters & setters
  private _errMsgEndDate: any;
  private _errorMsg: ValidationErrors;
  private _displaySpinner: boolean = false;
  private _displayHpLable: boolean = false;
  private _displayDate: string;
  private _showMemb: boolean = true;
  private _showProv: boolean = true;
  private _enableDownload: boolean = true;
  private _checkRunModalShow: boolean = false;
  private _payReadyChecked: boolean = false;
  private _disableBen: boolean = false;
  private _disableSpec: boolean = false;
  private _warningEdit: boolean = false;
  private _providerId: string;
  private _membId: string;
  private _claimTab: boolean = false;
  private _mebTab: boolean = false;
  private _provTab: boolean = false;
  private _dateTab: boolean = true;
  private _serviceTab: boolean = true;
  private _errMsgEndServDate: any;
  private _serviceToDate: any;
  private _claimNo: string;
  public _openAmountModal: boolean = false;
  public _largeModelOpen: boolean = false;


  @ViewChild('wizard', { static: false }) checkrunWizard: ClrWizard;

  constructor(public _appService: AppService, public fb: FormBuilder,
    public provfb: FormBuilder, public fbSpecialty: FormBuilder,
    public fbBenefit: FormBuilder, public fbMemb: FormBuilder,
    public fbClaim: FormBuilder, public fbSelect: FormBuilder, public fbAll: FormBuilder, public fbService: FormBuilder, private readonly joyrideService: JoyrideService) {

    this._errorMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      familyNumber: 'Please complete a family number field first!',
      valid: 'valid'
    };
    let y = this._appService.ClaimCaptureService.HpCodesList.length;
    if (y > 1) {
      this.displayHpLable = false;
    } else {
      this.displayHpLable = true;


    }
    this._appService.CheckRunService.GetRejectionReasons();


  }

  ngOnInit() {

    this.all = this.fbAll.group({
      selectall: []

    });

    this.cSelection = this.fbSelect.group({
      prov: [],
      memb: [],
      claim: [],
      date: [],
      service: []
    });
    this.cCheckProvForm = this.provfb.group({
      providerId: ['', Validators.required],
      HPCode: [],
      viewPayments: []
    });
    this.cCheckMembForm = this.fbMemb.group({
      membId: ['', Validators.required],
      HPCode: [],
      viewPayments: []
    });

    this.cCheckClaimForm = this.fbClaim.group({
      claimno: ['', Validators.required],
      HPCode: [],
      viewPayments: []
    });

    this.cCheckForm = this.fb.group({
      HPCode: [],
      endDate: ['', Validators.required],
      banking: [],
      viewPayments: [],
      payReady: []
    });
    this.cServiceForm = this.fbService.group({
      HPCode: [],
      endServDate: ['', Validators.required],
      viewPayments: [],
    });

    this.benefitForm = this.fbBenefit.group({
      benefit: []
    });

    this.specialtyForm = this.fbSpecialty.group({
      specialty: []
    });

    this.cCheckForm.valueChanges.subscribe(
      (changes) => {
        this.ChangeValues(changes);
      });
    this.cCheckForm.controls['viewPayments'].setValue(this._appService.CheckRunService.arrType[0]);

    this.benefitForm.disable();
  }

  //Getters & Setters
  public get errMsgEndDate(): any {
    return this._errMsgEndDate;
  }
  public set errMsgEndDate(value: any) {
    this._errMsgEndDate = value;
  }
  public get displaySpinner(): boolean {
    return this._displaySpinner;
  }
  public set displaySpinner(value: boolean) {
    this._displaySpinner = value;
  }
  public get displayHpLable(): boolean {
    return this._displayHpLable;
  }
  public set displayHpLable(value: boolean) {
    this._displayHpLable = value;
  }
  public get displayDate(): string {
    return this._displayDate;
  }
  public set displayDate(value: string) {
    this._displayDate = value;
  }
  public get showMemb(): boolean {
    return this._showMemb;
  }
  public set showMemb(value: boolean) {
    this._showMemb = value;
  }
  public get showProv(): boolean {
    return this._showProv;
  }
  public set showProv(value: boolean) {
    this._showProv = value;
  }
  public get enableDownload(): boolean {
    return this._enableDownload;
  }
  public set enableDownload(value: boolean) {
    this._enableDownload = value;
  }
  public get checkRunModalShow(): boolean {
    return this._checkRunModalShow;
  }
  public set checkRunModalShow(value: boolean) {
    this._checkRunModalShow = value;
  }
  public get payReadyChecked(): boolean {
    return this._payReadyChecked;
  }
  public set payReadyChecked(value: boolean) {
    this._payReadyChecked = value;
  }
  public get disableBen(): boolean {
    return this._disableBen;
  }
  public set disableBen(value: boolean) {
    this._disableBen = value;
  }
  public get disableSpec(): boolean {
    return this._disableSpec;
  }
  public set disableSpec(value: boolean) {
    this._disableSpec = value;
  }
  public get warningEdit(): boolean {
    return this._warningEdit;
  }
  public set warningEdit(value: boolean) {
    this._warningEdit = value;
  }
  public get providerId(): string {
    return this._providerId;
  }
  public set providerId(value: string) {
    this._providerId = value;
  }
  public get membId(): string {
    return this._membId;
  }
  public set membId(value: string) {
    this._membId = value;
  }
  public get dateTab(): boolean {
    return this._dateTab;
  }
  public set dateTab(value: boolean) {
    this._dateTab = value;
  }
  public get provTab(): boolean {
    return this._provTab;
  }
  public set provTab(value: boolean) {
    this._provTab = value;
  }
  public get mebTab(): boolean {
    return this._mebTab;
  }
  public set mebTab(value: boolean) {
    this._mebTab = value;
  }
  public get claimTab(): boolean {
    return this._claimTab;
  }
  public set claimTab(value: boolean) {
    this._claimTab = value;
  }
  public get claimNo(): string {
    return this._claimNo;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }
  public get serviceTab(): boolean {
    return this._serviceTab;
  }
  public set serviceTab(value: boolean) {
    this._serviceTab = value;
  }
  public get errMsgEndServDate(): any {
    return this._errMsgEndServDate;
  }
  public set errMsgEndServDate(value: any) {
    this._errMsgEndServDate = value;
  }
  public get serviceToDate(): any {
    return this._serviceToDate;
  }
  public set serviceToDate(value: any) {
    this._serviceToDate = value;
  }

  onClick() {
    this._appService.CheckRunService._expandFilter = true;
    this._appService.CheckRunService._expandLegend = true;
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['checkrun_1', 'checkrun_2', 'checkrun_3', 'checkrun_4', 'checkrun_5', 'checkrun_6', 'checkrun_7', 'checkrun_8', 'checkrun_9'
          , 'checkrun_10', 'checkrun_11', 'checkrun_12', 'checkrun_13', 'checkrun_14', 'checkrun_15', 'checkrun_16', 'checkrun_17', 'checkrun_18'
          , 'checkrun_19', 'checkrun_20', 'checkrun_21', 'checkrun_22', 'checkrun_23'],
        themeColor: '#313131'
      })
    }

  }

  ChangeValues(changes: any) {
    this._appService.CheckRunService.excludeBankingDetails = this.cCheckForm.get('banking').value;
  }

  FieldValidation(fieldData: any, controlName: string) {
    if ((fieldData === undefined) || (fieldData === '') || (fieldData === null)) {
      this.cCheckForm.get(controlName).setErrors(this._errorMsg['required']);
      return this._errorMsg['required'];
    }
    if ((controlName !== 'fromDateField') && (controlName !== 'endServDate')) {
      if (fieldData.length > 20) {
        this.cCheckForm.get(controlName).setErrors(this._errorMsg['maxLength']);
        return this._errorMsg['maxLength'];
      }
    }
    return this._errorMsg['valid'];

  }

  HpCodeChanged() {
    if (this._appService.CheckRunService._hpListShow === false) {
      this._appService.CheckRunService.amounts.hpCode = this._appService.ClaimCaptureService.HpCodesList[0].code;
    } else {
      if (this.mebTab) {
        this._appService.CheckRunService.amounts.hpCode = this.cCheckMembForm.get('HPCode').value;
      }
      if (this.provTab) {
        this._appService.CheckRunService.amounts.hpCode = this.cCheckProvForm.get('HPCode').value;
      }
      if (this.serviceTab) {
        this._appService.CheckRunService.amounts.hpCode = this.cServiceForm.get('HPCode').value;
      }
      if (this.claimTab) {
        this._appService.CheckRunService.amounts.hpCode = this.cCheckClaimForm.get('HPCode').value;
      }
      if (this.dateTab) {
        this._appService.CheckRunService.amounts.hpCode = this.cCheckForm.get('HPCode').value;
      }
      // console.log(this._appService.CheckRunService.amounts.hpCode);

    }
  }

  PopulateServiceDateErr() {
    let ToDay: Date = new Date;
    let comparedDate = new Date(this._appService.CheckRunService.amounts.serviceToDate);
    if (comparedDate.toString().includes('Invalid Date')) {
      this.errMsgEndServDate = 'Entered date is invalid.';
    } else {
      if (comparedDate > ToDay) {
        this.errMsgEndServDate = 'Chosen date cannot be greater than ' + ToDay.toLocaleString();
      } else {
        this.errMsgEndServDate = '';
      }
    }
  }

  PopulateFromDateErr() {
    let ToDay: Date = new Date;
    let comparedDate = new Date(this._appService.CheckRunService.amounts.endDate);
    if (comparedDate.toString().includes('Invalid Date')) {
      this.errMsgEndDate = 'Entered date is invalid.';
    } else {
      if (comparedDate > ToDay) {
        this.errMsgEndDate = 'Chosen date cannot be greater than ' + ToDay.toLocaleString();
      } else {
        this.errMsgEndDate = '';
      }
    }
  }

  ValidateServiceDateField() {
    this._appService.CheckRunService.amounts.serviceToDate = new Date(this.cServiceForm.get('endServDate').value).toDateString();
    this.errMsgEndServDate = this.FieldValidation(this._appService.CheckRunService.amounts.serviceToDate, 'endServDate');
    //  this.errMsgEndDate = this.FieldValidation(this.checkRunService.amounts.endDate, 'fromDateField');
    if (this.errMsgEndServDate === 'valid') {
      this.errMsgEndServDate = ' ';
      this.PopulateServiceDateErr();
    }
    if (this.errMsgEndServDate !== '') {
      this.cServiceForm.get('endServDate').setErrors(this.errMsgEndServDate);
    }
  }

  ValidateEndDateField() {
    this._appService.CheckRunService.amounts.endDate = new Date(this.cCheckForm.get('endDate').value).toDateString();
    this.errMsgEndDate = this.FieldValidation(this._appService.CheckRunService.amounts.endDate, 'fromDateField');
    //  this.errMsgEndDate = this.FieldValidation(this.checkRunService.amounts.endDate, 'fromDateField');
    if (this.errMsgEndDate === 'valid') {
      this.errMsgEndDate = ' ';
      this.PopulateFromDateErr();
    }
    if (this.errMsgEndDate !== '') {
      this.cCheckForm.get('endDate').setErrors(this.errMsgEndDate);
    }

  }

  viewChanged() {
    if (this.cCheckForm.get('viewPayments').value === 'Provider') {
      this.showMemb = false;
      this.showProv = true;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "P";
      this._appService.CheckRunService._isProvider = true;
    }
    if (this.cCheckForm.get('viewPayments').value === 'Member') {
      this.showMemb = true;
      this.showProv = false;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "M";
      this._appService.CheckRunService._isProvider = false;

    }
    if (this.cCheckProvForm.get('viewPayments').value === 'Provider') {
      this.showMemb = false;
      this.showProv = true;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "P";
      this._appService.CheckRunService._isProvider = true;

    }
    if (this.cCheckProvForm.get('viewPayments').value === 'Member') {
      this.showMemb = true;
      this.showProv = false;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "M";
      this._appService.CheckRunService._isProvider = false;

    }
    if (this.cCheckMembForm.get('viewPayments').value === 'Provider') {
      this.showMemb = false;
      this.showProv = true;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "P";
      this._appService.CheckRunService._isProvider = true;
    }
    if (this.cCheckMembForm.get('viewPayments').value === 'Member') {
      this.showMemb = true;
      this.showProv = false;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "M";
      this._appService.CheckRunService._isProvider = false;

    }
    if (this.cCheckClaimForm.get('viewPayments').value === 'Provider') {
      this.showMemb = false;
      this.showProv = true;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "P";
      this._appService.CheckRunService._isProvider = true;
    }
    if (this.cCheckClaimForm.get('viewPayments').value === 'Member') {
      this.showMemb = true;
      this.showProv = false;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "M";
      this._appService.CheckRunService._isProvider = false;

    }
    if (this.cServiceForm.get('viewPayments').value === 'Provider') {
      this.showMemb = false;
      this.showProv = true;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "P";
      this._appService.CheckRunService._isProvider = true;
    }
    if (this.cServiceForm.get('viewPayments').value === 'Member') {
      this.showMemb = true;
      this.showProv = false;
      this._appService.CheckRunService._paymentType = "";
      this._appService.CheckRunService._paymentType = "M";
      this._appService.CheckRunService._isProvider = false;

    }

  }

  SubmitDetail() {
    this._openAmountModal = true;
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService.tempDiscription = '';
    this._appService.CheckRunService.displaySpinner = true;
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.amountsToDisplay.splice(0, this._appService.CheckRunService.amountsToDisplay.length);
    let x: string;
    x = this._appService.CheckRunService._paymentType;
    if ((x === undefined) || (x === '') || (x === null)) {
      this._appService.CheckRunService._paymentType = 'M';
      this.showMemb = true;
      this.showProv = false;
    }
    this._appService.CheckRunService.GetExcludedClaims();
    this.displayDate = this._appService.CheckRunService.GetLastRecievedDate();
    this.specialtyForm.enable();
    this.benefitForm.disable();
    this._appService.CheckRunService._wizardClaimSort = false;
    this._appService.CheckRunService._wizardDateSort = true;
    this._appService.CheckRunService._wizardMembSort = false;
    this._appService.CheckRunService._wizardProvSort = false;
    this._appService.CheckRunService._wizardServiceSort = false;

  }

  SubmitDetailService() {
    this._openAmountModal = true;
    this._serviceToDate = this.cServiceForm.get('endServDate').value;
    this._appService.CheckRunService.amounts.serviceToDate = this._serviceToDate;
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService.tempDiscription = '';
    this._appService.CheckRunService.displaySpinner = true;
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.amountsToDisplay.splice(0, this._appService.CheckRunService.amountsToDisplay.length);
    let x: string;
    x = this._appService.CheckRunService._paymentType;
    if ((x === undefined) || (x === '') || (x === null)) {
      this._appService.CheckRunService._paymentType = 'M';
      this.showMemb = true;
      this.showProv = false;
    }
    //this._appService.CheckRunService.excludeBankingDetails = this.cCheckForm.get('banking').value;
    this._appService.CheckRunService.GetExcludedClaimsServ();
    this._appService.CheckRunService._wizardClaimSort = false;
    this._appService.CheckRunService._wizardDateSort = false;
    this._appService.CheckRunService._wizardMembSort = false;
    this._appService.CheckRunService._wizardProvSort = false;
    this._appService.CheckRunService._wizardServiceSort = true;
  }

  ResetVariables() {
    this._appService.CheckRunService.showText = false;
    this.checkRunModalShow = false;
  }

  CloseCheckrun() {
    this.checkrunWizard.reset();
    this._appService.CheckRunService._wizardNext = false;
    this.cCheckForm.reset();
    this.cSelection.reset();
    this.cCheckMembForm.reset();
    this.cCheckProvForm.reset();
    this.cCheckClaimForm.reset();
    this._appService.CheckRunService.closeCheckRun = true;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._filtteredList.splice(0, this._appService.CheckRunService._filtteredList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService.displayData = false;
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this.cCheckForm.reset();
    this._appService.CheckRunService._paymentType = "";
    this._appService.CheckRunService._providerGrouping = false;
    this._appService.CheckRunService._fillteredProvList.splice(0, this._appService.CheckRunService._fillteredProvList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService._uniqueProvList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._filteredBordRowList.splice(0, this._appService.CheckRunService._filteredBordRowList.length);
    this._appService.CheckRunService._scrollableView = false;
    this._appService.CheckRunService._isProvider = false;
    this._appService.CheckRunService.displaySpinner = false;
    this._appService.CheckRunService.enableNavButtons = true;
    this._appService.CheckRunService.GetListOfPaidBatches();

  }

  CloseCheckRunWarning() {
    this._appService.CheckRunService.closeCheckRun = false;
  }

  GenerateBordRow() {
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.loadBoardRow = true;
    this._appService.CheckRunService.GenerateBatchNumber();
  }

  GenerateBordRowProv() {
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.loadBoardRow = true;
    this._appService.CheckRunService.GenerateBatchNumberProv();
  }

  GenerateBordRowMemb() {
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.loadBoardRow = true;
    this._appService.CheckRunService.GenerateBatchNumberMemb();
  }


  AddSpecToWhereClause(desc: string) {

    this._appService.CheckRunService._hpListShow
    let check = this.specialtyForm.get('specialty').value;
    if (check === true) {

      this._appService.CheckRunService.amounts.sorting = 'spec';
      this._appService.CheckRunService._whereCauseSpec++;
      this._appService.CheckRunService.whereClause.push(desc);
      this._appService.CheckRunService.gridData.forEach((gd) => {
        if (desc === gd.specCode) {
          gd.include = true;
          if (this._appService.CheckRunService.amounts.claimType === 'P') {
            this._appService.CheckRunService.totalProvPayments = this._appService.CheckRunService.totalProvPayments + parseFloat(gd.providerPayment);
            this._appService.CheckRunService.totalProvPayments = + this._appService.CheckRunService.totalProvPayments.toFixed(3);
            this._appService.CheckRunService.totalClaims = this._appService.CheckRunService.totalClaims + parseFloat(gd.amountOfClaims);
            this._appService.CheckRunService.totalClaims = + this._appService.CheckRunService.totalClaims.toFixed(3);
          } else {
            this._appService.CheckRunService.totalMembPayments = this._appService.CheckRunService.totalMembPayments + parseFloat(gd.memberPayment);
            this._appService.CheckRunService.totalMembPayments = +this._appService.CheckRunService.totalMembPayments.toFixed(3);
            this._appService.CheckRunService.totalClaims = this._appService.CheckRunService.totalClaims + parseFloat(gd.amountOfClaims);
            this._appService.CheckRunService.totalClaims = + this._appService.CheckRunService.totalClaims.toFixed(3);
          }
        }
      });
    } else {
      this._appService.CheckRunService.amounts.sorting = 'spec';
      this._appService.CheckRunService._whereCauseSpec--;
      let count: number = 0;
      for (var i = 0; i < this._appService.CheckRunService.whereClause.length; i++) {
        if (this._appService.CheckRunService.whereClause[i] == desc) {
          count = i;
        }
      }
      this._appService.CheckRunService.whereClause.splice(count, 1);
      this._appService.CheckRunService.gridData.forEach((gd) => {
        if (desc === gd.specCode) {
          gd.include = false;
          if (this._appService.CheckRunService.amounts.claimType === 'P') {
            this._appService.CheckRunService.totalProvPayments = this._appService.CheckRunService.totalProvPayments - parseFloat(gd.providerPayment);
            this._appService.CheckRunService.totalProvPayments = + this._appService.CheckRunService.totalProvPayments.toFixed(3);
            this._appService.CheckRunService.totalClaims = this._appService.CheckRunService.totalClaims - parseFloat(gd.amountOfClaims);
            this._appService.CheckRunService.totalClaims = + this._appService.CheckRunService.totalClaims.toFixed(3);
          } else {
            this._appService.CheckRunService.totalMembPayments = this._appService.CheckRunService.totalMembPayments - parseFloat(gd.memberPayment);
            this._appService.CheckRunService.totalMembPayments = +this._appService.CheckRunService.totalMembPayments.toFixed(3);
            this._appService.CheckRunService.totalClaims = this._appService.CheckRunService.totalClaims - parseFloat(gd.amountOfClaims);
            this._appService.CheckRunService.totalClaims = + this._appService.CheckRunService.totalClaims.toFixed(3);
          }
        }
      });
    }
    if (this._appService.CheckRunService._whereCauseSpec > 0) {
      this.disableBen = true;
      this.benefitForm.disable();
    } else {
      this.disableBen = false;
      this.benefitForm.enable();
    }
    if (this._appService.CheckRunService.whereClause.length == 0) {
      this._appService.CheckRunService._wizardNext = false;
    } else {
      this._appService.CheckRunService._wizardNext = true;
    }
    //console.log(this._appService.CheckRunService.whereClause);
  }

  GeneratePritableFile() {
    this._appService.CheckRunService.GenerateBatchForPrinting();
  }

  SubmitDetailProvider() {
    this._openAmountModal = true;
    this.providerId = this.cCheckProvForm.get('providerId').value;
    this._appService.CheckRunService.amounts.provid = this.providerId;
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService.tempDiscription = '';
    this._appService.CheckRunService.displaySpinner = true;
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.amountsToDisplay.splice(0, this._appService.CheckRunService.amountsToDisplay.length);
    let x: string;
    x = this._appService.CheckRunService._paymentType;
    if ((x === undefined) || (x === '') || (x === null)) {
      this._appService.CheckRunService._paymentType = 'M';
      this.showMemb = true;
      this.showProv = false;
    }
    this._appService.CheckRunService.GetExcludedClaimsProv();
    this._appService.CheckRunService._wizardClaimSort = false;
    this._appService.CheckRunService._wizardDateSort = false;
    this._appService.CheckRunService._wizardMembSort = false;
    this._appService.CheckRunService._wizardProvSort = true;
    this._appService.CheckRunService._wizardServiceSort = false;
  }

  SubmitDetailMember() {
    this._openAmountModal = true;
    this.membId = this.cCheckMembForm.get('membId').value;
    this._appService.CheckRunService.amounts.membid = this.membId;
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService.tempDiscription = '';
    this._appService.CheckRunService.displaySpinner = true;
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.amountsToDisplay.splice(0, this._appService.CheckRunService.amountsToDisplay.length);
    let x: string;
    x = this._appService.CheckRunService._paymentType;
    if ((x === undefined) || (x === '') || (x === null)) {
      this._appService.CheckRunService._paymentType = 'M';
      this.showMemb = true;
      this.showProv = false;
    }
    this._appService.CheckRunService.GetExcludedClaimsMembNo();
    this._appService.CheckRunService._wizardClaimSort = false;
    this._appService.CheckRunService._wizardDateSort = false;
    this._appService.CheckRunService._wizardMembSort = true;
    this._appService.CheckRunService._wizardProvSort = false;
    this._appService.CheckRunService._wizardServiceSort = false;

  }

  SubmitDetailClaim() {
    this._openAmountModal = true;
    this.claimNo = this.cCheckClaimForm.get('claimno').value;
    this._appService.CheckRunService.amounts.claimno = this.claimNo;
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService._totalAmountToPay = 0;
    this._appService.CheckRunService.bordRowList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._uniqueProvList.splice(0, this._appService.CheckRunService.bordRowList.length);
    this._appService.CheckRunService._patientsPerProvider = {};
    this._appService.CheckRunService._uniqueList.splice(0, this._appService.CheckRunService._uniqueList.length);
    this._appService.CheckRunService._patientsPerFamily = {};
    this._appService.CheckRunService.showText = false;
    this._appService.CheckRunService.tempDiscription = '';
    this._appService.CheckRunService.displaySpinner = true;
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.amountsToDisplay.splice(0, this._appService.CheckRunService.amountsToDisplay.length);
    let x: string;
    x = this._appService.CheckRunService._paymentType;
    if ((x === undefined) || (x === '') || (x === null)) {
      this._appService.CheckRunService._paymentType = 'M';
      this.showMemb = true;
      this.showProv = true;
    }
    this._appService.CheckRunService.GetExcludedClaimsClaimNo();
    this._appService.CheckRunService._wizardClaimSort = true;
    this._appService.CheckRunService._wizardDateSort = false;
    this._appService.CheckRunService._wizardMembSort = false;
    this._appService.CheckRunService._wizardProvSort = false;
    this._appService.CheckRunService._wizardServiceSort = false;
  }

  GenerateBordRowClaim() {
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.loadBoardRow = true;
    this._appService.CheckRunService.GenerateBatchNumberClaim();
  }

  GenerateBordRowServiceDate() {
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.loadBoardRow = true;
    this._appService.CheckRunService.GenerateBatchNumberService();
  }

  Generatecheckrun() {
    this.checkrunWizard.reset();
    this._appService.CheckRunService._wizardNext = false;
    this.cCheckForm.reset();
    this.cSelection.reset();
    this.cCheckMembForm.reset();
    this.cCheckProvForm.reset();
    this.cCheckClaimForm.reset();
    if (this._appService.CheckRunService._wizardProvSort == true) {
      this.GenerateBordRowProv();
    }
    if (this._appService.CheckRunService._wizardClaimSort == true) {
      this.GenerateBordRowClaim();
    }
    if (this._appService.CheckRunService._wizardDateSort == true) {
      this.GenerateBordRow();
    }
    if (this._appService.CheckRunService._wizardMembSort == true) {
      this.GenerateBordRowMemb();
    }
    if (this._appService.CheckRunService._wizardServiceSort == true) {
      this.GenerateBordRowServiceDate();
    }
  }

  ProvForm() {
    this.cSelection.controls['service'].setValue(false);
    this.cSelection.controls['claim'].setValue(false);
    this.cSelection.controls['memb'].setValue(false);
    this.cSelection.controls['date'].setValue(false);
    let value: boolean;
    value = this.cSelection.get('prov').value;
    if (value) {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = true;
      this._appService.CheckRunService._wizardServiceSort = false;
      this.claimTab = false;
      this.mebTab = false;
      this.provTab = true;
      this.dateTab = false;
      this.serviceTab = false;
      this.cCheckClaimForm.reset();
      this.cCheckProvForm.reset();
      this.cCheckMembForm.reset();
      this.cCheckForm.reset();
      this.cServiceForm.reset();
      this._appService.CheckRunService._wizardNext = true;
    } else {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardNext = false;
      this._appService.CheckRunService._wizardServiceSort = false;
    }
  }

  MembForm() {
    this.cSelection.controls['service'].setValue(false);
    this.cSelection.controls['prov'].setValue(false);
    this.cSelection.controls['claim'].setValue(false);
    this.cSelection.controls['date'].setValue(false);
    let value: boolean;
    value = this.cSelection.get('memb').value;
    if (value) {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = true;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardServiceSort = false;
      this.claimTab = false;
      this.mebTab = true;
      this.provTab = false;
      this.dateTab = false;
      this.serviceTab = false;
      this.cCheckClaimForm.reset();
      this.cCheckProvForm.reset();
      this.cCheckMembForm.reset();
      this.cCheckForm.reset();
      this.cServiceForm.reset();
      this._appService.CheckRunService._wizardNext = true;
    } else {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardNext = false;
      this._appService.CheckRunService._wizardServiceSort = false;
    }
  }

  ClaimForm() {
    this.cSelection.controls['service'].setValue(false);
    this.cSelection.controls['prov'].setValue(false);
    this.cSelection.controls['memb'].setValue(false);
    this.cSelection.controls['date'].setValue(false);
    ;

    let value: any;
    value = this.cSelection.get('claim').value;
    if (value) {
      this._appService.CheckRunService._wizardClaimSort = true;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardServiceSort = false;
      this.claimTab = true;
      this.mebTab = false;
      this.provTab = false;
      this.dateTab = false;
      this.serviceTab = false;
      this.cCheckClaimForm.reset();
      this.cCheckProvForm.reset();
      this.cCheckMembForm.reset();
      this.cCheckForm.reset();
      this.cServiceForm.reset();
      this._appService.CheckRunService._wizardNext = true;
    } else {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardNext = false;
      this._appService.CheckRunService._wizardServiceSort = false;
    }
  }

  DateForm() {
    this.cSelection.controls['service'].setValue(false);
    this.cSelection.controls['prov'].setValue(false);
    this.cSelection.controls['memb'].setValue(false);
    this.cSelection.controls['claim'].setValue(false);
    let value: boolean;
    value = this.cSelection.get('date').value;
    if (value) {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = true;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardServiceSort = false;
      this.claimTab = false;
      this.mebTab = false;
      this.provTab = false;
      this.dateTab = true;
      this.serviceTab = false;
      this.cCheckClaimForm.reset();
      this.cCheckProvForm.reset();
      this.cCheckMembForm.reset();
      this.cCheckForm.reset();
      this.cServiceForm.reset();
      this._appService.CheckRunService._wizardNext = true
    } else {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardNext = false;
      this._appService.CheckRunService._wizardServiceSort = false;
    }
  }

  incWizardPage() {
    this._appService.CheckRunService._wizardpagecount++;
    if (this._appService.CheckRunService._wizardpagecount == 2) {
      this._appService.CheckRunService._wizardNext = false;
    }
    if (this._appService.CheckRunService._wizardpagecount == 3) {
      this._appService.CheckRunService._wizardNext = false;
    }
  }

  decWizardPage() {
    this._appService.CheckRunService._wizardpagecount--;
    if (this._appService.CheckRunService._wizardpagecount == 1) {
      this.cSelection.controls['prov'].setValue(false);
      this.cSelection.controls['claim'].setValue(false);
      this.cSelection.controls['memb'].setValue(false);
      this.cSelection.controls['date'].setValue(false);
      this._appService.CheckRunService._wizardNext = false;
    }
  }

  ResetWizard() {
    this._appService.CheckRunService._wizardpagecount = 1;
    this._appService.CheckRunService._wizardNext = false;
    this.checkrunWizard.reset();
    this.cSelection.reset();
    this.cCheckProvForm.reset();
    this.cCheckMembForm.reset();
    this.cCheckForm.reset();
    this.cCheckClaimForm.reset();
  }

  ResetForms() {
    this._appService.CheckRunService._largeAmountFound = false;
    this._appService.CheckRunService.excludingClaims = [];
    this.checkrunWizard.reset();
    this._appService.CheckRunService._wizardNext = false;
    this._appService.CheckRunService._wizard = false;
    this.cCheckForm.reset();
    this.cSelection.reset();
    this.cCheckMembForm.reset();
    this.cCheckProvForm.reset();
    this.cCheckClaimForm.reset();
    this.cServiceForm.reset();
  }

  AddAllSpecToWhereClause() {
    let x: boolean;
    x = this.all.get('selectall').value
    this._appService.CheckRunService.amounts.sorting = 'spec';
    this._appService.CheckRunService.totalClaims = 0;
    this._appService.CheckRunService.totalMembPayments = 0;
    this._appService.CheckRunService.totalProvPayments = 0;
    this._appService.CheckRunService.whereClause.splice(0, this._appService.CheckRunService.gridData.length);
    if (x) {
      this._appService.CheckRunService.gridData.forEach((el) => {
        el.include = true;
        this._appService.CheckRunService.whereClause.push(el.specCode);
        this._appService.CheckRunService.totalClaims = this._appService.CheckRunService.totalClaims + parseInt(el.amountOfClaims);
        this._appService.CheckRunService.totalMembPayments = this._appService.CheckRunService.totalMembPayments + parseFloat(el.memberPayment);
        this._appService.CheckRunService.totalProvPayments = this._appService.CheckRunService.totalProvPayments + parseFloat(el.providerPayment);
      });
    } else {
      this._appService.CheckRunService.whereClause.splice(0, this._appService.CheckRunService.whereClause.length);
      this._appService.CheckRunService.gridData.forEach((el) => {
        el.include = false;
      });
      this._appService.CheckRunService.totalClaims = 0;
      this._appService.CheckRunService.totalMembPayments = 0;
      this._appService.CheckRunService.totalProvPayments = 0;
    }
    if (this._appService.CheckRunService.whereClause.length == 0) {
      this._appService.CheckRunService._wizardNext = false;
    } else {
      this._appService.CheckRunService._wizardNext = true;
    }
  }

  ServiceForm() {
    this.cSelection.controls['claim'].setValue(false);
    this.cSelection.controls['memb'].setValue(false);
    this.cSelection.controls['date'].setValue(false);
    this.cSelection.controls['prov'].setValue(false);
    let value: boolean;
    value = this.cSelection.get('service').value;
    if (value) {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardServiceSort = true;
      this.claimTab = false;
      this.mebTab = false;
      this.provTab = false;
      this.dateTab = false;
      this.serviceTab = true;
      this.cCheckClaimForm.reset();
      this.cCheckProvForm.reset();
      this.cCheckMembForm.reset();
      this.cCheckForm.reset();
      this.cServiceForm.reset();
      this._appService.CheckRunService._wizardNext = true
    } else {
      this._appService.CheckRunService._wizardClaimSort = false;
      this._appService.CheckRunService._wizardDateSort = false;
      this._appService.CheckRunService._wizardMembSort = false;
      this._appService.CheckRunService._wizardProvSort = false;
      this._appService.CheckRunService._wizardNext = false;
      this._appService.CheckRunService._wizardServiceSort = false;

    }
  }

  OpenLargeClaimModel() {
    this._largeModelOpen = true;
    this._openAmountModal = false;
  }

}

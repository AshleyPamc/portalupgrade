import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { RejectClaimPopupComponent } from '../reject-claim-popup/reject-claim-popup.component';
import { CheckrunModel } from './../../../../models/CheckrunModel';
import { CheckRunDetailsModel } from './../../../../models/CheckRunDetailsModel';
import { RejectListTotalsModel } from './../../../../models/RejectListTotalsModel';
import { BankingDetailModel } from './../../../../models/BankingDetailModel';

import { BatchDetailsModel } from './../../../../models/BatchDetailsModel';
import { EditBankingDetailComponent } from '../edit-banking-detail/edit-banking-detail.component';
import { JoyrideService } from 'ngx-joyride'
import { EditReleasedBatchComponent } from '../edit-released-batch/edit-released-batch.component';

@Component({
  selector: 'app-provider-view',
  templateUrl: './provider-view.component.html',
  styleUrls: ['./provider-view.component.css']
})
export class ProviderViewComponent implements OnInit {

  @Input() cEntireClaimOptions: FormGroup;
  @Input() cClaimOptions: FormGroup;
  @ViewChild('rejectionReason', { static: false }) rejectionReason: RejectClaimPopupComponent;
  @ViewChild('searchBatch', {static:false}) searchBatch: EditReleasedBatchComponent;
  @ViewChild('searchUnreleadBatch', { static: false }) searchUnreleadBatch: EditReleasedBatchComponent;
  @ViewChild('banking', { static:false }) banking: EditBankingDetailComponent;

  @Input() cbordRowForm: FormGroup;
  @Input() cPayDateForm: FormGroup;
  @Input() cConfirmationForm: FormGroup;

  private _maxPages: number;
  private _minPages: number;
  private _rejectModalShow: boolean;
  private _currentMethod: string;
  private _cancelRejectionMethod: string;
  private _mainMembNo: string;
  private _membNo: string;
  private _providerID: string;
  private _openDetailModel: boolean = false;
  private _amountPaid: string;
  private _amountApp: string;
  private _memberName: string;
  private _expandtree: boolean = false;
  private _openWarn: boolean = false;
  public _bankingDetails: BankingDetailModel = new BankingDetailModel();
  public _displayProvbankingDetails: BankingDetailModel = new BankingDetailModel();
  private _errMsgPayDate: any;
  private _id: string;
  private _claimNo: string;
  private _enableSubmit: boolean = false;
  private _submitWarning: boolean = false;
  private _errorMsg: ValidationErrors;
  private _completeWizard: boolean = false;
  private _enableNextBtn: boolean = true;
  private _enableSubBtn: boolean = false;
  private counter: number = 0;
  public noBankingFound: boolean = false;
  public rejectedClaims: RejectListTotalsModel[] = [];


  constructor(public _appService: AppService, public fbAll: FormBuilder,
    public fbMember: FormBuilder, public fbBordRow: FormBuilder,
    public fbconfirm: FormBuilder, public fbDate: FormBuilder, private readonly joyrideService: JoyrideService) {

    this._appService.CheckRunService.currentPage = 1;
    this.rejectModalShow = false;
    this._errorMsg = {
      required: 'This field is required!'
    }

  }

  ngOnInit() {

    this.cPayDateForm = this.fbDate.group({
      payDate: ['', Validators.required]
    });

    this.cConfirmationForm = this.fbconfirm.group({
      payBatch: []
    });

    this.cEntireClaimOptions = this.fbAll.group({
      validateAll: [],
      excludeAll: [],
      rejectAll: []
    });

    this.cClaimOptions = this.fbMember.group({
      validateLine: [],
      excludeLine: [],
      rejectLine: []
    });

    this.cbordRowForm = this.fbBordRow.group({
      payReady: [],
      grouping: []


    });

    if (this._appService.CheckRunService.hideReleaseOpt !== true) {
      this.cEntireClaimOptions.disable();
      this.cClaimOptions.disable();
      this.cbordRowForm.disable();
    }

  }

  public get maxPages(): number {
    this._maxPages = this._appService.CheckRunService._providerPageCount;
    return this._maxPages;
  }
  public get minPages(): number {
    this._minPages = 1;
    return this._minPages;
  }
  public get rejectModalShow(): boolean {
    return this._rejectModalShow;
  }
  public set rejectModalShow(value: boolean) {
    this._rejectModalShow = value;
  }
  public set currentMethod(value: string) {
    this._currentMethod = value;
  }
  public get cancelRejectionMethod(): string {
    return this._cancelRejectionMethod;
  }
  public set cancelRejectionMethod(value: string) {
    this._cancelRejectionMethod = value;
  }
  public set membNo(value: string) {
    this._membNo = value;
  }
  public set id(value: string) {
    this._id = value;
  }
  public set mainMembNo(value: string) {
    this._mainMembNo = value;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }
  public get providerID(): string {
    return this._providerID;
  }
  public set providerID(value: string) {
    this._providerID = value;
  }
  public get openDetailModel(): boolean {
    return this._openDetailModel;
  }
  public set openDetailModel(value: boolean) {
    this._openDetailModel = value;
  }
  public get amountPaid(): string {
    return this._amountPaid;
  }
  public set amountPaid(value: string) {
    this._amountPaid = value;
  }
  public get amountApp(): string {
    return this._amountApp;
  }
  public set amountApp(value: string) {
    this._amountApp = value;
  }
  public get memberName(): string {
    return this._memberName;
  }
  public set memberName(value: string) {
    this._memberName = value;
  }
  public get expandtree(): boolean {
    return this._expandtree;
  }
  public set expandtree(value: boolean) {
    this._expandtree = value;
  }
  public get openWarn(): boolean {
    return this._openWarn;
  }
  public set openWarn(value: boolean) {
    this._openWarn = value;
  }
  public get errMsgPayDate(): any {
    return this._errMsgPayDate;
  }
  public set errMsgPayDate(value: any) {
    this._errMsgPayDate = value;
  }
  public get enableSubmit(): boolean {
    return this._enableSubmit;
  }
  public set enableSubmit(value: boolean) {
    this._enableSubmit = value;
  }
  public get submitWarning(): boolean {
    return this._submitWarning;
  }
  public set submitWarning(value: boolean) {
    this._submitWarning = value;
  }
  public get completeWizard(): boolean {
    return this._completeWizard;
  }
  public set completeWizard(value: boolean) {
    this._completeWizard = value;
  }
  public get enableNextBtn(): boolean {
    return this._enableNextBtn;
  }
  public set enableNextBtn(value: boolean) {
    this._enableNextBtn = value;
  }
  public get enableSubBtn(): boolean {
    return this._enableSubBtn;
  }
  public set enableSubBtn(value: boolean) {
    this._enableSubBtn = value;
  }



  IncreasePage() {

    this._appService.LoginService.CurrentLoggedInUser.userType === 3
    let max = this.maxPages;
    let min = this.minPages
    let cur = this._appService.CheckRunService.currentPage;
    cur = cur + 1;
    if ((cur <= max) && (cur >= min)) {
      this._appService.CheckRunService.FilterProvDictOnIncrease(cur);
      this._appService.CheckRunService.currentPage = cur;
    } else {
      this._appService.CheckRunService.FilterProvDictOnIncrease(1);
      this._appService.CheckRunService.currentPage = 1;
    }

  }

  DecreasePage() {
    let max = this.maxPages;
    let min = this.minPages
    let cur = this._appService.CheckRunService.currentPage;
    cur = cur - 1;
    if ((cur <= max) && (cur >= min)) {
      this._appService.CheckRunService.FilterProvDictOnIncrease(cur);
      this._appService.CheckRunService.currentPage = cur;
    } else {
      this._appService.CheckRunService.FilterProvDictOnIncrease(1);
      this._appService.CheckRunService.currentPage = 1;
    }
  }

  FormValidateAll(provID: string) {
    let tempArr: CheckrunModel[] = [];
    let va = this.cEntireClaimOptions.get('validateAll').value;
    if (va === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        if (ul.providerID === provID) {
          ul.validated = true;
          if (ul.excluded === true) {
            ul.excluded = false;
          }
        }
      });
      let line = this._appService.CheckRunService._patientsPerProvider[provID];
      line.forEach((el) => {
        el.validated = true;
        if (el.excluded === true) {
          el.excluded = false;
        }
        tempArr.push(el);
        if ((el.rejected === false) && (el.excluded === false)) {
          this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimPaid);
          this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
        }
      });

      this._appService.CheckRunService.ValidateClaim(tempArr);
    } else {
      if (va === false) {
        this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
          if (ul.providerID === provID) {
            ul.validated = false;
          }
        });
        let line = this._appService.CheckRunService._patientsPerProvider[provID];
        line.forEach((el) => {
          el.validated = false;
          if ((el.rejected === false) && (el.excluded === false)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimPaid);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
          tempArr.push(el);
        });
      }
      this._appService.CheckRunService.ValidateClaim(tempArr);
    }
  }

  FormExcludeAll(provID: string) {
    let ea = this.cEntireClaimOptions.get('excludeAll').value;
    let tempArr: CheckrunModel[] = [];
    if (ea === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        if (ul.providerID === provID) {
          ul.excluded = true;
          ul.validated = false;
        }
      });
      let line = this._appService.CheckRunService._patientsPerProvider[provID];
      line.forEach((el) => {
        el.rejected = false;
        el.excluded = true;
        tempArr.push(el);
        if ((el.validated === true)) {
          this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimPaid);
          this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
        }
        if (el.validated === true) {
          el.validated = false;
        }
      });
      this._appService.CheckRunService.ExcludeClaim(tempArr);
    } else {
      if (ea === false) {
        this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
          if (ul.providerID === provID) {
            if (ul.excluded === true) {
              ul.excluded = false;
            }
          }
        });
        let line = this._appService.CheckRunService._patientsPerProvider[provID];
        line.forEach((el) => {
          if (el.excluded === true) {
            el.excluded = false;
            tempArr.push(el);
            if ((el.validated === true) && (el.rejected === false)) {
              this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimPaid);
              this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
            }
          }
        });
      }
      this._appService.CheckRunService.ExcludeClaim(tempArr);
    }
  }

  FormRejectAll(provID: string) {
    this.rejectionReason.claimRejectionNotes.reset();
    let tempArr: CheckrunModel[] = [];
    let ra = this.cEntireClaimOptions.get('rejectAll').value;
    if (ra === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        if (ul.providerID === provID) {
          ul.rejected = true;
          ul.excluded = !ul.rejected;
        }
      });
      let line = this._appService.CheckRunService._patientsPerProvider[provID];
      line.forEach((el) => {
        el.rejected = true;
        el.reverse = 0;
        this.rejectedClaims.push({
          claimNo: el.claimNumber,
          amountPaid: el.amountOfClaimPaid,
          memberNo: el.patientMembNo,
          productCode: el.productCode,
          total: el.amountOfClaimApproved
        });
        if (el.validated === true) {
          this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimPaid);
          this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
        }
        el.amountOfClaimPaid = 0;
        el.note = this._appService.CheckRunService._rejectionNote;
        tempArr.push(el);
      });
      this._appService.CheckRunService.RejectClaim(tempArr);
    } else {
      if (ra === false) {
        this._appService.CheckRunService._rejectionNote = '';
        this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
          if (ul.providerID === provID) {
            if (ul.rejected === true) {
              ul.rejected = false;
            }
          }
        });
        let line = this._appService.CheckRunService._patientsPerProvider[provID];
        line.forEach((el) => {
          if (el.rejected === true) {
            el.rejected = false;
            this.rejectedClaims.forEach((rc) => {
              if ((el.claimNumber === rc.claimNo) && (el.patientMembNo === rc.memberNo) && (el.productCode === rc.productCode) && (el.amountOfClaimApproved === rc.total)) {
                el.amountOfClaimPaid = rc.amountPaid;
              }
            });
            tempArr.push(el)
            if ((el.validated === true) && (el.excluded === false)) {
              this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimPaid);
              this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
            }
          }
        });
      }
      this._appService.CheckRunService.RejectClaim(tempArr);
    }
  }

  FormValidateLine(rowId: string, membNo: string, claimNo: string, provId: string, amountapp: string, amountPaid: string, code: string) {
    let tempArr: CheckrunModel[] = [];
    let vd = this.cClaimOptions.get('validateLine').value;
    if (vd === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === provId) {
          up.validated = true;
        }
      });
      let y = this._appService.CheckRunService._patientsPerProvider[provId]
      y.forEach((el) => {
        if ((el.patientMembNo === membNo) && (el.claimNumber === claimNo)) {
          if (el.excluded === true) {
            el.excluded = false;
          }
        }
        if ((el.patientMembNo === membNo) && (el.claimNumber === claimNo) &&
          (el.amountOfClaimReported === amountapp) && (el.amountOfClaimPaid === amountPaid) && (el.productCode === code) && (el.rowId === rowId)) {
          el.validate = true;
          el.validated = true;
          if (el.excluded === true) {
            el.excluded = false;
          }
          tempArr.push(el);
          if ((el.excluded === false) && (el.rejected === false)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimApproved);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
        }

      });
      this._appService.CheckRunService.ValidateClaim(tempArr);
    } else {
      this._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === provId) {
          up.validated = false;
        }
      });
      let y = this._appService.CheckRunService._patientsPerProvider[provId]
      y.forEach((el) => {
        if ((el.patientMembNo === membNo) && (el.claimNumber === claimNo) && (el.amountOfClaimReported === amountapp) && (el.amountOfClaimPaid === amountPaid) && (el.productCode === code) && (el.rowId === rowId)) {
          el.validated = false;
          el.validate = false;
          tempArr.push(el);
          if ((el.excluded === false) && (el.rejected === false) && (this._appService.CheckRunService._totalAmountToPay !== 0.00)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimApproved);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
        }

      });
      this._appService.CheckRunService.ValidateClaim(tempArr);

    }
  }

  FormExcludeLine(membNo: string, claimNo: string, provId: string) {

    let ed = this.cClaimOptions.get('excludeLine').value;
    let tempArr: CheckrunModel[] = [];
    if (ed === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === provId) {
          up.excluded = true;
        }
      });
      this.openWarn = true;
      let y = this._appService.CheckRunService._patientsPerProvider[provId]
      y.forEach((el) => {
        if ((el.patientMembNo == membNo) && (el.claimNumber === claimNo)) {
          el.excluded = true;
          tempArr.push(el);
          if ((el.validated === true) && (el.rejected === false) && (this._appService.CheckRunService._totalAmountToPay !== 0.00)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimApproved);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
          if (el.validated === true) {
            el.validated = false;
          }
        }



      });
      this._appService.CheckRunService.ExcludeClaim(tempArr);
    } else {
      this._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === provId) {
          up.excluded = false;
        }
      });
      let y = this._appService.CheckRunService._patientsPerProvider[provId]
      y.forEach((el) => {
        if (el.claimNumber === claimNo) {
          el.excluded = false;
          tempArr.push(el);
          if ((el.validated === true) && (el.rejected === false)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimApproved);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
        }
      });
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        if (ul.providerID === provId) {
          if (ul.excluded === true) {
            ul.excluded = false;
          }
        }
      });
      this._appService.CheckRunService.ExcludeClaim(tempArr);
    }
  }

  FormRejectLine(claimNo: string, membNo: string, provId: string, amountPaid: string, amountapp: string, tblRowId: string) {
    this.rejectionReason.claimRejectionNotes.reset();

    let rd = this.cClaimOptions.get('rejectLine').value;
    let temparr: CheckrunModel[] = [];
    if (rd === true) {
      this._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === provId) {
          up.rejected = true;
        }
      });
      let y = this._appService.CheckRunService._patientsPerProvider[provId]
      y.forEach((el) => {
        if ((el.patientMembNo === membNo) && (el.claimNumber === claimNo) && (el.amountOfClaimReported === amountapp) && (el.amountOfClaimPaid === amountPaid) && (el.rowId === tblRowId)) {
          el.rejected = true;
          el.reverse = 0;

          this.rejectedClaims.push({
            claimNo: el.claimNumber,
            amountPaid: el.amountOfClaimPaid,
            memberNo: el.patientMembNo,
            productCode: el.productCode,
            total: el.amountOfClaimApproved
          });
          if ((el.validated === true) && (this._appService.CheckRunService._totalAmountToPay !== 0.00)) {
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay - parseFloat(el.amountOfClaimPaid);
            this._appService.CheckRunService._totalAmountToPay = parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
          }
          el.amountOfClaimPaid = 0;
          el.note = this._appService.CheckRunService._rejectionNote;
          el.rejReason = this._appService.CheckRunService._rejectionReason;
          temparr.push(el);
        }

      });
      this._appService.CheckRunService.RejectClaim(temparr);
    }
  }

  RejectClaimPopUp(method: string, mainNo: string, membNo: string, id: string, claimNo: string, provID: string, amountPaid: string, amountApp: string) {
    this._appService.CheckRunService._rowId = id;
    //console.log(this._appService.CheckRunService._rowId);
    this.rejectionReason.claimRejectionNotes.reset();
    this._cancelRejectionMethod = method;
    this.mainMembNo = mainNo;
    this.membNo = membNo;
    this.id = id;
    this.claimNo = claimNo;
    this.amountPaid = amountPaid;
    this.amountApp = amountApp
    this.providerID = provID;
    if (method === 'all') {
      this._appService.CheckRunService._uniqueProvList.forEach((fl) => {
        if (fl.providerID === this._providerID) {
          fl.rejected = true;
        }
      });

      let ra = this.cEntireClaimOptions.get('rejectAll').value;
      if (ra === true) {
        this.mainMembNo = mainNo;
        this.membNo = membNo;
        this.id = id;
        this.claimNo = claimNo;
        this.currentMethod = method;
        this.rejectModalShow = true;
      } else {
        this.mainMembNo = mainNo;
        this.membNo = membNo;
        this.id = id;
        this.claimNo = claimNo;
        this.currentMethod = method;
        this.rejectModalShow = false;
        this.CancelRejection();
      }
    }
    if (method === 'line') {
      let rl = this.cClaimOptions.get('rejectLine').value;
      if (rl === true) {
        this.mainMembNo = mainNo;
        this.membNo = membNo;
        this.id = id;
        this.claimNo = claimNo;
        this.currentMethod = method;
        this.rejectModalShow = true;
      } else {
        if ((this.amountPaid == '0.00') || (this.amountPaid == '0,00') || (this.amountPaid == '0')) {
          this.mainMembNo = mainNo;
          this.membNo = membNo;
          this.id = id;
          this.claimNo = claimNo;
          this.currentMethod = method;
          this.rejectModalShow = false;
          this.CancelRejection();
        }

      }
    }
  }

  Rejection() {
    this.rejectModalShow = false;
    if (this._currentMethod === 'all') {
      this.FormRejectAll(this._providerID);
    }
    if (this._currentMethod === 'line') {
      this.FormRejectLine(this._claimNo, this._membNo, this._providerID, this._amountPaid, this._amountApp, this._id);
    }

  }

  CancelModalRejection() {
    this._appService.CheckRunService._rejectionNote = '';
    //console.log(this._providerID);
    // console.log(this._membNo);
    this._appService.CheckRunService._uniqueProvList.forEach((up) => {
      if (up.providerID === this._providerID) {
        up.rejected = false;
      }
    });
    let tempArr: CheckrunModel[] = [];
    let pat = this._appService.CheckRunService._patientsPerProvider[this._providerID];
    pat.forEach((patLine) => {
      if ((patLine.patientMembNo === this._membNo) && (patLine.rowId == this._id)) {
        //console.log(patLine);
        patLine.rejected = false;
      }
    });
    this.rejectModalShow = false;
  }

  CancelRejection() {
    let p = this;
    if (this._cancelRejectionMethod === 'all') {
      let tempArr: CheckrunModel[] = [];
      this._appService.CheckRunService._rejectionNote = '';
      let line = this._appService.CheckRunService._patientsPerProvider[this._providerID];
      line.forEach((el) => {
        el.rejected = false;
        el.reverse = 1;
        this.rejectedClaims.forEach((rc) => {
          if ((el.claimNumber === rc.claimNo) && (el.patientMembNo === rc.memberNo) && (el.productCode === rc.productCode) && (el.amountOfClaimReportedd === rc.total)) {
            el.amountOfClaimPaid = rc.amountPaid;
          }
        });
        tempArr.push(el);

        if ((el.validated === true) && (el.excluded === false)) {
          this._appService.CheckRunService._totalAmountToPay =
            this._appService.CheckRunService._totalAmountToPay + parseFloat(el.amountOfClaimApproved);
          this._appService.CheckRunService._totalAmountToPay =
            parseFloat(this._appService.CheckRunService._totalAmountToPay.toFixed(2));
        }
      });
      this._appService.CheckRunService._uniqueList.forEach((ul) => {
        if (ul.mainPolicyNumber === this._mainMembNo) {
          ul.rejected = false;
        }
      });
      this._appService.CheckRunService.RejectClaim(tempArr);
    }
    let g = this;
    if (p._cancelRejectionMethod === 'line') {
      p._appService.CheckRunService._rejectionNote = '';
      p._appService.CheckRunService._uniqueProvList.forEach((up) => {
        if (up.providerID === p._providerID) {
          up.rejected = false;
        }
      });
      let tempArr: CheckrunModel[] = [];
      let pat = p._appService.CheckRunService._patientsPerProvider[p._providerID];
      pat.forEach((patLine) => {
        if ((patLine.patientMembNo === p._membNo) && (patLine.rowId === p._id)) {
          patLine.rejected = false;
          patLine.reverse = 1;
          p.rejectedClaims.forEach((rc) => {
            if ((patLine.claimNumber === rc.claimNo) && (patLine.patientMembNo === rc.memberNo) && (patLine.productCode === rc.productCode) && (patLine.amountOfClaimApproved === rc.total) && (patLine.rowId === p._id)) {
              patLine.amountOfClaimPaid = rc.amountPaid;
            }
          });
          tempArr.push(patLine);
          //if ((patLine.validated === true) && (patLine.excluded === false)) {
          //  p._appService.CheckRunService._totalAmountToPay = p._appService.CheckRunService._totalAmountToPay + parseFloat(patLine.amountOfClaimPaid);
          //  p._appService.CheckRunService._totalAmountToPay = parseFloat(p._appService.CheckRunService._totalAmountToPay.toFixed(2));
          //}

        }
      });
      this._appService.CheckRunService._uniqueList.forEach((ul) => {
        if (ul.mainPolicyNumber === this._mainMembNo) {
          if (ul.rejected === true) {
            ul.rejected = false;
          }
        }
      });
      //console.log(g.id);
      //  this._appService.CheckRunService._rowId = g.id;
      this._appService.CheckRunService._rejectProv = this._providerID;
      this._appService.CheckRunService._rejectmembNo = this._membNo;
      this._appService.CheckRunService.RejectClaim(tempArr);
    }
    this.rejectModalShow = false;

  }

  PayReady() {
    let value = this.cConfirmationForm.get('payBatch').value;
    if (value) {
      this._enableSubBtn = false;
    } else {

      this._enableSubBtn = true;
    }
    if (value) {
    } else {

    }

  }

  PayReadyAfterEdit() {

    this._appService.CheckRunService.openWarningPayReady = true;
    this._appService.CheckRunService.CheckIfAllClaimsAreValidated();


  }

  CancelMakeBatchReadyforPayment() {
    this._appService.CheckRunService.openWarningPayReady = false;
    this.cbordRowForm.controls['payBatch'].setValue(false);
    this.enableSubmit = false;
  }

  ValidateEntireBatch() {
    this._appService.CheckRunService._markReady++;
    if (this._appService.CheckRunService._markReady > 1) {
      this._appService.CheckRunService._markReady = 0;
    }
    if (this._appService.CheckRunService._markReady == 1) {
      this._appService.CheckRunService._totalAmountToPay = 0;
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        ul.validated = true;
        let y = this._appService.CheckRunService._patientsPerProvider[ul.providerID];
        y.forEach((pf) => {
          pf.validated = true;
          if ((pf.excluded === false) && (pf.rejected === false))
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(pf.amountOfClaimApproved);
        });
      });
      this._appService.CheckRunService.ValidateEntireBatch();
    }
    if (this._appService.CheckRunService._markReady == 0) {
      this._appService.CheckRunService._totalAmountToPay = 0;
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        ul.validated = false;
        let y = this._appService.CheckRunService._patientsPerProvider[ul.providerID];
        y.forEach((pf) => {
          pf.validated = false;
        });
      });
      this._appService.CheckRunService.UnvalidateEntireBatch();
    }
    /*let x: boolean;
    x = this.cbordRowForm.get('payReady').value;
    if (x) {
      this._appService.CheckRunService._totalAmountToPay = 0;
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        ul.validated = true;
        let y = this._appService.CheckRunService._patientsPerProvider[ul.providerID];
        y.forEach((pf) => {
          pf.validated = true;
          if ((pf.excluded === false) && (pf.rejected === false))
            this._appService.CheckRunService._totalAmountToPay = this._appService.CheckRunService._totalAmountToPay + parseFloat(pf.amountOfClaimApproved);
        });
      });
      this._appService.CheckRunService.ValidateEntireBatch();
    } else {
      this._appService.CheckRunService._totalAmountToPay = 0;
      this._appService.CheckRunService._uniqueProvList.forEach((ul) => {
        ul.validated = false;
        let y = this._appService.CheckRunService._patientsPerProvider[ul.providerID];
        y.forEach((pf) => {
          pf.validated = false;
        });
      });
      this._appService.CheckRunService.UnvalidateEntireBatch();
    }*/
  }

  GenerateUnpaidTotalForProvider(provId: string) {
    let x: number = parseFloat('0.00');
    let y = this._appService.CheckRunService._patientsPerProvider[provId];
    y.forEach((el) => {
      x = x + parseFloat(el.amountOfClaimReported);
    });
    return x;
  }

  GeneratePaidTotalForProvider(provId: string) {
    let x: number = +'0';
    let y = this._appService.CheckRunService._patientsPerProvider[provId];
    y.forEach((el) => {
      x = x + parseFloat(el.amountOfClaimPaid);

    });
    return x;
  }

  GenerareTotalAmountOfClaims(provID: string) {
    let x = this._appService.CheckRunService._patientsPerProvider[provID];
    this._appService.CheckRunService._totalClaimLines = 0;
    this._appService.CheckRunService._totalClaimLines = x.length;;
    return this._appService.CheckRunService._totalClaimLines;
  }

  GroupingChanged() {
    let p = this.cbordRowForm.get('grouping').value;
    if (p === 'Member') {
      this._appService.CheckRunService.showText = true;
      this._appService.CheckRunService._providerGrouping = false;
      this._appService.CheckRunService._scrollableView = false;
      this._appService.CheckRunService.currentView = "Member View";
    }
    if (p === 'Provider') {
      this._appService.CheckRunService._providerGrouping = true;
      this._appService.CheckRunService._scrollableView = false;
      this._appService.CheckRunService.currentView = "Provider View";
    }
    if (p === 'Scroll') {
      this._appService.CheckRunService._providerGrouping = false;
      this._appService.CheckRunService._scrollableView = true;
      this._appService.CheckRunService.currentView = "Scrollable View";
    }

  }

  public provspec: string;
  public specDesc: string;
  public provId: string;
  public provName: string;

  ViewDetailsOfClaim(claimType: string, provspec: string, specDesc: string, provName: string, provId: string, claimNo: string, rowID: string, membName: string, membNo: string, bank: string, accNr: string, branchcode: string) {

    this.provspec = provspec;
    this.specDesc = specDesc;
    this.provId = provId;
    this.provName = provName;
    this._bankingDetails = new BankingDetailModel();
    this._bankingDetails.accountNo = accNr;
    this._bankingDetails.bankName = bank;
    this._bankingDetails.branchCode = branchcode;
    this._bankingDetails.mainPolicyNo = membNo;
    if (this._appService.CheckRunService._paymentType == 'P') {
      this._displayProvbankingDetails = new BankingDetailModel();
      if ((this._bankingDetails.bankName !== "") && (this._bankingDetails.bankName !== undefined) && (this._bankingDetails.bankName !== null)) {
        let s: string = "";
        this._displayProvbankingDetails.bankName = s.padStart(this._bankingDetails.bankName.length, '*');
        s = "";
        this._displayProvbankingDetails.accountNo = s.padStart(this._bankingDetails.accountNo.length, '*');
        s = "";
        this._displayProvbankingDetails.branchCode = s.padStart(this._bankingDetails.branchCode.length, '*');
      }
    }
    this.memberName = membName;
    this.openDetailModel = true;
    this._appService.CheckRunService.membNo = membNo;
    this._appService.CheckRunService.details = new CheckRunDetailsModel();
    this._appService.CheckRunService.details.rowId = rowID;
    this._appService.CheckRunService.details.claimType = claimType;
    this._appService.CheckRunService.details.claimNo = claimNo;
    this._appService.CheckRunService.GetDetailsOfRefProv();

  }

  ExpandTree() {
    this.counter++;
    if (this.counter == 1) {
      this.expandtree = true;
    } else {
      this.counter = 0;
      this.expandtree = false;
    }
  }

  MakeBatchReadyforPayment() {
    this.enableSubmit = true;
    this._appService.CheckRunService.openWarningPayReady = false;
  }

  OpenBankEditModel(provId: string, mainMembNo: string, bank: string, accNr: string, branchcode: string) {
    this.provId = provId;
    this._appService.CheckRunService.currVenDetails.provId = provId;
    this.banking._component = "CHECKRUN";
    this.banking.bankinDetailsForm.reset();
    this._appService.CheckRunService._MembName = "";
    this._appService.CheckRunService._provId = "";
    this._appService.CheckRunService._provName = "";
    this._appService.CheckRunService.newBankinDetails = new BankingDetailModel();
    this._appService.CheckRunService.newBankinDetails.provId = provId;
    this._appService.CheckRunService.newBankinDetails.batchNo = this._appService.CheckRunService.batchNumber;
    this._appService.CheckRunService.bordRowList.forEach((bl) => {
      if ((bl.patientMembNo === mainMembNo) && (bl.provId === provId)) {
        this._appService.CheckRunService._MembName = bl.nameSurnamePrincipleMember;
        this._appService.CheckRunService.newBankinDetails.mainPolicyNo = bl.claimNumber;
        this._appService.CheckRunService._MembName = bl.nameSurnamePrincipleMember;
        this._appService.CheckRunService._MembName = this._appService.CheckRunService._MembName.padStart(this._appService.CheckRunService._MembName.length + 1)
        this._appService.CheckRunService._MembName = this._appService.CheckRunService._MembName.toLowerCase();
        for (var i = 0; i <= this._appService.CheckRunService._MembName.length; i++) {
          if (this._appService.CheckRunService._MembName.substr(i, 1) === " ") {
            let x = this._appService.CheckRunService._MembName.substr(i, 2);
            let y = x.toUpperCase();
            this._appService.CheckRunService._MembName = this._appService.CheckRunService._MembName.replace(x, y)
          }
        }
        this._appService.CheckRunService._provId = bl.provId;
        this._appService.CheckRunService._provName = bl.providerName;
        this._appService.CheckRunService._provName = this._appService.CheckRunService._provName.padStart(this._appService.CheckRunService._provName.length + 1)
        this._appService.CheckRunService._provName = this._appService.CheckRunService._provName.toLowerCase();
        for (var i = 0; i <= this._appService.CheckRunService._provName.length; i++) {
          if (this._appService.CheckRunService._provName.substr(i, 1) === " ") {
            let x = this._appService.CheckRunService._provName.substr(i, 2);
            let y = x.toUpperCase();
            this._appService.CheckRunService._provName = this._appService.CheckRunService._provName.replace(x, y)
          }
        }
      }
    });
    this._appService.CheckRunService.GetExistingBankDetails();
    this._appService.CheckRunService.GetVendorName(this._appService.CheckRunService.details.claimType, this._appService.CheckRunService.details.claimNo, this.provId, bank, accNr, branchcode);


  }

  InsertPayDate() {
    let date: string;
    date = this.cPayDateForm.get('payDate').value;
    let dateToday: Date = new Date;
    let enteredDate = new Date(date);

    if (enteredDate.toString().includes('Invalid Date')) {
      this.errMsgPayDate = 'Entered date is invalid.';
    } else {
      if (enteredDate <= dateToday) {
        let tomorrow = new Date(new Date().setDate(new Date().getDate() + 1));
        this.errMsgPayDate = 'Chosen date cannot be smaller than ' + tomorrow.toLocaleString();
        this.cPayDateForm.get('payDate').setErrors(this._errMsgPayDate);
      } else {
        this.errMsgPayDate = '';
        if (this._appService.CheckRunService._specificBatchNo === undefined) {
          this._appService.CheckRunService._specificBatchNo = new BatchDetailsModel();
          this._appService.CheckRunService._specificBatchNo.batchNumber = this._appService.CheckRunService.batchNumber;
          this._appService.CheckRunService._specificBatchNo.paydate = date;
          this._enableNextBtn = false;
        } else {
          this._appService.CheckRunService._specificBatchNo.paydate = date;
          this._enableNextBtn = false;
        }
      }

    }
  }

  OpenSearchBox() {
    this.searchBatch.batchSearch.reset();
    this.searchBatch.meberNo = '';
    this.searchBatch.providerNo = '';
    this._appService.CheckRunService.openEditBatch = true;
    this._appService.CheckRunService.enablesave = true;
  }

  SearchBatch() {
    this._appService.CheckRunService.FilterReleasedBatch(this.searchBatch.meberNo, this.searchBatch.providerNo);
    this.cClaimOptions.enable();
  }

  CancelSearchBatch() {
    this._appService.CheckRunService.openEditBatch = false;
    this._appService.CheckRunService.enablesave = false;
  }

  OpenSearchBoxUnreleased() {


    this.searchUnreleadBatch.batchSearch.reset();
    this.searchBatch.meberNo = '';
    this.searchBatch.providerNo = '';
    this.searchUnreleadBatch.membErrMsg = '';
    this.searchUnreleadBatch.provErrMsg = '';
    this._appService.CheckRunService.openUnEditBatch = true;
  }

  SearchUnreleasedBatch() {
    this._appService.CheckRunService.FilterUnReleasedBatch(this.searchUnreleadBatch.meberNo, this.searchUnreleadBatch.providerNo, this.searchUnreleadBatch.claimNo);
  }

  CancelSearchUnreleasedBatch() {
    this._appService.CheckRunService.openUnEditBatch = false;
  }

  SubmitWarning() {
    this.submitWarning = true;
  }

  async submitCheckrun() {
    this.completeWizard = false;
    this.submitWarning = false;
    await this._appService.CheckRunService.UpdatePaymentDate();
    await this._appService.CheckRunService.MarkBatchAsReadyToPay();
    if (!this._appService.CheckRunService.SubmitCheckrunError) {
      this.submitWarning = this._appService.CheckRunService.openWarningPayReady;
      this.enableSubBtn = true;
      this.cConfirmationForm.reset();
      this.cPayDateForm.reset();
    }
  }

  CancelSubmitCheckrun() {
    this.cbordRowForm.controls['payBatch'].setValue(false);
    this.submitWarning = false;
    this.enableSubmit = false;
  }

  ClickNext() {
    this._enableSubBtn = true;
  }

  CheckBankingDetails(provId: string): boolean {
    let x: boolean = false;
    let y = this._appService.CheckRunService._patientsPerProvider[provId];
    y.forEach((el) => {
      if ((el.bankAccountName !== "") && (el.bankAccountName !== undefined) && (el.bankAccountName !== null)) {
        x = false;
        return x;
      } else {
        x = true;
        return x;
      }
    });

    return x;
  }


  CheckForIncompleteBanking() {
    if (this._appService.CheckRunService._paymentType == 'P') {
      let nobankig: boolean = false;

      for (var i = 0; i < this._appService.CheckRunService._uniqueProvList.length; i++) {
        if ((this._appService.CheckRunService._uniqueProvList[i].hasBanking == false) && (this._appService.CheckRunService._uniqueProvList[i].excluded == false)) {
          nobankig = true;
          break;
        }
      }
      if (nobankig == true) {
        this._appService.CheckRunService.nobankingFound = true;
      } else {
        this.completeWizard = true;
      }
    } else {
      this.completeWizard = true;
    }

  }

}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bordrow-totals',
  templateUrl: './bordrow-totals.component.html',
  styleUrls: ['./bordrow-totals.component.css']
})
export class BordrowTotalsComponent implements OnInit {

  @Input() _totalApproved: number = 0;
  @Input() _totalPaid: number = 0;
  @Input() _totalClaimLines: number = 0;
  @Input() _totalDetailLines: number = 0;


  constructor() { }

  ngOnInit() {
  }

}

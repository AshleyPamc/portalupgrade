import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-reject-claim-popup',
  templateUrl: './reject-claim-popup.component.html',
  styleUrls: ['./reject-claim-popup.component.css']
})
export class RejectClaimPopupComponent implements OnInit {

  @Input() claimRejectionNotes: FormGroup;

  private _rejectExtra: boolean = false;


  public get rejectExtra(): boolean {
    return this._rejectExtra;
  }
  public set rejectExtra(value: boolean) {
    this._rejectExtra = value;
  }

  constructor(public fb: FormBuilder, public _appService: AppService) { }

  ngOnInit() {

    this.claimRejectionNotes = this.fb.group({
      reasons: [],
      other: [],
      customReason: []

    });
    this.claimRejectionNotes.get('customReason').disable();

  }


  defualtReason() {
    this._appService.CheckRunService._rejectionReason = '';
    this._appService.CheckRunService._rejectionReason = this.claimRejectionNotes.get('reasons').value;
    if (this._appService.CheckRunService._rejectionReason === '420 -Rejected By Scheme') {
      this.claimRejectionNotes.get('customReason').enable();
      this.rejectExtra = true;
    } else {
      this.claimRejectionNotes.get('customReason').disable();
      this.rejectExtra = false;
    }
  }

  CustomReason() {

    this._appService.CheckRunService._rejectionNote = '';
    this._appService.CheckRunService._rejectionNote = this.claimRejectionNotes.get('customReason').value;
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-banking-detail',
  templateUrl: './edit-banking-detail.component.html',
  styleUrls: ['./edit-banking-detail.component.css']
})
export class EditBankingDetailComponent implements OnInit {

  @Input() bankinDetailsForm: FormGroup;
  public _component: string = "";
  public openWarning = false;

  constructor(public _appService: AppService, public fb: FormBuilder) {
    this._appService.CheckRunService._filtteredList = [];

  }

  ngOnInit() {

    this.bankinDetailsForm = this.fb.group({
      bankName: ['', Validators.required],
      branchCode: ['', Validators.required],
      accountNo: ['', Validators.required],
      accType: ['', Validators.required]
    });

    let x = this.bankinDetailsForm.valid;
  }

  PopulateBankName() {
    if (this._component == 'CHECKRUN') {
      this._appService.CheckRunService.newBankinDetails.bankName = this.bankinDetailsForm.get("bankName").value;
    }
    if (this._component == 'CLAIMCAPTURE') {
      this._appService.ClaimCaptureService.newBankingDetails.bankName = this.bankinDetailsForm.get("bankName").value;
    }
  }

  PopulateBranchCode() {
    if (this._component == 'CHECKRUN') {
      this._appService.CheckRunService.newBankinDetails.branchCode = this.bankinDetailsForm.get("branchCode").value;
    }
    if (this._component == 'CLAIMCAPTURE') {
      this._appService.ClaimCaptureService.newBankingDetails.branchCode = this.bankinDetailsForm.get("branchCode").value;
    }

  }

  PopulateAccountNo() {
    if (this._component == 'CHECKRUN') {
      this._appService.CheckRunService.newBankinDetails.accountNo = this.bankinDetailsForm.get("accountNo").value;
    }
    if (this._component == 'CLAIMCAPTURE') {
      this._appService.ClaimCaptureService.newBankingDetails.accountNo = this.bankinDetailsForm.get("accountNo").value;
    }
  }

  OpenBankEditModel() {
    this.bankinDetailsForm.reset();
    this._appService.CheckRunService.changeBankModel = false;
    this._appService.ClaimCaptureService._openBanking = false;
    this.openWarning = false;
  }

  PopulateAccType() {
    if (this._component == 'CHECKRUN') {
      this._appService.CheckRunService.newBankinDetails.accType = this.bankinDetailsForm.get("accType").value;
    }
    if (this._component == 'CLAIMCAPTURE') {
      this._appService.ClaimCaptureService.newBankingDetails.accType = this.bankinDetailsForm.get("accType").value;

    }
  }

  ChangeBankingDetail() {
    this.openWarning = false;
    this.bankinDetailsForm.reset();
    if (this._component == 'CHECKRUN') {
      this._appService.CheckRunService.ChangeBankingDetail();
    }
    if (this._component == 'CLAIMCAPTURE') {
      this._appService.ClaimCaptureService.UpdateBankingDetails();
    }

  }


}

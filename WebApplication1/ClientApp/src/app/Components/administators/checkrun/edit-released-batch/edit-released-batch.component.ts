import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, ValidationErrors } from '@angular/forms';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-edit-released-batch',
  templateUrl: './edit-released-batch.component.html',
  styleUrls: ['./edit-released-batch.component.css']
})
export class EditReleasedBatchComponent implements OnInit {

  @Input() batchSearch: FormGroup;

  private _errMsg: ValidationErrors;
  private _membErrMsg: string = '';
  private _provErrMsg: string = '';
  private _claimErrMsg: string = '';
  private _meberNo: string = '';
  private _providerNo: string = '';
  private _claimNo: string = '';
  private _foundMember: boolean;
  private _foundclaim: boolean;
  private _foundProvider: boolean;




  constructor(public _appService: AppService, public fb: FormBuilder) {

    this._errMsg = {
      foundProv: 'Provider Number Entered is Correct',
      noProvFound: 'Incorrect Provider Number',
      foundMemb: 'Member Number Entered is Correct',
      noMembFound: 'Incorrect Member Number',
      foundClaim: 'Claim Number Entered is Correct',
      noClaimFound: 'Incorrect Claim Number'
    };

  }

  ngOnInit() {
    this.batchSearch = this.fb.group({
      membno: [],
      claim: [],
      provno: []
    });
  }

  public get membErrMsg(): string {
    return this._membErrMsg;
  }
  public set membErrMsg(value: string) {
    this._membErrMsg = value;
  }
  public get provErrMsg(): string {
    return this._provErrMsg;
  }
  public set provErrMsg(value: string) {
    this._provErrMsg = value;
  }

  public get claimErrMsg(): string {
    return this._claimErrMsg;
  }
  public set claimErrMsg(value: string) {
    this._claimErrMsg = value;
  }

  public get meberNo(): string {
    return this._meberNo;
  }
  public set meberNo(value: string) {
    this._meberNo = value;
  }
  public get providerNo(): string {
    return this._providerNo;
  }
  public set providerNo(value: string) {
    this._providerNo = value;
  }

  public get claimNo(): string {
    return this._claimNo;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }

  public get foundMember(): boolean {
    return this._foundMember;
  }
  public set foundMember(value: boolean) {
    this._foundMember = value;
  }

  public get foundclaim(): boolean {
    return this._foundclaim;
  }
  public set foundclaim(value: boolean) {
    this._foundclaim = value;
  }

  public get foundProvider(): boolean {
    return this._foundProvider;
  }
  public set foundProvider(value: boolean) {
    this._foundProvider = value;
  }


  ValidateMember() {
    this.foundMember = false;
    this.meberNo = this.batchSearch.get('membno').value;
    if ((this.meberNo !== '') && (this.meberNo !== null)) {
      this._appService.CheckRunService._uniqueProvList.forEach((pl) => {
        let y = this._appService.CheckRunService._patientsPerProvider[pl.providerID];
        y.forEach((pp) => {
          if (pp.mainPolicyNumber === this.meberNo) {
            this._foundMember = true;
          }
        });

      });

      if (this._foundMember === true) {
        this.membErrMsg = this._errMsg['foundMemb'];
        this._appService.CheckRunService.enableSearch = true;
      } else {
        this.membErrMsg = this._errMsg['noMembFound'];
        this.batchSearch.get('membno').setErrors(this._errMsg['noMembFound']);
        this._appService.CheckRunService.enableSearch = false;
      }
    }
  }

  ValidateProvider() {
    this.foundProvider = false;
    this.providerNo = this.batchSearch.get('provno').value;
    if ((this.providerNo !== '') && (this.providerNo !== null)) {
      this._appService.CheckRunService._uniqueProvList.forEach((pl) => {
        if (pl.providerID === this.providerNo) {
          this.foundProvider = true;
        }
      });

      if (this._foundProvider === true) {
        this.provErrMsg = this._errMsg['foundProv'];
        this._appService.CheckRunService.enableSearch = true;
      } else {
        this.provErrMsg = this._errMsg['noProvFound'];
        this.batchSearch.get('provno').setErrors(this._errMsg['noProvFound']);
        this._appService.CheckRunService.enableSearch = false;
      }
    }

  }


  ValidateClaimNo() {
    this.foundclaim = false;
    this.claimNo = this.batchSearch.get('claim').value;
    if ((this.claimNo !== '') && (this.claimNo !== null)) {
      this._appService.CheckRunService._uniqueProvList.forEach((pl) => {
        let y = this._appService.CheckRunService._patientsPerProvider[pl.providerID];
        y.forEach((pp) => {
          if (pp.claimNumber === this.claimNo) {
            this._foundclaim = true;
          }
        });

      });

      if (this._foundclaim === true) {
        this._claimErrMsg = this._errMsg['foundClaim'];
        this._appService.CheckRunService.enableSearch = true;
      } else {
        this._claimErrMsg = this._errMsg['noClaimFound'];
        this.batchSearch.get('claim').setErrors(this._errMsg['noClaimFound']);
        this._appService.CheckRunService.enableSearch = false;
      }
    }
  }
}

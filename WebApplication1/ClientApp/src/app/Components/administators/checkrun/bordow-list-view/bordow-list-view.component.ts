import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-bordow-list-view',
  templateUrl: './bordow-list-view.component.html',
  styleUrls: ['./bordow-list-view.component.css']
})
export class BordowListViewComponent implements OnInit {

  private _displaySpinner: boolean = false;


  constructor(public _appService: AppService, private readonly joyrideService: JoyrideService) { }

  ngOnInit() {
  }

  public get displaySpinner(): boolean {
    return this._displaySpinner;
  }
  public set displaySpinner(value: boolean) {
    this._displaySpinner = value;
  }

  generateChoosenBordRow(batch: string, batchtype: string) {
    this._appService.CheckRunService._largeAmountFound = false;
    this._appService.CheckRunService.enableSearch = false;
    this._appService.CheckRunService._paymentType = batchtype;
    let x: boolean;
    this._appService.CheckRunService._unreleasedBatchesType.forEach((el) => {
      if (batch === el.batchNo) {
        this._appService.CheckRunService.hideReleaseOpt = true;
      }
    });
    this._appService.CheckRunService._unpaidBatchesType.forEach((pl) => {
      if (batch === pl.batchNo) {
        this._appService.CheckRunService.hideReleaseOpt = false;
      }
    });
    this._appService.CheckRunService._displayFetchModel = true;
    this._appService.CheckRunService.GenerateBordRowWithSpecificBatchNo(batch);
  }

  GenerateBordRowFile() {
    this._appService.CheckRunService._largeAmountFound = false;
    this._appService.CheckRunService.hideReleaseOpt = true;
    this._appService.CheckRunService.displayData = true;
    this.displaySpinner = false;
    this._appService.CheckRunService.readyToGenerate = true;
    this._appService.CheckRunService.benefitGridData.splice(0, this._appService.CheckRunService.benefitGridData.length);
    this._appService.CheckRunService.gridData.splice(0, this._appService.CheckRunService.gridData.length);
    this._appService.CheckRunService.totalBenClaims = 0;
    this._appService.CheckRunService.totalbenprov = 0;
    this._appService.CheckRunService.tottalbenmen = 0;
    this._appService.CheckRunService.totalClaims = 0;
    this._appService.CheckRunService.totalMembPayments = 0;
    this._appService.CheckRunService.totalProvPayments = 0;
    this._appService.CheckRunService._wizard = true;
    this._appService.CheckRunService._wizardNext = false;
    this._appService.CheckRunService._wizardpagecount = 1;
    this._appService.CheckRunService.excludingClaims = [];
  }


  onClick() {

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['list_1', 'list_2', 'list_3', 'list_4', 'list_5'],
        themeColor: '#313131'
      })
    }

  }

}

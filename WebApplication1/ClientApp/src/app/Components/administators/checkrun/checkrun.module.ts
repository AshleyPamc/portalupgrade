import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CheckrunComponent } from './checkrun.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { UserGuideComponent } from "./user-guide/user-guide.component";
import { ProviderViewComponent } from "./provider-view/provider-view.component";
import { BordowListViewComponent } from './bordow-list-view/bordow-list-view.component';
import { BordrowTotalsComponent } from './bordrow-totals/bordrow-totals.component';
import { JoyrideModule } from 'ngx-joyride';
import { EditReleasedBatchComponent } from './edit-released-batch/edit-released-batch.component';
import { SharedModule } from './../../../shared.module';

const routes: Routes = [{

  path: '',
  component: CheckrunComponent,
  children: [
    { pathMatch: 'full', path: 'checkrun', component: CheckrunComponent }
  ]

}];

@NgModule({
  declarations: [CheckrunComponent, UserGuideComponent,
    BordowListViewComponent, ProviderViewComponent, BordrowTotalsComponent, EditReleasedBatchComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    JoyrideModule.forRoot(),
    ReactiveFormsModule,
    ClarityModule,
    SharedModule
    
  ]
})
export class CheckrunModule { }

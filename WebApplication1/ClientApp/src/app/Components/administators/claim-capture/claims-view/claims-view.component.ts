import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-claims-view',
  templateUrl: './claims-view.component.html',
  styleUrls: ['./claims-view.component.css']
})
export class ClaimsViewComponent implements OnInit {


  constructor(public _appService: AppService) { }

  ngOnInit() {
  }


}

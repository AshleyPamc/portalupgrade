import { Component, OnInit, Input } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Recordtype8Model } from './../../../../models/Recordtype8Model';

@Component({
  selector: 'app-rejection-codes',
  templateUrl: './rejection-codes.component.html',
  styleUrls: ['./rejection-codes.component.css']
})
export class RejectionCodesComponent implements OnInit {

  @Input() cRejectCodeForm: FormGroup;

  constructor(public _appService: AppService, public fb: FormBuilder) { }

  ngOnInit() {

    this.cRejectCodeForm = this.fb.group({
      rejectCode: [],
      description: []
    });

  }

  rejectChange() {
    console.log("reject change");
    let x = this.cRejectCodeForm.get('rejectCode').value;
    if (x !== "" && x !== undefined && x !== null) {
      this._appService.ClaimCaptureService.defualtCodesList.forEach((el) => {
        if (el.code === x) {
          this.cRejectCodeForm.controls['description'].setValue(el.notes);
          this._appService.ClaimCaptureService.rejectCode.ADJCODE = x;
          this._appService.ClaimCaptureService.rejectCode.COMMENT = this.cRejectCodeForm.get('description').value;
        }
      });
    }

  }

  ClearAdjustCode() {
    let y: number;
    this.cRejectCodeForm.reset();
    this._appService.ClaimCaptureService.rejectCode = new Recordtype8Model();
  }
}

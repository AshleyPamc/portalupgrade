import { Component, OnInit, Input } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-validation',
  templateUrl: './user-validation.component.html',
  styleUrls: ['./user-validation.component.css']
})
export class UserValidationComponent implements OnInit {

  @Input() userValidatedInfo: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder) { }

  ngOnInit() {

    this.userValidatedInfo = this.fb.group({
      checked: ['', [Validators.required]]
    });


  }
  UserValidated() {
    this._appService.ClaimCaptureService.enableWizardNext = this.userValidatedInfo.get('checked').value;
  }
}

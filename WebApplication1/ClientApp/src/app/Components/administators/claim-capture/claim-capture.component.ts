import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { ClrWizard } from '@clr/angular';
import { ClaimHeaderComponent } from './claim-header/claim-header.component';
import { Recordtype8Model } from './../../../models/Recordtype8Model';
import { Recordtype1Model } from './../../../models/Recordtype1Model';
import { Recordtype2Model } from './../../../models/Recordtype2Model';
import { Recordtype5Model } from './../../../models/Recordtype5Model';
import { DiagnosticCodesComponent } from './diagnostic-codes/diagnostic-codes.component';
import { ClaimDetailComponent } from './claim-detail/claim-detail.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
//import { ClaimReport } from './../../../models/ClaimReport';
import { FormGroup, FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { EditBankingDetailComponent } from './../checkrun/edit-banking-detail/edit-banking-detail.component';
import { BankingDetailModel } from './../../../models/BankingDetailModel';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-claim-capture',
  templateUrl: './claim-capture.component.html',
  styleUrls: ['./claim-capture.component.css']
})
export class ClaimCaptureComponent implements OnInit {

  @ViewChild('wizard', { static:false }) claimsWizard: ClrWizard;
  @ViewChild('diagComponent', {static: false}) diagComponent: DiagnosticCodesComponent;
  @ViewChild('validation', {static:false}) userValidationCheck: UserValidationComponent;
  @ViewChild('claimHeader', {static : false}) claimHeader: ClaimHeaderComponent;
  @ViewChild('ClaimDetailLines', {static:false}) claimDetailLines: ClaimDetailComponent;
  @ViewChild('banking', {static:false}) banking: EditBankingDetailComponent;
  //@Input() ReportFilterForm: FormGroup;
  //private ReportFilterForm: FormGroup;

  //public _checkUniportant: boolean = true;
  //public errorMsgFrom: any;
  //public errorMsgTo: any;
  //public curPage: number = 1;
  //private _errMsg: ValidationErrors;
  //public filterCountDetails: number = 0;
  //public filterCountNet: number = 0;


  constructor(private _appService: AppService, private fb: FormBuilder, private readonly joyrideService: JoyrideService) {
    //this._errMsg = {
    //  required: 'This field is required!'
    //}
  }

  ngOnInit() {
    //this.ReportFilterForm = this.fb.group({
    //  //'createdFrom': ['', Validators.required],      
    //  //'createdTo': ['', Validators.required],
    //  //unimportant: []
    //  'createdFrom': [null, Validators.required],
    //  'createdTo': [null, Validators.required],
    //  'capturedBy': [null, Validators.required]
    //});
    //this.claimHeader.VerifiedProv();
   // this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'P'

  }


  //onClick() {

  //  if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
  //    this.joyrideService.startTour({
  //      steps: ['cc_1', 'cc_2'],
  //      themeColor: '#313131'
  //    })
  //  }
  //  if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
  //    if (this._appService.ClaimCaptureService._showReport) {
  //      this.joyrideService.startTour({
  //        steps: ['cr_1', 'cr_2', 'cr_3', 'cr_4', 'cr_5', 'cr_6'],
  //        themeColor: '#313131'
  //      })
  //    }
  //  }
  //}

  onClickReport() {
    this.joyrideService.startTour({
      steps: ['cr_1', 'cr_2', 'cr_3', 'cr_4', 'cr_5', 'cr_6'],
      themeColor: '#313131'
    })
  }

  WizardReload(buttonName: string) {
    if (buttonName === 'cancel') {
      this._appService.ClaimCaptureService.wizardOpen = false;
      this._appService.ClaimCaptureService.enableWizardNext = true;
      this._appService.ClaimCaptureService.fileCreated = false;
      this._appService.ClaimCaptureService.btnClick = false;
    }
    if (buttonName === 'finish') {
      this._appService.ClaimCaptureService.fileCreated = false;
     // this._appService.ClaimCaptureService.UpdateHeader();
    }
    this._appService.ClaimCaptureService.vedorFieldVissible = true;
    this.claimsWizard.reset();
   // this.claimHeader.cMasterForm.disable();
    this._appService.ClaimCaptureService.pageCount = 1;
    this._appService.ClaimCaptureService.diagnosticCodesList.splice(0, this._appService.ClaimCaptureService.diagnosticCodesList.length);
    this._appService.ClaimCaptureService.diagnosticCodesList = [];
    this._appService.ClaimCaptureService.claimDetailList.splice(0, this._appService.ClaimCaptureService.claimDetailList.length);
    this._appService.ClaimCaptureService.claimDetailList = [];
    this.diagComponent.diagCodes.reset();
    this._appService.ClaimCaptureService.btnClaimDetailDone = false;
    this.diagComponent.errorMsgFirstCode = "";
    this.diagComponent.errorMsgSecondCode = "";
    this.diagComponent.errorMsgThirdCode = "";
    this.diagComponent.errorMsgFourthCode = "";
    this._appService.ClaimCaptureService.notesLists = [];
    this._appService.ClaimCaptureService.billedAmount = 0;
    this._appService.ClaimCaptureService.rejectCode = new Recordtype8Model();
    this._appService.ClaimCaptureService.RejectList.splice(0, this._appService.ClaimCaptureService.RejectList.length);
    this._appService.ClaimCaptureService.RejectList = [];
    this._appService.ClaimCaptureService._disableBack = false;
    this._appService.ClaimCaptureService.currentClaimHeader = new Recordtype1Model();
    this.claimHeader.DisableOnCancel();
    this._appService.ClaimCaptureService.currentClaimDetail = new Recordtype2Model();
    this._appService.ClaimCaptureService.diagnosticCodes = new Recordtype5Model();
  }

  IncreasePage() {
    this._appService.ClaimCaptureService.WizardIncPageCount();
    let x = this._appService.ClaimCaptureService.pageCount;
    if (x == 5) {
      this.userValidationCheck.userValidatedInfo.reset();
    }
    if (x == 3) {
      if (this.diagComponent.diagCodes.valid) {
        this._appService.ClaimCaptureService.enableWizardNext = true;
      } else {
        this._appService.ClaimCaptureService.enableWizardNext = false;
      }
    }
  }

  DecreasePage() {
    this._appService.ClaimCaptureService.WizardDecPageCount();
    let x = this._appService.ClaimCaptureService.pageCount;
    if (x == 5) {
      this.userValidationCheck.userValidatedInfo.reset();
    }
  }

  OpenEditBankingDetails() {
    if (this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'M') {
      this.banking._component = "CLAIMCAPTURE";
      this._appService.CheckRunService._paymentType = 'M';
      this._appService.ClaimCaptureService._openBanking = true;
      this.banking.bankinDetailsForm.reset();
      this._appService.CheckRunService._MembName = "";
      this._appService.CheckRunService._provId = "";
      this._appService.CheckRunService._provName = "";
      this._appService.ClaimCaptureService.newBankingDetails = new BankingDetailModel();
      //this._appService.CheckRunService._MembName = this._appService.ClaimCaptureService.dataBaseMessageMemb; // Jaco comment out 2024-01-05
      this._appService.CheckRunService._provId = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB;
      this._appService.CheckRunService._provName = "";
    }
    if (this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'P') {
      this._appService.CheckRunService._paymentType = 'P';
      this.banking._component = "CLAIMCAPTURE";
      this._appService.ClaimCaptureService._openBanking = true;
      this.banking.bankinDetailsForm.reset();
      this._appService.CheckRunService._MembName = "";
      this._appService.CheckRunService._provId = "";
      this._appService.CheckRunService._provName = "";
      this._appService.ClaimCaptureService.newBankingDetails = new BankingDetailModel();
      this._appService.CheckRunService._provId = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
      this._appService.CheckRunService._provName = this._appService.ClaimCaptureService.dataBaseMessageProv;
    }

    this._appService.ClaimCaptureService._bankingPrompt = false;
  }

  //GetReport() {

  //  this.ReportFilterForm.reset();
  //  this._appService.ClaimCaptureService._ReportList.splice(0, this._appService.ClaimCaptureService._ReportList.length);
  //  this._appService.ClaimCaptureService._ReportListTemp.splice(0, this._appService.ClaimCaptureService._ReportListTemp.length);
  //  if (this._appService.ClaimCaptureService.HpCodesList.length > 1) {
  //    let claim: ClaimReport = new ClaimReport();
  //    claim.hpcode = '';
  //    let x = this._appService.ClaimCaptureService.HpCodesList.length - 1
  //    for (var i = 0; i < this._appService.ClaimCaptureService.HpCodesList.length; i++) {
  //      if (i == x) {
  //        claim.hpcode = claim.hpcode + this._appService.ClaimCaptureService.HpCodesList[i].code;
  //      } else {
  //        claim.hpcode = claim.hpcode + this._appService.ClaimCaptureService.HpCodesList[i].code + ';';
  //      }
  //    }

  //    this._appService.ClaimCaptureService.GetAllClaims(claim);
  //  } else {
  //    let claims: ClaimReport = new ClaimReport();
  //    claims.hpcode = this._appService.ClaimCaptureService.HpCodesList[0].code;
  //    this._appService.ClaimCaptureService.GetAllClaims(claims);
  //  }
  //}

  /* toggelShowClaims() {
     let y: Date;
     let x: Date;
     let z: ClaimReport[];
 
     x = new Date(this.ReportFilterForm.get("createdFrom").value);
     y = new Date(this.ReportFilterForm.get("createdTo").value);
 
     this._appService.ClaimCaptureService._ReportList = [];
 
     if (this.ReportFilterForm.get("unimportant").value == true) {
       this._appService.ClaimCaptureService._ReportList = [];
       this._appService.ClaimCaptureService._ReportListTemp.forEach((el) => {
         this._appService.ClaimCaptureService._ReportList.push(el);
       });
 
     } else {
       this._appService.ClaimCaptureService._ReportList = [];
       this._appService.ClaimCaptureService._ReportListTemp.filter(
         (unimportant) => {
           return unimportant.batchNo !== "";
         }
 
       ).forEach((elf) => {
         this._appService.ClaimCaptureService._ReportList.push(elf);
       })
     }
   }*/

  //ApplyFilter() {
  //  let y: Date;
  //  let x: Date;
  //  let z: ClaimReport[];
  //  let a: boolean;
  //  let dateCreate: Date;
  //  let b: string;
  //  let c: string;

  //  a = this.ReportFilterForm.get("unimportant").value;
  //  x = new Date(this.ReportFilterForm.get("createdFrom").value);
  //  y = new Date(this.ReportFilterForm.get("createdTo").value);

  //  b = this.ReportFilterForm.get("createdFrom").value;
  //  c = this.ReportFilterForm.get("createdTo").value;


  //  if ((b == null) || (c == null) || (b == "") || (c == "") || (b == undefined) || (c == undefined)) {
  //    if ((b == null) || (b == "") || (b == undefined)) {
  //      this.ReportFilterForm.get("createdFrom").markAsDirty();
  //      this.ReportFilterForm.get("createdFrom").markAsTouched();
  //      this.errorMsgFrom = this._errMsg['required'];
  //      this.ReportFilterForm.get("createdFrom").setErrors(this.errorMsgFrom);
  //    }
  //    if ((c == null) || (c == "") || (c == undefined)) {
  //      this.ReportFilterForm.get("createdTo").markAsTouched();
  //      this.ReportFilterForm.get("createdTo").markAsDirty();
  //      this.errorMsgTo = this._errMsg['required'];
  //      this.ReportFilterForm.get("createdTo").setErrors(this.errorMsgTo);
  //    }

  //  } else {
  //    this._appService.ClaimCaptureService._ReportList.splice(0, this._appService.ClaimCaptureService._ReportList.length);
  //    if (a === false) {
  //      this._appService.ClaimCaptureService._ReportListTemp.filter(
  //        (unimportant) => {
  //          dateCreate = new Date(unimportant.dateCreated);
  //          if ((dateCreate >= x) && (dateCreate <= y) && (unimportant.batchNo == "")) {
  //            return unimportant
  //          }
  //        }
  //      ).forEach((elf) => {
  //        this._appService.ClaimCaptureService._ReportList.push(elf);
  //      });
  //    } else {
  //      this._appService.ClaimCaptureService._ReportListTemp.filter(
  //        (unimportant) => {
  //          dateCreate = new Date(unimportant.dateCreated);
  //          let d = dateCreate.getDate();
  //          let e = x.getDate();
  //          let f = y.getDate();
  //          if ((dateCreate >= x) && (dateCreate <= y)) {
  //            return unimportant
  //          }
  //        }
  //      ).forEach((elf) => {
  //        this._appService.ClaimCaptureService._ReportList.push(elf);
  //      });
  //    }
  //    let page = 1;
  //    let count = 1;
  //    this._appService.ClaimCaptureService._ReportList.forEach(el => {
  //      if (count == 20) {
  //        page++;
  //        count = 1;
  //      }
  //      count++
  //      el.page = page;
  //    })
  //    this._appService.ClaimCaptureService.tempList = [];
  //    this._appService.ClaimCaptureService._ReportList.forEach((el) => {
  //      if (el.page == 1) {
  //        this._appService.ClaimCaptureService.tempList.push(el);
  //      }
  //    });
  //    this.curPage = 1;
  //    this._appService.ClaimCaptureService.claimReportTotalPages = page;
  //      //  this._appService.ClaimCaptureService.tempList = this._appService.ClaimCaptureService._ReportList;
  //  }
  //}

  //RemoveFilter() {
  //  this._appService.ClaimCaptureService._ReportList.splice(0, this._appService.ClaimCaptureService._ReportList.length);
  //  this._appService.ClaimCaptureService._ReportListTemp.forEach((el) => {
  //    this._appService.ClaimCaptureService._ReportList.push(el);
  //  });
  //  this.ReportFilterForm.get("unimportant").setValue(true);
  //  this.ReportFilterForm.get("createdFrom").reset();
  //  this.ReportFilterForm.get("createdTo").reset();
  //  let page = 1;
  //  let count = 1;
  //  this._appService.ClaimCaptureService._ReportList.forEach(el => {
  //    if (count == 20) {
  //      page++;
  //      count = 1;
  //    }
  //    count++
  //    el.page = page;
  //  })
  //  this._appService.ClaimCaptureService.tempList = [];
  //  this._appService.ClaimCaptureService._ReportList.forEach((el) => {
  //    if (el.page == 1) {
  //      this._appService.ClaimCaptureService.tempList.push(el);
  //    }
  //  });
  //  this._appService.ClaimCaptureService.claimReportTotalPages = page;
  //  this.curPage = 1;
  //}  
}

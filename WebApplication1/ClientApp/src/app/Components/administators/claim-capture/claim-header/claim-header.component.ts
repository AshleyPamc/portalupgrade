import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, ValidationErrors, FormBuilder, Validators } from '@angular/forms';
import { AppService } from './../../../../services/app.service';
import { Recordtype2Model } from './../../../../models/Recordtype2Model';
import { Recordtype1Model } from './../../../../models/Recordtype1Model';
import { EditBankingDetailComponent } from '../../checkrun/edit-banking-detail/edit-banking-detail.component';
import { BankingDetailModel } from './../../../../models/BankingDetailModel';
import { JoyrideService } from 'ngx-joyride';
import { MemberEligibilityModel } from './../../../../models/MemberEligibilityModel';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ProviderInfoModel } from './../../../../models/ProviderInfoModel';

@Component({
  selector: 'app-claim-header',
  templateUrl: './claim-header.component.html',
  styleUrls: ['./claim-header.component.css']
})
export class ClaimHeaderComponent implements OnInit {

  @Input() cMasterForm: FormGroup;


  private _errorMsgProvClaim: string;
  private _errorMsgEzAuth: any;
  private _errorMsgEzProv: any;
  private _ezProvSpec: string = ''; // Jaco 2024-03-04
  private _errorMsgMemb: any;
  private _errorMsgClaimType: any;
  private _errorMsgRefProv: any;
  //private _errorMsgDepMem: any;
  public _errorMsg: ValidationErrors;
  private _errorMsgRecdate: any;

  public _familyList: MemberEligibilityModel[] = [];
  //public _membValid: boolean = false;
  //public _membErrorMsg: string = '';
  public _familyOpt: string = '';
  public _depOpthruDt: string = '';

  constructor(private fb: FormBuilder, public _appService: AppService, private readonly joyrideService: JoyrideService) {
    this._appService.ClaimCaptureService._disableCancel = false;
    this._appService.ClaimCaptureService.btnClick = false;
    this._appService.ClaimCaptureService.enableWizardNext = false;
    

    this._errorMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      minLength: 'Field Length To Short!',
      familyNumber: 'Please complete a family number field first!',
      valid: 'valid',
      invalid: 'Invalid Format!'
    };

  }

  ngOnInit() {

    this.cMasterForm = this.fb.group({
      provclaim: ['', [Validators.required, Validators.maxLength(20)]],
      ezcapauth: ['', [Validators.maxLength(16)]],
      ezcapprov: ['', [Validators.required, Validators.maxLength(20)]],
      ezcapmemb: ['', [Validators.required, Validators.maxLength(20)]],
      ezcapdepmemb: ['', [Validators.required, Validators.maxLength(20)]],
      reprovider: ['', [Validators.maxLength(20)]],
      vendoid: [],
      PHC: [],
      PlaceSelect: [],
      CTOpt: [],
      HPCode: [],
      RecDateField: ['', Validators.required]
    });


    
    //this.cMasterForm.valueChanges.subscribe(
    //  (changes) => {
    //    this.ValidateForm(changes);
    //  });

    
     this.cMasterForm.disable();

    
    
  }

  public get errorMsgProvClaim(): string {
    return this._errorMsgProvClaim;
  }
  public set errorMsgProvClaim(value: string) {
    this._errorMsgProvClaim = value;
  }

  public get errorMsgEzAuth(): any {
    return this._errorMsgEzAuth;
  }
  public set errorMsgEzAuth(value: any) {
    this._errorMsgEzAuth = value;
  }

  public get errorMsgEzProv(): any {
    return this._errorMsgEzProv;
  }
  public set errorMsgEzProv(value: any) {
    this._errorMsgEzProv = value;
  }

  public get ezProvSpec(): string {  // Jaco 2024-03-04
    return this._ezProvSpec;
  }
  public set ezProvSpec(value: string) {
    this._ezProvSpec = value;
  }

  public get errorMsgMemb(): any {
    return this._errorMsgMemb;
  }
  public set errorMsgMemb(value: any) {
    this._errorMsgMemb = value;
  }

  public get errorMsgClaimType(): any {
    return this._errorMsgClaimType;
  }
  public set errorMsgClaimType(value: any) {
    this._errorMsgClaimType = value;
  }

  public get errorMsgRefProv(): any {
    return this._errorMsgRefProv;
  }
  public set errorMsgRefProv(value: any) {
    this._errorMsgRefProv = value;
  }

  //public get errorMsgDepMem(): any {
  //  return this._errorMsgDepMem;
  //}
  //public set errorMsgDepMem(value: any) {
  //  this._errorMsgDepMem = value;
  //}

  public get errorMsgRecdate(): any {
    return this._errorMsgRecdate;
  }
  public set errorMsgRecdate(value: any) {
    this._errorMsgRecdate = value;
  }
      
  public get familyList(): MemberEligibilityModel[] {
    return this._familyList;
  }
  public get familyOpt(): string {
    return this._familyOpt;
  }
  public get depOpthruDt(): string {
    return this._depOpthruDt;
  }


  ValidateForm(changes: any) {
    if (this.cMasterForm.get('ezcapprov').dirty === true) {
      this.ValidateEzcapProv();
    }

    if ((this.cMasterForm.get('ezcapmemb').dirty === true)) {
      //this.ValidateEzcapMemb();
      this.ValidateEzcapMembNew();
    }
    if ((this.cMasterForm.get('ezcapdepmemb').dirty === true)) {
      //this.ValidateEzcapDep();
      this.ValidateEzcapDepNew();
    }

    if (this.cMasterForm.get('reprovider').dirty === true) {
      this.ValidateRefProv();
    }

    if (this.cMasterForm.get('ezcapauth').dirty === true) {
      this.ValidateEzcapAuth();
    }
  }

  FieldValidation(fieldName: string, controlName: string) {
    if ((fieldName === undefined) || (fieldName === '') || (fieldName === null)) {
      this.cMasterForm.get(controlName).setErrors(this._errorMsg['required']);
      return this._errorMsg['required'];
    }
    if (fieldName.length > 20) {
      this.cMasterForm.get(controlName).setErrors(this._errorMsg['maxLength']);
      return this._errorMsg['maxLength'];
    }
    if ((controlName == 'ezcapprov') || (controlName == 'reprovider')) {
      if (fieldName.length > 7) {
        return this._errorMsg['maxLength']
      }
      if (fieldName.length < 7) {
        return this._errorMsg['minLength']
      }
      let x = parseInt(fieldName);

      if (x.toString() == 'NaN') {
        return this._errorMsg['invalid'];
      }
    }
    return this._errorMsg['valid'];
  }

  PHCODEChanged() {
    let y = this.cMasterForm.get('PHC').value.charAt(0);
    this._appService.ClaimCaptureService.currentClaimHeader.PHCODE = y;
  }

  ClaimTypeChanged() {
    this.cMasterForm.get('CTOpt').markAsTouched();
    let z = this.cMasterForm.get('CTOpt').value.charAt(0);
    this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE = z;
    let x = this.cMasterForm.get('CTOpt').value.charAt(0);
    let y = this.cMasterForm.get('ezcapmemb').value;
    if (x !== "M") {
      this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
      this._appService.ClaimCaptureService.vedorFieldVissible = true;
      this.cMasterForm.get('vendoid').enable();
    } else {
      //this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = 'M' + y; //Jaco comment out 2024-01-08
      this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._familyList.filter(x => x.dependant == this.cMasterForm.get('ezcapdepmemb').value)[0].subssn;

      this._appService.ClaimCaptureService.vedorFieldVissible = false;
      this.cMasterForm.get('vendoid').disable();
    }

    this._appService.ClaimCaptureService._bankingPrompt = true;
  }

  HpCodeChanged() {
    this._appService.ClaimCaptureService.currentClaimHeader.hpCode = this.cMasterForm.get('HPCode').value;
    this._appService.ClaimCaptureService.currentClaimDetail.hpCode = this._appService.ClaimCaptureService.currentClaimHeader.hpCode;
    this._appService.ClaimCaptureService.LoadPhCodes();
  }

  PlcSrvChanged() {
    let x = this.cMasterForm.get('PlaceSelect').value;
    this._appService.ClaimCaptureService.currentClaimHeader.PLACE = x;
  }

  VendIdChanged() {
    let x = this.cMasterForm.get('vendoid').value;
    this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = x;
  }

  ValidateProvClaim() {
    this._appService.ClaimCaptureService.currentClaimHeader.PROVCLAIM = this.cMasterForm.get('provclaim').value;
    this.errorMsgProvClaim = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.PROVCLAIM, 'provclaim');
    if (this.errorMsgProvClaim === 'valid') {
      this.errorMsgProvClaim = this.cMasterForm.get('provclaim').value;
    }
  }

  ValidateEzcapAuth() {
    this._appService.ClaimCaptureService.currentClaimHeader.EZCAPAUTH = this.cMasterForm.get('ezcapauth').value;
    this.errorMsgEzAuth = '';
  }

  //ValidateEzcapProv() { // Jaco comment out 2024-03-04
  //  this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV = this.cMasterForm.get('ezcapprov').value;
  //  this.errorMsgEzProv = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV, 'ezcapprov');
  //  if (this.errorMsgEzProv === 'valid') {
  //    this.errorMsgEzProv = this._appService.ClaimCaptureService.DataBaseCheckEzcapProv();
  //    setTimeout((e) => {
  //      this.errorMsgEzProv = this._appService.ClaimCaptureService.dataBaseMessageProv;
  //      let v = this.errorMsgEzProv;
  //      if (this.errorMsgEzProv !== 'Practice Number is incorrect') {
  //        this.cMasterForm.get('ezcapprov').setErrors(null);
  //        this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
  //        this._appService.ClaimCaptureService.vedorFieldVissible = true;
  //        this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
  //      } else {
  //        this.errorMsgEzProv = '';
  //        this.errorMsgEzProv = this.cMasterForm.get('ezcapprov').value;
  //        this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
  //        this._appService.ClaimCaptureService.vedorFieldVissible = false;
  //      }
  //      this.cMasterForm.get('ezcapprov').markAsPristine();
  //    },300)

  //  } else {
  //    this.cMasterForm.get('ezcapprov').setErrors(this.errorMsgEzProv);
  //  }
  //}

  ValidateEzcapProv() {  // Jaco 2024-03-04

    // Trim provid that user entered
    this.cMasterForm.get('ezcapprov').setValue(this.cMasterForm.get('ezcapprov').value.trim());

    // Reset speciality valriable for control-helper
    this.ezProvSpec = '';

    // Set global
    this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV = this.cMasterForm.get('ezcapprov').value;

    // Manual validation of dat entered
    this.errorMsgEzProv = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV, 'ezcapprov');

    // Proceed to API call if valid data entered
    if (this.errorMsgEzProv != 'valid') {

      this.cMasterForm.get('ezcapprov').setErrors(this.errorMsgEzProv);
    }
    else {

      this._appService.ClaimCaptureService._busy = true;

      this._appService.ClaimCaptureService.DataBaseCheckEzcapProv().subscribe(
        (result: ProviderInfoModel) => {
               
          this._appService.ClaimCaptureService.dataBaseMessageProv = result.lastName;
          this.errorMsgEzProv = this._appService.ClaimCaptureService.dataBaseMessageProv;

          if (this.errorMsgEzProv !== 'Practice Number is incorrect') {
            this.cMasterForm.get('ezcapprov').setErrors(null);
            this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
            this._appService.ClaimCaptureService.vedorFieldVissible = true;
            this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
            this._ezProvSpec = 'SPECIALITY: ' + result.specCode + ' - ' + result.specCodeDescr;
          } else {            
            this.errorMsgEzProv = this.cMasterForm.get('ezcapprov').value;
            this._appService.ClaimCaptureService.currentClaimHeader.VENDORID = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
            this._appService.ClaimCaptureService.vedorFieldVissible = false;
          }

          this.cMasterForm.get('ezcapprov').markAsPristine();
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          this._appService.ClaimCaptureService._busy = false;
        },
        () => {                    
          this._appService.ClaimCaptureService._busy = false;
          this._appService.ClaimCaptureService.LoadVendorIds();
        }
      )     
    } 
  }
    
  //ValidateEzcapMemb() { // Jaco comment out 2024-01-05
  //  this.errorMsgMemb = 'Checking...';
  //  this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this.cMasterForm.get('ezcapmemb').value;
  //  this.errorMsgMemb = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB, 'ezcapmemb');
  //  if (this.errorMsgMemb === 'valid') {
  //    this.errorMsgMemb = this._appService.ClaimCaptureService.DataBaseCheckEzcapMemb();
  //    setTimeout((e) => {
  //      this.errorMsgMemb = this._appService.ClaimCaptureService.dataBaseMessageMemb;
  //      if (this.errorMsgMemb !== 'Family Number is incorrect') {
  //        this.cMasterForm.get('ezcapmemb').setErrors(null);
  //        if ((this.cMasterForm.get('ezcapdepmemb').value !== null) && (this.cMasterForm.get('ezcapdepmemb').value !== '') && (this.cMasterForm.get('ezcapdepmemb').value !== undefined)) {
  //          this.cMasterForm.get('ezcapdepmemb').markAsTouched();
  //          this.ValidateEzcapDep();
  //        }6
  //      } else {
  //        this.cMasterForm.get('ezcapmemb').setErrors(this.errorMsgMemb);
  //      }
  //    }, 2500)
  //  }
  //}

  ValidateEzcapMembNew() { // Jaco 2024-01-05
        
    this._familyList = [];
    this._familyOpt = '';
    this._depOpthruDt = '';
    this.cMasterForm.get('ezcapdepmemb').reset();

    this.errorMsgMemb = 'Checking...';

    // Trim policy number
    if (this.cMasterForm.get('ezcapmemb').value != null && this.cMasterForm.get('ezcapmemb').value != undefined) {
      this.cMasterForm.get('ezcapmemb').setValue(this.cMasterForm.get('ezcapmemb').value.trim()); 
    }

    this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this.cMasterForm.get('ezcapmemb').value;
    this.errorMsgMemb = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB, 'ezcapmemb');
       
    if (this.errorMsgMemb === 'valid') {

      this._appService.ClaimCaptureService._busy = true;

      this._appService.MembershipService.SearchEligibleMemberApi(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB, '', '', "1900/01/01", this._appService.ClaimCaptureService.currentClaimHeader.hpCode)
        .subscribe(
          (results: MemberEligibilityModel[]) => {

            results.forEach((el) => {
              if (el.hpcode == this._appService.ClaimCaptureService.currentClaimHeader.hpCode) { // to not include DRUM members (or any old hpcodes)
                this._familyList.push(el);
              }
            })

            if (this._familyList.length > 0 /*&& this._familyList[0].hpcode == this._appService.ClaimCaptureService.currentClaimHeader.hpCode*/) {

              this._familyOpt = this._familyList[0].opt;
              //this._membValid = true;
              //this._membErrorMsg = '';
              //this.authHeaderForm.get('member').setErrors(null);

              this.errorMsgMemb = '';
              this.cMasterForm.get('ezcapmemb').setErrors(null);
            }
            else {
              //this._membValid = false;
              //this._membErrorMsg = this.errMsg['invalidMemb'];
              //this.authHeaderForm.get('member').setErrors(this.errMsg['invalidMemb']);

              this.errorMsgMemb = 'Family Number not found / incorrect';
              this.cMasterForm.get('ezcapmemb').setErrors(this.errorMsgMemb);
            }

          },
          (error: HttpErrorResponse) => {
            //this._membValid = false;
            //this._membErrorMsg = this.errMsg['searchError'];
            //this.authHeaderForm.get('member').setErrors(this.errMsg['searchError']);

            this.errorMsgMemb = 'Unexpected error occured';
            this.cMasterForm.get('ezcapmemb').setErrors(this.errorMsgMemb);
            this._appService.ClaimCaptureService._busy = false;

            console.log(error)
          },
          () => {

            //this._appService.AuthorizationService.busy = false;
            this._appService.ClaimCaptureService._busy = false;

          }
        )
    }
  }

  //ValidateEzcapDep() { //Jaco comment out 2024-01-05
  //  this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this.cMasterForm.get('ezcapmemb').value;
  //  this._appService.ClaimCaptureService.currentClaimHeader.membDepCode = this.cMasterForm.get('ezcapdepmemb').value;
  //  this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB + '-' + this._appService.ClaimCaptureService.currentClaimHeader.membDepCode;
  //  this.errorMsgDepMem = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB, 'ezcapdepmemb');
  //  if ((this.cMasterForm.get('ezcapmemb').value === '') || (this.cMasterForm.get('ezcapmemb').value === null)) {
  //    this.errorMsgDepMem = this._errorMsg['familyNumber'];
  //    this.cMasterForm.get('ezcapdepmemb').setErrors(this.errorMsgDepMem);
  //  } else {
  //    if (this.errorMsgDepMem === 'valid')
  //      this.errorMsgDepMem = this._appService.ClaimCaptureService.DataBaseCheckEzcapDepMemb();
  //    setTimeout((e) => {
  //      this.errorMsgDepMem = this._appService.ClaimCaptureService.dataBaseMessageDepMemb;
  //    if (this.errorMsgDepMem !== 'Dependant Number is incorrect') {
  //      this.cMasterForm.get('ezcapdepmemb').setErrors(null);
  //    } else {
  //      this.cMasterForm.get('ezcapdepmemb').setErrors(this.errorMsgMemb);
  //    }
  //    }, 200)
  //  }
  //}

  ValidateEzcapDepNew() {
    this._depOpthruDt = '';

    //this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this.cMasterForm.get('ezcapmemb').value;
    this._appService.ClaimCaptureService.currentClaimHeader.membDepCode = this.cMasterForm.get('ezcapdepmemb').value;    
    //this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB + '-' + this._appService.ClaimCaptureService.currentClaimHeader.membDepCode;
    //this.errorMsgDepMem = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB, 'ezcapdepmemb');

    this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB = this._familyList.filter(x => x.dependant == this.cMasterForm.get('ezcapdepmemb').value)[0].membid;
    if (this._familyList.filter(x => x.dependant == this.cMasterForm.get('ezcapdepmemb').value)[0].opthrudt != "N/A") {

      this._depOpthruDt = "Termination Date: " + this._familyList.filter(x => x.dependant == this.cMasterForm.get('ezcapdepmemb').value)[0].opthrudt;
    }
  }

  ValidateRefProv() {
    this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID = this.cMasterForm.get('reprovider').value;
    this.errorMsgRefProv = '';
    if ((this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID !== '') && (this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID !== null)) {
      this.errorMsgRefProv = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.REFPROVID, 'reprovider');
      if (this.errorMsgRefProv === 'valid') {
        this.errorMsgRefProv = this._appService.ClaimCaptureService.DataBaseCheckRefProv();
        if (this.errorMsgRefProv !== 'Reffering Practice Number  is incorrect') {
          this.cMasterForm.get('reprovider').setErrors(null);
        } else {
          this.cMasterForm.get('reprovider').setErrors(null);
          this.errorMsgRefProv = this.cMasterForm.get('reprovider').value
        }
        this.cMasterForm.get('reprovider').markAsPristine();
      } else {
        this.cMasterForm.get('reprovider').setErrors(this.errorMsgRefProv);
      }
    }
  }

  SetDefaultDropDownValues() {
    //Default for place of Service if none is selected
    let p = this._appService.ClaimCaptureService.currentClaimHeader.PLACE;
    this.cMasterForm.get('PlaceSelect').setValue(p);

    //Default for phCode when none is selected
    let ph = this._appService.ClaimCaptureService.phSelect[0];
    this.cMasterForm.get('PHC').setValue(ph);

  }

  public ClearFormOnNew() {
    this._appService.ClaimCaptureService._disableCancel = false;
    this._appService.ClaimCaptureService.DefaultDate = '';
    this.cMasterForm.reset();
    this.errorMsgClaimType = '';
    this.errorMsgEzAuth = '';
    this.errorMsgEzProv = '';
    this.errorMsgMemb = '';
    this.errorMsgProvClaim = '';
    this.errorMsgRefProv = '';
    //this.errorMsgDepMem = '';
    this._familyList = [];  
    this._familyOpt = '';
    this._depOpthruDt = '';
    this.cMasterForm.enable();
    this._appService.ClaimCaptureService.NewClaimMaster();
    this.SetDefaultDropDownValues();
    this._appService.ClaimCaptureService.btnClick = true;
    this.cMasterForm.controls['HPCode'].setValue(this._appService.ClaimCaptureService.HpCodesList[0].code, { onlySelf: true });
    this._appService.ClaimCaptureService.LoadPhCodes();
    this.cMasterForm.get('PlaceSelect').disable();
    this._appService.ClaimCaptureService._disableBack = false;
    this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'P'


  }

  public DisableOnCancel() {
    this.cMasterForm.reset();
    this.errorMsgClaimType = '';
    this.errorMsgEzAuth = '';
    this.errorMsgEzProv = '';
    this.ezProvSpec = '';
    this.errorMsgMemb = '';
    this.errorMsgProvClaim = '';
    this.errorMsgRefProv = '';
    //this.errorMsgDepMem = '';
    this._familyList = [];
    this._familyOpt = '';
    this._depOpthruDt = '';
    this._appService.ClaimCaptureService.currentClaimHeader = new Recordtype1Model();
    this.cMasterForm.disable();
    this._appService.ClaimCaptureService.btnClick = false;
  }

  SaveData() {
    if (this._appService.ClaimCaptureService.dataBaseMessageProv == 'Practice Number is incorrect') {
      this._appService.ClaimCaptureService._incorrect = true;
    } else {
      this.cMasterForm.disable();
      this._appService.ClaimCaptureService.SaveClaimMaster();
      this._appService.ClaimCaptureService._incorrect = false;
    }

  }

  EditData() {
    this._appService.ClaimCaptureService._disableCancel = false;
    this.cMasterForm.enable();
    this._appService.ClaimCaptureService.btnClick = true;
    this.cMasterForm.get('PlaceSelect').disable();
  }

  ValidaterecDateField() {
    if ((this.cMasterForm.get('RecDateField').value === '') || (this.cMasterForm.get('RecDateField').value === null) || (this.cMasterForm.get('RecDateField').value === undefined)) {
      this.errorMsgRecdate = this._errorMsg['required'];
    } else {
      this._appService.ClaimCaptureService.currentClaimHeader.DATERECD = new Date(this.cMasterForm.get('RecDateField').value).toDateString();
      this.errorMsgRecdate = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimHeader.DATERECD, 'RecDateField');
      if (this.errorMsgRecdate === 'valid') {
        this.errorMsgRecdate = ' ';
        this.PopulateFromDateErr();
      }
      if (this.errorMsgRecdate !== '') {
        this.cMasterForm.get('RecDateField').setErrors(this.errorMsgRecdate);
      }
    }

  }

  PopulateFromDateErr() {
    let ToDay: Date = new Date;
    let comparedDate = new Date(this._appService.ClaimCaptureService.currentClaimHeader.DATERECD);

    if (comparedDate.toString().includes('Invalid Date')) {
      this.errorMsgRecdate = 'Entered date is invalid.';
    } else {
      if (comparedDate > ToDay) {
        this.errorMsgRecdate = 'Chosen date cannot be greater than ' + ToDay.toLocaleString();
      } else {
        this._appService.ClaimCaptureService.DefaultDate = this.cMasterForm.get('RecDateField').value;
        this.errorMsgRecdate = '';

      }
    }
  }

  VerifiedProv() {
    this.cMasterForm.disable();
    this._appService.ClaimCaptureService.SaveClaimMaster();
    this._appService.ClaimCaptureService._incorrect = false;
  }

  onClick() {

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['header_1', 'header_2', 'header_3', 'header_4', 'header_5', 'header_6', 'header_7', 'header_8', 'header_9', 'header_10',
          'header_11', 'header_12', 'header_13', 'header_14', 'header_15', 'header_16'],
        themeColor: '#313131'

      })
    }
  }

}

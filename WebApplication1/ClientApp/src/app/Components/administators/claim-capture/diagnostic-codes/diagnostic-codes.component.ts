import { Component, OnInit, Input } from '@angular/core';
import { ValidationErrors, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-diagnostic-codes',
  templateUrl: './diagnostic-codes.component.html',
  styleUrls: ['./diagnostic-codes.component.css']
})
export class DiagnosticCodesComponent implements OnInit {

  @Input() diagCodes: FormGroup;

  public _errorMsg: ValidationErrors;
  private _errorMsgFirstCode: any;
  private _errorMsgSecondCode: any;
  private _errorMsgThirdCode: any;
  private _errorMsgFourthCode: any;

  constructor(private fb: FormBuilder, private _appService: AppService) {
    this._errorMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      double: 'May not have fields with he same values!',
      valid: 'valid'
    };
  }

  ngOnInit() {

    this.diagCodes = this.fb.group({
      diagCode1: ['', [Validators.required, Validators.maxLength(10)]],
      diagCode2: ['', [Validators.maxLength(10)]],
      diagCode3: ['', [Validators.maxLength(10)]],
      diagCode4: ['', [Validators.maxLength(10)]],
    });

    //this.diagCodes.valueChanges.subscribe(
    //  (changes: any) => {
    //    this.ValidateForm();
    //  });

  }

  public get errorMsgFirstCode(): any {
    return this._errorMsgFirstCode;
  }
  public set errorMsgFirstCode(value: any) {
    this._errorMsgFirstCode = value;
  }

  public get errorMsgSecondCode(): any {
    return this._errorMsgSecondCode;
  }
  public set errorMsgSecondCode(value: any) {
    this._errorMsgSecondCode = value;
  }

  public get errorMsgThirdCode(): any {
    return this._errorMsgThirdCode;
  }
  public set errorMsgThirdCode(value: any) {
    this._errorMsgThirdCode = value;
  }

  public get errorMsgFourthCode(): any {
    return this._errorMsgFourthCode;
  }
  public set errorMsgFourthCode(value1: any) {
    this._errorMsgFourthCode = value1;
  }




  ValidateForm() {
    if (this.diagCodes.get('diagCode1').dirty === true) {
      this.ValidateFirstDiagCode()
    }
    if (this.diagCodes.get('diagCode2').dirty === true) {
      this.ValidateSecondDiagCode()
    }
    if (this.diagCodes.get('diagCode3').dirty === true) {
      this.ValidateThirdDiagCode()
    }
    if (this.diagCodes.get('diagCode4').dirty === true) {
      this.ValidateFourthDiagCode()
    }
  }

  FieldValidation(fieldName: string, controlName: string) {
    if ((fieldName === undefined) || (fieldName === '')) {
      if (controlName === 'diagCode1') {
        this.diagCodes.get(controlName).setErrors(this._errorMsg['required']);
        return this._errorMsg['required'];
      }
    }
    if (fieldName.length > 10) {
      this.diagCodes.get(controlName).setErrors(this._errorMsg['maxLength']);
      return this._errorMsg['maxLength'];
    }
    return this._errorMsg['valid'];
  }

  ValidateSecondDiagCode() {
    this.diagCodes.get('diagCode2').setErrors(null)
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this.diagCodes.get('diagCode2').value;
    this.errorMsgSecondCode = '';
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this._appService.ClaimCaptureService.diagnosticCodes.DIAG.replace(",", ".");
    if (this._appService.ClaimCaptureService.diagnosticCodes.DIAG !== '') {
      this.errorMsgSecondCode = this.FieldValidation(this._appService.ClaimCaptureService.diagnosticCodes.DIAG, 'diagCode2');
      if (this.errorMsgSecondCode === 'valid') {
        this.errorMsgSecondCode = '';
        this.errorMsgSecondCode = this._appService.ClaimCaptureService.DataBaseDiagCode2Check();
        setTimeout((a) => {
          this.errorMsgSecondCode = this._appService.ClaimCaptureService.dataBaseMessageDiagC2;
          if (this.errorMsgSecondCode != 'Invalid Diagnostic code') {
            let x = this.diagCodes.get('diagCode2').value.replace(",", ".");
            this._appService.ClaimCaptureService.diagnosticCodes.DIAG = x.toUpperCase();
            this._appService.ClaimCaptureService.diagnosticCodes.DIAGREF = 2;
            this._appService.ClaimCaptureService.AddDiagCode();
            if (this._appService.ClaimCaptureService.findSameDiagCode === true) {
              this.diagCodes.get('diagCode2').setErrors(this._errorMsg['double']);
              this.errorMsgSecondCode = this._errorMsg['double'];
            }
          } else {
            this._appService.ClaimCaptureService.diagnosticCodesList.splice(1, 1);
            this.diagCodes.get('diagCode2').setErrors(this.errorMsgSecondCode);
          }
        },350)

      }
    } else {
      this.RemoveOnEmptyField();
    }
    if (this.diagCodes.valid) {
      this._appService.ClaimCaptureService.enableWizardNext = true;
    } else {
      this._appService.ClaimCaptureService.enableWizardNext = false;
      //   this.diagCodes.get("").setValue("value");
    }
  }

  ValidateThirdDiagCode() {
    this.diagCodes.get('diagCode3').setErrors(null);
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this.diagCodes.get('diagCode3').value;
    this.errorMsgThirdCode = '';
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this._appService.ClaimCaptureService.diagnosticCodes.DIAG.replace(",", ".");
    if (this._appService.ClaimCaptureService.diagnosticCodes.DIAG !== '') {
      this.errorMsgThirdCode = this.FieldValidation(this._appService.ClaimCaptureService.diagnosticCodes.DIAG, 'diagCode3');
      if (this.errorMsgThirdCode === 'valid') {
        this.errorMsgThirdCode = '';
        this.errorMsgThirdCode = this._appService.ClaimCaptureService.DataBaseDiagCode3Check();
        setTimeout((a) => {
          this.errorMsgThirdCode = this._appService.ClaimCaptureService.dataBaseMessageDiagC3;
          if (this.errorMsgThirdCode != 'Invalid Diagnostic code') {
            let x = this.diagCodes.get('diagCode3').value.replace(",", ".");
            this._appService.ClaimCaptureService.diagnosticCodes.DIAG = x.toUpperCase();
            this._appService.ClaimCaptureService.diagnosticCodes.DIAGREF = 3;
            this._appService.ClaimCaptureService.AddDiagCode();
            if (this._appService.ClaimCaptureService.findSameDiagCode === true) {
              this.diagCodes.get('diagCode3').setErrors(this._errorMsg['double']);
              this.errorMsgThirdCode = this._errorMsg['double'];
            }
          } else {
            this._appService.ClaimCaptureService.diagnosticCodesList.splice(2, 1);
            this.diagCodes.get('diagCode3').setErrors(this.errorMsgThirdCode);
          }
        },1000)
      }
    } else {
      this.RemoveOnEmptyField();
    }
    if (this.diagCodes.valid) {
      this._appService.ClaimCaptureService.enableWizardNext = true;
    } else {
      this._appService.ClaimCaptureService.enableWizardNext = false;
    }
  }

  ValidateFourthDiagCode() {
    this.diagCodes.get('diagCode4').setErrors(null);
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this.diagCodes.get('diagCode4').value;
    this.errorMsgFourthCode = '';
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this._appService.ClaimCaptureService.diagnosticCodes.DIAG.replace(",", ".");
    if (this._appService.ClaimCaptureService.diagnosticCodes.DIAG !== '') {
      this.errorMsgFourthCode = this.FieldValidation(this._appService.ClaimCaptureService.diagnosticCodes.DIAG, 'diagCode4');
      if (this.errorMsgFourthCode === 'valid') {
        this.errorMsgFourthCode = '';
        this.errorMsgFourthCode = this._appService.ClaimCaptureService.DataBaseDiagCode4Check();
        setTimeout((a) => {
          this.errorMsgFourthCode = this._appService.ClaimCaptureService.dataBaseMessageDiagC4;
          if (this.errorMsgFourthCode != 'Invalid Diagnostic code') {
            let x = this.diagCodes.get('diagCode4').value.replace(",", ".");
            this._appService.ClaimCaptureService.diagnosticCodes.DIAG = x.toUpperCase();
            this._appService.ClaimCaptureService.diagnosticCodes.DIAGREF = 4;
            this._appService.ClaimCaptureService.AddDiagCode();
            if (this._appService.ClaimCaptureService.findSameDiagCode === true) {
              this.diagCodes.get('diagCode4').setErrors(this._errorMsg['double']);
              this.errorMsgFourthCode = this._errorMsg['double'];
            }
          } else {
            this.diagCodes.get('diagCode4').setErrors(this.errorMsgFourthCode);
            this._appService.ClaimCaptureService.diagnosticCodesList.splice(3, 1);
          }
        },1000)
      }
    } else {
      this.RemoveOnEmptyField();
    }
    if (this.diagCodes.valid) {
      this._appService.ClaimCaptureService.enableWizardNext = true;
    } else {
      this._appService.ClaimCaptureService.enableWizardNext = false;
    }
  }

  RemoveOnEmptyField() {
    if (this.diagCodes.get('diagCode1').value === '') {
      this._appService.ClaimCaptureService.diagnosticCodesList.splice(0, 1);
    }
    if (this.diagCodes.get('diagCode2').value === '') {
      this._appService.ClaimCaptureService.diagnosticCodesList.splice(1, 1);
    }
    if (this.diagCodes.get('diagCode3').value === '') {
      this._appService.ClaimCaptureService.diagnosticCodesList.splice(2, 1);
    }
    if (this.diagCodes.get('diagCode4').value === '') {
      this._appService.ClaimCaptureService.diagnosticCodesList.splice(3, 1);
    }
  }

  ValidateFirstDiagCode() {
    this.diagCodes.get('diagCode1').setErrors(null)
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this.diagCodes.get('diagCode1').value;
    this.errorMsgFirstCode = '';
    this._appService.ClaimCaptureService.diagnosticCodes.DIAG = this._appService.ClaimCaptureService.diagnosticCodes.DIAG.replace(",", ".");
    if (this._appService.ClaimCaptureService.diagnosticCodes.DIAG !== '') {
      this.errorMsgFirstCode = this.FieldValidation(this._appService.ClaimCaptureService.diagnosticCodes.DIAG, 'diagCode1');
      if (this.errorMsgFirstCode === 'valid') {
        this.errorMsgFirstCode = '';
        this.errorMsgFirstCode = this._appService.ClaimCaptureService.DataBaseDiagCode1Check();
        setTimeout((a) => {
          this.errorMsgFirstCode = this._appService.ClaimCaptureService.dataBaseMessageDiagC1;
          if (this.errorMsgFirstCode != 'Invalid Diagnostic code') {
            let x = this.diagCodes.get('diagCode1').value.replace(",", ".");
            this._appService.ClaimCaptureService.diagnosticCodes.DIAG = x.toUpperCase();
            this._appService.ClaimCaptureService.diagnosticCodes.DIAGREF = 1;
            this._appService.ClaimCaptureService.AddDiagCode();
            if (this._appService.ClaimCaptureService.findSameDiagCode === true) {
              this.diagCodes.get('diagCode1').setErrors(this._errorMsg['double']);
              this.errorMsgSecondCode = this._errorMsg['double'];
            }
          } else {
            this._appService.ClaimCaptureService.diagnosticCodesList.splice(0, 1);
            this.diagCodes.get('diagCode1').setErrors(this.errorMsgFirstCode);
          }
        },650)
      }
    } else {
      this.RemoveOnEmptyField();
    }
    if (this.diagCodes.valid) {
      this._appService.ClaimCaptureService.enableWizardNext = true;
    } else {
      this._appService.ClaimCaptureService.enableWizardNext = false;
      //   this.diagCodes.get("").setValue("value");
    }
  }
}

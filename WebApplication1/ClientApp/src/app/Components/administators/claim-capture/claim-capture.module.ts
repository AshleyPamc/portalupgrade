import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from "@clr/angular";
import { ClaimCaptureComponent } from './claim-capture.component';
import { ClaimDetailComponent } from './claim-detail/claim-detail.component';
import { ClaimHeaderComponent } from './claim-header/claim-header.component';
import { ClaimNotesComponent } from './claim-notes/claim-notes.component';
import { ClaimsViewComponent } from './claims-view/claims-view.component';
import { DiagnosticCodesComponent } from './diagnostic-codes/diagnostic-codes.component';
import { DisplayClaimHeaderComponent } from './display-claim-header/display-claim-header.component';
import { ModifiersComponent } from './modifiers/modifiers.component';
import { RejectionCodesComponent } from './rejection-codes/rejection-codes.component';
import { ToothNumbersComponent } from './tooth-numbers/tooth-numbers.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
import { JoyrideModule } from 'ngx-joyride';
import { SharedModule } from './../../../shared.module'



const routes: Routes = [{

  path: '',
  component: ClaimCaptureComponent,
  children: [
    { pathMatch: 'full', path: 'claim-capture', component: ClaimCaptureComponent }
  ]

}];

@NgModule({
  declarations: [ClaimCaptureComponent, ClaimDetailComponent,
    ClaimHeaderComponent, ClaimNotesComponent, ClaimsViewComponent, DiagnosticCodesComponent, DisplayClaimHeaderComponent,
    ModifiersComponent, RejectionCodesComponent, ToothNumbersComponent, UserValidationComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    JoyrideModule.forRoot(),
    SharedModule

  ],
  exports: [FormsModule, ReactiveFormsModule]
})
export class ClaimCaptureModule { }

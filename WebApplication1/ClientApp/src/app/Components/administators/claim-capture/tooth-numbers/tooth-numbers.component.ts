import { Component, OnInit, Input } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormGroup, ValidationErrors, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-tooth-numbers',
  templateUrl: './tooth-numbers.component.html',
  styleUrls: ['./tooth-numbers.component.css']
})
export class ToothNumbersComponent implements OnInit {

  @Input() cToothNumForm: FormGroup;

  private _errMsgTooth1: any;
  private _errMsgTooth2: any;
  private _errMsgTooth3: any;
  private _errMsgTooth4: any;
  private _errMsgTooth5: any;
  private _errMsgTooth6: any;
  private _errMsgTooth7: any;
  private _errMsgTooth8: any;
  public _errorMsg: ValidationErrors;

  constructor(public _appService: AppService, private fb: FormBuilder) {

    this._errorMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      double: 'May not have fields with he same values!',
      valid: 'valid'
    };

  }

  ngOnInit() {

    this.cToothNumForm = this.fb.group({

      toothNum1: ['', Validators.maxLength(10)],
      toothNum2: ['', Validators.maxLength(10)],
      toothNum3: ['', Validators.maxLength(10)],
      toothNum4: ['', Validators.maxLength(10)],
      toothNum5: ['', Validators.maxLength(10)],
      toothNum6: ['', Validators.maxLength(10)],
      toothNum7: ['', Validators.maxLength(10)],
      toothNum8: ['', Validators.maxLength(10)],
    });

    this.cToothNumForm.valueChanges.subscribe(
      (changes) => {
        this.ValidateForm(changes);
      });

  }

  public get errMsgTooth1(): any {
    return this._errMsgTooth1;
  }
  public set errMsgTooth1(value: any) {
    this._errMsgTooth1 = value;
  }

  public get errMsgTooth2(): any {
    return this._errMsgTooth2;
  }
  public set errMsgTooth2(value: any) {
    this._errMsgTooth2 = value;
  }

  public get errMsgTooth3(): any {
    return this._errMsgTooth3;
  }
  public set errMsgTooth3(value: any) {
    this._errMsgTooth3 = value;
  }

  public get errMsgTooth4(): any {
    return this._errMsgTooth4;
  }
  public set errMsgTooth4(value: any) {
    this._errMsgTooth4 = value;
  }

  public get errMsgTooth5(): any {
    return this._errMsgTooth5;
  }
  public set errMsgTooth5(value: any) {
    this._errMsgTooth5 = value;
  }

  public get errMsgTooth6(): any {
    return this._errMsgTooth6;
  }
  public set errMsgTooth6(value: any) {
    this._errMsgTooth6 = value;
  }

  public get errMsgTooth7(): any {
    return this._errMsgTooth7;
  }
  public set errMsgTooth7(value: any) {
    this._errMsgTooth7 = value;
  }

  public get errMsgTooth8(): any {
    return this._errMsgTooth8;
  }
  public set errMsgTooth8(value: any) {
    this._errMsgTooth8 = value;
  }

  ValidateForm(changes: any) {
    if (this.cToothNumForm.get('toothNum1').dirty === true) {
      this.ValidateToothNum1();
    }

    if (this.cToothNumForm.get('toothNum2').dirty === true) {
      this.ValidateToothNum2();
    }

    if (this.cToothNumForm.get('toothNum3').dirty === true) {
      this.ValidateToothNum3();
    }

    if (this.cToothNumForm.get('toothNum4').dirty === true) {
      this.ValidateToothNum4();
    }

    if (this.cToothNumForm.get('toothNum5').dirty === true) {
      this.ValidateToothNum5();
    }

    if (this.cToothNumForm.get('toothNum6').dirty === true) {
      this.ValidateToothNum6();
    }

    if (this.cToothNumForm.get('toothNum7').dirty === true) {
      this.ValidateToothNum7();
    }

    if (this.cToothNumForm.get('toothNum8').dirty === true) {
      this.ValidateToothNum8();
    }
  }

  FieldValidation(fieldName: string, controlName: string) {
    if ((fieldName !== '') && (fieldName !== null) && (fieldName !== undefined)) {
      if (fieldName.length > 10) {
        this.cToothNumForm.get(controlName).setErrors(this._errorMsg['maxLength']);
        return this._errorMsg['maxLength'];
      }
    }
    return this._errorMsg['valid'];
  }

  ValidateToothNum1() {
    this.errMsgTooth1 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum1').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== null)) {
      this.errMsgTooth1 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum1');
      if (this.errMsgTooth1 === 'valid') {
        this.errMsgTooth1 = '';
        this.errMsgTooth1 = this._appService.ClaimCaptureService.DataBaseCheckTooth1();
        if (this.errMsgTooth1 !== this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this._appService.ClaimCaptureService.displayClinicalCodes = true;
          this.cToothNumForm.get('toothNum1').setErrors(null);
        } else {
          this.cToothNumForm.get('toothNum1').setErrors(this.errMsgTooth1);
        }
      }
    }
    this.cToothNumForm.get('toothNum1').markAsPristine();
  }

  ValidateToothNum2() {
    this.errMsgTooth2 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum2').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== null)) {
      this.errMsgTooth2 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum2');
      if (this.errMsgTooth2 === 'valid') {
        this.errMsgTooth2 = '';
        this.errMsgTooth2 = this._appService.ClaimCaptureService.DataBaseCheckTooth2();
        if (this.errMsgTooth2 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum2').setErrors(this.errMsgTooth1);
        } else {
          this.cToothNumForm.get('toothNum2').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum2').markAsPristine();
  }

  ValidateToothNum3() {
    this.errMsgTooth3 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum3').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== null)) {
      this.errMsgTooth3 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum3');
      if (this.errMsgTooth3 === 'valid') {
        this.errMsgTooth3 = '';
        this.errMsgTooth3 = this._appService.ClaimCaptureService.DataBaseCheckTooth3();
        if (this.errMsgTooth3 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum3').setErrors(this.errMsgTooth3);
        } else {
          this.cToothNumForm.get('toothNum3').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum3').markAsPristine();
  }

  ValidateToothNum4() {
    this.errMsgTooth4 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum4').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== null)) {
      this.errMsgTooth4 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum4');
      if (this.errMsgTooth4 === 'valid') {
        this.errMsgTooth4 = '';
        this.errMsgTooth4 = this._appService.ClaimCaptureService.DataBaseCheckTooth4();
        if (this.errMsgTooth4 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum4').setErrors(this.errMsgTooth1);
        } else {
          this.cToothNumForm.get('toothNum4').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum4').markAsPristine();
  }

  ValidateToothNum5() {
    this.errMsgTooth5 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum5').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== null)) {
      this.errMsgTooth5 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum5');
      if (this.errMsgTooth5 === 'valid') {
        this.errMsgTooth5 = '';
        this.errMsgTooth5 = this._appService.ClaimCaptureService.DataBaseCheckTooth5();
        if (this.errMsgTooth5 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum5').setErrors(this.errMsgTooth5);
        } else {
          this.cToothNumForm.get('toothNum5').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum5').markAsPristine();
  }

  ValidateToothNum6() {
    this.errMsgTooth6 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum6').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 = this._appService.ClaimCaptureService.toothNum.toothNum;
    this.errMsgTooth6 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum6');
    if (this.errMsgTooth6 === 'valid') {
      this.errMsgTooth6 = '';
      this.errMsgTooth6 = this._appService.ClaimCaptureService.DataBaseCheckTooth6();
      if (this.errMsgTooth6 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
        this.cToothNumForm.get('toothNum6').setErrors(this.errMsgTooth1);
      } else {
        this.cToothNumForm.get('toothNum6').setErrors(null);
      }
    }
    this.cToothNumForm.get('toothNum6').markAsPristine();
  }

  ValidateToothNum7() {
    this.errMsgTooth7 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum7').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== null)) {
      this.errMsgTooth7 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum7');
      if (this.errMsgTooth7 === 'valid') {
        this.errMsgTooth7 = '';
        this.errMsgTooth7 = this._appService.ClaimCaptureService.DataBaseCheckTooth7();
        if (this.errMsgTooth7 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum7').setErrors(this.errMsgTooth1);
        } else {
          this.cToothNumForm.get('toothNum7').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum7').markAsPristine();
  }

  ValidateToothNum8() {
    this.errMsgTooth8 = '';
    this._appService.ClaimCaptureService.toothNum.toothNum = this.cToothNumForm.get('toothNum8').value;
    this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 = this._appService.ClaimCaptureService.toothNum.toothNum;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== null)) {
      this.errMsgTooth8 = this.FieldValidation(this._appService.ClaimCaptureService.toothNum.toothNum, 'toothNum1');
      if (this.errMsgTooth8 === 'valid') {
        this.errMsgTooth8 = '';
        this.errMsgTooth8 = this._appService.ClaimCaptureService.DataBaseCheckTooth8();
        if (this.errMsgTooth8 === this._appService.ClaimCaptureService.toothNum.toothNum + ' is not valid.') {
          this.cToothNumForm.get('toothNum8').setErrors(this.errMsgTooth1);
        } else {
          this.cToothNumForm.get('toothNum8').setErrors(null);
        }
      }
    }
    this.cToothNumForm.get('toothNum8').markAsPristine();
  }
}

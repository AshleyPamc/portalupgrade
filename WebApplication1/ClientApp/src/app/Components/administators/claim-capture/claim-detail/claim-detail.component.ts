import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormGroup, ValidationErrors, FormBuilder, Validators } from '@angular/forms';
import { Recordtype4Model } from './../../../../models/Recordtype4Model';
import { Recordtype8Model } from './../../../../models/Recordtype8Model';
import { ClaimNotesComponent } from '../claim-notes/claim-notes.component';
import { RejectionCodesComponent } from '../rejection-codes/rejection-codes.component';
import { ToothNumbersComponent } from '../tooth-numbers/tooth-numbers.component';
import { ModifiersComponent } from '../modifiers/modifiers.component';
import { from } from 'rxjs';
import { EditBankingDetailComponent } from '../../checkrun/edit-banking-detail/edit-banking-detail.component';
import { BankingDetailModel } from './../../../../models/BankingDetailModel';

@Component({
  selector: 'app-claim-detail',
  templateUrl: './claim-detail.component.html',
  styleUrls: ['./claim-detail.component.css']
})
export class ClaimDetailComponent implements OnInit {

  @Input() cDetailForm: FormGroup;
  @ViewChild('ModCodes', { static: false }) modCodes: ModifiersComponent;
  @ViewChild('ClinicalCodes', { static: false }) clinicalCodes: ToothNumbersComponent;
  @ViewChild('adjust', { static: false }) adjustCodes: RejectionCodesComponent;
  @ViewChild('notes', { static: false }) claimNotes: ClaimNotesComponent;
  @ViewChild('banking', { static:false }) banking: EditBankingDetailComponent;

  private _claimDetailModalShow: boolean;
  private _showAlert: boolean = false;
  private _errorMsg: ValidationErrors;
  private _errorMsgQTY: any;
  private _errorMsgBilled: string;
  private _errorMsgProccode: any;
  private _errorMsgDIAGREF: any;
  private _errorMsgFromdate: any;
  private _errorMsgTodate: any;
  private _tempProcCode: string;
  private _tempDiagCode: string;
  private _validForm: boolean = false;
  private _rowEdit: boolean = false;
  private _updateDisplay: boolean = false;
  private _containsElement: boolean = false;


  constructor(public _appService: AppService, private fb: FormBuilder) {

    this.claimDetailModalShow = false;
    this._errorMsg = {
      required: 'This is a required field.',
      number: 'This field requires a number',
      maxLength: 'Exceeded max length',
      rowID: 'Cannot find ID.',
      valid: 'valid'
    };
    this._appService.ClaimCaptureService.enableWizardNext = false;
    this.errorMsgBilled = '';
    this.errorMsgDIAGREF = '';
    this.errorMsgFromdate = '';
    this.errorMsgProccode = '';
    this.errorMsgQTY = '';
  }

  ngOnInit() {
    this.claimDetailModalShow = false;
    this.cDetailForm = this.fb.group({
      PROCCODE: ['', [Validators.required, Validators.maxLength(20)]],
      fromDateField: ['', Validators.required],
      ToDateField: [''],
      billedField: ['', [Validators.required, Validators.maxLength(20)]],
      qtyField: ['', [Validators.required, Validators.maxLength(10)]],
      diagrefField: [],
      phcodes: [],
      rowId: ['', [Validators.required]]
    });
    this._appService.ClaimCaptureService.displayClinicalCodes;
  }

  public get claimDetailModalShow(): boolean {
    return this._claimDetailModalShow;
  }
  public set claimDetailModalShow(value: boolean) {
    this._claimDetailModalShow = value;
  }

  public get showAlert(): boolean {
    return this._showAlert;
  }
  public set showAlert(value: boolean) {
    this._showAlert = value;
  }

  public get errorMsgQTY(): any {
    return this._errorMsgQTY;
  }
  public set errorMsgQTY(value: any) {
    this._errorMsgQTY = value;
  }

  public get errorMsgBilled(): string {
    return this._errorMsgBilled;
  }
  public set errorMsgBilled(value: string) {
    this._errorMsgBilled = value;
  }

  public get errorMsgProccode(): any {
    return this._errorMsgProccode;
  }
  public set errorMsgProccode(value: any) {
    this._errorMsgProccode = value;
  }

  public get errorMsgDIAGREF(): any {
    return this._errorMsgDIAGREF;
  }
  public set errorMsgDIAGREF(value: any) {
    this._errorMsgDIAGREF = value;
  }

  public get errorMsgFromdate(): any {
    return this._errorMsgFromdate;
  }
  public set errorMsgFromdate(value: any) {
    this._errorMsgFromdate = value;
  }

  public get errorMsgTodate(): any {
    return this._errorMsgTodate;
  }
  public set errorMsgTodate(value: any) {
    this._errorMsgTodate = value;
  }

  public set tempProcCode(value: string) {
    this._tempProcCode = value;
  }

  public set tempDiagCode(value: string) {
    this._tempDiagCode = value;
  }

  public get validForm(): boolean {
    return this._validForm;
  }
  public set validForm(value: boolean) {
    this._validForm = value;
  }

  public set rowEdit(value: boolean) {
    this._rowEdit = value;
  }

  public get updateDisplay(): boolean {
    return this._updateDisplay;
  }
  public set updateDisplay(value: boolean) {
    this._updateDisplay = value;
  }

  public get containsElement(): boolean {
    return this._containsElement;
  }
  public set containsElement(value: boolean) {
    this._containsElement = value;
  }


  PopulateFromDateErr() {
    let service: Date = new Date(this._appService.ClaimCaptureService.currentClaimHeader.DATERECD);
    let comparedDate = new Date(this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE);

    if (comparedDate.toString().includes('Invalid Date')) {
      this.errorMsgFromdate = 'Entered date is invalid.';
    } else {
      if (comparedDate > service) {
        this.errorMsgFromdate = 'Chosen date cannot be greater than ' + service.toLocaleString('en-za', { timeZone: 'UTC' }).substr(0, 10);
      } else {
        this._appService.ClaimCaptureService.DefaultDate = this.cDetailForm.get('fromDateField').value;
        this.errorMsgFromdate = '';
        let oneDay = 24 * 60 * 60 * 1000;
        let recieved = new Date(this._appService.ClaimCaptureService.currentClaimHeader.DATERECD);
        let difference: any;
        difference = Math.round(Math.abs((recieved.getTime() - comparedDate.getTime()) / (oneDay)));
        if (difference >= this._appService.ClaimCaptureService._stalePeriod) {
          this._appService.ClaimCaptureService._staleDateModel = true;
        }
        if ((this.cDetailForm.get('ToDateField').value === '') || (this.cDetailForm.get('ToDateField').value === null) || (this.cDetailForm.get('ToDateField').value === undefined)) {
          this.cDetailForm.get('ToDateField').setValue(this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE);
          this._appService.ClaimCaptureService.currentClaimDetail.TODATE = this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE
        }
      }
    }
  }

  PopulateToDateErr() {
    let service: Date = new Date(this._appService.ClaimCaptureService.currentClaimHeader.DATERECD);
    let comparedDate = new Date(this._appService.ClaimCaptureService.currentClaimDetail.TODATE);
    let fromdate = new Date(this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE);

    if (comparedDate.toString().includes('Invalid Date')) {
      this.errorMsgTodate = 'Entered date is invalid.';
    } else {
      if (comparedDate > service) {
        this.errorMsgTodate = 'Chosen date cannot be greater than ' + service.toLocaleString('en-za', { timeZone: 'UTC' }).substr(0, 10);
      }
      if (comparedDate < fromdate) {
        this.errorMsgTodate = 'Chosen date cannot be smaller than ' + fromdate.toLocaleString('en-za', { timeZone: 'UTC' }).substr(0, 10);
      }
    }
  }

  ShowModCodes() {
    this._appService.ClaimCaptureService.showModCode = true;
    this._appService.ClaimCaptureService.showToothNum = false;
    this._appService.ClaimCaptureService.showReject = false;
    this._appService.ClaimCaptureService.showNotes = false;
  }

  ShowToothNum() {
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showToothNum = true;
    this._appService.ClaimCaptureService.showReject = false;
    this._appService.ClaimCaptureService.showNotes = false;
  }

  RejectionCodes() {
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showToothNum = false;
    this._appService.ClaimCaptureService.showReject = true;
    this._appService.ClaimCaptureService.showNotes = false;
    this.adjustCodes.cRejectCodeForm.get('description').disable();
  }

  AddNotes() {
    this._appService.ClaimCaptureService.showNotes = true;
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showToothNum = false;
    this._appService.ClaimCaptureService.showReject = false;
  }

  FieldValidation(fieldData: any, controlName: string) {
    if ((fieldData === undefined) || (fieldData === '') || (fieldData === null)) {
      this.cDetailForm.get(controlName).setErrors(this._errorMsg['required']);
      return this._errorMsg['required'];
    }
    if ((controlName !== 'fromDateField') && (controlName !== 'ToDateField')) {
      if (fieldData.length > 20) {
        this.cDetailForm.get(controlName).setErrors(this._errorMsg['maxLength']);
        return this._errorMsg['maxLength'];
      }
    }
    return this._errorMsg['valid'];
  }

  ValidateFromDateField() {
    let x = this.cDetailForm.get('fromDateField').value
    if ((this.cDetailForm.get('fromDateField').value !== '') && (this.cDetailForm.get('fromDateField').value !== null) && (this.cDetailForm.get('fromDateField').value !== undefined)) {
      this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE = new Date(this.cDetailForm.get('fromDateField').value).toDateString();
      this._appService.ClaimCaptureService.currentClaimDetail.TODATE = new Date(this.cDetailForm.get('fromDateField').value).toDateString();
      this.errorMsgFromdate = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE, 'fromDateField');
      if (this.errorMsgFromdate === 'valid') {
        this.errorMsgFromdate = ' ';
        this.PopulateFromDateErr();

      }
      if (this.errorMsgFromdate !== '') {
        this.cDetailForm.get('fromDateField').setErrors(this.errorMsgFromdate);
      } else {
        this._appService.ClaimCaptureService.currentClaimDetail.TODATE = this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE;
        this.cDetailForm.controls['ToDateField'].setValue(this.cDetailForm.get('fromDateField').value);
      }

      this.TempValidFormMethod();
    }
  }

  ValidateToDateField() {
    if ((this.cDetailForm.get('ToDateField').value === '') || (this.cDetailForm.get('ToDateField').value === null) || (this.cDetailForm.get('ToDateField').value === undefined)) {
      this.cDetailForm.get('ToDateField').setValue(this.cDetailForm.get('fromDateField').value);
      this._appService.ClaimCaptureService.currentClaimDetail.TODATE = new Date(this.cDetailForm.get('fromDateField').value).toDateString();
    } else {
      this._appService.ClaimCaptureService.currentClaimDetail.TODATE = new Date(this.cDetailForm.get('ToDateField').value).toDateString();
      this.errorMsgTodate = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.TODATE, 'ToDateField');
      if (this.errorMsgTodate === 'valid') {
        this.errorMsgTodate = ' ';
        this.PopulateToDateErr();
      }
      if (this.errorMsgTodate === ' ') {
        this.cDetailForm.get('ToDateField').setErrors(null);
      } else {
        this.cDetailForm.get('ToDateField').setErrors(this.errorMsgTodate);
      }
      this.TempValidFormMethod();
    }

  }

  ValidateProcCodeField() {
    this.tempProcCode = this.cDetailForm.get('PROCCODE').value;
    this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this._tempProcCode
    this.errorMsgProccode = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE, 'PROCCODE');
    if (this.errorMsgProccode === 'valid') {
      let x = parseInt(this._tempProcCode);
      if (x === NaN) {
        if ((this._tempProcCode !== 'gmed') && (this._tempProcCode !== 'GMED')) {
          this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE =
            this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE.padEnd(20);
        } else {
          this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this._tempProcCode;
        }
      } else {
        if ((this._tempProcCode !== 'gmed') && (this._tempProcCode !== 'GMED')) {
          this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE.padStart(20)
        } else {
          this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this._tempProcCode;
        }
      };
      this.errorMsgProccode = this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE;
      this._appService.ClaimCaptureService.DataBaseCheckProccode();
    }
    this.TempValidFormMethod();
  }

  ValidateBilledField() {
    this._appService.ClaimCaptureService.currentClaimDetail.BILLED = this.cDetailForm.get('billedField').value;
    this.errorMsgBilled = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.BILLED, 'billedField');



    if (this.errorMsgBilled === 'valid') {
      this.errorMsgBilled = '';
      if (this._appService.ClaimCaptureService.currentClaimDetail.BILLED.includes(",")) {
        let c: string[] = this._appService.ClaimCaptureService.currentClaimDetail.BILLED.split(",");
        if (c.length == 2) {
          if (c[1].length == 2) {
            this._appService.ClaimCaptureService.currentClaimDetail.BILLED =this._appService.ClaimCaptureService.currentClaimDetail.BILLED.replace(",", ".");

          }
        } else if (c.length > 2) {
          let result: string = "";
          if (c[c.length - 1].length == 2) {
            for (var i = 0; i < c.length; i++) {
              if (i == c.length - 1) {
                result = result + "." + c[i];
              } else {
                result = result + c[i];
              }
            }
          }
          this._appService.ClaimCaptureService.currentClaimDetail.BILLED = result;
        }
      }
      this.errorMsgBilled = this.NumberCheck(this._appService.ClaimCaptureService.currentClaimDetail.BILLED.toString(), this.errorMsgBilled, 'billedField');
    }
    if (this.errorMsgBilled === 'valid') {
      this.errorMsgBilled = 'R ' + this.cDetailForm.get('billedField').value;
      this.cDetailForm.get('billedField').setErrors(null);
    }
    this.TempValidFormMethod();
  }

  ValidateQtyField() {
    this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.get('qtyField').value;
    this.errorMsgQTY = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.QTY, 'qtyField');
    if (this._appService.ClaimCaptureService.currentClaimDetail.QTY.toString().length > 10) {
      this.errorMsgQTY = this._errorMsg['maxLength'];
    }
    if (this.errorMsgQTY === 'valid') {
      this.errorMsgQTY = ' ';
      this.errorMsgQTY = this.NumberCheck(this._appService.ClaimCaptureService.currentClaimDetail.QTY.toString(), this.errorMsgQTY, 'qtyField');
    }
    if (this.errorMsgQTY === 'valid') {
      this.errorMsgQTY = this.cDetailForm.get('qtyField').value + ' is Valid';
    }
    this.TempValidFormMethod();
  }

  PHCODEChanged() {
    let x = this.cDetailForm.get('phcodes').value;
    this._appService.ClaimCaptureService.currentClaimDetail.PHCODE = x;
  }

  NumberCheck(checkValue: string, msg: any, controlName: string) {
    let x = + checkValue;
    if (x.toString() === 'NaN') {
      msg = this._errorMsg['number'];
      this.cDetailForm.get(controlName).setErrors(this._errorMsg['number']);
      return msg;
    } else {
      msg = 'valid';
      return msg;
    }
  }

  SubmitClaimDetail() {
    if ((this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE_DESC.toUpperCase() == "") || (this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE_DESC.toUpperCase() == "INCORRECT PROCEDURE CODE")) {
      this._appService.ClaimCaptureService._incorrectProc = true;
    } else {
      this.HideModAndClinicalCodes();
      if (this._rowEdit === true) {
        //claimDetail Edit
        this._appService.ClaimCaptureService.currentClaimDetail.PHCODE = this.cDetailForm.get('phcodes').value;
        this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this.cDetailForm.get('PROCCODE').value;
        this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE = this.cDetailForm.get('fromDateField').value;
        this._appService.ClaimCaptureService.currentClaimDetail.TODATE = this.cDetailForm.get('ToDateField').value;
        this._appService.ClaimCaptureService.currentClaimDetail.BILLED = this.cDetailForm.get('billedField').value;
        this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.get('qtyField').value;
        this._appService.ClaimCaptureService.currentClaimDetail.DIAGREF = this.cDetailForm.get('diagrefField').value;
        this._appService.ClaimCaptureService.currentClaimDetail.MODCODE = this.modCodes.cModifiersForm.get('modCode1').value;
        this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 = this.modCodes.cModifiersForm.get('modCode2').value;
        this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 = this.modCodes.cModifiersForm.get('modCode3').value;
        this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 = this.modCodes.cModifiersForm.get('modCode4').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 = this.clinicalCodes.cToothNumForm.get('toothNum1').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 = this.clinicalCodes.cToothNumForm.get('toothNum2').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 = this.clinicalCodes.cToothNumForm.get('toothNum3').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 = this.clinicalCodes.cToothNumForm.get('toothNum4').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 = this.clinicalCodes.cToothNumForm.get('toothNum5').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 = this.clinicalCodes.cToothNumForm.get('toothNum6').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 = this.clinicalCodes.cToothNumForm.get('toothNum7').value;
        this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 = this.clinicalCodes.cToothNumForm.get('toothNum8').value;
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== undefined)) {
          this._appService.ClaimCaptureService.toothNumString = this._appService.ClaimCaptureService.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.MODCODE;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4;
        }
        //rejectionCodes Edit
        if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched !== false) {
          this._appService.ClaimCaptureService.rejectCode.ADJCODE = this.adjustCodes.cRejectCodeForm.get('rejectCode').value;
          this._appService.ClaimCaptureService.rejectCode.COMMENT = this.adjustCodes.cRejectCodeForm.get('description').value;
          this._appService.ClaimCaptureService.rejectCode.ADJUST = this._appService.ClaimCaptureService.currentClaimDetail.BILLED;
          this._appService.ClaimCaptureService.RejectList.forEach((el) => {
            if (el.Rec2IDArr === this._appService.ClaimCaptureService.rejectCode.Rec2IDArr) {
              el.ADJCODE = this._appService.ClaimCaptureService.rejectCode.ADJCODE;
              el.COMMENT = this._appService.ClaimCaptureService.rejectCode.COMMENT;
              el.ADJUST = this.cDetailForm.get('billedField').value;
            }
          });

          if (this._appService.ClaimCaptureService.RejectList.length === 0) {
            this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.tempIdOnEdit;
            this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
            this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
          }
          this._appService.ClaimCaptureService.RejectList.forEach((rl) => {
            if (rl.Rec2IDArr !== this._appService.ClaimCaptureService.tempIdOnEdit) {
              this.containsElement = false;
            } else {
              this.containsElement = true;
            }
          });
          if (this.containsElement === false) {
            this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.tempIdOnEdit;
            this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
            this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
          }
        }
        if (this.claimNotes.cNoteForm.get('claimNotes').touched !== false) {
          this._appService.ClaimCaptureService.claimNotes.NOTE = this.claimNotes.cNoteForm.get('claimNotes').value;
          this._appService.ClaimCaptureService.notesLists.forEach((el) => {
            el.NOTE = this._appService.ClaimCaptureService.claimNotes.NOTE;
          });
        }
        if (this._appService.ClaimCaptureService.notesLists.length === 0) {
          this._appService.ClaimCaptureService.claimNotes.Rec2ArrID = this._appService.ClaimCaptureService.tempIdOnEdit;
          this._appService.ClaimCaptureService.claimNotes.LINE = this._appService.ClaimCaptureService.claimNotes.Rec2ArrID;
          this._appService.ClaimCaptureService.notesLists.push(this._appService.ClaimCaptureService.claimNotes);
        }
        if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched === false) {
          for (var i = 0; i < this._appService.ClaimCaptureService.RejectList.length; i++) {
            if (this._appService.ClaimCaptureService.RejectList[i].Rec2IDArr === this._appService.ClaimCaptureService.tempIdOnEdit) {
              this._appService.ClaimCaptureService.RejectList.splice(i, 1);
            }
          }
        }
        if (this.claimNotes.cNoteForm.get('claimNotes').touched === false) {
          for (var i = 0; i < this._appService.ClaimCaptureService.notesLists.length; i++) {
            if (this._appService.ClaimCaptureService.notesLists[i].Rec2ArrID === this._appService.ClaimCaptureService.tempIdOnEdit) {
              this._appService.ClaimCaptureService.notesLists.splice(i, 1);
            }
          }
        }
        this._appService.ClaimCaptureService.StoreClaimDetail("edit");
        this.claimDetailModalShow = false;

      } else {

        this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.get('qtyField').value;
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== undefined)) {
          this._appService.ClaimCaptureService.toothNumString = this._appService.ClaimCaptureService.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.MODCODE;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3;
        }
        if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== null)
          && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== undefined)) {
          this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4;
        }
        this._appService.ClaimCaptureService.StoreClaimDetail("new");

        if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched !== false) {
          this._appService.ClaimCaptureService.rejectCode = new Recordtype8Model();
          this._appService.ClaimCaptureService.rejectCode.ADJUST = this.cDetailForm.get('billedField').value;
          this._appService.ClaimCaptureService.rejectCode.ADJCODE = this.adjustCodes.cRejectCodeForm.get('rejectCode').value;
          this._appService.ClaimCaptureService.rejectCode.COMMENT = this.adjustCodes.cRejectCodeForm.get('description').value;
          this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.claimDetailList.length;
          this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
          this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
        }
        if (this.claimNotes.cNoteForm.get('claimNotes').touched !== false) {
          this._appService.ClaimCaptureService.claimNotes = new Recordtype4Model();
          this._appService.ClaimCaptureService.claimNotes.NOTE = this.claimNotes.cNoteForm.get('claimNotes').value;
          this._appService.ClaimCaptureService.claimNotes.Rec2ArrID = this._appService.ClaimCaptureService.notesLists.length + 1;
          this._appService.ClaimCaptureService.claimNotes.LINE = this._appService.ClaimCaptureService.claimNotes.Rec2ArrID;
          this._appService.ClaimCaptureService.notesLists.push(this._appService.ClaimCaptureService.claimNotes);
        }
        this._appService.ClaimCaptureService.btnClaimDetailDone = true;
        this.claimDetailModalShow = false;

      }
    }

    for (var i = 0; i < this._appService.ClaimCaptureService.RejectList.length; i++) {
      if (this._appService.ClaimCaptureService.RejectList[i].ADJCODE == null || this._appService.ClaimCaptureService.RejectList[i].ADJCODE == undefined
        || this._appService.ClaimCaptureService.RejectList[i].ADJCODE == "") {
        this._appService.ClaimCaptureService.RejectList.splice(i, 1);
      }
    }
  }

  ProcCodeValidated() {
    this._appService.ClaimCaptureService._incorrectProc = false;
    this.HideModAndClinicalCodes();
    if (this._rowEdit === true) {
      //claimDetail Edit
      this._appService.ClaimCaptureService.currentClaimDetail.PHCODE = this.cDetailForm.get('phcodes').value;
      this._appService.ClaimCaptureService.currentClaimDetail.PROCCODE = this.cDetailForm.get('PROCCODE').value;
      this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE = this.cDetailForm.get('fromDateField').value;
      this._appService.ClaimCaptureService.currentClaimDetail.TODATE = this.cDetailForm.get('ToDateField').value;
      this._appService.ClaimCaptureService.currentClaimDetail.BILLED = this.cDetailForm.get('billedField').value;
      this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.get('qtyField').value;
      this._appService.ClaimCaptureService.currentClaimDetail.DIAGREF = this.cDetailForm.get('diagrefField').value;
      this._appService.ClaimCaptureService.currentClaimDetail.MODCODE = this.modCodes.cModifiersForm.get('modCode1').value;
      this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 = this.modCodes.cModifiersForm.get('modCode2').value;
      this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 = this.modCodes.cModifiersForm.get('modCode3').value;
      this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 = this.modCodes.cModifiersForm.get('modCode4').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 = this.clinicalCodes.cToothNumForm.get('toothNum1').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 = this.clinicalCodes.cToothNumForm.get('toothNum2').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 = this.clinicalCodes.cToothNumForm.get('toothNum3').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 = this.clinicalCodes.cToothNumForm.get('toothNum4').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 = this.clinicalCodes.cToothNumForm.get('toothNum5').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 = this.clinicalCodes.cToothNumForm.get('toothNum6').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 = this.clinicalCodes.cToothNumForm.get('toothNum7').value;
      this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 = this.clinicalCodes.cToothNumForm.get('toothNum8').value;
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== undefined)) {
        this._appService.ClaimCaptureService.toothNumString = this._appService.ClaimCaptureService.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.MODCODE;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4;
      }
      //rejectionCodes Edit
      if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched !== false) {
        this._appService.ClaimCaptureService.rejectCode.ADJCODE = this.adjustCodes.cRejectCodeForm.get('rejectCode').value;
        this._appService.ClaimCaptureService.rejectCode.COMMENT = this.adjustCodes.cRejectCodeForm.get('description').value;
        this._appService.ClaimCaptureService.rejectCode.ADJUST = this._appService.ClaimCaptureService.currentClaimDetail.BILLED;
        this._appService.ClaimCaptureService.RejectList.forEach((el) => {
          if (el.Rec2IDArr === this._appService.ClaimCaptureService.rejectCode.Rec2IDArr) {
            el.ADJCODE = this._appService.ClaimCaptureService.rejectCode.ADJCODE;
            el.COMMENT = this._appService.ClaimCaptureService.rejectCode.COMMENT;
            el.ADJUST = this.cDetailForm.get('billedField').value;
          }
        });

        if (this._appService.ClaimCaptureService.RejectList.length === 0) {
          this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.tempIdOnEdit;
          this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
          this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
        }
        this._appService.ClaimCaptureService.RejectList.forEach((rl) => {
          if (rl.Rec2IDArr !== this._appService.ClaimCaptureService.tempIdOnEdit) {
            this.containsElement = false;
          } else {
            this.containsElement = true;
          }
        });
        if (this.containsElement === false) {
          this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.tempIdOnEdit;
          this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
          this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
        }
      }
      if (this.claimNotes.cNoteForm.get('claimNotes').touched !== false) {
        this._appService.ClaimCaptureService.claimNotes.NOTE = this.claimNotes.cNoteForm.get('claimNotes').value;
        this._appService.ClaimCaptureService.notesLists.forEach((el) => {
          el.NOTE = this._appService.ClaimCaptureService.claimNotes.NOTE;
        });
      }
      if (this._appService.ClaimCaptureService.notesLists.length === 0) {
        this._appService.ClaimCaptureService.claimNotes.Rec2ArrID = this._appService.ClaimCaptureService.tempIdOnEdit;
        this._appService.ClaimCaptureService.claimNotes.LINE = this._appService.ClaimCaptureService.claimNotes.Rec2ArrID;
        this._appService.ClaimCaptureService.notesLists.push(this._appService.ClaimCaptureService.claimNotes);
      }
      if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched === false) {
        for (var i = 0; i < this._appService.ClaimCaptureService.RejectList.length; i++) {
          if (this._appService.ClaimCaptureService.RejectList[i].Rec2IDArr === this._appService.ClaimCaptureService.tempIdOnEdit) {
            this._appService.ClaimCaptureService.RejectList.splice(i, 1);
          }
        }
      }
      if (this.claimNotes.cNoteForm.get('claimNotes').touched === false) {
        for (var i = 0; i < this._appService.ClaimCaptureService.notesLists.length; i++) {
          if (this._appService.ClaimCaptureService.notesLists[i].Rec2ArrID === this._appService.ClaimCaptureService.tempIdOnEdit) {
            this._appService.ClaimCaptureService.notesLists.splice(i, 1);
          }
        }
      }
      this._appService.ClaimCaptureService.StoreClaimDetail("edit");
      this.claimDetailModalShow = false;

    } else {

      this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.get('qtyField').value;
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE1;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE2;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE3;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE4;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5 !== undefined)) {
        this._appService.ClaimCaptureService.toothNumString = this._appService.ClaimCaptureService.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE5;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE6;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE7;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.toothNumString = this._appService.ClaimCaptureService.currentClaimDetail.toothNumString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.CLINICALCODE8;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.MODCODE;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3;
      }
      if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== null)
        && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== undefined)) {
        this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString = this._appService.ClaimCaptureService.currentClaimDetail.ModCodesString + '; ' + this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4;
      }
      this._appService.ClaimCaptureService.StoreClaimDetail("new");

      if (this.adjustCodes.cRejectCodeForm.get('rejectCode').touched !== false) {
        this._appService.ClaimCaptureService.rejectCode = new Recordtype8Model();
        this._appService.ClaimCaptureService.rejectCode.ADJUST = this.cDetailForm.get('billedField').value;
        this._appService.ClaimCaptureService.rejectCode.ADJCODE = this.adjustCodes.cRejectCodeForm.get('rejectCode').value;
        this._appService.ClaimCaptureService.rejectCode.COMMENT = this.adjustCodes.cRejectCodeForm.get('description').value;
        this._appService.ClaimCaptureService.rejectCode.Rec2IDArr = this._appService.ClaimCaptureService.claimDetailList.length;
        this._appService.ClaimCaptureService.rejectCode.LINE = this._appService.ClaimCaptureService.rejectCode.Rec2IDArr.toString();
        this._appService.ClaimCaptureService.RejectList.push(this._appService.ClaimCaptureService.rejectCode);
      }
      if (this.claimNotes.cNoteForm.get('claimNotes').touched !== false) {
        this._appService.ClaimCaptureService.claimNotes = new Recordtype4Model();
        this._appService.ClaimCaptureService.claimNotes.NOTE = this.claimNotes.cNoteForm.get('claimNotes').value;
        this._appService.ClaimCaptureService.claimNotes.Rec2ArrID = this._appService.ClaimCaptureService.notesLists.length + 1;
        this._appService.ClaimCaptureService.claimNotes.LINE = this._appService.ClaimCaptureService.claimNotes.Rec2ArrID;
        this._appService.ClaimCaptureService.notesLists.push(this._appService.ClaimCaptureService.claimNotes);
      }
      this._appService.ClaimCaptureService.btnClaimDetailDone = true;
      this.claimDetailModalShow = false;
    }
  }

  HideModAndClinicalCodes() {
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showToothNum = false;
  }

  EditClaimDetailData(id: number) {
    this.updateDisplay = true;
    this.rowEdit = true;
    this.adjustCodes.cRejectCodeForm.reset();
    this._appService.ClaimCaptureService.currentClaimDetail.Rec2IDArr = id;
    this.claimDetailModalShow = true;
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showNotes = false;
    this._appService.ClaimCaptureService.showReject = false;
    this._appService.ClaimCaptureService.showToothNum = false;
    this.cDetailForm.get('phcodes').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].PHCODE);
    this.cDetailForm.get('diagrefField').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].DIAGREF);
    this.cDetailForm.get('fromDateField').setValue(this._appService.ClaimCaptureService.DefaultDate);
    this.cDetailForm.get('PROCCODE').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].PROCCODE.trim());
    this.cDetailForm.get('billedField').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].BILLED);
    this.cDetailForm.get('qtyField').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].QTY);
    this.modCodes.cModifiersForm.get('modCode1').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].MODCODE);
    this.modCodes.cModifiersForm.get('modCode2').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].MODCODE2);
    this.modCodes.cModifiersForm.get('modCode3').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].MODCODE3);
    this.modCodes.cModifiersForm.get('modCode4').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].MODCODE4);
    this.clinicalCodes.cToothNumForm.get('toothNum1').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE1);
    this.clinicalCodes.cToothNumForm.get('toothNum2').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE2);
    this.clinicalCodes.cToothNumForm.get('toothNum3').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE3);
    this.clinicalCodes.cToothNumForm.get('toothNum4').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE4);
    this.clinicalCodes.cToothNumForm.get('toothNum5').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE5);
    this.clinicalCodes.cToothNumForm.get('toothNum6').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE6);
    this.clinicalCodes.cToothNumForm.get('toothNum7').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE7);
    this.clinicalCodes.cToothNumForm.get('toothNum8').setValue(this._appService.ClaimCaptureService.claimDetailList[id - 1].CLINICALCODE8);
    this._appService.ClaimCaptureService.tempIdOnEdit = id;

    this._appService.ClaimCaptureService.RejectList.forEach((el) => {
      if (el.Rec2IDArr === id) {
        this._appService.ClaimCaptureService.tempIdOnEdit = id;
        this.adjustCodes.cRejectCodeForm.controls['rejectCode'].setValue(el.ADJCODE);
        this.adjustCodes.cRejectCodeForm.controls['description'].setValue(el.COMMENT);
      }
    });
    this._appService.ClaimCaptureService.notesLists.forEach((el) => {
      if (el.Rec2ArrID === id) {
        this._appService.ClaimCaptureService.tempIdOnEdit = id;
        this.claimNotes.cNoteForm.controls['claimNotes'].setValue(el.NOTE);
      }

    });

  }

  DeleteClaimDetailData(id: number) {
    let y: number;
    let z: number;
    y = id - 1;
    if (this._appService.ClaimCaptureService.claimDetailList.length === 1) {
      this._appService.ClaimCaptureService.claimDetailList.splice(y, 1);
      for (var j = 0; j < this._appService.ClaimCaptureService.RejectList.length; j++) {
        if (this._appService.ClaimCaptureService.RejectList[j].Rec2IDArr === id) {
          this._appService.ClaimCaptureService.RejectList.splice(j, 1);
        }
      }
      for (var p = 0; p < this._appService.ClaimCaptureService.notesLists.length; p++) {
        if (this._appService.ClaimCaptureService.notesLists[p].Rec2ArrID === id) {
          this._appService.ClaimCaptureService.notesLists.splice(p, 1);
        }
      }
    } else {
      for (var j = 0; j < this._appService.ClaimCaptureService.RejectList.length; j++) {
        if (this._appService.ClaimCaptureService.RejectList[j].Rec2IDArr === id) {
          this._appService.ClaimCaptureService.RejectList.splice(j, 1);
          z = y;
          if (j >= z) {
            this._appService.ClaimCaptureService.RejectList[j].Rec2IDArr = this._appService.ClaimCaptureService.RejectList[j].Rec2IDArr - 1;
            this._appService.ClaimCaptureService.RejectList[j].LINE = this._appService.ClaimCaptureService.RejectList[j].Rec2IDArr.toString();
          }
        }

      }
      for (var d = 0; d < this._appService.ClaimCaptureService.notesLists.length; d++) {
        if (this._appService.ClaimCaptureService.notesLists[d].Rec2ArrID === id) {
          this._appService.ClaimCaptureService.notesLists.splice(d, 1);
          z = y;
          if (d >= z) {
            this._appService.ClaimCaptureService.notesLists[d].Rec2ArrID = this._appService.ClaimCaptureService.notesLists[d].Rec2ArrID - 1;
            this._appService.ClaimCaptureService.notesLists[d].LINE = this._appService.ClaimCaptureService.notesLists[d].Rec2ArrID;
          }
        }

      }
      this._appService.ClaimCaptureService.claimDetailList.splice(y, 1);
      for (var i = 0; i < this._appService.ClaimCaptureService.claimDetailList.length; i++) {
        if (i >= y) {

          this._appService.ClaimCaptureService.claimDetailList[i].Rec2IDArr = this._appService.ClaimCaptureService.claimDetailList[i].Rec2IDArr - 1;
        }
      }
    }
    this._appService.ClaimCaptureService.CalculateBilledAmount();
  }

  CancelDetail() {
    this.HideModAndClinicalCodes();
    this.cDetailForm.reset();
    this.errorMsgBilled = '';
    this.errorMsgDIAGREF = '';
    this.errorMsgFromdate = '';
    this.errorMsgProccode = '';
    this.errorMsgQTY = '';
    this._appService.ClaimCaptureService.CancelCLaimDetail();
    this.claimDetailModalShow = false;
    this.validForm = false;
  }

  NewClaimDetailLine() {
    this.updateDisplay = false;
    this.rowEdit = false;
    this.HideModAndClinicalCodes();
    this.claimDetailModalShow = true;
    this._appService.ClaimCaptureService.NewClaimDetail();
    this._appService.ClaimCaptureService.enableWizardNext = false;
    this.errorMsgBilled = '';
    this.errorMsgDIAGREF = '';
    this.errorMsgFromdate = '';
    this.errorMsgProccode = '';
    this.errorMsgQTY = '';
    let p = this._appService.ClaimCaptureService.currentClaimDetail.PHCODE;
    let ctl = this.cDetailForm.get('phcodes');
    ctl.setValue(p);
    this.validForm = false;
    this.cDetailForm.controls['qtyField'].setValue('1');
    this._appService.ClaimCaptureService.currentClaimDetail.QTY = this.cDetailForm.controls['qtyField'].value;
    this.modCodes.errorMsgModecode2 = '';
    this.modCodes.errorMsgModecode3 = '';
    this.modCodes.errorMsgModecode4 = '';
    this.modCodes.errorMsgModecode1 = '';
    this.clinicalCodes.errMsgTooth1 = '';
    this.clinicalCodes.errMsgTooth2 = '';
    this.clinicalCodes.errMsgTooth3 = '';
    this.clinicalCodes.errMsgTooth4 = '';
    this.clinicalCodes.errMsgTooth5 = '';
    this.clinicalCodes.errMsgTooth6 = '';
    this.clinicalCodes.errMsgTooth7 = '';
    this.clinicalCodes.errMsgTooth8 = '';
    this.clinicalCodes.cToothNumForm.reset();
    this.claimNotes.cNoteForm.reset();
    this.adjustCodes.cRejectCodeForm.reset();
    this._appService.ClaimCaptureService.rejectCode = new Recordtype8Model();
    this.modCodes.cModifiersForm.reset();
    this._appService.ClaimCaptureService.showModCode = false;
    this._appService.ClaimCaptureService.showNotes = false;
    this._appService.ClaimCaptureService.showReject = false;
    this._appService.ClaimCaptureService.showToothNum = false;
    this.claimNotes.cNoteForm.reset();
    this._appService.ClaimCaptureService.claimNotes = new Recordtype4Model();
    this.cDetailForm.reset();
    this.cDetailForm.controls['qtyField'].setValue('1');
    this.cDetailForm.controls['fromDateField'].setValue(this._appService.ClaimCaptureService.DefaultDate);
    this.cDetailForm.controls['ToDateField'].setValue(this._appService.ClaimCaptureService.DefaultDate);
    this._appService.ClaimCaptureService.currentClaimDetail.FROMDATE = this.cDetailForm.get('fromDateField').value;
    this._appService.ClaimCaptureService.currentClaimDetail.TODATE = this.cDetailForm.get('ToDateField').value;
  }

  DiagCodeChange() {
    this.tempDiagCode = this.cDetailForm.get('diagrefField').value;
    this._appService.ClaimCaptureService.diagnosticCodesList.forEach((el) => {
      if (el.DIAG === this._tempDiagCode) {
        this._appService.ClaimCaptureService.currentClaimDetail.DIAGREF = el.DIAGREF;
      }
    });
  }

  TempValidFormMethod() {
    if ((this.cDetailForm.get('billedField').valid === true) && (this.cDetailForm.get('qtyField').valid === true)
      && (this.cDetailForm.get('fromDateField').valid === true) && (this.cDetailForm.get('PROCCODE').valid === true)) {
      this.validForm = true;
    } else {
      this.validForm = false;
    }
  }

  OpenEditBankingDetails() {
    if (this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'M') {
      this.banking._component = "CLAIMCAPTURE";
      this._appService.CheckRunService._paymentType = 'M';
      this._appService.ClaimCaptureService._openBanking = true;
      this.banking.bankinDetailsForm.reset();
      this._appService.CheckRunService._MembName = "";
      this._appService.CheckRunService._provId = "";
      this._appService.CheckRunService._provName = "";
      this._appService.ClaimCaptureService.newBankingDetails = new BankingDetailModel();
      //this._appService.CheckRunService._MembName = this._appService.ClaimCaptureService.dataBaseMessageMemb; // Jaco comment out 2024-01-05
      this._appService.CheckRunService._provId = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPMEMB;
      this._appService.CheckRunService._provName = "";
    }
    if (this._appService.ClaimCaptureService.currentClaimHeader.CLAIMTYPE == 'P') {
      this._appService.CheckRunService._paymentType = 'P';
      this.banking._component = "CLAIMCAPTURE";
      this._appService.ClaimCaptureService._openBanking = true;
      this.banking.bankinDetailsForm.reset();
      this._appService.CheckRunService._MembName = "";
      this._appService.CheckRunService._provId = "";
      this._appService.CheckRunService._provName = "";
      this._appService.ClaimCaptureService.newBankingDetails = new BankingDetailModel();
      this._appService.CheckRunService._provId = this._appService.ClaimCaptureService.currentClaimHeader.EZCAPPROV;
      this._appService.CheckRunService._provName = this._appService.ClaimCaptureService.dataBaseMessageProv;
    }

  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, ValidationErrors, FormBuilder } from '@angular/forms';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-modifiers',
  templateUrl: './modifiers.component.html',
  styleUrls: ['./modifiers.component.css']
})
export class ModifiersComponent implements OnInit {

  @Input() cModifiersForm: FormGroup;

  private _errorMsgModecode1: any = '';
  private _errorMsgModecode2: any = '';
  private _errorMsgModecode3: any = '';
  private _errorMsgModecode4: any = '';
  public _errorMsg: ValidationErrors;
  public _arrModCodes: string[] = [];

  constructor(private fb: FormBuilder, public _appService: AppService) {
    this._errorMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      double: 'May not have fields with he same values!',
      valid: 'valid'
    };

  }

  ngOnInit() {

    this.cModifiersForm = this.fb.group({
      modCode1: [],
      modCode2: [],
      modCode3: [],
      modCode4: []
    });

    this.cModifiersForm.valueChanges.subscribe(
      (changes) => {
        this.ValidateForm(changes);
      });

  }

  public get errorMsgModecode1(): any {
    return this._errorMsgModecode1;
  }
  public set errorMsgModecode1(value: any) {
    this._errorMsgModecode1 = value;
  }

  public get errorMsgModecode2(): any {
    return this._errorMsgModecode2;
  }
  public set errorMsgModecode2(value: any) {
    this._errorMsgModecode2 = value;
  }

  public get errorMsgModecode3(): any {
    return this._errorMsgModecode3;
  }
  public set errorMsgModecode3(value: any) {
    this._errorMsgModecode3 = value;
  }


  public get errorMsgModecode4(): any {
    return this._errorMsgModecode4;
  }
  public set errorMsgModecode4(value: any) {
    this._errorMsgModecode4 = value;
  }


  ValidateForm(changes: any) {

    if (this.cModifiersForm.get('modCode1').dirty === true) {
      this.ValidateModeCode1();
    }
    if (this.cModifiersForm.get('modCode2').dirty === true) {
      this.ValidateModeCode2();
    }
    if (this.cModifiersForm.get('modCode3').dirty === true) {
      this.ValidateModeCode3();
    }
    if (this.cModifiersForm.get('modCode4').dirty === true) {
      this.ValidateModeCode4();
    }
  }

  ValidateModeCode1() {
    this.errorMsgModecode1 = '';
    this._appService.ClaimCaptureService.modCode.modcode = this.cModifiersForm.get('modCode1').value;
    this._appService.ClaimCaptureService.currentClaimDetail.MODCODE = this.cModifiersForm.get('modCode1').value;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE !== null)) {
      this._arrModCodes[0] = this.cModifiersForm.get('modCode1').value;
      this.errorMsgModecode1 = this.FieldValidation(this._appService.ClaimCaptureService.modCode.modcode, 'modCode1');
      if (this.errorMsgModecode1 === 'valid') {
        this.errorMsgModecode1 = '';
        this.errorMsgModecode1 = this._appService.ClaimCaptureService.DataBaseCheckModCode1();
      }
    }
    this.cModifiersForm.get('modCode1').markAsPristine();
  }

  ValidateModeCode2() {
    this.errorMsgModecode2 = '';
    this._appService.ClaimCaptureService.modCode.modcode = this.cModifiersForm.get('modCode2').value;
    this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 = this.cModifiersForm.get('modCode2').value;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE2 !== null)) {
      this._arrModCodes[1] = this.cModifiersForm.get('modCode2').value;
      this.errorMsgModecode2 = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.MODCODE, 'modCode2');
      if (this.errorMsgModecode2 === 'valid') {
        this.errorMsgModecode2 = '';
        this.errorMsgModecode2 = this._appService.ClaimCaptureService.DataBaseCheckModCode2();
      }
    }
    this.cModifiersForm.get('modCode2').markAsPristine();
  }

  ValidateModeCode3() {
    this.errorMsgModecode3 = '';
    this._appService.ClaimCaptureService.modCode.modcode = this.cModifiersForm.get('modCode3').value;
    this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 = this.cModifiersForm.get('modCode3').value;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE3 !== null)) {
      this._arrModCodes[2] = this.cModifiersForm.get('modCode3').value;
      this.errorMsgModecode3 = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.MODCODE, 'modCode3');
      if (this.errorMsgModecode3 === 'valid') {
        this.errorMsgModecode3 = this._appService.ClaimCaptureService.DataBaseCheckModCode3();
      }
    }
    this.cModifiersForm.get('modCode3').markAsPristine();
  }

  ValidateModeCode4() {
    this.errorMsgModecode4 = '';
    this._appService.ClaimCaptureService.modCode.modcode = this.cModifiersForm.get('modCode4').value;
    this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 = this.cModifiersForm.get('modCode4').value;
    if ((this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== '') && (this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4 !== null)) {
      this._arrModCodes[3] = this.cModifiersForm.get('modCode4').value;
      this.errorMsgModecode4 = this.FieldValidation(this._appService.ClaimCaptureService.currentClaimDetail.MODCODE4, 'modCode4');
      if (this.errorMsgModecode4 === 'valid') {
        this.errorMsgModecode4 = '';
        this.errorMsgModecode4 = this._appService.ClaimCaptureService.DataBaseCheckModCode4();
      }
    }
    this.cModifiersForm.get('modCode4').markAsPristine();
  }

  FieldValidation(fieldName: string, controlName: string) {
    if ((fieldName !== '') && (fieldName !== null) && (fieldName !== undefined)) {


      if (fieldName.length > 7) {
        this.cModifiersForm.get(controlName).setErrors(this._errorMsg['maxLength']);
        return this._errorMsg['maxLength'];
      }
    }
    return this._errorMsg['valid'];
  }

}

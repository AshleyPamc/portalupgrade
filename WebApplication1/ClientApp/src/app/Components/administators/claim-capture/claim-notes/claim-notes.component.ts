import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AppService } from './../../../../services/app.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EditBankingDetailComponent } from '../../checkrun/edit-banking-detail/edit-banking-detail.component';
import { BankingDetailModel } from './../../../../models/BankingDetailModel';

@Component({
  selector: 'app-claim-notes',
  templateUrl: './claim-notes.component.html',
  styleUrls: ['./claim-notes.component.css']
})
export class ClaimNotesComponent implements OnInit {

  @Input() cNoteForm: FormGroup;

  constructor(public _appService: AppService, public fb: FormBuilder) { }

  ngOnInit() {
    this.cNoteForm = this.fb.group({
      claimNotes: [],
    });
  }

  saveNote() {
    this._appService.ClaimCaptureService.claimNotes.NOTE = this.cNoteForm.get('claimNotes').value;
  }

}

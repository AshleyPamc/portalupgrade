import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/app.service';

@Component({
  selector: 'app-display-claim-header',
  templateUrl: './display-claim-header.component.html',
  styleUrls: ['./display-claim-header.component.css']
})
export class DisplayClaimHeaderComponent implements OnInit {

  constructor(public _appService:AppService) { }

  ngOnInit() {
  }

}

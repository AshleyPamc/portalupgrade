import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from "@clr/angular";
import { SharedModule } from './../../../shared.module';
import { JoyrideModule } from 'ngx-joyride';
import { LogNewAuthComponent } from './log-new-auth.component';




const routes: Routes = [{

  path: '',
  component: LogNewAuthComponent,
  children: [
    { pathMatch: 'full', path: 'Auth-log', component: LogNewAuthComponent }
  ]

}];

@NgModule({
  declarations: [LogNewAuthComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    JoyrideModule.forRoot()

  ],
  exports: [FormsModule, ReactiveFormsModule]
})
export class LogauthModule { }

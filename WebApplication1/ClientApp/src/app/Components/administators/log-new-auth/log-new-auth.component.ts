import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { AppService } from '../../../services/app.service';
import { HealthplanModel } from '../../../models/HealthplanModel';
//import { AuthorizationComponent } from '../authorization/authorization.component';
import { AuthModel } from '../../../models/AuthModel';
import { Specmasters } from '../../../models/specmasters';
import { SpecCodes } from '../../../models/SpecCodes';
import { JoyrideService } from 'ngx-joyride';
import { AuthSpecDetail } from '../../../models/AuthSpecDetail';
import { ValidationResultsModel } from '../../../models/ValidationResultsModel';
import { MemberEligibilityModel } from '../../../models/MemberEligibilityModel';
import { Diagcodes } from '../../../models/diagcodes';
import { ValidationService } from '../../../services/validation.service';

@Component({
  selector: 'app-log-new-auth',
  templateUrl: './log-new-auth.component.html',
  styleUrls: ['./log-new-auth.component.css']
})
export class LogNewAuthComponent implements OnInit {

  //@ViewChild('AuthCapture', { static:false }) AuthCapture: AuthorizationComponent;
  authHeaderForm: FormGroup;
  authDetailForm: FormGroup;
  private errMsg: ValidationErrors;

  private _selectedHeader: Specmasters = new Specmasters();
  //public MembMessage: string;
  //public _memberror: boolean = false;
  //public _selectedSpec: SpecCodes = new SpecCodes();

  private _openNew: boolean = false;

  private _selectedHpName: string = '';

  private _provValid: boolean = false;
  private _provErrorMsg: string = '';
  private _provName: string = '';
  private _provNetwork: string = '';
  private _dateErrorMsg: string = '';

  private _familyList: MemberEligibilityModel[] = [];
  private _membValid: boolean = false;
  private _membErrorMsg: string = '';
  private _familyOpt: string = '';
  private _depOpthruDt: string = '';

  private _outOfNetwork: boolean = false;
  private _outOfNetworkMsg: string = "";

  private _amtMsg: string = "";

  public get selectedHeader(): Specmasters {
    return this._selectedHeader;
  }
  public set selectedHeader(value: Specmasters) {
    this._selectedHeader = value;
  }

  constructor(public _appService: AppService, private fb: FormBuilder, private readonly joyrideService: JoyrideService, private _validationService: ValidationService) {
    this._appService.LoginService.GetExistingBenefits();
    this.errMsg = {
      'required': 'This field is required',
      'invalidMemb': 'Invalid family/policy number',
      'provSpec': 'Invalid Provider/Speciality',
      'maxLength': 'Exceeded max length',
      'number': 'This field requires a number',
      'amountLimit': 'Amount larger than the limit available',
      'amountInvalid': 'Invalid amount entered',
      'network': 'Out of network provider',
      'invalidProc': 'Invalid procedure code',
      'searchError': 'Unexpected error occured',
    }

    // Initilaise forms
    this.AuthHeaderFormInit();

    this.AuthDetailFormInit(); // Wrap the form creation in a method, because when we reset the form later, the values are set to null instead of '', so then we can call the method for initial values.
      
  }

  async ngOnInit() {
 
    // Load setup
    if (this._appService.AuthorizationService.HpCodesList.length < 1) {
      await this._appService.AuthorizationService.HpCodes();
    }

    if (this._appService.ClaimCaptureService.DiagCodesList.length < 1) {
      this._appService.ClaimCaptureService.LoadAllDiagCodes();
    }

    this.AuthHeaderFormInit(); // For hpcodes
    
  }

  AuthHeaderFormInit() {
    this.authHeaderForm = this.fb.group({
      'hp': ['', Validators.required],
      'authSet': ['', Validators.required],
      'member': ['', Validators.required],
      'dependant': ['', Validators.required]
    });

    if (this._appService.AuthorizationService.HpCodesList.length == 1) {
      this.authHeaderForm.controls['hp'].markAsTouched();
      this.authHeaderForm.controls['hp'].setValue(this._appService.AuthorizationService.HpCodesList[0].code);
      let c: HealthplanModel = new HealthplanModel();
      //this.newAuth.valid
      c.code = this.authHeaderForm.get('hp').value;
      this._appService.AuthorizationService.authDetail.hpcode = this.authHeaderForm.get('hp').value;
      this._appService.AuthorizationService.GetAuthSetListForHp(c);
    }
  }

  AuthDetailFormInit() {
    
    this.authDetailForm = this.fb.group({
      'provider': ['', Validators.required],
      'service': ['', Validators.required],
      'proc': ['', Validators.required],
      'amount': ['', [Validators.required, Validators.maxLength(20)]],
      'diag1': ['', Validators.maxLength(10)],
      'diag2': ['', Validators.maxLength(10)],
      'diag3': ['', Validators.maxLength(10)],
      'diag4': ['', Validators.maxLength(10)],
      'qty': [1, [Validators.required, Validators.min(1), Validators.max(9999), this._validationService.intCustomValidator.bind(this)]], // SQL decimal(6,2)
      'authRefNo': ['', Validators.maxLength(16)],
      'note': ['', Validators.maxLength(100)],
    });    
  }

  onClick() {
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['LogA_1', 'LogA_2', 'LogA_3', 'LogA_4', 'LogA_5', 'LogA_6', 'LogA_7', 'LogA_8'],
        themeColor: '#313131'
      })
    }
  }  

  ValidateDiagCode(controlName: string) {
    this.authDetailForm.get(controlName).setValue(this.authDetailForm.get(controlName).value.trim());

    let diagCodes: Diagcodes[] = this._appService.ClaimCaptureService.DiagCodesList.filter(x => x.code.toUpperCase() == this.authDetailForm.get(controlName).value.toUpperCase());

    if (diagCodes.length > 0) {

      if (controlName == 'diag1') {
        this.authDetailForm.get(controlName).setValue(diagCodes[0].code);
        this._appService.AuthorizationService.authDetail.diag1 = diagCodes[0].code;
        this._appService.AuthorizationService.authDetail.diag1desc = diagCodes[0].desc;
      } else if (controlName == 'diag2') {
        this.authDetailForm.get(controlName).setValue(diagCodes[0].code);
        this._appService.AuthorizationService.authDetail.diag2 = diagCodes[0].code;
        this._appService.AuthorizationService.authDetail.diag2desc = diagCodes[0].desc;
      } else if (controlName == 'diag3') {
        this.authDetailForm.get(controlName).setValue(diagCodes[0].code);
        this._appService.AuthorizationService.authDetail.diag3 = diagCodes[0].code;
        this._appService.AuthorizationService.authDetail.diag3desc = diagCodes[0].desc;
      } else if (controlName == 'diag4') {
        this.authDetailForm.get(controlName).setValue(diagCodes[0].code);
        this._appService.AuthorizationService.authDetail.diag4 = diagCodes[0].code;
        this._appService.AuthorizationService.authDetail.diag4desc = diagCodes[0].desc;
      } 
    } else {
      if (controlName == 'diag1') {
        this._appService.AuthorizationService.authDetail.diag1 = "";
        this._appService.AuthorizationService.authDetail.diag1desc = "";
        if (this.authDetailForm.get(controlName).value != '') {
          this.authDetailForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } else if (controlName == 'diag2') {
        this._appService.AuthorizationService.authDetail.diag2 = "";
        this._appService.AuthorizationService.authDetail.diag2desc = "";
        if (this.authDetailForm.get(controlName).value != '') {
          this.authDetailForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } else if (controlName == 'diag3') {
        this._appService.AuthorizationService.authDetail.diag3 = "";
        this._appService.AuthorizationService.authDetail.diag3desc = "";
        if (this.authDetailForm.get(controlName).value != '') {
          this.authDetailForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } else if (controlName == 'diag4') {
        this._appService.AuthorizationService.authDetail.diag4 = "";
        this._appService.AuthorizationService.authDetail.diag4desc = "";
        if (this.authDetailForm.get(controlName).value != '') {
          this.authDetailForm.get(controlName).setErrors({ 'incorrect': true });
        }
      } 
    }
  }

  HpChange() {

    this._selectedHpName = '';

    // Reset auth set related
    this._appService.AuthorizationService.authSetList = [];
    this.authHeaderForm.get('authSet').reset();

    // Reset member related
    this._membErrorMsg = '';
    this._familyOpt = '';
    this._depOpthruDt = '';

    this.authHeaderForm.get('member').reset();

    // Reset Dependant
    this._familyList = [];
    this.authHeaderForm.get('dependant').reset();

    // Reset auth model
    this._appService.AuthorizationService.authDetail.id = '';
    this._appService.AuthorizationService.authDetail.desc = '';
    this._appService.AuthorizationService.authDetail.membno = '';
    this._appService.AuthorizationService.authDetail.dep = '';
    this._appService.AuthorizationService.authDetail.opt = ''

    this.authHeaderForm.get('hp').markAsTouched();
    let c: HealthplanModel = new HealthplanModel();
    c.code = this.authHeaderForm.get('hp').value;
    this._appService.AuthorizationService.authDetail.hpcode = this.authHeaderForm.get('hp').value;
    this._appService.AuthorizationService.GetAuthSetListForHp(c);
    //this.newAuth.get('authSet').markAsTouched(); //Jaco 2022-09-12
  }

  AuthSetChange() {
    
    this._appService.AuthorizationService.authDetail.desc = '';
    if (this.authHeaderForm.get('authSet').value.trim() != '') {      
      this._appService.AuthorizationService.authDetail.id = this.authHeaderForm.get('authSet').value;
      this.authHeaderForm.get('authSet').markAsTouched();
    }
    else {
      this._appService.AuthorizationService.authDetail.id = ''
      this.authHeaderForm.get('authSet').setErrors(this.errMsg['required']);
    }
  }

  ValidateMember() {

    // Reset member related
    this._membErrorMsg = ''
    this._familyOpt = '';
    this._depOpthruDt = '';
    this._appService.AuthorizationService.authDetail.membno = '';
    this._appService.AuthorizationService.authDetail.opt = '';

    // Reset dependant related   
    this._familyList = [];
    this.authHeaderForm.get('dependant').reset();
    this._appService.AuthorizationService.authDetail.dep = '';

    // Start validation
    let c = this.authHeaderForm.get('member').value;
    if ((c == '') || (c == undefined) || (c == null)) {      
      this._membErrorMsg = this.errMsg['required'];
      this.authHeaderForm.get('member').setErrors(this.errMsg['required']);
    } else {
      //Modal Vars
      this._appService.AuthorizationService.authDetail.membno = c;
      this._appService.AuthorizationService.authDetail.username = this._appService.LoginService.CurrentLoggedInUser.username;
     
      // Api Call      
      this._appService.AuthorizationService.busy = true;
      this._appService.MembershipService.SearchEligibleMemberApi(this._appService.AuthorizationService.authDetail.membno, '', '', "1900/01/01", this._appService.AuthorizationService.authDetail.hpcode)
        .subscribe(
          (results: MemberEligibilityModel[]) => {

            results.forEach((el) => {
              if (el.hpcode == this._appService.AuthorizationService.authDetail.hpcode) { // to not include DRUM members (or any old hpcodes)
                this._familyList.push(el);
              }
            })

            if (this._familyList.length > 0 && this._familyList[0].hpcode == this._appService.AuthorizationService.authDetail.hpcode) {

              this._familyOpt = this._familyList[0].opt;
              this._membValid = true;
              this._membErrorMsg = '';
              this.authHeaderForm.get('member').setErrors(null);
            }
            else {
              this._membValid = false;
              this._membErrorMsg = this.errMsg['invalidMemb'];
              this.authHeaderForm.get('member').setErrors(this.errMsg['invalidMemb']);
            }

          },
          (error) => {
            this._membValid = false;
            this._membErrorMsg = this.errMsg['searchError'];
            this.authHeaderForm.get('member').setErrors(this.errMsg['searchError']);
            this._appService.AuthorizationService.busy = false;
            console.log(error)
          },
          () => {

            this._appService.AuthorizationService.busy = false;
                       
          }
        )      
    }
  }

  GetMemberFamily() {
    // Api Call
    this._appService.AuthorizationService.busy = true;
    this._appService.AuthorizationService.GetMemberFamily()
      .subscribe(
        (results: MemberEligibilityModel[]) => {

          let c: MemberEligibilityModel = new MemberEligibilityModel();
          results.forEach((el) => {
            this._familyList.push(el);
          })

          this._familyOpt = this._familyList[0].opt;

        },
        (error) => {
          console.log(error)
        },
        () => {
          this._appService.AuthorizationService.busy = false;
        }
      )
  }

  DepChange() {
    this._depOpthruDt = '';

    this._appService.AuthorizationService.authDetail.dep = this.authHeaderForm.get('dependant').value;
    this._appService.AuthorizationService.authDetail.membno =
      this._familyList.filter(x => x.dependant == this.authHeaderForm.get('dependant').value)[0].membid;    
    this._appService.AuthorizationService.authDetail.opt =
      this._familyList.filter(x => x.dependant == this.authHeaderForm.get('dependant').value)[0].opt;

    if (this._familyList.filter(x => x.dependant == this.authHeaderForm.get('dependant').value)[0].opthrudt != "N/A") {
      this._depOpthruDt = "Termination Date: " + this._familyList.filter(x => x.dependant == this.authHeaderForm.get('dependant').value)[0].opthrudt;
    }
  }

  CreateNewAuth() {

    // Get description of auth type (GP / Specialists etc)
    this._appService.AuthorizationService.authSetList.forEach((el) => {
      if (el.id == Number(this._appService.AuthorizationService.authDetail.id.trim())) {
        this._appService.AuthorizationService.authDetail.desc = el.desc;
      }
    });
        
    this._appService.AuthorizationService.HpCodesList.forEach((el) => {
      if (el.code == this._appService.AuthorizationService.authDetail.hpcode) {        
        this._selectedHpName = el.name;
      }
    })

    // Create Membid
    //this._appService.AuthorizationService.authDetail.membno = this.authHeaderForm.get('member').value.ToUpper() + "-" + this.authHeaderForm.get('dependant').value;

    // Move to authservice.GetDetailsBen()
    //this._appService.AuthorizationService.displayGrid = true;


    //this.newAuth.disable(); // Was commented out


    //this.SubmitAuthHeader();
    //let auth: AuthModel = new AuthModel();
    //auth.id = this.selectedHeader.id.toString();
    //auth.id = this._appService.AuthorizationService.authDetail.id;
    //auth.membno = this._appService.AuthorizationService.authDetail.membno + "-" + this._appService.AuthorizationService.authDetail.dep;
    //auth.username = this._appService.LoginService.CurrentLoggedInUser.username;
    //auth.username = this._appService.AuthorizationService.authDetail.username;
    //auth.hpcode = this._appService.AuthorizationService.authDetail.hpcode;
    //auth.desc = this.selectedHeader.desc;

    // Move to authservice.GetDetailsBen()
    //this._appService.AuthorizationService.SubmitAuthHeader(auth);

    //Jaco 2022-09-12 moved from BenefitSetChange() because we first need the HPCODE and OPT
    // Get list op specs to display for user on grid
    let d: Specmasters = new Specmasters();
    d.id = parseInt(this._appService.AuthorizationService.authDetail.id);
    d.hpcode = this._appService.AuthorizationService.authDetail.hpcode;
    d.opt = this._appService.AuthorizationService.authDetail.opt;
    this._appService.AuthorizationService.GetDetailsBen(d);
    //

    // Temp comment out
    //this._appService.AuthorizationService.selectedSpecs.forEach((el) => {

    //  el.provId = "";
    //  el.srvDate = "";
    //  el.amount = "";
    //  el.selectedBenCode = "";
    //  el.selectedProcCode = "";

    //});
  }

  Search() {
    this.selectedHeader = new Specmasters();
    this.selectedHeader.id = this.authHeaderForm.get('authSet').value;
    this._appService.AuthorizationService.authSetList.forEach((el) => {
      if (el.id == this.selectedHeader.id) {
        this.selectedHeader.desc = el.desc;
      }
    });
    this.selectedHeader.hpcode = this.authHeaderForm.get('hp').value;
    let auth: AuthModel = new AuthModel();
    auth.id = this.selectedHeader.id.toString();
    //auth.membno = this.authHeaderForm.get('member').value + "-" + this.authHeaderForm.get('dependant').value; // Can be with or without our prefix, so don't use
    auth.membno = this._appService.AuthorizationService.authDetail.membno;
    auth.username = this._appService.LoginService.CurrentLoggedInUser.username;
    auth.hpcode = this._appService.AuthorizationService.authDetail.hpcode;
    auth.desc = this.selectedHeader.desc;
    this._appService.AuthorizationService.SearchSpecificAuth(auth);
  }

  ClearAll() {
    // reset form
    this.authHeaderForm.reset();
    this.AuthHeaderFormInit();

    // reset lists
    this._appService.AuthorizationService.authSetList = [];
    this._familyList = [];

    // reset vars
    this._selectedHpName = '';

    this._provValid = false;
    this._provErrorMsg = '';
    this._provName = '';
    this._provNetwork = '';
    this._dateErrorMsg = '';

    this._membValid = false;
    this._membErrorMsg = '';
    this._familyOpt = '';

    this._outOfNetwork = false;
    this._outOfNetworkMsg = "";

    this._amtMsg = "";

    // reset auth modal
    this._appService.AuthorizationService.authDetail = new AuthModel();
       
    // set default hpcode - moved to this.AuthHeadeFormInit();
    //if (this._appService.AuthorizationService.HpCodesList.length == 1) {
    //  this.authHeaderForm.controls['hp'].markAsTouched();
    //  this.authHeaderForm.controls['hp'].setValue(this._appService.AuthorizationService.HpCodesList[0].code);
    //  let c: HealthplanModel = new HealthplanModel();
    //  c.code = this.authHeaderForm.get('hp').value;
    //  this._appService.AuthorizationService.authDetail.hpcode = this.authHeaderForm.get('hp').value;
    //  this._appService.AuthorizationService.GetAuthSetListForHp(c);
    //}   
  }

  newAuthSubmision(data: SpecCodes) {

    // Reset values
    
    //this.authDetailForm.reset(); // Jaco 2024-09-13
    this.AuthDetailFormInit(); // Jaco 2024-09-13
    
    this.authDetailForm.controls['proc'].disable();    
    
    this._appService.AuthorizationService.selectedSpec = data;
    this._appService.AuthorizationService.defaultProc = '';
    this._appService.AuthorizationService.authDetail.phCodeDesc = "";
    this._appService.AuthorizationService.authDetail.procCode = ''
    this._appService.AuthorizationService.authDetail.enterAmount = false;
    this._appService.AuthorizationService.remainingAmount = '';
    this._appService.AuthorizationService.remainingAmountMessage = '';
       
    // Do detail setup
    if (data.specDetail.length == 0) {                                                    //Jaco 2022-08-05
      this._appService.AuthorizationService.defaultProc = 'No Proc Code (Invalid setup)';
      
      this.authDetailForm.controls['proc'].setValue('No Proc Code (Invalid setup)');
     
      this.authDetailForm.controls['proc'].disable();
      
      this.authDetailForm.controls['proc'].setErrors(this.errMsg['invalidProc']);
    }
    else if (data.specDetail.length == 1) { // Set defaults if there is only one specdetail record
      this._appService.AuthorizationService.defaultProc = data.specDetail[0].procCode;
      
      this.authDetailForm.controls['proc'].setValue(data.specDetail[0].procCode);
      
      this.authDetailForm.controls['proc'].disable();
      this._appService.AuthorizationService.authDetail.procCode = data.specDetail[0].procCode;
      this._appService.AuthorizationService.authDetail.availableBenefitToAmtCheck = data.specDetail[0].availableBenefitToAmtCheck;
      this._appService.AuthorizationService.authDetail.displayAvailableBenefit = data.specDetail[0].displayAvailableBenefit;
      this._appService.AuthorizationService.authDetail.placeSvc = data.specDetail[0].placeSvcCode;
      this._appService.AuthorizationService.authDetail.includeLengthOfStay = data.specDetail[0].includeLengthOfStay;
      if (data.specDetail[0].enterAmount == false) {
        this.authDetailForm.get('amount').markAsTouched();
        this.authDetailForm.get('amount').setValue('0');
        this.authDetailForm.get('amount').setErrors(null);
        this._appService.AuthorizationService.authDetail.enterAmount = false;
      }
      else {
        this._appService.AuthorizationService.authDetail.enterAmount = true;
      }
    }
    else {
      this._appService.AuthorizationService.defaultProc = '';
      this._appService.AuthorizationService.authDetail.includeLengthOfStay = data.specDetail[0].includeLengthOfStay;
    }

    this._appService.AuthorizationService.authDetail.spec = data.code;

    this._appService.AuthorizationService.authDetail.specDesc = data.desc;
        
    this._appService.AuthorizationService.authDetail.amount = '';

    this._openNew = true;
  }

  ValidateProviderWithSpec() {

    this._provNetwork = "";
    this._appService.AuthorizationService.authDetail.reject = false;
    let c = this.authDetailForm.get('provider').value;

    // Check if data is entered
    if ((c == '') || (c == undefined) || (c == null)) {
      this._provErrorMsg = this.errMsg['required'];
      this._appService.AuthorizationService.AuthProvMsg.message = this.errMsg['required'];
      this.authDetailForm.get('provider').setErrors(this.errMsg['required']);
    } else {

      this._appService.AuthorizationService.authDetail.provid = c;

      // modal vars
      this._appService.AuthorizationService.busy = true;

      // Api Call
      this._appService.AuthorizationService.ValidateProvWithSpec()
        .subscribe(
          (result: ValidationResultsModel) => {

            if (result.valid == false) {

              this._provValid = false;
              this._provErrorMsg = "Invalid Provider/Speciality";
              this.authDetailForm.get('provider').setErrors(this.errMsg['provSpec']);
              this._provName = "";
              this.authDetailForm.controls['proc'].disable();
              this._appService.AuthorizationService.authDetail.phCodeDesc = "";
            }
            else {

              this._provValid = true;
              this._provErrorMsg = "";
              this.authDetailForm.get('provider').setErrors(null);

              let c: string[] = [];
              c = result.message.split(',');

              this._provName = c[0];

              if (c[1] == '2') {
                this._provNetwork = "IN NETWORK PROVIDER";
               
                this._appService.AuthorizationService.selectedSpec.specDetail.forEach((el) => {
                  el.phCodeDetermined = el.phCodeOnNetwork;
                  el.phCodeDeterminedDesc = el.phCodeOnNetworkDesc;
                  this.authDetailForm.controls['proc'].enable();
                });

              } else {
                this._provNetwork = "OUT OF NETWORK PROVIDER";
               
                //Jaco 2024-10-28 start
                if (this._appService.AuthorizationService.selectedSpec.id == 14) { // id 14 is AUL gp auths
                  this._appService.AuthorizationService.OutOfNetGpVisitCheck(this._familyList[0].subssn);
                }
                //Jaco 2024-10-28 end

                this._appService.AuthorizationService.selectedSpec.specDetail.forEach((el) => {
                  el.phCodeDetermined = el.phCodeOffNetwork;
                  el.phCodeDeterminedDesc = el.phCodeOffNetworkDesc;
                  this.authDetailForm.controls['proc'].enable();
                });
              }

              // Set for GP's or if there is only one
              if (this._appService.AuthorizationService.selectedSpec.specDetail.length == 1) {
                this._appService.AuthorizationService.authDetail.phCode = this._appService.AuthorizationService.selectedSpec.specDetail[0].phCodeDetermined;
                this._appService.AuthorizationService.authDetail.phCodeDesc = this._appService.AuthorizationService.selectedSpec.specDetail[0].phCodeDeterminedDesc;
              }
                   
              // Check limit if proc code, service date, and practice no is entered and then get bentype and avail limit
              if (this._appService.AuthorizationService.authDetail.scvdate != undefined && this._appService.AuthorizationService.authDetail.scvdate != ""
                && this._appService.AuthorizationService.authDetail.provid != undefined && this._appService.AuthorizationService.authDetail.provid != ""
                && this._appService.AuthorizationService.authDetail.procCode != undefined && this._appService.AuthorizationService.authDetail.procCode != ""
                && this._appService.AuthorizationService.authDetail.displayAvailableBenefit) {
                this.GetBenefitLimit();
              }
            }
          },
          (error) => {
            console.log(error)
          },
          () => {
            this._appService.AuthorizationService.busy = false;
          }
        )
    }
  }

  CheckOutOfNetBenefits(q: any, c: any) {

    let optAllowed: string[] = [];
    let optNotAllowed: string[] = [];
    let hpcode: string = this._appService.AuthorizationService.authDetail.hpcode;

    if (this._appService.AuthorizationService.provContract == 'NON-CONTRACTED') {

      this._appService.SettingService.Settings.forEach((el) => {

        if (el.settingsId == 'ProvAuthOutOfNetBen') {
          optAllowed = el.description.split(',');
        }
        if (el.settingsId == 'ProvAuthNonOutOfNetBen') {
          optNotAllowed = el.description.split(',');
        }
      });

      let found: boolean = false;

      optAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this._outOfNetworkMsg = "Provider not registered on the DRC/PAMC network. Please make sure the member has “out of network” benefits.";
        }
      });
      optNotAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this._outOfNetworkMsg = "Out of Network provider, the member does not have benefits ";
          
          q.ProvMessage = q.errMsg['network'];
          q.AuthForm.get('provider').setErrors(q.errMsg['network']);

        }
      });
      this._outOfNetwork = found;
    }
  }

  ValidateDate() {
        
    let c = this.authDetailForm.get('service').value;
    
    if ((c == '') || (c == undefined) || (c == null)) // Check if service date is entered
    {
      this._dateErrorMsg = this.errMsg['required'];
      this.authDetailForm.get('service').setErrors(this.errMsg['required']);
    }    
    else {
      
      this._appService.AuthorizationService.authDetail.scvdate = this.authDetailForm.get('service').value;
      this._dateErrorMsg = '';
      this.authDetailForm.get('service').setErrors(null);

      // Check limit if proc code, service date, and practice no is entered and then get bentype and avail limit
      if (this._appService.AuthorizationService.authDetail.scvdate != undefined && this._appService.AuthorizationService.authDetail.scvdate != ""
        && this._appService.AuthorizationService.authDetail.provid != undefined && this._appService.AuthorizationService.authDetail.provid != ""
        && this._appService.AuthorizationService.authDetail.procCode != undefined && this._appService.AuthorizationService.authDetail.procCode != ""
        && this._appService.AuthorizationService.authDetail.displayAvailableBenefit) {
        this.GetBenefitLimit();
      }
    }
  }

  async GetBenefitLimit() {
    let c: AuthModel = new AuthModel();

    c.membno = this._appService.AuthorizationService.authDetail.membno;
    c.dep = this._appService.AuthorizationService.authDetail.dep;   
    c.id = this._appService.AuthorizationService.authDetail.id;
    c.provid = this._appService.AuthorizationService.authDetail.provid;
    c.scvdate = this._appService.AuthorizationService.authDetail.scvdate;
    c.spec = this._appService.AuthorizationService.authDetail.spec;
    c.procCode = this._appService.AuthorizationService.authDetail.procCode;
    c.phCode = this._appService.AuthorizationService.authDetail.phCode;

    await this._appService.AuthorizationService.GetBenefitLimit(c);    
    this.ValidateAuthAmt();
    //this._appService.AuthorizationService.busy = true;    
  }

  ProcChange() {
    this._appService.AuthorizationService.remainingAmountMessage = "";
    this._appService.AuthorizationService.remainingAmount = "";

    let c: string[] = [];
    c = this.authDetailForm.get('proc').value.split('|');

    this._appService.AuthorizationService.authDetail.procCode = c[0];
    this._appService.AuthorizationService.authDetail.phCode = c[1];
    this._appService.AuthorizationService.authDetail.placeSvc = c[2];
    let enterAmt: string = c[3];

    if (enterAmt == 'true') {
      this._appService.AuthorizationService.authDetail.enterAmount = true;
    } else {
      this._appService.AuthorizationService.authDetail.enterAmount = false;
      this._appService.AuthorizationService.authDetail.amount = '';
      this.authDetailForm.get('amount').markAsTouched();
      this.authDetailForm.get('amount').setValue('0');
      this.authDetailForm.get('amount').setErrors(null);    
    }

    // Check if the amount enterd should be less than the available limit for this specdetail record
    this._appService.AuthorizationService.selectedSpec.specDetail.forEach((el) => {
      if (this._appService.AuthorizationService.authDetail.phCode == el.phCodeDetermined && this._appService.AuthorizationService.authDetail.procCode == el.procCode) {
        this._appService.AuthorizationService.authDetail.availableBenefitToAmtCheck = el.availableBenefitToAmtCheck;
        this._appService.AuthorizationService.authDetail.displayAvailableBenefit = el.displayAvailableBenefit;
      }
    });

    // Check limit if proc code, service date, and practice no is entered and then get bentype and avail limit
    if (this._appService.AuthorizationService.authDetail.scvdate != undefined && this._appService.AuthorizationService.authDetail.scvdate != ""
      && this._appService.AuthorizationService.authDetail.provid != undefined && this._appService.AuthorizationService.authDetail.provid != ""
      && this._appService.AuthorizationService.authDetail.procCode != undefined && this._appService.AuthorizationService.authDetail.procCode != ""
      && this._appService.AuthorizationService.authDetail.displayAvailableBenefit) {
      this.GetBenefitLimit();
    }
  }

  ValidateAuthAmt() {
    
    let c = this.authDetailForm.get('amount').value;

    if ((c == '') || (c == undefined) || (c == null)) {
      this._amtMsg = this.errMsg['required'];
      this.authDetailForm.get('amount').setErrors(this.errMsg['required']);
    }
    else if (c.length > 20) {
      this._amtMsg = this.errMsg['maxLength'];
      this.authDetailForm.get('amount').setErrors(this.errMsg['maxLength']);
    }
    else {
      this._amtMsg = '';

      if (this.authDetailForm.get('amount').value.includes(",")) {
        let c: string[] = this.authDetailForm.get('amount').value.split(",");
        if (c.length == 2) {
          if (c[1].length == 2) {
            this.authDetailForm.get('amount').setValue(this.authDetailForm.get('amount').value.replace(",", "."));
          }
        } else if (c.length > 2) {
          let result: any = "";
          if (c[c.length - 1].length == 2) {
            for (var i = 0; i < c.length; i++) {
              if (i == c.length - 1) {
                result = result + "." + c[i];
              } else {
                result = result + c[i];
              }
            }
          }
          //this.authDetailForm.get('amount').setValue = result; // Jaco comment out 2024-09-19 - WTF!
          this.authDetailForm.get('amount').setValue(result);
        }
      }
      this._amtMsg = this.NumberCheck(this.authDetailForm.get('amount').value.toString(), this._amtMsg, 'amount');

      if (this.authDetailForm.get('amount').value.trim() == "") {
        this._amtMsg = this.errMsg['required'];
        this.authDetailForm.get('amount').setErrors(this.errMsg['required']);
      }

      if (this._amtMsg === 'valid') {

        // Check if the amount enterd should be less than the available limit for this specdetail record
        if (this._appService.AuthorizationService.authDetail.availableBenefitToAmtCheck) {
         
          this._amtMsg = this.authDetailForm.get('amount').value;
          let aa: number = Number(this._amtMsg);
          let bb: number = Number(this._appService.AuthorizationService.remainingAmount);

          if (Number(this._amtMsg) > Number(this._appService.AuthorizationService.remainingAmount)) {
            this._amtMsg = this.errMsg['amountLimit'];
            this.authDetailForm.get('amount').setErrors(this.errMsg['amountLimit']);
          }
          else {
            this.authDetailForm.get('amount').setErrors(null);
            this._appService.AuthorizationService.authDetail.amount = this.authDetailForm.get('amount').value;
            this._amtMsg = 'valid';
          }
        } else {
          this.authDetailForm.get('amount').setErrors(null);
          this._appService.AuthorizationService.authDetail.amount = this.authDetailForm.get('amount').value;
          this._amtMsg = 'valid';
        }

      }
      else {
        this._amtMsg = this.errMsg['amountInvalid'];
        this.authDetailForm.get('amount').setErrors(this.errMsg['amountInvalid']);
      }
      
    }
  }

  NumberCheck(checkValue: string, msg: any, controlName: string) {
    let x = + checkValue;
    if (x.toString() === 'NaN') {
      msg = this.errMsg['number'];
      this.authDetailForm.get(controlName).setErrors(this.errMsg['number']);
      return msg;
    } else {
      msg = 'valid';
      return msg;
    }
  }

  PreAddDetailAuth() {
    // Get bentype if not obtained already
    if (this._appService.AuthorizationService.authDetail.benType == undefined
      || this._appService.AuthorizationService.authDetail.benType.trim() == ''
      || this._appService.AuthorizationService.authDetail.benType.trim() == null) {  // then first call get bentype api before AddDetailAuth()

      this._appService.AuthorizationService.GetBenTypeDescAuth()
      .subscribe(
        (result: ValidationResultsModel) => {
          let c: string[] = [];
          c = result.message.split('|');
          this._appService.AuthorizationService.authDetail.benType = c[0];
          this._appService.AuthorizationService.authDetail.benDesc = c[1];
        },
        (error) => {
          this._appService.AuthorizationService.authDetail.benType = "0";
          this._appService.AuthorizationService.authDetail.benDesc = "Error retrieving";
          console.log(error)
        },
        () => {
          this.AddDetailAuth();
        }
      )

    }
    else {
      this.AddDetailAuth();
    }

  }

  AddDetailAuth() {
        
    // Create Model to pass for Auth_template_detail record and new Client_Auth_Submissions record
    let auth: AuthModel = new AuthModel();
    auth.provid = this._appService.AuthorizationService.authDetail.provid;
    auth.amount = this._appService.AuthorizationService.authDetail.amount;
    auth.refno = this._appService.AuthorizationService.authDetail.refno;
    auth.scvdate = this._appService.AuthorizationService.authDetail.scvdate;
    auth.username = this._appService.LoginService.CurrentLoggedInUser.username;
    auth.hpcode = this._appService.AuthorizationService.authDetail.hpcode;
    auth.opt = this._appService.AuthorizationService.authDetail.opt;
    auth.phCode = this._appService.AuthorizationService.authDetail.phCode;
    auth.benType = this._appService.AuthorizationService.authDetail.benType;
    auth.benDesc = this._appService.AuthorizationService.authDetail.benDesc;
    auth.procCode = this._appService.AuthorizationService.authDetail.procCode;
    auth.placeSvc = this._appService.AuthorizationService.authDetail.placeSvc;    
    auth.spec = this._appService.AuthorizationService.selectedSpec.code;
    auth.qty = this.authDetailForm.get('qty').value;
    auth.diag1 = this.authDetailForm.get('diag1').value;
    auth.diag2 = this.authDetailForm.get('diag2').value;
    auth.diag3 = this.authDetailForm.get('diag3').value;
    auth.diag4 = this.authDetailForm.get('diag4').value;
    auth.authRefNo = this.authDetailForm.get('authRefNo').value;
    auth.note = this.authDetailForm.get('note').value;
    auth.reject = this._appService.AuthorizationService.authDetail.reject;

    // Get Auth_template_master rowid
    this._appService.AuthorizationService.detailedAuth.forEach((el) => {
      if (el.spec == auth.spec) {
        auth.id = el.id;
      }
    })

    // Fill in details on spec list for this captured spec for display after submission
    this._appService.AuthorizationService.selectedSpecs.forEach((el) => {
     
      if (el.code == this._appService.AuthorizationService.selectedSpec.code) {
        el.provId = auth.provid;
        el.srvDate = auth.scvdate;
        
        el.procCode = auth.procCode;
        el.benCode = auth.benType + '-' + auth.benDesc;

        if (auth.reject) {
          el.amount = "0.00";
        }
        else if (auth.amount != '') {
          el.amount = auth.amount; // Jaco 2022-08-01
        }
        else {
          el.amount = "(SYSTEM AMOUNT)";
        }
       
      }
    });
    this._appService.AuthorizationService.UpdateAuthDetails(auth);
    
    //this.authDetailForm.reset(); // Jaco 2024-09-13
    this.AuthDetailFormInit(); // Jaco 2024-09-13
    
    this._openNew = false;
    this.CancelDetailCapture();
  }

  RemoveDetailAuth(code: string) {
    let index: number = 0;
    for (var i = 0; i < this._appService.AuthorizationService.detailedAuth.length; i++) {
      if (this._appService.AuthorizationService.detailedAuth[i].spec == code) {
        index = i;
        break;
      }
    }

    this._appService.AuthorizationService.selectedSpecs.forEach((el) => {
      if (el.code == code) {
        el.provId = "";
        el.srvDate = "";
        el.amount = "";
      }
    });
    this._appService.AuthorizationService.detailedAuth.splice(index, 1);

    this._openNew = false;
  }

  CancelDetailCapture() {
    this._openNew = false;
   
    //this.authDetailForm.reset(); // Jaco 2024-09-13
    this.AuthDetailFormInit(); // Jaco 2024-09-13
    
    this._appService.AuthorizationService.selectedSpec = new SpecCodes();
    this._provValid = false;
    this._provErrorMsg = '';
    this._provName = '';
    this._provNetwork = '';
    this._dateErrorMsg = '';
    this._outOfNetwork = false;
    this._outOfNetworkMsg = "";
    this._amtMsg = "";
    this._appService.AuthorizationService.defaultProc = "";

    this._appService.AuthorizationService.authDetail.provid = '';
    this._appService.AuthorizationService.authDetail.benType = '';
    this._appService.AuthorizationService.authDetail.scvdate = '';
    this._appService.AuthorizationService.authDetail.refno = '';
    this._appService.AuthorizationService.authDetail.amount = '';
    this._appService.AuthorizationService.authDetail.spec = '';
    this._appService.AuthorizationService.authDetail.desc = '';
    this._appService.AuthorizationService.authDetail.create = '';
    this._appService.AuthorizationService.authDetail.proc = '';
    this._appService.AuthorizationService.authDetail.authno = '';
    this._appService.AuthorizationService.authDetail.phCode = '';
    this._appService.AuthorizationService.authDetail.benefitId = '';
    this._appService.AuthorizationService.authDetail.placeSvc = '';
    this._appService.AuthorizationService.authDetail.enterAmount = false;
    this._appService.AuthorizationService.authDetail.reject = false;
  }

  CloseSpecList() {
    this._appService.AuthorizationService.displayGrid = false;
    this._appService.AuthorizationService.selectedSpecs = [];
    this._appService.AuthorizationService.detailedAuth = [];
    this._appService.AuthorizationService.authDetail.provid = '';
    this._appService.AuthorizationService.authDetail.benType = '';
    this._appService.AuthorizationService.authDetail.scvdate = '';
    this._appService.AuthorizationService.authDetail.refno = '';
    this._appService.AuthorizationService.authDetail.amount = '';
    this._appService.AuthorizationService.authDetail.spec = '';
    this._appService.AuthorizationService.authDetail.desc = '';
    this._appService.AuthorizationService.authDetail.create = '';
    this._appService.AuthorizationService.authDetail.proc = '';
    this._appService.AuthorizationService.authDetail.authno = '';
    this._appService.AuthorizationService.authDetail.phCode = '';
    this._appService.AuthorizationService.authDetail.benefitId = '';
    this._appService.AuthorizationService.authDetail.placeSvc = '';
    this._appService.AuthorizationService.authDetail.enterAmount = false;
  }

  GetProcCodeForDisplay(rec: SpecCodes) {

    if (rec.selectedProcCode != undefined && rec.selectedProcCode != '') {
      return rec.selectedProcCode;
    }
    else {

      let lst: AuthSpecDetail[];
      lst = rec.specDetail;

      if (lst.length == 0) {
        return "None (Incorrect Setup)";
      }
      else if (lst.length == 1) {
        rec.selectedProcCode = lst[0].procCode;
        return rec.selectedProcCode;
      }
      else {
        return "Select from list";
      }
    }
  }

  GetBenCodeForDisplay(rec: SpecCodes) {

    if (rec.selectedBenCode != undefined && rec.selectedBenCode != '') {
      return rec.selectedBenCode;
    }
    else {

      let lst: AuthSpecDetail[];
      lst = rec.specDetail;

      if (lst.length == 0) {
        return "None (Incorrect Setup)";
      }
      else if (lst.length == 1) {
        rec.selectedBenCode = lst[0].benCode;
        return rec.selectedBenCode;
      }
      else {
        return "";
      }
    }
  }
}

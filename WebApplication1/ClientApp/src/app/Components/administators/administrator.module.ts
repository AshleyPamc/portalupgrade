import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { AdministratordashComponent } from './administratordash/administratordash.component';
import { SharedModule } from './../../../app/shared.module';




const routes: Routes = [{

  path: '',
  component: AdministratordashComponent,
  children: [
    { pathMatch: 'full', path: 'administratordash', component: AdministratordashComponent }
  ]

}];


@NgModule({
  declarations: [AdministratordashComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdministratorModule { }

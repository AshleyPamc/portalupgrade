import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from "@clr/angular";
import { SharedModule } from './../../../../app/shared.module';
import { JoyrideModule } from 'ngx-joyride';
import { BenSetupComponent } from './ben-setup.component';


const routes: Routes = [{

  path: '',
  component: BenSetupComponent,
  children: [
    { pathMatch: 'full', path: 'ben-setup', component: BenSetupComponent }
  ]

}];

@NgModule({
  declarations: [BenSetupComponent],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    JoyrideModule.forRoot()

  ],
  exports: [FormsModule, ReactiveFormsModule]
})
export class BensetupModule { }

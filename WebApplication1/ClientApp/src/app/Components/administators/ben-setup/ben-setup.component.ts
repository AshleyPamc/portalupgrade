import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { AppService } from '../../../services/app.service';
import { ValidationResultsModel } from '../../../models/ValidationResultsModel';
import { SpecCodes } from '../../../models/SpecCodes';
import { Specmasters } from '../../../models/specmasters';
import { JoyrideService } from 'ngx-joyride';
import { BenTrackCodes } from '../../../models/ben-track-codes';
import { ClaimHeadersModel } from '../../../models/ClaimHeadersModel';
import { ClaimLineModel } from '../../../models/ClaimLineModel';
import { Recordtype1Model } from '../../../models/Recordtype1Model';

@Component({
  selector: 'app-ben-setup',
  templateUrl: './ben-setup.component.html',
  styleUrls: ['./ben-setup.component.css']
})
export class BenSetupComponent implements OnInit {

  @Input() benForm: FormGroup;
  @Input() SpecForm: FormGroup;


  private _currentPage: number;
  private _lastPage: number;
  public tempList: SpecCodes[] = [];
  public _edit: boolean = false;
  public _new: boolean = true;
  public selectedId: number;
  public _showPopup: boolean = false;
  public _openSpec: boolean = false;
  public _selectedCode: string;
  public _procerror: string = '';
  public error: ValidationErrors;




  constructor(private fb: FormBuilder, private fbSpec: FormBuilder, public _appService: AppService, private readonly joyrideService: JoyrideService) {
    this._currentPage = 1;
    this._appService.SecurityService.specList = this._appService.LoginService.specList;
    this._appService.SecurityService.bencodes = this._appService.LoginService.bencodes;
    this._lastPage = this._appService.SecurityService.specList[(this._appService.SecurityService.specList.length - 1)].page;
    this.GetFirstItems();
    this.error = {
      invalid: 'Invalid Code!!',
      required: 'This field is required'
    };
    this._appService.SecurityService.GetExistingBenefits();
  }

  ngOnInit() {
    this.benForm = this.fb.group({
      'desc': ['', Validators.required],
      'hp': ['', Validators.required],
      'phcodes': ['', Validators.required],
      'select': [],
      'search': []
    });
    this.benForm.disable();

    this.SpecForm = this.fbSpec.group({
      'Proc': ['', Validators.required],
      'Ben': ['', Validators.required],
      'Search': []
    });
  }

  public get currentPage(): number {
    return this._currentPage;
  }
  public set currentPage(value: number) {
    this._currentPage = value;
  }
  public get lastPage(): number {
    return this._lastPage;
  }
  public set lastPage(value: number) {
    this._lastPage = value;
  }


  GetFirstItems() {
    this.tempList = [];
    this._appService.SecurityService.specList.forEach((el) => {
      if (el.page == this._currentPage) {
        this.tempList.push(el);
      }
    })
  }

  onClick() {

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['ben_2', 'ben_3', 'ben_4'],
        themeColor: '#313131'
      })
    }

  }

  IncPage() {
    this._currentPage++;
    this.tempList = [];
    this._appService.SecurityService.specList.forEach((el) => {
      if (el.page == this._currentPage) {
        this.tempList.push(el);
      }
    })
  }

  DecPage() {
    this._currentPage--;
    this.tempList = [];
    this._appService.SecurityService.specList.forEach((el) => {
      if (el.page == this._currentPage) {
        this.tempList.push(el);
      }
    })
  }

  Search() {
    let term: string;

    term = this.benForm.get("search").value;
    term = term.toUpperCase();
    this.tempList = [];

    this._appService.SecurityService.specList.forEach((el) => {
      let desc = el.desc.toUpperCase();
      if ((term == el.desc) || (term == el.code)) {
        this.tempList.push(el);
      }
    });

  }

  RemoveSearch() {
    this._currentPage = 1;
    this.tempList = [];
    this._appService.SecurityService.specList.forEach((el) => {
      if ((el.page == 1)) {
        this.tempList.push(el);
      }
    });
  }

  OptionChanged(code: string) {
    this.benForm.get('select').markAsTouched();
    this._appService.SecurityService.specList.forEach(
      (el) => {
        if (el.code == code) {
          el.selected = this.benForm.get('select').value;
          if (el.selected == true) {
            this._appService.SecurityService.selectedList.push(el);
          } else {
            let index = this._appService.SecurityService.selectedList.indexOf(el, 0);
            this._appService.SecurityService.selectedList.splice(index, 1);
          }

        }
        if ((code == '14') && (el.code == '15')) {
          el.selected = this.benForm.get('select').value;
        }
        if ((code == '15') && (el.code == '14')) {
          el.selected = this.benForm.get('select').value;
        }
      });

    if (this.benForm.get('select').value == true) {
      this._selectedCode = code;
      this._openSpec = true;
      this.SpecForm.reset();
    }


  }

  SearchBenTypes() {
    let c = this.SpecForm.get('Search').value.toUpperCase();
    let temp: BenTrackCodes[] = [];

    this._appService.SecurityService.bencodes.forEach((el) => {
      if (el.desc.includes(c, 0)) {
        temp.push(el);
      }
    });

    this._appService.SecurityService.bencodes = [];
    temp.forEach((el) => {
      this._appService.SecurityService.bencodes.push(el);
    });
  }

  ClearSearch() {
    this._appService.SecurityService.GetBenTrackCodes();
    this.SpecForm.controls['Search'].setValue('');
  }

  Clear() {

    this.selectedId = null;
    this._edit = false;
    this._new = true;
    this.benForm.reset();
    this.benForm.disable();
    this.benForm.get('select').disable();
    this._appService.SecurityService.specList.forEach(
      (el) => {
        el.selected = false;
        el.id = null;
        el.user = null;
      });
    this._currentPage = 1;
    this.GetFirstItems();
    this._appService.SecurityService.selectedList = [];
    this._showPopup = false;
  }

  Submit() {
    let c: Specmasters = new Specmasters();

    c.desc = this.benForm.get('desc').value;
    c.hpcode = this.benForm.get('hp').value;
    c.phcode = this.benForm.get('phcodes').value;
    c.user = this._appService.LoginService.CurrentLoggedInUser.username;
    this.benForm.disable();
    this.benForm.get('select').disable();
    this._appService.SecurityService.SubmitBenSpec(c);
    this._showPopup = false;
    //this.Clear();
  }

  Update() {
    let c: Specmasters = new Specmasters();

    c.desc = this.benForm.get('desc').value;
    c.hpcode = this.benForm.get('hp').value;
    c.user = this._appService.LoginService.CurrentLoggedInUser.username;
    c.id = this.selectedId;
    c.phcode = this.benForm.get('phcodes').value;
    // this._appService.SecurityService.SubmitBenSpec(c);
    this._appService.SecurityService.UpdateBenSpec(c);
    this.benForm.disable();
    this.benForm.get('select').disable();
    this._showPopup = false;
    //this.Clear();
  }

  New() {
    this.Clear();
    this._edit = false;
    this._new = true;
    this.benForm.get('select').enable();
    this.benForm.enable();
    this._showPopup = true;
  }

  Edit(data: Specmasters) {
    this.Clear();
    this.selectedId = data.id;
    this.benForm.controls['hp'].setValue(data.hpcode);
    this.benForm.controls['desc'].setValue(data.desc);
    let c: Recordtype1Model = new Recordtype1Model();

    c.hpCode = this.benForm.get('hp').value;

    this._appService.ClaimCaptureService.currentClaimHeader.hpCode = c.hpCode;

    this._appService.SecurityService.GetDetailsBen(data);
    this._edit = true;
    this._new = false;
    this.benForm.enable();
    this.benForm.get('select').enable();
    this._showPopup = true;
  }

  Delete(data: Specmasters) {
    this.Clear();
    this.selectedId = data.id;
    this._appService.SecurityService.DeleteBenSpec(data);
  }

  AddSpecs() {
    this._appService.SecurityService.selectedList.forEach((el) => {
      if (el.code == this._selectedCode) {
        el.benCode = this.SpecForm.get('Ben').value;
        el.procCode = this.SpecForm.get('Proc').value;
      }

    });

    this._appService.SecurityService.specList.forEach((el) => {
      if (el.code == this._selectedCode) {
        el.benCode = this.SpecForm.get('Ben').value;
        el.procCode = this.SpecForm.get('Proc').value;
      }
    })

    this._openSpec = false;
  }

  ValidateProcCode() {
    let c: SpecCodes = new SpecCodes();
    c.phcode = this.benForm.get('phcodes').value;
    c.procCode = this.SpecForm.get('Proc').value;

    if ((c.procCode == '') || (this._procerror == undefined) || (this._procerror == null)) {
      this._procerror = this.error['required'];
      this.SpecForm.controls['Proc'].setErrors(this.error['required']);
    }


    this._appService.SecurityService.ValidateProcCode(c);
    let p = this;
    setTimeout(() => {
      if (!p._appService.SecurityService.procerror) {
        p._procerror = p.error['invalid'];
        p.SpecForm.controls['Proc'].setErrors(p.error['invalid']);
      } else {
        p.SpecForm.controls['Proc'].setErrors(null);
        p._procerror = '';
      }
    }, 200)

  }

  CancelSpec() {
    this._openSpec = false;
    this._appService.SecurityService.specList.forEach((el) => {
      if (el.code == this._selectedCode) {
        el.selected = false;
      }
    });

    let count = 0;

    for (var i = 0; i < this._appService.SecurityService.selectedList.length; i++) {
      if (this._appService.SecurityService.selectedList[i].code == this._selectedCode) {
        break;
      }
      count++;
    }
    this._appService.SecurityService.selectedList.splice(count, 1);
  }

  GetPhCodes() {
    let c: Recordtype1Model = new Recordtype1Model();

    c.hpCode = this.benForm.get('hp').value;

    this._appService.ClaimCaptureService.currentClaimHeader.hpCode = c.hpCode;

    this._appService.ClaimCaptureService.LoadPhCodes();

  }

}

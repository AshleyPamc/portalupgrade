import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { JoyrideService } from 'ngx-joyride';
import { AuthModel } from '../../../models/AuthModel';
import { HealthPlanSetupModel } from '../../../models/HealthPlanSetupModel';
import { HealthplanModel } from '../../../models/HealthplanModel';
import { Specmasters } from '../../../models/specmasters';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';
import { ValidationResultsModel } from '../../../models/ValidationResultsModel';


@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {

  @Input() AuthForm: FormGroup;
  public MembMessage: string;
  //public ProvMessage: string;
  public seletecHp: string;
  public DateMessage: string;
  public AmountMessage: string;
  public errMsg: ValidationErrors;
  public _memberror: boolean = false;
  //public _proverror: boolean = false;
  public _amounterror: boolean = false;
  //public _dateerror: boolean = false; Jaco 2022-07-08 auth date can be past or future
  public RefMessage: string;
  //public spec: string;  
  //public specDescr: string;
  public outOfNetwork: boolean = false;
  public outOfNetworkMsg: string = "";
  //public sysAmount: boolean;

  constructor(public _appService: AppService, private _fb: FormBuilder, private readonly joyrideService: JoyrideService, public _router: Router) {
    this.errMsg = {
      'required': 'This field is required',
      'invalid': 'Invalid Member Number',
      'invalid1': 'Invalid Provider Number',
      'provSpec': 'Invalid Provider Speciality',
      'amountLimit': 'Amount larger than the limit available',
      'amountInvalid': 'Invalid amount entered',
      'maxLength': 'Exceeded max length',
      'number': 'This field requires a number',
      'network': 'Out of network provider',
      'invalidProc': 'Invalid Procedure Code',
    }
    this._router.url
    this._appService.AuthorizationService.showAuth = false;
    //this.sysAmount = true;
  }

  ngOnInit() {
    this.AuthForm = this._fb.group({
      'hp': [''],
      'member': [''],
      'provider': ['', Validators.required],
      'service': ['', Validators.required],
      'dependant': [''],
      //'proc': [],                       //Jaco 2022-08-05
      'proc': ['', Validators.required],  //Jaco 2022-08-05
      'benset': [''],
      'amount': ['', [Validators.required, Validators.maxLength(20)]]
    });

    //this.AuthForm.controls['proc'].setValue('0190'); //Jaco comment out 2022-08-05
    //this.AuthForm.controls['proc'].disable();        //Jaco comment out 2022-08-05

    this._appService.AuthorizationService.authDetail.amount = "";
    //this.sysAmount = true;
    //this.AuthForm.get('options').setValue(this.sysAmount);
    //this.AuthForm.get('amount').disable();
  }

  onClick() {

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['Authsub_1', 'Authsub_2', 'Authsub_3', 'Authsub_4', 'Authsub_5', 'Authsub_6', 'Authsub_7', 'Authsub_8', 'Authsub_9'],
        themeColor: '#313131'
      })
    }

  }

  HpCHange() {
    this._appService.AuthorizationService.authDetail.hpcode = this.AuthForm.get('hp').value;
    let c: HealthplanModel = new HealthplanModel();
    c.code = this._appService.AuthorizationService.authDetail.hpcode;
    //this._appService.AuthorizationService.GetBenifitListForHp(c);
  }

  EnableAmount() {
    //let c: boolean = this.AuthForm.get('options').value;
    //// this.sysAmount = c;
    //if (c) {
    //  this.AuthForm.get('amount').disable();
    //} else {
    //  this.AuthForm.get('amount').enable();
    //}
  }

  DepCHange() {
    this._appService.AuthorizationService.authDetail.dep = this.AuthForm.get('dependant').value;
  }

  ValidateMember() {
    let c = this.AuthForm.get('member').value;

    if ((c == '') || (c == undefined) || (c == null)) {
      this.MembMessage = this.errMsg['required'];
      this.AuthForm.get('member').setErrors(this.errMsg['required']);
    } else {
      this._appService.AuthorizationService.authDetail.membno = c;
      this._appService.AuthorizationService.authDetail.username = this._appService.LoginService.CurrentLoggedInUser.username;
      this._appService.AuthorizationService.ValidateMember();
      this._appService.AuthorizationService.busy = true;

      //setTimeout((e) => {
       //this._memberror = this._appService.AuthorizationService.AuthMembMsg.valid;
        //if (this._appService.AuthorizationService.AuthMembMsg.valid) {
          //this.MembMessage = this._appService.AuthorizationService.AuthMembMsg.message;

        //} else {
          //this.MembMessage = this.errMsg['invalid'];
          //this.AuthForm.get('member').setErrors(this.errMsg['invalid']);
        //}
        //this._appService.AuthorizationService.busy = false;
      //}, 500)
    }
  }  

  ValidateAuthAmt() {
    let q = this;
    let c = this.AuthForm.get('amount').value;

    if ((c == '') || (c == undefined) || (c == null)) {
      q.AmountMessage = q.errMsg['required'];
      q.AuthForm.get('amount').setErrors(q.errMsg['required']);
    }
    else if (c.length > 20) {
      q.AmountMessage = q.errMsg['maxLength'];
      q.AuthForm.get('amount').setErrors(q.errMsg['maxLength']);
    }
    else {
            q.AmountMessage = '';

            if (this.AuthForm.get('amount').value.includes(",")) {
              let c: string[] = this.AuthForm.get('amount').value.split(",");
              if (c.length == 2) {
                if (c[1].length == 2) {
                  this.AuthForm.get('amount').setValue = this.AuthForm.get('amount').value.replace(",", ".");
                }
              } else if (c.length > 2) {
                let result: any = "";
                if (c[c.length - 1].length == 2) {
                  for (var i = 0; i < c.length; i++) {
                    if (i == c.length - 1) {
                      result = result + "." + c[i];
                    } else {
                      result = result + c[i];
                    }
                  }
                }
                this.AuthForm.get('amount').setValue = result;
              }
            }
            q.AmountMessage = this.NumberCheck(this.AuthForm.get('amount').value.toString(), q.AmountMessage, 'amount');

            if (q.AmountMessage === 'valid') {
              //q.AmountMessage = 'R ' + this.AuthForm.get('amount').value;
              q.AmountMessage = this.AuthForm.get('amount').value;
              let aa: number = Number(q.AmountMessage);
              let bb: number = Number(this._appService.AuthorizationService.remainingAmount);

              if (Number(q.AmountMessage) > Number(this._appService.AuthorizationService.remainingAmount)) {
                q.AmountMessage = q.errMsg['amountLimit'];
                q.AuthForm.get('amount').setErrors(q.errMsg['amountLimit']);
              }
              else {
                this.AuthForm.get('amount').setErrors(null);
                q._appService.AuthorizationService.authDetail.amount = this.AuthForm.get('amount').value;
              }              
            }
            else {
              q.AmountMessage = q.errMsg['amountInvalid'];
              q.AuthForm.get('amount').setErrors(q.errMsg['amountInvalid']);
            }
            //this.TempValidFormMethod();
         }
  }

  NumberCheck(checkValue: string, msg: any, controlName: string) {
    let x = + checkValue;
    if (x.toString() === 'NaN') {
      msg = this.errMsg['number'];
      this.AuthForm.get(controlName).setErrors(this.errMsg['number']);
      return msg;
    } else {
      msg = 'valid';
      return msg;
    }
  }

  ValidateDate() {

    //this._appService.AuthorizationService.enableSubmit = false;
    //let ToDay: Date = new Date; Jaco 2022-07-08 auth date can be past or future
    //let comparedDate = new Date(this.AuthForm.get('service').value); Jaco 2022-07-08 auth date can be past or future
    let c = this.AuthForm.get('service').value;
    //this.proc = this.AuthForm.get('proc').value;

    if ((c == '') || (c == undefined) || (c == null)) // Check if service date is entered
    {
      this.DateMessage = this.errMsg['required'];
      this.AuthForm.get('service').setErrors(this.errMsg['required']);
    }
    //else if ((proc == '') || (proc == undefined) || (proc == null)) // Check if proc code is entered
    //{
    //  this.AuthForm.get('proc').setErrors(this.errMsg['required']);
    //}
    else
    {
      //if (comparedDate < ToDay) {  Jaco comment out 2022-07-08 - auth date can be past or future
      //  //this._dateerror = true; 
      //  this._appService.AuthorizationService.authDetail.scvdate = this.AuthForm.get('service').value;
      //  this.DateMessage = '';
      //  this.AuthForm.get('service').setErrors(null);
      //} else {
      //  this._dateerror = true;
      //  this._appService.AuthorizationService.authDetail.scvdate = this.AuthForm.get('service').value;
      //  this.DateMessage = '';
      //  this.AuthForm.get('service').setErrors(null);
      //}

      this._appService.AuthorizationService.authDetail.scvdate = this.AuthForm.get('service').value;
      this.DateMessage = '';
      this.AuthForm.get('service').setErrors(null);

      // Check limit if proc code, service date, and practice no is entered and then get bentype and avail limit
      if (this._appService.AuthorizationService.authDetail.scvdate != undefined && this._appService.AuthorizationService.authDetail.scvdate != ""
        && this._appService.AuthorizationService.authDetail.provid != undefined && this._appService.AuthorizationService.authDetail.provid != ""
        && this._appService.AuthorizationService.authDetail.procCode != undefined && this._appService.AuthorizationService.authDetail.procCode != "")
      {
        this.GetBenefitLimit();
      }      
    }
  }

  ValidateProviderWithSpec() {

    this._appService.AuthorizationService.authDetail.provid = this.AuthForm.get('provider').value;

    // modal vars
    this._appService.AuthorizationService.showAuth = false;
    this._appService.AuthorizationService.busy = true;

    // Api Call
    this._appService.AuthorizationService.ValidateProvWithSpec()
      .subscribe(
        (resutls: ValidationResultsModel) => {
                                              console.log(resutls)
                                             },
        (error) => {
                    console.log(error)
                   },
        () => {
          this._appService.AuthorizationService.busy = false;          
        }
      )
  }

  ValidateProv() {
    //let q = this;
    let c = this.AuthForm.get('provider').value;
    //this.proc = this.AuthForm.get('proc').value;

    // Check if data is entered
    if ((c == '') || (c == undefined) || (c == null)) {
      //this.ProvMessage = this.errMsg['required'];
      this._appService.AuthorizationService.AuthProvMsg.message = this.errMsg['required'];
      this.AuthForm.get('provider').setErrors(this.errMsg['required']);
    }
    // Check if prov is correct spec
    else {
      this._appService.AuthorizationService.authDetail.provid = c;
      //this._appService.AuthorizationService.authDetail.id = this.AuthForm.get('benset').value;
      //this._appService.AuthorizationService.authDetail.username = this._appService.LoginService.CurrentLoggedInUser.username;
      //this._appService.AuthorizationService.authDetail.spec = this.spec;
      this._appService.AuthorizationService.ValidateProvider();
        

      //setTimeout((e) => {
        //console.log(this._appService.AuthorizationService.AuthProvMsg.valid);
        //console.log(this._proverror);
        //this._proverror = this._appService.AuthorizationService.AuthProvMsg.valid;
        //this._appService.AuthorizationService.ProvError = this._appService.AuthorizationService.AuthProvMsg.valid;
        if (this._appService.AuthorizationService.AuthProvMsg.valid) {
          //this.ProvMessage = this._appService.AuthorizationService.AuthProvMsg.message;
          this.AuthForm.get('provider').setErrors(null);
        } else {
          //this.ProvMessage = this.errMsg['provSpec'];
          this.AuthForm.get('provider').setErrors(this.errMsg['provSpec']);
        }

        if (this.AuthForm.get('provider').errors == null) {

          // Set phcode
          if (this._appService.AuthorizationService.provContract == 'NON-CONTRACTED') {
            this._appService.AuthorizationService.authDetail.phCode = this._appService.AuthorizationService.selectedSpec.specDetail[0].phCodeOffNetwork;
          }
          else if (this._appService.AuthorizationService.provContract = "CONTRACTED")
          {
            this._appService.AuthorizationService.authDetail.phCode = this._appService.AuthorizationService.selectedSpec.specDetail[0].phCodeOnNetwork;
          }

          // Get provider network warning message
          if (this._appService.AuthorizationService.authDetail.spec == '14' || this._appService.AuthorizationService.authDetail.spec == '15')
          {
            this.CheckOutOfNetBenefits(this, c);
          }

          // Check limit if proc code, service date, and practice no is entered and then get bentype and avail limit
          if (this._appService.AuthorizationService.authDetail.scvdate != undefined && this._appService.AuthorizationService.authDetail.scvdate != ""
            && this._appService.AuthorizationService.authDetail.provid != undefined && this._appService.AuthorizationService.authDetail.provid != ""
            && this._appService.AuthorizationService.authDetail.procCode != undefined && this._appService.AuthorizationService.authDetail.procCode != "") {
            this.GetBenefitLimit();
          }
        }
      //}, 2000)
    }
  }

  GetBenefitLimit() {
    let c: AuthModel = new AuthModel();

    c.membno = this._appService.AuthorizationService.authDetail.membno;
    c.dep = this._appService.AuthorizationService.authDetail.dep;
    //c.dep = this._appService.AuthorizationService.authDetail.dep;
    //c.id = this._appService.AuthorizationService.selectedAuthSetId;
    c.provid = this._appService.AuthorizationService.authDetail.provid;
    c.scvdate = this._appService.AuthorizationService.authDetail.scvdate;
    c.spec = this._appService.AuthorizationService.authDetail.spec;
    c.procCode = this._appService.AuthorizationService.authDetail.procCode;
    c.phCode = this._appService.AuthorizationService.authDetail.phCode;

    this._appService.AuthorizationService.GetBenefitLimit(c);
    this._appService.AuthorizationService.busy = true;
    setTimeout(() => {
      //this.CheckOutOfNetBenefits(); // Jaco 2022-07-20 moved to provider validation
      this._appService.AuthorizationService.busy = false;
    }, 800)
  }

  ValidateRef() {
    let c = this.AuthForm.get('ref').value;

    if ((c == '') || (c == undefined) || (c == null)) {
      this.RefMessage = this.errMsg['required'];
      this.AuthForm.get('ref').setErrors(this.errMsg['required']);
    } else {
      this.RefMessage = '';
      this._appService.AuthorizationService.authDetail.refno = this.AuthForm.get('ref').value;
    }
  }

  ClearAllInputFields() {
    this.AuthForm.reset();

    this.RefMessage = '';
    this.DateMessage = '';
    //this.ProvMessage = '';
    this.MembMessage = '';
    this.AmountMessage = '';
    //this._dateerror = false; Jaco 2022-07-08 auth date can be past or future
    this._memberror = false;
    //this._proverror = false;
    //this._appService.AuthorizationService.ProvError = false;
    this._amounterror = false;
    //this._appService.AuthorizationService.familyList = [];

  }

  ValidateAmount() {
    //let c = this.AuthForm.get('amount').value;
    //let d: string = (this._appService.AuthorizationService.remainingAmount).replace('AVAILABLE LIMIT: R ', '').replace(',', '');
    //let limmit = +d;
    //if ((c == '') || (c == undefined) || (c == null)) {
    //  this.AmountMessage = this.errMsg['required'];
    //  this.AuthForm.get('amount').setErrors(this.errMsg['required']);
    //} else {
    //  if (c > limmit) {
    //    this.AmountMessage = this.errMsg['amount'];
    //    this.AuthForm.get('amount').setErrors(this.errMsg['amount']);
    //  } else {
    //    this.AuthForm.get('amount').setErrors(null);
    //    this._appService.AuthorizationService.authDetail.amount = c;
    //  }
    //}
  }

  SubmitAuthorization() {
    let c: AuthModel = new AuthModel();
    c.hpcode = this.AuthForm.get('hp').value;
    c.membno = this.AuthForm.get('member').value;
    c.dep = this.AuthForm.get('dependant').value;
    c.refno = this.AuthForm.get('ref').value;
    c.scvdate = this.AuthForm.get('service').value;
    c.provid = this.AuthForm.get('provider').value;
    c.proc = this.AuthForm.get('proc').value;
    c.username = this._appService.LoginService.CurrentLoggedInUser.username;
    c.id = this.AuthForm.get('benset').value;
    //this._appService.AuthorizationService.benefitList.forEach((el) => {
    //  if (c.id == el.id.toString()) {
    //    c.desc = el.desc;

    //  }
    //})

    this._appService.AuthorizationService.authDetail = c;
    this._appService.AuthorizationService.SubmitAuth();

    this.AuthForm.reset();
    this.ClearAllInputFields();

  }

  CheckOutOfNetBenefits(q: any, c: any) {

    let optAllowed: string[] = [];
    let optNotAllowed: string[] = [];
    let hpcode: string = this._appService.AuthorizationService.authDetail.hpcode;

    if (this._appService.AuthorizationService.provContract == 'NON-CONTRACTED') {

      this._appService.SettingService.Settings.forEach((el) => {

        if (el.settingsId == 'ProvAuthOutOfNetBen') {
          optAllowed = el.description.split(',');
        }
        if (el.settingsId == 'ProvAuthNonOutOfNetBen') {
          optNotAllowed = el.description.split(',');
        }
      });

      let found: boolean = false;

      optAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this.outOfNetworkMsg = "Provider not registered on the DRC/PAMC network. Please make sure the member has “out of network” benefits.";
        }
      });
      optNotAllowed.forEach((el) => {
        if (el.toUpperCase() == hpcode.toUpperCase()) {
          found = true;
          this.outOfNetworkMsg = "Out of Network provider, the member does not have benefits ";
          //this._appService.AuthorizationService.enableSubmit = false;
          q.ProvMessage = q.errMsg['network'];
          q.AuthForm.get('provider').setErrors(q.errMsg['network']);

        }
      });
      this.outOfNetwork = found;
    }
  }

  ProcChange() {
    this._appService.AuthorizationService.remainingAmountMessage = "";
    this._appService.AuthorizationService.remainingAmount = "";
    this.AuthForm.get('service').reset();
  }

}

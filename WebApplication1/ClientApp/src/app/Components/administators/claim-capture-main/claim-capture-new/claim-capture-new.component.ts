import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-claim-capture-new',
  templateUrl: './claim-capture-new.component.html',
  styleUrls: ['./claim-capture-new.component.css']
})
export class ClaimCaptureNewComponent implements OnInit {

  constructor(private _appService: AppService) { }

  ngOnInit() {

    this._appService.ClaimCaptureService.LoadClaimCaptureSetup();

  }

}

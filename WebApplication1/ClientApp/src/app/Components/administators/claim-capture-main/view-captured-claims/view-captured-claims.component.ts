import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../../services/app.service';

@Component({
  selector: 'app-view-captured-claims',
  templateUrl: './view-captured-claims.component.html',
  styleUrls: ['./view-captured-claims.component.css']
})
export class ViewCapturedClaimsComponent implements OnInit {

  constructor(private _appService: AppService) { }

  ngOnInit() {

    this._appService.ClaimCaptureService.LoadClaimCaptureSetup(); // Need to load hpcodes for claim search to work
    
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";

import { VoucherTransactionsComponent } from './voucher-transactions.component';

const routes: Routes = [{

  path: '',
  component: VoucherTransactionsComponent,
  children: [
    { pathMatch: 'full', path: 'voucher-transactions', component: VoucherTransactionsComponent }
  ]
}];

@NgModule({
  declarations: [VoucherTransactionsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule
  ],
  exports: [ReactiveFormsModule, FormsModule]
})
export class VoucherTransactionsModule { }

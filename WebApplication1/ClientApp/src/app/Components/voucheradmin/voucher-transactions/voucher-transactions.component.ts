import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { AppService } from './../../../services/app.service';
import { VoucherTransactionModel } from './../../../models/VoucherTransactionModel';
import { DefaultCodesModel } from './../../../models/DefaultCodesModel';

@Component({
  selector: 'app-voucher-transactions',
  templateUrl: './voucher-transactions.component.html',
  styleUrls: ['./voucher-transactions.component.css']
})
export class VoucherTransactionsComponent implements OnInit {

  _depositForm: FormGroup;
  public detailsOfBasket: DefaultCodesModel[] = [];
  
  constructor(private _appService: AppService, private route: Router) { }

  ngOnInit() {

    //Get current user transactions
    if (this._appService.VoucherAdminService.VoucherClient.clientId != "") {
      this._appService.VoucherAdminService.GetClientTransactions(this._appService.VoucherAdminService.VoucherClient)
    }

    this._depositForm = new FormGroup({       
      'amount': new FormControl(null, Validators.required),  //First arg is initial state/value // Second arg is single validator or array of validators // Third arg is potential async validators
      'notes': new FormControl(null, null)      
    });

  }

  SaveDeposit() {

    if (this._appService.VoucherAdminService.VoucherClient.clientId != "") {

      // Clear current client in VoucherAdminService
      this._appService.VoucherAdminService.ClientTransaction = new VoucherTransactionModel;

      // Set current client for API call
      this._appService.VoucherAdminService.ClientTransaction.clientId = this._appService.VoucherAdminService.VoucherClient.clientId;
      this._appService.VoucherAdminService.ClientTransaction.amount = this._depositForm.get("amount").value;
      this._appService.VoucherAdminService.ClientTransaction.notes = this._depositForm.get("notes").value;
      this._appService.VoucherAdminService.ClientTransaction.user = this._appService.LoginService.CurrentLoggedInUser.username;

      // Call service
      this._appService.VoucherAdminService.SaveDeposit(this._depositForm); // pass deposit form to reset if api call is successful
    }
  }

  GetBasketDetails(id: string) {
    this._appService.VoucherAdminService.busy = true;
    this.detailsOfBasket = [];

    this._appService.VoucherAdminService.basketDetailLists.forEach((el) => {
      if (el.id == id) {
        this.detailsOfBasket.push(el);
      }
    });

    this._appService.VoucherAdminService.busy = false;

  }

  CloseErrorMessage() {
    this._appService.VoucherAdminService.error = false;
    this._appService.VoucherAdminService.errorMessage = "";
  }

  

}

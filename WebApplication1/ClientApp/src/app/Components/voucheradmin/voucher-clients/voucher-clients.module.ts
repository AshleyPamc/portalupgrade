import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";

import { VoucherClientsComponent } from './voucher-clients.component';

const routes: Routes = [{

  path: '',
  component: VoucherClientsComponent,
  children: [
    { pathMatch: 'full', path: 'voucher-clients', component: VoucherClientsComponent }
  ]
}];

@NgModule({
  declarations: [VoucherClientsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule
  ],
  exports: [ReactiveFormsModule, FormsModule]
})
export class VoucherClientsModule { }

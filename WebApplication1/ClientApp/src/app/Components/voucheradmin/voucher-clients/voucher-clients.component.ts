import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { AppService } from './../../../services/app.service';
import { VoucherClientModel } from './../../../models/VoucherClientModel';

@Component({
  selector: 'app-voucher-clients',
  templateUrl: './voucher-clients.component.html',
  styleUrls: ['./voucher-clients.component.css']
})
export class VoucherClientsComponent implements OnInit {

  _clientForm: FormGroup;
  _currentClient: VoucherClientModel;
  
  constructor(private _appService: AppService, private route: Router) { }   

  ngOnInit() {

    // Set username
    this._appService.VoucherAdminService.username = this._appService.LoginService.CurrentLoggedInUser.username;

    // Get clients if client list is empty (so only on first load)
    if (this._appService.VoucherAdminService.VoucherClients.length < 1) {
      this._appService.VoucherAdminService.GetVoucherClients();
    }

    // Load Basket Hp Codes for Select(Dropdown) in html
    this._appService.MedivouchService.GetBasketHpCodes();

    // Build form
    this._clientForm = new FormGroup({
      'clientName': new FormControl(null, Validators.required),  //First arg is initial state/value // Second arg is single validator or array of validators // Third arg is potential async validators
      'contactPerson': new FormControl(null, Validators.required),
      'telNo': new FormControl(null, Validators.required),
      'clientEmail': new FormControl(null, [Validators.required, Validators.email]),
      'hp': new FormControl(null, null)
    });
          
  }

  AddNewClient() {
    this._clientForm.reset;
    this._appService.VoucherAdminService.showAddNewClientModal = true
  }

  SaveNewClient() {

    // Clear current client in VoucherAdminService
    this._appService.VoucherAdminService.VoucherClient = new VoucherClientModel;

    // Get hpcode and opt from what was selected in dropdown
    this._appService.MedivouchService.VoucherHpList.forEach((el) => {
          if(el.opt == this._clientForm.get("hp").value)
          {
            this._appService.VoucherAdminService.VoucherClient.hpCode = el.hpCode;
            this._appService.VoucherAdminService.VoucherClient.opt = el.opt;
          }
    })
    
    // Set current client for API call
    this._appService.VoucherAdminService.VoucherClient.clientName = this._clientForm.get("clientName").value;
    this._appService.VoucherAdminService.VoucherClient.contactPerson = this._clientForm.get("contactPerson").value;
    this._appService.VoucherAdminService.VoucherClient.telNo = this._clientForm.get("telNo").value;
    this._appService.VoucherAdminService.VoucherClient.email = this._clientForm.get("clientEmail").value;
    this._appService.VoucherAdminService.VoucherClient.user = this._appService.LoginService.CurrentLoggedInUser.username;
    
    // Call service
    this._appService.VoucherAdminService.CreateNewClient(this._clientForm);
  }

  EditClient(clientToEdit: VoucherClientModel) {
        
    // Reset Form
    this._clientForm.reset;

    // Assign Values to form controls from selected item in datagrid 
    this._clientForm.controls.clientName.setValue(clientToEdit.clientName);
    this._clientForm.controls.contactPerson.setValue(clientToEdit.contactPerson);
    this._clientForm.controls.telNo.setValue(clientToEdit.telNo);    
    this._clientForm.controls.clientEmail.setValue(clientToEdit.email);
    this._clientForm.controls.hp.setValue(clientToEdit.hpCode);
        
    // Clear current client model
    this._currentClient = new VoucherClientModel();
    // Assing client id value to model
    this._currentClient.clientId = clientToEdit.clientId;

    // Show edit form modal
    this._appService.VoucherAdminService.showEditClientModal = true;
  }

  UpdateClient() {

    // Get hpcode and opt from what was selected in dropdown
    this._appService.MedivouchService.VoucherHpList.forEach((el) => {
          if(el.opt == this._clientForm.get("hp").value)
          {
            this._currentClient.hpCode = el.hpCode;
            this._currentClient.opt = el.opt;
          }
    })

    // Assing form values to client model
    this._currentClient.clientName = this._clientForm.get("clientName").value;
    this._currentClient.contactPerson = this._clientForm.get("contactPerson").value;
    this._currentClient.telNo = this._clientForm.get("telNo").value;
    this._currentClient.email = this._clientForm.get("clientEmail").value;    
    this._currentClient.user = this._appService.LoginService.CurrentLoggedInUser.username;

    // Pass model with data to edit and pass form to reset if edit is successful
    this._appService.VoucherAdminService.UpdateClient(this._currentClient, this._clientForm);
  }

  CancelEditModal() {

    // Clear current client model    
    this._currentClient = new VoucherClientModel();

    // Close edit modal
    this._appService.VoucherAdminService.showEditClientModal = false;
  }

  CloseErrorMessage() {
    this._appService.VoucherAdminService.error = false;
    this._appService.VoucherAdminService.errorMessage = "";
  }

  GoToTransactions(voucherClientDetail: VoucherClientModel) {
    this._appService.VoucherAdminService.VoucherClient = voucherClientDetail;    
    this.route.navigate(['/voucher-transactions']);
  }
}

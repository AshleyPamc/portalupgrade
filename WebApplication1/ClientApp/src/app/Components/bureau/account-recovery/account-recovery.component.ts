import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';

@Component({
  selector: 'app-account-recovery',
  templateUrl: './account-recovery.component.html',
  styleUrls: ['./account-recovery.component.css']
})
export class AccountRecoveryComponent implements OnInit {

  private _names = new Array();
  private _enableAddprovider: boolean = false

  constructor(private _appservice: AppService) {

  }

  ngOnInit() {
  }




  ClearingProviderArray() {
    this._names = [];
  }

  get btnClear(): boolean {
    let isEmpty: boolean = true

    if (this._names.length > 0) {
      isEmpty = false
    }
    return isEmpty
  }



}

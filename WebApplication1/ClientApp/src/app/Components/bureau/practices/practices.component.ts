import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-practices',
  templateUrl: './practices.component.html',
  styleUrls: ['./practices.component.css']
})
export class PracticesComponent implements OnInit {

  private _provider: string = "";
  private _providerSearchModalToggler: boolean = false
  private _unregisteredProviderModal: boolean = false

  private _missingProviderarray = []
  private _notRegistererdproviders = []

  private _copiedValuemissingproviders = []
  private _copiedValuemissingProvidersstring: string = ""

  private _copiedValueunregisterdProviders = []
  private _copiedValueunregisterdProvidersstring: string = ""

  private _valueOnclipBoard = ""
  private _valueOnclipBoard2 = ""

  private _alreadyRegistered: boolean

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService) { }

  get EnableSearchForUnregisteredProviders() {
    if (this._provider.length != 0) {
      return false
    } else {
      return true
    }

  }

  onClick() {
    this.joyrideService.startTour({
      steps: ['burPrac_1', 'burPrac_2', 'burPrac_3', 'burPrac_4', 'burPrac_5'],
      themeColor: '#313131'
    })
  }

  ReturnRemainingProviders() {
    this._copiedValuemissingproviders = []
    this._provider
    this._provider = this._provider.replace(/(?:\r\n|\r|\n)/g, " ");
    let RecievedProviderArray = this._provider.split(" ");
    let concatValue
    let amounttoberepeated: number
    let zerostringvalue: string = "0"


    RecievedProviderArray.forEach(el => {
      if (el.length < 7) {
        RecievedProviderArray = RecievedProviderArray.filter(x => x.length == 7)

        amounttoberepeated = 7 - el.length
        let x = zerostringvalue.repeat(amounttoberepeated)
        concatValue = x + el
        RecievedProviderArray.push(concatValue)

        if (concatValue === "0000000") {
          let needtoremove = concatValue
          RecievedProviderArray = RecievedProviderArray.filter(filteredvalue => filteredvalue !== needtoremove)
        }

      }
    });
    let missing = this._appService.LoginService.ProvidersLinkedToBureau.filter(item => RecievedProviderArray.indexOf(item.vendorid) < 0);
    this._missingProviderarray = missing;
    this._provider = RecievedProviderArray.toString();
    this._provider = this._provider.replace(/,/g, " ")
    this._providerSearchModalToggler = true
  }

  ShowMissingProvider() {
    this.ReturnRemainingProviders();
  }

  copyMessageMissingProviders() {
    try {
      this._missingProviderarray.forEach(el => {
        this._copiedValuemissingproviders.push(el.vendorid)
      });
      this._copiedValuemissingProvidersstring = this._copiedValuemissingproviders.toString();
      var splitcondition = this._copiedValuemissingProvidersstring.split(',');
      var ClipboardValue = splitcondition.join("\n");
      this._valueOnclipBoard = ClipboardValue
      let selBox = document.createElement('textarea');
      selBox.style.position = 'relative';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = this._valueOnclipBoard;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);

    } catch (error) {

    }




  }

  ReturnUnregisteredProviders() {
    this._copiedValueunregisterdProviders = []
    this._provider
    this._provider = this._provider.replace(/(?:\r\n|\r|\n)/g, " ");
    let RecievedProviderArray = this._provider.split(" ");
    let amounttoberepeated: number
    let zerostringvalue: string = "0"
    let concatValue
    /*     this._BackendService.ListOfProvider.forEach(el => {
        }); */
    let vendorIds = this._appService.LoginService.ProvidersLinkedToBureau.map(function (e) { return e.vendorid; });
    let notRegistered: string[] = []
    RecievedProviderArray.forEach(el => {
      if (el.length < 7) {
        RecievedProviderArray = RecievedProviderArray.filter(x => x.length == 7)
        amounttoberepeated = 7 - el.length
        let x = zerostringvalue.repeat(amounttoberepeated)
        concatValue = x + el
        RecievedProviderArray.push(concatValue)
        if (concatValue === "0000000") {
          let needtoremove = concatValue
          RecievedProviderArray = RecievedProviderArray.filter(filteredvalue => filteredvalue !== needtoremove)
        }
      }
      RecievedProviderArray.forEach(recievedprovider => {
        if (recievedprovider != el) {
          el = null
          el = recievedprovider
        }

      });
      let foundIndex = vendorIds.indexOf(el);
      if (foundIndex < 0) {
        notRegistered.push(el);
      }
    })

    this._provider = RecievedProviderArray.toString();
    this._provider = this._provider.replace(/,/g, " ")
    this._notRegistererdproviders = notRegistered
    this._notRegistererdproviders.forEach(el => {
      if (el.length < 7) {
        this._notRegistererdproviders = this._notRegistererdproviders.filter(x => x.length == 7)
        amounttoberepeated = 7 - el.length
        let x = zerostringvalue.repeat(amounttoberepeated)
        concatValue = x + el
        this._notRegistererdproviders.push(concatValue)
        if (concatValue === "0000000") {
          let needtoremove = concatValue
          this._notRegistererdproviders = this._notRegistererdproviders.filter(filteredvalue => filteredvalue !== needtoremove)
        }
      }
    });
    this._notRegistererdproviders = this._notRegistererdproviders.filter(x => x != "")
    if (this._notRegistererdproviders.length == 0) {
      this._alreadyRegistered = true
    } else {
      this._unregisteredProviderModal = true
    }


  }

  ShowUnRegisteredProviders() {
    this.ReturnUnregisteredProviders()
  }

  copyMessageUnregisteredProviders() {
    try {
      this._notRegistererdproviders.forEach(el => {
        this._copiedValueunregisterdProviders.push(el)
      });

      this._copiedValueunregisterdProvidersstring = this._copiedValueunregisterdProviders.toString()
      var splitcondition2 = this._copiedValueunregisterdProvidersstring.split(',');
      var ClipboardValue2 = splitcondition2.join("\n");
      this._valueOnclipBoard2 = ClipboardValue2


      let selBox2 = document.createElement('textarea');
      selBox2.style.position = 'fixed';
      selBox2.style.left = '0';
      selBox2.style.top = '0';
      selBox2.style.opacity = '0';
      selBox2.value = this._valueOnclipBoard2;
      document.body.appendChild(selBox2);
      selBox2.focus();
      selBox2.select();
      document.execCommand('copy');
      document.body.removeChild(selBox2);

    } catch (error) {
    }

  }

  ngOnInit() {

  }

}

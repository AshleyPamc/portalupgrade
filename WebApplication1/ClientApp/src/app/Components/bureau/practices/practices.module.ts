import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { PracticesComponent } from './practices.component';
import { SharedModule } from './../../../../app/shared.module';
import { JoyrideModule } from 'ngx-joyride'

const routes: Routes = [{

  path: '',
  component: PracticesComponent,
  children: [
    { pathMatch: 'full', path: 'bureau-practices', component: PracticesComponent }
  ]

}];

@NgModule({
  declarations: [PracticesComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule,
    JoyrideModule.forRoot()

  ]
})
export class PracticeModule { }

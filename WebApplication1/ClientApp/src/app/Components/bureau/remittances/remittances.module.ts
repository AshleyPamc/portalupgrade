import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../../../app/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClarityModule } from "@clr/angular";
import { RemittancesComponent } from './remittances.component';
import { JoyrideModule } from 'ngx-joyride'

const routes: Routes = [{

  path: '',
  component: RemittancesComponent,
  children: [
    { pathMatch: 'full', path: 'bureau-remittances', component: RemittancesComponent }
  ]

}];

@NgModule({
  declarations: [RemittancesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),    
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    SharedModule,
    JoyrideModule.forRoot()
  ]
})
export class RimittancesModule { }

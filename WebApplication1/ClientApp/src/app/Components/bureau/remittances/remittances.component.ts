import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-remittances',
  templateUrl: './remittances.component.html',
  styleUrls: ['./remittances.component.css']
})
export class RemittancesComponent implements OnInit {

  constructor(public _appService: AppService, private readonly joyrideService: JoyrideService) { }

  ngOnInit() {
    //this, this._appService.LoginService.r
  }

  onClick() {
    this.joyrideService.startTour({
      steps: ['burRem_1', 'burRem_2', 'burRem_3', 'burRem_4'],
      themeColor: '#313131'
    })
  }
}

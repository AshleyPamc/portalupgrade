import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { HttpClientModule } from '@angular/common/http';
import { ClarityModule } from "@clr/angular";
import { SharedModule} from './../../../app/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes =
  [{
    path: '',
    component: DashboardComponent,
    children: [
      { pathMatch: 'full', path: 'bureau-dash', component: DashboardComponent }]
  }]

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    //HttpClientModule,
    ClarityModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [ReactiveFormsModule, FormsModule]
})
export class BureauModule { }

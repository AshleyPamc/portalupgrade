import { Component, OnInit, Inject, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './../../../services/app.service';
import { HttpClient } from '@angular/common/http';

import { LoginDetailsModel } from './../../../models/LoginDetailsModel';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';








@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() loginForm: FormGroup;


  constructor(private _appService: AppService, private fb: FormBuilder) { }



  ngOnInit() {
    this._appService.LoginService.BureauRecovery
    this._appService.LoginService.ClearLoginFields();
    this.loginForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    })
  }


  //Below method References the login http call that happens on LoginService service Refer to "Login()" on Security Service.
  Login() {
    let c: string;
    let d: string;

    c = this.loginForm.get('username').value;
    d = this.loginForm.get('password').value;

    this._appService.LoginService.Username = c;
    this._appService.LoginService.Password = d;

    this._appService.LoginService.Login();



  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { RejectClaimDetailModel } from './../../../models/RejectClaimDetaiModel';
import { RejectClaimComponent } from '../reject-claim/reject-claim.component';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';



@Component({
  selector: 'app-info-detail-lines',
  templateUrl: './info-detail-lines.component.html',
  styleUrls: ['./info-detail-lines.component.css']
})
export class InfoDetailLinesComponent implements OnInit {

  private _claimNo: string;
  private _tblrow: string;
  public _paidPRov: boolean;
  public _paidMemb: boolean;

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  @ViewChild('rejectionReason', {static: false}) rejectionReason: RejectClaimComponent;

  constructor(public _appService: AppService, public _router: Router, public sanitizer: DomSanitizer) {
    this._appService.CheckRunService.GetRejectionReasons();
    let selcl = this._appService.SecurityService.SelectedClaim.claimNo;

    //  console.log(selcl);
    for (var i = 0; i < this._appService.SecurityService.RecievedClaims.length; i++) {
      let cl = this._appService.SecurityService.RecievedClaims[i];
      let rclno = cl.claimNo;

      if (rclno === selcl) {
        if (cl.datePaid.toUpperCase() == "IN PROCESS") {
          this._paidPRov = false;
        } else {
          this._paidPRov = true;
        }
        if ((cl.reversedDate !== null) && (cl.reversedDate !== undefined) && (cl.reversedDate !== '')) {
          this._paidPRov = true;
        }
        if (cl.paid.includes("-")) {
          this._paidPRov = true;
        }

      }
    }

    let selclm = this._appService.MemberClaimService.SelectedClaim.claimNo;
    for (var i = 0; i < this._appService.MemberClaimService.RecievedClaims.length; i++) {
      let clM = this._appService.MemberClaimService.RecievedClaims[i];
      let rclnoM = clM.claimNo;

      if (rclnoM === selclm) {
        if (clM.datePaid.toUpperCase() == "IN PROCESS") {
          this._paidMemb = false;
        } else {
          this._paidMemb = true;
        }
        if ((clM.reversedDate !== null) && (clM.reversedDate !== undefined) && (clM.reversedDate !== '')) {
          this._paidPRov = true;
        }
        if (clM.paid.includes("-")) {
          this._paidMemb = true;
        }
      }
    }

  }

  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
  }

  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  public get tblrow(): string {
    return this._tblrow;
  }
  public set tblrow(value: string) {
    this._tblrow = value;
  }
  public get claimNo(): string {
    return this._claimNo;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }

  Rejection() {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();
    x.claimNo = this._claimNo;
    x.tblRowId = this._tblrow;
    x.reverse = '0';
    x.rejectionReason = this._appService.SecurityService.rejectionReason;
    x.notes = this._appService.SecurityService.rejectionNote;
    let y = this._router.url;

  
    this._appService.SecurityService.RejectClaim(x);


  }

  Unreject(claimno: string, tblrowId: string) {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();
    x.claimNo = claimno;
    x.tblRowId = tblrowId;
    x.reverse = '1';
    let y = this._router.url;
    this._appService.SecurityService.RejectClaim(x);
  }

  CancelRejection() {
    this._appService.SecurityService.rejectModalShow = false;
    this.rejectionReason.claimRejectionNotes.reset();
  }

  popupRejections(claimno: string, tblrowId: string) {
    this.rejectionReason.claimRejectionNotes.reset();
   

    this.claimNo = claimno;
    this.tblrow = tblrowId;

    let y: RejectClaimDetailModel = new RejectClaimDetailModel();
    y.claimNo = this._claimNo;
    y.tblRowId = this._tblrow;

    this._appService.SecurityService.VerifyRejection(y);

  }

  ExistOnWebReverse(claimno: string) {

    let z: RejectClaimDetailModel = new RejectClaimDetailModel();

    z.claimNo = claimno;

    this._appService.SecurityService.CheckExistOnWebReverse(z);

  }

}

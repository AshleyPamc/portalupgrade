import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberClaimInfoComponent } from './member-claim-info.component';

describe('MemberClaimInfoComponent', () => {
  let component: MemberClaimInfoComponent;
  let fixture: ComponentFixture<MemberClaimInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberClaimInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberClaimInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

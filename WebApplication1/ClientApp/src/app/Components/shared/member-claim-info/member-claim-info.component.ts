import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { RejectClaimComponent } from '../reject-claim/reject-claim.component';
import { RejectClaimDetailModel } from './../../../models/RejectClaimDetaiModel';

@Component({
  selector: 'app-member-claim-info',
  templateUrl: './member-claim-info.component.html',
  styleUrls: ['./member-claim-info.component.css']
})
export class MemberClaimInfoComponent implements OnInit {

  private _rejectModalShow: boolean = false;
  @ViewChild('rejectionReason', { static: false }) rejectionReason: RejectClaimComponent;

  private _claimNo: string;
  private _tblrow: string;



  constructor(private _appService: AppService) {

    this._appService.CheckRunService.GetRejectionReasons();
  }

  ngOnInit() {

  }


  public get claimNo(): string {
    return this._claimNo;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }
  public get tblrow(): string {
    return this._tblrow;
  }
  public set tblrow(value: string) {
    this._tblrow = value;
  }

  Rejection() {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();

    x.claimNo = this._claimNo;
    x.tblRowId = this._tblrow;
    x.reverse = '0';
    x.rejectionReason = this._appService.SecurityService.rejectionNote;
    this._appService.MemberClaimService.RejectClaim(x);
    this._appService.SecurityService.unreverseBtn = false;
  }

  Unreject(claimno: string, tblrowId: string) {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();
    //let count: number = 0;
    //let i: number = 0;
    //this._appService.SecurityService.rejectionClaimList_1.forEach((rl) => {
    //  if ((rl.claimNo == claimno) && (rl.tblRowId === this.tblrow)) {
    //    x.rejectionReason = rl.rejectionReason;
    //    count = i;
    //  }
    //  i++;
    //});
    x.claimNo = claimno;
    x.tblRowId = tblrowId;
    x.reverse = '1';
    this._appService.MemberClaimService.RejectClaim(x);
    //this._appService.SecurityService.rejectionClaimList_1.splice(count, 1);
  }

  CancelRejection() {
    this._appService.SecurityService.rejectModalShow = false;
    this.rejectionReason.claimRejectionNotes.reset();
  }

  popupRejections(claimno: string, tblrowId: string) {
    this.rejectionReason.claimRejectionNotes.reset();
    this.claimNo = claimno;
    this.tblrow = tblrowId;

    // console.log("claimNo: ", claimno);
    //console.log("row: ", tblrowId);

    let y: RejectClaimDetailModel = new RejectClaimDetailModel();
    y.claimNo = this._claimNo;
    y.tblRowId = this._tblrow;

    this._appService.SecurityService.VerifyRejection(y);

  }
}



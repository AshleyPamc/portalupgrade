import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-member-claims',
  templateUrl: './member-claims.component.html',
  styleUrls: ['./member-claims.component.css']
})
export class MemberClaimsComponent implements OnInit {

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService) { }

  ngOnInit() {

  }

  onClick() {


    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['memclaim_1', 'memclaim_2', 'memclaim_3', 'memclaim_4', 'memclaim_5'],
        themeColor: '#313131'
      })

    }


  }
}

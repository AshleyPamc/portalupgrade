import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { RejectClaimComponent } from './../reject-claim/reject-claim.component';
import { RejectClaimDetailModel } from './../../../models/RejectClaimDetaiModel';

@Component({
  selector: 'app-claim-info',
  templateUrl: './claim-info.component.html',
  styleUrls: ['./claim-info.component.css']
})
export class ClaimInfoComponent implements OnInit {

  @ViewChild('rejectionReason', {static: false}) rejectionReason: RejectClaimComponent;

  private _claimNo: string;
  private _tblrow: string;



  constructor(private _appService: AppService) {

    this._appService.CheckRunService.GetRejectionReasons();

  }

  ngOnInit() {

  }

  public get tblrow(): string {
    return this._tblrow;
  }
  public set tblrow(value: string) {
    this._tblrow = value;
  }
  public get claimNo(): string {
    return this._claimNo;
  }
  public set claimNo(value: string) {
    this._claimNo = value;
  }

  Rejection() {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();

    x.claimNo = this._claimNo;
    x.tblRowId = this._tblrow;
    x.reverse = '0';
    x.rejectionReason = this._appService.SecurityService.rejectionNote;
    this._appService.SecurityService.RejectClaim(x);
  }

  Unreject(claimno: string, tblrowId: string) {
    let x: RejectClaimDetailModel = new RejectClaimDetailModel();

    x.claimNo = claimno;
    x.tblRowId = tblrowId;
    x.reverse = '1';
    this._appService.SecurityService.RejectClaim(x);
  }

  CancelRejection() {
    this._appService.SecurityService.rejectModalShow = false;
    this.rejectionReason.claimRejectionNotes.reset();
  }

  popupRejections(claimno: string, tblrowId: string) {
    this.rejectionReason.claimRejectionNotes.reset();
    console.log("claimNo: ", claimno);
    console.log("row: ", tblrowId);

    this.claimNo = claimno;
    this.tblrow = tblrowId;

    let y: RejectClaimDetailModel = new RejectClaimDetailModel();
    y.claimNo = this._claimNo;
    y.tblRowId = this._tblrow;

    this._appService.SecurityService.VerifyRejection(y);

  }
}

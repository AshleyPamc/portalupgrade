import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-downloads',
  templateUrl: './downloads.component.html',
  styleUrls: ['./downloads.component.css']
})
export class DownloadsComponent implements OnInit {

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(public _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  onClick() {
    if (this._appService.LoginService.isSpecialist) {
      this.joyrideService.startTour({
        steps: ['ops_1', 'ops_2'],
        themeColor: '#313131'
      })
    }
    if (this._appService.LoginService.isDentist) {
      this.joyrideService.startTour({
        steps: ['pg_1', 'pg_2'],
        themeColor: '#313131'
      })
    }
  }

  GetCurrYear(): string {
    let currYear = new Date().getFullYear().toString();
    return currYear;
  }
  GetPreviousYear(): string {
    let previousYear = (new Date().getFullYear()-1).toString();
    return previousYear;
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AppService } from './../../../services/app.service';

@Component({
  selector: 'app-reject-claim',
  templateUrl: './reject-claim.component.html',
  styleUrls: ['./reject-claim.component.css']
})
export class RejectClaimComponent implements OnInit {

  @Input() claimRejectionNotes: FormGroup;

  private _rejectExtra: boolean = false;

  public get rejectExtra(): boolean {
    return this._rejectExtra;
  }
  public set rejectExtra(value: boolean) {
    this._rejectExtra = value;
  }
  constructor(public fb: FormBuilder, public _appService: AppService) { }

  ngOnInit() {
    this.claimRejectionNotes = this.fb.group({
      reasons: [],
      other: [],
      customReason: []

    });
    this.claimRejectionNotes.get('customReason').disable();
  }

  defualtReason() {
    this._appService.SecurityService.rejectionReason = '';
    this._appService.SecurityService.rejectionReason = this.claimRejectionNotes.get('reasons').value;
    if (this._appService.SecurityService.rejectionReason === '420 -Rejected By Scheme') {
      this.claimRejectionNotes.get('customReason').enable();
      this.rejectExtra = true;
    } else {
      this.claimRejectionNotes.get('customReason').disable();
      this.rejectExtra = false;
    }
  }

  CustomReason() {
    this._appService.SecurityService.rejectionNote = '';
    this._appService.SecurityService.rejectionNote = this.claimRejectionNotes.get('customReason').value;
  }
}

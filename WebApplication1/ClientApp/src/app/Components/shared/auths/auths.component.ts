import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-auths',
  templateUrl: './auths.component.html',
  styleUrls: ['./auths.component.css']
})
export class AuthsComponent implements OnInit {


  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this._appService.AuthorizationService.ClearAllInputFields();
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    this.SetDetails();
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  onClick() {
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
      this.joyrideService.startTour({
        steps: ['auth_1', 'auth_2', 'auth_3', 'auth_4', 'auth_5',],
        themeColor: '#313131'
      })
    }
  }

  SetDetails() {
    if (this._appService.MembershipService.currMember != "") {
      this._appService.AuthorizationService.AuthDetail.memberno = this._appService.MembershipService.currMember;
      this._appService.AuthorizationService.Healthplan.hpcode = this._appService.MembershipService.currHpCode;
      this._appService.AuthorizationService.SearchAuth();
    }
  }

}

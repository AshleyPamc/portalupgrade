import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { RejectClaimComponent } from '../reject-claim/reject-claim.component';

@Component({
  selector: 'app-provclaims',
  templateUrl: './provclaims.component.html',
  styleUrls: ['./provclaims.component.css']
})
export class ProvclaimsComponent implements OnInit {

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;
  public lblName: string = 'Member No:';

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) {
    //console.log(this._appService.LoginService.CurrentLoggedInUser.userType);
    this.SetDetails();
  }

  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;

    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  SetDetails() {
    if (this._appService.MembershipService.currMember != "") {
      this._appService.SecurityService.ClaimHeaders.memberNo = this._appService.MembershipService.currMember;
      this._appService.SecurityService.HP_Contracts.hpcode = this._appService.MembershipService.currHpCode;
      this._appService.SecurityService.ValidateForm();
    }
  }

  onClick() {

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['claim_1', 'claim_2', 'claim_3', 'claim_4', 'claim_5'],
        themeColor: '#313131'
      })
    }

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
      this.joyrideService.startTour({
        steps: ['claim_1', 'claim_2', 'claim_3', 'claim_4', 'claim_5'],
        themeColor: '#313131'
      })

    }
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 2) {
      this.joyrideService.startTour({
        steps: ['claim_1', 'claim_2', 'claim_3', 'claim_4', 'claim_5', 'claim_6', 'claim_7'],
        themeColor: '#313131'
      })
    }

  }

  DisplayLableChange(data: string) {
    if (data.toUpperCase() == 'MDV') {
      this.lblName = 'Patient ID No:'
    } else {
      this.lblName = 'Member No:'
    }
  }

}

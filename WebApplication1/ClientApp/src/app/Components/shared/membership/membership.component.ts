import { Component, OnInit, Input, SecurityContext } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { MemberEligibilityModel } from './../../../models/MemberEligibilityModel';
import { MemberWaitSpecExlModel } from './../../../models/MemberWaitSpecExlModel';
import { BenifitRights } from './../../../models/benifit-rights';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BenHist } from '../../../models/ben-hist';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ChronicConditionApplicationModel } from './../../../models/ChronicConditionApplicationModel';


@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})




export class MembershipComponent implements OnInit {

  @Input() searchForm: FormGroup;
  public orginalHist: BenHist[] = [];
  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;
  public selectedMember: MemberEligibilityModel = new MemberEligibilityModel();
  public days: number = 0;
  public disableBtn: boolean = true;
  public imageLoad: boolean = false;
  public showBody: boolean = true;

  public showWaitSpecExclModal: boolean = false;
  public waitAndSpecExcls: MemberWaitSpecExlModel[] = [];

  public showSubmittedAppModal: boolean = false;

  @Input() AggreeForm: FormGroup;

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService,
    private fb: FormBuilder, public sanitizer: DomSanitizer, private fbAgr: FormBuilder)
  {
   
  }

  ngOnInit() {
   // console.log(this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filetype)
    this.searchForm = this.fb.group({
      'search': []
    });
    this.AggreeForm = this.fbAgr.group({
      'cbxAgree': []
    });
    if (this._appService.LoginService.currPopMsg > 0) {
      console.log(this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filename)
    }
  }

  getSafeUrl():SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe =  this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
   // console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }

  CheckLoadedImg() {
    this.imageLoad = true;
  }

  onClick() {
    if (this._appService.LoginService.CurrentLoggedInUser.userType == 3) {
      this.joyrideService.startTour({
        steps: ['Membship_1', 'Membship_2', 'Membship_5', 'Membship_6'],
        themeColor: '#313131'
      })
    }

    if (this._appService.LoginService.CurrentLoggedInUser.userType == 1) {
      this.joyrideService.startTour({
        steps: ['Membship_1', 'Membship_2', 'Membship_3', 'Membship_4', 'Membship_5', 'Membship_6'],
        themeColor: '#313131'
      });
    }


   
  }

  OpenMoreInfoModal(data: MemberEligibilityModel) {
    this.selectedMember = data;
    this._appService.MembershipService.GetBenRights(data);
    this._appService.MembershipService.returnBenSet = new BenifitRights();
    this._appService.MembershipService.moreInfo = true;
    this._appService.MembershipService.resultSetDisplay = [];
    console.log(this._appService.MembershipService.benRights.length);

  }

  GetBenifitResults(data: BenifitRights) {
    let c: BenifitRights = new BenifitRights();
    
    c.provid = this._appService.LoginService.CurrentLoggedInUser.provID;
    c.membid = this.selectedMember.subssn.substr(1, this.selectedMember.subssn.length - 1) + "-" + this.selectedMember.dependant;
    c.hp = this._appService.MembershipService.HealthplanContracts.hpcode;
    c.benName = data.benName;
    c.id = data.id;
    c.defaultProc = data.defaultProc;
    c.preAuth = data.preAuth;
    this._appService.MembershipService.GetBenifitResult(c);
  }


  ShowAllSeacrhHist() {
    this._appService.MembershipService.allHist = true;
    this._appService.MembershipService.specificHist = false;
    this._appService.MembershipService.benHistOpen = false;
    this._appService.MembershipService.busy
    this._appService.MembershipService.GetAllBeifitSearchHist();
  }

  SplitResults(data: string): string[]{

    if (data != '') {
      let c: string[] = data.split(";");

        //console.log(data);

      return c;
    }

  }

  SearchBenHist(data: string) {
    if (data == "" || data == null || data == undefined) {
      this._appService.MembershipService.benHisList = [];
      this._appService.MembershipService.benHisList = this._appService.MembershipService.orgHistLIst;
    } else {
      let c: string = data.toLowerCase();
      this._appService.MembershipService.benHisList = this._appService.MembershipService.orgHistLIst.filter((a) => {

        if (a.logDate.includes(c)) {
          return a;
        }
        if (a.membId.toLowerCase().includes(c)) {
          return a;
        }
        if (a.hp.toLowerCase().includes(c)) {
          return a;
        }
        if (a.benName.toLowerCase().includes(c)) {
          return a;
        }
        if (a.reff.toLowerCase().includes(c)) {
          return a;
        }
      })
    }
    this._appService.LoginService.currPopMsg
    this._appService.LoginService.popupList[0].filetype
  }

  NextModal() {
    this.showBody = false;
    this.imageLoad = false;
    let c: boolean = false;
    this._appService.MembershipService.busy = true;
    c = this.AggreeForm.get('cbxAgree').value;
    if (c) {
      if (this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filetype == 1) {
        var a = document.createElement('a');
        a.target = "_blank";
        a.setAttribute('type', 'hidden');
        //a.href = '../../PamcPortal/assets/PopUpDoc/' + this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filename;
        a.href = 'assets/PopUpDoc/' + this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filename;
        //console.log(a.href);
        a.download = this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg].filename;
        document.body.appendChild(a);
        a.click();
        a.remove();
      }
      this._appService.LoginService.MarkPopUpAsAccepted(this._appService.LoginService.popupList[this._appService.LoginService.currPopMsg]);
      this.AggreeForm.reset();
    } else {
      if (this._appService.LoginService.popupList.length > (this._appService.LoginService.currPopMsg + 1)) {
        this._appService.LoginService.PopModals((this._appService.LoginService.currPopMsg + 1))
      }
    }
    this._appService.MembershipService.busy = false;
    setTimeout((e) => {
      this.showBody = true;
    },250)
    
  }
  DisableContinue() {
    let c: boolean = false;
    c = this.AggreeForm.get('cbxAgree').value;
    if (c) {
      this.disableBtn = false;
    } else {
      this.disableBtn = true;
    }
  }

  ShowWaitSpecExclModal(list: MemberWaitSpecExlModel[]) {    
    this.showWaitSpecExclModal = true;
    this.waitAndSpecExcls = list;
  }

  CloseWaitSpecExclModal() {    
    this.showWaitSpecExclModal = false;
    this.waitAndSpecExcls = [];
  }

  viewSubmittedApplication(condAppNo: string) {

    let applicationNo: ChronicConditionApplicationModel = new ChronicConditionApplicationModel();
    applicationNo.conditionApplicationNumber = condAppNo;

    this._appService.ChronicConditionService.ViewSubmittedApplication(applicationNo);
    this.showSubmittedAppModal = true;    
  }
}

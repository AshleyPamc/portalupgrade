import { Component, OnInit } from '@angular/core';
import { AppService } from './../../../services/app.service';
import { JoyrideService } from 'ngx-joyride';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  public openSurvey: boolean = false;
  public url: string = "";
  public urlSafe: SafeResourceUrl;

  constructor(private _appService: AppService, private readonly joyrideService: JoyrideService, public sanitizer: DomSanitizer) { }


  ngOnInit() {
    //this.url = this._appService.SettingService.surveyLink;
    //this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    //this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
  }
  getSafeUrl(): SafeResourceUrl {
    this.url = this._appService.SettingService.surveyLink;
    this.url = this.url + "?e=" + this._appService.LoginService.CurrentLoggedInUser.username;
    //console.log("LINK: " + this.url);
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(encodeURI(this.url));
    //console.log("LINK: " + this.urlSafe);
    return this.urlSafe;

  }
  //onClick() {
  //  if (this._appService.SettingService.AllDetails) {
  //    this.joyrideService.startTour({
  //      steps: ['firstStep', 'secondStep'],
  //      themeColor: '#313131'
  //    })
  //  }
  //  if (this._appService.SettingService.DRCDetails) {
  //    this.joyrideService.startTour({
  //      steps: ['firstStep', 'secondStep'],
  //      themeColor: '#313131'
  //    })
  //  }
  //  if (this._appService.SettingService.PAMCDetails) {
  //    this.joyrideService.startTour({
  //      steps: ['secondStep'],
  //      themeColor: '#313131'
  //    })
  //  }


  //}

}

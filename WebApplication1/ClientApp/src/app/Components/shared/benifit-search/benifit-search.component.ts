import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AppService } from '../../../services/app.service';

@Component({
  selector: 'app-benifit-search',
  templateUrl: './benifit-search.component.html',
  styleUrls: ['./benifit-search.component.css']
})
export class BenifitSearchComponent implements OnInit {

  @Input() BenSearch: FormGroup;


  constructor(public _appService: AppService, private fb: FormBuilder) {

  }

  ngOnInit() {

    this.BenSearch = this.fb.group({

      'MemberNo': [],
      'date': [],
      'benType': [],
      'allBen': [],
      'allDates': [],
      'allSearch':[]
    });

   
  }





}

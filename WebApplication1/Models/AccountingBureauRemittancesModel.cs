﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AccountingBureauRemittancesModel
    {
        public string VENDORID { get; set; }
        public float AMOUNT { get; set; }
        public DateTime DATEPAID { get; set; }
        public int PREFIX { get; set; }
        public string PAYEE { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimMasterModel
    {
        public string hpCode { get; set; } = "";
        public string hpCodeDesc { get; set; } = "";
        public string provHosp { get; set; } = "";
        public string provHospDesc { get; set; } = "";
        public string provId { get; set; } = "";
        public string provName { get; set; } = "";
        public string provSpec { get; set; } = "";
        public string provSpecDescr { get; set; } = "";
        public string vendor { get; set; } = "";
        public string vendorName { get; set; } = "";
        public string refProvId { get; set; } = "";
        public string refProvName { get; set; } = "";
        public string refProvSpec { get; set; } = "";
        public string refProvSpecDescr { get; set; } = "";
        public string provClaim { get; set; } = "";
        public string policyNo { get; set; } = "";
        public string familyOpt { get; set; } = "";
        public string dependant { get; set; } = "";
        public string dependantFirstnm { get; set; } = "";
        public string dependantLastnm { get; set; } = "";
        public string membId { get; set; } = "";
        public string depOpthruDt { get; set; } = "";
        public DateTime dateReceived { get; set; }
        public string claimType { get; set; } = "";
        public string claimTypeDesc { get; set; } = "";
        public string authNo { get; set; } = "";
        public string diag1 { get; set; } = "";
        public string diag1desc { get; set; } = "";
        public string diag2 { get; set; } = "";
        public string diag2desc { get; set; } = "";
        public string diag3 { get; set; } = "";
        public string diag3desc { get; set; } = "";
        public string diag4 { get; set; } = "";
        public string diag4desc { get; set; } = "";
        //public string diag5 { get; set; } = "";
        //public string diag5desc { get; set; } = "";
        public string userName { get; set; } = "";
        public List<ClaimDetailModel> claimDetails { get; set; }        
        public string clientCreateDate { get; set; } = "";
        public string apiCreateDate { get; set; } = "";
        public int rec1Id { get; set; }
        public int fileId { get; set; }
        public string exception { get; set; } = "";
        public decimal detailLinesBilledTotal { get; set; }
    }
}

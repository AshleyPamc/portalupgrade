﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class RegisteredUsersModel
    {
        public string name { get; set; }
        public string username { get; set; }

        public bool showPass { get; set; }
        public string hiddenPass { get; set; }
        public string password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class HP_ContractsModel
    {
        public string HPCODE { get; set; }
        public string HPNAME { get; set; }
        public Boolean isEnabledMember { get; set; }
        public Boolean isEnabledClaim { get; set; }
        public Boolean isDentist { get; set; }
        public Boolean isSpecialist { get; set; }
        public Boolean contracted { get; set; }
        public int userType { get; set; }
        public bool isHospital { get; set; }
        public bool isPharmacy { get; set; }
        public bool chronicConditionsContract { get; set; }
    }
}

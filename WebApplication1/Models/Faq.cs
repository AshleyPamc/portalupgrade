﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class Faq
    {
        public string question { get; set; }
        public string answer { get; set; }
        public string usertype { get; set; }
    }
}

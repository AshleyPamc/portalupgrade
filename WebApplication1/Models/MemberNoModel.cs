﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberNoModel
    {
        public string memberNo { get; set; }
        public string hpcode { get; set; }
        public string usertype { get; set; }
    }
}

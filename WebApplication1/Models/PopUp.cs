﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class PopUpModel
    {
        
        public int msgId { get; set; }
        public string heading { get; set; }
        public string body { get; set; }
        public string filename { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public bool show { get; set; }
        public bool forceAccept { get; set; }
        public string username { get; set; }
        public int filetype { get; set; }
        public int modalsize { get; set; }
    }
}

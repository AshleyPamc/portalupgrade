﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimsSummary
    {
        public String Hpcode { get; set; } = "";
        public String Opt { get; set; } = "";
        public string IsRiskOpt { get; set; } = "";
        public Decimal ServiceYear { get; set; } = 0;
        public Decimal ServiceMonth { get; set; } = 0;
        public Decimal PaidYear { get; set; } = 0;
        public Decimal PaidMonth { get; set; } = 0;
        public Decimal ReceivedYear { get; set; } = 0;
        public Decimal ReceivedMonth { get; set; } = 0;
        public Decimal Billed { get; set; } = 0;
        public Decimal Net { get; set; } = 0;
        public string Status { get; set; } = "";
        public string ServiceYM { get; set; } = "";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimRemittance
    {
        public string Username { get; set; }
        public bool File { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime PaidDate { get; set; }
        public bool memProv { get; set; }
        public string paidTo { get; set; }
        public DateTime Date = DateTime.Now;
        public int UserType { get; set; }
        public bool isMember { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BenTrackCodes
    {
        public string code { get; set; }
        public string desc { get; set; }
    }
}

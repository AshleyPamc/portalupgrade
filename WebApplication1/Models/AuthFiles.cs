﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AuthFiles
    {
        public string membid { get; set; }
        public string hpcode { get; set; }
        public string username { get; set; }
        public string provid { get; set; }
        public string AddText { get; set; }
    }
}

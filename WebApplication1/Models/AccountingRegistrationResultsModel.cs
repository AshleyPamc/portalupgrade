﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AccountingRegistrationResultsModel
    {
        public Boolean isRegistered { get; set; }
        public string RegistrationMessage { get; set; }
        public int Matches { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderRegistrationResultsModel
    {
        public Boolean isRegistered { get; set; }
        public string RegistrationMessage { get; set; }
    }
}

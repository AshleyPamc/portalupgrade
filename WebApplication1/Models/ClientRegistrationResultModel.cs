﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClientRegistrationResultModel
    {
        public string registrationMessage { get; set; }
        public Boolean isRegistered { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class DetailedProviderRemittanceModel
    {
        public string CLAIMNO { get; set; }
        public string PROVID { get; set; }
        public string MEMBID { get; set; }
        public DateTime DATEFROM { get; set; }
        public double BILLED { get; set; }
        public double NET { get; set; }
        public DateTime DATEPAID { get; set; }
    }
}

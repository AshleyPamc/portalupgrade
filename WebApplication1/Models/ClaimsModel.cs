﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimsModel
    {
        public string hpCode { get; set; }
        public string specCode { get; set; }
        public string user { get; set; }
        public string endDate { get; set; }
        public string sum { get; set; }
        public string amountOfRecords { get; set; }
        public string specCodeDesc { get; set; }
        public string claimType { get; set; }
        public string whereClause { get; set; }
        public string sorting { get; set; }
        public string provid { get; set; }
        public string membid { get; set; }
        public string claimno { get; set; }
        public string serviceToDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderRemittancesModel
    {
        public string PROVID { get; set; }
        public DateTime DATEPAID { get; set; }
        public double NET { get; set; }
        public int CHPREFIX { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class VendorInfoModel
    {
        public string vendorType { get; set; }
        public string vendorTypeDescr { get; set; }
        public string vendorId { get; set; }
        public string vendorName { get; set; }
        public string specCode { get; set; }
        public string specCodeDescr { get; set; }
    }
}

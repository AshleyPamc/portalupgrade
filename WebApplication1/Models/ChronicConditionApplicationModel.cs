﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ChronicConditionApplicationModel
    {
        // Base info (applicable to all conditions)
        public string conditionApplicationNumber { get; set; }
        public string conditionCode { get; set; }
        public string conditionDescription { get; set; }
        public string membId { get; set; }
        public string membFirstName { get; set; }
        public string membLastName { get; set; }
        public string membEmail { get; set; }
        public string provId { get; set; }
        public string provEmail { get; set; }
        public string referredYN { get; set; }
        public string refProvSpec { get; set; }
        public string refProvid { get; set; }
        public string refProvSurname { get; set; }
        public string refProvNotes { get; set; }
        public List<FileItemModel> referralLetterFiles { get; set; }
        public string applicationDate { get; set; }
        public string status { get; set; }
        public string cancellationNote { get; set; }

        // Conditions Applied For
        public string[] selectedConditions { get; set; }

        // Base Biometric
        public string weight { get; set; }
        public string height { get; set; }
        public string bmi { get; set; }
        public string bloodPresSystolic { get; set; }
        public string bloodPresDiastolic { get; set; }
        public string cholesterolHDL { get; set; }
        public string cholesterolLDL { get; set; }
        public string cholesterolTRI { get; set; }
        public string cholesterolTotal { get; set; }
        public string cholesterolLdlHdlRatio { get; set; }
        public string diabetesYN { get; set; }
        public string diabetesType { get; set; }
        public string ogttResult { get; set; }
        public string hba1c { get; set; }        
        public string fastingGlucoseResult { get; set; }
        public string randomGlucoseResult { get; set; }
        public string diabTestDate { get; set; }


        // Risk Factors        
        public string predisposingRiskFactors { get; set; }
        public string alcohol { get; set; }
        public string obesity { get; set; }
        public string smoker { get; set; }
        public string smokeInPast { get; set; }
        public string cessationDate { get; set; }
        public string preExistingComplications { get; set; }
        public string ischaemicHeartDisease { get; set; }
        public string peripheralVascularDisease { get; set; }
        public string strokeAttacks { get; set; }

        //Requested Medication
        public List<MedicationModel> requestedMeds { get; set; }
        public List<FileItemModel> medPrescriptionFiles { get; set; }

        // Additional Docs
        public List<FileItemModel> additionalDocsFiles { get; set; }       

        // Diabetes
        public string diabetesIcd10 { get; set; }
        public List<FileItemModel> diabPathResultFiles { get; set; }

        // Hyperlipidaemia
        public string hyperlipidaemiaIcd10 { get; set; }
        public string hyperlipidaemiatherapyYN { get; set; }
        public string hyperlipidaemiaTherapyDuration { get; set; }
        public List<MedicationModel> hyperlipidaemiaMedHist { get; set; }
        public string hyperlipidaemiaAtheroscleroticYN { get; set; }
        public List<FileItemModel> hyperlipPathResultFiles { get; set; }
        public string hyperlipidaemiaHypertensionTreatmentYN { get; set; }
        public string hyperlipidaemiaGeneticHyperlipidaemiaYN { get; set; }
        public string hyperlipidaemiaMaleBloodRelativeYN { get; set; }
        public string hyperlipidaemiaFemaleBloodRelativeYN { get; set; }
        public string hyperlipidaemiaTendonXanthomaYN { get; set; }

        // Hypertension
        public string hypertensionIcd10 { get; set; }
        public string hypertensionSeverity { get; set; }
        //public string hypertensionCurrBloodPressure { get; set; }
        public string hypertensionTherapyYN { get; set; }
        public List<MedicationModel> hypertensionMedHist { get; set; }

        // Asthma
        public string asthmaIcd10 { get; set; }
        public string asthmaPleakFlow { get; set; }
        public string asthmaYounger3YN { get; set; }
        public List<FileItemModel> asthmaPaedFiles { get; set; }
        public List<FileItemModel> asthmaFlowVolFiles { get; set; }

        // Comorbids 
        public bool comorbidsExist { get; set; }

        public string asthmaCmICD10 { get; set; }  
        public List<MedicationModel> asthmaCmMedList { get; set; }
        public string diabCmICD10 { get; set; }
        public List<MedicationModel> diabCmMedList { get; set; }
        public string hyperlipCmICD10 { get; set; }
        public List<MedicationModel> hyperlipCmMedList { get; set; }
        public string hypertensionCmICD10 { get; set; }
        public List<MedicationModel> hypertensionCmMedList { get; set; }
        public string addisCmICD10 { get; set; }
        public List<MedicationModel> addisCmMedList { get; set; }
        public string bipolCmICD10 { get; set; }
        public List<MedicationModel> bipolCmMedList { get; set; }
        public string bronchCmICD10 { get; set; }
        public List<MedicationModel> bronchCmMedList { get; set; }
        public string cardiacCmICD10 { get; set; }
        public List<MedicationModel> cardiacCmMedList { get; set; }
        public string cardiomCmICD10 { get; set; }
        public List<MedicationModel> cardiomCmMedList { get; set; }
        public string copdCmICD10 { get; set; }
        public List<MedicationModel> copdCmMedList { get; set; }
        public string renalCmICD10 { get; set; }
        public List<MedicationModel> renalCmMedList { get; set; }
        public string coronarCmICD10 { get; set; }
        public List<MedicationModel> coronarCmMedList { get; set; }
        public string crohnsCmICD10 { get; set; }
        public List<MedicationModel> crohnsCmMedList { get; set; }
        public string diabinsipCmICD10 { get; set; }
        public List<MedicationModel> diabinsipCmMedList { get; set; }
        public string dysrhytCmICD10 { get; set; }
        public List<MedicationModel> dysrhytCmMedList { get; set; }
        public string epilCmICD10 { get; set; }
        public List<MedicationModel> epilCmMedList { get; set; }
        public string glaucomCmICD10 { get; set; }
        public List<MedicationModel> glaucomCmMedList { get; set; }
        public string haemophilCmICD10 { get; set; }
        public List<MedicationModel> haemophilCmMedList { get; set; }
        public string hypothCmICD10 { get; set; }
        public List<MedicationModel> hypothCmMedList { get; set; }
        public string sclerosisCmICD10 { get; set; }
        public List<MedicationModel> sclerosisCmMedList { get; set; }
        public string parkinsCmICD10 { get; set; }
        public List<MedicationModel> parkinsCmMedList { get; set; }
        public string rheumaCmICD10 { get; set; }
        public List<MedicationModel> rheumaCmMedList { get; set; }
        public string schizoCmICD10 { get; set; }
        public List<MedicationModel> schizoCmMedList { get; set; }
        public string lupusCmICD10 { get; set; }
        public List<MedicationModel> lupusCmMedList { get; set; }
        public string ulceraCmICD10 { get; set; }
        public List<MedicationModel> ulceraCmMedList { get; set; }

        // When searching for submitted applications for whole family
        public List<MemberEligibilityModel> familyMembers { get; set; }

        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }

        public string currentPage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class LobCodesModel
    {
        public string code { get; set; }
        public string descr { get; set; }
    }
}

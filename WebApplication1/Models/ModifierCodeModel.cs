﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ModifierCodeModel
    {
        public string modCode { get; set; } = "";
        public string modCodeDesc { get; set; } = "";
    }
}

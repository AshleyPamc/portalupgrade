﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ChronicConditionDiagsModel
    {
        public string conditionCode { get; set; }
        public string diagCode { get; set; }
        public string diagDesc { get; set; }

        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }
    }
}

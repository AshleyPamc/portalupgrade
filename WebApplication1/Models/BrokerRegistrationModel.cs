﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BrokerRegistrationModel
    {
        public string email { get; set; }
        public string brokerId { get; set; }
        public string hpCode { get; set; }
        public string name { get; set; }
        public string password { get; set; }

    }
}

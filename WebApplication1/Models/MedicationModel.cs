﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MedicationModel
    {
        // Params to manage list items in front end
        public string arrayId { get; set; }
        public string section { get; set; }

        // Data to be saved to DB
        public string icd10 { get; set; }        
        public string prescriptionSvcCode { get; set; }
        public string svcCodeDescr { get; set; }
        public string strength { get; set; }
        public string dosage { get; set; }
        public string qtyPerMonth { get; set; }
        public string numRepeats { get; set; }
        public bool historyMed { get; set; }        
        public bool comorbid { get; set; }
        //public string condCode { get; set; }


        // From Database
        public string conditionApplicationNumber { get; set; }
        public string rowid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class RejectClaimDetailModel
    {
        public string claimNo { get; set; }
        public string tblRowId { get; set; }
        public string rejectionReason { get; set; }
        public string rejectCode { get; set; }
        public string reverse { get; set; }
        public string notes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimSearchModel
    {
        public string provname { get; set; }
        public string userName { get; set; }
        public string practice { get; set; }
        public string doctor { get; set; }
        public string memberNo { get; set; }
        public string dependent { get; set; }
        public string memberName { get; set; }
        public string memberFirstName { get; set; }
        public string memberLastName { get; set; }
        public string plan { get; set; }
        public string receivedDate { get; set; }
        public string serviceDate { get; set; }
        public string serviceDateFrom { get; set; }
        public string serviceDateTo { get; set; }
        public string claimNo { get; set; }
        public string charged { get; set; }
        public string paid { get; set; }
        public string accountNo { get; set; }
        public string datePaid { get; set; }
        public string reversedDate { get; set; }
        public int checkNo { get; set; }
        public int userType { get; set; }
        public string isPaid { get; set; }
        public string status { get; set; }
        public string lobCode { get; set; }
        public string paidTo { get; set; }
        public string brokerId { get; set; }
        public string opt { get; set; }
        public string contracted { get; set; }
    }
}

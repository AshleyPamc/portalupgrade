﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class RejectionCodesModel
    {
        public string code { get; set; }
        public string reason { get; set; }
        public string lobCode { get; set; }
        public string username{ get; set; }
        public int userType { get; set; }
    }
}

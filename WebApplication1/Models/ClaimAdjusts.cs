﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimAdjusts
    {
        public string CLAIMNO { get; set; }
        public int CLAIMTBLROW { get; set; }
        public int SEQUENCE { get; set; }
        public string ADJCODE { get; set; }
        public decimal ADJUST { get; set; }
        public string COMMENTS { get; set; }
        public int CREATEBY { get; set; }
        public DateTime CREATEDATE { get; set; }
        public int LASTCHANGEBY { get; set; }
        public DateTime LASTCHANGEDATE { get; set; }
        public decimal ROWID { get; set; }
    }
}

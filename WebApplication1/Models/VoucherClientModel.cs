﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class VoucherClientModel
    {
        // From DB
        public string clientId { get; set; }
        public string clientName { get; set; }
        public string balance { get; set; }
        public string contactPerson { get; set; }
        public string telNo { get; set; }
        public string email { get; set; }
        public string hpCode { get; set; }
        public string opt { get; set; }
        public string createDate { get; set; }


        // Validation Params
        public bool success { get; set; }        
        public string message { get; set; }


        // Frontend params
        public string user { get; set; }
        public string fromDate { get; set; }
        public string todate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AuthModel
    {
        public string membno { get; set; }
        public string provid { get; set; }
        public string dep { get; set; }
        public string hpcode { get; set; }
        public string opt { get; set; }
        public string benType { get; set; }
        public string procCode { get; set; }
        public string scvdate { get; set; }
        public string refno { get; set; }
        public string username { get; set; }
        public string amount { get; set; }
        public string spec { get; set; }
        public string desc { get; set; }
        public string id { get; set; }
        public string create { get; set; }
        public string benDesc { get; set; }
        public string proc { get; set; }
        public string authno { get; set; }
        public string phCode { get; set; }
        public string benefitId { get; set; }
        public string placeSvc { get; set; }
        public decimal qty { get; set; }
        public string diag1 { get; set; }
        public string diag2 { get; set; }
        public string diag3 { get; set; }
        public string diag4 { get; set; }
        public string authRefNo { get; set; }
        public string note { get; set; }
        public bool reject { get; set; } 
    }
}

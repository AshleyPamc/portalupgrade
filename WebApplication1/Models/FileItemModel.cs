﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class FileItemModel
    {
        public string name { get; set; }
        public string type { get; set; }
        public string fileDesc { get; set; }
        public bool isEnabled { get; set; }
        public bool isProgress { get; set; }
        public string isProgressValue { get; set; }       
        public string base64Str { get; set; }
              
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class UsertypeModel
    {
        public string userTypeId { get; set; }
        public string userType { get; set; }
        public string createBy { get; set; }
        public string createDate { get; set; }
        public string changeBy { get; set; }
        public string changeDate { get; set; }
        public Boolean userRegistration { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class DefaultCodeModel
    {
        public string heading { get; set; }
        public string membName { get; set; }
        public string membSurname { get; set; }
        public string membIDNo { get; set; }
        public string prefix { get; set; }
        public string hpCode { get; set; }
        public string option { get; set; }
        public string provid { get; set; }
        public string provSpec { get; set; }
        public string provClass { get; set; }
        public string user { get; set; }
        public string id { get; set; }
        public bool enabled { get; set; }
        public string code { get; set; }

        public string phcode { get; set; }

        public string desc { get; set; }
        public string headDesc { get; set; }
        public bool createClaim { get; set; }



        public bool enterQty { get; set; }

        public int defaultQty { get; set; }

        public bool enterTooth { get; set; }

        public bool dependantOnCode { get; set; }

        public string depCode { get; set; }

        public string price { get; set; }
        public string fullPrice { get; set; }
        public string total { get; set; }
        public string membContact { get; set; }
        public string toothNo { get; set; }
        public string merchantID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class specmasters
    {
        public string desc { get; set; }
        public string bentype { get; set; }
        public string hpcode { get; set; }
        public string opt { get; set; }
        public int id { get; set; }
        public string user { get; set; }
        public string phcode { get; set; }
    }
}

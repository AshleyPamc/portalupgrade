﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ExcludedClaims
    {
        public string batchNo { get; set; }
        public string claimNo { get; set; }
        public string validated { get; set; }
        public string exluded { get; set; }
        public string rejected { get; set; }
    }
}

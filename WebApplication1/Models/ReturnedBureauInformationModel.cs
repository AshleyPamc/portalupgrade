﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ReturnedBureauInformationModel
    {
        public string ParsedDate { get; set; }
        public string FileName { get; set; }
    }
}

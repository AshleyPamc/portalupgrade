﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberInfoModel
    {
        public string familynumber { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string healthplan { get; set; }
        public string option { get; set; }
        public string relation { get; set; }
        public string dependant { get; set; }
        public string status { get; set; }
    }
}

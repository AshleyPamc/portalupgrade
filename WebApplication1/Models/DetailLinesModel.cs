﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class DetailLinesModel
    {
        public string diagdesc { get; set;  }
        public string diagcode{ get; set; }
        public string claimNo{ get; set; }
        public string provid{ get; set; }
        public string provspec{ get; set; }
        public string provname{ get; set; }
        public string provdesc{ get; set; }
        public string refProvId{ get; set; }
        public string refProfName{ get; set; }
        public string specCode{ get; set; }
        public string srvCode{ get; set; }
        public string srvCodeDesc { get; set; }
        public string fromDate{ get; set; }
        public string tooDate{ get; set; }
        public string provClaim{ get; set; }
        public string desc{ get; set; }
        public string dateRec{ get; set; }
        public string tblRowID{ get; set; }
        public string billed{ get; set; }
        public string coPay{ get; set; }
        public string adjCode{ get; set; }
        public string adjust{ get; set; }
        public string tarrif{ get; set; }
        public string net{ get; set; }
        public string adjCode2{ get; set; }
        public string comments{ get; set; }
        public string reversedate{ get; set; }
        public string claimOriginal{ get; set; }
        public string claimreverse{ get; set; }
        public bool isRejected{ get; set; }
        public bool rejectable{ get; set; }
        public string status{ get; set; }
        public string clinicalCodes { get; set; }
    }
}

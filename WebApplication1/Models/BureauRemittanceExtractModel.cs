﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BureauRemittanceExtractModel
    {
        public DateTime PaidDate { get; set; }
        public string Username { get; set; }
        public int UserType { get; set; }
        public DateTime Date = DateTime.Now;
        public bool File { get; set; }
        public string ProvID { get; set; }
        public int BureauID { get; set; }
        public string ClaimNumber { get; set; }
        public bool memProv { get; set; }
        public bool isMember { get; set; }
        public string paidTo { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BenifitRights
    {
        public int days { get; set; }
        public string benName { get; set; }
        public string opt { get; set; }
        public string hp { get; set; }
        public string provid { get; set; }
        public string membid { get; set; }
        public bool enabled { get; set; }
        public bool defaultProc { get; set; }
        public int id { get; set; }
        public List<string> results { get; set; }
        public bool preAuth { get; set; }
        public string reff {get;set;}
        public bool dentice { get; set; }
    }
}

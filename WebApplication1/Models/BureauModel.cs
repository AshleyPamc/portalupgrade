﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BureauModel
    {
        public string Name { get; set; }
        public int BureauID { get; set; }
    }
}

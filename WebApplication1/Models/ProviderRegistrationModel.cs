﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderRegistrationModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string provPracNum { get; set; }
        public string ProvSurname { get; set; }
        public string createBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ChangeBy { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool Success { get; set; }
        public int userType { get; set; }
        public string ErrorMessage { get; set; }
        public string contactNo { get; set; }
        public bool isAdmin { get; set; }
    }
}

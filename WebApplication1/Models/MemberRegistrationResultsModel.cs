﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberRegistrationResultsModel
    {
        public Boolean isRegistered { get; set; }
        public string RegistrationMessage { get; set; }
        public string RegistrationUser { get; set; }
    }
}

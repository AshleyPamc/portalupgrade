﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class VendoridsModel
    {
        public string provider { get; set; }
        public string vendor { get; set; }
        public string lastName { get; set; }
        public string displayItem { get; set; }
    }
}

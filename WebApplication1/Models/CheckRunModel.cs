﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class CheckRunModel
    {
        public string mainPolicyNumber { get; set; }
        public string principleMainMemberNumber { get; set; }
        public string NameSurnamePrincipleMember { get; set; }
        public string idNumberofMember { get; set; }
        public string claimType { get; set; }
        public string provId { get; set; }
        public string speciality { get; set; }
        public string providerName { get; set; }
        public string refProvId { get; set; }
        public string reqSpec { get; set; }
        public string productDescription { get; set; }
        public string productCode { get; set; }
        public string claimNumber { get; set; }
        public string dateofIncident { get; set; }
        public string namSurnameofdeceased { get; set; }
        public string idNumberofDeceased { get; set; }
        public string dateOfClaimReceived { get; set; }
        public string amountOfClaimReported { get; set; }
        public string amountOfClaimApproved { get; set; }
        public string dateOfClaimPaid { get; set; }
        public string paymentBatchNo { get; set; }
        public string amountOfClaimPaid { get; set; }
        public string claimStatus { get; set; }
        public string claimAmountOutstanding { get; set; }
        public string bankAccountName { get; set; }
        public string bankAccountBranchCode { get; set; }
        public string bankAccountNumber { get; set; }
        public string reasonCode1 { get; set; }
        public string reasonCode2 { get; set; }
        public string option { get; set; }
        public string reqSpecDescription { get; set; }
        public string patientName { get; set; }
        public string patientMembNo { get; set; }
        public string patientBirth { get; set; }
        public string rowId { get; set; }
        public string rowCount { get; set; }
        public string reverse { get; set; }
        public string note { get; set; }
        public bool validated { get; set; }
        public bool excluded { get; set; }
        public bool rejected { get; set; }
        public string batchNo { get; set; }
        public int page { get; set; }
        public string rejReason { get; set; }
        public string type { get; set; }
    }
}

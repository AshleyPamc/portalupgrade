﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AuthSpecDetail
    {
        public string procCode { get; set; }
        public string benCode { get; set; }
        public string phCodeOnNetwork { get; set; }
        public string phCodeOnNetworkDesc { get; set; }
        public string phCodeOffNetwork { get; set; }
        public string phCodeOffNetworkDesc { get; set; }
        public string phCodeDetermined { get; set; }
        public string phCodeDeterminedDesc { get; set; }
        public string placeSvcCode { get; set; }
        public bool displayAvailableBenefit { get; set; }
        public bool enterAmount { get; set; }
        public bool availableBenefitToAmtCheck { get; set; }
        public bool includeLengthOfStay { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberWaitSpecExlModel
    {
        public string membId { get; set; }
        public string dependant { get; set; }
        public string description { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}

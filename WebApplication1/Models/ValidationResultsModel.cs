﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ValidationResultsModel
    {
        public bool valid { get; set; }
        public string message { get; set; }
    }
}

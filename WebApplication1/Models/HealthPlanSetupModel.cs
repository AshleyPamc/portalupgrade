﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class HealthPlanSetupModel
    {
        public string HPCode { get; set; }
        public string HPName { get; set; }
        public Boolean isEnabledMember { get; set; }
        public Boolean isEnabledBroker { get; set; }
        public Boolean isEnabledClaim { get; set; }
        public Boolean dentists { get; set; }
        public Boolean specialists { get; set; }
        public Boolean membRegistration { get; set; }
        public Boolean contracted { get; set; }
        public Boolean notcontracted { get; set; }
        public int HPDATE { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class VoucherTransactionModel
    {
        // From DB
        public string tranId { get; set; }
        public string tranCode { get; set; }
        public string description { get; set; }
        public string amount { get; set; }        
        public string clientId { get; set; }
        public string voucherId { get; set; }
        public string voucherBatch { get; set; }
        public string notes { get; set; }
        public string createDate { get; set; }
              
        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }
        public string balance { get; set; }
    }
}
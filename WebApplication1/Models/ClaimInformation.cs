﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimInformation
    {
        public string PROCCODE { get; set; }
        public DateTime FROMDATESVC { get; set; }
        public decimal BILLED { get; set; }
        public decimal NET { get; set; }
        public decimal QTY { get; set; }
        public string DIAGCODE { get; set; }
        public int TBLROWID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderDetailModel
    {
        public string ProvID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Contact { get; set; }
        public string Contract { get; set; }
        public string Email { get; set; }
    }
}

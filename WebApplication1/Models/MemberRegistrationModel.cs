﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberRegistrationModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public int userType { get; set; }
        public string createBy { get; set; }
        public DateTime createDate { get; set; }
        public string changeBy { get; set; }
        public DateTime changeDate { get; set; }
        public string memberNumber { get; set; }
        public string memberIdnumber { get; set; }
        public string memberDOB { get; set; }
        public string hpCode { get; set; }
    }
}

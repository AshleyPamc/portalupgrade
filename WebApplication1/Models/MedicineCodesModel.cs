﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MedicineCodesModel
    {      
        public string svcCode { get; set; }
        public string phCode { get; set; }
        public string svcDesc { get; set; }

        // Validations vars
        public bool success { get; set; }
        public string message { get; set; }
        public string user { get; set; }
    }
}

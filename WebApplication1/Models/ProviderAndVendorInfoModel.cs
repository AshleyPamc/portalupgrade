﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderAndVendorInfoModel
    {
        public string provid { get; set; }
        public string lastName { get; set; }
        public string specCode { get; set; }
        public string specCodeDescr { get; set; }
        public List<VendoridsModel> vendors { get; set; } = new List<VendoridsModel>();
    }
}

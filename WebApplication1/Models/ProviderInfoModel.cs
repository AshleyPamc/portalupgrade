﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderInfoModel
    {
        public string provid { get; set; }
        public string lastName { get; set; }
        public string specCode { get; set; }
        public string specCodeDescr { get; set; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class TestClaimHeaderModel
    {
        public string membNum { get; set; }
        public string depCode { get; set; }
        public string hpCode { get; set; }
    }
}

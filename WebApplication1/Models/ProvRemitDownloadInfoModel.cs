﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProvRemitDownloadInfoModel
    {
        public string ParsedDate { get; set; }
        public string FileName { get; set; }
    }
}

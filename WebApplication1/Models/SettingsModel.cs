﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class SettingsModel
    {
        public string origanalSettingsId { get; set; }
        public string settingsId { get; set; }
        public string description { get; set; }
        public bool required { get; set; }
        public bool enabled { get; set; }
        public string createBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ChangeBy { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool administrator { get; set; }
        public bool administratorRequired { get; set; }
        public bool provider { get; set; }
        public bool providerRequired { get; set; }
    }
}

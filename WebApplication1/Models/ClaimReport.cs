﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimReport
    {
        public string dateCreated { get; set; }
        public string batchNo { get; set; }
        public string provid { get; set; }
        public string memb { get; set; }
        public string billed { get; set; }
        public string hpcode { get; set; }
        public string detailLines { get; set; }
        public string user { get; set; }
        public string ezcapClaimNo { get; set; }
        public int page { get; set; }
        public string capturedFrom { get; set; }
        public string capturedTo { get; set; }
        public bool userClaimsOnly { get; set; }
        public string username { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderRecoveryModel
    {
        public string NewPassword { get; set; }
        public string ChangeBy { get; set; }
        public DateTime ChangeDate { get; set; }
        public string Email { get; set; }
        public string provPracNum { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}

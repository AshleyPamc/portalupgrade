﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimReversalModel
    {
        public string memberNumber { get; set; }
        public string healthPlan { get; set; }
        public string serviceDate { get; set; }
        public string claimNumber { get; set; }
        public string chargedAmount { get; set; }
        public string paidAmount { get; set; }
        public string paidDate { get; set; }
        public string recievedDate { get; set; }
    }
}

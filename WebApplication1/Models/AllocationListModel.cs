﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AllocationListModel
    {
        public string basketId { get; set; }
        public decimal basketAmount { get; set; }
        public decimal fullPrice { get; set; }
        public int qty { get; set; }
        public string clientId { get; set; }

        // Validation params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }
    }
}

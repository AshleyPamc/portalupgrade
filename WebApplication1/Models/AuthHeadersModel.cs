﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AuthHeadersModel
    {
        public string userName { get; set; }
        public string Practice { get; set; }
        public string Doctor { get; set; }
        public string MemberNo { get; set; }
        public string DependentCode { get; set; }
        public string MemberName { get; set; }
        public string Plan { get; set; }
        public string ServiceDate { get; set; }
        public string AuthNo { get; set; }
        public string AuthorizedAmount { get; set; }
        public string LOBCODE { get; set; }
        public string DESCR { get; set; }
        public string MemberID { get; set; }
        public string reff { get; set; }
        public DateTime MemberDOB { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class UserRightsModel
    {
        public bool checkrunTab { get; set; }
        public bool benefitlookup { get; set; }
        public bool claimCaptureTab { get; set; }
        public bool claimSearchRejection { get; set; }
        public bool releaseCheckrun { get; set; }
        public bool claimSearchReverse { get; set; }
        public bool editReleaseBatch { get; set; }
        public bool reportTab { get; set; }
        public bool authUploadTabd { get; set; }
        public bool authsubmisionTab { get; set; }
        public bool benSetupTab { get; set; }
        public bool authLogTab { get; set; }
        public bool singleAuth { get; set; }
        public bool membership { get; set; }
        public bool membClaims { get; set; }
        public bool provClaims { get; set; }
        public bool accRegister { get; set; }
        public bool Settings { get; set; }
        public bool hpSetup { get; set; }
        public bool RegUsers { get; set; }
        public bool manuals { get; set; }
        public bool claimsRestore { get; set; }
        public bool userRights { get; set; }
        public bool voucherPartners { get; set; }
        public bool medivouch { get; set; }
        public bool chronicConditions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ServicetypesModel
    {
        public string phCode { get; set; }
        public string phDesc { get; set; }
        public string phOpt { get; set; }
        public string hpCode { get; set; }
        public string type { get; set; }
    }
}

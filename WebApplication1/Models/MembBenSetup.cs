﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MembBenSetup
    {
        public int id { get; set; }
        public string hpcode { get; set; }
        public string opt { get; set; }
        public string copyhp { get; set; }
        public string copyopt { get; set; }
        public string copydesc { get; set; }
        public bool all { get; set; }
        public string descr { get; set; }
        public string procCodes { get; set; }
        public string procExc { get; set; }
        public int fromAge { get; set; }
        public int toAge { get; set; }
        public int limit { get; set; }
        public int days { get; set; }
        public bool family { get; set; }
        public bool useDefaultProc { get; set; }
        public string hpname { get; set; }
        public bool preAuth { get; set; }
        public string dentist { get; set; }
        public int page { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class MemberEligibilityModel
    {
        public string MEMBID { get; set; }
        public Int16 RLSHIP { get; set; }
        public string LASTNM { get; set; }
        public string FIRSTNM { get; set; }
        public string EMAIL { get; set; }
        public DateTime HPFROMDT { get; set; }
        public string HPCODE { get; set; }
        public string OPT { get; set; }
        public DateTime OPFROMDT { get; set; }
        public string OPTHRUDT { get; set; }
        public string BIRTH { get; set; }
        public string SUBSSN { get; set; }
        public string RELATION { get; set; }
        public string DEPENDANT { get; set; }
        public string HPName { get; set; }
        public string LOBCODE { get; set; }
        public string USER { get; set; }
        public int USERTYPE { get; set; }
        public string term_code { get; set; }
        public string MemberStatus { get; set; }
        public string ToServDate { get; set; }
        public double days { get; set; }
        public decimal consultations { get; set; }
        public List<string> listOfhpCode = new List<string>();
        public int type { get; set; }
        public bool dentist { get; set; }
        public string FROMDATE { get; set; }
        public string TODATE { get; set; }
        public List<MemberWaitSpecExlModel> waitAndSpecExcls { get; set; }

    }
}

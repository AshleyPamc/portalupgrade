﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class PatientWellnessFormModel
    {
        // From/To DB
        public string idNo { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string tel { get; set; }
        public string cell { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string medicalAidYN { get; set; }
        public string medicalAidPlan { get; set; }
        public string medicalAidNo { get; set; }
        public string medicalAidDepCode { get; set; }
        public string screeningDate { get; set; }
        public string assessmentConsent { get; set; }
        public string chronicConditions { get; set; }
        public string medications { get; set; }
        public string height { get; set; }
        public string weight { get; set; }
        public string bmi { get; set; }
        public string waist { get; set; }
        public string bloodPressureSystolic { get; set; }
        public string bloodPressureDiastolic { get; set; }
        public string cholesterolHDL { get; set; }
        public string cholesterolTRI { get; set; }
        public string cholesterolLDL { get; set; }
        public string cholesterolTotal { get; set; }
        public string bloodSugarMmols { get; set; }
        public string hivConsent { get; set; }
        public string sexPastSixMonths { get; set; }
        public string condomPastSixMonths { get; set; }
        public string getAidsWorry { get; set; }
        public string exposedAidsWorry { get; set; }
        public string monthLastAidsTest { get; set; }
        public string yearLastAidsTest { get; set; }
        public string toldAids { get; set; }
                
        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }
        
    }
}

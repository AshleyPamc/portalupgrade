﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class UserRightsGridModel
    {
        public string username { get; set; }
        public string objectid { get; set; }
        public bool enabled { get; set; }
        public string description { get; set; }
    }
}

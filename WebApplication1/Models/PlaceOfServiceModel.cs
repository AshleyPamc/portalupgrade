﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class PlaceOfServiceModel
    {
        public string Code { get; set; }
        public string Descr { get; set; }
        public Boolean Default_YN { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class BenHist
    {
        public string logDate { get; set; }
        public string hp { get; set; }
        public string membId { get; set; }
        public string[] results { get; set; }
        public string benName { get; set; }
        public string reff { get; set; }
    }
}

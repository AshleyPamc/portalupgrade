﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class AccountingBureuaRegistrationModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string[] ListofProviders { get; set; }
        public string createBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string ChangeBy { get; set; }
        public DateTime ChangeDate { get; set; }
        public bool Success { get; set; }
        public int userType { get; set; }
        public string Message { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class VoucherHpModel
    {
        // From DB
        public string hpCode { get; set; }
        public string opt { get; set; }
        public string frontFacingOptDesc { get; set; }
        public string prefix { get; set; }
        public string hpName { get; set; }
        public string lobCode { get; set; }
        
        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ClaimDetailModel
    {
        public string arrayId { get; set; } = "";
        public int lineNo { get; set; }
        public string procCode { get; set; } = "";
        public string procCodeDesc { get; set; } = "";
        public string procCodePadded { get; set; } = "";
        public string phCode { get; set; } = "";
        public string phCodeDesc { get; set; } = "";
        public DateTime svcDateFrom { get; set; }
        public DateTime svcDateTo { get; set; }
        public string billed { get; set; } = "";
        public decimal qty { get; set; }
        public string diag { get; set; } = "";
        public string diagDesc { get; set; } = "";
        public string rejCode { get; set; } = "";
        public string rejCodeDesc { get; set; } = "";
        public string tooth1 { get; set; } = "";
        public string tooth1Desc { get; set; } = "";
        public string tooth2 { get; set; } = "";
        public string tooth2Desc { get; set; } = "";
        public string tooth3 { get; set; } = "";
        public string tooth3Desc { get; set; } = "";
        public string tooth4 { get; set; } = "";
        public string tooth4Desc { get; set; } = "";
        public string tooth5 { get; set; } = "";
        public string tooth5Desc { get; set; } = "";
        public string tooth6 { get; set; } = "";
        public string tooth6Desc { get; set; } = "";
        public string tooth7 { get; set; } = "";
        public string tooth7Desc { get; set; } = "";
        public string tooth8 { get; set; } = "";
        public string tooth8Desc { get; set; } = "";
        public string modifier1 { get; set; } = "";
        public string modifier1Desc { get; set; } = "";
        public string modifier2 { get; set; } = "";
        public string modifier2Desc { get; set; } = "";
        public string modifier3 { get; set; } = "";
        public string modifier3Desc { get; set; } = "";
        public string modifier4 { get; set; } = "";
        public string modifier4Desc { get; set; } = "";
        public string note { get; set; } = "";
        public int rec2Id { get; set; }
        public string toothNumString { get; set; } = "";
        public string modCodesString { get; set; } = "";        
    }
}

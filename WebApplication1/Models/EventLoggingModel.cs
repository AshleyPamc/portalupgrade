﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class EventLoggingModel
    {
        public string amount { get; set; }
        public string eventDesc { get; set; }
        public string member { get; set; }
        public string provid { get; set; }
        public string username { get; set; }
        public string uuid { get; set; }

    }
}

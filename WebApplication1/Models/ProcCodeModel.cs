﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProcCodeModel
    {
        public string ProcCode { get; set; } = "";
        public string ProcCodeDesc { get; set; } = "";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BankingDetailModel
    {
        public string provId { get; set; }
        public string claimno { get; set; }
        public string claimType { get; set; }
        public string batchNo { get; set; }
        public string bankName { get; set; }
        public string branchCode { get; set; }
        public string accountNo { get; set; }
        public string mainPolicyNo { get; set; }
        public string accType { get; set; }
        public string user { get; set; }
        public string hpcode { get; set; }

    }
}

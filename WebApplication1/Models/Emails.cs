﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class Emails
    {
        public string provid { get; set; }
        public string email { get; set; }
        public string mediref { get; set; }
        public string drcref { get; set; }
        public string membid { get; set; }
    }
}

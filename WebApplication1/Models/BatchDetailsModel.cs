﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class BatchDetailsModel
    {
        public string provId { get; set; }
        public string claimno { get; set; }
        public string claimType { get; set; }
        public string batchNumber { get; set; }
        public string paymentType { get; set; }
        public string whereClause { get; set; }
        public string paydate { get; set; }
        public string hpCode { get; set; }
        public string sorting { get; set; }
    }
}

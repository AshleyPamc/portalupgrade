﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ChronicConditionCodesModel
    {
        public string code { get; set; }
        public string description { get; set; }
        public string selectable { get; set; }

        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

        // Frontend params
        public string user { get; set; }
    }
}

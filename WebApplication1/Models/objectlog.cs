﻿using PamcEzLinkLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class objectlog
    {
        public RecordType1 header { get; set; }
        public RecordType2[] detail { get; set; }
        public RecordType4[] notes { get; set; }
        public RecordType5[] diags { get; set; }
        public RecordType8[] rejects { get; set; }
    }
}

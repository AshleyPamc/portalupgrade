﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class SpecCodes
    {
        public string code { get; set; } // Spec Code
        public string desc { get; set; } // s.benCode = row["DESCR"].ToString() + "-" + row["BenType"].ToString();
        public int page { get; set; } // used by ben-setup.component
        public string procCode { get; set; }
        public string benCode { get; set; } // used by ben-setup.component
        public List<AuthSpecDetail> specDetail { get; set; }
        public bool selected { get; set; } // Selected from front end by user
        public int id { get; set; } // MasterId
        public string user { get; set; }
        public string provId { get; set; }
        public string srvDate { get; set; }
        public string amount { get; set; }
        public string phcode { get; set; }

        // Validation Params
        public bool success { get; set; }
        public string message { get; set; }

    }
}

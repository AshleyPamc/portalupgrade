﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediWalletWebsite_1.Models
{
    public class ClaimCodesModel
    {
        public string provid { get; set; }
        public string mediRef { get; set; }
        public string drcRef { get; set; }
        public bool enterQty { get; set; }
        public bool enterTooth { get; set; }
        public string toothNo { get; set; }
        public string diag { get; set; }
        public string user { get; set; }
        public string id { get; set; }
        public string membId { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string prefix { get; set; }
        public string phCode { get; set; }
        public string hpCode { get; set; }
        public string option { get; set; }
        public string code { get; set; }
        public string desc { get; set; }
        public string qty { get; set; }
        public string singleUnitAmount { get; set; }
        public string total { get; set; }
        public string membpay { get; set; }
        public string provpay { get; set; }
        public bool createClaim { get; set; }
    }
}

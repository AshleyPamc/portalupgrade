﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class CheckRunDetailsModel
    {
        public string claimType { get; set; }
        public string claimNo { get; set; }
        public string rowId { get; set; }
    }
}

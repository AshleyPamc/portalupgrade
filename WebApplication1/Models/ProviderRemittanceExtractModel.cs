﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class ProviderRemittanceExtractModel
    {
        public DateTime Date = DateTime.Now;
        public bool File { get; set; }
        public DateTime PaidDate { get; set; }
        public string Username { get; set; }
        public string ProvID { get; set; }
        public string BureauID { get; set; }
        public string claim { get; set; }
        public int prefix { get; set; }
        public string loginProvId { get; set; }
    }
}

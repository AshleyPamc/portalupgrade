﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PamcEzLinkLibrary;
using NEWPAMCPORTAL.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Base.PAMCController
    {


        public AdminController(IWebHostEnvironment env,IConfiguration con):base(env,con)
        {

        }

        /*Registration for accounting bureau*/
        [HttpPost]
        [Route("registerAccountingBureau")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //todo:TRY/CATCH missing
        public AccountingRegistrationResultsModel registerAccountingBureau([FromBody] AccountingBureuaRegistrationModel model)
        {
            AccountingRegistrationResultsModel registrationResults = new AccountingRegistrationResultsModel();

            SettingsModel returnedRow = Utillity.GetSpecificSetting("accReg", _portalConnectionString);
            string EmailBody = returnedRow.description.Replace("<email>", model.Email);
            string emailSubject = "Accounting Bureau - Registration";

            OperationLog log = new OperationLog();
            log.registration("Registration", DateTime.Now, model.Email, _portalConnectionString);

            registrationResults.isRegistered = true;

            DataTable dt = new DataTable();

            
            using (SqlConnection sqlCon = new SqlConnection(_drcConnectionString))
            {
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    string providernumbersstring = string.Format("'{0}'", string.Join("','", model.ListofProviders.Select(i => i.Replace("'", "''"))));

                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "select BUREAU_MASTERS.Name, count(*) AS Matches FROM VEND_MASTERS INNER JOIN BUREAU_MASTERS ON VEND_MASTERS.BUREAUID = BUREAU_MASTERS.BUREAUID\r\n" + "where VENDORID in (" + providernumbersstring + ") and VEND_MASTERS.EMAIL = '" + model.Email + "' and VEND_MASTERS.BUREAUID is NOT NULL GROUP BY BUREAU_MASTERS.Name";
                    cmd.Connection = sqlCon;
                    cmd.CommandText = sqlTextToExecute;
                    dt.Load(cmd.ExecuteReader());


                    foreach (DataRow dr in dt.Rows)
                    {
                        registrationResults.Name = dr["Name"].ToString();
                        registrationResults.Matches = Convert.ToInt32(dr["Matches"]);
                    }
                    //Object o = cmd.ExecuteScalar();
                    //int NumberOfValidProviders = Convert.ToInt32(o);

                    if (registrationResults.Matches != model.ListofProviders.Length)
                    {
                        dt = null;
                    }

                    if (dt != null)
                    {
                        model.Success = Convert.ToBoolean(registrationResults.Matches);

                    }
                    if (model.Success == true)
                    {
                        //Below is used to do the insert for accountingburo
                        using (SqlConnection SqlconInsert = new SqlConnection(_portalConnectionString))
                        {
                            if (SqlconInsert.State != ConnectionState.Open)
                            {
                                SqlconInsert.Open();
                            }

                            model.CreateDate = DateTime.Now;
                            model.ChangeDate = DateTime.Now;
                            model.userType = 2;
                            model.createBy = "System Registration";
                            model.ChangeBy = "System Registration";

                            BureauModel BureauId = Utillity.GetBureauId(registrationResults.Name, _drcConnectionString);


                            cmd.CommandType = CommandType.Text;

                            string sqlInsert = "INSERT INTO [dbo].[Users] ([username],[password],[name],[userType],[createBy],[createDate],[changeBy],[changeDate],[bureauId]) VALUES ('" + model.Email + "'" + ",'" + model.Password + "'" + ",'" + registrationResults.Name + "'" + " ,'" + model.userType + "'" + ",'" + model.createBy + "'" + " ,'" + model.CreateDate + "'" + ",'" + model.ChangeBy + "'" + " ,'" + model.ChangeDate + "', '" + BureauId.BureauID + "')";
                            cmd.Connection = SqlconInsert;
                            cmd.CommandText = sqlInsert;

                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {
                                    //throw new Exception();
                                }
                            }
                            catch (Exception ex)
                            {

                                if (ex.Message.Contains("Violation of PRIMARY KEY constraint"))
                                {
                                    registrationResults.isRegistered = false;
                                    registrationResults.RegistrationMessage = "This email address (" + model.Email + ") has already been registered";
                                    //throw new Exception();
                                }
                                else
                                {
                                    LogError(ex.Message, ex.StackTrace);
                                }
                            }


                            SqlconInsert.Close();
                        }
                    }
                    else
                    {
                        registrationResults.isRegistered = false;
                        registrationResults.RegistrationMessage = "The practice numbers that you have entered is not linked to your Bureau";
                    }
                }

                if (registrationResults.isRegistered == true)
                {
                    registrationResults.RegistrationMessage = "Welcome " + registrationResults.Name + " Thank you for Registering with PAMCs Provider-Portal. a email will be sent to you confriming successful registration.";
                    //Utillity.Sendnotification(emailSubject, EmailBody, "montie2206@gmail.com"); Jaco comment out 2023-09-14
                    Utillity.Sendnotification(emailSubject, EmailBody, "jaco@pamc.co.za");
                }

                return registrationResults;

            }
        }

        /*Registration for Providers*/
        [HttpPost]
        [Route("registerprovider")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProviderRegistrationResultsModel registerprovider([FromBody] ProviderRegistrationModel model)
        {
            ProviderRegistrationResultsModel registrationresults = new ProviderRegistrationResultsModel();
            SettingsModel returnedRow = Utillity.GetSpecificSetting("ProfReg", _portalConnectionString);
            SettingsModel provspecs = Utillity.GetSpecificSetting("AllowedProvSpecs", _portalConnectionString);
            SettingsModel provcontracts = Utillity.GetSpecificSetting("AllowedProvContracts", _portalConnectionString);
            DataTable dt = new DataTable();
            DataTable tbl = new DataTable();
            bool correctSpec = false;
            bool correctContr = false;


            string EmailBody = returnedRow.description.Replace("<email>", model.Email);
          //  string EmailSubject = "Provider - Registration";
            string[] AllowedSpecs = new string[provspecs.description.Split(",").Length];
            string[] AllowedContracts = new string[provcontracts.description.Split(",").Length];
            AllowedSpecs = provspecs.description.Split(",");
            AllowedContracts = provcontracts.description.Split(",");

            registrationresults.isRegistered = true;
            registrationresults.RegistrationMessage = "Thank you for Registering with PAMCs Provider-Portal.";

            using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    string sqlTextToExecute = "";
                    cmd.CommandType = CommandType.Text;
                    if (model.isAdmin)
                    {
                        sqlTextToExecute = "select count(*) from [PROV_MASTERS]\r\n" + "where PROVID = '" + model.provPracNum + "'\r\n";
                    }
                    else
                    {
                        sqlTextToExecute = "select count(*) from [PROV_MASTERS]\r\n" + "where PROVID = '" + model.provPracNum + "' and Email = '" + model.Email + "'\r\n";
                    }

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    Object o = cmd.ExecuteScalar();

                    if (o != null)
                    {
                        model.Success = Convert.ToBoolean(o);
                    }

                    if (model.Success == true)
                    {
                        using (SqlConnection SqlconInsert = new SqlConnection(_portalConnectionString))
                        {

                            using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                            {
                                cmd.CommandText = $"SELECT CONTRACT FROM PROV_MASTERS WHERE PROVID = '{model.provPracNum}'";

                                using (SqlDataReader dr = cmd.ExecuteReader())
                                {
                                    dt.Load(dr);
                                }
                                foreach (DataRow row in dt.Rows)
                                {
                                    foreach (string contract in AllowedContracts)
                                    {
                                        if (contract.Trim() == row["CONTRACT"].ToString().Trim())
                                        {
                                            correctContr = true;
                                        }
                                    }
                                }

                                cmd.CommandText = $"SELECT SPECCODE  FROM PROV_SPECINFO WHERE PROVID = '{model.provPracNum}'";

                                using (SqlDataReader dr = cmd.ExecuteReader())
                                {
                                    tbl.Load(dr);
                                }
                                foreach (DataRow row in tbl.Rows)
                                {
                                    foreach (string specs in AllowedSpecs)
                                    {
                                        if (specs.Trim() == row["SPECCODE"].ToString().Trim())
                                        {
                                            correctSpec = true;
                                        }
                                    }
                                }
                                Sqlcon.Close();
                            }
                            if (SqlconInsert.State != ConnectionState.Open)
                            {
                                SqlconInsert.Open();
                            }
                            model.CreateDate = DateTime.Now;
                            model.ChangeDate = DateTime.Now;
                            model.userType = 1;
                            model.createBy = "System Registration";
                            model.ChangeBy = "System Registration";

                            cmd.CommandType = CommandType.Text;

                            if (model.ProvSurname == "")
                            {
                                ProviderDetailModel ProviderName = Utillity.GetProviderName(model.Email, _drcConnectionString, model.provPracNum, model.isAdmin);
                                model.ProvSurname = ProviderName.LastName;
                            }
                            string sqlInsert = "INSERT INTO [dbo].[Users] ([username],[password],[name],[contact],[userType],[createBy],[createDate],[changeBy],[changeDate],[provId]) VALUES ('" + model.Email + "'" + ",'" + model.Password + "'" + ",'" + model.ProvSurname + "'" + " ,'" + model.contactNo + "'" + " ,'" + model.userType + "'" + ",'" + model.createBy + "'" + " ,'" + model.CreateDate + "'" + ",'" + model.ChangeBy + "'" + " ,'" + model.ChangeDate + "', '" + model.provPracNum + "')";

                            // string sqlInsert = "INSERT INTO [dbo].[Users] ([username],[password],[name],[userType],[createBy],[createDate],[changeBy],[changeDate],[provId]) VALUES ('" + model.Email + "'" + ",'" + model.Password + "'" + ",'" + model.ProvSurname + "'" + " ,'" + model.userType + "'" + ",'" + model.createBy + "'" + " ,'" + model.CreateDate + "'" + ",'" + model.ChangeBy + "'" + " ,'" + model.ChangeDate + "', '" + model.provPracNum + "')";
                            cmd.Connection = SqlconInsert;
                            cmd.CommandText = sqlInsert;

                            try
                            {
                                if (correctSpec && correctContr)
                                {
                                    int rowsAffected = cmd.ExecuteNonQuery();
                                    if (rowsAffected != 1)
                                    {
                                        //throw new Exception();
                                    }
                                }
                                else
                                {
                                    registrationresults.isRegistered = false;
                                    registrationresults.RegistrationMessage =
                                        "The provider must be a contracted and a dental or network gp provider on our system.";


                                }

                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Contains("Violation of PRIMARY KEY constraint"))
                                {
                                    registrationresults.isRegistered = false;
                                    registrationresults.RegistrationMessage = "This email address (" + model.Email + ") has already been registered";
                                }
                                else
                                {
                                    LogError(ex.Message, ex.StackTrace);
                                }
                            }
                            Sqlcon.Close();
                        }
                    }
                    else
                    {
                        registrationresults.isRegistered = false;
                        registrationresults.RegistrationMessage = "The practice numbers that you have entered is not linked to your account";
                    }
                }
                if (registrationresults.isRegistered == true)
                {
                    //Utillity.Sendnotification(EmailSubject, EmailBody, "montie2206@gmail.com"); // Was commented out already - Jaco 2023-09-14
                    Utillity.Sendnotification("Portal montie", EmailBody, "jaco@pamc.co.za");
                }
                Sqlcon.Close();
                return registrationresults;

            }
        }

        /*Registeration for client*/
        [HttpPost]
        [Route("registerclient")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ClientRegistrationResultModel Registerclient([FromBody] ClientRegistrationModel model)
        {
            ClientRegistrationResultModel registrationResults = new ClientRegistrationResultModel();
            registrationResults.isRegistered = true;

            using (SqlConnection SqlconInsert = new SqlConnection(_portalConnectionString))
            {
                if (SqlconInsert.State != ConnectionState.Open)
                {
                    SqlconInsert.Open();
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    model.CreateDate = DateTime.Now;
                    model.ChangeDate = DateTime.Now;
                    model.userType = 3;
                    model.createBy = "System Registration";
                    model.ChangeBy = "System Registration";

                    cmd.CommandType = CommandType.Text;

                    string sqlInsert = "INSERT INTO [dbo].[Users] ([username],[password],[name],[userType],[createBy],[createDate],[changeBy],[changeDate],[lobcode],[client]) VALUES('" + model.Email + "','" + model.Password + "','" + model.Name + "','" + model.userType + "','" + model.createBy + "','" + model.CreateDate + "','" + model.ChangeBy + "','" + model.ChangeDate + "','" + model.LOBCode + "', '" + model.client + "') \r\n";
                    cmd.Connection = SqlconInsert;
                    cmd.CommandText = sqlInsert;

                    try
                    {
                        int rowsAffected = cmd.ExecuteNonQuery();
                        if (rowsAffected != 1)
                        {
                            //throw new Exception();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Length != 0)
                        {
                            registrationResults.isRegistered = false;
                            //registrationResults.registrationMessage = "This email address (" + adminuser.Email + ") has already been registered";
                            registrationResults.registrationMessage = "The following error occured during the registration process: " + ex.Message + "";

                        }
                        LogError(ex.Message, ex.StackTrace);
                    }

                    if (registrationResults.isRegistered == true)
                    {
                        registrationResults.registrationMessage = "Welcome " + model.Name + " Thank you for Registering as an administrator with PAMCs Portal.";
                    }

                }
                SqlconInsert.Close();
            }
            return registrationResults;
        }

        /*Registration For Member*/
        [HttpPost]
        [Route("registermember")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public MemberRegistrationResultsModel Registermember([FromBody] MemberRegistrationModel model)
        {
            MemberRegistrationResultsModel registrationResults = new MemberRegistrationResultsModel();
            DataTable memberDetails = new DataTable();

            SettingsModel returnedRow = Utillity.GetSpecificSetting("memReg", _portalConnectionString);
            string EmailBody = returnedRow.description.Replace("<email>", model.username);
            string emailSubject = "Member - Registration";
            string memberFullname = "";

            OperationLog log = new OperationLog();
            log.membRegistration("Registration", DateTime.Now, model.username, model.memberNumber, _portalConnectionString);

            //registrationResults.isRegistered = true;
            //registrationResults.RegistrationMessage = "Thank you for Registering with PAMCs Provider-Portal. a email will be sent to you confriming successful registration.";

            if (model.hpCode == "000")
            {
                model.hpCode = "";
            }
            if (model.name == null)
            {
                var x = Utillity.membername(model.memberIdnumber, _portalConnectionString, DRCDatabase, PamcPortalDatabase, ReportingDatabase);
                model.name = x.Item1;
                memberFullname = x.Item2;
            }

            if (model.name == "No Member Found")
            {
                registrationResults.isRegistered = false;
                registrationResults.RegistrationMessage = "No Registered Member Was Found.";
            }
            else
            {




                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region  WhereClause

                        cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = model.hpCode;
                        cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = model.name;
                        cmd.Parameters.Add("@BIRTH", SqlDbType.DateTime).Value = model.memberDOB;
                        cmd.Parameters.Add("@membId", SqlDbType.VarChar).Value = model.memberIdnumber;
                        cmd.Parameters.Add("@membNum", SqlDbType.VarChar).Value = model.memberNumber;
                        cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = model.username;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.password;
                        cmd.Parameters.Add("@userType", SqlDbType.Int).Value = model.userType;
                        cmd.Parameters.Add("@createBy", SqlDbType.VarChar).Value = model.createBy;
                        cmd.Parameters.Add("@createDate", SqlDbType.DateTime).Value = model.createDate;
                        cmd.Parameters.Add("@changeBy", SqlDbType.VarChar).Value = model.changeBy;
                        cmd.Parameters.Add("@changeDate", SqlDbType.DateTime).Value = model.changeDate;

                        string WhereClause = "";


                        if (model.memberNumber != "")
                        {
                            WhereClause = "where (SUBSSN = 'M' + isnull([MemberPrefix],'') + @membid OR SUBSSN = 'M'+ @membid OR  MEMBERSHIP.MEMBID = @membid OR isnull([MemberPrefix],'') + @membid =  MEMBERSHIP.MEMBID)";
                        }
                        if (model.hpCode != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "HPCODE = @hpCode";
                            }
                            else
                            {
                                WhereClause = "where HPCODE = @hpCode";
                            }
                        }

                        if (model.name != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "FIRSTNM = @name";
                            }
                            else
                            {
                                WhereClause = "where FIRSTNM = @name";
                            }
                        }
                        if (model.memberDOB != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "BIRTH = @BIRTH";
                            }
                            else
                            {
                                WhereClause = "where BIRTH = @BIRTH";
                            }
                        }
                        if (model.memberIdnumber != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "MEMBERSHIP.PATID = @membId";
                            }
                            else
                            {
                                WhereClause = "where MEMBERSHIP.PATID = @membId";
                            }
                        }
                        #endregion endregion
                        cmd.CommandType = CommandType.Text;

                        string checkValidmember = 
                            $" declare @membid varchar(20) \n " +
                            $" set @membid = @membNum \n" +
                            $" SELECT * \n" +
                          //$" FROM MEMBERSHIP INNER JOIN\r\n" + // Jaco comment out 2023-09-20 Databases hardcoded in MEMBERSHIP view
                            $" FROM (" +
                            $"           SELECT        mm.MEMBID, mm.RLSHIP, mm.LASTNM, mm.MI, mm.FIRSTNM, mm.EMAIL, mh.HPFROMDT, mh.HPCODE, ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, mh.OPFROMDT, CASE WHEN isnull(OPTHRUDT, GETDATE())  \n " +
                            $"                         >= GETDATE() THEN NULL ELSE OPTHRUDT END AS OPTHRUDT, mm.BIRTH, mm.SUBSSN, ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1)  \n " +
                            $"                         AS DEPENDANT, mh.OPT AS HPName, {DRCDatabase}..HP_CONTRACTS.LOBCODE, mh.TERM_CODE, CASE WHEN mh.CURRHIST = 'c' AND isnull(OPTHRUDT, GETDATE()) >= GETDATE() \n " +
                            $"                          THEN 'ACTIVE' ELSE 'TERMINATED' END AS MemberStatus, mm.PATID, mm.BROKERID, memberprefix AS LenOfPrefix, 0 AS PosOdDash \n " +
                            $"           FROM {DRCDatabase}..MEMB_MASTERS AS mm INNER JOIN \n " +
                            $"                {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID INNER JOIN \n " +
                            $"                {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = {DRCDatabase}..HP_CONTRACTS.HPCODE LEFT OUTER JOIN \n " +
                            $"                  (SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix \n " +
                            $"                   FROM            {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n " +
                            $"                             {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n " +
                            $"                   WHERE({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') \n " +
                            $"                  ) AS z ON {DRCDatabase}..HP_CONTRACTS.HPCODE = z.ezHpCode LEFT OUTER JOIN \n " +
                            $"                {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName LEFT OUTER JOIN \n " +
                            $"                {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                            $"           WHERE(mh.CURRHIST = 'c') \n " +                          
                            $"       ) AS MEMBERSHIP INNER JOIN\r\n" +
                            $" (SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix\r\n" +
                            $"  FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN\r\n" +
                            $"       {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode) AS op ON op.ezHpCode = MEMBERSHIP.HPCODE LEFT OUTER JOIN\r\n" +
                            $"       (SELECT      MEMBID,  SUM(cd.QTY) AS CONSULTATIONS, HPCODE \r\n" +
                            $"        FROM {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \r\n" +
                            $"             {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO INNER JOIN \r\n" +
                            $"             {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \r\n" +
                            $"        WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \r\n" +
                          //$"            AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \r\n" + //Jaco 2024-07-12
                            $"            AND 1 = ( \r\n" +
                            $"                              CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                            $"                                   WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                            $"                                   ELSE 0 \r\n" +
                            $"                               END \r\n" +
                            $"					   ) \r\n" +
                            $"            AND(cm.SPEC IN('14', '15')) \r\n" +
                            $"            AND cm.CONTRACT = \r\n" +
                          //$"            CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN '2' ELSE cm.CONTRACT END \r\n" + // Jaco 2024-07-25
                            $"            CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE cm.CONTRACT END \r\n" +
                            $"            AND(cd.NET <> 0) \r\n" +
                          //$"            AND cd.PHCODE = CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN 'P' ELSE cd.PHCODE END \r\n" + // Jaco 2024-07-25
                            $"            AND cd.PHCODE = 'P' \r\n" +
                            $"            GROUP BY MEMBID, HPCODE)  \r\n" +
                            $"        ) as GP_Consultations ON MEMBERSHIP.MEMBID = GP_Consultations.MEMBID\r\n" +
                            WhereClause;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = checkValidmember;
                        memberDetails.Load(cmd.ExecuteReader());
                        int isMainmeber = memberDetails.Select("RLSHIP = 1").Length;
                        //Object membCount = cmd.ExecuteScalar();

                        if (isMainmeber != 0)
                        {
                            registrationResults.isRegistered = Convert.ToBoolean(isMainmeber);
                        }
                        else
                        {
                            registrationResults.isRegistered = false;
                            registrationResults.RegistrationMessage = "Please Register With The Main Member's Details";
                        }

                        if (registrationResults.isRegistered == true)
                        {
                            model.createDate = DateTime.Now;
                            model.changeDate = DateTime.Now;
                            model.userType = 4;
                            model.createBy = "System Registration";
                            model.changeBy = "System Registration";


                            string registerUser = "INSERT INTO Users([username], [password], [name], [userType], [createBy], [createDate], [changeBy], [changeDate],[provId], [bureauId], [lobcode], [specifications], [client], [memberNumber], [memberIdnumber], [hpCode])\r\n" +
                                                  "VALUES (@username, @password, @name, @userType, @createBy,@createDate, @changeBy, @changeDate, null, null, null, null, null, @membNum, @membId, @hpCode);\r\n";
                            cmd.CommandText = registerUser;
                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {
                                    registrationResults.isRegistered = false;
                                    registrationResults.RegistrationMessage = "An Error Occurred During The Registration Process";
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.Contains("Violation of PRIMARY KEY constraint"))
                                {
                                    registrationResults.isRegistered = false;
                                    registrationResults.RegistrationMessage = "This email address (" + model.username + ") has already been registered";
                                }
                                else
                                {
                                    LogError(ex.Message, ex.StackTrace);
                                }
                            }
                            if (registrationResults.isRegistered == true)
                            {
                                registrationResults.isRegistered = true;
                                registrationResults.RegistrationMessage = "Successful Registration";
                                registrationResults.RegistrationUser = memberFullname;
                                Utillity.Sendnotification(emailSubject, EmailBody, model.username);
                            }
                        }
                        else
                        {
                            if (registrationResults.RegistrationMessage.Length == 0)
                            {
                                registrationResults.isRegistered = false;
                                registrationResults.RegistrationMessage = "No Member Was Found With The Given Detail Kindly Check Entered Information (please use your member number provided on your medical aid card)";
                            }
                        }
                    }
                    Sqlcon.Close();
                }
            }
            return registrationResults;
        }

        /*Registration For brokers*/
        [HttpPost]
        [Route("registerbroker")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public BrokerRegistrationResultsModel Registerbroker([FromBody] BrokerRegistrationModel model)
        {
            BrokerRegistrationResultsModel registrationResults = new BrokerRegistrationResultsModel();
            SettingsModel returnedRow = Utillity.GetSpecificSetting("memReg", _portalConnectionString);
            string EmailBody = returnedRow.description.Replace("<email>", model.email);
            string emailSubject = "Broker - Registration";

            //OperationLog log = new OperationLog();
            //log.BrokerRegistration("Registration",  model.email, model.brokerId, model.hpCode, _portalConnection);

            if (model.hpCode == "000")
            {
                model.hpCode = "";
            }

            if (model.name == "")
            {
                model.name = Utillity.brokerName(model.brokerId, model.hpCode, _drcConnectionString);

                if (model.name == "")
                {
                    registrationResults.isRegistered = false;
                    registrationResults.RegistrationMessage = "Broker identification failed";
                }
            }
            if (model.name != "")
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string checkValidbroker = "select count(*) from BROKER_MASTERS\r\n" +
                                                  "where HPCODE = @hpCode and BROKERID = @brokerId\r\n";

                        cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = model.hpCode;
                        cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = model.brokerId;
                        cmd.Parameters.Add("@email", SqlDbType.VarChar, 20).Value = model.email;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.password;
                        cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = model.name;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = checkValidbroker;
                        object isValid = cmd.ExecuteScalar();

                        if (isValid != null)
                        {
                            registrationResults.isRegistered = Convert.ToBoolean(isValid);
                        }
                        if (registrationResults.isRegistered == false)
                        {
                            registrationResults.RegistrationMessage = "Registration Failed";
                        }
                        else
                        {
                            using (SqlConnection SqlconInsert = new SqlConnection(_portalConnectionString))
                            {
                                if (SqlconInsert.State != ConnectionState.Open)
                                {
                                    SqlconInsert.Open();
                                }
                                cmd.CommandType = CommandType.Text;
                                string sqlInsert = "insert into Users\r\n" +
                                                    "(username,[password],[name],userType,createBy,createDate,changeBy,changeDate,brokerId,hpCode)\r\n" +
                                                    "  values(@email,@password,@name,5,'System Registration',GETDATE(),'System Registration',GETDATE(),@brokerId,@hpCode)\r\n";
                                cmd.Connection = SqlconInsert;
                                cmd.CommandText = sqlInsert;

                                try
                                {
                                    int rowsAffected = cmd.ExecuteNonQuery();
                                    if (rowsAffected != 1)
                                    {
                                        //throw new Exception();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("Violation of PRIMARY KEY constraint"))
                                    {
                                        registrationResults.isRegistered = false;
                                        registrationResults.RegistrationMessage = "This email address (" + model.email + ") has already been registered";
                                    }
                                    else
                                    {
                                        LogError(ex.Message, ex.StackTrace);
                                    }
                                }
                                Sqlcon.Close();
                            }
                            if (registrationResults.isRegistered == true)
                            {
                                //Utillity.Sendnotification(emailSubject, EmailBody, "montie2206@gmail.com"); Jaco comment out 2023-09-14
                                Utillity.Sendnotification(emailSubject, EmailBody, "jaco@pamc.co.za");
                                registrationResults.RegistrationMessage = "Thank you for Registering with the Portal, a email will be sent to you confriming successful registration.";
                            }
                        }
                    }
                    Sqlcon.Close();
                }
            }


            return registrationResults;

        }

        /*Loading all registered users*/
        [HttpGet]
        [Route("LoadUsers")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<RegisteredUsersModel> LoadUsers()
        {
            DataTable dt = new DataTable();
            List<RegisteredUsersModel> ListOfUsers = new List<RegisteredUsersModel>();
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "";
                        cmd.Connection = Sqlcon;

                        sqlTextToExecute = "select [name],username,[password] AS password from Users\r\n" +
                     "order by createDate desc\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());
                        foreach (DataRow dr in dt.Rows)
                        {
                            RegisteredUsersModel user = new RegisteredUsersModel();
                            user.name = dr["name"].ToString();
                            user.username = dr["username"].ToString();
                            //user.password = dr["password"].ToString();
                            user.password = dr["password"].ToString();
                            ListOfUsers.Add(user);
                        }
                    }
                    Sqlcon.Close();
                }

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return ListOfUsers;
        }

        [HttpPost]
        [Route("ResetPassword")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]

        public LoginDetailsModel ResetPassword([FromBody] LoginDetailsModel model)
        {
            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    model.changeDate = DateTime.Now;
                    cmd.CommandType = CommandType.Text;

                    string sqlTextToExecute = "update Users set [password] = '" + model.newPassword + "', changeBy = '" + model.changeBy + "', changeDate ='" + model.changeDate + "' where username = '" + model.username + "'\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    try
                    {

                        int rowsAffected = cmd.ExecuteNonQuery();
                        if (rowsAffected != 1)
                        {
                            //throw new Exception();
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError(ex.Message, ex.StackTrace);
                        var msg = ex.Message;
                        throw new Exception();

                    }

                }
                Sqlcon.Close();
            }
            return model;
        }

        [HttpPost]
        [Route("RestoreClaim")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel RestoreClaim([FromBody] objectlog data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    RecordType0 rec0 = new RecordType0();
                    rec0.TYPE = 0;
                    rec0.CONTROL = "BATCH";
                    rec0.FILLER1 = "";
                    rec0.FILLER2 = "";
                    rec0.FILLER3 = "";
                    rec0.FILLER4 = "";
                    rec0.PROVIDER = "";
                    rec0.CLRHOUSE = rec0.PROVIDER;
                    rec0.DATECREATED = DateTime.Today.ToShortDateString();
                    cmd.CommandText = $"INSERT INTO Record0 (TYPE,CONTROL, FILLER1, PROVIDER, FILLER2, CLRHOUSE, FILLER3, DATECREATED, FILEER4) OUTPUT Inserted.FileID " +
                    $"VALUES ('{rec0.TYPE}', '{rec0.CONTROL}', '{rec0.FILLER1}', '{rec0.PROVIDER}', '{rec0.FILLER2}', '{rec0.CLRHOUSE}', '{rec0.FILLER3}', '{rec0.DATECREATED}', '{rec0.FILLER4}')";

                    int rec0id = 0;

                    rec0id = (int)cmd.ExecuteScalar();

                    var defaultDate = Convert.ToDateTime(data.header.DATERECD);
                    string recDate = defaultDate.ToString("yyyy/MM/dd");
                    data.header.EXT_SWITCH = "EZWEB";
                    data.header.Rec0ID = rec0id;
                    cmd.CommandText = $"INSERT INTO Record1 (TYPE, PHCODE, PROVCLAIM, EZCAPUATH, EZCAPPROV, EZCAPMEMB, PLACE, OUTCOME, EDIREF, CASENUM, UNITS, CLAIMTYPE, REFPROVID," +
                                      $" VENDORID,DATERECD, BATCH_NO, [EXT SWITCH], FileID,CREATEBY,CREATEDATE) OUTPUT Inserted.Rec1ID VALUES " +
                                      $"('{data.header.TYPE}', '{data.header.PHCODE}', '{data.header.PROVCLAIM}', '{data.header.EZCAPAUTH}', '{data.header.EZCAPPROV}'," +
                                      $" '{data.header.EZCAPMEMB}', '{data.header.PLACE}', '', '', '', '', '{data.header.CLAIMTYPE}', " +
                                      $"'{data.header.REFPROVID}','{data.header.VENDORID}' ,'{recDate}', '', '{ data.header.EXT_SWITCH}', '{data.header.Rec0ID}', '{data.header.CREATEBY}','{DateTime.Now.ToString("yyyy/MM/dd")}')";
                    int rec1id = 0;

                    rec1id = (int)cmd.ExecuteScalar();

                    foreach (RecordType2 item in data.detail)
                    {
                        item.Rec1ID = rec1id;
                    }
                    foreach (RecordType5 item in data.diags)
                    {
                        item.FieldID = rec1id;
                    }
                    int[] ids = new int[data.detail.Length];
                    int count = 0;
                    decimal billed = 0;
                    foreach (var detailLine in data.detail)
                    {
                        var lineID = detailLine.Rec1ID;
                        var fromDate = Convert.ToDateTime(detailLine.FROMDATE).ToString("yyyy/MM/dd");
                        var toDate = Convert.ToDateTime(detailLine.TODATE).ToString("yyyy/MM/dd"); ;
                        cmd.CommandText = $"INSERT INTO Record2 (TYPE, PHCODE, FROMDATE, TODATE, PROCCODE, CURR_MODCODE, CURR_MODAMOUNT, MODCODE, MODAMOUNT," +
                                       $"MODCODE2, MODAMOUNT2, MODCODE3, MODAMOUNT3, MODCODE4, MODAMOUNT4, QTY, BILLED, DIAGREF, BUNDLER, BUNDLERTYP, COPAY, MANDISCOUNT, TRANSACT_NO, PROCCODE_DESC, " +
                                       $"CLINICALCODE1, CLINICALCODE2, CLINICALCODE3, CLINICALCODE4, CLINICALCODE5, CLINICALCODE6, CLINICALCODE7, CLINICALCODE8, " +
                                       $"MEMO1, MEMO2, MEMO3, MEMO4, MEMO5, MEMO6, MEMO7, MEMO8, MEMO9,MEMO10, Rec1ID) " +
                                       $"OUTPUT	INSERTED.Rec2ID\r\n" +
                                       $"VALUES ('2', '{detailLine.PHCODE}',CAST('{fromDate}' AS date), CAST('{toDate}' AS date) , '{detailLine.PROCCODE}'" +
                                       $", '', '', '{detailLine.MODCODE}', '', '{detailLine.MODCODE2}', '', '{detailLine.MODCODE3}', '', '{detailLine.MODCODE4}', '', '{detailLine.QTY}', '{detailLine.BILLED}', '{detailLine.DIAGREF}', '', '', '', '', ''," +
                                       $" '{detailLine.PROCCODE_DESC}', '{detailLine.CLINICALCODE1}', '{detailLine.CLINICALCODE2}', '{detailLine.CLINICALCODE3}', '{detailLine.CLINICALCODE4}', '{detailLine.CLINICALCODE5}', '{detailLine.CLINICALCODE6}', '{detailLine.CLINICALCODE7}', '{detailLine.CLINICALCODE8}'" +
                                       $", '', '', '', '', '', '', '', '', '', '', '{lineID}')";
                        billed = billed + Convert.ToDecimal(detailLine.BILLED);
                        ids[count] = (int)cmd.ExecuteScalar();
                        count++;

                    }
                    foreach (var line in data.diags)
                    {
                        var lineID = line.FieldID;
                        cmd.CommandText = $"INSERT INTO Record5 (TYPE, DIAGREF, DIAG, Rec1ID) VALUES ('5', '{line.DIAGREF}', '{line.DIAG}', '{lineID}')";
                        cmd.ExecuteNonQuery();
                    }

                    foreach (var item in data.notes)
                    {
                        item.Rec2ID = ids[(item.LINE - 1)];
                        var itemID = item.Rec2ID;
                        cmd.CommandText = $"INSERT INTO Record4 (TYPE, LINE, NOTE, Rec2ID) VALUES ('4', '{item.LINE}', '{item.NOTE}', '{itemID}')";
                        cmd.ExecuteNonQuery();
                    }
                    foreach (var item in data.rejects)
                    {
                        item.Rec2IDdb = ids[(Convert.ToInt32(item.LINE) - 1)];
                        var itemID = item.Rec2IDdb;
                        cmd.CommandText = $"INSERT INTO Record8 (TYPE, LINE, ADJCODE, ADJUST, COMMENT, Rec2ID) VALUES ('8', '{item.LINE}', '{item.ADJCODE}', '{item.ADJUST}', '{item.COMMENT}', '{itemID}')";
                        cmd.ExecuteNonQuery();
                    }

                    vr.message = rec0id.ToString() + "," + rec1id.ToString() + $",1 Claim with \n" + $"{data.detail.Length} detail line/s in total and \n"
                        + $"{data.diags.Length} diagnostic code/s saved \n" + $" with a total billed amount of R {billed} \n";
                    vr.valid = true;
                }

            }
            catch (Exception e)
            {

                var msg = e.Message;
            }



            return vr;
        }

        [HttpPost]
        [Route("ValidateUserName")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public bool ValidateUserName([FromBody]LoginDetailsModel model)
        {
            DataTable td = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            bool valid = false;
            try
            {
                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT * FROM USERS WHERE username = '{model.username}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    td.Load(dr);

                }

                if (td.Rows.Count > 0)
                {
                    valid = true;
                }
                else
                {
                    valid = false;
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return valid;
        }

        [HttpPost]
        [Route("GetObjectIds")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<string> GetObjectIds([FromBody]LoginDetailsModel model)
        {
            List<string> ids = new List<string>();

            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            string desc = "";

            try
            {
                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                cmd.CommandText = "SELECT OBJECTID, DESCR FROM USER_RIGHTS_DESCR";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow item in dt.Rows)
                {
                    desc = "";
                    desc = item["OBJECTID"].ToString().TrimEnd() + " - " + item["DESCR"].ToString().TrimEnd();
                    ids.Add(desc);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return ids;
        }

        [HttpPost]
        [Route("GetUserRights")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<UserRightsGridModel> GetUserRights([FromBody] LoginDetailsModel model)
        {
            List<UserRightsGridModel> list = new List<UserRightsGridModel>();
            string[] objectids;
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                cmd.CommandText = $"SELECT OBJECTID FROM USER_RIGHTS WHERE USERNAME = '{model.username}'";
                using (SqlDataReader drw = cmd.ExecuteReader())
                {
                    dtbl.Load(drw);
                }
                objectids = new string[dtbl.Rows.Count];
                int count = 0;
                foreach (DataRow rows in dtbl.Rows)
                {
                    objectids[count] = rows["OBJECTID"].ToString();
                    count++;
                }
                cmd.CommandText = "SELECT OBJECTID, DESCR FROM USER_RIGHTS_DESCR";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    UserRightsGridModel data = new UserRightsGridModel();

                    data.objectid = row["OBJECTID"].ToString();
                    data.description = row["DESCR"].ToString().TrimEnd();
                    data.enabled = objectids.Contains(data.objectid);

                    list.Add(data);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return list;
        }

        [HttpPost]
        [Route("AddRightsForUser")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public bool AddRightsForUser([FromBody] UserRightsGridModel model)
        {
            bool inserted = false;
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                cmd.CommandText = $"INSERT INTO USER_RIGHTS (USERNAME,OBJECTID,CREATEBY,CREATEDATE,LASTCAHNGEBY,LASTCHANGEDATE) " +
                    $"VALUES('{model.username}','{model.objectid}','ADMIN',CONVERT(VARCHAR(10),GETDATE(),126)," +
                    $"'ADMIN',CONVERT(VARCHAR(10),GETDATE(),126))";
                cmd.ExecuteNonQuery();
                inserted = true;
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return inserted;
        }

        [HttpPost]
        [Route("RemoveRightsForUser")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public bool RemoveRightsForUser([FromBody] UserRightsGridModel model)
        {
            bool inserted = false;
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                cmd.CommandText = $"DELETE FROM USER_RIGHTS WHERE USERNAME = '{model.username}' " +
                    $"AND OBJECTID = '{model.objectid}'";
                cmd.ExecuteNonQuery();
                inserted = true;
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return inserted;
        }

        [HttpPost]
        [Route("GetUserNames")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<string> GetUserNames([FromBody] UserRightsGridModel model)
        {
            List<string> users = new List<string>();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cn.ConnectionString = _portalConnectionString;

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT  username FROM Users WHERE userType = 3 OR userType = 1";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow name in dt.Rows)
                {
                    string username = "";
                    username = name["username"].ToString();
                    users.Add(username);

                }

                dt.Clear();
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return users;
        }

        /*Method used to log all errors that take place in this controller*/
        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "ADMIN";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\ADMIN\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }

    }
}
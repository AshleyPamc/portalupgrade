﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : Base.PAMCController
    {
        public AuthorizationController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }
                
        [HttpGet]
        [Route("GetOutOfNetGpVisitCount")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<int>> GetOutOfNetGpVisitCount(string subssn)
        {
            SqlCommand cmd = new SqlCommand("GetOutNetGpVisits", new SqlConnection(_drcConnectionString));
            try
            {                    
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@subssn", subssn.Trim()));
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                int count = Convert.ToInt32(cmd.ExecuteScalar().ToString());                  
                cmd.Connection.Close();               
                return Ok(count);
            }
            catch (Exception e)
            {
                cmd.Connection.Close();
                LogError(e.Message, e.StackTrace);

                ModelState.AddModelError("Exception", e.Message);
                return ValidationProblem(ModelState);
            }
        }

        /*Validates if a provider falls in the specs of the Auth Group Selected*/
        [HttpPost]
        [Route("ValidateProvWithSpec")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProvWithSpec([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@provid", data.provid));
                    cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));

                    string where = "";                    
                    if (data.spec.Length == 1)
                    {
                        data.spec = "0" + data.spec;
                        where = "'" + data.spec + "'";
                    }
                    else
                    {
                        where = "'" + data.spec + "'";
                    }
                    if (data.spec == "14")
                    {
                        where = "'" + data.spec + "','15'";
                    }
                    if (data.spec == "15")
                    {
                        where = "'" + data.spec + "','14'";
                    }

                    cmd.Parameters.Add(new SqlParameter("@spec", where));
                    cmd.Connection = cn;

                    // Jaco comment out 2024-09-04 this does not look at HP Excusions
                    //cmd.CommandText = $"SELECT m.CONTRACT, m.LASTNAME,i.SPECCODE FROM {DRCDatabase}.dbo.PROV_MASTERS m \n" +
                    //    $"INNER JOIN {DRCDatabase}.dbo.PROV_SPECINFO i ON i.PROVID = m.PROVID \n" +
                    //    $" WHERE m.PROVID = @provid AND i.SPECCODE IN ({where})";

                    // Jaco 2024-09-04 check if provider has special exclusions
                    cmd.CommandText =
                    " SELECT PM.[CONTRACT], PM.LASTNAME,PS.SPECCODE, PC.CLASS, isnull(PH.HPCODE, '') as CURRHPEXCLUSION  \n" +
                    " FROM PROV_MASTERS PM \n" +
                    "   INNER JOIN PROV_SPECINFO PS on(PS.PROVID = PM.PROVID AND PM.LASTNAME not like '%DUMMY_VAL%') \n" +
                    "   INNER JOIN PROV_CONTCLASS PC on(PS.PROVID = PC.PROVID AND GETDATE() BETWEEN  PC.FROMDATE AND ISNULL(PC.TERMDATE, '2099-12-31')) \n" +
                    "   LEFT JOIN PROV_HPEXLHIST PH on(PS.PROVID = PH.PROVID AND PH.HPCODE = @hpcode AND GETDATE() between DATEADD(DAY, -10, PH.FROMDATE) AND DATEADD(DAY, 1, ISNULL(PH.TODATE, '2099-12-31'))) \n" +
                   $" WHERE PM.PROVID = @provid AND SPECCODE in ({where}) ";

                    DataTable dt = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["CURRHPEXCLUSION"].ToString() != ""  // Current HP Exclusion exist
                                || (data.hpcode != "SIZW" && row["CLASS"].ToString() == "53"))  // Exclude SIZW class 53 contracted providers
                            {
                                row["CONTRACT"] = "1";
                            }

                            vr.message = row["LASTNAME"].ToString() + "," + row["CONTRACT"].ToString();
                            vr.valid = true;
                        }
                    }
                    else
                    {
                        vr.message = "";
                        vr.valid = false;
                    }
                }                
            }
            catch (Exception e)
            {                
                LogError(e.Message, e.StackTrace);
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateProv")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProv([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@provid", data.provid));

                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT m.CONTRACT,m.LASTNAME,i.SPECCODE FROM {DRCDatabase}.dbo.PROV_MASTERS m \n" +
                        $"INNER JOIN {DRCDatabase}.dbo.PROV_SPECINFO i ON i.PROVID = m.PROVID \n" +
                        $" WHERE m.PROVID = @provid ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);

                    }

                    if (dt.Rows.Count > 0)
                    {

                        cmd.CommandText = $"SELECT i.SpecCode FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail i \n" +

                                          $" WHERE i.MasterId= '{data.id}' ";
                        DataTable tbl = new DataTable();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            tbl.Load(dr);
                        }

                        List<string> specs = new List<string>();

                        foreach (DataRow row in tbl.Rows)
                        {
                            string spec = "";
                            spec = row["SpecCode"].ToString();
                            if (spec.Length < 2)
                            {
                                spec = "0" + spec;
                            }
                            specs.Add(spec);
                        }

                        vr.message = "";
                        vr.valid = false;
                        foreach (DataRow row in dt.Rows)
                        {
                            if (specs.Contains(row["SPECCODE"].ToString()))
                            {
                                vr.message = row["LASTNAME"].ToString() + "," + row["CONTRACT"].ToString();
                                vr.valid = true;
                            }

                        }
                    }
                    else
                    {
                        vr.message = "";
                        vr.valid = false;
                    }

                }

                // return vr;
            }
            catch (Exception e)
            {

                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }
            return vr;
        }

        /*Authorizations Affinity*/
        [HttpPost]
        [Route("SubmitAuthorizationHeader")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SubmitAuthorizationHeader([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    //DataTable dt = new DataTable();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@mId", data.id));
                    cmd.Parameters.Add(new SqlParameter("@hp", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@memb", data.membno.ToUpper()));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Auth_template_master (masterid,hpcode,membid,description,createDate,createBy,changedate,changeby) \n" +
                        $"OUTPUT INSERTED.id \n VALUES(@mId,@hp,@memb,@desc,@date,@user,@date,@user)";
                    vr.message = cmd.ExecuteScalar().ToString();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("SubmitAuthorization")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SubmitAuthorization([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;


                    string url = _AuthWebSvcConnection;
                    EndpointAddress address = new EndpointAddress(url);
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = 2147483647;
                    var time = new TimeSpan(1, 30, 0);
                    binding.CloseTimeout = time;
                    binding.OpenTimeout = time;
                    binding.ReceiveTimeout = time;
                    binding.SendTimeout = time;

                    AuthorizationService.AuthorizationsSoapClient au = new AuthorizationService.AuthorizationsSoapClient(binding, address);

                    var a = au.CreateAuthorizationAsync(data.membno, data.dep, data.provid, data.scvdate, data.proc, Convert.ToDouble(data.amount));

                    a.Wait();

                    vr.message = a.Result.Body.CreateAuthorizationResult;

                    cmd.Parameters.Add(new SqlParameter("@hp", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@memb", data.membno));
                    cmd.Parameters.Add(new SqlParameter("@dep", data.dep));
                    cmd.Parameters.Add(new SqlParameter("@prov", data.provid));
                    cmd.Parameters.Add(new SqlParameter("@user", data.provid));
                    cmd.Parameters.Add(new SqlParameter("@proc", data.proc));
                    cmd.Parameters.Add(new SqlParameter("@amount", data.amount));
                    cmd.Parameters.Add(new SqlParameter("@svc", data.scvdate));
                    cmd.Parameters.Add(new SqlParameter("@ref", data.refno));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@auth", vr.message));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Client_Auth_Submissions (hpcode,BenSetId,BenSetDesc,membid,dep,provid,amount,ref,svcDate,proccode," +
                        $"authNo,CreateBy,CreateDate,ChangeBy,ChangeDate) \n" +
                        $"OUTPUT INSERTED.id \n " +
                        $"VALUES(@hp,@id,@desc,@memb,@dep,@prov,@amount,@ref,@svc,@proc,@auth,@user,@date,@user,@date)";

                    string id = cmd.ExecuteScalar().ToString();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("SubmitAuthorizationDetail")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AuthModel> SubmitAuthorizationDetail([FromBody] List<AuthModel> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();

                    cmd.Connection = cn;

                    foreach (AuthModel item in data)
                    {
                        cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Auth_template_detail (masterid,spec," +
                            $"createDate,createBy,changedate,changeBy) OUTPUT INSERTED.id VALUES ('{item.id}','{item.spec}',\n" +
                            $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.username}',\n" +
                            $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.username}')\n";

                        item.id = cmd.ExecuteScalar().ToString();
                    }
                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return data;
        }

        [HttpPost]
        [Route("SearchAuthorizationDetail")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AuthModel> SearchAuthorizationDetail([FromBody] AuthModel data)
        {
            List<AuthModel> list = new List<AuthModel>();
            ValidationResultsModel vr = new ValidationResultsModel();

            SqlConnection cn = new SqlConnection(_portalConnectionString);
            try
            {
                
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                DataTable dt = new DataTable();

                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpcode));
                cmd.Parameters.Add(new SqlParameter("@membNo", data.membno));

                // Jaco 2024-08-29 changed. problem: in api param, membno is passed without prefix, membid in Client_Auth_Submissions is saved with prefix
                //int index = data.membno.IndexOf("-");

                //string sql = " SELECT ca.ref,ca.BenSetDesc,ca.SpecCode,sp.DESCR,ca.proccode,ca.BenefitId,ca.authNo,ca.provid,ca.svcDate,ca.amount "
                //          +  " FROM Client_Auth_Submissions ca \n"
                //          + $" INNER JOIN  {DRCDatabase}.dbo.R_PROV_SPECCODES sp ON sp.CODE = ca.SpecCode \n"
                //          + $" LEFT JOIN(select top 1 ezhpcode, isnull(MemberPrefix, '') as MemberPrefix from {ReportingDatabase}..OxygenBenefitOptions where ezhpcode = @hpCode) O on(ca.hpcode = o.ezhpcode) \n"
                //          + $" WHERE (ca.membid + '-' + ca.dep) = isnull(O.MemberPrefix,'') + @membNo \n"
                //          +  " order by ca.svcDate desc ";
                                
                
                // Jaco 2024-11-19 if auth exist in DRC then use the DRC date, otherwise, use the Portal data
                string sql = $"   SELECT ca.ref,ca.BenSetDesc,ca.authNo,ca.SpecCode,sp1.DESCR,ca.proccode,ca.BenefitId,ca.provid,REPLACE(ca.svcDate, '/', '-') as svcDate,ca.amount,  \n" +
                              "                                           AM.REQSPEC, sp2.DESCR as DESCR_drc, AD.PROCCODE as PROCCODE_drc, AD.BENTYPE, AM.REQPROV, convert(date, AM.AUTHDATE) as AUTHDATE, AD.NET \n" +
                             $"   FROM {PamcPortalDatabase}..Client_Auth_Submissions ca \n" +
                              "          LEFT JOIN (select top 1 ezhpcode, isnull(MemberPrefix, '') as MemberPrefix  \n" +
                             $"                  from {ReportingDatabase}..OxygenBenefitOptions   \n" +
                              "                  where ezhpcode = @hpCode) O on (ca.hpcode = o.ezhpcode) \n" +
                             $"          LEFT JOIN {DRCDatabase}..AUTH_MASTERS AM on (ca.authNo = AM.AUTHNO) \n" +
                             $"          LEFT JOIN {DRCDatabase}..AUTH_DETAILS AD on (AM.AUTHNO = AD.AUTHNO and AD.TBLROWID = 1) \n" +
                             $"          LEFT JOIN {DRCDatabase}..PROV_SPECCODES sp1 ON sp1.CODE = ca.SpecCode \n" +
                             $"          LEFT JOIN {DRCDatabase}..PROV_SPECCODES sp2 ON sp2.CODE = AM.REQSPEC \n" +
                            //  "   WHERE (ca.membid + '-' + ca.dep) = isnull(O.MemberPrefix, '') + @membNo \n" + // Jaco 2024-12-31 Now getting full membid with prefix from frontend
                              " WHERE (ca.membid + '-' + ca.dep) = @membNo \n" +
                              "   ORDER by REPLACE(ca.svcDate, '/', '-') desc ";

                cmd.CommandText = sql;
                cmd.CommandTimeout = 120;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                cn.Close();

                foreach (DataRow row in dt.Rows)
                {
                    AuthModel a = new AuthModel();

                    if (row["authNo"].ToString().Trim() == "") // No auth in DRC yet, use data in portal table
                    {
                        a.spec = row["SpecCode"].ToString();
                        a.desc = row["DESCR"].ToString();
                        a.provid = row["provid"].ToString();
                        a.scvdate = row["svcDate"].ToString();
                        a.amount = row["amount"].ToString();
                        a.benDesc = row["BenSetDesc"].ToString();
                        a.authno = row["authNo"].ToString();
                        a.benType = row["BenefitId"].ToString();
                        a.procCode = row["proccode"].ToString();
                        a.refno = row["ref"].ToString();
                    }
                    else // Use the data from the auth created in DRC
                    {
                        a.spec = row["REQSPEC"].ToString();
                        a.desc = row["DESCR_drc"].ToString();
                        a.provid = row["REQPROV"].ToString();
                        a.scvdate = row["AUTHDATE"].ToString();
                        a.amount = row["NET"].ToString();
                        a.benDesc = row["BenSetDesc"].ToString();
                        a.authno = row["authNo"].ToString();
                        a.benType = row["BENTYPE"].ToString();
                        a.procCode = row["PROCCODE_drc"].ToString();
                        a.refno = row["ref"].ToString();
                    }
                        
                    list.Add(a);
                }
                
            }
            catch (Exception e)
            {
                cn.Close();
                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("UpdateAuthorizationDetail")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateAuthorizationDetail([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                                        
                    //DataTable dt = new DataTable();
                    //DataTable tbl = new DataTable();


                    // Bind first set of params to insert Template details record
                    //string refno = "";
                    //if (data.refno != null && data.refno != "") refno = data.refno;

                    // If there is an amount convert it to decimal
                    string amount = "";
                    if (data.amount != null && data.amount != "")
                    {
                        amount = data.amount.Trim();
                        
                        try 
                        {
                            Decimal d = Convert.ToDecimal(amount);                            
                            amount = d.ToString("F2");
                        }
                        catch
                        {
                            amount = "";
                        }                                                    
                    }

                    cmd.Parameters.Add(new SqlParameter("@masterId", data.id));
                    cmd.Parameters.Add(new SqlParameter("@spec", data.spec));
                    cmd.Parameters.Add(new SqlParameter("@provId", data.provid));
                    //cmd.Parameters.Add(new SqlParameter("@refno", refno));
                    cmd.Parameters.Add(new SqlParameter("@amount", amount));
                    cmd.Parameters.Add(new SqlParameter("@serviceDate", data.scvdate));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));
                    //cmd.Parameters.Add(new SqlParameter("@changeDate", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    //cmd.Parameters.Add(new SqlParameter("@changeBy", data.username));
                    cmd.Parameters.Add(new SqlParameter("@qty", data.qty));
                    cmd.Parameters.Add(new SqlParameter("@diag1", data.diag1));
                    cmd.Parameters.Add(new SqlParameter("@diag2", data.diag2));
                    cmd.Parameters.Add(new SqlParameter("@diag3", data.diag3));
                    cmd.Parameters.Add(new SqlParameter("@diag4", data.diag4));
                    cmd.Parameters.Add(new SqlParameter("@authRefNo", data.authRefNo));
                    cmd.Parameters.Add(new SqlParameter("@note", data.note));

                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Auth_template_detail " +
                                       " (masterid, spec, provid, amount, serviceDate, createDate, createBy, changedate, changeBy, qty, diag1, diag2, diag3, diag4, schemeAuthNo, note) " +
                                       " OUTPUT INSERTED.id VALUES " +
                                       " (@masterId, @spec, @provId, @amount, @serviceDate, @date, @user, @date, @user, @qty, @diag1, @diag2, @diag3, @diag4, @authRefNo, @note) ";

                    //cmd.ExecuteNonQuery();
                    //item.id = cmd.ExecuteScalar().ToString();
                    string detailrowid = cmd.ExecuteScalar().ToString();
                    int detailrowidI = Int32.Parse(detailrowid);

                    //string sql = "";
                    //if ((data.amount == null) || (data.amount == ""))
                    //{
                    //    sql = $"UPDATE {PamcPortalDatabase}.dbo.Auth_template_detail  SET provid = '{data.provid}', refno = '{data.refno}', amount = '{data.amount}', \n" +
                    //    $"servicedate = '{data.scvdate}', changedate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}',changeBy = '{data.username}' \n" +
                    //    $"WHERE id = '{data.id}'";
                    //    cmd.CommandText = sql;
                    //}
                    //else
                    //{
                    //    sql = $"UPDATE {PamcPortalDatabase}.dbo.Auth_template_detail  SET provid = '{data.provid}', refno = '{data.refno}', \n" +
                    //   $"servicedate = '{data.scvdate}', changedate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}',changeBy = '{data.username}' \n" +
                    //   $"WHERE id = '{data.id}'";
                    //    cmd.CommandText = sql;
                    //}

                    //dt = new DataTable();

                    //sql = $"SELECT masterid,spec FROM {PamcPortalDatabase}.dbo.Auth_template_detail WHERE id = '{data.id}'";     //bug jaco
                    //sql = $"SELECT masterid,spec FROM {PamcPortalDatabase}.dbo.Auth_template_detail WHERE masterid = '{data.id}'"; //unnecessary
                    //cmd.CommandText = sql;

                    //using (SqlDataReader dr = cmd.ExecuteReader())
                    //{
                    //    dt.Load(dr);
                    //}

                    // Declare vars

                    //string msId = dt.Rows[0][0].ToString();
                    string msBenId = "";                    
                    string hp = "";
                    string membid = "";
                    string dep = "";                    
                    string proc = "";
                    //string ben = data.benefitId.Substring(data.benefitId.IndexOf("-") + 1);
                    string ben = data.benType;
                    string benDesc = "";
                    //string spec = dt.Rows[0][1].ToString();
                    //string spec = data.spec;


                    //sql = $"SELECT hpcode,membid,masterid,description FROM {PamcPortalDatabase}.dbo.Auth_template_master WHERE id = '{msId}'";
                    //cmd.CommandText = sql;

                    // Get data from template master
                    cmd.CommandText = $"SELECT hpcode,membid,masterid,description FROM {PamcPortalDatabase}.dbo.Auth_template_master WHERE id = @masterId";
                    DataTable tbl = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }

                    foreach (DataRow row in tbl.Rows)
                    {
                        msBenId = row["masterid"].ToString();
                        hp = row["hpcode"].ToString();
                        membid = row["membid"].ToString();

                        int dashCount = membid.Count(x => x == '-');
                        if (dashCount == 1)
                        {
                            int index = membid.IndexOf("-");
                            dep = membid.Substring((index + 1));
                            membid = membid.Substring(0, (index));
                        }
                        else if (dashCount > 1)
                        {
                            string[] splitmemb = membid.Split("-");
                            membid = "";
                            for (int i = 0; i < splitmemb.Length; i++)
                            {
                                if (i < splitmemb.Length - 1)
                                {
                                    membid = membid.Trim() + "-" + splitmemb[i].Trim();
                                }
                                else
                                {
                                    dep = splitmemb[i].Trim();
                                }
                            }
                            membid = membid.Substring(1);
                            //membid = membid.Substring(0, (index));
                        }
                        benDesc = row["description"].ToString();
                    }


                    // Get Proc code and bentype
                    //sql = $"SELECT procCode,BenType FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE masterId = '{msId}' AND SpecCode = '{spec}'";
                    //cmd.CommandText = $"SELECT procCode,BenType FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE masterId = '{msBenId}' AND SpecCode = '{data.spec}'";
                    //tbl = new DataTable();
                    //using (SqlDataReader dr = cmd.ExecuteReader())
                    //{
                    //    tbl.Load(dr);
                    //}
                    //proc = tbl.Rows[0][0].ToString();
                    //ben = tbl.Rows[0][1].ToString();
                          
                    /*  
                      string url = _AuthWebSvcConnection;
                      EndpointAddress address = new EndpointAddress(url);
                      BasicHttpBinding binding = new BasicHttpBinding();
                      binding.MaxReceivedMessageSize = 2147483647;
                      var time = new TimeSpan(1, 30, 0);
                      binding.CloseTimeout = time;
                      binding.OpenTimeout = time;
                      binding.ReceiveTimeout = time;
                      binding.SendTimeout = time;

                      AuthorizationService.AuthorizationsSoapClient au = new AuthorizationService.AuthorizationsSoapClient(binding, address);

                      string am = "";
                      if (data.amount == "" || data.amount == null)
                      {
                          am = "0.00";
                          double test = Convert.ToDouble(am);
                      }
                      else
                      {
                          am = data.amount;
                      }

                      var a = au.CreateAuthorizationAsync(membid, dep, data.provid, data.scvdate, proc, Convert.ToDouble(am));
                      a.Wait();
                      vr.message = a.Result.Body.CreateAuthorizationResult;
                    */                    

                    // Bind more params to insert complete auth into Client_Auth_Submissions
                    cmd.Parameters.Add(new SqlParameter("@hp", hp));
                    cmd.Parameters.Add(new SqlParameter("@memb", membid));
                    //cmd.Parameters.Add(new SqlParameter("@spec", spec));
                    cmd.Parameters.Add(new SqlParameter("@dep", dep));
                    //cmd.Parameters.Add(new SqlParameter("@prov", data.provid));
                    //cmd.Parameters.Add(new SqlParameter("@user", data.username));
                    cmd.Parameters.Add(new SqlParameter("@proc", data.procCode));
                    cmd.Parameters.Add(new SqlParameter("@ben", ben));
                    cmd.Parameters.Add(new SqlParameter("@benId", msBenId));
                    cmd.Parameters.Add(new SqlParameter("@benDesc", benDesc));
                    cmd.Parameters.Add(new SqlParameter("@phCode", data.phCode));
                    cmd.Parameters.Add(new SqlParameter("@placeSvc", data.placeSvc));
                    cmd.Parameters.Add(new SqlParameter("@opt", data.opt));
                    cmd.Parameters.Add(new SqlParameter("@reject", data.reject));

                    // Get prov spec code from db (in case of incorrect prior validation)
                    DataTable dtSpec = new DataTable();
                    cmd.CommandText = $" SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO  WHERE PROVID = '{data.provid}' ";
                    dtSpec.Load(cmd.ExecuteReader());
                    string specCode = dtSpec.Rows[0]["SPECCODE"].ToString();
                    cmd.Parameters.Add(new SqlParameter("@dbSpec", specCode));

                    // cmd.CommandText = $"SELECT NET FROM {DRCDatabase}.dbo.AUTH_MASTERS WHERE AUTHNO = '{vr.message}'";
                    // dt = new DataTable();

                    // dt.Load(cmd.ExecuteReader());

                    //cmd.Parameters.Add(new SqlParameter("@amount", ""));

                    //// Set amount to '' always //comment by jaco
                    //cmd.CommandText = $" UPDATE {PamcPortalDatabase}.dbo.Auth_template_detail  SET amount = @amount, \n" +
                    //                  $" changedate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}',changeBy = '{data.username}' \n" +
                    //                  //$" WHERE id = '{data.id}'";
                    //                  $" WHERE id = '{detailrowid}'";

                    //cmd.ExecuteNonQuery();

                    //cmd.Parameters.Add(new SqlParameter("@svc", data.scvdate));
                    
                    //cmd.Parameters.Add(new SqlParameter("@auth", vr.message));
                    //cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.CommandText = 
                       $" INSERT INTO {PamcPortalDatabase}.dbo.Client_Auth_Submissions " +
                        " (hpcode,opt,BenSetId,BenSetDesc,SpecCode,membid,dep,provid,amount,svcDate,proccode,phcode,BenefitId,placeSvc,authNo,FileOrWeb,CreateBy,CreateDate,ChangeBy,ChangeDate, qty, diag1, diag2, diag3, diag4, schemeAuthNo, note, reject) \n" +
                        " OUTPUT INSERTED.id VALUES" +
                        " (@hp,@opt,@benId,@benDesc,@dbSpec,@memb,@dep,@provId,@amount,@serviceDate,@proc,@phCode,@ben,@placeSvc,NULL,'WEB',@user,@date,@user,@date, @qty, @diag1, @diag2, @diag3, @diag4, @authRefNo, @note, @reject)";

                    string id = cmd.ExecuteScalar().ToString();                    
                    string s = $"#PAMC{id}{DateTime.Now.ToString("yyyyMMddHHmmss")}";

                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Client_Auth_Submissions SET ref = '{s}' WHERE id = '{id}'";
                    cmd.ExecuteNonQuery();
                                        
                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Auth_template_detail SET refno = {Int32.Parse(id)} WHERE id = {detailrowidI}";
                    cmd.ExecuteNonQuery();

                    vr.message = $"{s}";

                    // Send confirmation email
                    string AltEmail = "";
                    cmd.CommandText = $"SELECT AltEmail From {PamcPortalDatabase}.dbo.Users where Username = @user";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    AltEmail = dt.Rows[0]["AltEmail"].ToString();
                    string msg =    $"Good day, \r\n\r\n" +
                                    $"The following Auth was captured via the PAMC portal:\r\n\r\n" +
                                    $"MEMBID: {membid}-{dep}\r\n" +
                                    $"PROVID: {data.provid}\r\n" +
                                    $"SERVICE DATE: {data.scvdate}\r\n" +
                                    $"REF: {s}\r\n\r\n" +
                                    $"Regards\r\n" +
                                    $"PAMC TEAM";
                    //string url = _EmailWebSvcConnection;
                    //EndpointAddress address = new EndpointAddress(url);
                    //BasicHttpBinding binding = new BasicHttpBinding();
                    //binding.MaxReceivedMessageSize = 2147483647;
                    //var time = new TimeSpan(0, 30, 0);
                    //binding.CloseTimeout = time;
                    //binding.OpenTimeout = time;
                    //binding.ReceiveTimeout = time;
                    //binding.SendTimeout = time;
                    //EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                    //testEmail.EmailServiceSoapClient emialclient = new testEmail.EmailServiceSoapClient(binding, address);

                    //if (_isTest)
                    //{
                    //    var c = client.SendEmailAsync(msg, "PORTAL TEST: PAMC PORTAL AUTH SUBMISSION", _testEmails);
                    //    c.Wait();
                    //}                    
                    //else
                    //{
                    //    var c = client.SendEmailAsync(msg, "PAMC PORTAL AUTH SUBMISSION", $"{data.username};{AltEmail}");
                    //    c.Wait();
                    //}

                    // Get mailserver to send from
                    cmd = new SqlCommand();
                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = dt.Rows[0]["SERVERADDRESS"].ToString();
                    string username = dt.Rows[0]["USERNAME"].ToString();
                    string password = dt.Rows[0]["PASSWORD"].ToString();
                    string from = dt.Rows[0]["MAILFROM"].ToString();
                    string port = dt.Rows[0]["OUTGOINGPORT"].ToString();

                    List<FileInfo> attachments = new List<FileInfo>();

                    bool emailSuccess = false;

                    if (_isTest)
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PORTAL TEST: PAMC PORTAL AUTH SUBMISSION", msg, _testEmails, attachments);
                    }
                    else if (hp == "AUL")
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PAMC PORTAL AUTH SUBMISSION", msg, $"{data.username};Cecilia@PAMC.CO.ZA;Annelien@PAMC.CO.ZA;{AltEmail}", attachments);
                    }
                    else
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PAMC PORTAL AUTH SUBMISSION", msg, $"{data.username};{AltEmail}", attachments);
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return vr;
        }

        /*Provider Auth Files*/
        [HttpPost, DisableRequestSizeLimit]
        [Route("SendAuthFiles")]
        public ValidationResultsModel SendAuthFiles()
        {
            //const string Value = "Upload Successful.";
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();
            DataTable tbl = new DataTable();
            DataTable t = new DataTable();
            try
            {
                string date = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");

                var file = Request.Form.Files;
                foreach (var item in file)
                {
                    using (var ms = new MemoryStream())
                    {
                        item.CopyTo(ms);
                        byte[] filebytes = ms.ToArray();
                        using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                        {
                            if (cn.State != ConnectionState.Open)
                            {
                                cn.Open();
                            }
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = cn;

                            cmd.Parameters.Add(new SqlParameter("@name", item.FileName));
                            string[] extention = new string[item.FileName.Split('.').Length];
                            extention = item.FileName.Split('.');
                            cmd.Parameters.Add(new SqlParameter("@ext", "." + extention[(extention.Length - 1)]));
                            cmd.Parameters.Add(new SqlParameter("@size", item.Length));
                            cmd.Parameters.Add("@array", SqlDbType.VarBinary, filebytes.Length).Value = filebytes;
                            cmd.Parameters.Add(new SqlParameter("@id", _authId));
                            cmd.CommandText = $"INSERT INTO AuthDetailFile (FileName,Extention,FileSize,ByteArray,CreateDate,CreateBy," +
                                $"LastChangedBy,LastCHangeDate, HeadID) VALUES (@name,@ext,@size,@array,'{date}',999," +
                                $"999,'{date}',@id)";

                            cmd.ExecuteNonQuery();
                            cn.Close();
                        }
                    }
                }

                string provid = "";
                string membid = "";
                string username = "";
                string hp = "";
                string submit = "";
                // string mails = "";
                string text = "";
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM AuthsHeaderFile WHERE HeadID = {_authId}";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        provid = row["ProvId"].ToString();
                        membid = row["MembId"].ToString();
                        username = row["Username"].ToString();
                        hp = row["HPCode"].ToString();
                        submit = row["CreateDate"].ToString();
                        text = row["Text"].ToString();
                    }

                    foreach (var item in file)
                    {
                        string[] extention = new string[item.FileName.Split('.').Length];
                        extention = item.FileName.Split('.');
                        cmd.CommandText = $"INSERT INTO AUTH_UPLOAD_LOG (FILENAME,FILESIZE,EXTENTION,PROVID,SUBSSN," +
                            $"USERNAME,CREATEDATE,CREATEBY,LASTCHANGEBY,LASTCHANGEDATE) " +
                            $"VALUES ('{item.Name}','{item.Length}','{extention[(extention.Length - 1)]}','{provid}'," +
                            $"'{membid}','{username}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}', " +
                            $"999,999,'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}')";
                        cmd.ExecuteNonQuery();
                    }
                }

                //string url = _EmailWebSvcConnection;

                //EndpointAddress address = new EndpointAddress(url);
                //BasicHttpBinding binding = new BasicHttpBinding();
                //binding.MaxReceivedMessageSize = 2147483647;
                //var time = new TimeSpan(0, 30, 0);
                //binding.CloseTimeout = time;
                //binding.OpenTimeout = time;
                //binding.ReceiveTimeout = time;
                //binding.SendTimeout = time;

                //EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);

                ////testEmail.EmailServiceSoapClient emialclient = new testEmail.EmailServiceSoapClient(binding, address);

                int id = Convert.ToInt32(_authId);
                //var c = client.SendAuthorizationAsync(id);

                //c.Wait();

                vr.valid = SendAuthorizationAttachments(id);

            }
            catch (System.Exception ex)
            {
                vr.valid = false;
                vr.message = "Could not send auth files.\r\n\r\n" + ex.Message;
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }
            return vr;
        }

        public bool SendAuthorizationAttachments(int headerid)
        {
            bool success = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    DataTable tbl = new DataTable();
                    DataTable dtbl = new DataTable();
                    DataTable data = new DataTable();
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();

                    }

                    cmd.Connection = cn;

                    int _authId = 0;
                    _authId = headerid;
                    string username = "";
                    string provid = "";
                    string family = "";
                    string hp = "";
                    int filecount = 0;
                    string submit = "";
                    string[] emails;
                    string text = "";
                    string mails = "";

                    DataTable datatbl = new DataTable();
                    SqlCommand cmd1 = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    datatbl.Load(cmd.ExecuteReader());

                    string server = "";
                    string usernameL = "";
                    string password = "";
                    string from = "";
                    string port = "";

                    foreach (DataRow row in datatbl.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        usernameL = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();
                    }

                    SmtpClient client = new SmtpClient(server);
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(usernameL, password);
                    client.Port = Convert.ToInt32(port);
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(from);

                    cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}..AuthsHeaderFile WHERE HeadID = " + _authId.ToString() + "";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        provid = row["ProvId"].ToString();
                        family = row["MembId"].ToString();
                        username = row["Username"].ToString();
                        hp = row["HPCode"].ToString();
                        submit = row["CreateDate"].ToString();
                        text = row["Text"].ToString();
                    }

                    cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}..Auth_Email_Log WHERE HPCODE = '" + hp + "' AND DATESENT = '" + DateTime.Now.ToString("yyyy/MM/dd") + "' ORDER BY COUNTER ASC";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }
                    int counter = 0;
                    string to = "";
                    if (tbl.Rows.Count > 0)
                    {
                        emails = new string[1];
                        emails[0] = tbl.Rows[0]["EMAIL"].ToString();
                        to = tbl.Rows[0]["EMAIL"].ToString();
                        mail.To.Add(emails[0]);
                        counter = Convert.ToInt32(tbl.Rows[0]["COUNTER"].ToString()) + 1;
                        cmd.CommandText = $"UPDATE {PamcPortalDatabase}..Auth_Email_Log SET COUNTER = " + counter.ToString() + " WHERE HPCODE = '" + hp + "' AND EMAIL = '" + emails[0] + "' AND DATESENT = '" + DateTime.Now.ToString("yyyy/MM/dd") + "'";
                        cmd.ExecuteNonQuery();

                    }
                    else
                    {
                        cmd.CommandText = $"SELECT EMAILADDRESSES FROM {PamcPortalDatabase}..Internal_Auth_Distribution WHERE HPCODE = '" + hp + "'";
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dtbl.Load(dr);
                        }
                        foreach (DataRow row in dtbl.Rows)
                        {
                            mails = row["EMAILADDRESSES"].ToString();

                        }

                        emails = new string[mails.Split(';').Length];
                        emails = mails.Split(';');
                        foreach (string email in emails)
                        {
                            cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}..Auth_Email_Log (HPCODE,EMAIL,DATESENT,COUNTER,CREATEDATE,CREATEBY,LASTCHANGEDATE,LASTCHANGEBY) VALUES " +
                                "('" + hp + "','" + email + "','" + DateTime.Now.ToString("yyyy/MM/dd") + "',0,'" + DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ") + "'," +
                                "999,'" + DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ") + "',999)";
                            cmd.ExecuteNonQuery();
                        }

                        cmd.CommandText = $"UPDATE {PamcPortalDatabase}..Auth_Email_Log SET COUNTER = 1 WHERE HPCODE = '" + hp + "' AND EMAIL = '" + emails[0] + "' " +
                            "AND DATESENT = '" + DateTime.Now.ToString("yyyy/MM/dd") + "'";
                        cmd.ExecuteNonQuery();
                        to = emails[0];

                    }

                    cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}..AuthDetailFile WHERE HeadID =" + _authId.ToString();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        data.Load(dr);
                    }

                    //DirectoryInfo d = new DirectoryInfo(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "files\\");
                    //DirectoryInfo d = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "wwwroot\\Auth_Attachments\\");

                    string folderName = DateTime.Now.ToString("yyyyMMddHHmmssfff"); // Jaco 2022-04-14
                    folderName += "_provId_" + provid;

                    var root = _env.WebRootPath;
                    DirectoryInfo newFolder = new DirectoryInfo($"{root}\\Auth_Attachments\\{folderName}");
                    newFolder.Create();

                    FileInfo fi = new FileInfo($"{root}\\Auth_Attachments\\{folderName}");

                    // Delete files already in the folder
                    //foreach (FileInfo item in newFolder.GetFiles())
                    //{
                    //    FileInfo f = new FileInfo(item.FullName);
                    //    f.Delete();
                    //}

                    List<FileInfo> ff = new List<FileInfo>();

                    foreach (DataRow row in data.Rows)
                    {
                        //using (var ms = new MemoryStream())
                        //{
                            var filebytes = (byte[])row["ByteArray"];
                            var FileName = row["FileName"].ToString();
                            FileStream fs = new FileStream(fi.FullName + "\\" + FileName, FileMode.Create);
                            MemoryStream m = new MemoryStream(filebytes);
                            m.WriteTo(fs);

                            FileInfo files = new FileInfo(fi.FullName + "\\" + FileName);
                            if (files.Exists)
                            {
                                ff.Add(files);
                            }
                            fs.Flush();
                            fs.Dispose();
                            fs.Close();
                            ////ms.Flush();
                            ////ms.Dispose();
                            ////ms.Close();
                            m.Flush();
                            m.Dispose();
                            m.Close();
                        //}

                    }
                    filecount = data.Rows.Count;

                    string subject = "Authorization Submission";
                    string body = "Good day, \n\n" +
                        "An Authorization was uploaded via the provider portal on " + submit + ".\n\n" +
                        "Details of Auth: \n" +
                        "REFERENCE: #" + _authId.ToString() + " \n" +
                        "USERNAME: " + username + " \n" +
                        "PROVID: " + provid + " \n" +
                        "FAMILY NO: " + family.Remove(0, 1) + " \n" +
                        "HPCODE: " + hp + " \n" +
                        "NO. OF FILES: " + filecount.ToString() + "\n\n" +
                        "Additional Notes: \n\n " + text + "\n\n\n" +
                        "Regards.";

                    //client.Send(mail);

                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    server = "";
                    username = "";
                    password = "";
                    from = "";
                    port = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();

                    }

                    if (_isTest)
                    {
                        success = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PORTAL TEST: " + subject, body, _testEmails, ff);
                    }
                    else
                    {
                        success = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, to, ff);
                    }

                    fi = null;
                    ff.Clear();
                    ff = null;
                    newFolder = null;

                    // Do not delete files for now, maybe in future if it makes server too full
                    //if (success)
                    //{ 
                    //    Directory.Delete(deletepath, true);                       
                    //}
                }
            }
            catch (Exception e)
            {
                //success = false;
                //var msg = e.Message;
                throw;
            }

            return success;
        }

        [HttpPost]
        [Route("ValidateUser")]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel ValidateUser([FromBody] AuthFiles data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    // Jaco comment out 2023-09-28 - Databases hardcoded in GetMemberPrefix function
                    //cmd.CommandText = $"DECLARE @membid  VARCHAR(20)" +
                    //                  $"DECLARE @prefix  VARCHAR(20)" +
                    //                  $"SET @membid = '{data.membid}'" +
                    //                  $"SET @prefix = {PamcPortalDatabase}.dbo.GetMemberPrefix('{data.hpcode}')" +
                    //                  $"SET @membid = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix, PamcPortal.dbo.RemoveDependantDash(@membid))" +
                    //                  $"SELECT * FROM MEMB_MASTERS WHERE SUBSSN = @membid and HPCODE = '{data.hpcode}'";

                    // Jaco comment out 2023-09-28
                    cmd.CommandText = $" DECLARE @membid  VARCHAR(20) \n" + //Tested with Dentist login
                                      $" DECLARE @prefix  VARCHAR(20) \n" +
                                      $" SET @membid = '{data.membid}' \n" +
                                      
                                      $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                        $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                        $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                        $" where OxygenBenefitOptions.ezHpCode = '{data.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross' \n " +

                                      $" SET @membid = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membid)) \n" +
                                      $" SELECT * FROM {DRCDatabase}..MEMB_MASTERS WHERE SUBSSN = @membid and HPCODE = '{data.hpcode}' \n";
                    
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }

                    if (vr.valid)
                    {
                        string date = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");
                        cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                        cmd.Parameters.Add(new SqlParameter("@user", data.username));
                        cmd.Parameters.Add(new SqlParameter("@provid", data.provid));
                        if (data.AddText == null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@Text", ""));
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@Text", data.AddText.Replace("'", "")));
                        }
                        cmd.CommandText = $"DECLARE @membid  VARCHAR(20) \n" + //Tested with dentist login
                                      $"DECLARE @prefix  VARCHAR(20) \n" +
                                      $"SET @membid = '{data.membid}' \n" +
                                      //$"SET @prefix = {PamcPortalDatabase}.dbo.GetMemberPrefix('{data.hpcode}') \n" + // Jaco comment out 2023-09-28 - Databases harcoded in GetMemberPrefix function
                                      
                                      // Jaco 2023-09-28
                                      $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                      $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                      $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                      $" where OxygenBenefitOptions.ezHpCode = '{data.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross'  \n" +

                                      $"SET @membid = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membid)) \n" +
                                      $"INSERT INTO {PamcPortalDatabase}..AuthsHeaderFile (MembId,HPCode,Username,ProvId,Text,CreateDate,CreateBy,LastChangedDate,LastChangedBy) \n" +
                                      $"VALUES (@membid,@hpcode,@user,@provid,@Text,'{date}',999,'{date}',999)";
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = $"DECLARE @membid  VARCHAR(20) \n" +
                                      $"DECLARE @prefix  VARCHAR(20) \n" +
                                      $"SET @membid = '{data.membid}' \n" +
                                      //$"SET @prefix = {PamcPortalDatabase}.dbo.GetMemberPrefix('{data.hpcode}') \n" + // Jaco comment out 2023-09-28 - Databases harcoded in GetMemberPrefix function

                                      // Jaco 2023-09-28
                                      $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                      $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                      $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                      $" where OxygenBenefitOptions.ezHpCode = '{data.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross'  \n" +

                                      $"SET @membid = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membid)) \n" +
                                      $"SELECT HeadID FROM {PamcPortalDatabase}..AuthsHeaderFile WHERE MembId = @membid AND ProvId = @provid AND " +
                                      $"Username = @user AND CreateDate = '{date}'";
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dtbl.Load(dr);
                        }
                        foreach (DataRow row in dtbl.Rows)
                        {
                            _authId = row["HeadID"].ToString();
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                vr.valid = false;
                vr.message = "Could not submit authorisation\r\n\r\n" +  ex.Message;
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }
            return vr;
        }

        [HttpPost]
        [Route("SearchSpecificAuth")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AuthHeadersModel> MemberDetails([FromBody] AuthHeadersModel model)
        {
            OperationLog log = new OperationLog();
            log.LogAuth("Auth Search", model.userName, model.Practice, _portalConnectionString);


            SettingsModel returnedClaimlag = Utillity.GetSpecificSetting("Authlag", _portalConnectionString);

            DataTable dt = new DataTable();

            List<AuthHeadersModel> ListOfAuths = new List<AuthHeadersModel>();
            if (model.Plan == "000")
            {
                model.Plan = "";
            }
            try
            {
                model.ServiceDate = model.ServiceDate.Replace("-", "/");
                //claimcredentials.DatePaid = claimcredentials.DatePaid.Replace("-", "/");
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }



            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {

                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {

                    #region Build WhereClause 

                    if (model.Practice != "")
                    {
                        cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = model.Practice;
                    }
                    else
                    {
                        cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = DBNull.Value;
                    }
                    if (model.MemberNo != "")
                    {
                        cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = model.MemberNo;
                    }
                    else
                    {
                        cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = DBNull.Value;
                    }
                    if (model.Plan != "")
                    {
                        cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = model.Plan;
                    }
                    else
                    {
                        cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = DBNull.Value;
                    }
                    if (model.ServiceDate != "" && model.ServiceDate != null)
                    {
                        cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = model.ServiceDate;
                    }
                    else
                    {
                        cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = DBNull.Value;
                    }

                    string WhereClause = "";

                    if (model.Practice != "")
                    {
                        WhereClause = "where [Practice] = @practice";
                    }
                    if (model.MemberNo != "")
                    {
                        if (WhereClause != "")
                        {
                            WhereClause = WhereClause + " AND " + "([Family Number] = 'M'+isnull([MemberPrefix],'') + @membid"
                                                                + " OR [Family Number] = 'M' + @membid"
                                                                + " OR [Family Number] = @membid"
                                                                + " OR [Family Number] = isnull([MemberPrefix],'') + @membid"
                                                                + " OR [Member No] = @membid"
                                                                + " OR isnull([MemberPrefix],'') +@membid = [Member No])\r\n";
                        }
                        else
                        {
                            WhereClause = "WHERE ([Family Number] = 'M'+isnull([MemberPrefix],'') + @membid"
                                                                        + " OR [Family Number] = 'M' + @membid"
                                                                        + " OR [Family Number] = @membid"
                                                                        + " OR [Family Number] = isnull([MemberPrefix],'') + @membid"
                                                                        + " OR [Member No] = @membid"
                                                                        + " OR isnull([MemberPrefix],'') +@membid = [Member No])\r\n";
                        }
                    }
                    if (model.Plan != "")
                    {
                        if (WhereClause != "")
                        {
                            WhereClause = WhereClause + "AND " + "[Plan] = @hpCode";
                        }
                        else
                        {
                            WhereClause = "where [Plan] = @hpCode ";
                        }

                    }
                    if (model.ServiceDate != "" && model.ServiceDate != null)
                    {
                        if (WhereClause != "")
                        {
                            WhereClause = WhereClause + " AND " + "[Service Date] =  @serviceDate";
                        }
                        else
                        {
                            WhereClause = "where [Service Date] = @serviceDate";
                        }
                    }
                    #endregion endregion

                    cmd.CommandType = CommandType.Text;

                    // Jaco 2023-09-27 -Parameterise hardcoded databases
                    string sqlTextToExecute = "declare @membid varchar(20)\r\n" + // Tested with provider login
                                                "set @membid = @membnum\r\n" +
                                                "SELECT isnull([MemberPrefix],'') +@membid,[Member No],mm.OPT,AUTHS_HEADER.Practice, AUTHS_HEADER.Doctor, AUTHS_HEADER.[Family Number], " +
                                                "RIGHT(AUTHS_HEADER.[Member No],2) as DependentCode, AUTHS_HEADER.[Plan], AUTHS_HEADER.[Member Name], AUTHS_HEADER.[Service Date], " +
                                                " AUTHS_HEADER.[Auth No],cs.ref, AUTHS_HEADER.[Authorized Amount], AUTHS_HEADER.LOBCODE, AUTHS_HEADER.DESCR, op.MemberPrefix, case op.MemberPrefix when null then 0 else len(memberprefix) end as prefixlength\r\n" +
                                               $"from {PamcPortalDatabase}..AUTHS_HEADER \r\n" +
                                                "left outer join \r\n" +
                                                "(\r\n" +
                                               $"select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix from {ReportingDatabase}..[OxygenBenefitOptions] inner join {DRCDatabase}..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode where isnull(hp_c.CONTREND,getdate()) >= GETDATE() and scheme <> 'carecross'\r\n" +
                                                ") op \r\n" +
                                                "on op.ezHpCode = AUTHS_HEADER.[Plan]\r\n" +
                                                $"LEFT OUTER JOIN {PamcPortalDatabase}..Client_Auth_Submissions cs ON cs.authNo = AUTHS_HEADER.[Auth No]\n" +
                                                $"INNER JOIN {DRCDatabase}..MEMB_MASTERS mm ON mm.MEMBID = AUTHS_HEADER.[Member No]\n" +
                                                 WhereClause + " AND [Service Date] >= DATEADD(Month, - @claimLag, GETDATE())" +
                                                "order by [Service Date] desc\r\n";

                    cmd.Parameters.Add("@claimLag", SqlDbType.Int).Value = returnedClaimlag.description;

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        AuthHeadersModel auth = new AuthHeadersModel();
                        auth.Practice = dr["Practice"].ToString();
                        auth.Doctor = dr["Doctor"].ToString();
                        auth.MemberNo = dr["Family Number"].ToString();
                        auth.reff = dr["ref"].ToString();
                        if (auth.reff == "" || auth.reff == null)
                        {
                            auth.reff = "N/A";
                        }
                        auth.DependentCode = dr["DependentCode"].ToString();
                        auth.MemberName = dr["Member Name"].ToString();
                        auth.Plan = dr["OPT"].ToString();
                        auth.ServiceDate = dr["Service Date"].ToString();
                        auth.AuthNo = dr["Auth No"].ToString();
                        auth.AuthorizedAmount = dr["Authorized Amount"].ToString();
                        auth.LOBCODE = dr["LOBCODE"].ToString();
                        auth.DESCR = dr["DESCR"].ToString();
                        if (auth.DESCR == "EXPIRED")
                        {
                            auth.AuthorizedAmount = "N/A";
                        }
                        ListOfAuths.Add(auth);
                    }
                }
                Sqlcon.Close();
            }
            return ListOfAuths;
        }

        /*Auth Set Setup*/
        [HttpGet]
        [Route("GetSpecCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<SpecCodes> GetSPecCodes()
        {
            List<SpecCodes> list = new List<SpecCodes>();

            try
            {  
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT DESCR,CONVERT(INT,[CODE]) AS CODE FROM {DRCDatabase}.dbo.PROV_SPECCODES ORDER BY CODE ASC";
                    
                    DataTable dt = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    int count = 1;
                    int page = 1;
                    foreach (DataRow row in dt.Rows)
                    {
                        SpecCodes code = new SpecCodes();

                        if (row["CODE"].ToString().Length == 1)
                        {
                            code.code = "0" + row["CODE"].ToString();
                        }
                        else
                        {
                            code.code = row["CODE"].ToString();
                        }
                        code.page = page;
                        code.desc = row["DESCR"].ToString();
                        count++;
                        if (count == 11)
                        {
                            count = 1;
                            page++;
                        }
                        list.Add(code);
                    }
                }
            }
            catch (Exception e)
            {                
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetBenTypeDescAuth")]
        public ValidationResultsModel GetBenTypeDesc([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;                    
                    cmd.CommandText = $"EXEC {DRCDatabase}.dbo.GetBentypePortalAuths '{data.membno}', '{data.scvdate}', '{data.phCode}', '{data.procCode}'";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    data.benType = dt.Rows[0]["BENTYPE"].ToString();
                    data.benDesc = dt.Rows[0]["DESCR"].ToString();
                                        
                    vr.message = data.benType + '|' + data.benDesc;
                    vr.valid = true;
                }
            }
            catch (Exception e)
            {
                vr.valid = false;
                vr.message = "0" + '|' + "Error retrieving";
                LogError(e.Message, e.StackTrace);                
            }

            return vr;
        }

        [HttpPost]
        [Route("GetBenefitLimit")]
        public ValidationResultsModel GetBenefitLimit([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {                    

                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    // Get spec code
                    DataTable dt = new DataTable();
                    string spec = "";
                    if (data.spec != null && data.spec.Trim() != "") // Spec passed to API from front end
                    {
                        spec = data.spec.Trim();
                    }
                    else // Get spec from provid
                    {
                        cmd.CommandText = $"SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO WHERE PROVID = '{data.provid}'";                        
                        dt.Load(cmd.ExecuteReader());                        
                        spec = dt.Rows[0][0].ToString();
                    }

                    // Get bentype and proc
                    //cmd.CommandText = $"SELECT BenType,ProcCode FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE Masterid = '{data.id}' AND SpecCode = '{spec}' and ProcCode = '{data.procCode}'";
                    //dt = new DataTable();
                    //dt.Load(cmd.ExecuteReader());
                    //data.benType = dt.Rows[0][0].ToString();
                    //data.procCode = dt.Rows[0][1].ToString();

                    // Get prefix
                    //cmd.CommandText = 
                    //    $" select distinct memberprefix from {ReportingDatabase}.[dbo].[OxygenBenefitOptions] " +
                    //    $" where ezhpcode = 'esse' and memberprefix is not null ";

                    //dt = new DataTable();
                    //dt.Load(cmd.ExecuteReader());                    
                    //if (dt.Rows.Count > 0)
                    //{
                    //    data.membno = dt.Rows[0]["memberprefix"].ToString().Trim() + data.membno;
                    //}
                                        
                    // Get bentype     
                    cmd.CommandText = $"EXEC {DRCDatabase}.dbo.GetBentypePortalAuths '{data.membno}', '{data.scvdate}', '{data.phCode}', '{data.procCode}'";
                    dt = new DataTable();
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    dt.Load(cmd.ExecuteReader());
                    data.benType = dt.Rows[0]["BENTYPE"].ToString();
                    data.benDesc = dt.Rows[0]["DESCR"].ToString();
                 
                    // Get benefits left according to claims and auths on DRC database
                    cmd.CommandText = $"EXEC {DRCDatabase}.dbo.procGetBenLeft '{data.membno}', '{data.scvdate}', '{data.benType}'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string memb = "";
                    string fam = "";

                    memb = dt.Rows[0][0].ToString();
                    fam = dt.Rows[0][1].ToString();

                    // If there is family limit and member limit, use the minimum of the two, otherwise use the member or family limit (For AFFI there will only be member(96) or family(103))
                    string limitToUse = "0.00";
                    if((memb == "") && (fam == ""))
                    {
                        limitToUse = "0.00";
                    }                    
                    else if ((memb != "") && (fam != ""))
                    {
                        limitToUse = (Math.Min(Convert.ToDecimal(memb), Convert.ToDecimal(fam))).ToString();                        
                    }
                    else if (memb != "")
                    {
                        limitToUse = memb;
                    }
                    else if (fam != "")
                    {
                        limitToUse = fam;
                    }

                    // Take into account used benefits from unprocessed auths in Client_Auth_Submissions (Auths not yet in DRC)
                    string queryStr =  " SELECT isnull( sum(convert(decimal (16,2), amount)) , 0)  as AMOUNTUSED \r\n " +
                                      $" FROM {PamcPortalDatabase}.dbo.Client_Auth_Submissions \r\n " +
                                       " WHERE membid = @policynum and BenefitId = @bentype and authno is null  AND AMOUNT <> '' ";

                    //cmd.Connection = _pamcPortalConnection; // Jaco 2023-09-11
                    cmd.Connection = new SqlConnection(_portalConnectionString);  // Jaco 2023-09-11
                    cmd.CommandText = queryStr;                    
                    
                    cmd.Parameters.Add(new SqlParameter("@policynum", data.membno.Substring(0, data.membno.LastIndexOf('-')) )); //remove dependant code
                    cmd.Parameters.Add(new SqlParameter("@bentype", data.benType));

                    dt = new DataTable();
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    dt.Load(cmd.ExecuteReader());

                    string unprocessedAuthsUsedAmt = dt.Rows[0]["AMOUNTUSED"].ToString();

                    limitToUse = (Convert.ToDecimal(limitToUse) - Convert.ToDecimal(unprocessedAuthsUsedAmt)).ToString();

                    //vr.message = (Convert.ToDouble(memb) + Convert.ToDouble(fam)).ToString() + '|' + data.benType + '|' + data.benDesc;
                    vr.message = limitToUse + '|' + data.benType + '|' + data.benDesc;
                    vr.valid = true;
                }
            }
            catch (Exception e)
            {
                vr.valid = false;
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return vr;
        }

        [HttpPost]
        [Route("GetExistingBef")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<specmasters> GetExistingBef([FromBody] LoginDetailsModel data)
        {
            List<specmasters> list = new List<specmasters>();
            try
            {    
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    // Jaco comment out 2023-09-08 View is hardcoded to DRC
                    //cmd.CommandText = " SELECT i.ID, i.SetDesc,i.hpcode FROM dbo.UserLobCodesHpCodes u \n " + 
                    //                  " INNER JOIN Insurance_Templates_Master i on i.hpcode = u.HPCODE \n " +
                    //                  " WHERE u.username = @user ";

                    // Jaco 2023-09-08
                    cmd.CommandText =   " SELECT i.ID, i.SetDesc, i.hpcode " + // Tested with client login
                                        " FROM( " +
                                        "        SELECT        u.username, u.lobcode, hp.HPCODE " +
                                       $"        FROM          {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                       $"        {DRCDatabase}.dbo.HP_CONTRACTS AS hp " +
                                       $"        where hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode, ',')) " +
                                        "     ) u " +
                                       $" INNER JOIN  {PamcPortalDatabase}..Insurance_Templates_Master i on i.hpcode = u.HPCODE " +
                                        " WHERE u.username = @user ";

                    DataTable dt = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        specmasters sp = new specmasters();
                        sp.id = Convert.ToInt32(row["ID"].ToString());
                        sp.desc = row["SetDesc"].ToString();
                        sp.hpcode = row["HPCODE"].ToString();
                        list.Add(sp);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetBenifitListForHp")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<specmasters> GetBenifitListForHp([FromBody] HealthplanModelcs data)
        {
            List<specmasters> list = new List<specmasters>();
            try
            {                
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@hp", data.code));
                    cmd.CommandText = "SELECT ID, SetDesc,hpcode FROM\n"
                                   + $"    {PamcPortalDatabase}.dbo.Insurance_Templates_Master \n"
                                   + "WHERE hpcode = @hp";

                    DataTable dt = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        specmasters sp = new specmasters();

                        sp.id = Convert.ToInt32(row["ID"].ToString());
                        sp.desc = row["SetDesc"].ToString();
                        sp.hpcode = row["HPCODE"].ToString();

                        list.Add(sp);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetDetailBen")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<SpecCodes> GetDetailBen([FromBody] specmasters data)
        {
            List<SpecCodes> list = new List<SpecCodes>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@opt", data.opt));

                    //cmd.CommandText = $"SELECT DISTINCT  SpecCode,ProcCode,BenType, MasterId, bc.DESCR  FROM \n" +
                    //cmd.CommandText = $"SELECT DISTINCT Opt,SpecCode,ProcCode,BenType,PhCodeOnNetwork,PhCodeOffNetwork,PlaceSvcCode,AvailableBenefitToAmtCheck,MasterId,bc.DESCR  FROM \n" +
                    //    $"{PamcPortalDatabase}.dbo.Insurance_Templates_Detail itd\n" +
                    //    $"LEFT OUTER JOIN {DRCDatabase}.dbo.PROC_CODES pc on REPLACE(pc.SVCCODE,' ','') = itd.ProcCode \n" +
                    //    $"LEFT OUTER JOIN {DRCDatabase}.dbo.BENTRACK_CODES bc ON bc.CODE = itd.BenType \n" +
                    //    $"\n WHERE MasterId = @id order by SpecCode";

                    cmd.CommandText =
                    $" SELECT d.MasterId, m.SetDesc, m.HpCode, d.id as DetailId, d.Opt, convert(int,ltrim(ltrim(d.SpecCode))) as SpecCode, d.ProcCode, d.PhCodeOnNetwork, STnetw.DESCR as NetwDesc, d.PhCodeOffNetwork, STnonetw.DESCR as NoNetwDesc, d.PlaceSvcCode, d.DisplayAvailableBenefit, d.EnterAmount, d.AvailableBenefitToAmtCheck,d.BenType, d.IncludeLengthOfStay \n" +
                    //$", STnetw.HPCODE, STnetw.OPT, STnonetw.HPCODE, STnonetw.OPT \n" + // Only for dev
                    $" FROM Insurance_Templates_Detail d \n" +
                    $" INNER JOIN Insurance_Templates_Master m on(m.id = d.Masterid) \n" +
                    $" INNER JOIN {DRCDatabase}.dbo.SERVICE_TYPES STnetw on(m.HpCode = STnetw.HPCODE and d.PhCodeOnNetwork = STnetw.PHCODE) \n" +
                    $" INNER JOIN {DRCDatabase}.dbo.SERVICE_TYPES STnonetw on(m.HpCode = STnonetw.HPCODE and d.PhCodeOffNetwork = STnonetw.PHCODE) \n" +
                    $" where d.MasterId = @id \n" +
                    $" and STnetw.OPT = @opt and STnonetw.OPT = @opt \n" +
                    $" and(d.opt = @opt or ltrim(rtrim(d.OPT)) = 'All') \n" +
                    $" order by SpecCode ";

                    DataTable dt = new DataTable();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    string prevSpecCode = "";
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["SpecCode"].ToString() != prevSpecCode) // Add new spec code
                        {
                            // Initialise Model and list within model
                            SpecCodes spec = new SpecCodes();
                            spec.specDetail = new List<AuthSpecDetail>();

                            // Add Masterid and SpecCode
                            spec.code = row["SpecCode"].ToString();
                            spec.id = Convert.ToInt32(row["MasterId"].ToString());
                            //s.procCode = row["ProcCode"].ToString();
                            //s.benCode = row["DESCR"].ToString() + "-" + row["BenType"].ToString();

                            // Add Proc Code Detail to SpecCodes.ProcCodeInfo
                            AuthSpecDetail specDetail = new AuthSpecDetail();
                            specDetail.procCode = row["ProcCode"].ToString();
                            //specDetail.benCode = row["DESCR"].ToString() + "-" + row["BenType"].ToString();
                            specDetail.phCodeOnNetwork = row["PhCodeOnNetwork"].ToString();
                            specDetail.phCodeOnNetworkDesc = row["NetwDesc"].ToString();
                            specDetail.phCodeOffNetwork = row["PhCodeOffNetwork"].ToString();
                            specDetail.phCodeOffNetworkDesc = row["NoNetwDesc"].ToString();
                            specDetail.phCodeDetermined = "";
                            specDetail.phCodeDeterminedDesc = "";
                            specDetail.placeSvcCode = row["PlaceSvcCode"].ToString();
                            specDetail.displayAvailableBenefit = Convert.ToBoolean(row["DisplayAvailableBenefit"].ToString());
                            specDetail.enterAmount = Convert.ToBoolean(row["EnterAmount"].ToString());
                            specDetail.availableBenefitToAmtCheck = Convert.ToBoolean(row["AvailableBenefitToAmtCheck"].ToString());
                            specDetail.includeLengthOfStay = Convert.ToBoolean(row["IncludeLengthOfStay"].ToString());

                            spec.specDetail.Add(specDetail);

                            list.Add(spec);
                        }
                        else   // Only add procCode detail to spec code           
                        {          
                            // Find list item for this spec code
                            SpecCodes spec = list.Find(x => x.code == row["SpecCode"].ToString());

                            // Add Proc Code Detail to list item
                            AuthSpecDetail a = new AuthSpecDetail();
                            a.procCode = row["ProcCode"].ToString();
                            //a.benCode = row["DESCR"].ToString() + "-" + row["BenType"].ToString();
                            a.phCodeOnNetwork = row["PhCodeOnNetwork"].ToString();
                            a.phCodeOnNetworkDesc = row["NetwDesc"].ToString();
                            a.phCodeOffNetwork = row["PhCodeOffNetwork"].ToString();
                            a.phCodeOffNetworkDesc = row["NoNetwDesc"].ToString();
                            a.placeSvcCode = row["PlaceSvcCode"].ToString();
                            a.displayAvailableBenefit = Convert.ToBoolean(row["DisplayAvailableBenefit"].ToString());
                            a.enterAmount = Convert.ToBoolean(row["EnterAmount"].ToString());
                            a.availableBenefitToAmtCheck = Convert.ToBoolean(row["AvailableBenefitToAmtCheck"].ToString());
                            a.includeLengthOfStay = Convert.ToBoolean(row["IncludeLengthOfStay"].ToString());

                            spec.specDetail.Add(a);
                        }

                        prevSpecCode = row["SpecCode"].ToString();
                    }
                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        /*Add new auth template*/
        [HttpPost]
        [Route("InsuranceMasterSubmit")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterSubmit([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data.user));
                    cmd.Connection = cn;
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Master  (SetDesc,hpcode,CreateDate,CreateBy,ChangedDate,ChangeBy) \n" +
                        $"OUTPUT INSERTED.ID \n" +
                        $"VALUES(@desc,@hpcode,@date,@user,@date,@user)";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceDetailSubmit")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel InsuranceDetailSubmit([FromBody] List<SpecCodes> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    bool spec_1 = false;
                    bool spec_2 = false;

                    foreach (SpecCodes item in data)
                    {
                        if ((item.code == "14") && (item.selected == true))
                        {
                            spec_1 = true;
                        }
                        if ((item.code == "15") && (item.selected == true))
                        {
                            spec_2 = true;
                        }
                    }

                    if (spec_1 && spec_2)
                    {
                        foreach (SpecCodes item in data)
                        {
                            if ((item.code == "15"))
                            {
                                item.selected = false;
                            }
                        }
                    }
                    foreach (SpecCodes item in data)
                    {
                        cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Detail (MasterId,ProcCode,BenType,SpecCode,CreateDate,CreateBy,ChangedDate,ChangedBy) \n" +
                            $"VALUES('{item.id}','{item.procCode}','{item.benCode}','{item.code}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.user}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.user}')";

                        if (item.selected == true)
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }


        /*delete  auth template*/
        [HttpPost]
        [Route("InsuranceMasterDelete")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterDelete([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Connection = cn;
                    cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Master WHERE ID = @id";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceDetailDelete")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceDetailDelete([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Connection = cn;
                    cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE MasterId = @id";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }


        /*Update existing auth set*/
        [HttpPost]
        [Route("InsuranceMasterUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterUpdate([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data.user));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));

                    cmd.Connection = cn;
                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Insurance_Templates_Master SET SetDesc = @desc ,hpcode = @hpcode ," +
                        $"ChangedDate = @date ,ChangeBy = @user WHERE ID = @id \n";
                    cmd.ExecuteNonQuery();


                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("ValidateProcCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProcCode([FromBody] SpecCodes data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@proc", data.procCode.PadLeft(20, ' ')));
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.SERVICE_CODES WHERE SVCCODE = @proc";

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }


        [HttpPost]
        [Route("InsuranceDetailUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel InsuranceDetailUPdate([FromBody] List<SpecCodes> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                string ph = data[0].phcode;
                List<string> newList = new List<string>();

                string username = "";
                foreach (SpecCodes item in data)
                {
                    if (item.selected == true)
                    {
                        username = item.user;
                        newList.Add(item.code);
                    }
                }

                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    int id = 0;
                    cmd.Connection = cn;
                    foreach (SpecCodes item in data)
                    {
                        if (item.selected == true)
                        {
                            id = item.id;
                        }
                    }

                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    cmd.CommandText = $"SELECT SpecCode FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE MasterId = @id ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    List<string> codes = new List<string>();

                    foreach (DataRow row in dt.Rows)
                    {
                        string code = "";
                        code = row["SpecCode"].ToString();
                        if (code.Length == 1)
                        {
                            code = "0" + code;
                        }
                        codes.Add(code);
                    }
                    List<string> delete = new List<string>();
                    List<string> newCodes = new List<string>();
                    List<string> update = new List<string>();

                    // bool found = false;
                    foreach (string item in newList)
                    {

                        if (codes.Contains(item))
                        {
                            update.Add(item);
                        }
                        else
                        {
                            newCodes.Add(item);
                        }

                    }
                    foreach (string item in codes)
                    {
                        if (newList.Contains(item))
                        {

                        }
                        else
                        {
                            delete.Add(item);
                        }

                    }


                    foreach (string code in update)
                    {

                        foreach (SpecCodes item in data)
                        {

                            if (item.code == code)
                            {
                                if ((item.benCode != null) && (item.procCode != null) && (item.benCode != "") && (item.procCode != "") && (item.benCode.Length < 4))
                                {
                                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Insurance_Templates_Detail SET \n" +
                                         $"ProcCode = '{item.procCode}' , BenType = '{item.benCode}'," +
                                         $"ChangedDate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}' WHERE   MasterId = '{id}' AND SpecCode = '{code}'\n";
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }

                    }

                    foreach (string code in delete)
                    {
                        string deletecode = code;
                        if (code.Length == 2)
                        {
                            if (code.Substring(0, 1) == "0")
                            {

                                deletecode = code.Substring(1, 1);
                            }
                        }
                        cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail  WHERE MasterId = '{id}' AND SpecCode = '{deletecode}'";
                        cmd.ExecuteNonQuery();

                    }

                    foreach (string code in newCodes)
                    {

                        foreach (SpecCodes item in data)
                        {

                            if (item.code == code)
                            {
                                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Detail " +
                                             $"(MasterId,ProcCode,BenType,SpecCode,CreateDate,CreateBy,ChangedDate,ChangedBy) \n" +
                                          $"VALUES('{id}','{item.procCode}','{item.benCode}','{code}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
                                          $"'{username}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{username}')";
                                cmd.ExecuteNonQuery();
                            }
                        }

                    }

                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }

        [HttpGet]
        [Route("GetBenTypeCodes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<BenTrackCodes> GetBenTypeCodes()
        {
            List<BenTrackCodes> list = new List<BenTrackCodes>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.BENTRACK_CODES WHERE CODE IN('96','103','90') ORDER BY DESCR ASC";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        BenTrackCodes t = new BenTrackCodes();

                        t.code = row["CODE"].ToString();
                        t.desc = row["DESCR"].ToString() + $" ({t.code})";

                        list.Add(t);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

            }
            return list;
        }


        /*Method used to log all errors that take place in this controller*/
        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "AUTH";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\AUTH\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }
    }
}
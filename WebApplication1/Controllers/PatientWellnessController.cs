﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientWellnessController : Base.PAMCController
    {
        public PatientWellnessController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("CheckIfPatientExist")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public PatientWellnessFormModel CheckIfPatientExist([FromBody] PatientWellnessFormModel inputData)
        {            
            PatientWellnessFormModel returnData = new PatientWellnessFormModel();
            returnData.success = false;
            returnData.message = "";

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@idNo", inputData.idNo.Trim()));                    
                    cmd.CommandText =
                        $"SELECT * FROM {PamcPortalDatabase}.dbo.PATIENT_WELLNESSMASTERS WHERE IDNO = @idNo";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader()); 
                    cmd.Dispose();

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            returnData.idNo = row["IDNO"].ToString();
                            if (row["NAME"].ToString() != "") { returnData.name = row["NAME"].ToString(); }
                            if (row["SURNAME"].ToString() != "") { returnData.surname = row["SURNAME"].ToString(); }
                            if (row["TEL"].ToString() != "") { returnData.tel = row["TEL"].ToString(); }
                            if (row["CELL"].ToString() != "") { returnData.cell = row["CELL"].ToString(); }
                            if (row["GENDER"].ToString() != "")
                            {
                                returnData.gender = row["GENDER"].ToString();
                                if (returnData.gender == "M") returnData.gender = "Male";
                                else if (returnData.gender == "F") returnData.gender = "Female";
                            }
                            if (row["ADDRESS"].ToString() != "") { returnData.address = row["ADDRESS"].ToString(); }
                            if (row["EMAIL"].ToString() != "") { returnData.email = row["EMAIL"].ToString(); }
                            if (row["MEDICALAIDYN"].ToString() != "") 
                            { 
                                returnData.medicalAidYN = row["MEDICALAIDYN"].ToString();
                                if (returnData.medicalAidYN == "True") returnData.medicalAidYN = "Yes";
                                else if (returnData.medicalAidYN == "False") returnData.medicalAidYN = "No";
                            }                            
                            if (row["MEDICALAIDPLAN"].ToString() != "") { returnData.medicalAidPlan = row["MEDICALAIDPLAN"].ToString(); }                            
                            if (row["MEDICALAIDNO"].ToString() != "") { returnData.medicalAidNo = row["MEDICALAIDNO"].ToString(); }
                            if (row["MEDICALAIDDEPCODE"].ToString() != "") { returnData.medicalAidDepCode = row["MEDICALAIDDEPCODE"].ToString(); }                                
                            if (row["SCREENINGDATE"].ToString() != "")
                            {
                                DateTime tempDate = Convert.ToDateTime(row["SCREENINGDATE"]);
                                returnData.screeningDate = tempDate.ToString("yyyy-MM-dd");
                            }
                            if (row["ASSESMENTCONSENT"].ToString() != "") 
                            { 
                                returnData.assessmentConsent = row["ASSESMENTCONSENT"].ToString();
                                if (returnData.assessmentConsent == "True") returnData.assessmentConsent = "Yes";
                                else if (returnData.assessmentConsent == "False") returnData.assessmentConsent = "No";
                            }                           
                            if (row["CHRONICCONDITIONS"].ToString() != "") { returnData.chronicConditions = row["CHRONICCONDITIONS"].ToString(); }
                            if (row["MEDICATIONS"].ToString() != "") { returnData.medications = row["MEDICATIONS"].ToString(); }
                            if (row["HEIGHT"].ToString() != "") { returnData.height = row["HEIGHT"].ToString(); }
                            if (row["WEIGHT"].ToString() != "") { returnData.weight = row["WEIGHT"].ToString(); }
                            if (row["BMI"].ToString() != "") { returnData.bmi = row["BMI"].ToString(); }
                            if (row["WAIST"].ToString() != "") { returnData.waist = row["WAIST"].ToString(); }
                            if (row["BLOODPRESSURESYSTOLIC"].ToString() != "") { returnData.bloodPressureSystolic = row["BLOODPRESSURESYSTOLIC"].ToString(); }
                            if (row["BLOODPRESSUREDIASTOLIC"].ToString() != "") { returnData.bloodPressureDiastolic = row["BLOODPRESSUREDIASTOLIC"].ToString(); }
                            if (row["CHOLESTEROLHDL"].ToString() != "") { returnData.cholesterolHDL = row["CHOLESTEROLHDL"].ToString(); }
                            if (row["CHOLESTEROLTRI"].ToString() != "") { returnData.cholesterolTRI = row["CHOLESTEROLTRI"].ToString(); }
                            if (row["CHOLESTEROLLDL"].ToString() != "") { returnData.cholesterolLDL = row["CHOLESTEROLLDL"].ToString(); }
                            if (row["CHOLESTEROLTOTAL"].ToString() != "") { returnData.cholesterolTotal = row["CHOLESTEROLTOTAL"].ToString(); }
                            if (row["BLOODSUGARMMOLS"].ToString() != "") { returnData.bloodSugarMmols = row["BLOODSUGARMMOLS"].ToString(); }
                            if (row["HIVCONSENT"].ToString() != "") 
                            { 
                                returnData.hivConsent = row["HIVCONSENT"].ToString();
                                if (returnData.hivConsent == "True") returnData.hivConsent = "Yes";
                                else if (returnData.hivConsent == "False") returnData.hivConsent = "No";
                            }                            
                            if (row["SEXPASTSIXMONTHS"].ToString() != "") { returnData.sexPastSixMonths = row["SEXPASTSIXMONTHS"].ToString(); }
                            if (row["CONDOMPASTSIXMONTHS"].ToString() != "") { returnData.condomPastSixMonths = row["CONDOMPASTSIXMONTHS"].ToString(); }
                            if (row["GETAIDSWORRY"].ToString() != "") { returnData.getAidsWorry = row["GETAIDSWORRY"].ToString(); }
                            if (row["EXPOSEDAIDSWORRY"].ToString() != "") { returnData.exposedAidsWorry = row["EXPOSEDAIDSWORRY"].ToString(); }
                            if (row["MONTHLASTAIDSTEST"].ToString() != "") { returnData.monthLastAidsTest = row["MONTHLASTAIDSTEST"].ToString(); }
                            if (row["YEARLASTAIDSTEST"].ToString() != "") { returnData.yearLastAidsTest = row["YEARLASTAIDSTEST"].ToString(); }
                            if (row["TOLDAIDS"].ToString() != "") { returnData.toldAids = row["TOLDAIDS"].ToString(); }                               

                            returnData.message = "found";
                            break;
                        }
                    }
                    else
                    {
                        returnData.message = "not found";
                    }

                    returnData.success = true;                           
                }
            }
            catch (Exception e)
            {
                returnData.success = false;
                returnData.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return returnData;
        }

        [HttpPost]
        [Route("SubmitPatientWellnessForm")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public PatientWellnessFormModel SubmitPatientWellnessForm([FromBody] PatientWellnessFormModel formData)
        {
            formData.success = false;
            formData.message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {                    
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@idNo", formData.idNo));
                    cmd.Parameters.Add(new SqlParameter("@name", formData.name ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@surname", formData.surname ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@tel", formData.tel ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cell", formData.cell ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@gender", formData.gender ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@address", formData.address ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@email", formData.email ?? (object)DBNull.Value));
                    if (formData.medicalAidYN == "Yes") { cmd.Parameters.Add(new SqlParameter("@medicalAidYN", true)); }
                    else if (formData.medicalAidYN == "No") { cmd.Parameters.Add(new SqlParameter("@medicalAidYN", false)); }
                    else { cmd.Parameters.Add(new SqlParameter("@medicalAidYN", DBNull.Value)); }
                    cmd.Parameters.Add(new SqlParameter("@medicalAidPlan", formData.medicalAidPlan ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@medicalAidNo", formData.medicalAidNo ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@medicalAidDepCode", formData.medicalAidDepCode ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@screeningDate", formData.screeningDate ?? (object)DBNull.Value));                    
                    if (formData.assessmentConsent == "Yes") { cmd.Parameters.Add(new SqlParameter("@assessmentConsent", true)); }
                    else if (formData.assessmentConsent == "No") { cmd.Parameters.Add(new SqlParameter("@assessmentConsent", false)); }
                    else { cmd.Parameters.Add(new SqlParameter("@assessmentConsent", DBNull.Value)); }
                    cmd.Parameters.Add(new SqlParameter("@chronicConditions", formData.chronicConditions ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@medications", formData.medications ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@height", formData.height ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@weight", formData.weight ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@bmi", formData.bmi ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@waist", formData.waist ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@bloodPressureSystolic", formData.bloodPressureSystolic ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@bloodPressureDiastolic", formData.bloodPressureDiastolic ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolHDL", formData.cholesterolHDL ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolTRI", formData.cholesterolTRI ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolLDL", formData.cholesterolLDL ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolTotal", formData.cholesterolTotal ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@bloodSugarMmols", formData.bloodSugarMmols ?? (object)DBNull.Value));                    
                    if (formData.hivConsent == "Yes") { cmd.Parameters.Add(new SqlParameter("@hivConsent", true)); }
                    else if (formData.hivConsent == "No") { cmd.Parameters.Add(new SqlParameter("@hivConsent", false)); }
                    else { cmd.Parameters.Add(new SqlParameter("@hivConsent", DBNull.Value)); }
                    cmd.Parameters.Add(new SqlParameter("@sexPastSixMonths", formData.sexPastSixMonths ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@condomPastSixMonths", formData.condomPastSixMonths ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@getAidsWorry", formData.getAidsWorry ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@exposedAidsWorry", formData.exposedAidsWorry ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@monthLastAidsTest", formData.monthLastAidsTest ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@yearLastAidsTest", formData.yearLastAidsTest ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@toldAids", formData.toldAids ?? (object)DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@user", formData.user));
                    cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}.dbo.PATIENT_WELLNESSMASTERS WHERE IDNO = @idNo";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0) // update
                    {
                        cmd.CommandText = 
                           $" UPDATE {PamcPortalDatabase}.dbo.PATIENT_WELLNESSMASTERS SET " +
                            "  [NAME] = @name " +
                            " ,[SURNAME] = @surname " +
                            " ,[TEL] = @tel " +
                            " ,[CELL] = @cell " +
                            " ,[GENDER] = @gender" +
                            " ,[ADDRESS] = @address " +
                            " ,[EMAIL] = @email " +
                            " ,[MEDICALAIDYN] = @medicalAidYN " +
                            " ,[MEDICALAIDPLAN] = @medicalAidPlan " +
                            " ,[MEDICALAIDNO] = @medicalAidNo " +
                            " ,[MEDICALAIDDEPCODE] = @medicalAidDepCode " +
                            " ,[SCREENINGDATE] = @screeningDate " +
                            " ,[ASSESMENTCONSENT] = @assessmentConsent " +
                            " ,[CHRONICCONDITIONS] = @chronicConditions " +
                            " ,[MEDICATIONS] = @medications " +
                            " ,[HEIGHT] = @height " +
                            " ,[WEIGHT] = @weight " +
                            " ,[BMI] = @bmi " +
                            " ,[WAIST] = @waist " +
                            " ,[BLOODPRESSURESYSTOLIC] = @bloodPressureSystolic " +
                            " ,[BLOODPRESSUREDIASTOLIC] = @bloodPressureDiastolic " +
                            " ,[CHOLESTEROLHDL] = @cholesterolHDL " +
                            " ,[CHOLESTEROLTRI] = @cholesterolTRI " +
                            " ,[CHOLESTEROLLDL] = @cholesterolLDL " +
                            " ,[CHOLESTEROLTOTAL] = @cholesterolTotal " +
                            " ,[BLOODSUGARMMOLS] = @bloodSugarMmols " +
                            " ,[HIVCONSENT] = @hivConsent " +
                            " ,[SEXPASTSIXMONTHS] = @sexPastSixMonths " +
                            " ,[CONDOMPASTSIXMONTHS] = @condomPastSixMonths " +
                            " ,[GETAIDSWORRY] = @getAidsWorry " +
                            " ,[EXPOSEDAIDSWORRY] = @exposedAidsWorry " +
                            " ,[MONTHLASTAIDSTEST] = @monthLastAidsTest  " +
                            " ,[YEARLASTAIDSTEST] = @yearLastAidsTest " +
                            " ,[TOLDAIDS] = @toldAids " +
                            " ,[LASTCHANGEBY] = @user " +
                            " ,[LASTCHANGEDATE] = GETDATE() " +
                            " WHERE IDNO = @idNo ";
                        
                        if (cn.State != ConnectionState.Open) cn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    else //insert
                    {                        
                        cmd.CommandText =
                            $"INSERT INTO {PamcPortalDatabase}.dbo.PATIENT_WELLNESSMASTERS " +
                            " ([IDNO]  " +
                            " ,[NAME] " +
                            " ,[SURNAME] " +
                            " ,[TEL] " +
                            " ,[CELL] " +
                            " ,[GENDER] " +
                            " ,[ADDRESS] " +
                            " ,[EMAIL] " +
                            " ,[MEDICALAIDYN] " +
                            " ,[MEDICALAIDPLAN] " +
                            " ,[MEDICALAIDNO] " +
                            " ,[MEDICALAIDDEPCODE] " +
                            " ,[SCREENINGDATE] " +
                            " ,[ASSESMENTCONSENT] " +
                            " ,[CHRONICCONDITIONS] " +
                            " ,[MEDICATIONS] " +
                            " ,[HEIGHT] " +
                            " ,[WEIGHT] " +
                            " ,[BMI] " +
                            " ,[WAIST] " +
                            " ,[BLOODPRESSURESYSTOLIC] " +
                            " ,[BLOODPRESSUREDIASTOLIC] " +
                            " ,[CHOLESTEROLHDL] " +
                            " ,[CHOLESTEROLTRI] " +
                            " ,[CHOLESTEROLLDL] " +
                            " ,[CHOLESTEROLTOTAL] " +
                            " ,[BLOODSUGARMMOLS] " +
                            " ,[HIVCONSENT] " +
                            " ,[SEXPASTSIXMONTHS] " +
                            " ,[CONDOMPASTSIXMONTHS] " +
                            " ,[GETAIDSWORRY] " +
                            " ,[EXPOSEDAIDSWORRY] " +
                            " ,[MONTHLASTAIDSTEST] " +
                            " ,[YEARLASTAIDSTEST] " +
                            " ,[TOLDAIDS] " +
                            " ,[CREATEBY] " +
                            " ,[CREATEDATE] " +
                            " ,[LASTCHANGEBY] " +
                            " ,[LASTCHANGEDATE]) \n" +
                            " OUTPUT INSERTED.ROWID \n " +
                            " VALUES(@idNo, " +
                            "        @name, " +
                            "        @surname, " +
                            "        @tel, " +
                            "        @cell, " +
                            "        @gender, " +
                            "        @address, " +
                            "        @email, " +
                            "        @medicalAidYN, " +
                            "        @medicalAidPlan, " +
                            "        @medicalAidNo, " +
                            "        @medicalAidDepCode, " +
                            "        @screeningDate, " +
                            "        @assessmentConsent, " +
                            "        @chronicConditions, " +
                            "        @medications, " +
                            "        @height, " +
                            "        @weight, " +
                            "        @bmi, " +
                            "        @waist, " +
                            "        @bloodPressureSystolic, " +
                            "        @bloodPressureDiastolic, " +
                            "        @cholesterolHDL, " +
                            "        @cholesterolTRI, " +
                            "        @cholesterolLDL, " +
                            "        @cholesterolTotal, " +
                            "        @bloodSugarMmols, " +
                            "        @hivConsent, " +
                            "        @sexPastSixMonths, " +
                            "        @condomPastSixMonths, " +
                            "        @getAidsWorry, " +
                            "        @exposedAidsWorry, " +
                            "        @monthLastAidsTest, " +
                            "        @yearLastAidsTest, " +
                            "        @toldAids, " +
                            "        @user, " +
                            "        GETDATE(), " +
                            "        @user, " +
                            "        GETDATE())";

                        if (cn.State != ConnectionState.Open) cn.Open();
                        string rowid = cmd.ExecuteScalar().ToString();                        
                    }

                    cn.Close();
                    cmd.Dispose();

                    formData.success = true;
                    formData.message = "";
                }
            }
            catch (Exception e)
            {
                formData.success = false;
                formData.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return formData;
        }

        /*Method used to log all errors that take place in this controller*/
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "PATIENTWELLNESS";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\PATIENTWELLNESS\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}

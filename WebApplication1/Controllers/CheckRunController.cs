﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using OfficeOpenXml;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckRunController : Base.PAMCController
    {
        public CheckRunController(IWebHostEnvironment env, IConfiguration con):base(env,con)
        {

        }

        //Generates a new CheckRun according to the users choosen filter
        [HttpPost]
        [Route("BordRowCreation")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunModel> BordRowCreation([FromBody] BatchDetailsModel batchNo)
        {
            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : BordRowCreation---------------\r\n";
            ValidationResultsModel vr = new ValidationResultsModel();
            List<CheckRunModel> boardRowList = new List<CheckRunModel>();
            string[] where;
            string whereClause = "";
            int count = 0;

            try
            {
                if (batchNo.sorting == "ben")
                {


                    if ((batchNo.whereClause.Length > 1) && (batchNo.whereClause != null))
                    {
                        where = batchNo.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;
                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"'{condition}', ";
                            }
                            else
                            {
                                whereClause = whereClause + $"'{condition}'";
                            }


                        }
                        whereClause = $"RPT_CLAIMS_BR.SPEC IN ({whereClause})";
                        whereClause = $"AND ({whereClause})";

                    }
                }
                if (batchNo.sorting == "spec")
                {


                    if ((batchNo.whereClause.Length > 1) && (batchNo.whereClause != null))
                    {
                        where = batchNo.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;
                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"'{condition}', ";
                            }
                            else
                            {
                                whereClause = whereClause + $"'{condition}'";
                            }


                        }
                        whereClause = $"RPT_CLAIMS_BR.SPEC IN ({whereClause})";
                        whereClause = $"AND ({whereClause})";

                    }
                }
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} : WhereClause - '{whereClause}'";
                //Get SQL Data and generate model to be sent back
                try
                {
                    DataTable dt = new DataTable();
                    DataTable dtabel = new DataTable();
                    DataTable tbl = new DataTable();
                    DataTable data = new DataTable();
                    DataTable hptabel = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    SqlConnection cn = new SqlConnection();
                    cn.ConnectionString = _drcConnectionString;
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    cmd.Connection = cn;

                    whereClause = whereClause.Replace("'", "\"");
                    cmd.CommandText = $"UPDATE PAYBATCH SET  FILTER = '{whereClause}' WHERE BATCH = {batchNo.batchNumber}";
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :UPDATE WHERE - {cmd.CommandText}---\r\n";
                    cmd.ExecuteNonQuery();
                    whereClause = whereClause.Replace("\"", "'");
                    if (whereClause.Contains("'PRIVATE HOSPITAL ('B'- STATUS)'"))
                    {
                        whereClause = whereClause.Replace("'PRIVATE HOSPITAL ('B'- STATUS)'", @"'PRIVATE HOSPITAL (""B"" - STATUS)'");
                    }

                    string hpcode = "";
                    string[] hpname;

                    // Jaco comment out 2023-09-14 View is hardcoded to DRC
                    //string sql = $"SELECT HPNAME FROM {PamcPortalDatabase}..UserLobCodesHpCodes INNER JOIN\n"   
                    //           + $"{PamcPortalDatabase}..INSURANCE_CLIENTS  IC ON CONVERT(VARCHAR,IC.HPCODE) = {PamcPortalDatabase}..UserLobCodesHpCodes.HPCODE \n"
                    //           + $" WHERE USERNAME = '{batchNo.hpCode}'";
                    //cmd.CommandText = sql;

                    // Jaco 2023-09-14
                    cmd.CommandText = $" SELECT HPNAME " + //Jaco 2023-11-24 tested 
                                      $" FROM ( " +
                                      $"         SELECT        u.username, u.lobcode, hp.HPCODE                                                 " +
                                      $"         FROM          {PamcPortalDatabase}..Users AS u CROSS JOIN                                      " +
                                      $"                           {DRCDatabase}..HP_CONTRACTS AS hp                                            " +
                                      $"         where hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') )              " +
                                      $"       ) u                                                                                              " +
                                      $"       INNER JOIN {PamcPortalDatabase}..INSURANCE_CLIENTS IC ON CONVERT(VARCHAR,IC.HPCODE) = u.HPCODE   " +
                                      $" WHERE USERNAME = '{batchNo.hpCode}'                                                                    ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }
                    hpname = new string[tbl.Rows.Count];
                    int counter = 0;
                    foreach (DataRow drow in tbl.Rows)
                    {
                        hpname[counter] = drow["HPNAME"].ToString();
                        counter++;
                    }

                    //sql = $"SELECT DESCR FROM PAYBATCH  WHERE BATCH = '{batchNo.batchNumber}'";  //Jaco comment out 2023-09-14
                    //cmd.CommandText = sql;  //Jaco comment out 2023-09-14
                    cmd.CommandText = $"SELECT DESCR FROM PAYBATCH  WHERE BATCH = '{batchNo.batchNumber}'"; //Jaco 2023-09-14

                    string temp = "";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        data.Load(dr);
                    }
                    counter = 0;
                    foreach (DataRow drow in data.Rows)
                    {
                        temp = drow["DESCR"].ToString();
                    }

                    foreach (string name in hpname)
                    {
                        if (temp.Contains(name))
                        {
                            cmd.CommandText = $"SELECT HPCODE FROM {PamcPortalDatabase}..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR,HPNAME) = '{name}'";
                            break;
                        }
                    }

                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        hptabel.Load(rd);
                    }

                    foreach (DataRow roww in hptabel.Rows)
                    {
                        hpcode = roww["HPCODE"].ToString();
                    }

                    dtabel.Clear();
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :HPCODE- {batchNo.hpCode}..... VIEWSPECIFICATION- {batchNo.whereClause}---\r\n";

                    bool submit = false;
                    //sql = GetBrExtractSql(batchNo.batchNumber, submit, hpcode); // Jaco comment out 2023-09-14 

                    /*cmd.CommandText = "declare @prefix varchar(10)"
                                      + $"select @prefix = isnull(" + PamcPortalDatabase + $".dbo.GetMemberPrefix('{hpcode}'), '')\n"
                                      + "SELECT RPT_CLAIMS_BR.PAY,TBLROWID,RPT_CLAIMS_BR.[Main Policy Number (policy owner)], RPT_CLAIMS_BR.[Principle / Main Member number (applicable to schemes)], RPT_CLAIMS_BR.[Name & Surname of principle member / policy holder], \n"
                                      + " RPT_CLAIMS_BR.[ID number of member / policy holder], RPT_CLAIMS_BR.[Claim Type], RPT_CLAIMS_BR.PROVID, RPT_CLAIMS_BR.SPECIALTY, RPT_CLAIMS_BR.[PROVIDER NAME], \n"
                                      + " RPT_CLAIMS_BR.REFPROVID, RPT_CLAIMS_BR.ReqSpec, RPT_CLAIMS_BR.[Product Description], RPT_CLAIMS_BR.[Product Code], RPT_CLAIMS_BR.[Claim Number], \n"
                                      + " RPT_CLAIMS_BR.[Date of Incident], RPT_CLAIMS_BR.[Name & Surname of deceased / patient], RPT_CLAIMS_BR.[ID number of deceased / patient], \n"
                                      + " RPT_CLAIMS_BR.[Date of claim reported / received], RPT_CLAIMS_BR.[Amount of claim reported], RPT_CLAIMS_BR.[Amount of claim approved], RPT_CLAIMS_BR.[Date of claim paid], \n"
                                      + " RPT_CLAIMS_BR.[PAYMENT BATCH NO], RPT_CLAIMS_BR.[Amount of claim paid], RPT_CLAIMS_BR.[Claim Status], RPT_CLAIMS_BR.[Claim amount outstanding], \n"
                                      + " RPT_CLAIMS_BR.[Bank Account Name], RPT_CLAIMS_BR.[Bank Account Branch Code], RPT_CLAIMS_BR.[Bank Account Number], RPT_CLAIMS_BR.ReasonCode1, \n"
                                      + " RPT_CLAIMS_BR.ReasonCode2, RPT_CLAIMS_BR.OPT AS 'Option', RPT_CLAIMS_BR.ReqSpecDescription, RPT_CLAIMS_BR.Patient, RPT_CLAIMS_BR.PatientFirstNm, RPT_CLAIMS_BR.PatientBirth,RPT_CLAIMS_BR.HPCODE,RPT_CLAIMS_BR.SPEC,"
                                      + " RPT_CLAIMS_BR.ROWID,  PAYBATCH_CLAIM.EXCLUDED, PAYBATCH_CLAIM.VALIDATED, PAYBATCH_CLAIM.REJECTED,\n"
                                      + " CASE WHEN @prefix = '' THEN RPT_CLAIMS_BR.Patient ELSE REPLACE(RPT_CLAIMS_BR.Patient, @prefix, '') END AS MEMB,\n "
                                      + " CASE WHEN  @prefix = ''  THEN RPT_CLAIMS_BR.[Main Policy Number (policy owner)]\n"
                                      + " ELSE REPLACE(RPT_CLAIMS_BR.[Main Policy Number (policy owner)],\n"
                                      + " @prefix , '') END AS FAMILY\n"
                                      + "FROM RPT_CLAIMS_BR AS RPT_CLAIMS_BR INNER JOIN\n"
                                      + " PAYBATCH_CLAIM ON RPT_CLAIMS_BR.[Claim Number] = PAYBATCH_CLAIM.CLAIMNO\n"
                                      + "INNER JOIN PAYBATCH ON PAYBATCH_CLAIM.BATCH = PAYBATCH.BATCH "
                                      + $"WHERE (PAYBATCH_CLAIM.BATCH = {batchNo.batchNumber}) {whereClause}\r\n" +
                                      $"ORDER BY RPT_CLAIMS_BR.[Claim Number],RPT_CLAIMS_BR.TBLROWID";*/

                    //cmd.CommandText = sql; // Jaco comment out 2023-09-14
                    cmd.CommandText = GetBrExtractSql(batchNo.batchNumber, submit, hpcode, DRCDatabase, ReportingDatabase); // Jaco 2023-09-14
                    cmd.CommandTimeout = 3600;
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :SQLStatement - {cmd.CommandText}---\r\n";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        CheckRunModel line = new CheckRunModel();
                        line.rowId = (row["TBLROWID"]).ToString();
                        line.mainPolicyNumber = (row["FAMILY"]).ToString();
                        line.principleMainMemberNumber = (row["Principle / Main Member number (applicable to schemes)"]).ToString();
                        line.NameSurnamePrincipleMember = (row["Name & Surname of principle member / policy holder"]).ToString();
                        line.idNumberofMember = (row["ID number of member / policy holder"]).ToString();
                        line.claimType = (row["Claim Type"]).ToString();
                        line.provId = (row["PROVID"]).ToString();
                        line.speciality = (row["SPECIALTY"]).ToString();
                        line.providerName = (row["PROVIDER NAME"]).ToString();
                        line.refProvId = (row["REFPROVID"]).ToString();
                        line.reqSpec = (row["SPEC"]).ToString();
                        line.productDescription = (row["Product Description"]).ToString();
                        line.productCode = (row["Product Code"]).ToString();
                        line.claimNumber = (row["Claim Number"]).ToString();
                        DateTime dtRec = Convert.ToDateTime(row["Date of Incident"]);
                        line.dateofIncident = dtRec.ToString("yyyy/MM/dd");
                        line.namSurnameofdeceased = (row["Name & Surname of deceased / patient"]).ToString();
                        line.idNumberofDeceased = (row["ID number of deceased / patient"]).ToString();
                        line.dateOfClaimReceived = (row["Date of claim reported / received"]).ToString();
                        line.amountOfClaimReported = (row["Amount of claim reported"]).ToString();
                        line.amountOfClaimApproved = (row["Amount of claim approved"]).ToString();
                        line.dateOfClaimPaid = (row["Date of claim paid"]).ToString();
                        line.paymentBatchNo = (row["PAYMENT BATCH NO"]).ToString();
                        line.amountOfClaimPaid = (row["Amount of claim paid"]).ToString();
                        line.claimStatus = (row["Claim Status"]).ToString();
                        line.claimAmountOutstanding = (row["Claim amount outstanding"]).ToString();
                        line.bankAccountName = (row["Bank Account Name"]).ToString();
                        line.bankAccountBranchCode = (row["Bank Account Branch Code"]).ToString();
                        line.bankAccountNumber = (row["Bank Account Number"]).ToString();
                        line.reasonCode1 = (row["ReasonCode1"]).ToString();
                        line.reasonCode2 = (row["ReasonCode2"]).ToString();
                        line.option = (row["OPT"]).ToString();
                        line.reqSpecDescription = (row["SPECIALTY"]).ToString();
                        line.patientBirth = (row["PatientBirth"].ToString());
                        line.patientMembNo = (row["MEMB"].ToString());
                        line.patientName = (row["PatientFirstNm"].ToString());
                        line.type = (row["PAY"].ToString());


                        boardRowList.Add(line);

                    }
                    dt.Clear();
                    cn.Close();
                }
                catch (Exception e)
                {
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} : Error during Get/Generate Data - {e.Message}\r\n{e.StackTrace}\r\n";
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :general Error - {e.Message}\r\n{e.StackTrace}\r\n";
                throw;
            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----End : BordRowCreation---------------\r\n";
            }

            return boardRowList;
        }

        //Generates a summarized version of the checkrun per recieved Date
        [HttpPost]
        [Route("GetClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsModel> GetClaims([FromBody] ClaimsModel dataToFilter)
        {
            List<ClaimsModel> claimsToReturned = new List<ClaimsModel>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string _whereClause = "";
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            var tempEndDate = Convert.ToDateTime(dataToFilter.endDate).ToString("yyyy/MM/dd");
            cmd.Connection = cn;
            cmd.CommandTimeout = 3600;
            cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{dataToFilter.user}'";
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    _whereClause = dr["specifications"].ToString();
                    if (_whereClause.Length == 0)
                    {
                        _whereClause = "1=1";
                    }
                }
                dt.Clear();
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }
            /*  string sql = "SELECT PROV_SPECCODES.CODE AS CODE,PROV_SPECCODES.DESCR, COUNT(*) AS ClaimCount, SUM(cm.NET) AS NetPayment, cm.CLAIMTYPE\n"
             + "FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "LEFT OUTER JOIN\n"
             + "(\n"
             + "       SELECT CM.CLAIMNO FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "       PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "       INNER JOIN \n"
             + "       (\n"
             + "             SELECT CM.VENDOR, SUM(CM.NET) AS AMOUNT\n"
             + "             FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "             PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "             WHERE (cm.STATUS = '1' OR\n"
             + $"             cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE =  '{dataToFilter.hpCode}') AND (cm.DATERECD <= '{tempEndDate}') AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
             + "             GROUP BY CM.VENDOR\n"
             + "             HAVING SUM(CM.NET) < 0\n"
             + "       ) NV ON NV.VENDOR = CM.VENDOR\n"
             + "       WHERE (cm.STATUS = '1' OR\n"
             + $"       cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE = '{dataToFilter.hpCode}') AND (cm.DATERECD <= '{tempEndDate}')  AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
             + ")IGNORE ON IGNORE.CLAIMNO = CM.CLAIMNO\n"
             + "WHERE (cm.STATUS = '1' OR\n"
             + $"cm.STATUS = '5') AND  ({_whereClause})  " +
             $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
             $"AND (cm.DATERECD <= '{tempEndDate}') " +
             $"AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
             + "AND IGNORE.CLAIMNO IS  NULL\n"
             + "AND Get_Claims_Checkrun_1.CLAIMNO IS NULL\n"
             + "GROUP BY PROV_SPECCODES.DESCR, cm.CLAIMTYPE,CODE \n"
             + "ORDER BY DESCR";*/

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            string sql = "SELECT PROV_SPECCODES.CODE, \n" // Jaco 2023-11-24 tested with latest received date 2023-10-01
           + "       PROV_SPECCODES.DESCR, \n"
           + "       COUNT(*) AS ClaimCount, \n"
           + "       SUM(cm.NET) AS NetPayment, \n"
           + "       cm.CLAIMTYPE,cm.CLAIMNO\n"
           + "FROM CLAIM_MASTERS AS cm\n"
           + "     INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
           + "     \n"
           + $"	 LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}','{prefix}') AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
           + "    \n"
           + $"	 LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{dataToFilter.hpCode}', '{tempEndDate}', '{dataToFilter.claimType}', NULL, NULL, NULL, NULL, NULL) AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
           + "\n"
           + "WHERE(cm.STATUS = '1' OR\n"
           + $"cm.STATUS = '5') AND  ({_whereClause})  " +
           $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
           $"AND (cm.DATERECD <= '{tempEndDate}') " +
           $"AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
           + "     AND IGNORE.CLAIMNO IS NULL\n"
           + "	  AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)\n"
           + "GROUP BY PROV_SPECCODES.DESCR, \n"
           + "         cm.CLAIMTYPE, \n"
           + "         CODE,\n"
           + "		 cm.CLAIMNO\n"
           + "ORDER BY DESCR;\n"
           + "\n"
           + "\n"
           + "";
            cmd.CommandText = sql;
            cmd.CommandTimeout = 3600;

            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow dr in dt.Rows)
                {
                    ClaimsModel returnedAmounts = new ClaimsModel();
                    returnedAmounts.specCode = (dr["CODE"]).ToString();
                    returnedAmounts.specCodeDesc = (dr["DESCR"]).ToString();
                    returnedAmounts.sum = (dr["NetPayment"]).ToString();
                    returnedAmounts.amountOfRecords = (dr["ClaimCount"]).ToString();
                    returnedAmounts.claimType = (dr["CLAIMTYPE"]).ToString();
                    claimsToReturned.Add(returnedAmounts);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            dt.Reset();
            return claimsToReturned;
        }

        //Generates a summarized view of the checkrun  per provider no
        [HttpPost]
        [Route("GetProvClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsModel> GetProvClaims([FromBody] ClaimsModel dataToFilter)
        {
            List<ClaimsModel> claimsToReturned = new List<ClaimsModel>();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string _whereClause = "";
            cn.ConnectionString = _drcConnectionString;
            string provId;
            provId = dataToFilter.provid;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            cmd.CommandTimeout = 3600;
            cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{dataToFilter.user}'";
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    _whereClause = dr["specifications"].ToString();
                    if (_whereClause.Length == 0)
                    {
                        _whereClause = "1=1";
                    }
                }
                dt.Clear();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            string sql = "SELECT PROV_SPECCODES.CODE, \n" // Jaco 2023-11-24 tested with vendor 0359033
                       + "       PROV_SPECCODES.DESCR, \n"
                       + "       COUNT(*) AS ClaimCount, \n"
                       + "       SUM(cm.NET) AS NetPayment, \n"
                       + "       cm.CLAIMTYPE,cm.CLAIMNO\n"
                       + "FROM CLAIM_MASTERS AS cm\n"
                       + "     INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
                       + "     \n"
                       + $"	 LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
                       + "    \n"
                       + $"	 LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{dataToFilter.hpCode}', NULL, '{dataToFilter.claimType}', '{provId}', NULL, NULL, NULL, NULL) AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
                       + "\n"
                       + "WHERE(cm.STATUS = '1' OR\n"
                       + $"cm.STATUS = '5') AND  ({_whereClause})  " +
                         $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
                         $"AND (cm.PROVID = '{provId}') " +
                         $"AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
                       + "     AND IGNORE.CLAIMNO IS NULL\n"
                       + "	  AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)\n"
                       + "GROUP BY PROV_SPECCODES.DESCR, \n"
                       + "         cm.CLAIMTYPE, \n"
                       + "         CODE,\n"
                       + "		 cm.CLAIMNO\n"
                       + "ORDER BY DESCR;\n"
                       + "\n"
                       + "\n"
                       + "";
            /* string sql = "SELECT PROV_SPECCODES.CODE AS CODE,PROV_SPECCODES.DESCR, COUNT(*) AS ClaimCount, SUM(cm.NET) AS NetPayment, cm.CLAIMTYPE\n"
            + "FROM CLAIM_MASTERS AS cm INNER JOIN\n"
            + "PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
            + "LEFT OUTER JOIN\n"
            + "(\n"
            + "       SELECT CM.CLAIMNO FROM CLAIM_MASTERS AS cm INNER JOIN\n"
            + "       PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
            + "       INNER JOIN \n"
            + "       (\n"
            + "             SELECT CM.VENDOR, SUM(CM.NET) AS AMOUNT\n"
            + "             FROM CLAIM_MASTERS AS cm INNER JOIN\n"
            + "             PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
            + "             WHERE (cm.STATUS = '1' OR\n"
            + $"             cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE =  '{dataToFilter.hpCode}') AND (cm.CLAIMTYPE = '{dataToFilter.claimType}') AND (cm.PROVID = '{provId}')\n"
            + "             GROUP BY CM.VENDOR\n"
            + "             HAVING SUM(CM.NET) < 0\n"
            + "       ) NV ON NV.VENDOR = CM.VENDOR\n"
            + "       WHERE (cm.STATUS = '1' OR\n"
            + $"       cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE = '{dataToFilter.hpCode}')   AND (cm.CLAIMTYPE = '{dataToFilter.claimType}') AND (cm.PROVID = '{provId}')\n"
            + ")IGNORE ON IGNORE.CLAIMNO = CM.CLAIMNO\n"
            + "WHERE (cm.STATUS = '1' OR\n"
            + $"cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE = '{dataToFilter.hpCode}') AND (cm.CLAIMTYPE = '{dataToFilter.claimType}') AND (cm.PROVID = '{provId}')\n"
            + "AND IGNORE.CLAIMNO IS  NULL\n"
            + "GROUP BY PROV_SPECCODES.DESCR, cm.CLAIMTYPE,CODE \n"
            + "ORDER BY DESCR";*/
            cmd.CommandText = sql;
            cmd.CommandTimeout = 3600;

            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow dr in dtable.Rows)
                {
                    ClaimsModel returnedAmounts = new ClaimsModel();
                    returnedAmounts.specCode = (dr["CODE"]).ToString();
                    returnedAmounts.specCodeDesc = (dr["DESCR"]).ToString();
                    returnedAmounts.sum = (dr["NetPayment"]).ToString();
                    returnedAmounts.amountOfRecords = (dr["ClaimCount"]).ToString();
                    returnedAmounts.claimType = (dr["CLAIMTYPE"]).ToString();
                    claimsToReturned.Add(returnedAmounts);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(e.Message, e.StackTrace);
            }
            cn.Close();
            return claimsToReturned;
        }

        //Generates a summarized view of the checkrun per member no
        [HttpPost]
        [Route("GetMembClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsModel> GetMembClaims([FromBody] ClaimsModel dataToFilter)
        {
            List<ClaimsModel> claimsToReturned = new List<ClaimsModel>();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string _whereClause = "";
            cn.ConnectionString = _drcConnectionString;
            string provId;
            provId = dataToFilter.provid;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.CommandTimeout = 3600;
            cmd.Connection = cn;
            cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{dataToFilter.user}'";
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    _whereClause = dr["specifications"].ToString();
                    if (_whereClause.Length == 0)
                    {
                        _whereClause = "1=1";
                    }
                }
                dt.Clear();
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(e.Message, e.StackTrace);
            }
            cmd.CommandText = "declare @membid varchar(20)\n" // Jaco 2023-11-24 tested with membid ESG-100662-01
                       + "declare @hpCode varchar(10)\n"
                       + "declare @prefix varchar(10)\n"
                       + "\n"
                       + $"set @hpCode= '{dataToFilter.hpCode}'\n"
                       + $"set @membid = '{dataToFilter.membid}'\n"

                       // Jaco comment out 2023-09-28 - Databases is hardcoded in GetMemberPrefix function
                       //+ "set @prefix = " + this.PamcPortalDatabase + $".dbo.GetMemberPrefix(@hpCode)\n"
                                              
                       // Jaco 2023-09-28
                       + $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n"
                       + $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n"
                       + $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n"
                       + $" where OxygenBenefitOptions.ezHpCode = @hpCode AND OxygenBenefitOptions.scheme <> 'carecross' \n"

                       + "set @membid = " + this.PamcPortalDatabase + $".dbo.RemoveDependantDash(@membid)\n"
                       + "set @membid = " + this.PamcPortalDatabase + $".dbo.GenerateSubSsn(@prefix,@membid)\n"
                       + "\n"
                       + "SELECT PROV_SPECCODES.CODE, \n"
                     + "       PROV_SPECCODES.DESCR, \n"
                     + "       COUNT(*) AS ClaimCount, \n"
                     + "       SUM(cm.NET) AS NetPayment, \n"
                     + "       cm.CLAIMTYPE,cm.CLAIMNO\n"
                     + "FROM CLAIM_MASTERS AS cm\n"
                     + "     INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
                     + "     \n"
                     + $"	 LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', @prefix) AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
                     + "    \n"
                     + $"	 LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{dataToFilter.hpCode}', NULL, '{dataToFilter.claimType}', NULL, @membid, NULL, NULL, NULL) AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
                     + "\n"
                     + "WHERE(cm.STATUS = '1' OR\n"
                     + $"cm.STATUS = '5') AND  ({_whereClause})  " +
                       $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
                       $"AND (cm.SUBSSN= @membid) " +
                       $"AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
                     + "     AND IGNORE.CLAIMNO IS NULL \n"
                     + "	  AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)\n"
                     + "GROUP BY PROV_SPECCODES.DESCR, \n"
                     + "         cm.CLAIMTYPE, \n"
                     + "         CODE,\n"
                     + "		 cm.CLAIMNO\n"
                     + "ORDER BY DESCR;\n"
                     + "\n"
                     + "\n"
                     + "";


            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow dr in dtable.Rows)
                {
                    ClaimsModel returnedAmounts = new ClaimsModel();
                    returnedAmounts.specCode = (dr["CODE"]).ToString();
                    returnedAmounts.specCodeDesc = (dr["DESCR"]).ToString();
                    returnedAmounts.sum = (dr["NetPayment"]).ToString();
                    returnedAmounts.amountOfRecords = (dr["ClaimCount"]).ToString();
                    returnedAmounts.claimType = (dr["CLAIMTYPE"]).ToString();
                    claimsToReturned.Add(returnedAmounts);
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return claimsToReturned;
        }

        //Generates a summarized view of the checkrun per claim no
        [HttpPost]
        [Route("GetClaimsNo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsModel> GetClaimsNo([FromBody] ClaimsModel dataToFilter)
        {
            List<ClaimsModel> claimsToReturned = new List<ClaimsModel>();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string _whereClause = "";
            cn.ConnectionString = _drcConnectionString;
            string provId;
            provId = dataToFilter.provid;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.CommandTimeout = 3600;
            cmd.Connection = cn;
            cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{dataToFilter.user}'";
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    _whereClause = dr["specifications"].ToString();
                    if (_whereClause.Length == 0)
                    {
                        _whereClause = "1=1";
                    }
                }
                dt.Clear();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            cmd.CommandText = "SELECT PROV_SPECCODES.CODE, \n" //Jaco 2023-11-24 tested with claimno 2023100399905033
           + "       PROV_SPECCODES.DESCR, \n"
           + "       COUNT(*) AS ClaimCount, \n"
           + "       SUM(cm.NET) AS NetPayment, \n"
           + "       cm.CLAIMTYPE,cm.CLAIMNO\n"
           + "FROM CLAIM_MASTERS AS cm\n"
           + "     INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
           + "     \n"
           + $"	 LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
           + "    \n"
           + $"	 LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{dataToFilter.hpCode}', NULL, NULL, NULL, NULL, '{dataToFilter.claimno}', NULL, NULL) AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
           + "\n"
           + "WHERE(cm.STATUS = '1' OR\n"
           + $"cm.STATUS = '5') AND  ({_whereClause})  " +
             $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
             $"AND (cm.CLAIMNO= '{dataToFilter.claimno}') "
           + "     AND IGNORE.CLAIMNO IS NULL\n"
           + "	  AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)\n"
           + "GROUP BY PROV_SPECCODES.DESCR, \n"
           + "         cm.CLAIMTYPE, \n"
           + "         CODE,\n"
           + "		 cm.CLAIMNO\n"
           + "ORDER BY DESCR;\n"
           + "\n"
           + "\n"
           + "";
            /*  string sql = "SELECT PROV_SPECCODES.CODE AS CODE,PROV_SPECCODES.DESCR, COUNT(*) AS ClaimCount, SUM(cm.NET) AS NetPayment, cm.CLAIMTYPE\n"
             + "FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "LEFT OUTER JOIN\n"
             + "(\n"
             + "       SELECT CM.CLAIMNO FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "       PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "       INNER JOIN \n"
             + "       (\n"
             + "             SELECT CM.VENDOR, SUM(CM.NET) AS AMOUNT\n"
             + "             FROM CLAIM_MASTERS AS cm INNER JOIN\n"
             + "             PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
             + "             WHERE (cm.STATUS = '1' OR\n"
             + $"             cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE =  '{dataToFilter.hpCode}') AND (cm.CLAIMNO= '{dataToFilter.claimno}')\n"
             + "             GROUP BY CM.VENDOR\n"
             + "             HAVING SUM(CM.NET) < 0\n"
             + "       ) NV ON NV.VENDOR = CM.VENDOR\n"
             + "       WHERE (cm.STATUS = '1' OR\n"
             + $"       cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE = '{dataToFilter.hpCode}')   AND (cm.CLAIMNO= '{dataToFilter.claimno}')\n"
             + ")IGNORE ON IGNORE.CLAIMNO = CM.CLAIMNO\n"
             + "WHERE (cm.STATUS = '1' OR\n"
             + $"cm.STATUS = '5') AND  ({_whereClause})  AND (cm.HPCODE = '{dataToFilter.hpCode}')  AND (cm.CLAIMNO= '{dataToFilter.claimno}')\n"
             + "AND IGNORE.CLAIMNO IS  NULL\n"
             + "GROUP BY PROV_SPECCODES.DESCR, cm.CLAIMTYPE,CODE \n"
             + "ORDER BY DESCR";
              cmd.CommandText = sql;
              cmd.CommandTimeout = 180;*/

            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow dr in dtable.Rows)
                {
                    ClaimsModel returnedAmounts = new ClaimsModel();
                    returnedAmounts.specCode = (dr["CODE"]).ToString();
                    returnedAmounts.specCodeDesc = (dr["DESCR"]).ToString();
                    returnedAmounts.sum = (dr["NetPayment"]).ToString();
                    returnedAmounts.amountOfRecords = (dr["ClaimCount"]).ToString();
                    returnedAmounts.claimType = (dr["CLAIMTYPE"]).ToString();
                    claimsToReturned.Add(returnedAmounts);
                }

                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return claimsToReturned;
        }

        [HttpPost]
        [Route("GetClaimsService")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsModel> GetClaimsService([FromBody] ClaimsModel dataToFilter)
        {
            List<ClaimsModel> claimsToReturn = new List<ClaimsModel>();
            List<ClaimsModel> claimsToReturned = new List<ClaimsModel>();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string _whereClause = "";
            string tempdate = "";

            cn.ConnectionString = _drcConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;
                cmd.CommandTimeout = 3600;

                cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{dataToFilter.user}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);

                }
                foreach (DataRow row in dt.Rows)
                {
                    _whereClause = row["specifications"].ToString();
                    if (_whereClause.Length == 0)
                    {
                        _whereClause = "1=1";
                    }
                }

                tempdate = dataToFilter.serviceToDate;

                // Jaco 2023-11-24
                string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

                string sql = "SELECT PROV_SPECCODES.CODE, \n" // Jaco 2023-11-24 tested with service date 2023/10/01
           + "       PROV_SPECCODES.DESCR, \n"
           + "       COUNT(*) AS ClaimCount, \n"
           + "       SUM(cm.NET) AS NetPayment, \n"
           + "       cm.CLAIMTYPE,cm.CLAIMNO\n"
           + "FROM CLAIM_MASTERS AS cm\n"
           + "     INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
           + "     \n"
           + $"	 LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
           + "    \n"
           + $"	 LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{dataToFilter.hpCode}', NULL, '{dataToFilter.claimType}', NULL, NULL, NULL, '{tempdate}', NULL) AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
           + "\n"
           + "WHERE(cm.STATUS = '1' OR\n"
           + $"cm.STATUS = '5') AND  ({_whereClause})  " +
             $"AND (cm.HPCODE = '{dataToFilter.hpCode}') " +
             $"AND (ISNULL(cm.DATETO, cm.DATEFROM) <= '{tempdate}') " +
             $"AND (cm.CLAIMTYPE = '{dataToFilter.claimType}')\n"
           + "     AND IGNORE.CLAIMNO IS NULL\n"
           + "	  AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)\n"
           + "GROUP BY PROV_SPECCODES.DESCR, \n"
           + "         cm.CLAIMTYPE, \n"
           + "         CODE,\n"
           + "		 cm.CLAIMNO\n"
           + "ORDER BY DESCR;\n"
           + "\n"
           + "\n"
           + "";

                dt.Clear();
                cmd.CommandText = sql;
                cmd.CommandTimeout = 3600;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow dr in dtable.Rows)
                {
                    ClaimsModel returnedAmounts = new ClaimsModel();
                    returnedAmounts.specCode = (dr["CODE"]).ToString();
                    returnedAmounts.specCodeDesc = (dr["DESCR"]).ToString();
                    returnedAmounts.sum = (dr["NetPayment"]).ToString();
                    returnedAmounts.amountOfRecords = (dr["ClaimCount"]).ToString();
                    returnedAmounts.claimType = (dr["CLAIMTYPE"]).ToString();
                    claimsToReturned.Add(returnedAmounts);
                }
                dtable.Clear();
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                // throw;
            }


            return claimsToReturned;
        }

        [HttpPost]
        [Route("GetAllExcludedClaimsMembNo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcludedClaims> GetAllExcludedClaims([FromBody] ClaimsModel dataToFilter)
        {
            List<ExcludedClaims> returnedList = new List<ExcludedClaims>();

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();

            cn.ConnectionString = _drcConnectionString;

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            try
            {
                cmd.Connection = cn;

                cmd.CommandText = "declare @membid varchar(20)\n" // Jaco 2023-11-24 tested with membid ESG-100662-01
                       + "declare @hpCode varchar(10)\n"
                       + "declare @prefix varchar(10)\n"
                       + "\n"
                       + $"set @hpCode= '{dataToFilter.hpCode}'\n"
                       + $"set @membid = '{dataToFilter.membid}'\n"
                       //+ "set @prefix = " + this.PamcPortalDatabase + $".dbo.GetMemberPrefix(@hpCode)\n"  //Jaco 2023-11-24 Databases hardcoded in  
                       + $"set @prefix = '{prefix}'\n"  //Jaco 2023-11-24 
                       + "set @membid = " + this.PamcPortalDatabase + $".dbo.RemoveDependantDash(@membid)\n"
                       + "\n"
                       + $"SELECT * FROM dbo.Get_Claims_Checkrun_new(@hpCode, @prefix) WHERE FAMILYNO = @membid AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ExcludedClaims claim = new ExcludedClaims();

                    claim.batchNo = row["BATCH"].ToString();
                    claim.claimNo = row["CLAIMNO"].ToString();
                    claim.validated = row["VALIDATED"].ToString();
                    claim.exluded = row["EXCLUDED"].ToString();
                    claim.rejected = row["REJECTED"].ToString();
                    returnedList.Add(claim);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                //throw;
            }



            return returnedList;
        }

        [HttpPost]
        [Route("GetAllExcludedClaimsClaimNo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcludedClaims> GetAllExcludedClaimsClaimNo([FromBody] ClaimsModel dataToFilter)
        {
            List<ExcludedClaims> returnedList = new List<ExcludedClaims>();

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();

            cn.ConnectionString = _drcConnectionString;

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            try
            {
                cmd.Connection = cn;

                // Jaco 2023-11-24 Databases hardcoded in dbo.Get_Claims_Checkrun function
                //cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun('{dataToFilter.hpCode}') WHERE CLAIMNO = '{dataToFilter.claimno}' AND EXCLUDED = 0";

                // Jaco 2023-11-24
                cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') WHERE CLAIMNO = '{dataToFilter.claimno}' AND EXCLUDED = 0"; // Jaco 2023-11-24 tested with claimno 2023100399905033

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ExcludedClaims claim = new ExcludedClaims();

                    claim.batchNo = row["BATCH"].ToString();
                    claim.claimNo = row["CLAIMNO"].ToString();
                    claim.validated = row["VALIDATED"].ToString();
                    claim.exluded = row["EXCLUDED"].ToString();
                    claim.rejected = row["REJECTED"].ToString();
                    returnedList.Add(claim);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                //throw;
            }



            return returnedList;
        }

        [HttpPost]
        [Route("GetAllExcludedClaimsProv")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcludedClaims> GetAllExcludedClaimsProv([FromBody] ClaimsModel dataToFilter)
        {
            List<ExcludedClaims> returnedList = new List<ExcludedClaims>();

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();

            cn.ConnectionString = _drcConnectionString;

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            try
            {
                cmd.Connection = cn;

                // Jaco 2023-11-24 Databases hardcoded in dbo.Get_Claims_Checkrun function
                //cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun('{dataToFilter.hpCode}') WHERE PROVID = '{dataToFilter.provid}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0";

                // Jaco 2023-11-24
                cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') WHERE PROVID = '{dataToFilter.provid}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0"; // Jaco 2023-11-24 tested with vendor 0359033

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ExcludedClaims claim = new ExcludedClaims();

                    claim.batchNo = row["BATCH"].ToString();
                    claim.claimNo = row["CLAIMNO"].ToString();
                    claim.validated = row["VALIDATED"].ToString();
                    claim.exluded = row["EXCLUDED"].ToString();
                    claim.rejected = row["REJECTED"].ToString();
                    returnedList.Add(claim);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                //throw;
            }

            return returnedList;
        }

        [HttpPost]
        [Route("GetAllExcludedClaimsRec")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcludedClaims> GetAllExcludedClaimsRec([FromBody] ClaimsModel dataToFilter)
        {
            List<ExcludedClaims> returnedList = new List<ExcludedClaims>();

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();

            cn.ConnectionString = _drcConnectionString;

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            try
            {
                cmd.Connection = cn;

                // Jaco 2023-11-24 Databases hardcoded in dbo.Get_Claims_Checkrun function
                //cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun('{dataToFilter.hpCode}') WHERE DATERECD <= '{Convert.ToDateTime(dataToFilter.endDate).ToString("yyyy-MM-dd")}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0";

                // Jaco 2023-11-24
                cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') WHERE DATERECD <= '{Convert.ToDateTime(dataToFilter.endDate).ToString("yyyy-MM-dd")}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0"; // Jaco tested with last receive date 2023-10-01

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ExcludedClaims claim = new ExcludedClaims();

                    claim.batchNo = row["BATCH"].ToString();
                    claim.claimNo = row["CLAIMNO"].ToString();
                    claim.validated = row["VALIDATED"].ToString();
                    claim.exluded = row["EXCLUDED"].ToString();
                    claim.rejected = row["REJECTED"].ToString();
                    returnedList.Add(claim);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                //throw;
            }

            return returnedList;
        }

        [HttpPost]
        [Route("GetAllExcludedClaimsSer")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcludedClaims> GetAllExcludedClaimsSer([FromBody] ClaimsModel dataToFilter)
        {
            List<ExcludedClaims> returnedList = new List<ExcludedClaims>();

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            DataTable dt = new DataTable();

            cn.ConnectionString = _drcConnectionString;

            // Jaco 2023-11-24
            string prefix = Utillity.GetMemberPrefix(cn, dataToFilter.hpCode, DRCDatabase, ReportingDatabase);

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            try
            {
                cmd.Connection = cn;

                // Jaco 2023-11-24 Databases hardcoded in dbo.Get_Claims_Checkrun function
                //cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun('{dataToFilter.hpCode}') WHERE SERVDATE <= '{Convert.ToDateTime(dataToFilter.serviceToDate).ToString("yyyy-MM-dd")}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0";

                // Jaco 2023-11-24
                cmd.CommandText = $"SELECT * FROM dbo.Get_Claims_Checkrun_new('{dataToFilter.hpCode}', '{prefix}') WHERE SERVDATE <= '{Convert.ToDateTime(dataToFilter.serviceToDate).ToString("yyyy-MM-dd")}' AND CLAIMTYPE = '{dataToFilter.claimType}' AND EXCLUDED = 0"; // Jaco 2023-11-24 tested with service date 2023/10/01

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ExcludedClaims claim = new ExcludedClaims();

                    claim.batchNo = row["BATCH"].ToString();
                    claim.claimNo = row["CLAIMNO"].ToString();
                    claim.validated = row["VALIDATED"].ToString();
                    claim.exluded = row["EXCLUDED"].ToString();
                    claim.rejected = row["REJECTED"].ToString();
                    returnedList.Add(claim);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                //throw;
            }



            return returnedList;
        }

        //Get the date of the oldest claim in the checkrunF
        [HttpPost]
        [Route("GetLastRecDate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetLastRecDate([FromBody] ClaimsModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString; ;
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            var tempEndDate = Convert.ToDateTime(data.endDate).ToString("yyyy/MM/dd");
            cmd.Connection = cn;
            cmd.CommandText = $"SELECT CONVERT(VARCHAR(10),MIN(DATERECD),111) FROM CLAIM_MASTERS WHERE (STATUS = '1' OR STATUS = '5') AND (HPCODE = '{data.hpCode}') AND (DATERECD <= '{tempEndDate}') ";
            cmd.CommandTimeout = 3600;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        if (DBNull.Value.Equals(dt.Rows[0][0]))
                        {
                            vr.message = "No Date Was Found!...Please Pick a Later Date";
                        }
                        else
                        {
                            vr.message = dt.Rows[0][0].ToString().Substring(0, 10);
                        }

                    }
                    else
                    {
                        vr.message = "No Date Was Found!...Please Pick a Later Date";
                    }
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            dt.Reset();
            return vr;
        }

        //generates a batch number when checkrun is sorted according to the last recieved date
        [HttpPost]
        [Route("GenerateBatchNumber")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GenerateBatchNumber([FromBody] ClaimsModel data)
        {
            string filterStatement = "";
            string batchNum;
            string clientName;
            string description;
            string createDate;
            string date = "";
            DataTable dt = new DataTable();
            DataTable dttbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _drcConnectionString;
            string[] where;
            string whereClause = "";
            int count = 0;
            cmd.CommandTimeout = 3600;

            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : BatchCreation---------------\r\n";
            try
            {
                date = Convert.ToDateTime(data.endDate).ToString("yyyy/MM/dd");
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                if (data.claimType == "P")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'P')";
                }
                if (data.claimType == "M")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }
                if (data.claimType == null)
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }

                cmd.Connection = cn;
                string sql = "declare @batchNumber varchar(18)\n"
           + "declare @counter int\n"
           + "declare @stringCounter varchar(3)\n"
           + "SELECT/*CONVERT(VARCHAR(10),GETDATE(),111) AS DATE, " + this.DRCDatabase + $".dbo.LOB_CODES.CODE, " + this.PamcPortalDatabase + $".dbo.Users.name, ROWID ,\n"
           + "REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) AS ID,*/\n"
           + "@batchNumber =  CONVERT(VARCHAR(10),GETDATE(),112)+REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) \n"
           + "FROM " + this.DRCDatabase + $".dbo.LOB_CODES INNER JOIN\n"
           + "" + this.PamcPortalDatabase + $".dbo.Users ON " + this.DRCDatabase + $".dbo.LOB_CODES.CODE = " + this.PamcPortalDatabase + $".dbo.Users.lobcode WHERE " + this.PamcPortalDatabase + $".dbo.Users.username = '{data.user}';\n"
           + "--print @batchNumber\n"
           + "SELECT @counter = COUNT(*)+1 FROM " + this.DRCDatabase + $"..PAYBATCH WHERE SUBSTRING(CONVERT(VARCHAR(18),BATCH ),1,11) = @batchNumber;\n"
           + "--print @counter \n"
           + "SELECT @stringCounter = REPLICATE('0',3  - LEN(@counter)) + CONVERT(VARCHAR(3),@counter);\n"
           + "SELECT @batchNumber+@stringCounter;";
                cmd.CommandText = sql;
                //cmd.ExecuteNonQuery();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    dt.Load(dr);
                    batchNum = dt.Rows[0][0].ToString();
                }

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----BatchNo {batchNum}---------------\r\n";
                dt.Clear();
                cmd.CommandText = $"SELECT HPNAME FROM " + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR," + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS.HPCODE) = '{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    clientName = dt.Rows[0][1].ToString();
                }
                dt.Clear();
                description = $"Web PayBatch Generated for {clientName}";
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----ClientName:{clientName}  Desc:{description}---------------\r\n";
                createDate = DateTime.Now.ToString("yyyy/MM/dd");
                cmd.CommandText = $"INSERT INTO " + this.DRCDatabase + $"..PAYBATCH (BATCH, DESCR, PAYREADY, PAYRUN, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE,BATCHTYPE,USERNAME) VALUES (CONVERT(decimal(18),{batchNum})," +
                    $"'{description}',0,0,999,CONVERT(DATE,'{createDate}'),999,CONVERT(DATE,'{createDate}'),'{data.claimType}','{data.user}')";

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Insert {cmd.CommandText}---------------\r\n";
                cmd.ExecuteNonQuery();
                try
                {
                    if ((data.whereClause.Length > 1) && (data.whereClause != null))
                    {


                        where = data.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;
                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"'{condition}', ";
                            }
                            else
                            {
                                whereClause = whereClause + $"'{condition}'";
                            }


                        }
                        whereClause = $"AND PROV_SPECCODES.CODE IN ({whereClause})";
                        //whereClause = $"AND ({whereClause})";
                        // cmd.ExecuteNonQuery();

                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----WhereCLause {whereClause}---------------\r\n";

                        cmd.CommandText = $"SELECT specifications FROM {PamcPortalDatabase}..Users WHERE username = '{data.user}'";
                        using (SqlDataReader drr = cmd.ExecuteReader())
                        {
                            dttbl.Load(drr);
                        }
                        string spefication = "";
                        foreach (DataRow drow in dttbl.Rows)
                        {
                            spefication = drow["specifications"].ToString();
                            if (spefication.Length == 0)
                            {
                                spefication = "1=1";
                            }

                        }

                        // Jaco 2023-11-24
                        string prefix = Utillity.GetMemberPrefix(cn, data.hpCode, DRCDatabase, ReportingDatabase);

                        cmd.CommandText = "INSERT INTO PAYBATCH_CLAIM\n"
           + "(BATCH, \n"
           + " CLAIMNO, \n"
           + " CREATEBY, \n"
           + " CREATEDATE, \n"
           + " LASTCHANGEBY, \n"
           + " LASTCHANGEDATE\n"
           + ")\n"
           + "       SELECT DISTINCT \n"
           + $"              {batchNum} , \n"
           + "              cm.CLAIMNO, \n"
           + "              999 , \n"
           + "              GETDATE() , \n"
           + "              999 , \n"
           + "              GETDATE() \n"
           + "       FROM CLAIM_MASTERS AS cm\n"
           + "            INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
           + $"	        LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{data.hpCode}', '{prefix}') \n"
           + "			AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
           + $"	        LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{data.hpCode}', '{date}', '{data.claimType}', NULL, NULL, NULL, NULL, NULL)\n"
           + "			 AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
           + "\n"
           + "       WHERE(cm.STATUS = '1'\n"
           + "             OR cm.STATUS = '5')\n"
           + $"            AND (cm.HPCODE = '{data.hpCode}')\n"
           + $"            AND (cm.DATERECD <= '{date}')\n"
           + $"            {filterStatement}\n"
           + $"            {whereClause}\n"
           + "            AND (IGNORE.CLAIMNO IS NULL)\n"
           + $"			AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND {spefication}";


                        cmd.CommandTimeout = 3600;
                        cmd.ExecuteNonQuery();

                    }

                    vr.message = batchNum;
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Batch INSER ERRor {e.Message}---------------\r\n";
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----STACKTRACE {e.StackTrace}---------------\r\n";
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----GEnreral Error {e.Message}---------------\r\n";
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----STACKTRACE {e.StackTrace}---------------\r\n";
                var msg = e.Message;
            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----End : BatchNOCreation---------------\r\n";
            }

            cn.Close();
            return vr;
        }

        //generates a batchnumber when the checkrun is sorted according to provider no
        [HttpPost]
        [Route("GenerateBatchNumberProvider")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GenerateBatchNumberProvider([FromBody] ClaimsModel data)
        {
            string filterStatement = "";
            string batchNum;
            string clientName;
            string description;
            string createDate;
            string date;
            DataTable dt = new DataTable();
            DataTable dttbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _drcConnectionString;
            string[] where;
            string whereClause_1 = "";
            string whereClause_2 = "";
            string whereClause = "";
            int count = 0;
            cmd.CommandTimeout = 3600;
            try
            {
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }

                if (data.claimType == "P")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'P')";
                }
                if (data.claimType == "M")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }
                if (data.claimType == null)
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }

                date = Convert.ToDateTime(data.endDate).ToString("yyyy/MM/dd");
                cmd.Connection = cn;
                string sql = "declare @batchNumber varchar(18)\n"
           + "declare @counter int\n"
           + "declare @stringCounter varchar(3)\n"
           + "SELECT/*CONVERT(VARCHAR(10),GETDATE(),111) AS DATE, " + this.DRCDatabase + $".dbo.LOB_CODES.CODE, " + this.PamcPortalDatabase + $".dbo.Users.name, ROWID ,\n"
           + "REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) AS ID,*/\n"
           + "@batchNumber =  CONVERT(VARCHAR(10),GETDATE(),112)+REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) \n"
           + "FROM " + this.DRCDatabase + $".dbo.LOB_CODES INNER JOIN\n"
           + "" + this.PamcPortalDatabase + $".dbo.Users ON " + this.DRCDatabase + $".dbo.LOB_CODES.CODE = " + this.PamcPortalDatabase + $".dbo.Users.lobcode WHERE " + this.PamcPortalDatabase + $".dbo.Users.username = '{data.user}';\n"
           + "--print @batchNumber\n"
           + "SELECT @counter = COUNT(*)+1 FROM " + this.DRCDatabase + $"..PAYBATCH WHERE SUBSTRING(CONVERT(VARCHAR(18),BATCH ),1,11) = @batchNumber;\n"
           + "--print @counter \n"
           + "SELECT @stringCounter = REPLICATE('0',3  - LEN(@counter)) + CONVERT(VARCHAR(3),@counter);\n"
           + "SELECT @batchNumber+@stringCounter;";
                cmd.CommandText = sql;
                //cmd.ExecuteNonQuery();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    dt.Load(dr);
                    batchNum = dt.Rows[0][0].ToString();
                }
                dt.Clear();
                cmd.CommandText = $"SELECT HPNAME FROM " + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR," + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS.HPCODE) = '{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    clientName = dt.Rows[0][1].ToString();
                }
                dt.Clear();
                description = $"Web PayBatch Generated for {clientName}";
                createDate = DateTime.Now.ToString("yyyy/MM/dd");
                cmd.CommandText = $"INSERT INTO " + this.DRCDatabase + $"..PAYBATCH (BATCH, DESCR, PAYREADY, PAYRUN, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE,BATCHTYPE,USERNAME) VALUES (CONVERT(decimal(18),{batchNum})," +
                    $"'{description}',0,0,999,CONVERT(DATE,'{createDate}'),999,CONVERT(DATE,'{createDate}'),'{data.claimType}','{data.user}')";
                cmd.ExecuteNonQuery();
                try
                {
                    if ((data.whereClause.Length > 1) && (data.whereClause != null))
                    {

                        where = data.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;


                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}' OR ";
                                whereClause_1 = whereClause_1 + $"PROV_SPECCODES_1.CODE = '{condition}' OR ";
                                whereClause_2 = whereClause_2 + $"PROV_SPECCODES_2.CODE = '{condition}' OR ";
                            }
                            else
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}'";
                                whereClause_1 = whereClause_1 + $"PROV_SPECCODES_1.CODE = '{condition}'";
                                whereClause_2 = whereClause_2 + $"PROV_SPECCODES_2.CODE = '{condition}'";
                            }


                        }
                        whereClause = $"AND ({whereClause})";
                        whereClause_1 = $"AND ({whereClause_1})";
                        whereClause_2 = $"AND ({whereClause_2})";

                        cmd.CommandText = $"SELECT specifications FROM {PamcPortalDatabase}..Users WHERE username = '{data.user}'";
                        using (SqlDataReader drr = cmd.ExecuteReader())
                        {
                            dttbl.Load(drr);
                        }
                        string spefication = "";
                        foreach (DataRow drow in dttbl.Rows)
                        {
                            spefication = drow["specifications"].ToString();
                            if (spefication.Length == 0)
                            {
                                spefication = "1=1";
                            }

                        }

                        // Jaco 2023-11-24
                        string prefix = Utillity.GetMemberPrefix(cn, data.hpCode, DRCDatabase, ReportingDatabase);

                        cmd.CommandText = "INSERT INTO PAYBATCH_CLAIM\n"
                        + "(BATCH, \n"
                        + " CLAIMNO, \n"
                        + " CREATEBY, \n"
                        + " CREATEDATE, \n"
                        + " LASTCHANGEBY, \n"
                        + " LASTCHANGEDATE\n"
                        + ")\n"
                        + "       SELECT DISTINCT \n"
                        + $"              {batchNum} , \n"
                        + "              cm.CLAIMNO, \n"
                        + "              999 , \n"
                        + "              GETDATE() , \n"
                        + "              999 , \n"
                        + "              GETDATE() \n"
                        + "       FROM CLAIM_MASTERS AS cm\n"
                        + "            INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
                        + $"	        LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{data.hpCode}', '{prefix}') \n"
                        + "			AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
                        + $"	        LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{data.hpCode}', NULL, '{data.claimType}', '{data.provid}', NULL, NULL, NULL, NULL)\n"
                        + "			 AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
                        + "\n"
                        + "       WHERE(cm.STATUS = '1'\n"
                        + "             OR cm.STATUS = '5')\n"
                        + $"            AND (cm.HPCODE = '{data.hpCode}')\n"
                        + $"             AND (cm.PROVID = '{data.provid}')\n"
                        + $"            {filterStatement}\n"
                        + $"            {whereClause}\n"
                        + "            AND (IGNORE.CLAIMNO IS NULL)\n"
                        + $"			AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND {spefication} AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)";



                    }

                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    vr.message = batchNum;

                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return vr;
        }

        //generates a batchnumber when the checkrun is sorted according to member no
        [HttpPost]
        [Route("GenerateBatchNumberMember")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GenerateBatchNumberMember([FromBody] ClaimsModel data)
        {
            string filterStatement = "";
            string batchNum;
            string clientName;
            string description;
            string createDate;
            string date;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            DataTable dttbl = new DataTable();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _drcConnectionString;
            string[] where;
            string whereClause_1 = "";
            string whereClause_2 = "";
            string whereClause = "";
            int count = 0;
            cmd.CommandTimeout = 3600;
            try
            {
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }

                if (data.claimType == "P")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'P')";
                }
                if (data.claimType == "M")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }
                if (data.claimType == null)
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }

                date = Convert.ToDateTime(data.endDate).ToString("yyyy/MM/dd");
                cmd.Connection = cn;
                string sql = "declare @batchNumber varchar(18)\n"
           + "declare @counter int\n"
           + "declare @stringCounter varchar(3)\n"
           + "SELECT/*CONVERT(VARCHAR(10),GETDATE(),111) AS DATE, " + this.DRCDatabase + $".dbo.LOB_CODES.CODE, " + this.PamcPortalDatabase + $".dbo.Users.name, ROWID ,\n"
           + "REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) AS ID,*/\n"
           + "@batchNumber =  CONVERT(VARCHAR(10),GETDATE(),112)+REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) \n"
           + "FROM " + this.DRCDatabase + $".dbo.LOB_CODES INNER JOIN\n"
           + "" + this.PamcPortalDatabase + $".dbo.Users ON " + this.DRCDatabase + $".dbo.LOB_CODES.CODE = " + this.PamcPortalDatabase + $".dbo.Users.lobcode WHERE " + this.PamcPortalDatabase + $".dbo.Users.username = '{data.user}';\n"
           + "--print @batchNumber\n"
           + "SELECT @counter = COUNT(*)+1 FROM " + this.DRCDatabase + $"..PAYBATCH WHERE SUBSTRING(CONVERT(VARCHAR(18),BATCH ),1,11) = @batchNumber;\n"
           + "--print @counter \n"
           + "SELECT @stringCounter = REPLICATE('0',3  - LEN(@counter)) + CONVERT(VARCHAR(3),@counter);\n"
           + "SELECT @batchNumber+@stringCounter;";
                cmd.CommandText = sql;
                //cmd.ExecuteNonQuery();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    dt.Load(dr);
                    batchNum = dt.Rows[0][0].ToString();
                }
                dt.Clear();
                cmd.CommandText = $"SELECT HPNAME FROM " + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR," + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS.HPCODE) = '{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    clientName = dt.Rows[0][1].ToString();
                }
                dt.Clear();
                description = $"Web PayBatch Generated for {clientName}";
                createDate = DateTime.Now.ToString("yyyy/MM/dd");
                cmd.CommandText = $"INSERT INTO " + this.DRCDatabase + $"..PAYBATCH (BATCH, DESCR, PAYREADY, PAYRUN, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE,BATCHTYPE,USERNAME) VALUES (CONVERT(decimal(18),{batchNum})," +
                    $"'{description}',0,0,999,CONVERT(DATE,'{createDate}'),999,CONVERT(DATE,'{createDate}'),'{data.claimType}','{data.user}')";
                cmd.ExecuteNonQuery();
                try
                {
                    if ((data.whereClause.Length > 1) && (data.whereClause != null))
                    {

                        where = data.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;


                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}' OR ";
                                whereClause_1 = whereClause_1 + $"PROV_SPECCODES_1.CODE = '{condition}' OR ";
                                whereClause_2 = whereClause_2 + $"PROV_SPECCODES_2.CODE = '{condition}' OR ";
                            }
                            else
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}'";
                                whereClause_1 = whereClause_1 + $"PROV_SPECCODES_1.CODE = '{condition}'";
                                whereClause_2 = whereClause_2 + $"PROV_SPECCODES_2.CODE = '{condition}'";
                            }


                        }
                        whereClause = $"AND ({whereClause})";
                        whereClause_1 = $"AND ({whereClause_1})";
                        whereClause_2 = $"AND ({whereClause_2})";

                        cmd.CommandText = $"SELECT specifications FROM {PamcPortalDatabase}..Users WHERE username = '{data.user}'";
                        using (SqlDataReader drr = cmd.ExecuteReader())
                        {
                            dttbl.Load(drr);
                        }
                        string spefication = "";
                        foreach (DataRow drow in dttbl.Rows)
                        {
                            spefication = drow["specifications"].ToString();
                            if (spefication.Length == 0)
                            {
                                spefication = "1=1";
                            }

                        }

                        // Jaco 2023-11-24
                        string prefix = Utillity.GetMemberPrefix(cn, data.hpCode, DRCDatabase, ReportingDatabase);

                        cmd.CommandText = "declare @membid varchar(20)\n"
                                  + "declare @hpCode varchar(10)\n"
                                  + "declare @prefix varchar(10)\n"
                                  + "\n"
                                  + $"set @hpCode= '{data.hpCode}'\n"
                                  + $"set @membid = '{data.membid}'\n"
                                  //+ "set @prefix = " + this.PamcPortalDatabase + $".dbo.GetMemberPrefix(@hpCode)\n" //Jaco 2023-11-24 Database hardcoded
                                  + $"set @prefix = '{prefix}' \n" //Jaco 2023-11-24
                                  + "set @membid = " + this.PamcPortalDatabase + $".dbo.RemoveDependantDash(@membid)\n"
                                  + "set @membid = " + this.PamcPortalDatabase + $".dbo.GenerateSubSsn(@prefix,@membid)\n"
                                  + "\n"
                                  + "INSERT INTO PAYBATCH_CLAIM\n"
                      + "(BATCH, \n"
                      + " CLAIMNO, \n"
                      + " CREATEBY, \n"
                      + " CREATEDATE, \n"
                      + " LASTCHANGEBY, \n"
                      + " LASTCHANGEDATE\n"
                      + ")\n"
                      + "       SELECT DISTINCT \n"
                      + $"              {batchNum} , \n"
                      + "              cm.CLAIMNO, \n"
                      + "              999 , \n"
                      + "              GETDATE() , \n"
                      + "              999 , \n"
                      + "              GETDATE() \n"
                      + "       FROM CLAIM_MASTERS AS cm\n"
                      + "            INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
                      + $"	        LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{data.hpCode}', '{prefix}') \n"
                      + "			AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
                      + $"	        LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{data.hpCode}', NULL, '{data.claimType}', NULL, @membid, NULL, NULL, NULL)\n"
                      + "			 AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
                      + "\n"
                      + "       WHERE(cm.STATUS = '1'\n"
                      + "             OR cm.STATUS = '5')\n"
                      + $"            AND (cm.HPCODE = '{data.hpCode}')\n"
                      + $"             AND (cm.SUBSSN = @membid)\n"
                      + $"            {filterStatement}\n"
                      + $"            {whereClause}\n"
                      + "            AND (IGNORE.CLAIMNO IS NULL)\n"
                      + $"			AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND {spefication} AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)";


                    }
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    vr.message = batchNum;
                    cn.Close();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return vr;
        }

        //generates a batchnumber when the checkrun is sorted according to claim no
        [HttpPost]
        [Route("GenerateBatchNumberClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GenerateBatchNumberClaim([FromBody] ClaimsModel data)
        {
            string batchNum;
            string clientName;
            string description;
            string createDate;
            string date;
            DataTable dt = new DataTable();
            DataTable tbl = new DataTable();
            DataTable dttbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _drcConnectionString;
            string[] where;
            string whereClause = "";
            int count = 0;
            cmd.CommandTimeout = 3600;
            try
            {

                date = Convert.ToDateTime(data.endDate).ToString("yyyy/MM/dd");
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                string sql = "declare @batchNumber varchar(18)\n"
           + "declare @counter int\n"
           + "declare @stringCounter varchar(3)\n"
           + "SELECT/*CONVERT(VARCHAR(10),GETDATE(),111) AS DATE, " + this.DRCDatabase + $".dbo.LOB_CODES.CODE, " + this.PamcPortalDatabase + $".dbo.Users.name, ROWID ,\n"
           + "REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) AS ID,*/\n"
           + "@batchNumber =  CONVERT(VARCHAR(10),GETDATE(),112)+REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) \n"
           + "FROM " + this.DRCDatabase + $".dbo.LOB_CODES INNER JOIN\n"
           + "" + this.PamcPortalDatabase + $".dbo.Users ON " + this.DRCDatabase + $".dbo.LOB_CODES.CODE = " + this.PamcPortalDatabase + $".dbo.Users.lobcode WHERE " + this.PamcPortalDatabase + $".dbo.Users.username = '{data.user}';\n"
           + "--print @batchNumber\n"
           + "SELECT @counter = COUNT(*)+1 FROM " + this.DRCDatabase + $"..PAYBATCH WHERE SUBSTRING(CONVERT(VARCHAR(18),BATCH ),1,11) = @batchNumber;\n"
           + "--print @counter \n"
           + "SELECT @stringCounter = REPLICATE('0',3  - LEN(@counter)) + CONVERT(VARCHAR(3),@counter);\n"
           + "SELECT @batchNumber+@stringCounter;";
                cmd.CommandText = sql;
                //cmd.ExecuteNonQuery();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    dt.Load(dr);
                    batchNum = dt.Rows[0][0].ToString();
                }
                dt.Clear();

                cmd.CommandText = $"SELECT CLAIMTYPE FROM {this.DRCDatabase}.dbo.CLAIM_MASTERS WHERE CLAIMNO = '{data.claimno}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    tbl.Load(dr);
                }
                foreach (DataRow rows in tbl.Rows)
                {
                    data.claimType = rows["CLAIMTYPE"].ToString();
                }

                cmd.CommandText = $"SELECT HPNAME FROM " + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR," + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS.HPCODE) = '{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    clientName = dt.Rows[0][1].ToString();
                }
                dt.Clear();
                description = $"Web PayBatch Generated for {clientName}";
                createDate = DateTime.Now.ToString("yyyy/MM/dd");
                cmd.CommandText = $"INSERT INTO " + this.DRCDatabase + $"..PAYBATCH (BATCH, DESCR, PAYREADY, PAYRUN, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE,BATCHTYPE, USERNAME) VALUES (CONVERT(decimal(18),{batchNum})," +
                    $"'{description}',0,0,999,CONVERT(DATE,'{createDate}'),999,CONVERT(DATE,'{createDate}'),'{data.claimType}','{data.user}')";
                cmd.ExecuteNonQuery();
                try
                {
                    if ((data.whereClause.Length > 1) && (data.whereClause != null))
                    {

                        where = data.whereClause.Split(',');
                        foreach (string condition in where)
                        {
                            count++;


                            if (count < where.Length)
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}' OR ";

                            }
                            else
                            {
                                whereClause = whereClause + $"PROV_SPECCODES.CODE = '{condition}'";
                            }


                        }
                        whereClause = $"AND ({whereClause})";

                        cmd.CommandText = $"SELECT specifications FROM {PamcPortalDatabase}..Users WHERE username = '{data.user}'";
                        using (SqlDataReader drr = cmd.ExecuteReader())
                        {
                            dttbl.Load(drr);
                        }
                        string spefication = "";
                        foreach (DataRow drow in dttbl.Rows)
                        {
                            spefication = drow["specifications"].ToString();
                            if (spefication.Length == 0)
                            {
                                spefication = "1=1";
                            }

                        }

                        // Jaco 2023-11-24
                        string prefix = Utillity.GetMemberPrefix(cn, data.hpCode, DRCDatabase, ReportingDatabase); 

                        cmd.CommandText = "INSERT INTO PAYBATCH_CLAIM\n"  // Jaco 2023-11-24 tested
                      + "(BATCH, \n"
                      + " CLAIMNO, \n"
                      + " CREATEBY, \n"
                      + " CREATEDATE, \n"
                      + " LASTCHANGEBY, \n"
                      + " LASTCHANGEDATE\n"
                      + ")\n"
                      + "       SELECT DISTINCT \n"
                      + $"              {batchNum} , \n"
                      + "              cm.CLAIMNO, \n"
                      + "              999 , \n"
                      + "              GETDATE() , \n"
                      + "              999 , \n"
                      + "              GETDATE() \n"
                      + "       FROM CLAIM_MASTERS AS cm\n"
                      + "            INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
                      + $"	        LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{data.hpCode}', '{prefix}') \n"
                      + "			AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
                      + $"	        LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{data.hpCode}', NULL, NULL, NULL, NULL,'{data.claimno}', NULL, NULL)\n"
                      + "			 AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
                      + "\n"
                      + "       WHERE(cm.STATUS = '1'\n"
                      + "             OR cm.STATUS = '5')\n"
                      + $"            AND (cm.HPCODE = '{data.hpCode}')\n"
                      + $"            AND (cm.CLAIMNO = '{data.claimno}')\n"
                      //+ $"            {filterStatement}\n"
                      + $"            {whereClause}\n"
                      + "            AND (IGNORE.CLAIMNO IS NULL)\n"
                      + $"			AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND {spefication} AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)";

                    }

                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    vr.message = batchNum;
                    cn.Close();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return vr;
        }

        [HttpPost]
        [Route("GenerateBatchNumberServiceDate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GenerateBatchNumberServiceDate([FromBody] ClaimsModel data)
        {
            string filterStatement = "";
            string batchNum;
            string clientName;
            string description;
            string createDate;
            string date;
            DataTable dt = new DataTable();
            DataTable dttbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _drcConnectionString;
            string[] where;
            string whereClause = "";
            int count = 0;
            cmd.CommandTimeout = 3600;

            cn.ConnectionString = _drcConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                date = data.serviceToDate;

                if (data.claimType == "P")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'P')";
                }
                if (data.claimType == "M")
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }
                if (data.claimType == null)
                {
                    filterStatement = "AND (cm.CLAIMTYPE = 'M')";
                }

                string sql = "declare @batchNumber varchar(18)\n"
      + "declare @counter int\n"
      + "declare @stringCounter varchar(3)\n"
      + "SELECT/*CONVERT(VARCHAR(10),GETDATE(),111) AS DATE, " + this.DRCDatabase + $".dbo.LOB_CODES.CODE, " + this.PamcPortalDatabase + $".dbo.Users.name, ROWID ,\n"
      + "REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) AS ID,*/\n"
      + "@batchNumber =  CONVERT(VARCHAR(10),GETDATE(),112)+REPLICATE('0',3-LEN(CONVERT(VARCHAR(3),ROWID)))+CONVERT(VARCHAR(3),ROWID) \n"
      + "FROM " + this.DRCDatabase + $".dbo.LOB_CODES INNER JOIN\n"
      + "" + this.PamcPortalDatabase + $".dbo.Users ON " + this.DRCDatabase + $".dbo.LOB_CODES.CODE = " + this.PamcPortalDatabase + $".dbo.Users.lobcode WHERE " + this.PamcPortalDatabase + $".dbo.Users.username = '{data.user}';\n"
      + "--print @batchNumber\n"
      + "SELECT @counter = COUNT(*)+1 FROM " + this.DRCDatabase + $"..PAYBATCH WHERE SUBSTRING(CONVERT(VARCHAR(18),BATCH ),1,11) = @batchNumber;\n"
      + "--print @counter \n"
      + "SELECT @stringCounter = REPLICATE('0',3  - LEN(@counter)) + CONVERT(VARCHAR(3),@counter);\n"
      + "SELECT @batchNumber+@stringCounter;";
                cmd.CommandText = sql;
                //cmd.ExecuteNonQuery();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    dt.Load(dr);
                    batchNum = dt.Rows[0][0].ToString();
                }
                dt.Clear();
                cmd.CommandText = $"SELECT HPNAME FROM " + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR," + this.PamcPortalDatabase + $"..INSURANCE_CLIENTS.HPCODE) = '{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    clientName = dt.Rows[0][1].ToString();
                }
                dt.Clear();
                description = $"Web PayBatch Generated for {clientName}";
                createDate = DateTime.Now.ToString("yyyy/MM/dd");

                if ((data.whereClause.Length > 1) && (data.whereClause != null))
                {


                    where = data.whereClause.Split(',');
                    foreach (string condition in where)
                    {
                        count++;
                        if (count < where.Length)
                        {
                            whereClause = whereClause + $"'{condition}', ";
                        }
                        else
                        {
                            whereClause = whereClause + $"'{condition}'";
                        }

                    }
                }

                whereClause = $"AND PROV_SPECCODES.CODE IN ({whereClause})";

                cmd.CommandText = $"SELECT specifications FROM {PamcPortalDatabase}..Users WHERE username = '{data.user}'";
                using (SqlDataReader drr = cmd.ExecuteReader())
                {
                    dttbl.Load(drr);
                }
                string spefication = "";
                foreach (DataRow drow in dttbl.Rows)
                {
                    spefication = drow["specifications"].ToString();
                    if (spefication.Length == 0)
                    {
                        spefication = "1=1";
                    }

                }

                cmd.CommandText = $"INSERT INTO " + this.DRCDatabase + $"..PAYBATCH (BATCH, DESCR, PAYREADY, PAYRUN, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE,BATCHTYPE,USERNAME) VALUES (CONVERT(decimal(18),{batchNum})," +
                $"'{description}',0,0,999,CONVERT(DATE,'{createDate}'),999,CONVERT(DATE,'{createDate}'),'{data.claimType}','{data.user}')";
                cmd.ExecuteNonQuery();

                // Jaco 2023-11-24
                string prefix = Utillity.GetMemberPrefix(cn, data.hpCode, DRCDatabase, ReportingDatabase);

                cmd.CommandText = "INSERT INTO PAYBATCH_CLAIM\n"
        + "(BATCH, \n"
        + " CLAIMNO, \n"
        + " CREATEBY, \n"
        + " CREATEDATE, \n"
        + " LASTCHANGEBY, \n"
        + " LASTCHANGEDATE\n"
        + ")\n"
        + "       SELECT DISTINCT \n"
        + $"              {batchNum} , \n"
        + "              cm.CLAIMNO, \n"
        + "              999 , \n"
        + "              GETDATE() , \n"
        + "              999 , \n"
        + "              GETDATE() \n"
        + "       FROM CLAIM_MASTERS AS cm\n"
        + "            INNER JOIN PROV_SPECCODES ON cm.SPEC = PROV_SPECCODES.CODE\n"
        + $"	        LEFT OUTER JOIN dbo.Get_Claims_Checkrun_new('{data.hpCode}', '{prefix}') \n"
        + "			AS Get_Claims_Checkrun_1 ON cm.CLAIMNO = Get_Claims_Checkrun_1.CLAIMNO\n"
        + $"	        LEFT OUTER JOIN dbo.Checkrun_Claim_Ignore('{data.hpCode}', NULL , '{data.claimType}', NULL, NULL, NULL, '{date}', NULL)\n"
        + "			 AS IGNORE ON IGNORE.CLAIMNO = cm.CLAIMNO\n"
        + "\n"
        + "       WHERE(cm.STATUS = '1'\n"
        + "             OR cm.STATUS = '5')\n"
        + $"            AND (cm.HPCODE = '{data.hpCode}')\n"
        + $"            AND (ISNULL(cm.DATETO,cm.DATEFROM) <= '{date}')\n"
        + $"            {filterStatement}\n"
        + $"            {whereClause}\n"
        + "            AND (IGNORE.CLAIMNO IS NULL)\n"
        + $"			AND Get_Claims_Checkrun_1.CLAIMNO IS NULL AND {spefication} AND EXISTS (SELECT 1 FROM CLAIM_DETAILS WHERE CLAIMNO = cm.CLAIMNO)";



                cmd.CommandTimeout = 3600;
                cmd.ExecuteNonQuery();
                vr.message = batchNum;
                cn.Close();
            }


            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;

                // throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("RejectClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel RejectClaim([FromBody] CheckRunModel[] rejectedList)
        {
            string date = "";
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            string claimNo;
            string note;
            int rowNo;
            int reverse;
            string code = "";
            char[] notes;
            int indexOfCode;

            //bool validated;
            cn.ConnectionString = _portalConnectionString;
            cmd.Connection = cn;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                date = $"{DateTime.Today.Year.ToString()}/{DateTime.Today.Month.ToString()}/{DateTime.Today.Day.ToString()} " +
                   $"{DateTime.Now.Hour.ToString()}:{DateTime.Now.Minute.ToString()}:{DateTime.Now.Second.ToString()}";

                foreach (CheckRunModel line in rejectedList)
                {
                    if (line.reverse == "1")
                    {
                        cmd.CommandText = $"SELECT ADJCODE FROM " + this.DRCDatabase + $"..CLAIM_ADJUSTS WHERE CLAIMNO = '{line.claimNumber}' AND CLAIMTBLROW = {Convert.ToInt32(line.rowId)} AND CREATEBY = 991";
                        using (SqlDataReader rw = cmd.ExecuteReader())
                        {
                            dtable.Load(rw);
                        }

                        foreach (DataRow row in dtable.Rows)
                        {
                            code = row["ADJCODE"].ToString();
                        }
                        dtable.Clear();
                    }
                    else
                    {
                        indexOfCode = line.rejReason.IndexOf('-');
                        notes = line.rejReason.ToCharArray();
                        for (int i = 0; i < notes.Length; i++)
                        {
                            if (i < indexOfCode)
                            {
                                code = code + notes[i];
                            }
                        }
                    }

                    reverse = Convert.ToInt32(line.reverse);
                    note = line.note;
                    rowNo = Convert.ToInt32(line.rowId);
                    claimNo = line.claimNumber;

                    if ((line.note != null) && (line.note != ""))
                    {
                        cmd.CommandText = $"EXEC " + this.DRCDatabase + $".dbo.AdjustWebClaim {claimNo}, {rowNo},'{code}','{line.note}', {reverse} \n";
                    }
                    else
                    {
                        cmd.CommandText = $"EXEC " + this.DRCDatabase + $".dbo.AdjustWebClaim {claimNo}, {rowNo},'{code}','{note}', {reverse} \n";
                    }



                    cmd.ExecuteNonQuery();

                    cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET REJECTED ='{line.rejected}', LASTCHANGEDATE = '{date}' WHERE (BATCH = '{line.batchNo}' AND CLAIMNO = '{line.claimNumber}')";
                    cmd.ExecuteNonQuery();


                    cmd.CommandText = $"SELECT NET FROM {DRCDatabase}..CLAIM_DETAILS WHERE CLAIMNO = '{line.claimNumber}' AND TBLROWID = '{rowNo}'";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);

                    }
                    foreach (DataRow row in dt.Rows)
                    {
                        vr.message = row["NET"].ToString();
                    }


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;

            }
            cn.Close();
            return vr;
        }

        //use to  exclude claims from the checkrun
        [HttpPost]
        [Route("ExcludeClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ExcludeClaim([FromBody] CheckRunModel[] excludedList)
        {
            string date = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _portalConnectionString;
            cmd.Connection = cn;
            bool validated;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                date = $"{DateTime.Today.Year.ToString()}/{DateTime.Today.Month.ToString()}/{DateTime.Today.Day.ToString()} " +
                $"{DateTime.Now.Hour.ToString()}:{DateTime.Now.Minute.ToString()}:{DateTime.Now.Second.ToString()}";
                foreach (CheckRunModel line in excludedList)
                {
                    cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET EXCLUDED ='{line.excluded}', VALIDATED = '0',LASTCHANGEDATE = '{date}' WHERE (BATCH = '{line.batchNo}' AND CLAIMNO = '{line.claimNumber}')";
                    cmd.ExecuteNonQuery();

                    if (line.excluded == true)
                    {
                        cmd.CommandText = $"SELECT VALIDATED FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE (BATCH = '{line.batchNo}' AND CLAIMNO = '{line.claimNumber}')";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt.Load(dr);
                        }
                        foreach (DataRow row in dt.Rows)
                        {
                            if (DBNull.Value.Equals(row["VALIDATED"]) == false)
                            {
                                validated = Convert.ToBoolean(row["VALIDATED"]);
                                if (validated == true)
                                {
                                    cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET VALIDATED ='{!line.excluded}' WHERE (BATCH = '{line.batchNo}' AND CLAIMNO = '{line.claimNumber}')";
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        //use to validate an include a claim in a checkrun
        [HttpPost]
        [Route("ValidateClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateClaim([FromBody] CheckRunModel[] validatedList)
        {
            string date = "";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            ValidationResultsModel vr = new ValidationResultsModel();
            cn.ConnectionString = _portalConnectionString;
            cmd.Connection = cn;
            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                date = $"{DateTime.Today.Year.ToString()}/{DateTime.Today.Month.ToString()}/{DateTime.Today.Day.ToString()} " +
                       $"{DateTime.Now.Hour.ToString()}:{DateTime.Now.Minute.ToString()}:{DateTime.Now.Second.ToString()}";

                foreach (CheckRunModel line in validatedList)
                {

                    cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET VALIDATED ='{line.validated}',EXCLUDED ='0', LASTCHANGEDATE = '{date}' WHERE (BATCH = '{line.batchNo}' AND CLAIMNO = '{line.claimNumber}')";

                    cmd.ExecuteNonQuery();
                }


            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        //use to generate a list of all paid batches
        [HttpPost]
        [Route("GetPaidBatches")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunTypeModel> GetPaidBatches([FromBody] ClaimsModel data)
        {
            CheckRunTypeModel detailedBatch = new CheckRunTypeModel();
            DataTable datatbl = new DataTable();
            List<CheckRunTypeModel> returnList = new List<CheckRunTypeModel>();
            DataTable dtable = new DataTable();
            List<string> paidList = new List<string>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            //string claimNo = "";
            cmd.CommandTimeout = 3600;
            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            string batchNo = "";
            string[] clientName;
            string[] description;
            string[] hpcode;
            try
            {
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                //cmd.CommandText = $"SELECT HPCODE FROM UserLobCodesHpCodes WHERE username = '{data.user}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + // Jaco checked 2023-11-23
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{data.user}' ";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                hpcode = new string[dt.Rows.Count];
                int i = 0;
                foreach (DataRow drow in dt.Rows)
                {
                    hpcode[i] = drow["HPCODE"].ToString();
                    i++;
                }
                dt.Clear();
                string where = "";
                if (hpcode.Length > 1)
                {
                    for (int j = 0; j < hpcode.Length; j++)
                    {
                        if (j == 0)
                        {
                            where = $"CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                        else
                        {
                            where = where + $" OR CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                    }
                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE {where}";
                }
                else
                {
                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE CONVERT(VARCHAR,HPCODE) = '{hpcode[0]}'";
                }

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                i = 0;
                clientName = new string[dtable.Rows.Count];
                description = new string[dtable.Rows.Count];
                foreach (DataRow ro in dtable.Rows)
                {
                    clientName[i] = ro["HPNAME"].ToString();
                    i++;
                }
                SqlCommand command = new SqlCommand();
                if (clientName.Length > 1)
                {
                    for (int k = 0; k < description.Length; k++)
                    {
                        description[k] = $"Web PayBatch Generated for {clientName[k]}";
                    }

                    where = "";

                    for (int q = 0; q < description.Length; q++)
                    {
                        if (q == 0)
                        {
                            where = $"'{description[q]}'";
                        }
                        else
                        {
                            where = where + $",'{description[q]}'";
                        }
                    }
                    command.CommandText = $"SELECT BATCH,BATCHTYPE FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 1 AND PAYRUN = 1) AND (DESCR IN({where}))";
                }
                else
                {
                    description[0] = $"Web PayBatch Generated for {clientName[0]}";
                    command.CommandText = $"SELECT BATCH,BATCHTYPE FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 1 AND PAYRUN = 1) AND (DESCR = '{description[0]}')";
                }
                dt.Clear();
                cn.Close();
                cn.ConnectionString = _portalConnectionString;
                cn.Open();

                DataTable table = new DataTable();
                command.Connection = cn;

                using (SqlDataReader dreader = command.ExecuteReader())
                {
                    table.Load(dreader);
                }
                foreach (DataRow row in table.Rows)
                {
                    batchNo = row["BATCH"].ToString();
                    detailedBatch = new CheckRunTypeModel();
                    detailedBatch.batchNo = batchNo;
                    detailedBatch.xtype = row["BATCHTYPE"].ToString();
                    paidList.Add(batchNo);
                    returnList.Add(detailedBatch);
                }
                table.Clear();
                /* foreach (CheckRunTypeModel batch in returnList)
                 {
                     try
                     {
                         cmd.CommandText = $"SELECT CLAIMNO FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batch.batchNo}'";

                         using (SqlDataReader dr = cmd.ExecuteReader())
                         {
                             datatbl.Load(dr);
                         }
                         DataTable tbl = new DataTable();
                         foreach (DataRow row in datatbl.Rows)
                         {
                             claimNo = row[0].ToString();
                             cmd.CommandText = $"SELECT CLAIMTYPE FROM " + this.DRCDatabase + $"..CLAIM_MASTERS WHERE CLAIMNO = '{claimNo}'";
                             dtable.Clear();
                             using (SqlDataReader dr = cmd.ExecuteReader())
                             {
                                 tbl.Load(dr);
                             }
                             foreach (DataRow type in tbl.Rows)
                             {
                                 batch.xtype = type[0].ToString();
                             }
                         }
                         datatbl.Clear();
                         dtable.Clear();
                         dt.Clear();
                     }
                     catch (Exception e)
                     {
                         LogError( e.Message, e.StackTrace);
                         var msg = e.Message;
                     }
                 }*/
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            cn.Dispose();

            returnList = returnList.OrderByDescending(o => o.batchNo).ToList(); // Jaco 2023-12-18 sort descending 
            return returnList;
        }

        //used to generate a list of all unreleased batches
        [HttpPost]
        [Route("GetUnreleasedBatches")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunTypeModel> GetUnreleasedBatches([FromBody] ClaimsModel data)
        {
            CheckRunTypeModel detailedBatch = new CheckRunTypeModel();
            DataTable datatbl = new DataTable();
            List<CheckRunTypeModel> returnList = new List<CheckRunTypeModel>();
            DataTable dtable = new DataTable();
            List<string> paidList = new List<string>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string claimNo = "";
            cmd.CommandTimeout = 3600;
            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            string batchNo = "";
            string[] clientName;
            string[] description;
            string[] hpcode;
            try
            {
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                //cmd.CommandText = $"SELECT HPCODE FROM UserLobCodesHpCodes WHERE username = '{data.user}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + // Jaco tested 2023-11-23
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{data.user}' ";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                hpcode = new string[dt.Rows.Count];
                int i = 0;
                foreach (DataRow drow in dt.Rows)
                {
                    hpcode[i] = drow["HPCODE"].ToString();
                    i++;
                }
                dt.Clear();
                string where = "";
                if (hpcode.Length > 1)
                {
                    for (int j = 0; j < hpcode.Length; j++)
                    {
                        if (j == 0)
                        {
                            where = $"CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                        else
                        {
                            where = where + $" OR CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                    }
                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE {where}";
                }
                else
                {

                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE CONVERT(VARCHAR,HPCODE) = '{hpcode[0]}'";
                }

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                i = 0;
                clientName = new string[dtable.Rows.Count];
                description = new string[dtable.Rows.Count];
                foreach (DataRow ro in dtable.Rows)
                {
                    clientName[i] = ro["HPNAME"].ToString();
                    i++;
                }
                SqlCommand command = new SqlCommand();
                if (clientName.Length > 1)
                {
                    for (int k = 0; k < description.Length; k++)
                    {
                        description[k] = $"Web PayBatch Generated for {clientName[k]}";
                    }

                    where = "";

                    for (int q = 0; q < description.Length; q++)
                    {
                        if (q == 0)
                        {
                            where = $"'{description[q]}'";
                        }
                        else
                        {
                            where = where + $",'{description[q]}'";
                        }

                    }
                    command.CommandText = $"SELECT BATCH FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 0 AND PAYRUN = 0) AND (DESCR IN({where}))";
                }
                else
                {
                    description[0] = $"Web PayBatch Generated for {clientName[0]}";
                    command.CommandText = $"SELECT BATCH FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 0 AND PAYRUN = 0) AND (DESCR = '{description[0]}')";
                }
                dt.Clear();
                cn.Close();
                cn.ConnectionString = _portalConnectionString;
                cn.Open();

                DataTable table = new DataTable();
                command.Connection = cn;

                using (SqlDataReader dreader = command.ExecuteReader())
                {
                    table.Load(dreader);
                }
                foreach (DataRow row in table.Rows)
                {
                    batchNo = row["BATCH"].ToString();
                    detailedBatch = new CheckRunTypeModel();
                    detailedBatch.batchNo = batchNo;
                    paidList.Add(batchNo);
                    returnList.Add(detailedBatch);
                }
                table.Clear();
                foreach (CheckRunTypeModel batch in returnList)
                {
                    try
                    {
                        cmd.CommandText = $"SELECT CLAIMNO FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batch.batchNo}'";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            datatbl.Load(dr);
                        }
                        DataTable tbl = new DataTable();
                        foreach (DataRow row in datatbl.Rows)
                        {
                            claimNo = row[0].ToString();
                            cmd.CommandText = $"SELECT CLAIMTYPE FROM " + this.DRCDatabase + $"..CLAIM_MASTERS WHERE CLAIMNO = '{claimNo}'";
                            dtable.Clear();
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                tbl.Load(dr);
                            }
                            foreach (DataRow type in tbl.Rows)
                            {
                                batch.xtype = type[0].ToString();


                            }
                        }
                        datatbl.Clear();
                        dtable.Clear();
                        dt.Clear();
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message, e.StackTrace);
                        var msg = e.Message;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            cn.Dispose();

            returnList = returnList.OrderByDescending(o => o.batchNo).ToList(); // Jaco 2023-12-18 sort descending 
            return returnList;
        }

        //use to generate a list of all unpaid batches
        [HttpPost]
        [Route("GetUnPaidBatches")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunTypeModel> GetUnPaidBatches([FromBody] ClaimsModel data)
        {
            CheckRunTypeModel detailedBatch = new CheckRunTypeModel();
            DataTable datatbl = new DataTable();
            List<CheckRunTypeModel> returnList = new List<CheckRunTypeModel>();
            DataTable dtable = new DataTable();
            List<string> paidList = new List<string>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string claimNo = "";
            cmd.CommandTimeout = 3600;
            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            string batchNo = "";
            string[] clientName;
            string[] description;
            string[] hpcode;
            try
            {
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                //cmd.CommandText = $"SELECT HPCODE FROM UserLobCodesHpCodes WHERE username = '{data.user}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + // Jaco cheked 2023-11-23
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{data.user}' ";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                hpcode = new string[dt.Rows.Count];
                int i = 0;
                foreach (DataRow drow in dt.Rows)
                {
                    hpcode[i] = drow["HPCODE"].ToString();
                    i++;
                }
                dt.Clear();
                string where = "";
                if (hpcode.Length > 1)
                {
                    for (int j = 0; j < hpcode.Length; j++)
                    {
                        if (j == 0)
                        {
                            where = $"CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                        else
                        {
                            where = where + $" OR CONVERT(VARCHAR,HPCODE) = '{hpcode[j]}'";
                        }
                    }
                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE {where}";
                }
                else
                {
                    cmd.CommandText = $"SELECT HPNAME FROM INSURANCE_CLIENTS WHERE CONVERT(VARCHAR,HPCODE) = '{hpcode[0]}'";
                }

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                i = 0;
                clientName = new string[dtable.Rows.Count];
                description = new string[dtable.Rows.Count];
                foreach (DataRow ro in dtable.Rows)
                {
                    clientName[i] = ro["HPNAME"].ToString();
                    i++;
                }
                SqlCommand command = new SqlCommand();
                if (clientName.Length > 1)
                {
                    for (int k = 0; k < description.Length; k++)
                    {
                        description[k] = $"Web PayBatch Generated for {clientName[k]}";
                    }

                    where = "";

                    for (int q = 0; q < description.Length; q++)
                    {
                        if (q == 0)
                        {
                            where = $"'{description[q]}'";
                        }
                        else
                        {
                            where = where + $",'{description[q]}'";
                        }

                    }
                    command.CommandText = $"SELECT BATCH FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 1 AND PAYRUN = 0) AND (DESCR IN({where}))";
                }
                else
                {
                    description[0] = $"Web PayBatch Generated for {clientName[0]}";
                    command.CommandText = $"SELECT BATCH FROM " + this.DRCDatabase + $"..PAYBATCH WHERE (PAYREADY = 1 AND PAYRUN = 0) AND (DESCR = '{description[0]}')";
                }
                dt.Clear();
                cn.Close();
                cn.ConnectionString = _portalConnectionString;
                cn.Open();

                DataTable table = new DataTable();
                command.Connection = cn;

                using (SqlDataReader dreader = command.ExecuteReader())
                {
                    table.Load(dreader);
                }
                foreach (DataRow row in table.Rows)
                {
                    batchNo = row["BATCH"].ToString();
                    detailedBatch = new CheckRunTypeModel();
                    detailedBatch.batchNo = batchNo;
                    paidList.Add(batchNo);
                    returnList.Add(detailedBatch);
                }
                table.Clear();
                foreach (CheckRunTypeModel batch in returnList)
                {
                    try
                    {
                        cmd.CommandText = $"SELECT CLAIMNO FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batch.batchNo}'";

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            datatbl.Load(dr);
                        }
                        DataTable tbl = new DataTable();
                        foreach (DataRow row in datatbl.Rows)
                        {
                            claimNo = row[0].ToString();
                            cmd.CommandText = $"SELECT CLAIMTYPE FROM " + this.DRCDatabase + $"..CLAIM_MASTERS WHERE CLAIMNO = '{claimNo}'";
                            dtable.Clear();
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                tbl.Load(dr);
                            }
                            foreach (DataRow type in tbl.Rows)
                            {
                                batch.xtype = type[0].ToString();
                            }
                        }
                        datatbl.Clear();
                        dtable.Clear();
                        dt.Clear();
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message, e.StackTrace);
                        var msg = e.Message;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            cn.Dispose();

            returnList = returnList.OrderByDescending(o => o.batchNo).ToList(); // Jaco 2023-12-18 sort descending 
            return returnList;
        }

        //Generates a prevoius checkrun thatwas not completed
        [HttpPost]
        [Route("GetSpecificBatch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunModel> GetSpecificBatch([FromBody] BatchDetailsModel batchNo)
        {
            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : OpenUnreleasedBatch---------------\r\n";
            ValidationResultsModel vr = new ValidationResultsModel();
            List<CheckRunModel> boardRowList = new List<CheckRunModel>();
            try
            {
                try
                {
                    string sorting = "";
                    DataTable dtabel = new DataTable();
                    DataTable tbl = new DataTable();
                    DataTable dt = new DataTable();
                    DataTable tabeldt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    DataTable hptabel = new DataTable();
                    DataTable data = new DataTable();
                    SqlConnection cn = new SqlConnection();
                    cn.ConnectionString = _drcConnectionString;
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    cmd.CommandTimeout = 3600;
                    cmd.Connection = cn;

                    // Jaco comment out 2023-09-14 View is hardcoded to DRC
                    //cmd.CommandText = "SELECT " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes.HPCODE, " + this.PamcPortalDatabase + $"..Users.ViewSpecification\n"
                    //           + "FROM " + this.PamcPortalDatabase + $"..Users INNER JOIN\n"
                    //           + "" + this.PamcPortalDatabase + $"..UserLobCodesHpCodes ON " + this.PamcPortalDatabase + $"..Users.username = " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes.username\n"
                    //           + $"WHERE (" + this.PamcPortalDatabase + $"..Users.username = '{batchNo.hpCode}')";

                    // Jaco 2023-09-14
                    cmd.CommandText =   $" SELECT H.HPCODE, U.ViewSpecification " +  //Jaco tested 2023/11/23
                                        $" FROM {PamcPortalDatabase}..Users U " +
                                        $" INNER JOIN ( " +
                                        $"               SELECT u.username, u.lobcode, hp.HPCODE " +
                                        $"               FROM   {PamcPortalDatabase}..Users AS u CROSS JOIN" +
                                        $"                        {DRCDatabase}..HP_CONTRACTS AS hp  " +
                                        $"               where hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) " +
                                        $"            ) H on (U.username = H.username) " +
                                        $" WHERE U.username = '{batchNo.hpCode}' ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtabel.Load(dr);
                    }
                    foreach (DataRow rows in dtabel.Rows)
                    {
                        // batchNo.hpCode = rows["HPCODE"].ToString();
                        batchNo.whereClause = rows["ViewSpecification"].ToString();
                    }
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}BatchNo:{batchNo.batchNumber} ---- HPCODE: {batchNo.hpCode}\r\n";
                    dtabel.Clear();
                    cmd.CommandText = $"SELECT FILTER FROM PAYBATCH WHERE BATCH = '{batchNo.batchNumber}'";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tbl.Load(dr);
                    }
                    foreach (DataRow rows in tbl.Rows)
                    {
                        sorting = rows["FILTER"].ToString();
                    }

                    string hpcode = "";
                    string[] hpname;

                    // Jaco comment out 2023-09-14 View is hardcoded to DRC
                    //string sql = $"SELECT HPNAME FROM {PamcPortalDatabase}..UserLobCodesHpCodes INNER JOIN\n"   
                    //           + $"{PamcPortalDatabase}..INSURANCE_CLIENTS  IC ON CONVERT(VARCHAR,IC.HPCODE) = {PamcPortalDatabase}..UserLobCodesHpCodes.HPCODE \n"
                    //           + $" WHERE USERNAME = '{batchNo.hpCode}'";
                    //cmd.CommandText = sql;

                    // Jaco 2023-09-14
                    cmd.CommandText = $" SELECT HPNAME " + // Jaco tested 2023-11-23
                                      $" FROM ( " +
                                      $"         SELECT        u.username, u.lobcode, hp.HPCODE                                                 " +
                                      $"         FROM          {PamcPortalDatabase}..Users AS u CROSS JOIN                                      " +
                                      $"                           {DRCDatabase}..HP_CONTRACTS AS hp                                            " +
                                      $"         where hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') )              " +
                                      $"       ) u                                                                                              " +
                                      $"       INNER JOIN {PamcPortalDatabase}..INSURANCE_CLIENTS IC ON CONVERT(VARCHAR,IC.HPCODE) = u.HPCODE   " +
                                      $" WHERE USERNAME = '{batchNo.hpCode}'                                                                    ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        tabeldt.Load(dr);
                    }
                    hpname = new string[tabeldt.Rows.Count];
                    int counter = 0;
                    foreach (DataRow drow in tabeldt.Rows)
                    {
                        hpname[counter] = drow["HPNAME"].ToString();
                        counter++;
                    }

                    //sql = $"SELECT DESCR FROM PAYBATCH  WHERE BATCH = '{batchNo.batchNumber}'";
                    //cmd.CommandText = sql;
                    cmd.CommandText = $"SELECT DESCR FROM PAYBATCH  WHERE BATCH = '{batchNo.batchNumber}'";

                    string temp = "";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        data.Load(dr);
                    }
                    counter = 0;
                    foreach (DataRow drow in data.Rows)
                    {
                        temp = drow["DESCR"].ToString();
                    }

                    foreach (string name in hpname)
                    {
                        if (temp.Contains(name))
                        {
                            cmd.CommandText = $"SELECT HPCODE FROM {PamcPortalDatabase}..INSURANCE_CLIENTS WHERE CONVERT(VARCHAR,HPNAME) = '{name}'";
                            break;
                        }
                    }

                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        hptabel.Load(rd);
                    }

                    foreach (DataRow roww in hptabel.Rows)
                    {
                        hpcode = roww["HPCODE"].ToString();
                    }

                    bool submit = false;
                    //sql = GetBrExtractSql(batchNo.batchNumber, submit, hpcode);
                    //cmd.CommandText = sql;
                    cmd.CommandText = GetBrExtractSql(batchNo.batchNumber, submit, hpcode, DRCDatabase, ReportingDatabase);
                    cmd.CommandTimeout = 3600;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        CheckRunModel line = new CheckRunModel();
                        line.rowId = (row["TBLROWID"]).ToString();
                        line.mainPolicyNumber = (row["FAMILY"]).ToString();
                        line.principleMainMemberNumber = (row["Principle / Main Member number (applicable to schemes)"]).ToString();
                        line.NameSurnamePrincipleMember = (row["Name & Surname of principle member / policy holder"]).ToString();
                        line.idNumberofMember = (row["ID number of member / policy holder"]).ToString();
                        line.claimType = (row["Claim Type"]).ToString();
                        line.provId = (row["PROVID"]).ToString();
                        line.speciality = (row["SPECIALTY"]).ToString();
                        line.providerName = (row["PROVIDER NAME"]).ToString();
                        line.refProvId = (row["REFPROVID"]).ToString();
                        line.reqSpec = (row["SPEC"]).ToString();
                        line.productDescription = (row["Product Description"]).ToString();
                        line.productCode = (row["Product Code"]).ToString();
                        line.claimNumber = (row["Claim Number"]).ToString();
                        DateTime dtRec = Convert.ToDateTime(row["Date of Incident"]);
                        line.dateofIncident = dtRec.ToString("yyyy/MM/dd");
                        line.namSurnameofdeceased = (row["Name & Surname of deceased / patient"]).ToString();
                        line.idNumberofDeceased = (row["ID number of deceased / patient"]).ToString();
                        line.dateOfClaimReceived = (row["Date of claim reported / received"]).ToString();
                        line.amountOfClaimReported = (row["Amount of claim reported"]).ToString();
                        line.amountOfClaimApproved = (row["Amount of claim approved"]).ToString();
                        line.dateOfClaimPaid = (row["Date of claim paid"]).ToString();
                        line.paymentBatchNo = (row["PAYMENT BATCH NO"]).ToString();
                        line.amountOfClaimPaid = (row["Amount of claim paid"]).ToString();
                        line.claimStatus = (row["Claim Status"]).ToString();
                        line.claimAmountOutstanding = (row["Claim amount outstanding"]).ToString();
                        line.bankAccountName = (row["Bank Account Name"]).ToString();
                        line.bankAccountBranchCode = (row["Bank Account Branch Code"]).ToString();
                        line.bankAccountNumber = (row["Bank Account Number"]).ToString();
                        line.reasonCode1 = (row["ReasonCode1"]).ToString();
                        line.reasonCode2 = (row["ReasonCode2"]).ToString();
                        line.option = (row["OPT"]).ToString();
                        line.reqSpecDescription = (row["SPECIALTY"]).ToString();
                        line.patientBirth = (row["PatientBirth"].ToString());
                        line.patientMembNo = (row["MEMB"].ToString());
                        line.patientName = (row["PatientFirstNm"].ToString());
                        line.type = row["PAY"].ToString();
                        if (DBNull.Value.Equals(row["VALIDATED"]))
                        {
                            line.validated = false;
                        }
                        else
                        {
                            if ((Convert.ToInt32(row["VALIDATED"])) == 1)
                            {
                                line.validated = true;
                            }
                            else
                            {
                                line.validated = false;
                            }
                        }

                        if (DBNull.Value.Equals(row["REJECTED"]))
                        {
                            line.rejected = false;
                        }
                        else
                        {
                            if ((Convert.ToInt32(row["REJECTED"])) == 1)
                            {
                                line.rejected = true;
                            }
                            else
                            {
                                line.rejected = false;
                            }
                        }

                        if (DBNull.Value.Equals(row["EXCLUDED"]))
                        {
                            line.excluded = false;
                        }
                        else
                        {
                            if ((Convert.ToInt32(row["EXCLUDED"])) == 1)
                            {
                                line.excluded = true;
                            }
                            else
                            {
                                line.excluded = false;
                            }
                        }

                        boardRowList.Add(line);
                    }
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}CREATED LIST DONE: {boardRowList.Capacity}\r\n";
                    dt.Clear();
                    cn.Close();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}CREATED LIST ERROR :{e.Message} ---- \r\n STACKTRACE: {e.StackTrace}\r\n";
                    var msg = e.Message;
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}GENERAL ERROR :{e.Message} ---- \r\n STACKTRACE: {e.StackTrace}\r\n";
                throw;
            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----ENDING : OpenUnreleasedBatch---------------\r\n";
            }

            return boardRowList;
        }

        //updates banking details of the provider
        [HttpPost]
        [Route("ChangeBankingDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ChangeBankingDetails([FromBody] BankingDetailModel details)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable dtble = new DataTable();
            DataTable dtb = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string batchType = "";
            string provid = "";
            string body = "";
            string emails = "";
            string subject = "";
            string hpcode = "";
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;

            try
            {
                cmd.CommandText = $"UPDATE PAYBATCH_CLAIM SET BANK = '{details.bankName}', BRANCHCODE = '{details.branchCode}', ACCNR = '{details.accountNo}', ACCTYPE ='{details.accType}' WHERE (BATCH = {details.batchNo} AND CLAIMNO ='{details.mainPolicyNo}')";
                cmd.ExecuteNonQuery();
                cmd.CommandText = $"SELECT BATCHTYPE FROM  PAYBATCH WHERE BATCH = '{details.batchNo}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    batchType = row["BATCHTYPE"].ToString();
                }

                subject = "Banking Details Update";

                if (batchType.ToUpper() == "M")
                {
                    // Jaco 2023-09-28 parameterise databses
                    cmd.CommandText = $"SELECT {PamcPortalDatabase}.dbo.RemoveDependantDash(MEMBID) AS MEMB, HPCODE FROM {DRCDatabase}..CLAIM_MASTERS WHERE CLAIMNO = '{details.mainPolicyNo}'";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtble.Load(dr);
                    }

                    foreach (DataRow row in dtble.Rows)
                    {
                        provid = row["MEMB"].ToString();
                        hpcode = row["HPCODE"].ToString();
                    }
                    cmd.CommandText = $"EXECUTE {DRCDatabase}.dbo.CreateVendorBankHist 'M{provid}','{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{DateTime.Now.ToString("yyyy/MM/dd")}',NULL,999";
                    cmd.ExecuteNonQuery();

                    // Jaco comment out 2023-09-28 - Databases hardcoded in GetMemberPrefix function
                    cmd.CommandText = 
                        //$"DECLARE @membID AS VARCHAR(MAX) \r\n SELECT @membID = ({PamcPortalDatabase}.dbo.GenerateSubSsn({PamcPortalDatabase}.dbo.GetMemberPrefix('{details.hpcode}'), {PamcPortalDatabase}.dbo.RemoveDependantDash('{details.mainPolicyNo}')))\r\n" + // This line of SQL is not used
                        $" INSERT INTO {PamcPortalDatabase}..BankingDetailsLog (VendorId,BankName,BranchCode,AccNo,AccType,ChangedBy,ChangedDate,Completed) \n" +
                        $" VALUES('M{provid}','{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{details.user}','{DateTime.Now.ToString("yyyy/MM/dd")}',1) ";
                    cmd.ExecuteNonQuery();
                                        
                    body = $"Good day \r\n\n The following banking details was updated via a Checkrun on the PAMC website.\r\n\n USER: {details.user}\r\n MEMBERID:M{provid}\r\n BANK NAME:{details.bankName}\r\n" +
                                       $"BRANCH CODE: {details.branchCode}\r\n ACCNO: {details.accountNo} \r\n ACC TYPE: {details.accType} \r\n\n Regards";
                }
                if (batchType.ToUpper() == "P")
                {
                    cmd.CommandText = $"SELECT PROVID,HPCODE  FROM CLAIM_MASTERS WHERE CLAIMNO = '{details.mainPolicyNo}'";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtble.Load(dr);
                    }

                    foreach (DataRow row in dtble.Rows)
                    {
                        provid = row["PROVID"].ToString();
                        hpcode = row["HPCODE"].ToString();
                    }

                    cmd.CommandText = $"\r\n" +
                   $"INSERT INTO {PamcPortalDatabase}..BankingDetailsLog (VendorId,BankName,BranchCode,AccNo,AccType,ChangedBy,ChangedDate,Completed) " +
                   $"VALUES('{provid}','{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{details.user}','{DateTime.Now.ToString("yyyy/MM/dd")}',0)";
                    cmd.ExecuteNonQuery();
                    body = $"Good day \r\n\n The following banking details was captured via a Checkrun on the PAMC website and needs to be updated.\r\n\n USER: {details.user}\r\n PROVIDER ID:{provid}\r\n BANK NAME:{details.bankName}\r\n" +
                                      $"BRANCH CODE: {details.branchCode}\r\n ACCNO: {details.accountNo} \r\n ACC TYPE: {details.accType} \r\n\n Regards";
                }


                cmd.CommandText = $"SELECT CheckrunEmail FROM {PamcPortalDatabase}.dbo.HealthPlanSetup WHERE HPCode = '{hpcode}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtb.Load(dr);
                }

                foreach (DataRow row in dtb.Rows)
                {
                    emails = row["CheckrunEmail"].ToString();
                }

                string url = _EmailWebSvcConnection;

                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;

                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                //var q = client.SendEmailAsync(body, subject, emails);
                //q.Wait();

                if (_isTest)
                {
                    var q = client.SendEmailAsync(body, "PORTAL TEST: " + subject, _testEmails);
                    q.Wait();
                }
                else
                {
                    var q = client.SendEmailAsync(body, subject, emails);
                    q.Wait();
                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            Thread.Sleep(7000);
            cn.Close();
            return vr;
        }

        [HttpPost]
        [Route("CheckValidation")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel CheckValidation([FromBody] BatchDetailsModel batchNo)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;

            try
            {
                vr.valid = true;
                Task.Delay(5000);
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;

            }
            cn.Close();
            vr.valid = true;
            return vr;

        }

        [HttpPost]
        [Route("CheckUpdatedBankingDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<string> CheckUpdatedBankingDetails([FromBody] BatchDetailsModel batchNo)
        {
            List<string> vr = new List<string>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;

            try
            {
                cmd.CommandText = $"SELECT CLAIMNO,ISNULL(BANK,'') FROM PAYBATCH_CLAIM WHERE BATCH = '{batchNo.batchNumber}' AND BANK <> ''  ";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    string claimno = "";
                    claimno = row["CLAIMNO"].ToString();
                    vr.Add(claimno);
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;

            }
            cn.Close();
            return vr;

        }

        //Makes the batch ready for payment
        [HttpPost]
        [Route("MarkBatchReady")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel MarkBatchReadyAsync([FromBody] BatchDetailsModel batchNo)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();            
            DataTable moveStatus = new DataTable();
            DataTable tbl = new DataTable();
            DataTable emailtbl = new DataTable();
            DataTable excluded = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string date;
            string log = "";
            string paydate = "";
            string user = "";
            string type = "";
            string body = "";
            string To = "";
            List<string> claimNo = new List<string>();
            string batchHpCode = "";
            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open) { cn.Open(); }
            cmd.Connection = cn;
            cmd.CommandTimeout = 3600;

            try
            {
                // Jaco 2023-12-19 check if more than one healthplan is contained in batch
                cmd.CommandText =
                     " SELECT DISTINCT CM.HPCODE \n" +
                    $" FROM {this.DRCDatabase}..CLAIM_MASTERS CM \n" +
                    $" INNER JOIN {this.DRCDatabase}..PAYBATCH_CLAIM PC on(CM.CLAIMNO = PC.CLAIMNO) \n" +
                     " WHERE PC.BATCH = @batch ";                     

                cmd.Parameters.Add(new SqlParameter("@batch", batchNo.batchNumber));
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    tbl.Load(reader);
                }

                if (tbl.Rows.Count > 1)
                {
                    vr.valid = false;
                    vr.message = $"Checkrun batch {batchNo.batchNumber} contains more than one healthplan.";
                    return vr;
                }
                else
                {
                    batchHpCode = tbl.Rows[0]["HPCODE"].ToString();
                }
                // end Jaco 2023-12-19 check if more than one healthplan is contained in batch

                // Jaco comment out 2023-09-11 View is hardcoded to DRC
                //cmd.CommandText = $"SELECT HPCODE FROM UserLobCodesHpCodes WHERE username = '{batchNo.hpCode}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " +
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{batchNo.hpCode}' ";

                bool hpCodeValidation = false;
                tbl = new DataTable();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    tbl.Load(dr);
                }
                foreach (DataRow row in tbl.Rows)
                {
                    if (batchHpCode == row["HPCODE"].ToString())
                    {
                        hpCodeValidation = true;
                        break;
                    }
                }

                if (!hpCodeValidation)
                {
                    vr.valid = false;
                    vr.message = $"User not linked to healthplan contained in checkrun batch {batchNo.batchNumber}.";
                    return vr;
                }
                else
                {
                    batchNo.hpCode = batchHpCode;
                }

                log += $"{DateTime.Now.ToString()}---Release Batch Started-----------\r\n\n";
                cmd.CommandText = $"SELECT DATETOPAY,DESCR,BATCHTYPE FROM " + this.DRCDatabase + $"..PAYBATCH WHERE BATCH = '{batchNo.batchNumber}'";
                log += $"{DateTime.Now.ToString()}---Date to pay sql {cmd.CommandText}-----------\r\n\n";
                tbl = new DataTable();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    tbl.Load(reader);
                }
                foreach (DataRow row in tbl.Rows)
                {
                    paydate = row["DATETOPAY"].ToString();
                    user = row["DESCR"].ToString();
                    type = row["BATCHTYPE"].ToString();
                    if (type.ToUpper() == "P")
                    {
                        type = "PROVIDER";
                    }
                    if (type.ToUpper() == "M")
                    {
                        type = "MEMBER";
                    }
                }

                log += $"{DateTime.Now.ToString()}---Date to pay {paydate}-----------\r\n\n";

                cmd.CommandText = 
                    $" SELECT VEND_MASTERS.VENDORNM, CLAIM_MASTERS.VENDOR, PAYBATCH.BATCHTYPE,PAYBATCH_CLAIM.CLAIMNO, PAYBATCH_CLAIM.BANK, PAYBATCH_CLAIM.BRANCHCODE, PAYBATCH_CLAIM.ACCNR,PAYBATCH_CLAIM.ACCTYPE," +
                    $"          CLAIM_MASTERS.PROVID, CLAIM_MASTERS.MEMBID, PROV_MASTERS.LASTNAME, CLAIM_MASTERS.MEMBNAME \n" +
                    $" FROM {this.DRCDatabase}..PAYBATCH_CLAIM INNER JOIN \n" +
                    $"      {this.DRCDatabase}..PAYBATCH ON PAYBATCH.BATCH = {this.DRCDatabase}..PAYBATCH_CLAIM.BATCH INNER JOIN \n" +
                    $"      {this.DRCDatabase}..CLAIM_MASTERS ON PAYBATCH_CLAIM.CLAIMNO = {this.DRCDatabase}..CLAIM_MASTERS.CLAIMNO INNER JOIN \n" +
                    $"      {this.DRCDatabase}..PROV_MASTERS ON {this.DRCDatabase}..CLAIM_MASTERS.PROVID = {this.DRCDatabase}..PROV_MASTERS.PROVID INNER JOIN \n" +
                    $"      {this.DRCDatabase}..VEND_MASTERS ON {this.DRCDatabase}..VEND_MASTERS.VENDORID = {this.DRCDatabase}..CLAIM_MASTERS.VENDOR \n" +
                    $" WHERE(PAYBATCH_CLAIM.BATCH = '{batchNo.batchNumber}') AND (PAYBATCH_CLAIM.BANK <> '')";

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dt.Load(reader);
                }
                try
                {
                    body = "Good Day" + "\n \n" + $"The Following Checkrun was submitted for payment with batch No: {batchNo.batchNumber}\r\n\n Batch Type: {type} Payment \r\n\n";
                    body = body + $"Pay Date For Checkrun: {batchNo.paydate} \r\n\n";

                    if (dt.Rows.Count > 0)
                    {
                        body += $"Please take note of the following banking detail changes : \r\n\n";
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["BATCHTYPE"].ToString().ToUpper() == "M")
                            {
                                body = body + "\n" +
                                                           $"Claim No: {row["CLAIMNO"].ToString()} \r\n" +
                                                           $"Member ID: {row["MEMBID"].ToString()} \r\n" +
                                                           $"Member Name: {row["MEMBNAME"].ToString()} \r\n" +
                                                           $"New Bank Name: {row["BANK"].ToString()} \r\n" +
                                                           $"New BranchCode: {row["BRANCHCODE"].ToString()} \r\n" +
                                                           $"New Account No: {row["ACCNR"].ToString()} \r\n" +
                                                           $"New Account Type: {row["ACCTYPE"].ToString()} \r\n";
                            }
                            if (row["BATCHTYPE"].ToString().ToUpper() == "P")
                            {
                                body = body + "\n" +
                                                           $"Claim No: {row["CLAIMNO"].ToString()} \r\n" +
                                                           $"Vendor ID: {row["VENDOR"].ToString()} \r\n" +
                                                           $"Vendor Name: {row["VENDORNM"].ToString()} \r\n" +
                                                           $"New Bank Name: {row["BANK"].ToString()} \r\n" +
                                                           $"New BranchCode: {row["BRANCHCODE"].ToString()} \r\n" +
                                                           $"New Account No: {row["ACCNR"].ToString()} \r\n" +
                                                           $"New Account Type: {row["ACCTYPE"].ToString()} \r\n";
                            }
                        }
                    }
                                        
                    log += $"{DateTime.Now.ToString()}---Email body {body}-----------\r\n\n";
                        
                    cmd.CommandText = $"SELECT CheckrunEmail FROM {PamcPortalDatabase}..HealthPlanSetup WHERE HPCode = '{batchNo.hpCode}'";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        emailtbl.Load(dr);
                    }
                    To = "";
                    foreach (DataRow row in emailtbl.Rows)
                    {
                        To = row["CheckrunEmail"].ToString();
                    }                    
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    log += $"{DateTime.Now.ToString()}---Error Msg: {e.Message}------Stack Trace: {e.StackTrace}-----\r\n\n";
                    var msg = e.Message;
                }
                finally
                {
                    date = DateTime.Now.ToString("yyyy/MM/dd");
                    cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH SET PAYREADY = 1, LASTCHANGEDATE='{date}'WHERE BATCH = '{batchNo.batchNumber}'";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = $"SELECT CLAIMNO,ISNULL(VALIDATED,0) AS VALIDATED FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batchNo.batchNumber}' AND VALIDATED = 1";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        moveStatus.Load(reader);
                    }
                    foreach (DataRow row in moveStatus.Rows)
                    {
                        claimNo.Add(row["CLAIMNO"].ToString());
                    }
                    date = date + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();
                    foreach (string claim in claimNo)
                    {
                        cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..CLAIM_MASTERS SET STATUS = 4 WHERE CLAIMNO = '{claim}' AND STATUS <> 9";
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = $"SELECT CLAIMNO FROM {DRCDatabase}..PAYBATCH_CLAIM WHERE BATCH = '{batchNo.batchNumber}' AND ISNULL(VALIDATED,0)=0 AND ISNULL(REJECTED,0)=0 AND ISNULL(EXCLUDED,0)=0";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        excluded.Load(dr);
                    }
                    List<string> claims = new List<string>();
                    foreach (DataRow drow in excluded.Rows)
                    {
                        claims.Add(drow["CLAIMNO"].ToString());
                    }

                    foreach (string claim in claims)
                    {
                        cmd.CommandText = $"UPDATE {DRCDatabase}..PAYBATCH_CLAIM SET EXCLUDED = 1 , LASTCHANGEDATE = '{DateTime.Now.ToString("yyyy/MM/dd")}' WHERE BATCH = '{batchNo.batchNumber}' AND CLAIMNO = '{claim}'";
                        cmd.ExecuteNonQuery();
                    }

                    string batchTotal = "";

                    cmd.CommandText = $" SELECT SUM(cm.NET) FROM {DRCDatabase}..CLAIM_MASTERS cm " +
                                      $" INNER JOIN {DRCDatabase}..PAYBATCH_CLAIM pc on pc.CLAIMNO = cm.CLAIMNO " +
                                      $" WHERE pc.VALIDATED = 1 and ISNULL(VALIDATED,0) = 1 AND pc.BATCH = '{batchNo.batchNumber}' ";

                    DataTable total = new DataTable();

                    total.Load(cmd.ExecuteReader());

                    if (total.Rows.Count > 0)
                    {
                        batchTotal = total.Rows[0][0].ToString();
                    }
                    else
                    {
                        batchTotal = "0.00";
                    }

                    if (batchTotal.Trim() == "") // Jaco 2023-12-18
                    {
                        batchTotal = "0.00";
                    }

                    body += $"\r\n\r\nBATCH TOTAL: R{batchTotal}\r\n\r\n";
                    body = body + "\n" + "Regards";
                    
                    string subject = $"Checkrun Submitted({user})";

                    string url = _EmailWebSvcConnection;

                    EndpointAddress address = new EndpointAddress(url);
                    BasicHttpBinding binding = new BasicHttpBinding();
                    binding.MaxReceivedMessageSize = 2147483647;
                    var time = new TimeSpan(0, 30, 0);
                    binding.CloseTimeout = time;
                    binding.OpenTimeout = time;
                    binding.ReceiveTimeout = time;
                    binding.SendTimeout = time;

                    EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                                       
                    try
                    {   
                        if (_isTest)
                        {
                            var q = client.SendEmailAsync("\n\n !!!THIS IS A PORTAL TEST!!! \n\n" + body, "PORTAL TEST: " + subject, _testEmails);
                            q.Wait();
                        }
                        else
                        {
                            var q = client.SendEmailAsync(body, subject, To);
                            q.Wait();
                        }

                        if (_isTest)
                        {
                            var c = client.SendMeetingInviteAsync($"Good day,\n\n !!!THIS IS A PORTAL TEST!!! \n\n Please take note this batch needs to be paid on {batchNo.paydate}." +
                            "\n\n Regards", "PORTAL TEST: " + subject + "► BatchNo: " + batchNo.batchNumber + " ►Type: " + type + " Payment ►Pay Date: " + batchNo.paydate + " ► BatchTotal: R" + batchTotal, _testEmails, batchNo.paydate);
                            c.Wait();
                        }
                        else
                        {
                            var c = client.SendMeetingInviteAsync($"Good day, \n\n Please take note this batch needs to be paid on {batchNo.paydate}." +
                            "\n\n Regards", subject + "► BatchNo: " + batchNo.batchNumber + " ►Type: " + type + " Payment ►Pay Date: " + batchNo.paydate + " ► BatchTotal: R" + batchTotal, To, batchNo.paydate);
                            c.Wait();
                        }

                        cn.Close();
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message, e.StackTrace);
                        var msg = e.Message;
                        log += $"{DateTime.Now.ToString()}---Error Msg: {e.Message}------Stack Trace: {e.StackTrace}-----\r\n\n";
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"{DateTime.Now.ToString()}---General error  {e.Message}-----------\r\n\n";
                log += $"{DateTime.Now.ToString()}---General statck trace  {e.StackTrace}-----------\r\n\n";
                var msg = e.Message;
            }
            finally
            {
                log += $"{DateTime.Now.ToString()}---Release Batch Ended-----------\r\n\n";
            }
            cn.Close();
            vr.valid = true;
            return vr;
        }

        //gets banking details if it was previously changed
        [HttpPost]
        [Route("GetEditedBankingDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public BankingDetailModel GetEditedBankingDetails([FromBody] BankingDetailModel data)
        {
            BankingDetailModel details = new BankingDetailModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;
            cmd.CommandText = $"SELECT BANK, BRANCHCODE, ACCNR FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{data.batchNo}' AND CLAIMNO = '{data.mainPolicyNo}'";

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }

            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (DBNull.Value.Equals(row["BANK"]))
                    {
                        details.bankName = "";
                    }
                    else
                    {
                        details.bankName = row["BANK"].ToString();
                    }
                    if (DBNull.Value.Equals(row["BRANCHCODE"]))
                    {
                        details.branchCode = "";
                    }
                    else
                    {
                        details.branchCode = row["BRANCHCODE"].ToString();
                    }
                    if (DBNull.Value.Equals(row["ACCNR"]))
                    {
                        details.accountNo = "";
                    }
                    else
                    {
                        details.accountNo = row["ACCNR"].ToString();
                    }
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }
            cn.Close();
            return details;
        }

        //finds the details of the provider for a specific claim
        [HttpPost]
        [Route("FindProvider")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public BankingDetailModel FindProvide([FromBody] BankingDetailModel data)
        {
            string _vendId = "";
            BankingDetailModel vr = new BankingDetailModel();
            DataTable dt = new DataTable();
            DataTable dataTable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            cmd.Connection = cn;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            try
            {
                cmd.CommandText = $"SELECT VENDOR FROM CLAIM_MASTERS WHERE CLAIMNO = '{data.claimno}' AND PROVID = '{data.provId}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    _vendId = row["VENDOR"].ToString();
                }
                cmd.CommandText = //" SELECT VM.VENDORNM,VB.BANK,VB.BRANCHCODE,VB.ACCNR FROM DRCREP..VEND_MASTERS VM \n"         // Jaco comment out 2023-09-28
                                  //+ "INNER JOIN DRCREP..VENDOR_BANKHIST VB ON VB.VENDORID = VM.VENDORID\n"                     // Jaco comment out 2023-09-28
                                    $" SELECT VM.VENDORNM,VB.BANK,VB.BRANCHCODE,VB.ACCNR FROM {DRCDatabase}..VEND_MASTERS VM \n" // Jaco comment out 2023-09-28
                                  + $" INNER JOIN {DRCDatabase}..VENDOR_BANKHIST VB ON VB.VENDORID = VM.VENDORID \n"             // Jaco 2023-09-28
                                  + $" WHERE VM.VENDORID = '{_vendId}' AND VB.CURRHIST = 'C'";

                using (SqlDataReader drr = cmd.ExecuteReader())
                {
                    dataTable.Load(drr);
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    vr.accountNo = row["ACCNR"].ToString();
                    vr.branchCode = row["BRANCHCODE"].ToString();
                    vr.provId = row["VENDORNM"].ToString();
                    vr.bankName = row["BANK"].ToString();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;


        }

        //genrates a list of rejections codes that are allowed to be used by the client
        [HttpPost]
        [Route("GetRejectionCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<RejectionCodesModel> GetRejectionCodes([FromBody] RejectionCodesModel lobCode)
        {
            List<RejectionCodesModel> returnedList = new List<RejectionCodesModel>();
            DataTable dataTable = new DataTable();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string lob = "";

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            try
            {
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                //cmd.CommandText = $"SELECT lobcode FROM  UserLobCodesHpCodes where username = '{lobCode.username}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT u.lobcode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " +
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{lobCode.username}' ";

                using (SqlDataReader drow = cmd.ExecuteReader())
                {
                    dt.Load(drow);
                }
                foreach (DataRow rows in dt.Rows)
                {
                    lob = rows["lobcode"].ToString();

                }
                dt.Clear();
                cmd.CommandText = $"SELECT * FROM RejectionReasons WHERE LOBCODE = '{lob}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dataTable.Load(dr);
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    RejectionCodesModel reason = new RejectionCodesModel();
                    reason.code = row["ADJUST CODE"].ToString();
                    reason.reason = row["REASON"].ToString().ToUpper();
                    returnedList.Add(reason);

                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return returnedList;

        }

        //gets the type of checkrun eg. provider or member payments
        [HttpPost]
        [Route("CheckCheckRunType")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public bool CheckCheckRunType([FromBody] BatchDetailsModel batch)
        {
            bool isProvider;
            isProvider = false;
            string claimNo = "";
            string typeClaim = "";
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;

            try
            {
                cmd.CommandText = $"SELECT CLAIMNO FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batch.batchNumber}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    claimNo = row[0].ToString();
                    cmd.CommandText = $"SELECT CLAIMTYPE FROM CLAIM_MASTERS WHERE CLAIMNO = '{claimNo}'";
                    dtable.Clear();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtable.Load(dr);
                    }
                    foreach (DataRow type in dtable.Rows)
                    {
                        typeClaim = type[0].ToString();
                        if (typeClaim == "P")
                        {
                            isProvider = true;
                        }
                        else
                        {
                            isProvider = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return isProvider;
        }

        //gets the type of checkrun eg. provider or member payments
        [HttpPost]
        [Route("CheckCheckRunTypeArray")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<CheckRunTypeModel> CheckCheckRunTypeArray([FromBody] CheckRunTypeModel[] batch)
        {
            string claimNo = "";
            List<CheckRunTypeModel> returnList = new List<CheckRunTypeModel>();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            foreach (CheckRunTypeModel batchNo in batch)
            {
                try
                {
                    cmd.CommandText = $"SELECT CLAIMNO FROM " + this.DRCDatabase + $"..PAYBATCH_CLAIM WHERE BATCH = '{batchNo.batchNo}'";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        claimNo = row[0].ToString();
                        cmd.CommandText = $"SELECT CLAIMTYPE FROM CLAIM_MASTERS WHERE CLAIMNO = '{claimNo}'";
                        dtable.Clear();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dtable.Load(dr);
                        }
                        foreach (DataRow type in dtable.Rows)
                        {
                            batchNo.xtype = type[0].ToString();

                        }
                    }
                    returnList.Add(batchNo);
                    dtable.Clear();
                    dt.Clear();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }
            }


            cn.Close();
            return returnList;
        }

        //Marks every claim in batch valid
        [HttpPost]
        [Route("ValidateEntireBatch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateEntireBatch([FromBody] BatchDetailsModel batch)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;
            cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET VALIDATED = 1 WHERE BATCH = '{batch.batchNumber}'";
            cmd.ExecuteNonQuery();
            cn.Close();
            return vr;
        }

        //Marks every claim in batch unvalid
        [HttpPost]
        [Route("UnvalidateEntireBatch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UnvalidateEntireBatch([FromBody] BatchDetailsModel batch)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;
            cmd.CommandText = $"UPDATE " + this.DRCDatabase + $"..PAYBATCH_CLAIM SET VALIDATED = 0 WHERE BATCH = '{batch.batchNumber}'";
            cmd.ExecuteNonQuery();
            cn.Close();
            return vr;

        }

        //use to get details for detail popup in checkrun
        [HttpPost]
        [Route("GetDetailsOfClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DetailLinesModel> GetDetailsOfClaim([FromBody] CheckRunDetailsModel data)
        {
            DetailLinesModel returned = new DetailLinesModel();
            List<DetailLinesModel> lines = new List<DetailLinesModel>();
            DataTable dtable = new DataTable();
            DataTable datatable = new DataTable();
            DataTable tbldata = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                // Jaco comment out 2024-01-15
                //cmd.CommandText = "  SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_DETAILS.TBLROWID, CLAIM_DETAILS.BILLED, CLAIM_DETAILS.TARRIFREDUCT,CLAIM_DETAILS.PROCCODE, CLAIM_DETAILS.FROMDATESVC, CLAIM_DETAILS.TODATESVC, \n"
                //                         + "CLAIM_DETAILS.COPAY, CLAIM_MASTERS.DATERECD, CLAIM_DETAILS.ADJUST, \n"
                //                         + "CLAIM_DETAILS.ADJUSTWH AS ADJUST, CLAIM_DETAILS.NET,CLAIM_MASTERS.PROVCLAIM, CLAIM_DETAILS.DIAGCODE,DIAGDESC, \n"
                //                         + "dbo.ConcatToothNumbers(CLAIM_DETAILS.CLAIMNO, CLAIM_DETAILS.TBLROWID) as ClinicalCodes \n"
                //                         + "FROM CLAIM_MASTERS  INNER JOIN\n"
                //                         + "CLAIM_DETAILS ON CLAIM_MASTERS.CLAIMNO = CLAIM_DETAILS.CLAIMNO INNER JOIN DIAG_CODES DC ON DC.DIAGCODE = CLAIM_DETAILS.DIAGCODE \n"
                //                         + $"WHERE (CLAIM_MASTERS.CLAIMNO = '{data.claimNo}') AND TBLROWID = {data.rowId}";

                // Jaco 2024-01-15
                cmd.CommandText =   " SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_DETAILS.TBLROWID, CLAIM_DETAILS.BILLED, CLAIM_DETAILS.TARRIFREDUCT,CLAIM_DETAILS.PROCCODE, CLAIM_DETAILS.FROMDATESVC, CLAIM_DETAILS.TODATESVC,  \n" +
                                    " CLAIM_DETAILS.COPAY, CLAIM_MASTERS.DATERECD, CLAIM_DETAILS.ADJUST,  \n" +
                                    " CLAIM_DETAILS.ADJUSTWH AS ADJUST, CLAIM_DETAILS.NET,CLAIM_MASTERS.PROVCLAIM, CLAIM_DETAILS.DIAGCODE,DIAGDESC,  \n" +
                                    " dbo.ConcatToothNumbers(CLAIM_DETAILS.CLAIMNO, CLAIM_DETAILS.TBLROWID) as ClinicalCodes, PC.SVCDESC  \n" +
                                    " FROM CLAIM_MASTERS   \n" +
                                    " 		INNER JOIN CLAIM_DETAILS ON CLAIM_MASTERS.CLAIMNO = CLAIM_DETAILS.CLAIMNO  \n" +
                                    " 		LEFT JOIN DIAG_CODES DC ON DC.DIAGCODE = CLAIM_DETAILS.DIAGCODE  \n" +
                                    " 		LEFT JOIN PROC_CODES PC ON CLAIM_DETAILS.PHCODE = PC.PHCODE AND CLAIM_DETAILS.PROCCODE = PC.SVCCODE AND CLAIM_DETAILS.FROMDATESVC between PC.FROMDATE and isnull(PC.TODATE, '9999-12-31') \n" +
                                    " WHERE (CLAIM_MASTERS.CLAIMNO = @claimNo) AND TBLROWID = @tblRowId ";
               
                cmd.Parameters.Add(new SqlParameter("@claimNo", data.claimNo));
                cmd.Parameters.Add(new SqlParameter("@tblRowId", data.rowId));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                foreach (DataRow row in dtable.Rows)
                {
                    returned = new DetailLinesModel();
                    cmd.CommandText = $"SELECT [dbo].GetAdjustString('{data.claimNo}',{data.rowId})AS CODES";

                    using (SqlDataReader drows = cmd.ExecuteReader())
                    {
                        tbldata.Load(drows);
                    }
                    foreach (DataRow codes in tbldata.Rows)
                    {
                        returned.adjCode = codes["CODES"].ToString();
                    }
                    tbldata.Clear();
                    returned.diagcode = row["DIAGCODE"].ToString();
                    returned.diagdesc = row["DIAGDESC"].ToString();
                    returned.billed = row["BILLED"].ToString();
                    returned.coPay = row["COPAY"].ToString();
                    returned.fromDate = Convert.ToDateTime(row["FROMDATESVC"]).ToString("yyyy/MM/dd");
                    returned.tooDate = Convert.ToDateTime(row["TODATESVC"]).ToString("yyyy/MM/dd");
                    returned.provClaim = row["PROVCLAIM"].ToString();
                    returned.srvCode = row["PROCCODE"].ToString();
                    returned.srvCodeDesc = row["SVCDESC"].ToString();
                    DateTime dtRec = Convert.ToDateTime(row["DATERECD"]);
                    returned.dateRec = dtRec.ToString("yyyy/MM/dd");
                    returned.adjust = row["ADJUST"].ToString();
                    returned.tarrif = row["TARRIFREDUCT"].ToString();
                    returned.net = row["NET"].ToString();
                    returned.clinicalCodes = row["CLINICALCODES"].ToString();
                    lines.Add(returned);
                }
                dtable.Clear();

                cmd.CommandText = $"SELECT * FROM [CLAIM_ADJ] WHERE CLAIMNO = '{data.claimNo}' AND TBLROWID = {data.rowId}";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    datatable.Load(dr);
                }
                foreach (DataRow row in datatable.Rows)
                {
                    returned = new DetailLinesModel();
                    returned.claimNo = row["CLAIMNO"].ToString();
                    returned.tblRowID = row["TBLROWID"].ToString();
                    returned.adjCode2 = row["ADJCODE"].ToString();
                    returned.comments = row["COMMENTS"].ToString();
                    lines.Add(returned);
                }
                datatable.Clear();
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return lines;
        }

        //used to get the visit count per member
        [HttpPost]
        [Route("GetVisitClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public MemberNoModel GetVisitCount([FromBody] MemberNoModel membNo)
        {
            MemberNoModel num = new MemberNoModel();
            //string visitCounts = "";
            DataTable dtable = new DataTable();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            string prefix = "";

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 View is hardcoded to DRC
                //cmd.CommandText = $"SELECT HPCODE FROM  " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes where username = '{membNo.hpcode}'"; 

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + // Jaco tested 2023-11-23
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{membNo.hpcode}' ";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtbl.Load(dr);
                }
                foreach (DataRow row in dtbl.Rows)
                {
                    membNo.hpcode = row["HPCODE"].ToString();
                }
                string sql =
                    $" SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix, hp_c.LOBCODE \n" +
                    $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                    $"      {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n" +
                    $" WHERE ({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') AND {ReportingDatabase}..OxygenBenefitOptions.ezHpCode = '{membNo.hpcode}' ";
                cmd.CommandText = sql;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    prefix = row["MemberPrefix"].ToString();
                }
                dtable.Clear();

                // Jaco comment out 2023-09-27 - Database harcoded in view
                //cmd.CommandText = "SELECT TOP (1000) [MEMBID]\n"
                //                + "      ,[CONSULTATIONS]\n"
                //                + "  FROM [PamcPortal].[dbo].[GP_Consultations]\n"
                //                + $"WHERE MEMBID = '{prefix + membNo.memberNo}'";

                //Jaco 2023-09-27
                cmd.CommandText =  " SELECT MEMBID, CONSULTATIONS \n" +                            //Jaco checked 2023-11-23     
                                   " FROM ( SELECT      MEMBID,  SUM(cd.QTY) AS CONSULTATIONS, HPCODE \r\n" +
                                  $"        FROM {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \r\n" +
                                  $"             {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO INNER JOIN \r\n" +
                                  $"             {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \r\n" +
                                   "        WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \r\n" +
                                 //"            AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \r\n" + //Jaco 2024-07-12
                                  $"            AND 1 = ( \r\n" +
                                  $"                              CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                                  $"                                   WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                                  $"                                   ELSE 0 \r\n" +
                                  $"                               END \r\n" +
                                  $"					   ) \r\n" +
                                   "            AND(cm.SPEC IN('14', '15')) \r\n" +
                                   "            AND cm.CONTRACT = \r\n" +
                                 //"            CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN '2' ELSE cm.CONTRACT END \r\n" + // Jaco 2024-07-25
                                   "            CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE cm.CONTRACT END \r\n" +
                                   "            AND(cd.NET <> 0) \r\n" +
                                 //"           AND cd.PHCODE = CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN 'P' ELSE cd.PHCODE END \r\n" + // Jaco 2024-07-25
                                   "           AND cd.PHCODE = 'P' \r\n" +
                                   "            GROUP BY MEMBID, HPCODE  \r\n" +
                                   "        ) as GP_Consultations \r\n" +
                                  $" WHERE MEMBID = '{prefix + membNo.memberNo}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                foreach (DataRow row in dtable.Rows)
                {
                    num.memberNo = row["CONSULTATIONS"].ToString();
                }
                dtable.Clear();
                if (num.memberNo == null)
                {
                    num.memberNo = "0";
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return num;

        }

        //use to get the details of the referring prov for the claim details popup
        [HttpPost]
        [Route("GetDetailsOfRefProv")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public DetailLinesModel GetDetailsOfRefProv([FromBody] CheckRunDetailsModel data)
        {
            DetailLinesModel lines = new DetailLinesModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                cmd.CommandText = "SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_MASTERS.REFPROVID, ISNULL(PROV_MASTERS.FIRSTNAME, '') + ' ' + PROV_MASTERS.LASTNAME AS NAME,\n"
                           + " PROV_SPECINFO.SPECCODE, PROV_SPECCODES.DESCR\n"
                           + "FROM CLAIM_MASTERS INNER JOIN\n"
                           + "PROV_MASTERS ON CLAIM_MASTERS.REFPROVID = PROV_MASTERS.PROVID INNER JOIN\n"
                           + "PROV_SPECINFO ON PROV_MASTERS.PROVID = PROV_SPECINFO.PROVID INNER JOIN\n"
                           + "PROV_SPECCODES ON PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE \n"                          
                           + $"WHERE (CLAIM_MASTERS.CLAIMNO = @claimNo)";

                cmd.Parameters.Add(new SqlParameter("@claimNo", data.claimNo));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow rows in dt.Rows)
                {
                    lines.claimNo = rows["CLAIMNO"].ToString();
                    lines.refProvId = rows["REFPROVID"].ToString();
                    lines.refProfName = rows["NAME"].ToString();
                    lines.specCode = rows["SPECCODE"].ToString();
                    lines.desc = rows["DESCR"].ToString();
                }

                dt.Clear();


            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            cn.Close();
            return lines;
        }

        //use to get auth no of claim to be displayed in the claim detail popup
        [HttpPost]
        [Route("GetAuthNo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetAuthNo([FromBody]ValidationResultsModel claimNo)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                cmd.CommandText = $"SELECT AUTHNO FROM CLAIM_MASTERS WHERE CLAIMNO = '{claimNo.message}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow rows in dtable.Rows)
                {
                    vr.message = rows["AUTHNO"].ToString();
                }

                if ((DBNull.Value.Equals(vr.message)) || (vr.message == ""))
                {
                    vr.message = "No Auth Number";
                }

                dtable.Clear();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                throw;
            }
            cn.Close();
            return vr;
        }

        //used to export the batch to an excl sheet
        [HttpPost]
        [Route("PrintCurrentBatch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel PrintCurrentBatch([FromBody] BatchDetailsModel printfile)
        {
            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : ExportBatch---------------\r\n";
            ValidationResultsModel vr = new ValidationResultsModel();
            string fileName = "template.xlsx";
            var root = _env.ContentRootPath;
            DirectoryInfo di = new DirectoryInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet");
            FileInfo f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
            List<CheckRunModel> filteredList = new List<CheckRunModel>();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();
            DataTable tbl = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            bool dirExist = false;
            string hpcode = "";

            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            try
            {
                if (di.Exists)
                {
                    dirExist = true;
                }
                else
                {
                    dirExist = false;
                }

                // Jaco 2023-09-11 comment out - View is hardcoded to DRC
                //string sql = $"SELECT HPCODE FROM " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes WHERE username = '{printfile.hpCode}'"; 
                //cmd.CommandText = sql;

                // Jaco 2023-09-11 
                cmd.CommandText = $" SELECT hp.HPCODE " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + //Jaco tested 2023-11-29
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{printfile.hpCode}' ";

                using (SqlDataReader drs = cmd.ExecuteReader())
                {
                    dtbl.Load(drs);
                }
                foreach (DataRow rows in dtbl.Rows)
                {
                    hpcode = rows["HPCODE"].ToString();
                }

                dtbl.Clear();
                if (dirExist)
                {
                    switch (hpcode)
                    {                        
                        case "GETS":
                            fileName = "GETSAVVI.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "SANL":
                        case "ESSE":
                            fileName = "ESSENTIALMED.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "EEBS":
                            fileName = "EEBS.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "WESM":
                            fileName = "WESMART.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "GENR":
                            fileName = "GENRIC.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        default:
                            fileName = "EEBS.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\src\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                    }
                }
                else
                {
                    switch (hpcode)
                    {
                        case "GETS":
                            fileName = "GETSAVVI.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "ESSE":
                            fileName = "ESSENTIALMED.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "EEBS":
                            fileName = "EEBS.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "WESM":
                            fileName = "WESMART.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        case "GENR":
                            fileName = "GENRIC.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                        default:
                            fileName = "EEBS.xlsx";
                            f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CheckRunSheet\\{ fileName }");
                            break;
                    }
                }


                string sql = $"SELECT PAYREADY FROM PAYBATCH WHERE BATCH = '{printfile.batchNumber}'";

                cmd.CommandText = sql;

                bool submit = false;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    tbl.Load(dr);
                }
                foreach (DataRow row in tbl.Rows)
                {
                    submit = Convert.ToBoolean(row["PAYREADY"].ToString());
                }

                //and[Date of Incident] between OPFROMDT and isnull(OPTHRUDT, [Date of Incident])

                sql = GetBrExtractSql(printfile.batchNumber, submit, "", DRCDatabase, ReportingDatabase);


                cmd.CommandText = sql;

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----BatchNo: {printfile.batchNumber}---------------\r\n";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                try
                {
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Creating Excel File---------------\r\n";

                    using (var current = new ExcelPackage(f))
                    {
                        ExcelPackage.LicenseContext = LicenseContext.Commercial;
                        ExcelWorksheet worksheet = current.Workbook.Worksheets[0];
                        int startRow = worksheet.Dimension.Start.Row + 1;
                        int endRow = worksheet.Dimension.End.Row;
                        int startCol = worksheet.Dimension.Start.Column;
                        int endCol = worksheet.Dimension.End.Column;
                        for (int i = startRow; i <= endRow; i++)
                        {
                            for (int j = startCol; j <= endCol; j++)
                            {
                                worksheet.Cells[i, j].Clear();
                            }
                        }
                        int rows = 2;
                        int cols = 1;

                        worksheet.Cells[worksheet.Dimension.Start.Column.ToString() + ":" + worksheet.Dimension.End.Column.ToString()].AutoFilter = true;
                        switch (hpcode)
                        {
                            case "GETS":

                                foreach (DataRow row in dt.Rows)
                                {
                                    worksheet.Cells[rows, cols].Value = row["Main Policy Number (policy owner)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Principle / Main Member number (applicable to schemes)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Name & Surname of principle member / policy holder"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ID number of member / policy holder"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Type"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["SPECIALTY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["REFPROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["SPEC"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["SPECIALTY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVIDER NAME"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Description"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC"].ToString();
                                    cols++;

                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code3"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC3"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code4"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC4"].ToString();
                                    cols++;

                                    worksheet.Cells[rows, cols].Value = row["Claim Number"].ToString();
                                    cols++;
                                    // log += $"--------------------------------------Loging Date Format {row["Date of Incident"].ToString()} --------CLAIM NO: {row["Claim Number"].ToString()}------------------------------\r\n\n";
                                    worksheet.Cells[rows, cols].Value = row["Date of Incident"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Member Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Name & Surname of deceased / patient"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ID number of deceased / patient"].ToString();
                                    // log += $"--------------------------------------Loging Date Format {row["Date of claim reported / received"].ToString()} --------CLAIM NO: {row["Claim Number"].ToString()}------------------------------\r\n\n";

                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim reported / received"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim reported"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim approved"];
                                    // log += $"--------------------------------------Loging Date Format {row["Date of claim paid"].ToString()} --------CLAIM NO: {row["Claim Number"].ToString()}------------------------------\r\n\n";
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Status"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim amount outstanding"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Name"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Branch Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAYMENT BATCH NO"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ReasonCode1"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ReasonCode2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPT"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth1"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth3"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth4"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth5"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth6"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth7"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Tooth8"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = "";
                                    cols++;
                                    if (row["VALIDATED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["VALIDATED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    cols = 1;
                                    rows++;
                                };
                                break;
                            case "SANL":
                            case "ESSE":
                                foreach (DataRow row in dt.Rows)
                                {
                                    worksheet.Cells[rows, cols].Value = row["Main Policy Number (policy owner)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPFROMDT"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim reported"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim approved"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientFirstNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientLastNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Patient"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPT"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of Incident"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Description"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = "";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVIDER NAME"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAYMENT BATCH NO"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim reported / received"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Type"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC"].ToString();
                                    cols++;

                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC2"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code3"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC3"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code4"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC4"].ToString();
                                    cols++;

                                    worksheet.Cells[rows, cols].Value = row["ReasonCode1"].ToString() + "  " + row["ReasonCode2"].ToString();
                                    cols++;
                                    if (row["VALIDATED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["VALIDATED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    cols = 1;
                                    rows++;
                                };
                                break;

                            case "EEBS":
                                foreach (DataRow row in dt.Rows)
                                {
                                    worksheet.Cells[rows, cols].Value = row["Main Policy Number (policy owner)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPFROMDT"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim reported"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim approved"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientFirstNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientLastNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Patient"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPT"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of Incident"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Description"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = "";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVIDER NAME"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAYMENT BATCH NO"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim reported / received"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Type"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ReasonCode1"].ToString() + "  " + row["ReasonCode2"].ToString();
                                    cols++;
                                    if (row["VALIDATED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["VALIDATED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    cols = 1;
                                    rows++;
                                };
                                break;
                            case "WESM":
                            case "GENR":
                                foreach (DataRow row in dt.Rows)
                                {
                                    worksheet.Cells[rows, cols].Value = row["Main Policy Number (policy owner)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPFROMDT"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of Incident"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim reported"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim approved"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientFirstNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientLastNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Patient"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["SPEC"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVIDER NAME"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAYMENT BATCH NO"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Name"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Branch Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Bank Account Type"].ToString();
                                    cols++;
                                    if (row["VALIDATED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["VALIDATED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    cols = 1;
                                    rows++;
                                };
                                break;
                            default:
                                foreach (DataRow row in dt.Rows)
                                {
                                    worksheet.Cells[rows, cols].Value = row["Main Policy Number (policy owner)"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPFROMDT"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim reported"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Amount of claim approved"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "R ###,###,##0.00";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim paid"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientFirstNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PatientLastNm"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Patient"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["OPT"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Number"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of Incident"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Product Description"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = "";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAY"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVID"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PROVIDER NAME"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["PAYMENT BATCH NO"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Date of claim reported / received"];
                                    worksheet.Cells[rows, cols].Style.Numberformat.Format = "yyyy/MM/dd";
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Claim Type"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["Diagnosis Code"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["DIAGDESC"].ToString();
                                    cols++;
                                    worksheet.Cells[rows, cols].Value = row["ReasonCode1"].ToString() + "  " + row["ReasonCode2"].ToString();
                                    cols++;
                                    if (row["VALIDATED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["VALIDATED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "FALSE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "NO";
                                        cols++;
                                    }
                                    if (row["EXCLUDED"].ToString().ToUpper() == "TRUE")
                                    {
                                        worksheet.Cells[rows, cols].Value = "YES";
                                        cols++;
                                    }
                                    cols = 1;
                                    rows++;
                                };
                                break;

                        }


                        worksheet.Cells.AutoFitColumns(0);
                        try
                        {
                            current.SaveAs(f);
                        }
                        catch (Exception e)
                        {
                            LogError(e.Message, e.StackTrace);
                            log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----General error saving file: {e.Message}  \r\n StackTrace: {e.StackTrace}---\r\n";
                            var msg = e.Message;
                        }
                        vr.message = f.Name;
                    }
                    cn.Close();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----General error opening file: {e.Message}  \r\n StackTrace: {e.StackTrace}---\r\n";
                    throw;
                }



            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----OverAll error  {e.Message}  \r\n StackTrace: {e.StackTrace}---\r\n";
                var msg = e.Message;
            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----END: ExportBatch---\r\n";
            }
            cn.Close();
            return vr;
        }

        //used to set the payment date of the batch
        [HttpPost]
        [Route("UpdatePaymentDate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdatePaymentDate([FromBody] BatchDetailsModel batch)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;

            cmd.Connection = cn;
            string date;
            date = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" +
                      DateTime.Now.Year.ToString();
            date = date + " " + DateTime.Now.Hour.ToString() + ":" +
                   DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            DateTime paydate = DateTime.Parse(batch.paydate);

            batch.paydate = paydate.Day.ToString() + '/' + paydate.Month.ToString() + '/' + paydate.Year.ToString();

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.CommandText = $"UPDATE PAYBATCH SET LASTCHANGEBY = 999,LASTCHANGEDATE = CONVERT(DATETIME,'{date}',103), DATETOPAY = CONVERT(DATETIME,'{batch.paydate}',103) WHERE BATCH = '{batch.batchNumber}'";
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        //used to change the status of the claims when a unpaid batch is being edited
        [HttpPost]
        [Route("ChangetoEditStatus")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ChangetoEditStatus([FromBody] CheckRunModel[] claims)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();


            cn.ConnectionString = _drcConnectionString;

            string date;
            date = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" +
                      DateTime.Now.Year.ToString();
            date = date + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            try
            {
                cmd.Connection = cn;
                foreach (CheckRunModel claim in claims)
                {
                    cmd.CommandText = $"UPDATE CLAIM_MASTERS SET STATUS = '1',  LASTCHANGEBY = 999, LASTCHANGEDATE = {date} WHERE CLAIMNO = '{claim.claimNumber}'";
                    // cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        //used to change the status of the claims when a unpaid batch is being edited
        [HttpPost]
        [Route("ChangetoPendingStatus")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ChangetoPendingStatus([FromBody] CheckRunModel[] claims)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _drcConnectionString;

            string date;
            date = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" +
                      DateTime.Now.Year.ToString();
            date = date + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();


            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }
            cmd.Connection = cn;

            try
            {
                foreach (CheckRunModel claim in claims)
                {
                    cmd.CommandText = $"UPDATE CLAIM_MASTERS SET STATUS = '4',  LASTCHANGEBY = 999, LASTCHANGEDATE = {date} WHERE CLAIMNO = '{claim.claimNumber}'";
                    // cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        //use to get the lasts entered pay date for the batch by the user
        [HttpPost]
        [Route("GetLastPayedDate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetLastPayedDate([FromBody] BatchDetailsModel batch)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;

            try
            {
                cmd.CommandText = $"SELECT DATETOPAY FROM PAYBATCH WHERE BATCH = '{batch.batchNumber}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    vr.message = row["DATETOPAY"].ToString();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            cn.Close();
            return vr;
        }

        private static string GetBrExtractSql(string batchNumber, bool validateOnly, string hpCode, string DRCDatabase, string ReportingDatabase)
        {
            string validateOnlyString = validateOnly ? " 1" : "0"; //Jaco 2023-11-24 tested
            string sql = "declare @prefix varchar(10)\n"
                       + "\n"
                       + "DECLARE @PAYBATCH VARCHAR(20)\n"
                       + "DECLARE @VALIDATEONLY BIT\n"
                       + "----*************************************\n"
                       + $"SET @VALIDATEONLY = {validateOnlyString}\n"
                       + $"SET @PAYBATCH = '{batchNumber}'  \n"

                       // Jaco comment out 2023-09-28 - Databases is hardcoded in GetMemberPrefix function
                       //+ $"SET @prefix = isnull(PamcPortal.dbo.GetMemberPrefix('{hpCode}'), '')\n"
                       
                       // Jaco 2023-09-28
                       + $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n"  // Jaco tested 2023-11-23
                       + $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" 
                       + $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" 
                       + $" where OxygenBenefitOptions.ezHpCode = '{hpCode}' AND OxygenBenefitOptions.scheme <> 'carecross' \n"

                       + "----*************************************\n"
                       + "CREATE TABLE [dbo].[#tmpBR](\n"
                       + "                         [PHCODE] [varchar](1) NULL,\n"
                       + "                         [Main Policy Number (policy owner)] [varchar](20) NULL,\n"
                       + "                         [Member Number] [varchar](20) NULL,\n"
                       + "                         [Principle / Main Member number (applicable to schemes)] [varchar](20) NULL,\n"
                       + "                         [Name & Surname of principle member / policy holder] [varchar](61) NULL,\n"
                       + "                         [ID number of member / policy holder] [varchar](15) NULL,\n"
                       + "                         [Claim Type] [varchar](30) NULL,\n"
                       + "                         [PROVID] [varchar](20) NULL,\n"
                       + "                         [SPECIALTY] [varchar](30) NULL,\n"
                       + "                         [PROVIDER NAME] [varchar](56) NULL,\n"
                       + "                         [REFPROVID] [varchar](20) NULL,\n"
                       + "                         [Product Description] [varchar](200) NULL,\n"
                       + "                         [Product Code] [varchar](20) NULL,\n"
                       + "                         [Claim Number] [varchar](16) NULL,\n"
                       + "                         [Date of Incident] [datetime] NULL,\n"
                       + "                         [Name & Surname of deceased / patient] [varchar](30) NULL,\n"
                       + "                         [ID number of deceased / patient] [varchar](15) NULL,\n"
                       + "                         [Date of claim reported / received] [datetime] NULL,\n"
                       + "                         [Amount of claim reported] [decimal](10, 2) NULL,\n"
                       + "                         [Amount of claim approved] [decimal](15, 2) NULL,\n"
                       + "                         [Date of claim paid] [datetime] NULL,\n"
                       + "                         [PAYMENT BATCH NO] [int] NULL,\n"
                       + "                         [Amount of claim paid] [decimal](15, 2) NULL,\n"
                       + "                         [Claim Status] [varchar](6) NULL,\n"
                       + "                         [Claim amount outstanding] [int] NULL,\n"
                       + "                         [Bank Account Name] [varchar](30) NULL,\n"
                       + "                         [Bank Account Branch Code] [varchar](10) NULL,\n"
                       + "                         [Bank Account Number] [varchar](20) NULL, \n "
                       + "                         [Bank Account Type] [varchar](20) NULL,\n"
                       + "                         [ReasonCode1] [varchar](100) NULL,\n"
                       + "                         [ReasonCode2] [varchar](100) NULL,\n"
                       + "                         [TBLROWID] [int] NULL,\n"
                       + "                         [OPT] [varchar](20) NULL,\n"
                       + "                         [SUBSSN] [varchar](20) NULL,\n"
                       + "                         [subssncm] [varchar](20) NULL,\n"
                       + "                         [Diagnosis Code] [varchar](10) NULL,\n"
                       + "                         [DIAGDESC][varchar](100) NULL, \n"
                       + "                         [Diagnosis Code2] [varchar](10) NULL, \n"
                       + "                         [DIAGDESC2] [varchar](100) NULL, \n"
                       + "                         [Diagnosis Code3] [varchar](10) NULL, \n"
                       + "                         [DIAGDESC3] [varchar](100) NULL, \n"
                       + "                         [Diagnosis Code4] [varchar](10) NULL, \n"
                       + "                         [DIAGDESC4] [varchar](100) NULL, \n"                   
                       + "                         [Commencement Date] [datetime]  NULL,\n"
                       + "                         [Patient] [varchar](20) NULL,\n"
                       + "                         [PatientFirstNm] [varchar](30) NULL,\n"
                       + "                         [PatientLastNm] [varchar](30) NULL,\n"
                       + "                         [DIAG] [varchar](10) NULL,\n"
                       + "                         [PatientBirth] [datetime] NULL,\n"
                       + "                         [ROWID] [decimal](18, 0) NULL,\n"
                       + "                         [HPCODE] [varchar](4) NULL,\n"
                       + "                         [SPEC] [varchar](3) NULL,\n"
                       + "                         [PAY] [varchar](1) NULL,\n"
                       + "                         [MEMBID] [varchar](20) NULL,\n"
                       + "                         [VENDOR] [varchar](20) NULL,\n"
                       //+ "                         [DIAGDESC] [varchar](1000) NULL,\n"
                       + "                         [TOOTH1] [varchar](3) NULL,\n"
                       + "                         [TOOTH2] [varchar](3) NULL,\n"
                       + "                         [TOOTH3] [varchar](3) NULL,\n"
                       + "                         [TOOTH4] [varchar](3) NULL,\n"
                       + "                         [TOOTH5] [varchar](3) NULL,\n"
                       + "                         [TOOTH6] [varchar](3) NULL,\n"
                       + "                         [TOOTH7] [varchar](3) NULL,\n"
                       + "                         [TOOTH8] [varchar](3) NULL,\n"
                       + "                         [OPFROMDT] [datetime] NULL,\n"
                       + "                         [MEMB] [varchar](20) NULL,\n"
                       + "						 [FAMILY] [varchar](20) NULL,\n"
                       + "						 VALIDATED BIT NULL,\n"
                       + "						 EXCLUDED BIT NULL,\n"
                       + "						 REJECTED BIT NULL,\n"
                       + "          \n"
                       + "           \n"
                       + "           \n"
                       + "           ) ON [PRIMARY]\n"
                       + "            \n"
                       + "           \n"
                       + "           insert into [#tmpBR]([PHCODE],[Main Policy Number (policy owner)],[Member Number],[Principle / Main Member number (applicable to schemes)],[Claim Type],[PROVID],[SPECIALTY],[PROVIDER NAME],[REFPROVID],[Product Code],[Claim Number],[Date of Incident],[Name & Surname of deceased / patient],[Date of claim reported / received],[Amount of claim reported],[Amount of claim approved],[Date of claim paid],[PAYMENT BATCH NO],[Amount of claim paid],[Claim Status],[Claim amount outstanding],[ReasonCode1],[ReasonCode2],[TBLROWID],[OPT],[SUBSSN], \n" 
                       + "           [Diagnosis Code],[DIAGDESC],[Diagnosis Code2],[DIAGDESC2],[Diagnosis Code3],[DIAGDESC3],[Diagnosis Code4],[DIAGDESC4],[Patient],[DIAG],[ROWID],[HPCODE],[SPEC],[PAY], \n"
                       + "		   MEMBID, VENDOR, MEMB,FAMILY,VALIDATED, EXCLUDED,REJECTED)\n"
                       + "           SELECT        cd.PHCODE, SUBSTRING(cm.SUBSSN, 2, 10) AS [Main Policy Number (policy owner)], cm.MEMBID AS [Member Number],SUBSTRING(cm.SUBSSN, 2, 10) AS [Principle / Main Member number (applicable to schemes)], bc.DESCR AS [Claim Type], cm.PROVID, \n"
                       + "                                    spc.DESCR AS SPECIALTY, ISNULL(pm.FIRSTNAME, '') + ' ' + pm.LASTNAME AS [PROVIDER NAME], cm.REFPROVID, LTRIM(cd.PROCCODE) AS [Product Code], cm.CLAIMNO AS [Claim Number], \n"
                       + "                                    cd.FROMDATESVC AS [Date of Incident], cm.MEMBNAME AS [Name & Surname of deceased / patient], cm.DATERECD AS [Date of claim reported / received], cd.BILLED AS [Amount of claim reported], \n"
                       + "                                    cd.NET AS [Amount of claim approved], cm.DATEPAID AS [Date of claim paid], cm.CHPREFIX AS [PAYMENT BATCH NO], cd.NET AS [Amount of claim paid], \n"
                       + "                                    CASE WHEN CM.STATUS = '9' THEN 'PAID' ELSE 'UNPAID' END AS [Claim Status], 0 AS [Claim amount outstanding], ISNULL(ca1.COMMENTS, '') AS ReasonCode1, ISNULL(ca2.COMMENTS, '') AS ReasonCode2, cd.TBLROWID, \n"
                       + "                                    cm.OPT, cm.SUBSSN, D1.DIAG AS [Diagnosis Code], D1.DIAGDESC AS [DIAGDESC], D2.DIAG AS[Diagnosis Code2], D2.DIAGDESC AS[DIAGDESC2], D3.DIAG AS[Diagnosis Code3], D3.DIAGDESC AS[DIAGDESC3], D4.DIAG AS[Diagnosis Code4], D4.DIAGDESC AS[DIAGDESC4], \n"
                       + "                                    cm.MEMBID AS Patient, cd.DIAGCODE AS DIAG, cm.ROWID, cm.HPCODE, cm.SPEC,  cm.CLAIMTYPE AS PAY, CM.MEMBID,\n"
                       + "                                    CM.VENDOR,\n"
                       + "									 CASE WHEN @prefix = '' THEN cm.MEMBID ELSE REPLACE(cm.MEMBID, @prefix, '') END AS MEMB,\n"
                       + "  CASE WHEN  @prefix = ''  THEN SUBSTRING(cm.SUBSSN, 2, 10)\n"
                       + " ELSE REPLACE(SUBSTRING(cm.SUBSSN, 2, 10),\n"
                       + " @prefix , '') END AS FAMILY,ISNULL(VALIDATED,0)VALIDATED, ISNULL(EXCLUDED,0)EXCLUDED, ISNULL(REJECTED,0)REJECTED\n"
                       + "           FROM            CLAIM_MASTERS AS cm INNER JOIN\n"
                       + "                                    CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO INNER JOIN\n"
                       + "                                    PROV_MASTERS AS pm ON cm.PROVID = pm.PROVID INNER JOIN\n"
                       + "                                    PAYBATCH_CLAIM AS pb ON cm.CLAIMNO = pb.CLAIMNO LEFT OUTER JOIN\n"
                       + "                                    BENTRACK_CODES AS bc ON cd.BENTYPE = bc.CODE LEFT OUTER JOIN\n"
                       + "                                    PROV_SPECCODES AS spc ON cm.SPEC = spc.CODE LEFT OUTER JOIN\n"
                       + "                                    CLAIM_ADJUSTS AS ca1 ON cd.CLAIMNO = ca1.CLAIMNO AND cd.TBLROWID = ca1.CLAIMTBLROW AND ca1.SEQUENCE = 1 LEFT OUTER JOIN\n"
                       + "                                    CLAIM_ADJUSTS AS ca2 ON cd.CLAIMNO = ca2.CLAIMNO AND cd.TBLROWID = ca2.CLAIMTBLROW AND ca1.SEQUENCE = 2\n"
                       + "                                    LEFT OUTER JOIN CLAIM_DIAGS D1 on cd.claimno = D1.claimno and D1.diagrefno = 1 \n"
                       + "                                    LEFT OUTER JOIN CLAIM_DIAGS D2 on cd.claimno = D2.claimno and D2.diagrefno = 2 \n"
                       + "                                    LEFT OUTER JOIN CLAIM_DIAGS D3 on cd.claimno = D3.claimno and D3.diagrefno = 3 \n"
                       + "                                    LEFT OUTER JOIN CLAIM_DIAGS D4 on cd.claimno = D4.claimno and D4.diagrefno = 4 \n"
                       + "           where pb.BATCH =  @PAYBATCH  --**********************************\n"
                       + "            AND isnull(pb.VALIDATED,0) =CASE WHEN @VALIDATEONLY = 1 THEN 1 ELSE  isnull(pb.VALIDATED,0) END AND cm.STATUS <>  9\n"
                       + "            \n"
                       + "           update t\n"
                       + "           set  [Name & Surname of principle member / policy holder] = mm.FIRSTNM + ' ' + mm.LASTNM ,\n"
                       + "            [ID number of member / policy holder]  = mm.PATID ,\n"
                       + "             subssncm = mm.SUBSSN ,\n"
                       + "              [Commencement Date] = mh.HPFROMDT\n"
                       + "           from #tmpBR t inner join memb_masters mm on mm.SUBSSN                   =             t.SUBSSN and rlship = '1'                \n"
                       + "           INNER JOIN memb_hphists mh on mm.MEMBID = mh.MEMBID and CURRHIST = 'C' \n"
                       + "           \n"
                       + "           update t\n"
                       + "           set  PatientFirstNm = mm.FIRSTNM ,\n"
                       + "           PatientLastNm = mm.LASTNM ,\n"
                       + "            [ID number of deceased / patient]   = mm.PATID ,\n"
                       + "            PatientBirth = mm.BIRTH,\n"
                       + "              [OPFROMDT] = mh.OPFROMDT\n"
                       + "            from #tmpBR t                inner join memb_masters mm on mm.MEMBID                 =             t.MEMBID           \n"
                       + "            INNER JOIN memb_hphists mh on mm.MEMBID = mh.MEMBID and CURRHIST = 'C' \n"
                       + "            \n"
                       + "            update t\n"
                       + "            set [Bank Account Name] = ISNULL(BANK,''),\n"
                       + "                         [Bank Account Branch Code] =  ISNULL(BRANCHCODE,''),\n"
                       + "                         [Bank Account Number] = ISNULL(ACCNR,''),\n "
                       + "                         [Bank Account Type] = ISNULL(ACCTYPE,'')"
                       + "             from #tmpBR t               inner join VEND_MASTERS vm on vm.VENDORID=             t. VENDOR          \n"
                       + "                           \n"
                       + "           update t\n"
                       + "            set [Product Description] = M.SVCDESC\n"
                       + "             from #tmpBR t               inner join MEDICINE_CODES M on LTRIM(M.SVCCODE)=                t.[Product Code] AND M.PHCODE = T.PHCODE \n"
                       + "             AND M.TODATE IS  NULL\n"
                       + "           \n"
                       + "           update t\n"
                       + "            set [Product Description] = M.SVCDESC\n"
                       + "             from #tmpBR t               inner join SERVICE_CODES M on LTRIM(M.SVCCODE)=    t.[Product Code] AND M.PHCODE = T.PHCODE \n"
                       + "             AND M.TODATE IS  NULL\n"
                       //+ "           \n"
                       //+ "             update t\n"
                       //+ "            set DIAGDESC = M.DIAGDESC\n"
                       //+ "             from #tmpBR t               inner join DIAG_CODES M on t.DIAG =    m.DIAGCODE \n"
                       //+ "           \n"
                       //+ "               update t\n"
                       //+ "            set DIAGDESC = M.DIAGDESC\n"
                       //+ "             from #tmpBR t               inner join DIAG_CODES M on t.DIAG =    m.DIAGCODE  \n"
                       //+ "           \n"
                       + "                 update t\n"
                       + "            set TOOTH1 = M.Tooth1,\n"
                       + "            TOOTH2 = M.Tooth2,\n"
                       + "            TOOTH3 = M.Tooth3,\n"
                       + "            TOOTH4 = M.Tooth4,\n"
                       + "            TOOTH5 = M.Tooth5,\n"
                       + "            TOOTH6 = M.Tooth6,\n"
                       + "            TOOTH7 = M.Tooth7,\n"
                       + "            TOOTH8 = M.Tooth8\n"
                       + "            \n"
                       + "             from #tmpBR t               inner join CLAIM_CLINICALCODES_TRANSPOSED M on t.[Claim Number] =                 m.CLAIMNO AND T.TBLROWID = M.CLAIMTBLROW\n"
                       + "            \n"
                       + "           select * from #tmpBR ORDER BY [Claim Number], TBLROWID\n"
                       + "            \n"
                       + "           DROP TABLE #tmpBR\n"
                       + "           ";
            return sql;
        }

        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "CHECKRUN";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\CHECKRUN\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception )
            {


            }


        }
    }

}
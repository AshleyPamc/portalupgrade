﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using WebApplication1.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Base.PAMCController
    {
        public LoginController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        /*Validate Login Details*/
        [HttpPost]
        [Route("LoginDetails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel LoginDetails([FromBody]LoginDetailsModel model)
        {

            OperationLog log = new OperationLog();

            log.LogLogin("Login", DateTime.Now, model.username, _portalConnectionString);

            try
            {

                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {

                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        //string sqlTextToExecute = "select count(*) from users where Username = '" + model.username + "' and Password = '" + model.password + "'";
                        string sqlTextToExecute = "select count(*) from users where Username = @username and Password = @password";

                        cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.username;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.password;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        if (o != null)
                        {
                            model.success = Convert.ToBoolean(o);
                        }


                        if (model.success == true)
                        {
                            sqlTextToExecute = "SELECT * from users WHERE (Users.Username = @username)";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            DataTable dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            DataRow dr = dt.Rows[0];

                            model.username = dr["Username"].ToString();
                            model.Firstname = dr["name"].ToString();
                            model.password = model.password;
                            model.userType = Convert.ToInt32(dr["UserType"]);
                            model.createBy = dr["createBy"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CreateDate"]);
                            model.changeBy = dr["ChangeBy"].ToString();
                            model.changeDate = Convert.ToDateTime(dr["ChangeDate"]);
                            model.provID = dr["provId"].ToString();
                            model.bureauId = dr["bureauId"].ToString();
                            model.lobcode = dr["lobcode"].ToString();
                            model.client = dr["client"].ToString();
                            if (dr["memberIdnumber"] != DBNull.Value)
                            {
                                model.memberIdNumber = dr["memberIdnumber"].ToString();
                            }
                            if (dr["memberNumber"] != DBNull.Value)
                            {
                                model.memberNumber = dr["memberNumber"].ToString();
                            }
                            if (dr["brokerId"] != DBNull.Value)
                            {
                                model.brokerId = dr["brokerId"].ToString();
                            }
                            if (model.userType == 0)
                            {
                                if (_allowAdmin)
                                {
                                    model.success = true;
                                }
                                else
                                {
                                    model.success = false;
                                }
                            }
                        }




                        if (model.userType == 1 || model.userType == 6)
                        {
                            sqlTextToExecute = ProviderDetails(model);

                        }
                        if (model.userType == 3)
                        {
                            model.lobdesc = Utillity.LobDesc(model.lobcode, _drcConnectionString);
                        }

                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                model.errorMessage = ex.InnerException.Message;
            }



            return model;

        }

        /*Validates if user is logged in for the first time*/
        [HttpPost]
        [Route("CheckTandC")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public bool CheckTandC([FromBody] LoginDetailsModel model)
        {
            bool accepted = false;
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();

            cn.ConnectionString = _portalConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT ISNULL(TCaccepted,'false') AS TCaccepted  FROM Users WHERE username = '{model.username}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    accepted = Convert.ToBoolean(row["TCaccepted".ToString()]);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return accepted;

        }

        /*Updates Database when user logs in for the first time*/
        [HttpPost]
        [Route("UpdateTCAccepted")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public bool UpdateTCAccepted([FromBody] LoginDetailsModel model)
        {
            bool done = false;

            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();

            cn.ConnectionString = _portalConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"UPDATE Users SET TCaccepted = 1, changeBy = '{model.username}', changeDate = '{DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss")}' WHERE username = '{model.username}'";
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            return done;
        }

        /*Gets user rights assigned to the specific user who is logged in*/

        [HttpPost]
        [Route("GetUserRights")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public UserRightsModel GetUserRights([FromBody]LoginDetailsModel model)
        {

            UserRightsModel rights = new UserRightsModel();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();
            List<string> objectIds = new List<string>();

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            try
            {
                cmd.Connection = cn;
                cmd.CommandText = $"SELECT OBJECTID FROM USER_RIGHTS WHERE USERNAME = '{model.username}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    objectIds.Add(row["OBJECTID"].ToString());
                }

                string _id = "";
                foreach (string id in objectIds)
                {
                    _id = id.TrimEnd();
                    switch (_id)
                    {
                        case "BL00":
                            rights.benefitlookup = true;
                            break;
                        case "CC00":
                            rights.claimCaptureTab = true;
                            break;
                        case "CR00":
                            rights.checkrunTab = true;
                            break;
                        case "CSREJ00":
                            rights.claimSearchRejection = true;
                            break;
                        case "CR01":
                            rights.releaseCheckrun = true;
                            break;
                        case "CR02":
                            rights.editReleaseBatch = true;
                            break;
                        case "CSREJ01":
                            rights.claimSearchReverse = true;
                            break;
                        case "RV00":
                            rights.reportTab = true;
                            break;
                        case "AU00":
                            rights.authUploadTabd = true;
                            break;
                        case "AU01":
                            rights.authsubmisionTab = true;
                            break;
                        case "AU02":
                            rights.authLogTab = true;
                            break;
                        case "BS00":
                            rights.benSetupTab = true;
                            break;
                        case "AU03":
                            rights.singleAuth = true;
                            break;
                        case "ME00":
                            rights.membership = true;
                            break;
                        case "CS00":
                            rights.membClaims = true;
                            break;
                        case "CS01":
                            rights.provClaims = true;
                            break;
                        case "ADMIN00":
                            rights.accRegister = true;
                            break;
                        case "ADMIN01":
                            rights.Settings = true;
                            break;
                        case "ADMIN02":
                            rights.hpSetup = true;
                            break;
                        case "ADMIN03":
                            rights.RegUsers = true;
                            break;
                        case "ADMIN04":
                            rights.manuals = true;
                            break;
                        case "ADMIN05":
                            rights.claimsRestore = true;
                            break;
                        case "ADMIN06":
                            rights.userRights = true;
                            break;
                        case "VP00":
                            rights.voucherPartners = true;
                            break;
                        case "VP01":
                            rights.medivouch = true;
                            break;
                        case "BC00":
                            rights.chronicConditions = true;
                            break;
                        default:
                            break;
                    }
                }

                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;

            }

            return rights;
        }

        [HttpPost]
        [Route("GetPopMessage")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
       
        public List<PopUpModel> GetPopMessage([FromBody] LoginDetailsModel data)
        {
            List<PopUpModel> pmList = new List<PopUpModel>();
            List<int> acceptedList = new List<int>();
            List<PopUpModel> finalList = new List<PopUpModel>();
            try
            {
                SqlConnection cn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                DataTable dt = new DataTable();

                cn.ConnectionString = _portalConnectionString;

                if(cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@USERNAME",data.username));
                cmd.Parameters.Add(new SqlParameter("@USERTYPE", data.userType));

                cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}.dbo.POPUP_MESSAGES \r\n WHERE " +
                    $"GETDATE() BETWEEN EFFECTIVEDATE AND ISNULL(ENDDATE,DATEADD(YEAR,100,GETDATE())) AND \r\n" +
                    $" SHOW = 1 AND USERTYPE = @USERTYPE";

                dt.Load(cmd.ExecuteReader());

                if(dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        PopUpModel pm = new PopUpModel();
                        pm.msgId = Convert.ToInt32(row["ROWID"].ToString());
                        pm.body = row["BODY"].ToString();
                        pm.heading = row["HEADING"].ToString();
                        if (DBNull.Value.Equals(row["FILENAME"]))
                        {
                            pm.filename = "";
                        }
                        else
                        {
                            pm.filename = row["FILENAME"].ToString();
                            if (DBNull.Value.Equals(row["HEIGHT"]))
                            {
                                pm.height = 0;
                                pm.width = 0;
                            }
                            else
                            {
                                pm.height = Convert.ToInt32(row["HEIGHT"].ToString());
                                pm.width = Convert.ToInt32(row["WIDTH"].ToString()); ;
                            }
                        }
                        
                        pm.forceAccept = Convert.ToBoolean(row["FORCEACCEPT"].ToString());
                        pm.body = row["BODY"].ToString();
                        pm.filetype = Convert.ToInt32(row["FILETYPE"].ToString());
                        pm.username = data.username;
                        pmList.Add(pm);
                    }
                    
                }

                dt = new DataTable();
                cmd.CommandText = $"SELECT MSGID FROM {PamcPortalDatabase}.dbo.POPUP_MESSAGES_ACCEPT_LOG \r\n" +
                    $" WHERE USERNAME = @USERNAME AND ACCEPTED = 1";
                dt.Load(cmd.ExecuteReader());

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int id = Convert.ToInt32(row["MSGID"].ToString());
                        acceptedList.Add(id);
                    }

                    foreach (PopUpModel item in pmList)
                    {
                        if (acceptedList.Contains(item.msgId))
                        {
                            continue;
                        }
                        else
                        {
                            finalList.Add(item);
                        }
                    }
                }
                else
                {
                    finalList = pmList;
                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }

            return finalList;
        }

        [HttpPost]
        [Route("MarkPopUpAsAccepted")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel MarkPopUpAsAccepted([FromBody] PopUpModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                SqlConnection cn = new SqlConnection(_portalConnectionString);
                SqlCommand cmd = new SqlCommand();
                if(cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@MSGID", data.msgId));
                cmd.Parameters.Add(new SqlParameter("@USERNAME", data.username));
                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.POPUP_MESSAGES_ACCEPT_LOG \r\n" +
                    $"(MSGID,USERNAME,ACCEPTED,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES \r\n" +
                    $" (@MSGID,@USERNAME,'1',@USERNAME,GETDATE(),@USERNAME,GETDATE())" ;

                cmd.ExecuteNonQuery();
                vr.valid = true;
            }
            catch (Exception ex)
            {
                vr.valid = false;
                LogError(ex.Message, ex.StackTrace);
            }
            return vr;
        }

        /*Return password*/
        [HttpPost]
        [Route("getPassword")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel getPassword([FromBody]LoginDetailsModel model)
        {
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(_portalConnectionString))
                {
                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = '" + model.username + "'";
                        cmd.Connection = sqlCon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        if (o != null)
                        {
                            model.success = Convert.ToBoolean(o);
                        }
                        if (model.success == true)
                        {
                            sqlTextToExecute = "SELECT * from users WHERE (Users.Username = '" + model.username + "')";
                            cmd.Connection = sqlCon;
                            cmd.CommandText = sqlTextToExecute;

                            DataTable dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            DataRow dr = dt.Rows[0];

                            model.username = dr["Username"].ToString();
                            model.password = dr["Password"].ToString();
                            model.userType = Convert.ToInt32(dr["UserType"]);
                            model.createBy = dr["createBy"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CreateDate"]);
                            model.changeBy = dr["ChangeBy"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CreateDate"]);
                        }

                    }
                    sqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                model.errorMessage = ex.InnerException.Message;

            }



            return model;

        }

        /*Updates passwords*/
        [HttpPost]
        [Route("updatePassword")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel updatePassword([FromBody]LoginDetailsModel model)
        {
            string accountinBureauSuccessMessage = "accRec";
            SettingsModel returnedRow = Utillity.GetSpecificSetting(accountinBureauSuccessMessage, _portalConnectionString);

            model.changeDate = DateTime.Now;
            model.changeBy = "System Password Recovery";

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = '" + model.username + "'";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        using (SqlConnection SqlconDrc = new SqlConnection(_drcConnectionString))
                        {
                            if (SqlconDrc.State != ConnectionState.Open)
                            {
                                SqlconDrc.Open();
                            }
                            using (SqlCommand cmddrc = new SqlCommand())
                            {
                                string providerNumbersString = string.Format("'{0}'", string.Join("','", model.listOfproviders.Select(i => i.Replace("'", "''"))));

                                cmd.CommandType = CommandType.Text;
                                string sqlText = "select count(*) from [VEND_MASTERS]" + "where VENDORID in (" + providerNumbersString + ") and EMAIL = '" + model.username + "'\r\n";
                                cmd.Connection = SqlconDrc;
                                cmd.CommandText = sqlText;
                                Object x = cmd.ExecuteScalar();

                                Boolean provID = Convert.ToBoolean(x);
                                Boolean users = Convert.ToBoolean(o);

                                if ((provID == true) & (users == true))
                                {
                                    model.success = Convert.ToBoolean(o);

                                }
                                else
                                {
                                    model.errorMessage = "We were unable to verify your account with the entered practice numbers";
                                }

                            }
                            SqlconDrc.Close();
                        }
                        if (model.success == true)
                        {

                            sqlTextToExecute = "update Users set [password] = '" + model.newPassword + "', changeBy = '" + model.changeBy + "', changeDate = GETDATE() where username = '" + model.username + "'\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {

                                }
                            }
                            catch (Exception ex)
                            {
                                LogError(ex.Message, ex.StackTrace);
                                var msg = ex.Message;
                                model.success = false;
                            }

                            if (model.success == true)
                            {

                            }
                        }

                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                model.errorMessage = ex.InnerException.Message;

            }



            return model;

        }

        /*updated provider passwords*/
        [HttpPost]
        [Route("UpdateProvPassword")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProviderRecoveryModel updateProvpassword([FromBody]ProviderRecoveryModel model)
        {
            string accountinBureauSuccessMessage = "accRec";
            SettingsModel returnedRow = Utillity.GetSpecificSetting(accountinBureauSuccessMessage, _portalConnectionString);

            model.ChangeDate = DateTime.Now;
            model.ChangeBy = "System Password Recovery";

            string EmailBody = returnedRow.description.Replace("<email>", model.Email);

            string emailSubject = "Provider - Email Recovery";

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = @email";

                        cmd.Parameters.Add("@email", SqlDbType.VarChar, 20).Value = model.Email;
                        cmd.Parameters.Add("@provId", SqlDbType.VarChar, 20).Value = model.provPracNum;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.NewPassword;
                        cmd.Parameters.Add("@changeBy", SqlDbType.VarChar).Value = model.ChangeBy;
                        cmd.Parameters.Add("@changeDate", SqlDbType.DateTime).Value = model.ChangeDate;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        using (SqlConnection Sqlcondrc = new SqlConnection(_drcConnectionString))
                        {
                            if (Sqlcondrc.State != ConnectionState.Open)
                            {
                                Sqlcondrc.Open();
                            }
                            using (SqlCommand cmddrc = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                string sqlText = "select count(*) from [PROV_MASTERS]\r\n" + "where PROVID = @provId and Email = @email\r\n";
                                cmd.Connection = Sqlcondrc;
                                cmd.CommandText = sqlText;
                                Object x = cmd.ExecuteScalar();

                                Boolean provID = Convert.ToBoolean(x);
                                Boolean users = Convert.ToBoolean(o);

                                if ((provID == true) & (users == true))
                                {
                                    model.Success = Convert.ToBoolean(o);

                                }
                                else
                                {
                                    model.ErrorMessage = "We were unable to verify your account with the entered practice numbers";
                                }
                            }
                            Sqlcondrc.Close();
                        }
                        if (model.Success == true)
                        {
                            sqlTextToExecute = "update Users set [password] = @password, changeBy = @changeBy, changeDate = @changeDate where username = @email\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {

                                }
                            }
                            catch (Exception ex)
                            {
                                var msg = ex.Message;
                                model.Success = false;
                            }

                            if (model.Success == true)
                            {
                                //Utillity.Sendnotification(emailSubject, EmailBody, "montie2206@gmail.com"); // Jaco comment out 2023-09-14
                                Utillity.Sendnotification(emailSubject, EmailBody, "jaco@pamc.co.za");
                            }

                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);

            }

            return model;
        }

        /*update client passwords*/
        [HttpPost]
        [Route("UpdateClientPassword")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ClientRecoveryModel updateClientPassword([FromBody]ClientRecoveryModel model)
        {
            string accountinBureauSuccessMessage = "accRec";
            SettingsModel returnedRow = Utillity.GetSpecificSetting(accountinBureauSuccessMessage, _portalConnectionString);

            model.ChangeDate = DateTime.Now;
            model.ChangeBy = "System Password Recovery";

            string EmailBody = returnedRow.description.Replace("<email>", model.Email);

            string emailSubject = "Client - Email Recovery";

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = @email";

                        cmd.Parameters.Add("@email", SqlDbType.VarChar, 20).Value = model.Email;
                        cmd.Parameters.Add("@changeBy", SqlDbType.VarChar).Value = model.ChangeBy;
                        cmd.Parameters.Add("@changeDate", SqlDbType.DateTime).Value = model.ChangeDate;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.password;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        model.Success = Convert.ToBoolean(o);

                        if (model.Success == true)
                        {
                            sqlTextToExecute = "update Users set [password] = @password, changeBy = @changeBy, changeDate = @changeDate where username = @email\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {

                                }
                            }
                            catch (Exception ex)
                            {
                                var msg = ex.Message;
                                model.Success = false;
                            }
                            if (model.Success == true)
                            {
                                //Utillity.Sendnotification(emailSubject, EmailBody, "montie2206@gmail.com"); // Jaco comment out 2023-09-14
                                Utillity.Sendnotification(emailSubject, EmailBody, "jaco@pamc.co.za");
                            }
                        }
                        else
                        {
                            model.ErrorMessage = "Failed to update password.";
                        }

                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);

            }

            return model;
        }

        /*Testing to see if valid SQL connection has been made*/
        [HttpGet]
        [Route("validConnection")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel validConnection()
        {
            //Below variables will be used to check the connection of both databases in use
            ValidationResultsModel vr = new ValidationResultsModel();
            Boolean pamcortal;
            Boolean drcConnection;

            using (SqlConnection connection = new SqlConnection(_drcConnectionString))
            {
                try
                {
                    connection.Open();
                    drcConnection = true;
                    vr.valid = true;
                    connection.Close();
                }
                catch (SqlException ex)
                {
                    var msg = ex.Message;
                    drcConnection = false;
                    vr.valid = false;
                }

            }
            using (SqlConnection connection = new SqlConnection(_portalConnectionString))
            {
                try
                {
                    connection.Open();
                    pamcortal = true;
                    vr.valid = true;
                    connection.Close();
                }
                catch (SqlException)
                {
                    pamcortal = false;
                    vr.valid = false;
                }
            }
            if (drcConnection == true && pamcortal == true)
            {
                SqlConnectionStringBuilder drcIp = new SqlConnectionStringBuilder(_drcConnectionString);
                string[] drc = new string[drcIp.DataSource.Split(".").Length];
                drc = drcIp.DataSource.Split(".");
                SqlConnectionStringBuilder pamcIp = new SqlConnectionStringBuilder(_portalConnectionString);
                string[] pamc = new string[pamcIp.DataSource.Split(".").Length];
                pamc = pamcIp.DataSource.Split(".");

                vr.message = drc[drc.Length - 1] + pamc[pamc.Length - 1];
                vr.message = vr.message + " " + _releaseDate + " " + drcIp.InitialCatalog + " " + pamcIp.InitialCatalog;
                return vr;
            }
            else
            {
                vr.message = "";
                return vr;
            }
        }


        /*Updating the users password when the user clicked change password*/
        [HttpPost]
        [Route("ChangePassword")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public string ChangePassword([FromBody]ChangePasswordModel model)
        {
            SettingsModel returnedRow = Utillity.GetSpecificSetting("accRec", _portalConnectionString);

            model.ChangeBy = "Update Password";
            model.ChangeDate = DateTime.Now;
            string Confirmation = "";

            OperationLog log = new OperationLog();
            log.passwordChange("Password Changed By User", DateTime.Now, model.Email, _portalConnectionString);

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string sqlTextToExecute = "update Users set [password] = '" + model.newPassword + "', changeBy = '" + model.ChangeBy + "', changeDate = GETDATE() where username = '" + model.Email + "'\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            Confirmation = "Failed To Update Password, Please Retry";
                        }
                        Confirmation = "Your password has been successfully updated";
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return Confirmation;
        }

        /*Will be called when the user clicks on the forget password link*/
        [HttpPost]
        [Route("RecoverPassword")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel RecoverPassword([FromBody]LoginDetailsModel model)
        {
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = @username";
                        cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = model.username;
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        if (o != null)
                        {
                            model.success = Convert.ToBoolean(o);
                        }
                        if (model.success == true)
                        {
                            sqlTextToExecute = "SELECT * from users WHERE (Users.Username = @username)";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            DataTable dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            DataRow dr = dt.Rows[0];

                            model.username = dr["Username"].ToString();
                            model.password = dr["Password"].ToString();
                            model.userType = Convert.ToInt32(dr["UserType"]);
                            model.createBy = dr["createBy"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CreateDate"]);
                            model.changeBy = dr["ChangeBy"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CreateDate"]);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;

            }
            return model;
        }

        /*Will be called when the use clicks on the "Forget Password Link on the login page is called"*/
        [HttpPost]
        [Route("NewPassword")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel NewPassword([FromBody]LoginDetailsModel model)
        {
 
            SettingsModel returnedRow = Utillity.GetSpecificSetting("accRec", _portalConnectionString);

            model.changeDate = DateTime.Now;
            model.changeBy = "System Password Recovery";

            OperationLog log = new OperationLog();

            log.forgetPassword("Password Changed By Admin", DateTime.Now, model.username, _portalConnectionString);


            //Builds the email that will get sent upon successful update 

            string EmailBody = returnedRow.description.Replace("<email>", model.username);

            string EmailSubject = "Accounting Bureau - Email Recovery";
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select count(*) from users where Username = @username";

                        cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = model.username;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.newPassword;
                        cmd.Parameters.Add("@changeBy", SqlDbType.VarChar).Value = model.changeBy;
                        cmd.Parameters.Add("@changeDate", SqlDbType.DateTime).Value = model.changeDate;


                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();


                        //if (FoundAccountingBuro != null)
                        //{
                        //    logindetails.Success = Convert.ToBoolean(o);

                        //}

                        using (SqlConnection Sqlcondrc = new SqlConnection(_drcConnectionString))
                        {
                            if (Sqlcondrc.State != ConnectionState.Open)
                            {
                                Sqlcondrc.Open();
                            }
                            using (SqlCommand cmddrc = new SqlCommand())
                            {
                                string providernumbersstring = string.Format("'{0}'", string.Join("','", model.listOfproviders.Select(i => i.Replace("'", "''"))));

                                cmd.CommandType = CommandType.Text;
                                string sqlText = "select count(*) from [VEND_MASTERS]" + "where VENDORID in (" + providernumbersstring + ") and EMAIL = @username\r\n";
                                cmd.Parameters.AddWithValue("@vendId", providernumbersstring);
                                cmd.Connection = Sqlcondrc;
                                cmd.CommandText = sqlText;
                                Object x = cmd.ExecuteScalar();

                                Boolean provID = Convert.ToBoolean(x);
                                Boolean users = Convert.ToBoolean(o);

                                //if (x != null)
                                //{
                                //    logindetails.Success = Convert.ToBoolean(o);

                                //}
                                if ((provID == true) & (users == true))
                                {
                                    model.success = Convert.ToBoolean(o);

                                }
                                else
                                {
                                    model.errorMessage = "We were unable to verify your account with the entered practice numbers";
                                }

                            }
                            Sqlcon.Close();

                        }
                        if (model.success == true)
                        {

                            sqlTextToExecute = "update Users set [password] = @password, changeBy = @changeBy, changeDate = GETDATE() where username = @username\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            try
                            {
                                int rowsAffected = cmd.ExecuteNonQuery();
                                if (rowsAffected != 1)
                                {

                                }
                            }
                            catch (Exception ex)
                            {
                                LogError(ex.Message, ex.StackTrace);
                                var msg = ex.Message;
                                model.success = false;
                            }

                            if (model.success == true)
                            {
                                //Utillity.Sendnotification(EmailSubject, EmailBody, "montie2206@gmail.com"); // Jaco comment out 2023-09-14
                                Utillity.Sendnotification(EmailSubject, EmailBody, "jaco@pamc.co.za");
                            }
                        }

                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }



            return model;

        }

        /*Method used to log all errors that take place in this controller*/
        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "LOGIN";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\LOGIN\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception )
            {


            }


        }

        /*Gets details of provider*/
        private string ProviderDetails(LoginDetailsModel model)
        {
            string sqlTextToExecute;
            DataTable dt = new DataTable();
            ProviderDetailModel providerdetail = Utillity.GetProviderNameLogIn(model.provID, _drcConnectionString);



            model.Firstname = providerdetail.FirstName;
            model.Lastname = providerdetail.LastName;
            model.contact = providerdetail.Contact;
            model.contract = providerdetail.Contract;
            model.email = providerdetail.Email;

            using (SqlConnection drccon = new SqlConnection(_drcConnectionString))
            {
                if (drccon.State != ConnectionState.Open)
                {
                    drccon.Open();
                }
                using (SqlCommand drccmd = new SqlCommand())
                {
                    sqlTextToExecute = "SELECT PM.[PROVID],PS.SPECCODE,PC.DESCR,PM.CLASS FROM [dbo].[PROV_SPECINFO] PS " +
                        " INNER JOIN PROV_SPECCODES PC ON PC.CODE = PS.SPECCODE " +
                        " INNER JOIN PROV_MASTERS PM ON PM.PROVID = PS.PROVID " +
                        " WHERE PS.TYPE = 'PRIMARY' and PM.PROVID = '" + providerdetail.ProvID + "'";
                    drccmd.Connection = drccon;
                    drccmd.CommandText = sqlTextToExecute;
                    dt.Load(drccmd.ExecuteReader());
                    if (dt.Rows.Count == 0)
                    {
                        throw new Exception();
                    }

                    DataRow dr = dt.Rows[0];
                    model.provID = dr["PROVID"].ToString();
                    model.SpecCode = dr["SPECCODE"].ToString();
                    model.description = dr["DESCR"].ToString();
                    model.provClass = dr["CLASS"].ToString();
                }
                drccon.Close();
            }

            return sqlTextToExecute;
        }
    }
}
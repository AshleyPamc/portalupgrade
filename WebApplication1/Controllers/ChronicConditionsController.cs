﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Text;
using System.ServiceModel;
using Microsoft.AspNetCore.Authorization;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChronicConditionsController : Base.PAMCController
    {
        public ChronicConditionsController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("GetChronicConditions")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ChronicConditionCodesModel> GetChronicConditions([FromBody] ChronicConditionCodesModel userName)
        {
            List<ChronicConditionCodesModel> list = new List<ChronicConditionCodesModel>();

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText =
                    $" SELECT CONDCODE, DESCR, SELECTABLE  " +
                    $" FROM {DRCDatabase}.dbo.MEMB_COND_CODES ";

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    ChronicConditionCodesModel r = new ChronicConditionCodesModel();

                    r.code = row["CONDCODE"].ToString();
                    r.description = row["DESCR"].ToString();
                    r.selectable = row["SELECTABLE"].ToString();
                    r.success = true;

                    list.Add(r);
                }

                // Conditions DIAB1 and DIAB2 in backend is only one condition in frontend
                if (list.Where(s => s != null && (s.code == "DIAB1" || s.code == "DIAB2")).Count() > 0)
                {
                    // Remove DIAB1 and DIAB2
                    list.RemoveAll(s => s.code == "DIAB1" || s.code == "DIAB2");

                    // Add DIAB - DIABETES MELLITUS
                    ChronicConditionCodesModel cond = new ChronicConditionCodesModel();
                    cond.code = "DIAB";
                    cond.description = "DIABETES MELLITUS";
                    cond.selectable = "1";
                    cond.success = true;
                    list.Add(cond);
                }

                // Sort List
                List<ChronicConditionCodesModel> sortedList = list.OrderBy(o => o.description).ToList();
                list = sortedList;
            }
            catch (Exception e)
            {
                list.Clear();
                ChronicConditionCodesModel r = new ChronicConditionCodesModel();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetConditionDiags")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ChronicConditionDiagsModel> GetConditionDiags([FromBody] ChronicConditionDiagsModel userName)
        {
            List<ChronicConditionDiagsModel> list = new List<ChronicConditionDiagsModel>();

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText =
                    $" SELECT CD.CONDCODE, CD.DIAGCODE, DC.DIAGDESC  " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_DIAGS CD \n" +
                    $" LEFT JOIN {DRCDatabase}.dbo.DIAG_CODES DC on (CD.DIAGCODE = DC.DIAGCODE) ";

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    ChronicConditionDiagsModel r = new ChronicConditionDiagsModel();

                    r.conditionCode = row["CONDCODE"].ToString();
                    r.diagCode = row["DIAGCODE"].ToString();
                    r.diagDesc = row["DIAGDESC"].ToString();

                    r.success = true;

                    list.Add(r);
                }
            }
            catch (Exception e)
            {
                list.Clear();
                ChronicConditionDiagsModel r = new ChronicConditionDiagsModel();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetMedicationDiags")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ChronicConditionDiagsModel> GetMedicationDiags([FromBody] ChronicConditionDiagsModel userName)
        {
            List<ChronicConditionDiagsModel> list = new List<ChronicConditionDiagsModel>();

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText =
                    $" SELECT CD.CONDCODE, CD.DIAGCODE, DC.DIAGDESC  " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_MEDICATION_DIAGS CD \n" +
                    $" LEFT JOIN {DRCDatabase}.dbo.DIAG_CODES DC on (CD.DIAGCODE = DC.DIAGCODE) ";

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    ChronicConditionDiagsModel r = new ChronicConditionDiagsModel();

                    r.conditionCode = row["CONDCODE"].ToString();
                    r.diagCode = row["DIAGCODE"].ToString();
                    r.diagDesc = row["DIAGDESC"].ToString();

                    r.success = true;

                    list.Add(r);
                }
            }
            catch (Exception e)
            {
                list.Clear();
                ChronicConditionDiagsModel r = new ChronicConditionDiagsModel();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetReferralSpecCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<SpecCodes> GetReferralSpecCodes([FromBody] SpecCodes userName)
        {
            List<SpecCodes> list = new List<SpecCodes>();

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText =
                     " SELECT CODE, DESCR \n" +
                    $" FROM {DRCDatabase}.dbo.PROV_SPECCODES  \n" +
                     // sindi list " WHERE CODE in ('4','8','9','10','11','12','13','14','26','27','28','30','32','33','34','36','38','39','40','42','44','46','50','54','64','66','67','68','72','75','83','84','86','87','88','89','90','92','93','94','95','96','97') ";
                     " ORDER BY convert(int, CODE)";
                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    SpecCodes r = new SpecCodes();

                    r.code = row["CODE"].ToString();
                    r.desc = row["DESCR"].ToString();

                    r.success = true;

                    list.Add(r);
                }
            }
            catch (Exception e)
            {
                list.Clear();
                SpecCodes r = new SpecCodes();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetMedicineCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<MedicineCodesModel> GetMedicineCodes([FromBody] MedicineCodesModel userName)
        {
            List<MedicineCodesModel> list = new List<MedicineCodesModel>();

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandText =
                     " SELECT Top 10 SVCCODE, PHCODE, SVCDESC  \n" +
                    $" FROM {DRCDatabase}.dbo.MEDICINE_CODES  \n" +
                     " WHERE TODATE IS NULL AND CURRHIST = 'C' ";

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    MedicineCodesModel r = new MedicineCodesModel();

                    r.svcCode = row["SVCCODE"].ToString();
                    r.phCode = row["PHCODE"].ToString();
                    r.svcDesc = row["SVCDESC"].ToString();

                    r.success = true;

                    list.Add(r);
                }
            }
            catch (Exception e)
            {
                list.Clear();
                MedicineCodesModel r = new MedicineCodesModel();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetSubmissionHistory")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ChronicConditionApplicationModel> GetSubmissionHistory([FromBody] ChronicConditionApplicationModel submissionHistory)
        {
            List<ChronicConditionApplicationModel> list = new List<ChronicConditionApplicationModel>();

            try
            {
                // Check if search must be done for a single family, and build string for SQL (otherwise serch for all applications for this provider)
                string familyMembers = "";
                if (submissionHistory.familyMembers != null && submissionHistory.familyMembers.Count > 0)
                {
                    foreach (MemberEligibilityModel memb in submissionHistory.familyMembers)
                    {
                        familyMembers += $"'{memb.MEMBID}',";
                    }

                    // remove last comma
                    familyMembers = familyMembers.Substring(0, familyMembers.Length - 1);
                }

                // Build SQL query
                string sqlQuery =
                    $" SELECT CA.CONDAPPNO, MC.DESCR as [CONDITION], CA.MEMBID, MM.FIRSTNM, MM.LASTNM, CS.DESCR as [STATUS]  " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_APPLICATION CA \n" +
                    $" LEFT JOIN {DRCDatabase}.dbo.MEMB_MASTERS MM on CA.MEMBID = MM.MEMBID " +
                    $" LEFT JOIN {DRCDatabase}.dbo.CONDITION_APPSTATUS CS on CA.[STATUS] = CS.[STATUS] " +
                    $" LEFT JOIN {DRCDatabase}.dbo.MEMB_COND_CODES MC on CA.CONDCODE = MC.CONDCODE " +
                    $" WHERE PROVID = @provId ";

                // Add param if only applicatins for a certain family
                if (familyMembers != "")
                {
                    sqlQuery += $" AND CA.MEMBID in ({familyMembers}) ";
                }

                // Always order by 
                sqlQuery += $" ORDER BY CA.CONDAPPNO DESC ";

                // Do SqlCommand
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@provId", submissionHistory.provId));
                cmd.CommandText = sqlQuery;

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    ChronicConditionApplicationModel r = new ChronicConditionApplicationModel();

                    r.conditionApplicationNumber = row["CONDAPPNO"].ToString();
                    r.conditionDescription = row["CONDITION"].ToString();
                    r.membId = row["MEMBID"].ToString();
                    r.membFirstName = row["FIRSTNM"].ToString();
                    r.membLastName = row["LASTNM"].ToString();
                    r.status = row["STATUS"].ToString();

                    r.success = true;

                    list.Add(r);
                }
            }
            catch (Exception e)
            {
                list.Clear();
                ChronicConditionApplicationModel r = new ChronicConditionApplicationModel();
                r.success = false;
                r.message = e.Message;
                list.Add(r);

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("CheckExistingApplications")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ChronicConditionApplicationModel CheckExistingApplications ([FromBody] ChronicConditionApplicationModel condUpdateInfo)
        {           
            ChronicConditionApplicationModel approvedOrPendingApp = new ChronicConditionApplicationModel();
            approvedOrPendingApp.success = false;
            approvedOrPendingApp.message = "";

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                // Get data from CONDITION_APPLICATION
                cmd.Parameters.Add(new SqlParameter("@membId", condUpdateInfo.membId));


                if (condUpdateInfo.conditionCode == "DIAB")
                {
                    cmd.Parameters.Add(new SqlParameter("@condCode", "DIAB1"));
                    cmd.Parameters.Add(new SqlParameter("@condCode2", "DIAB2"));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@condCode", condUpdateInfo.conditionCode));
                    cmd.Parameters.Add(new SqlParameter("@condCode2", "no 2nd cond code"));
                }

                cmd.CommandText =
                    $" SELECT TOP 1 CONDAPPNO " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_APPLICATION  \n" +
                    $" WHERE MEMBID = @membId AND CONDCODE in (@condCode, @condCode2) and [STATUS] in ('A', 'P') order by CONDAPPNO DESC";

                DataTable dtCondMaster = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dtCondMaster.Load(cmd.ExecuteReader());
                cn.Close();

                if (dtCondMaster.Rows.Count < 1)
                {
                    approvedOrPendingApp.success = true;
                    approvedOrPendingApp.message = "";
                }
                else
                {
                    // Get Approved or Pending application
                    approvedOrPendingApp = GetApplicationDetail(dtCondMaster.Rows[0]["CONDAPPNO"].ToString());

                    approvedOrPendingApp.success = false;
                    approvedOrPendingApp.message = "Prior existing application";    
                }
            }
            catch (Exception e)
            {
                approvedOrPendingApp = new ChronicConditionApplicationModel(); // New model for error message 
                approvedOrPendingApp.success = false;
                approvedOrPendingApp.message = e.Message;
                    
                LogError(e.Message, e.StackTrace);
            }

            return approvedOrPendingApp;
        }

        [HttpPost]
        [Route("GetApprovedApplicationAndLastUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ChronicConditionApplicationModel> GetApprovedApplicationAndLastUpdate([FromBody] ChronicConditionApplicationModel condUpdateInfo)
        {
            List<ChronicConditionApplicationModel> lstAppAndUpdate = new List<ChronicConditionApplicationModel>();

            ChronicConditionApplicationModel approvedApplication = new ChronicConditionApplicationModel();
            approvedApplication.success = false;
            approvedApplication.message = "";

            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                // Get data from CONDITION_APPLICATION
                cmd.Parameters.Add(new SqlParameter("@membId", condUpdateInfo.membId));


                if (condUpdateInfo.conditionCode == "DIAB")
                {
                    cmd.Parameters.Add(new SqlParameter("@condCode", "DIAB1"));
                    cmd.Parameters.Add(new SqlParameter("@condCode2", "DIAB2"));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@condCode", condUpdateInfo.conditionCode));
                    cmd.Parameters.Add(new SqlParameter("@condCode2", "no 2nd cond code"));
                }

                cmd.CommandText =
                    $" SELECT TOP 1 CONDAPPNO " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_APPLICATION  \n" +                    
                    $" WHERE MEMBID = @membId AND CONDCODE in (@condCode, @condCode2) and [STATUS] in ('A') order by CONDAPPNO DESC";

                DataTable dtCondMaster = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dtCondMaster.Load(cmd.ExecuteReader());
                cn.Close();

                if (dtCondMaster.Rows.Count < 1)
                {
                    approvedApplication.success = false;
                    approvedApplication.message = "Updates can only be done on approved applications. There is no approved application for the selected member and condition.";
                    lstAppAndUpdate.Add(approvedApplication);
                }
                else
                {
                    // Get Approved application
                    approvedApplication = GetApplicationDetail(dtCondMaster.Rows[0]["CONDAPPNO"].ToString());

                    if (approvedApplication.conditionApplicationNumber == null)
                    {
                        approvedApplication.success = false;
                        approvedApplication.message = "Could not retrieve condition application information for this member and condition.";
                    }
                    else
                    {                       
                        approvedApplication.success = true;
                        lstAppAndUpdate.Add(approvedApplication);

                        // Get last update if there are any
                        cmd.CommandText =
                                $" SELECT TOP 1 CONDAPPNO " +
                                $" FROM {DRCDatabase}.dbo.CONDITION_APPLICATION \n" +
                                $" WHERE MEMBID = @membId AND CONDCODE in (@condCode, @condCode2) and [STATUS] in ('U') order by CONDAPPNO DESC ";
                        dtCondMaster = new DataTable();
                        if (cn.State != ConnectionState.Open) cn.Open();
                        dtCondMaster.Load(cmd.ExecuteReader());
                        cn.Close();

                        if (dtCondMaster.Rows.Count > 0)
                        {
                            ChronicConditionApplicationModel lastUpdate = new ChronicConditionApplicationModel();
                            lastUpdate.success = false;
                            lastUpdate.message = "";
                            lastUpdate = GetApplicationDetail(dtCondMaster.Rows[0]["CONDAPPNO"].ToString());

                            if (lastUpdate.conditionApplicationNumber == null)
                            {                               
                                lastUpdate.message = "Could not retrieve latest condition update information for this member and condition.";
                            }
                            else
                            {
                                lastUpdate.success = true;
                                lstAppAndUpdate.Add(lastUpdate);
                            }
                        }                        
                    }                        
                }
            }
            catch (Exception e)
            {
                lstAppAndUpdate = new List<ChronicConditionApplicationModel>(); // Clear list
                ChronicConditionApplicationModel err = new ChronicConditionApplicationModel(); // New model for error message 
                err.success = false;
                err.message = e.Message;

                lstAppAndUpdate.Add(err);

                LogError(e.Message, e.StackTrace);
            }

            return lstAppAndUpdate;
        }

        [HttpPost]
        [Route("ViewSubmittedApplication")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ChronicConditionApplicationModel ViewSubmittedApplication([FromBody] ChronicConditionApplicationModel applicationNumber)
        {
            ChronicConditionApplicationModel submittedApplication = new ChronicConditionApplicationModel();
            submittedApplication.success = false;
            submittedApplication.message = "";

            try
            {
                submittedApplication = GetApplicationDetail(applicationNumber.conditionApplicationNumber);

                if (submittedApplication.conditionApplicationNumber == null)
                {
                    submittedApplication.message = "No application found";
                }
                else
                {
                    submittedApplication.success = true;
                }
            }
            catch (Exception e)
            {                
                submittedApplication.success = false;
                submittedApplication.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return submittedApplication;
        }

        private ChronicConditionApplicationModel GetApplicationDetail(string condAppNo)
        {
            ChronicConditionApplicationModel condModel = new ChronicConditionApplicationModel();

            SqlConnection cn = new SqlConnection(_drcConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;

            // Get data from CONDITION_APPLICATION
            cmd.Parameters.Add(new SqlParameter("@condAppNo", condAppNo));
            cmd.CommandText =
                $" SELECT TOP 1 CA.*, MC.DESCR as [COND_DESCR], CS.DESCR as [STATUS_DESCR], MM.FIRSTNM, MM.LASTNM, DC.DIAGDESC, PS.DESCR as SPEC_DESCR  " +
                $" FROM {DRCDatabase}.dbo.CONDITION_APPLICATION CA \n" +
                $" LEFT JOIN {DRCDatabase}.dbo.MEMB_MASTERS MM on CA.MEMBID = MM.MEMBID \n" +
                $" LEFT JOIN {DRCDatabase}.dbo.CONDITION_APPSTATUS CS on CA.[STATUS] = CS.[STATUS] \n" +
                $" LEFT JOIN {DRCDatabase}.dbo.MEMB_COND_CODES MC on CA.CONDCODE = MC.CONDCODE \n" +
                $" LEFT JOIN {DRCDatabase}.dbo.PROV_SPECCODES PS on CA.REFPROVSPEC = PS.CODE \n" +
                $" LEFT JOIN {DRCDatabase}.dbo.DIAG_CODES DC ON DC.DIAGCODE = \n" +
                $"                                                              CASE  \n" +
                $"                                                                  WHEN CA.CONDCODE = 'DIAB1' THEN CA.DIABDIAGCODE \n" +
                $"                                                                  WHEN CA.CONDCODE = 'DIAB2' THEN CA.DIABDIAGCODE \n" +
                $"                                                                  WHEN CA.CONDCODE = 'CHOL' THEN CA.HYPLDIAGCODE \n" +
                $"                                                                  WHEN CA.CONDCODE = 'HYPER' THEN CA.HYPTDIAGCODE \n" +
                $"                                                                  WHEN CA.CONDCODE = 'ASTHMA' THEN CA.ASTHMDIAGCODE \n" +
                $"                                                              END \n" +
                $" WHERE CA.CONDAPPNO = @condAppNo ";

            DataTable dtCondMaster = new DataTable();
            if (cn.State != ConnectionState.Open) cn.Open();
            dtCondMaster.Load(cmd.ExecuteReader());
            cn.Close();

            if (dtCondMaster.Rows.Count > 0)
            {
                DataRow condMasterRow = dtCondMaster.Rows[0];

                condModel.conditionApplicationNumber = condMasterRow["CONDAPPNO"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["CONDCODE"]))
                    condModel.conditionCode = condMasterRow["CONDCODE"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["COND_DESCR"]))
                    condModel.conditionDescription = condMasterRow["COND_DESCR"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["MEMBID"]))
                    condModel.membId = condMasterRow["MEMBID"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["FIRSTNM"]))
                    condModel.membFirstName = condMasterRow["FIRSTNM"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["LASTNM"]))
                    condModel.membLastName = condMasterRow["LASTNM"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["MEMBEMAIL"]))
                    condModel.membEmail = condMasterRow["MEMBEMAIL"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["REFERRED"]))
                {
                    if (condMasterRow["REFERRED"].ToString() == "Y") condModel.referredYN = "Yes";
                    else condModel.referredYN = "No";
                }
                else
                {
                    condModel.referredYN = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["REFPROVID"]))
                    condModel.refProvid = condMasterRow["REFPROVID"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["REFPROVLASTNM"]))
                    condModel.refProvSurname = condMasterRow["REFPROVLASTNM"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["REFPROVSPEC"])) condModel.refProvSpec = condMasterRow["REFPROVSPEC"].ToString();
                else condModel.refProvSpec = "";

                if (!DBNull.Value.Equals(condMasterRow["SPEC_DESCR"]) && condMasterRow["SPEC_DESCR"].ToString().Trim() != "")
                    condModel.refProvSpec += " - " + condMasterRow["SPEC_DESCR"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["REFNOTES"]))
                    condModel.refProvNotes = condMasterRow["REFNOTES"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["STATUS_DESCR"]))
                    condModel.status = condMasterRow["STATUS_DESCR"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["CANCELLATIONNOTE"]))
                    condModel.cancellationNote = condMasterRow["CANCELLATIONNOTE"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["PROVID"]))
                    condModel.provId = condMasterRow["PROVID"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["PROVEMAIL"]))
                    condModel.provEmail = condMasterRow["PROVEMAIL"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["APPDATE"]))
                    condModel.applicationDate = condMasterRow["APPDATE"].ToString().Substring(0,10);

                if (!DBNull.Value.Equals(condMasterRow["WGHT"]))
                    condModel.weight = condMasterRow["WGHT"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["HEIGHT"]))
                    condModel.height = condMasterRow["HEIGHT"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["BMI"]))
                    condModel.bmi = condMasterRow["BMI"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["BPSYSTOLIC"]))
                    condModel.bloodPresSystolic = condMasterRow["BPSYSTOLIC"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["BPDIASTOLIC"]))
                    condModel.bloodPresDiastolic = condMasterRow["BPDIASTOLIC"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["HDL"]))
                    condModel.cholesterolHDL = condMasterRow["HDL"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["LDL"]))
                    condModel.cholesterolLDL = condMasterRow["LDL"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["TRIGLYCERIDES"]))
                    condModel.cholesterolTRI = condMasterRow["TRIGLYCERIDES"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["CHOLESTEROL"]))
                    condModel.cholesterolTotal = condMasterRow["CHOLESTEROL"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["HDLLDLRATIO"]))
                    condModel.cholesterolLdlHdlRatio = condMasterRow["HDLLDLRATIO"].ToString();

                string diabetesType1 = "";
                string diabetesType2 = "";
                if (!DBNull.Value.Equals(condMasterRow["DIABTYPE1"]))
                    diabetesType1 = condMasterRow["DIABTYPE1"].ToString();

                if (!DBNull.Value.Equals(condMasterRow["DIABTYPE2"]))
                    diabetesType2 = condMasterRow["DIABTYPE2"].ToString();


                if (diabetesType1 == "Y" || diabetesType2 == "Y")
                    condModel.diabetesYN = "Yes";
                else
                    condModel.diabetesYN = "No";

                if (diabetesType1 == "Y")
                    condModel.diabetesType = "Type1";
                else if (diabetesType2 == "Y")
                    condModel.diabetesType = "Type2";
                else
                    condModel.diabetesType = "";

                if (!DBNull.Value.Equals(condMasterRow["DIABOGTT"]))
                    condModel.ogttResult = condMasterRow["DIABOGTT"].ToString();
                else
                    condModel.ogttResult = "";

                if (!DBNull.Value.Equals(condMasterRow["DIABHBA1C"]))
                    condModel.hba1c = condMasterRow["HBA1CHBA1C"].ToString();
                else
                    condModel.hba1c = "";

                //string diabTestFast = "";
                //string diabTestRandom = "";
                //if (!DBNull.Value.Equals(condMasterRow["DIABTYPE2FAST"]))
                //    diabTestFast = condMasterRow["DIABTYPE2FAST"].ToString();

                //if (!DBNull.Value.Equals(condMasterRow["DIABTYPE2RANDOM"]))
                //    diabTestRandom = condMasterRow["DIABTYPE2RANDOM"].ToString();

                //if (diabTestFast == "Y")
                //    condModel.fastingOrRandom = "Fasting";
                //else if (diabTestRandom == "Y")
                //    condModel.fastingOrRandom = "Random glucose";
                //else
                //    condModel.fastingOrRandom = "";

                if (!DBNull.Value.Equals(condMasterRow["DIABFASTRESULT"]))
                    condModel.fastingGlucoseResult = condMasterRow["DIABFASTRESULT"].ToString();
                else
                    condModel.fastingGlucoseResult = "";

                if (!DBNull.Value.Equals(condMasterRow["DIABRANDOMRESULT"]))
                    condModel.randomGlucoseResult = condMasterRow["DIABRANDOMRESULT"].ToString();
                else
                    condModel.randomGlucoseResult = "";

                if (!DBNull.Value.Equals(condMasterRow["DIABTESTDATE"]))
                    condModel.diabTestDate = condMasterRow["DIABTESTDATE"].ToString().Substring(0, Math.Min(10, condMasterRow["DIABTESTDATE"].ToString().Length));
                else
                    condModel.diabTestDate = "";

                if (!DBNull.Value.Equals(condMasterRow["ALCOHOL"]))
                {
                    if (condMasterRow["ALCOHOL"].ToString() == "Y") condModel.alcohol = "Yes";
                    else condModel.alcohol = "No";
                }
                else
                {
                    condModel.alcohol = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["OBESITY"]))
                {
                    if (condMasterRow["OBESITY"].ToString() == "Y") condModel.obesity = "Yes";
                    else condModel.obesity = "No";
                }
                else
                {
                    condModel.obesity = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["SMOKER"]))
                {
                    if (condMasterRow["SMOKER"].ToString() == "Y") condModel.smoker = "Yes";
                    else condModel.smoker = "No";
                }
                else
                {
                    condModel.smoker = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["PASTSMOKE"]))
                {
                    if (condMasterRow["PASTSMOKE"].ToString() == "Y") condModel.smokeInPast = "Yes";
                    else condModel.smokeInPast = "No";
                }
                else
                {
                    condModel.smokeInPast = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["SMOKEENDDATE"]))
                    condModel.cessationDate = condMasterRow["SMOKEENDDATE"].ToString().Substring(0, Math.Min(10, condMasterRow["SMOKEENDDATE"].ToString().Length));
                else
                    condModel.cessationDate = "";

                if (!DBNull.Value.Equals(condMasterRow["HEARTDISEASE"]))
                {
                    if (condMasterRow["HEARTDISEASE"].ToString() == "Y") condModel.ischaemicHeartDisease = "Yes";
                    else condModel.ischaemicHeartDisease = "No";
                }
                else
                {
                    condModel.ischaemicHeartDisease = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["VASCDISEASE"]))
                {
                    if (condMasterRow["VASCDISEASE"].ToString() == "Y") condModel.peripheralVascularDisease = "Yes";
                    else condModel.peripheralVascularDisease = "No";
                }
                else
                {
                    condModel.peripheralVascularDisease = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["TIA"]))
                {
                    if (condMasterRow["TIA"].ToString() == "Y") condModel.strokeAttacks = "Yes";
                    else condModel.strokeAttacks = "No";
                }
                else
                {
                    condModel.strokeAttacks = "";
                }

                if (!DBNull.Value.Equals(condMasterRow["CONDCODE"]))
                {
                    if (condMasterRow["CONDCODE"].ToString() == "DIAB1" || condMasterRow["CONDCODE"].ToString() == "DIAB2")
                    {
                        if (!DBNull.Value.Equals(condMasterRow["DIABDIAGCODE"])) condModel.diabetesIcd10 = condMasterRow["DIABDIAGCODE"].ToString();
                        else condModel.diabetesIcd10 = "";

                        if (!DBNull.Value.Equals(condMasterRow["DIAGDESC"]) && condMasterRow["DIAGDESC"].ToString().Trim() != "")
                        {
                            condModel.diabetesIcd10 += " - " + condMasterRow["DIAGDESC"].ToString();
                        }
                    }
                    else if (condMasterRow["CONDCODE"].ToString() == "CHOL")
                    {
                        if (!DBNull.Value.Equals(condMasterRow["HYPLDIAGCODE"])) condModel.hyperlipidaemiaIcd10 = condMasterRow["HYPLDIAGCODE"].ToString();
                        else condModel.hyperlipidaemiaIcd10 = "";

                        if (!DBNull.Value.Equals(condMasterRow["DIAGDESC"]) && condMasterRow["DIAGDESC"].ToString().Trim() != "")
                            condModel.hyperlipidaemiaIcd10 += " - " + condMasterRow["DIAGDESC"].ToString();

                        if (!DBNull.Value.Equals(condMasterRow["HYPLONTHERAPY"]))
                        {
                            if (condMasterRow["HYPLONTHERAPY"].ToString() == "Y") condModel.hyperlipidaemiatherapyYN = "Yes";
                            else condModel.hyperlipidaemiatherapyYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiatherapyYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLTHERAPYDURATUION"])) condModel.hyperlipidaemiaTherapyDuration = condMasterRow["HYPLTHERAPYDURATUION"].ToString();
                        else condModel.hyperlipidaemiaTherapyDuration = "";

                        if (!DBNull.Value.Equals(condMasterRow["HYPLATHEROSCLEROTIC"]))
                        {
                            if (condMasterRow["HYPLATHEROSCLEROTIC"].ToString() == "Y") condModel.hyperlipidaemiaAtheroscleroticYN = "Yes";
                            else condModel.hyperlipidaemiaAtheroscleroticYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaAtheroscleroticYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLTREATHYPERTENSION"]))
                        {
                            if (condMasterRow["HYPLTREATHYPERTENSION"].ToString() == "Y") condModel.hyperlipidaemiaHypertensionTreatmentYN = "Yes";
                            else condModel.hyperlipidaemiaHypertensionTreatmentYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaHypertensionTreatmentYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLGENETIC"]))
                        {
                            if (condMasterRow["HYPLGENETIC"].ToString() == "Y") condModel.hyperlipidaemiaGeneticHyperlipidaemiaYN = "Yes";
                            else condModel.hyperlipidaemiaGeneticHyperlipidaemiaYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaGeneticHyperlipidaemiaYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLMALERELATIVE"]))
                        {
                            if (condMasterRow["HYPLMALERELATIVE"].ToString() == "Y") condModel.hyperlipidaemiaMaleBloodRelativeYN = "Yes";
                            else condModel.hyperlipidaemiaMaleBloodRelativeYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaMaleBloodRelativeYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLFEMALERELATIVE"]))
                        {
                            if (condMasterRow["HYPLFEMALERELATIVE"].ToString() == "Y") condModel.hyperlipidaemiaFemaleBloodRelativeYN = "Yes";
                            else condModel.hyperlipidaemiaFemaleBloodRelativeYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaFemaleBloodRelativeYN = "";
                        }

                        if (!DBNull.Value.Equals(condMasterRow["HYPLTENDONXANTHOMA"]))
                        {
                            if (condMasterRow["HYPLTENDONXANTHOMA"].ToString() == "Y") condModel.hyperlipidaemiaTendonXanthomaYN = "Yes";
                            else condModel.hyperlipidaemiaTendonXanthomaYN = "No";
                        }
                        else
                        {
                            condModel.hyperlipidaemiaTendonXanthomaYN = "";
                        }

                    }
                    else if (condMasterRow["CONDCODE"].ToString() == "HYPER")
                    {
                        if (!DBNull.Value.Equals(condMasterRow["HYPTDIAGCODE"])) condModel.hypertensionIcd10 = condMasterRow["HYPTDIAGCODE"].ToString();
                        else condModel.hypertensionIcd10 = "";

                        if (!DBNull.Value.Equals(condMasterRow["DIAGDESC"]) && condMasterRow["DIAGDESC"].ToString().Trim() != "")
                            condModel.hypertensionIcd10 += " - " + condMasterRow["DIAGDESC"].ToString();

                        if (!DBNull.Value.Equals(condMasterRow["HYPTSEVERITY"])) condModel.hypertensionSeverity = condMasterRow["HYPTSEVERITY"].ToString();
                        else condModel.hypertensionSeverity = "";

                        if (!DBNull.Value.Equals(condMasterRow["HYPTONTHERAPY"]))
                        {
                            if (condMasterRow["HYPTONTHERAPY"].ToString() == "Y") condModel.hypertensionTherapyYN = "Yes";
                            else condModel.hypertensionTherapyYN = "No";
                        }
                        else
                        {
                            condModel.hypertensionTherapyYN = "";
                        }
                    }
                    else if (condMasterRow["CONDCODE"].ToString() == "ASTHMA")
                    {
                        if (!DBNull.Value.Equals(condMasterRow["ASTHMDIAGCODE"])) condModel.asthmaIcd10 = condMasterRow["ASTHMDIAGCODE"].ToString();
                        else condModel.asthmaIcd10 = "";

                        if (!DBNull.Value.Equals(condMasterRow["DIAGDESC"]) && condMasterRow["DIAGDESC"].ToString().Trim() != "")
                            condModel.asthmaIcd10 += " - " + condMasterRow["DIAGDESC"].ToString();

                        if (!DBNull.Value.Equals(condMasterRow["ASTHMPEAKFLOW"])) condModel.asthmaPleakFlow = condMasterRow["ASTHMPEAKFLOW"].ToString();
                        else condModel.asthmaPleakFlow = "";

                        if (!DBNull.Value.Equals(condMasterRow["ASTHM3YOUNGER"]))
                        {
                            if (condMasterRow["ASTHM3YOUNGER"].ToString() == "Y") condModel.asthmaYounger3YN = "Yes";
                            else condModel.asthmaYounger3YN = "No";
                        }
                        else
                        {
                            condModel.asthmaYounger3YN = "";
                        }
                    }
                }

                // Get data from CONDITION_MEDICATION
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@condAppNo", condAppNo));
                cmd.CommandText =
                    $" SELECT CM.DIAGCODE, CD.DIAGDESC, CM.SVCDESCR, CM.STRENGTH, CM.DOSAGE, CM.QTY, CM.NOREPEAT ,CM.HISTORY, CM.COMORBID, CM.SECTION " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_MEDICATION CM \n" +
                    $" LEFT JOIN {DRCDatabase}.dbo.DIAG_CODES CD on (CM.DIAGCODE = CD.DIAGCODE) \n" +
                    $" WHERE CM.CONDAPPNO = @condAppNo ";

                DataTable dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();

                condModel.requestedMeds = new List<MedicationModel>();
                condModel.hyperlipidaemiaMedHist = new List<MedicationModel>();
                condModel.hypertensionMedHist = new List<MedicationModel>();
                condModel.asthmaCmMedList = new List<MedicationModel>();
                condModel.diabCmMedList = new List<MedicationModel>();
                condModel.hyperlipCmMedList = new List<MedicationModel>();
                condModel.hypertensionCmMedList = new List<MedicationModel>();
                condModel.addisCmMedList = new List<MedicationModel>();
                condModel.bipolCmMedList = new List<MedicationModel>();
                condModel.bronchCmMedList = new List<MedicationModel>();
                condModel.cardiacCmMedList = new List<MedicationModel>();
                condModel.cardiomCmMedList = new List<MedicationModel>();
                condModel.copdCmMedList = new List<MedicationModel>();
                condModel.renalCmMedList = new List<MedicationModel>();
                condModel.coronarCmMedList = new List<MedicationModel>();
                condModel.crohnsCmMedList = new List<MedicationModel>();
                condModel.diabinsipCmMedList = new List<MedicationModel>();
                condModel.dysrhytCmMedList = new List<MedicationModel>();
                condModel.epilCmMedList = new List<MedicationModel>();
                condModel.glaucomCmMedList = new List<MedicationModel>();
                condModel.haemophilCmMedList = new List<MedicationModel>();
                condModel.hypothCmMedList = new List<MedicationModel>();
                condModel.sclerosisCmMedList = new List<MedicationModel>();
                condModel.parkinsCmMedList = new List<MedicationModel>();
                condModel.rheumaCmMedList = new List<MedicationModel>();
                condModel.schizoCmMedList = new List<MedicationModel>();
                condModel.lupusCmMedList = new List<MedicationModel>();
                condModel.ulceraCmMedList = new List<MedicationModel>();

                foreach (DataRow medRow in dt.Rows)
                {
                    MedicationModel medItem = new MedicationModel();

                    medItem.icd10 = medRow["DIAGCODE"].ToString();
                    if (!DBNull.Value.Equals(medRow["DIAGDESC"]))
                        medItem.icd10 += " - " + medRow["DIAGDESC"].ToString();
                    medItem.svcCodeDescr = medRow["SVCDESCR"].ToString();
                    medItem.strength = medRow["STRENGTH"].ToString();
                    medItem.dosage = medRow["DOSAGE"].ToString();
                    medItem.qtyPerMonth = medRow["QTY"].ToString();
                    medItem.numRepeats = medRow["NOREPEAT"].ToString();
                    if (!DBNull.Value.Equals(medRow["HISTORY"]) && medRow["HISTORY"].ToString() == "Y")
                        medItem.historyMed = true;
                    else
                        medItem.historyMed = false;
                    if (!DBNull.Value.Equals(medRow["COMORBID"]) && medRow["COMORBID"].ToString() == "Y")
                        medItem.comorbid = true;
                    else
                        medItem.comorbid = false;
                    if (!DBNull.Value.Equals(medRow["SECTION"]))
                        medItem.section = medRow["SECTION"].ToString();
                    else
                        medItem.section = "";

                    // Add to relevant list                       
                    if (medItem.section == "hyperlipOnTherapyMedHist")
                    {
                        condModel.hyperlipidaemiaMedHist.Add(medItem);
                    }
                    else if (medItem.section == "hypertensionOnTherapyMedHist")
                    {
                        condModel.hypertensionMedHist.Add(medItem);
                    }
                    else if (medItem.section == "requestedMedication")
                    {
                        condModel.requestedMeds.Add(medItem);
                    }
                    else if (medItem.section == "asthmaComorbidMedHist")
                    {
                        condModel.asthmaCmMedList.Add(medItem);
                    }
                    else if (medItem.section == "diabComorbidMedHist")
                    {
                        condModel.diabCmMedList.Add(medItem);
                    }
                    else if (medItem.section == "hyperlipComorbidMedHist")
                    {
                        condModel.hyperlipCmMedList.Add(medItem);
                    }
                    else if (medItem.section == "hypertensionComorbidMedHist") 
                    { 
                        condModel.hypertensionCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "addisComorbidMedHist") 
                    { 
                        condModel.addisCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "bipolComorbidMedHist") 
                    { 
                        condModel.bipolCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "bronchComorbidMedHist") 
                    { 
                        condModel.bronchCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "cardiacComorbidMedHist") 
                    { 
                        condModel.cardiacCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "cardiomComorbidMedHist") 
                    { 
                        condModel.cardiomCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "copdComorbidMedHist") 
                    { 
                        condModel.copdCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "renalComorbidMedHist") 
                    { 
                        condModel.renalCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "coronarComorbidMedHist") 
                    { 
                        condModel.coronarCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "crohnsComorbidMedHist") 
                    { 
                        condModel.crohnsCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "diabinsipComorbidMedHist") 
                    { 
                        condModel.diabinsipCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "dysrhytComorbidMedHist") 
                    { 
                        condModel.dysrhytCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "epilComorbidMedHist") 
                    { 
                        condModel.epilCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "glaucomComorbidMedHist") 
                    { 
                        condModel.glaucomCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "haemophilComorbidMedHist") 
                    { 
                        condModel.haemophilCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "hypothComorbidMedHist") 
                    { 
                        condModel.hypothCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "sclerosisComorbidMedHist") 
                    { 
                        condModel.sclerosisCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "parkinsComorbidMedHist") 
                    { 
                        condModel.parkinsCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "rheumaComorbidMedHist") 
                    { 
                        condModel.rheumaCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "schizoComorbidMedHist") 
                    { 
                        condModel.schizoCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "lupusComorbidMedHist") 
                    { 
                        condModel.lupusCmMedList.Add(medItem); 
                    }
                    else if (medItem.section == "ulceraComorbidMedHist") 
                    { 
                        condModel.ulceraCmMedList.Add(medItem); 
                    }

                }

                // Get data from CONDITION_FILES
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@condAppNo", condAppNo));
                cmd.CommandText =
                    $" SELECT CD.CONDAPPNO, CD.ID, CD.FILEDESC, CD.FILETYPE, CF.FILENAME, CF.DOCBINARY " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_FILES CD \n" +
                    $" INNER JOIN {DRCDatabase}.dbo.CPS_FILES CF on (CD.ID = CF.ID) \n" +
                    $" WHERE CD.CONDAPPNO = @condAppNo ";
                dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();

                condModel.referralLetterFiles = new List<FileItemModel>();
                condModel.medPrescriptionFiles = new List<FileItemModel>();
                condModel.additionalDocsFiles = new List<FileItemModel>();
                condModel.asthmaPaedFiles = new List<FileItemModel>();
                condModel.asthmaFlowVolFiles = new List<FileItemModel>();
                condModel.hyperlipPathResultFiles = new List<FileItemModel>();
                condModel.diabPathResultFiles = new List<FileItemModel>();

                foreach (DataRow fileRow in dt.Rows)
                {
                    FileItemModel fileItem = new FileItemModel();

                    fileItem.name = fileRow["FILENAME"].ToString();
                    fileItem.type = fileRow["FILETYPE"].ToString();
                    fileItem.fileDesc = fileRow["FILEDESC"].ToString();
                    //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    byte[] arrFileBytes = fileRow["DOCBINARY"] as byte[];
                    fileItem.base64Str = Convert.ToBase64String(arrFileBytes, 0, arrFileBytes.Length);

                    // Add to relevant file array
                    if (fileItem.fileDesc == "referralLetter")
                    {
                        condModel.referralLetterFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "asthmaPaedReport")
                    {
                        condModel.asthmaPaedFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "asthmaFlowVolFile")
                    {
                        condModel.asthmaFlowVolFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "medPrescription")
                    {
                        condModel.medPrescriptionFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "additionalDocs")
                    {
                        condModel.additionalDocsFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "hyperlipPathResults")
                    {
                        condModel.hyperlipPathResultFiles.Add(fileItem);
                    }
                    else if (fileItem.fileDesc == "diabPathResults")
                    {
                        condModel.diabPathResultFiles.Add(fileItem);
                    }
                }

                // Get data from CONDITION_COMORBIDS
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@condAppNo", condAppNo));
                cmd.CommandText =
                    $" SELECT CM.CONDAPPNO, CM.COMORBIDCONDCODE, CM.COMORBIDDIAGCODE, CD.DIAGDESC " +
                    $" FROM {DRCDatabase}.dbo.CONDITION_COMORBIDS CM \n" +
                    $" LEFT JOIN {DRCDatabase}.dbo.DIAG_CODES CD on (CM.COMORBIDDIAGCODE = CD.DIAGCODE) \n" +
                    $" WHERE CM.CONDAPPNO = @condAppNo ";

                dt = new DataTable();
                if (cn.State != ConnectionState.Open) cn.Open();
                dt.Load(cmd.ExecuteReader());
                cn.Close();

                if (dt.Rows.Count < 1)
                {
                    condModel.comorbidsExist = false;
                }
                else
                {
                    condModel.comorbidsExist = true;

                    foreach (DataRow comorbidRow in dt.Rows)
                    {
                        if (!DBNull.Value.Equals(comorbidRow["COMORBIDCONDCODE"]) && !DBNull.Value.Equals(comorbidRow["COMORBIDDIAGCODE"]))
                        {
                            string cmCondition = comorbidRow["COMORBIDCONDCODE"].ToString();
                            string cmDiag = comorbidRow["COMORBIDDIAGCODE"].ToString();
                            if (!DBNull.Value.Equals(comorbidRow["DIAGDESC"]))
                                cmDiag = cmDiag + " - " + comorbidRow["DIAGDESC"].ToString();

                            if (cmCondition == "ASTHMA") condModel.asthmaCmICD10 = cmDiag;
                            else if (cmCondition == "DIAB1" || cmCondition == "DIAB2") condModel.diabCmICD10 = cmDiag;
                            else if (cmCondition == "CHOL") condModel.hyperlipCmICD10 = cmDiag;
                            else if (cmCondition == "HYPER") condModel.hypertensionCmICD10 = cmDiag;
                            else if (cmCondition == "ADDIS") condModel.addisCmICD10 = cmDiag;
                            else if (cmCondition == "BIPOL") condModel.bipolCmICD10 = cmDiag;
                            else if (cmCondition == "BRONCH") condModel.bronchCmICD10 = cmDiag;
                            else if (cmCondition == "CARDIAC") condModel.cardiacCmICD10 = cmDiag;
                            else if (cmCondition == "CARDIOM") condModel.cardiomCmICD10 = cmDiag;
                            else if (cmCondition == "COPD") condModel.copdCmICD10 = cmDiag;
                            else if (cmCondition == "RENAL") condModel.renalCmICD10 = cmDiag;
                            else if (cmCondition == "CORONAR") condModel.coronarCmICD10 = cmDiag;
                            else if (cmCondition == "CROHNS") condModel.crohnsCmICD10 = cmDiag;
                            else if (cmCondition == "DIABINSIP") condModel.diabinsipCmICD10 = cmDiag;
                            else if (cmCondition == "DYSRHYT") condModel.dysrhytCmICD10 = cmDiag;
                            else if (cmCondition == "EPIL") condModel.epilCmICD10 = cmDiag;
                            else if (cmCondition == "GLAUCOM") condModel.glaucomCmICD10 = cmDiag;
                            else if (cmCondition == "HAEMOPHIL") condModel.haemophilCmICD10 = cmDiag;
                            else if (cmCondition == "HYPOTH") condModel.hypothCmICD10 = cmDiag;
                            else if (cmCondition == "SCLEROSIS") condModel.sclerosisCmICD10 = cmDiag;
                            else if (cmCondition == "PARKINS") condModel.parkinsCmICD10 = cmDiag;
                            else if (cmCondition == "RHEUMA") condModel.rheumaCmICD10 = cmDiag;
                            else if (cmCondition == "SCHIZO") condModel.schizoCmICD10 = cmDiag;
                            else if (cmCondition == "LUPUS") condModel.lupusCmICD10 = cmDiag;
                            else if (cmCondition == "ULCERA") condModel.ulceraCmICD10 = cmDiag;
                        }
                    }
                }

                condModel.success = true;
            }
            else
            {
                condModel.success = false;
                condModel.message = "No application found";
            }

            return condModel;
        }

        //[HttpPost]
        //[Route("SubmitApplication")]
        //[Authorize(Roles = "NormalUser,Administrator")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public ChronicConditionApplicationModel SubmitApplication([FromBody] ChronicConditionApplicationModel applicationData)
        //{
        //    applicationData.success = false;
        //    applicationData.message = "";
        //    try
        //    {
        //        SqlConnection cn = new SqlConnection(_drcConnectionString);
        //        SqlCommand cmd = new SqlCommand();


        //        foreach (string condition in applicationData.selectedConditions)
        //        {
        //            cmd = new SqlCommand();
        //            cmd.Connection = cn;

        //            string conditionApplicationNumber = DateTime.Now.ToString("yyyyMMddHHmmssff");

        //            cmd.Parameters.Add(new SqlParameter("@condAppNumber", conditionApplicationNumber));
        //            cmd.Parameters.Add(new SqlParameter("@membId", applicationData.membId));
        //            cmd.Parameters.Add(new SqlParameter("@membEmail", applicationData.membEmail));
        //            cmd.Parameters.Add(new SqlParameter("@provId", applicationData.provId));
        //            cmd.Parameters.Add(new SqlParameter("@provEmail", applicationData.provEmail));
        //            //cmd.Parameters.Add(new SqlParameter("@applicationDate", DateTime.Now));
        //            //cmd.Parameters.Add(new SqlParameter("@status", applicationData.status)); // P
        //            cmd.Parameters.Add(new SqlParameter("@weight", applicationData.weight));
        //            cmd.Parameters.Add(new SqlParameter("@height", applicationData.height));
        //            cmd.Parameters.Add(new SqlParameter("@bmi", applicationData.bmi));
        //            cmd.Parameters.Add(new SqlParameter("@bloodPresSystolic", applicationData.bloodPresSystolic));
        //            cmd.Parameters.Add(new SqlParameter("@bloodPresDiastolic", applicationData.bloodPresDiastolic));
        //            if (applicationData.diabetesType == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
        //                cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
        //            }
        //            else if (applicationData.diabetesType == "diabetesType1")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@type1Diab", "Y"));
        //                cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
        //            }
        //            else if (applicationData.diabetesType == "diabetesType2")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
        //                cmd.Parameters.Add(new SqlParameter("@type2Diab", "Y"));
        //            }
        //            if (applicationData.hba1c == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@HbA1c", DBNull.Value));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@HbA1c", applicationData.hba1c));
        //            }
        //            if (applicationData.fastingOrRandom == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
        //            }
        //            else if (applicationData.fastingOrRandom == "fasting")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", "Y"));
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
        //            }
        //            else if (applicationData.fastingOrRandom == "random")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
        //                cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", "Y"));
        //            }
        //            if (applicationData.diabTestDate == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@diabTestDate", DBNull.Value));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@diabTestDate", applicationData.diabTestDate));
        //            }
        //            cmd.Parameters.Add(new SqlParameter("@cholesterolHDL", applicationData.cholesterolHDL));
        //            cmd.Parameters.Add(new SqlParameter("@cholesterolLDL", applicationData.cholesterolLDL));
        //            cmd.Parameters.Add(new SqlParameter("@cholesterolTRI", applicationData.cholesterolTRI));
        //            cmd.Parameters.Add(new SqlParameter("@cholesterolTotal", applicationData.cholesterolTotal));
        //            cmd.Parameters.Add(new SqlParameter("@alcohol", applicationData.alcohol));
        //            cmd.Parameters.Add(new SqlParameter("@obesity", applicationData.obesity));
        //            cmd.Parameters.Add(new SqlParameter("@smoker", applicationData.smoker));
        //            if (applicationData.smokeInPast == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@smokeInPast", DBNull.Value));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@smokeInPast", applicationData.smokeInPast));
        //            }
        //            if (applicationData.cessationDate == null)
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@cessationDate", DBNull.Value));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@cessationDate", applicationData.cessationDate));
        //            }
        //            if (applicationData.alcohol == "Y" || applicationData.obesity == "Y" || applicationData.smoker == "Y")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "Y"));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "N"));
        //            }
        //            cmd.Parameters.Add(new SqlParameter("@ischaemicHeartDisease", applicationData.ischaemicHeartDisease));
        //            cmd.Parameters.Add(new SqlParameter("@peripheralVascularDisease", applicationData.peripheralVascularDisease));
        //            cmd.Parameters.Add(new SqlParameter("@strokeAttacks", applicationData.strokeAttacks));
        //            if (applicationData.ischaemicHeartDisease == "Y" || applicationData.peripheralVascularDisease == "Y" || applicationData.strokeAttacks == "Y")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "Y"));
        //            }
        //            else
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "N"));
        //            }

        //            // Columns and values for the fields that is inserted with every application
        //            string sqlColumns =
        //                 "  [CONDAPPNO] \n" +
        //                 " ,[MEMBID]  \n" +
        //                 " ,[MEMBEMAIL]  \n" +
        //                 " ,[PROVID]  \n" +
        //                 " ,[PROVEMAIL]  \n" +
        //                 " ,[APPDATE]  \n" +
        //                 " ,[STATUS]  \n" +
        //                 " ,[WGHT]  \n" +
        //                 " ,[HEIGHT]  \n" +
        //                 " ,[BMI]  \n" +
        //                 " ,[BPSYSTOLIC] \n" +
        //                 " ,[BPDIASTOLIC] \n" +
        //                 " ,[DIABTYPE1] \n" +
        //                 " ,[DIABTYPE2] \n" +
        //                 " ,[HBA1C]  \n" +
        //                 " ,[DIABTYPE2FAST] \n" +
        //                 " ,[DIABTYPE2RANDOM] \n" +
        //                 " ,[DIABTESTDATE] \n" +
        //                 " ,[HDL] \n" +
        //                 " ,[LDL] \n" +
        //                 " ,[TRIGLYCERIDES] \n" +
        //                 " ,[CHOLESTEROL] \n" +
        //                 " ,[PREDRISK]  \n" +
        //                 " ,[ALCOHOL]  \n" +
        //                 " ,[OBESITY]  \n" +
        //                 " ,[SMOKER]  \n" +
        //                 " ,[PASTSMOKE] \n" +
        //                 " ,[SMOKEENDDATE] \n" +
        //                 " ,[PRECOMP]  \n" +
        //                 " ,[HEARTDISEASE] \n" +
        //                 " ,[VASCDISEASE] \n" +
        //                 " ,[TIA]  \n" +
        //                 " ,[MAILSENT] \n" +
        //                 " ,[CREATEBY]  \n" +
        //                 " ,[CREATEDATE]  \n" +
        //                 " ,[LASTCHANGEBY]  \n" +
        //                 " ,[LASTCHANGEDATE]  \n";

        //            string sqlValues =
        //                "  @condAppNumber  \n " +
        //                " ,@membId  \n " +
        //                " ,@membEmail  \n " +
        //                " ,@provId  \n " +
        //                " ,@provEmail  \n " +
        //                " ,GETDATE()  \n" + // APPDATE
        //                " ,'P'  \n" +
        //                " ,@weight  \n" +
        //                " ,@height  \n" +
        //                " ,@bmi  \n" +
        //                " ,@bloodPresSystolic \n " +
        //                " ,@bloodPresDiastolic \n " +
        //                " ,@type1Diab \n " +
        //                " ,@type2Diab \n " +
        //                " ,@HbA1c \n" +
        //                " ,@DIABTYPE2FAST " +
        //                " ,@DIABTYPE2RANDOM " +
        //                " ,@diabTestDate  \n" +
        //                " ,@cholesterolHDL \n" +
        //                " ,@cholesterolLDL \n" +
        //                " ,@cholesterolTRI \n" +
        //                " ,@cholesterolTotal \n" +
        //                " ,@predisposingRiskFactors  \n" +
        //                " ,@alcohol  \n" +
        //                " ,@obesity  \n" +
        //                " ,@smoker  \n" +
        //                " ,@smokeInPast  \n" +
        //                " ,@cessationDate  \n" +
        //                " ,@preExistingComplications  \n" +
        //                " ,@ischaemicHeartDisease  \n" +
        //                " ,@peripheralVascularDisease  \n" +
        //                " ,@strokeAttacks  \n" +
        //                " ,0 \n" + //mailsent
        //                " ,555  \n" +
        //                " ,GETDATE()  \n" +
        //                " ,555  \n" +
        //                " ,GETDATE()  \n";

        //            // Condition specific fields
        //            if (condition == "DIAB")
        //            {
        //                if (applicationData.diabetesType == "diabetesType1")
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@ConditionCode", "DIAB1"));
        //                }
        //                else if (applicationData.diabetesType == "diabetesType2")
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@ConditionCode", "DIAB2"));
        //                }

        //                cmd.Parameters.Add(new SqlParameter("@diabetesIcd10", applicationData.diabetesIcd10));

        //                sqlColumns +=
        //                    " ,[CONDCODE] \n" +
        //                    " ,[DIABDIAGCODE] \n";

        //                sqlValues +=
        //                     " ,@ConditionCode \n" +
        //                     " ,@diabetesIcd10 \n";
        //            }
        //            else if (condition == "CHOL")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@ConditionCode", "CHOL"));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaIcd10", applicationData.hyperlipidaemiaIcd10));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiatherapyYN", applicationData.hyperlipidaemiatherapyYN));
        //                if (applicationData.hyperlipidaemiaTherapyDuration == null)
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTherapyDuration", DBNull.Value));
        //                }
        //                else
        //                {
        //                    cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTherapyDuration", applicationData.hyperlipidaemiaTherapyDuration));
        //                }
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaAtheroscleroticYN", applicationData.hyperlipidaemiaAtheroscleroticYN));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaHypertensionTreatmentYN", applicationData.hyperlipidaemiaHypertensionTreatmentYN));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaGeneticHyperlipidaemiaYN", applicationData.hyperlipidaemiaGeneticHyperlipidaemiaYN));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaMaleBloodRelativeYN", applicationData.hyperlipidaemiaMaleBloodRelativeYN));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaFemaleBloodRelativeYN", applicationData.hyperlipidaemiaFemaleBloodRelativeYN));
        //                cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTendonXanthomaYN", applicationData.hyperlipidaemiaTendonXanthomaYN));

        //                sqlColumns +=
        //                    " ,[CONDCODE] \n" +
        //                    " ,[HYPLDIAGCODE] \n" +
        //                    " ,[HYPLONTHERAPY] \n" +
        //                    " ,[HYPLTHERAPYDURATUION] \n" +
        //                    " ,[HYPLATHEROSCLEROTIC] \n" +
        //                    " ,[HYPLTREATHYPERTENSION] \n" +
        //                    " ,[HYPLGENETIC] \n" +
        //                    " ,[HYPLMALERELATIVE] \n" +
        //                    " ,[HYPLFEMALERELATIVE] \n" +
        //                    " ,[HYPLTENDONXANTHOMA] \n";

        //                sqlValues +=
        //                     " ,@ConditionCode \n" +
        //                     " ,@hyperlipidaemiaIcd10 \n" +
        //                     " ,@hyperlipidaemiatherapyYN \n" +
        //                     " ,@hyperlipidaemiaTherapyDuration \n" +
        //                     " ,@hyperlipidaemiaAtheroscleroticYN \n" +
        //                     " ,@hyperlipidaemiaHypertensionTreatmentYN \n" +
        //                     " ,@hyperlipidaemiaGeneticHyperlipidaemiaYN \n" +
        //                     " ,@hyperlipidaemiaMaleBloodRelativeYN \n" +
        //                     " ,@hyperlipidaemiaFemaleBloodRelativeYN \n" +
        //                     " ,@hyperlipidaemiaTendonXanthomaYN \n";
        //            }
        //            else if (condition == "HYPER")
        //            {
        //                cmd.Parameters.Add(new SqlParameter("@ConditionCode", "HYPER"));
        //                cmd.Parameters.Add(new SqlParameter("@hypertensionIcd10", applicationData.hypertensionIcd10));
        //                cmd.Parameters.Add(new SqlParameter("@hypertensionSeverity", applicationData.hypertensionSeverity));
        //                cmd.Parameters.Add(new SqlParameter("@hypertensionTherapyYN", applicationData.hypertensionTherapyYN));

        //                sqlColumns +=
        //                    " ,[CONDCODE] \n" +
        //                    " ,[HYPTDIAGCODE] \n" +
        //                    " ,[HYPTSEVERITY] \n" +
        //                    " ,[HYPTONTHERAPY] \n";

        //                sqlValues +=
        //                     " ,@ConditionCode \n" +
        //                     " ,@hypertensionIcd10 \n" +
        //                     " ,@hypertensionSeverity \n" +
        //                     " ,@hypertensionTherapyYN \n";
        //            }

        //            // Do CONDITION_APPLICATION insert
        //            cmd.CommandText =
        //                 $" INSERT INTO {DRCDatabase}.dbo.CONDITION_APPLICATION ( " + sqlColumns + " ) \n " +
        //                  " VALUES ( " + sqlValues + " ) ";

        //            if (cn.State != ConnectionState.Open) cn.Open();
        //            cmd.ExecuteScalar();


        //            // Do Medication inserts
        //            int sequence = 0;
        //            InsertMedication(ref sequence, conditionApplicationNumber, applicationData.requestedMeds, cn, cmd);

        //            if (/*condition == "CHOL" && */ applicationData.hyperlipidaemiaMedHist.Count > 0)
        //            {
        //                InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hyperlipidaemiaMedHist, cn, cmd);
        //            }
        //            if (/*condition == "HYPER" && */ applicationData.hypertensionMedHist.Count > 0)
        //            {
        //                InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hypertensionMedHist, cn, cmd);
        //            }

        //            // Do File inserts
        //            InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.medPrescriptionFiles, cn, cmd);
        //            if (condition == "DIAB")
        //            {
        //                InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.diabetesFiles, cn, cmd);
        //            }
        //            if (condition == "CHOL")
        //            {
        //                InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.hyperlipidaemiaFiles, cn, cmd);
        //            }

        //            // Send confirmation Email
        //            string condCode = "";
        //            if (condition == "DIAB")
        //            {
        //                if (applicationData.diabetesType == "diabetesType1") condCode = "DIAB1";
        //                else if (applicationData.diabetesType == "diabetesType2") condCode = "DIAB2";
        //                else condCode = condition;
        //            }
        //            else
        //            {
        //                condCode = condition;
        //            }

        //            cmd = new SqlCommand();
        //            if (cn.State != ConnectionState.Open) cn.Open();
        //            cmd.Connection = cn;
        //            cmd.CommandText = $"SELECT description From {PamcPortalDatabase}.dbo.Settings where settingsId = 'ChronicConditionNotificationEmail' ";
        //            DataTable dt = new DataTable();
        //            dt.Load(cmd.ExecuteReader());
        //            string DestinationEmail = dt.Rows[0]["description"].ToString();
        //            string msg = $"Good day, \r\n\r\n" +
        //                            $"The following Chronic Condition Application was captured via the web portal:\r\n\r\n" +
        //                            $"CONDITION APPLICATION NUMBER: {conditionApplicationNumber}.\r\n" +
        //                            $"CONDITION CODE: {condCode}\r\n" +
        //                            $"MEMBID: {applicationData.membId}\r\n" +
        //                            $"PROVID: {applicationData.provId}\r\n\r\n" +
        //                            $"Regards\r\n" +
        //                            $"WEB PORTAL\r\n";
        //            string url = _EmailWebSvcConnection;
        //            EndpointAddress address = new EndpointAddress(url);
        //            BasicHttpBinding binding = new BasicHttpBinding();
        //            binding.MaxReceivedMessageSize = 2147483647;
        //            var time = new TimeSpan(0, 30, 0);
        //            binding.CloseTimeout = time;
        //            binding.OpenTimeout = time;
        //            binding.ReceiveTimeout = time;
        //            binding.SendTimeout = time;
        //            EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
        //            var c = client.SendEmailAsync(msg, $"NEW CHRONIC CONDITION APPLICATION: {conditionApplicationNumber}", $"{DestinationEmail}");
        //            //c.Wait();                                        
        //        }

        //        cn.Close();
        //        cmd.Dispose();

        //        applicationData.success = true;
        //        applicationData.message = "";
        //    }
        //    catch (Exception e)
        //    {
        //        applicationData.success = false;
        //        applicationData.message = e.Message;

        //        LogError(e.Message, e.StackTrace);
        //    }

        //    return applicationData;
        //}

        [HttpPost]
        [Route("SubmitApplicationOrUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ChronicConditionApplicationModel SubmitApplicationOrUpdate([FromBody] ChronicConditionApplicationModel applicationData)
        {
            applicationData.success = false;
            applicationData.message = "";
            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand();

                foreach (string condition in applicationData.selectedConditions)
                {
                    cmd = new SqlCommand();
                    cmd.Connection = cn;

                    string conditionApplicationNumber = DateTime.Now.ToString("yyyyMMddHHmmssff");

                    if (applicationData.currentPage == "ChronicCondApp")
                    {
                        cmd.Parameters.Add("@update", SqlDbType.Bit).Value = 0;
                        cmd.Parameters.Add(new SqlParameter("@status", "P"));
                    }
                    else if (applicationData.currentPage == "ChronicCondUpdate")
                    {
                        cmd.Parameters.Add("@update", SqlDbType.Bit).Value = 1;
                        cmd.Parameters.Add(new SqlParameter("@status", "U"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@update", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@status", ""));
                    }

                    cmd.Parameters.Add(new SqlParameter("@condAppNumber", conditionApplicationNumber));
                    cmd.Parameters.Add(new SqlParameter("@membId", applicationData.membId));
                    cmd.Parameters.Add(new SqlParameter("@membEmail", applicationData.membEmail));
                    cmd.Parameters.Add(new SqlParameter("@provId", applicationData.provId));
                    cmd.Parameters.Add(new SqlParameter("@provEmail", applicationData.provEmail));
                    cmd.Parameters.Add(new SqlParameter("@referredYN", applicationData.referredYN));

                    if (applicationData.referredYN == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@refProvid", applicationData.refProvid));
                        cmd.Parameters.Add(new SqlParameter("@refProvSurname", applicationData.refProvSurname));
                        cmd.Parameters.Add(new SqlParameter("@refProvSpec", applicationData.refProvSpec));
                        cmd.Parameters.Add(new SqlParameter("@refProvNotes", applicationData.refProvNotes));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@refProvid", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@refProvSurname", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@refProvSpec", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@refProvNotes", DBNull.Value));
                    }

                    //cmd.Parameters.Add(new SqlParameter("@applicationDate", DateTime.Now));
                    //cmd.Parameters.Add(new SqlParameter("@status", applicationData.status)); // P
                    cmd.Parameters.Add(new SqlParameter("@weight", applicationData.weight));
                    cmd.Parameters.Add(new SqlParameter("@height", applicationData.height));
                    cmd.Parameters.Add(new SqlParameter("@bmi", applicationData.bmi));
                    cmd.Parameters.Add(new SqlParameter("@bloodPresSystolic", applicationData.bloodPresSystolic));
                    cmd.Parameters.Add(new SqlParameter("@bloodPresDiastolic", applicationData.bloodPresDiastolic));
                    if (applicationData.diabetesType == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
                    }
                    else if (applicationData.diabetesType == "diabetesType1")
                    {
                        cmd.Parameters.Add(new SqlParameter("@type1Diab", "Y"));
                        cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
                    }
                    else if (applicationData.diabetesType == "diabetesType2")
                    {
                        cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
                        cmd.Parameters.Add(new SqlParameter("@type2Diab", "Y"));
                    }
                    if (applicationData.ogttResult == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ogttResult", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@ogttResult", applicationData.ogttResult));
                    }
                    if (applicationData.hba1c == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@HbA1c", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@HbA1c", applicationData.hba1c));
                    }
                    //if (applicationData.fastingOrRandom == null)
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
                    //}
                    //else if (applicationData.fastingOrRandom == "fasting")
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", "Y"));
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
                    //}
                    //else if (applicationData.fastingOrRandom == "random")
                    //{
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
                    //    cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", "Y"));
                    //}
                    if (applicationData.fastingGlucoseResult == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@fastingGlucoseResult", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@fastingGlucoseResult", applicationData.fastingGlucoseResult));
                    }
                    if (applicationData.randomGlucoseResult == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@randomGlucoseResult", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@randomGlucoseResult", applicationData.randomGlucoseResult));
                    }
                    if (applicationData.diabTestDate == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@diabTestDate", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@diabTestDate", applicationData.diabTestDate));
                    }
                    cmd.Parameters.Add(new SqlParameter("@cholesterolHDL", applicationData.cholesterolHDL));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolLDL", applicationData.cholesterolLDL));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolTRI", applicationData.cholesterolTRI));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolTotal", applicationData.cholesterolTotal));
                    cmd.Parameters.Add(new SqlParameter("@cholesterolLdlHdlRatio", applicationData.cholesterolLdlHdlRatio));
                    cmd.Parameters.Add(new SqlParameter("@alcohol", applicationData.alcohol));
                    cmd.Parameters.Add(new SqlParameter("@obesity", applicationData.obesity));
                    cmd.Parameters.Add(new SqlParameter("@smoker", applicationData.smoker));
                    if (applicationData.smokeInPast == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@smokeInPast", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@smokeInPast", applicationData.smokeInPast));
                    }
                    if (applicationData.cessationDate == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@cessationDate", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@cessationDate", applicationData.cessationDate));
                    }
                    if (applicationData.alcohol == "Y" || applicationData.obesity == "Y" || applicationData.smoker == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "Y"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "N"));
                    }
                    cmd.Parameters.Add(new SqlParameter("@ischaemicHeartDisease", applicationData.ischaemicHeartDisease));
                    cmd.Parameters.Add(new SqlParameter("@peripheralVascularDisease", applicationData.peripheralVascularDisease));
                    cmd.Parameters.Add(new SqlParameter("@strokeAttacks", applicationData.strokeAttacks));
                    if (applicationData.ischaemicHeartDisease == "Y" || applicationData.peripheralVascularDisease == "Y" || applicationData.strokeAttacks == "Y")
                    {
                        cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "Y"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "N"));
                    }

                    // Columns and values for the fields that is inserted with every application
                    string sqlColumns =
                         "  [CONDAPPNO] \n" +
                         " ,[MEMBID]  \n" +
                         " ,[MEMBEMAIL]  \n" +
                         " ,[PROVID]  \n" +
                         " ,[PROVEMAIL]  \n" +
                         " ,[REFERRED]  \n" +
                         " ,[REFPROVID]  \n" +
                         " ,[REFPROVLASTNM]  \n" +
                         " ,[REFPROVSPEC]  \n" +
                         " ,[REFNOTES]  \n" +
                         " ,[APPDATE]  \n" +
                         " ,[STATUS]  \n" +
                         " ,[WGHT]  \n" +
                         " ,[HEIGHT]  \n" +
                         " ,[BMI]  \n" +
                         " ,[BPSYSTOLIC] \n" +
                         " ,[BPDIASTOLIC] \n" +
                         " ,[DIABTYPE1] \n" +
                         " ,[DIABTYPE2] \n" +
                         " ,[DIABHBA1C]  \n" +
                         " ,[DIABFASTRESULT]  \n" +
                         " ,[DIABRANDOMRESULT] \n" +
                         " ,[DIABOGTT] \n" +  
                         " ,[DIABTESTDATE] \n" +
                         " ,[HDL] \n" +
                         " ,[LDL] \n" +
                         " ,[TRIGLYCERIDES] \n" +
                         " ,[CHOLESTEROL] \n" +
                         " ,[HDLLDLRATIO] \n" + 
                         " ,[PREDRISK]  \n" +
                         " ,[ALCOHOL]  \n" +
                         " ,[OBESITY]  \n" +
                         " ,[SMOKER]  \n" +
                         " ,[PASTSMOKE] \n" +
                         " ,[SMOKEENDDATE] \n" +
                         " ,[PRECOMP]  \n" +
                         " ,[HEARTDISEASE] \n" +
                         " ,[VASCDISEASE] \n" +
                         " ,[TIA]  \n" +
                         " ,[MAILSENT] \n" +
                         " ,[UPDATE] \n" +
                         " ,[CREATEBY]  \n" +
                         " ,[CREATEDATE]  \n" +
                         " ,[LASTCHANGEBY]  \n" +
                         " ,[LASTCHANGEDATE]  \n";

                    string sqlValues =
                        "  @condAppNumber  \n " +
                        " ,@membId  \n " +
                        " ,@membEmail  \n " +
                        " ,@provId  \n " +
                        " ,@provEmail  \n " +
                        " ,@referredYN  \n " +
                        " ,@refProvid  \n " +
                        " ,@refProvSurname  \n " +
                        " ,@refProvSpec  \n " +
                        " ,@refProvNotes  \n " +
                        " ,GETDATE()  \n" + // APPDATE
                        " ,@status  \n" +
                        " ,@weight  \n" +
                        " ,@height  \n" +
                        " ,@bmi  \n" +
                        " ,@bloodPresSystolic \n " +
                        " ,@bloodPresDiastolic \n " +
                        " ,@type1Diab \n " +
                        " ,@type2Diab \n " +
                        " ,@HbA1c \n" +
                        " ,@fastingGlucoseResult \n" +
                        " ,@randomGlucoseResult " +
                        " ,@ogttResult " +
                        " ,@diabTestDate  \n" +
                        " ,@cholesterolHDL \n" +
                        " ,@cholesterolLDL \n" +
                        " ,@cholesterolTRI \n" +
                        " ,@cholesterolTotal \n" +
                        " ,@cholesterolLdlHdlRatio \n" +
                        " ,@predisposingRiskFactors \n" +
                        " ,@alcohol  \n" +
                        " ,@obesity  \n" +
                        " ,@smoker  \n" +
                        " ,@smokeInPast  \n" +
                        " ,@cessationDate  \n" +
                        " ,@preExistingComplications  \n" +
                        " ,@ischaemicHeartDisease  \n" +
                        " ,@peripheralVascularDisease  \n" +
                        " ,@strokeAttacks  \n" +
                        " ,0 \n" + //MAILSENT
                        " ,@update \n" + //UPDATE
                        " ,555  \n" +
                        " ,GETDATE()  \n" +
                        " ,555  \n" +
                        " ,GETDATE()  \n";

                    // Condition specific fields
                    if (condition == "DIAB")
                    {
                        if (applicationData.diabetesType == "diabetesType1")
                        {
                            cmd.Parameters.Add(new SqlParameter("@ConditionCode", "DIAB1"));
                        }
                        else if (applicationData.diabetesType == "diabetesType2")
                        {
                            cmd.Parameters.Add(new SqlParameter("@ConditionCode", "DIAB2"));
                        }

                        cmd.Parameters.Add(new SqlParameter("@diabetesIcd10", applicationData.diabetesIcd10));

                        sqlColumns +=
                            " ,[CONDCODE] \n" +
                            " ,[DIABDIAGCODE] \n";

                        sqlValues +=
                             " ,@ConditionCode \n" +
                             " ,@diabetesIcd10 \n";
                    }
                    else if (condition == "CHOL")
                    {
                        cmd.Parameters.Add(new SqlParameter("@ConditionCode", condition));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaIcd10", applicationData.hyperlipidaemiaIcd10));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiatherapyYN", applicationData.hyperlipidaemiatherapyYN));
                        if (applicationData.hyperlipidaemiaTherapyDuration == null)
                        {
                            cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTherapyDuration", DBNull.Value));
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTherapyDuration", applicationData.hyperlipidaemiaTherapyDuration));
                        }
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaAtheroscleroticYN", applicationData.hyperlipidaemiaAtheroscleroticYN));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaHypertensionTreatmentYN", applicationData.hyperlipidaemiaHypertensionTreatmentYN));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaGeneticHyperlipidaemiaYN", applicationData.hyperlipidaemiaGeneticHyperlipidaemiaYN));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaMaleBloodRelativeYN", applicationData.hyperlipidaemiaMaleBloodRelativeYN));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaFemaleBloodRelativeYN", applicationData.hyperlipidaemiaFemaleBloodRelativeYN));
                        cmd.Parameters.Add(new SqlParameter("@hyperlipidaemiaTendonXanthomaYN", applicationData.hyperlipidaemiaTendonXanthomaYN));

                        sqlColumns +=
                            " ,[CONDCODE] \n" +
                            " ,[HYPLDIAGCODE] \n" +
                            " ,[HYPLONTHERAPY] \n" +
                            " ,[HYPLTHERAPYDURATUION] \n" +
                            " ,[HYPLATHEROSCLEROTIC] \n" +
                            " ,[HYPLTREATHYPERTENSION] \n" +
                            " ,[HYPLGENETIC] \n" +
                            " ,[HYPLMALERELATIVE] \n" +
                            " ,[HYPLFEMALERELATIVE] \n" +
                            " ,[HYPLTENDONXANTHOMA] \n";

                        sqlValues +=
                             " ,@ConditionCode \n" +
                             " ,@hyperlipidaemiaIcd10 \n" +
                             " ,@hyperlipidaemiatherapyYN \n" +
                             " ,@hyperlipidaemiaTherapyDuration \n" +
                             " ,@hyperlipidaemiaAtheroscleroticYN \n" +
                             " ,@hyperlipidaemiaHypertensionTreatmentYN \n" +
                             " ,@hyperlipidaemiaGeneticHyperlipidaemiaYN \n" +
                             " ,@hyperlipidaemiaMaleBloodRelativeYN \n" +
                             " ,@hyperlipidaemiaFemaleBloodRelativeYN \n" +
                             " ,@hyperlipidaemiaTendonXanthomaYN \n";
                    }
                    else if (condition == "HYPER")
                    {
                        cmd.Parameters.Add(new SqlParameter("@ConditionCode", condition));
                        cmd.Parameters.Add(new SqlParameter("@hypertensionIcd10", applicationData.hypertensionIcd10));
                        cmd.Parameters.Add(new SqlParameter("@hypertensionSeverity", applicationData.hypertensionSeverity));
                        cmd.Parameters.Add(new SqlParameter("@hypertensionTherapyYN", applicationData.hypertensionTherapyYN));

                        sqlColumns +=
                            " ,[CONDCODE] \n" +
                            " ,[HYPTDIAGCODE] \n" +
                            " ,[HYPTSEVERITY] \n" +
                            " ,[HYPTONTHERAPY] \n";

                        sqlValues +=
                             " ,@ConditionCode \n" +
                             " ,@hypertensionIcd10 \n" +
                             " ,@hypertensionSeverity \n" +
                             " ,@hypertensionTherapyYN \n";
                    }
                    else if (condition == "ASTHMA")
                    {
                        cmd.Parameters.Add(new SqlParameter("@ConditionCode", condition));
                        cmd.Parameters.Add(new SqlParameter("@asthmaIcd10", applicationData.asthmaIcd10));
                        cmd.Parameters.Add(new SqlParameter("@asthmaPleakFlow", applicationData.asthmaPleakFlow));
                        cmd.Parameters.Add(new SqlParameter("@asthmaYounger3YN", applicationData.asthmaYounger3YN));

                        sqlColumns +=
                             " ,[CONDCODE] \n" +
                             " ,[ASTHMDIAGCODE] \n" +
                             " ,[ASTHMPEAKFLOW] \n" +
                             " ,[ASTHM3YOUNGER] \n";

                        sqlValues +=
                             " ,@ConditionCode \n" +
                             " ,@asthmaIcd10 \n" +
                             " ,@asthmaPleakFlow \n" +
                             " ,@asthmaYounger3YN \n";
                    }

                    // Do CONDITION_APPLICATION insert
                    cmd.CommandText =
                         $" INSERT INTO {DRCDatabase}.dbo.CONDITION_APPLICATION ( " + sqlColumns + " ) \n " +
                          " VALUES ( " + sqlValues + " ) ";

                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.ExecuteScalar();


                    // Do Medication inserts
                    int sequence = 0;
                    InsertMedication(ref sequence, conditionApplicationNumber, applicationData.requestedMeds, cn, cmd);

                    if (condition == "CHOL" && applicationData.hyperlipidaemiaMedHist.Count > 0)
                    {
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hyperlipidaemiaMedHist, cn, cmd);
                    }
                    if (condition == "HYPER" && applicationData.hypertensionMedHist.Count > 0)
                    {
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hypertensionMedHist, cn, cmd);
                    }
                    if (applicationData.asthmaCmICD10 != null && applicationData.asthmaCmMedList.Count > 0)
                    {
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.asthmaCmMedList, cn, cmd);
                    }
                    if (applicationData.diabCmICD10 != null && applicationData.diabCmMedList.Count > 0)
                    {
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.diabCmMedList, cn, cmd);
                    }
                    if (applicationData.hyperlipCmICD10 != null && applicationData.hyperlipCmMedList.Count > 0)
                    {
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hyperlipCmMedList, cn, cmd);
                    }
                    if (applicationData.hypertensionCmICD10 != null && applicationData.hypertensionCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hypertensionCmMedList, cn, cmd); 
                    }
                    if (applicationData.addisCmICD10 != null && applicationData.addisCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.addisCmMedList, cn, cmd); 
                    }
                    if (applicationData.bipolCmICD10 != null && applicationData.bipolCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.bipolCmMedList, cn, cmd); 
                    }
                    if (applicationData.bronchCmICD10 != null && applicationData.bronchCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.bronchCmMedList, cn, cmd); 
                    }
                    if (applicationData.cardiacCmICD10 != null && applicationData.cardiacCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.cardiacCmMedList, cn, cmd); 
                    }
                    if (applicationData.cardiomCmICD10 != null && applicationData.cardiomCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.cardiomCmMedList, cn, cmd); 
                    }
                    if (applicationData.copdCmICD10 != null && applicationData.copdCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.copdCmMedList, cn, cmd); 
                    }
                    if (applicationData.renalCmICD10 != null && applicationData.renalCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.renalCmMedList, cn, cmd); 
                    }
                    if (applicationData.coronarCmICD10 != null && applicationData.coronarCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.coronarCmMedList, cn, cmd); 
                    }
                    if (applicationData.crohnsCmICD10 != null && applicationData.crohnsCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.crohnsCmMedList, cn, cmd); 
                    }
                    if (applicationData.diabinsipCmICD10 != null && applicationData.diabinsipCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.diabinsipCmMedList, cn, cmd); 
                    }
                    if (applicationData.dysrhytCmICD10 != null && applicationData.dysrhytCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.dysrhytCmMedList, cn, cmd); 
                    }
                    if (applicationData.epilCmICD10 != null && applicationData.epilCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.epilCmMedList, cn, cmd); 
                    }
                    if (applicationData.glaucomCmICD10 != null && applicationData.glaucomCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.glaucomCmMedList, cn, cmd); 
                    }
                    if (applicationData.haemophilCmICD10 != null && applicationData.haemophilCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.haemophilCmMedList, cn, cmd); 
                    }
                    if (applicationData.hypothCmICD10 != null && applicationData.hypothCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.hypothCmMedList, cn, cmd); 
                    }
                    if (applicationData.sclerosisCmICD10 != null && applicationData.sclerosisCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.sclerosisCmMedList, cn, cmd); 
                    }
                    if (applicationData.parkinsCmICD10 != null && applicationData.parkinsCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.parkinsCmMedList, cn, cmd); 
                    }
                    if (applicationData.rheumaCmICD10 != null && applicationData.rheumaCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.rheumaCmMedList, cn, cmd); 
                    }
                    if (applicationData.schizoCmICD10 != null && applicationData.schizoCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.schizoCmMedList, cn, cmd); 
                    }
                    if (applicationData.lupusCmICD10 != null && applicationData.lupusCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.lupusCmMedList, cn, cmd); 
                    }
                    if (applicationData.ulceraCmICD10 != null && applicationData.ulceraCmMedList.Count > 0) 
                    { 
                        InsertMedication(ref sequence, conditionApplicationNumber, applicationData.ulceraCmMedList, cn, cmd); 
                    }

                    // Do File inserts
                    InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.referralLetterFiles, cn, cmd);
                    InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.medPrescriptionFiles, cn, cmd);
                    InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.additionalDocsFiles, cn, cmd);
                    if (condition == "DIAB")
                    {
                        InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.diabPathResultFiles, cn, cmd);
                    }
                    if (condition == "CHOL")
                    {
                        InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.hyperlipPathResultFiles, cn, cmd);
                    }
                    if (condition == "ASTHMA")
                    {
                        InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.asthmaPaedFiles, cn, cmd);
                        InsertFiles(conditionApplicationNumber, applicationData.provId, applicationData.membId, applicationData.asthmaFlowVolFiles, cn, cmd);
                    }

                    // Do Comorbid inserts                    
                    if (applicationData.asthmaCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "ASTHMA", applicationData.asthmaCmICD10, cn, cmd); 
                    }
                    if (applicationData.diabCmICD10 != null)
                    {
                        if (applicationData.diabetesType == "diabetesType1")
                        {
                            InsertComorbidInfo(conditionApplicationNumber, "DIAB1", applicationData.diabCmICD10, cn, cmd);
                        }
                        else if (applicationData.diabetesType == "diabetesType2")
                        {
                            InsertComorbidInfo(conditionApplicationNumber, "DIAB2", applicationData.diabCmICD10, cn, cmd);
                        }
                    }
                    if (applicationData.hyperlipCmICD10 != null)
                    {
                        InsertComorbidInfo(conditionApplicationNumber, "CHOL", applicationData.hyperlipCmICD10, cn, cmd);
                    }
                    if (applicationData.hypertensionCmICD10 != null)
                    {
                        InsertComorbidInfo(conditionApplicationNumber, "HYPER", applicationData.hypertensionCmICD10, cn, cmd);
                    }
                    if (applicationData.addisCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "ADDIS", applicationData.addisCmICD10, cn, cmd); 
                    }
                    if (applicationData.bipolCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "BIPOL", applicationData.bipolCmICD10, cn, cmd); 
                    }
                    if (applicationData.bronchCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "BRONCH", applicationData.bronchCmICD10, cn, cmd); 
                    }
                    if (applicationData.cardiacCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "CARDIAC", applicationData.cardiacCmICD10, cn, cmd); 
                    }
                    if (applicationData.cardiomCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "CARDIOM", applicationData.cardiomCmICD10, cn, cmd); 
                    }
                    if (applicationData.copdCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "COPD", applicationData.copdCmICD10, cn, cmd); 
                    }
                    if (applicationData.renalCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "RENAL", applicationData.renalCmICD10, cn, cmd); 
                    }
                    if (applicationData.coronarCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "CORONAR", applicationData.coronarCmICD10, cn, cmd); 
                    }
                    if (applicationData.crohnsCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "CROHNS", applicationData.crohnsCmICD10, cn, cmd); 
                    }
                    if (applicationData.diabinsipCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "DIABINSIP", applicationData.diabinsipCmICD10, cn, cmd); 
                    }
                    if (applicationData.dysrhytCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "DYSRHYT", applicationData.dysrhytCmICD10, cn, cmd); 
                    }
                    if (applicationData.epilCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "EPIL", applicationData.epilCmICD10, cn, cmd); 
                    }
                    if (applicationData.glaucomCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "GLAUCOM", applicationData.glaucomCmICD10, cn, cmd); 
                    }
                    if (applicationData.haemophilCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "HAEMOPHIL", applicationData.haemophilCmICD10, cn, cmd); 
                    }
                    if (applicationData.hypothCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "HYPOTH", applicationData.hypothCmICD10, cn, cmd); 
                    }
                    if (applicationData.sclerosisCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "SCLEROSIS", applicationData.sclerosisCmICD10, cn, cmd); 
                    }
                    if (applicationData.parkinsCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "PARKINS", applicationData.parkinsCmICD10, cn, cmd); 
                    }
                    if (applicationData.rheumaCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "RHEUMA", applicationData.rheumaCmICD10, cn, cmd); 
                    }
                    if (applicationData.schizoCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "SCHIZO", applicationData.schizoCmICD10, cn, cmd); 
                    }
                    if (applicationData.lupusCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "LUPUS", applicationData.lupusCmICD10, cn, cmd); 
                    }
                    if (applicationData.ulceraCmICD10 != null) 
                    { 
                        InsertComorbidInfo(conditionApplicationNumber, "ULCERA", applicationData.ulceraCmICD10, cn, cmd); 
                    }

                    // Send confirmation Email
                    string condCode = "";
                    if (condition == "DIAB")
                    {
                        if (applicationData.diabetesType == "diabetesType1") condCode = "DIAB1";
                        else if (applicationData.diabetesType == "diabetesType2") condCode = "DIAB2";
                        else condCode = condition;
                    }
                    else
                    {
                        condCode = condition;
                    }

                    // Prepare string for mail message
                    string appOrUpdate = "APPLICATION/UPDATE";
                    if (applicationData.currentPage == "ChronicCondApp")
                    {
                        appOrUpdate = "APPLICATION";
                    } 
                    else if (applicationData.currentPage == "ChronicCondUpdate")
                    {
                        appOrUpdate = "UPDATE";
                    }

                    // Prepare mail message
                    string subject = $"NEW CHRONIC CONDITION {appOrUpdate}: {conditionApplicationNumber}";
                    string body =   $"Good day, \r\n\r\n" +
                                    $"The following Chronic Condition {appOrUpdate.ToLower()} was captured via the web portal:\r\n\r\n" +
                                    $"CONDITION {appOrUpdate} NUMBER: {conditionApplicationNumber}.\r\n" +
                                    $"CONDITION CODE: {condCode}\r\n" +
                                    $"MEMBID: {applicationData.membId}\r\n" +
                                    $"PROVID: {applicationData.provId}\r\n\r\n" +
                                    $"Regards\r\n" +
                                    $"WEB PORTAL\r\n";
                    //string url = _EmailWebSvcConnection;
                    //EndpointAddress address = new EndpointAddress(url);
                    //BasicHttpBinding binding = new BasicHttpBinding();
                    //binding.MaxReceivedMessageSize = 2147483647;
                    //var time = new TimeSpan(0, 30, 0);
                    //binding.CloseTimeout = time;
                    //binding.OpenTimeout = time;
                    //binding.ReceiveTimeout = time;
                    //binding.SendTimeout = time;
                    //EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                    //var c = client.SendEmailAsync(msg, $"NEW CHRONIC CONDITION APPLICATION: {conditionApplicationNumber}", $"{DestinationEmail}");
                    //c.Wait();

                    // Get emailaddress to send to
                    cmd = new SqlCommand();
                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT description From {PamcPortalDatabase}.dbo.Settings where settingsId = 'ChronicConditionNotificationEmail' ";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    string to = dt.Rows[0]["description"].ToString();

                    // Get mailserver to send from
                    cmd = new SqlCommand();
                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = dt.Rows[0]["SERVERADDRESS"].ToString();
                    string username = dt.Rows[0]["USERNAME"].ToString();
                    string password = dt.Rows[0]["PASSWORD"].ToString();
                    string from = dt.Rows[0]["MAILFROM"].ToString();
                    string port = dt.Rows[0]["OUTGOINGPORT"].ToString();

                    List<FileInfo> attachments = new List<FileInfo>();

                    bool emailSuccess = false;

                    if (_isTest)
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PORTAL TEST: " + subject, body, _testEmails, attachments);
                    }
                    else
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, to, attachments);
                    }
                }

                cn.Close();
                cmd.Dispose();

                applicationData.success = true;
                applicationData.message = "";
            }
            catch (Exception e)
            {
                applicationData.success = false;
                applicationData.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return applicationData;
        }

        //[HttpPost]
        //[Route("SubmitBiometricUpdate")]
        //[Authorize(Roles = "NormalUser,Administrator")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public ChronicConditionApplicationModel SubmitBiometricUpdate([FromBody] ChronicConditionApplicationModel updateData)
        //{
        //    updateData.success = false;
        //    updateData.message = "";
        //    try
        //    {
        //        SqlConnection cn = new SqlConnection(_drcConnectionString);
        //        SqlCommand cmd = new SqlCommand();

        //        cmd = new SqlCommand();
        //        cmd.Connection = cn;

        //        //string conditionApplicationNumber = DateTime.Now.ToString("yyyyMMddHHmmssff");

        //        //cmd.Parameters.Add(new SqlParameter("@condAppNumber", conditionApplicationNumber));
        //        cmd.Parameters.Add(new SqlParameter("@membId", updateData.membId));
        //        cmd.Parameters.Add(new SqlParameter("@membEmail", updateData.membEmail));
        //        cmd.Parameters.Add(new SqlParameter("@provId", updateData.provId));
        //        cmd.Parameters.Add(new SqlParameter("@provEmail", updateData.provEmail));
        //        //cmd.Parameters.Add(new SqlParameter("@applicationDate", DateTime.Now));
        //        //cmd.Parameters.Add(new SqlParameter("@status", applicationData.status)); // P
        //        cmd.Parameters.Add(new SqlParameter("@weight", updateData.weight));
        //        cmd.Parameters.Add(new SqlParameter("@height", updateData.height));
        //        cmd.Parameters.Add(new SqlParameter("@bmi", updateData.bmi));
        //        cmd.Parameters.Add(new SqlParameter("@bloodPresSystolic", updateData.bloodPresSystolic));
        //        cmd.Parameters.Add(new SqlParameter("@bloodPresDiastolic", updateData.bloodPresDiastolic));
        //        if (updateData.diabetesType == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
        //            cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
        //        }
        //        else if (updateData.diabetesType == "diabetesType1")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@type1Diab", "Y"));
        //            cmd.Parameters.Add(new SqlParameter("@type2Diab", DBNull.Value));
        //        }
        //        else if (updateData.diabetesType == "diabetesType2")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@type1Diab", DBNull.Value));
        //            cmd.Parameters.Add(new SqlParameter("@type2Diab", "Y"));
        //        }
        //        if (updateData.hba1c == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@HbA1c", DBNull.Value));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@HbA1c", updateData.hba1c));
        //        }
        //        if (updateData.fastingOrRandom == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
        //        }
        //        else if (updateData.fastingOrRandom == "fasting")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", "Y"));
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", DBNull.Value));
        //        }
        //        else if (updateData.fastingOrRandom == "random")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2FAST", DBNull.Value));
        //            cmd.Parameters.Add(new SqlParameter("@DIABTYPE2RANDOM", "Y"));
        //        }
        //        if (updateData.diabTestDate == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@diabTestDate", DBNull.Value));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@diabTestDate", updateData.diabTestDate));
        //        }
        //        cmd.Parameters.Add(new SqlParameter("@cholesterolHDL", updateData.cholesterolHDL));
        //        cmd.Parameters.Add(new SqlParameter("@cholesterolLDL", updateData.cholesterolLDL));
        //        cmd.Parameters.Add(new SqlParameter("@cholesterolTRI", updateData.cholesterolTRI));
        //        cmd.Parameters.Add(new SqlParameter("@cholesterolTotal", updateData.cholesterolTotal));
        //        cmd.Parameters.Add(new SqlParameter("@alcohol", updateData.alcohol));
        //        cmd.Parameters.Add(new SqlParameter("@obesity", updateData.obesity));
        //        cmd.Parameters.Add(new SqlParameter("@smoker", updateData.smoker));
        //        if (updateData.smokeInPast == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@smokeInPast", DBNull.Value));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@smokeInPast", updateData.smokeInPast));
        //        }
        //        if (updateData.cessationDate == null)
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@cessationDate", DBNull.Value));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@cessationDate", updateData.cessationDate));
        //        }
        //        if (updateData.alcohol == "Y" || updateData.obesity == "Y" || updateData.smoker == "Y")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "Y"));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@predisposingRiskFactors", "N"));
        //        }
        //        cmd.Parameters.Add(new SqlParameter("@ischaemicHeartDisease", updateData.ischaemicHeartDisease));
        //        cmd.Parameters.Add(new SqlParameter("@peripheralVascularDisease", updateData.peripheralVascularDisease));
        //        cmd.Parameters.Add(new SqlParameter("@strokeAttacks", updateData.strokeAttacks));
        //        if (updateData.ischaemicHeartDisease == "Y" || updateData.peripheralVascularDisease == "Y" || updateData.strokeAttacks == "Y")
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "Y"));
        //        }
        //        else
        //        {
        //            cmd.Parameters.Add(new SqlParameter("@preExistingComplications", "N"));
        //        }

        //        // Columns and values for the fields that is inserted with every application
        //        string sqlColumns =
        //           //"  [CONDAPPNO] \n" +
        //             "  [MEMBID]  \n" +
        //           //" ,[MEMBEMAIL]  \n" +
        //             " ,[PROVID]  \n" +
        //             " ,[TESTDATE]  \n" +
        //             " ,[CURRHIST] \n" +
        //           //" ,[PROVEMAIL]  \n" +                     
        //           //" ,[STATUS]  \n" +
        //             " ,[WGHT]  \n" +
        //             " ,[HEIGHT]  \n" +
        //             " ,[BMI]  \n" +
        //             " ,[BPSYSTOLIC] \n" +
        //             " ,[BPDIASTOLIC] \n" +
        //             " ,[DIABTYPE1] \n" +
        //             " ,[DIABTYPE2] \n" +
        //             " ,[HBA1C]  \n" +
        //             " ,[DIABTYPE2FAST] \n" +
        //             " ,[DIABTYPE2RANDOM] \n" +
        //             " ,[DIABTESTDATE] \n" +
        //             " ,[HDL] \n" +
        //             " ,[LDL] \n" +
        //             " ,[TRIGLYCERIDES] \n" +
        //             " ,[CHOLESTEROL] \n" +
        //             //" ,[PREDRISK]  \n" +
        //             //" ,[ALCOHOL]  \n" +
        //             //" ,[OBESITY]  \n" +
        //             //" ,[SMOKER]  \n" +
        //             //" ,[PASTSMOKE] \n" +
        //             //" ,[SMOKEENDDATE] \n" +
        //             //" ,[PRECOMP]  \n" +
        //             //" ,[HEARTDISEASE] \n" +
        //             //" ,[VASCDISEASE] \n" +
        //             //" ,[TIA]  \n" +
        //             //" ,[MAILSENT] \n" +
        //             " ,[CREATEBY]  \n" +
        //             " ,[CREATEDATE]  \n" +
        //             " ,[LASTCHANGEBY]  \n" +
        //             " ,[LASTCHANGEDATE]  \n";

        //        string sqlValues =
        //          //"  @condAppNumber  \n " +
        //            "  @membId  \n " +
        //          //" ,@membEmail  \n " +
        //            " ,@provId  \n " +
        //            " ,GETDATE()  \n" + // TESTDATE
        //            " ,'C'  \n" + // CURRHIST
        //          //" ,@provEmail  \n " +                   
        //          //" ,'P'  \n" +
        //            " ,@weight  \n" +
        //            " ,@height  \n" +
        //            " ,@bmi  \n" +
        //            " ,@bloodPresSystolic \n " +
        //            " ,@bloodPresDiastolic \n " +
        //            " ,@type1Diab \n " +
        //            " ,@type2Diab \n " +
        //            " ,@HbA1c \n" +
        //            " ,@DIABTYPE2FAST " +
        //            " ,@DIABTYPE2RANDOM " +
        //            " ,@diabTestDate  \n" +
        //            " ,@cholesterolHDL \n" +
        //            " ,@cholesterolLDL \n" +
        //            " ,@cholesterolTRI \n" +
        //            " ,@cholesterolTotal \n" +
        //            //" ,@predisposingRiskFactors  \n" +
        //            //" ,@alcohol  \n" +
        //            //" ,@obesity  \n" +
        //            //" ,@smoker  \n" +
        //            //" ,@smokeInPast  \n" +
        //            //" ,@cessationDate  \n" +
        //            //" ,@preExistingComplications  \n" +
        //            //" ,@ischaemicHeartDisease  \n" +
        //            //" ,@peripheralVascularDisease  \n" +
        //            //" ,@strokeAttacks  \n" +
        //            //" ,0 \n" + //mailsent
        //            " ,555  \n" +
        //            " ,GETDATE()  \n" +
        //            " ,555  \n" +
        //            " ,GETDATE()  \n";

        //        // Do CONDITION_APPLICATION insert
        //        cmd.CommandText =
        //             $" INSERT INTO {DRCDatabase}.dbo.MEMB_BIOMETRICHIST ( " + sqlColumns + " ) \n " +
        //              "  OUTPUT INSERTED.ROWID " +                     
        //              " VALUES ( " + sqlValues + " ) ";

        //        if (cn.State != ConnectionState.Open) cn.Open();
        //        int rowid = Convert.ToInt32(cmd.ExecuteScalar().ToString());

        //        // Set other biometri updates to History (H)
        //        cmd = new SqlCommand();
        //        cmd.Connection = cn;
        //        cmd.Parameters.Add(new SqlParameter("@membId", updateData.membId));
        //        cmd.Parameters.Add(new SqlParameter("@rowid", rowid));
        //        cmd.CommandText =
        //            $" UPDATE {DRCDatabase}.dbo.MEMB_BIOMETRICHIST " +
        //            $" SET CURRHIST = 'H' " +
        //            $" WHERE MEMBID = @membId and ROWID <> @rowid ";                
        //        if (cn.State != ConnectionState.Open) cn.Open();
        //        cmd.ExecuteNonQuery();

        //        cn.Close();
        //        cmd.Dispose();

        //        updateData.success = true;
        //        updateData.message = "";
        //    }
        //    catch (Exception e)
        //    {
        //        updateData.success = false;
        //        updateData.message = e.Message;

        //        LogError(e.Message, e.StackTrace);
        //    }

        //    return updateData;
        //}

        private void InsertMedication(ref int sequence, string conditionApplicationNumber, List<MedicationModel> medList, SqlConnection cn, SqlCommand cmd)
        {
            foreach (MedicationModel medItem in medList)
            {
                sequence++;
                cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@condAppNumber", conditionApplicationNumber));
                cmd.Parameters.Add(new SqlParameter("@sequence", sequence));
                cmd.Parameters.Add(new SqlParameter("@icd10", medItem.icd10));
                if (medItem.prescriptionSvcCode == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@prescriptionSvcCode", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@prescriptionSvcCode", medItem.prescriptionSvcCode));
                }
                cmd.Parameters.Add(new SqlParameter("@svcCodeDescr", medItem.svcCodeDescr));
                cmd.Parameters.Add(new SqlParameter("@strength", medItem.strength));
                cmd.Parameters.Add(new SqlParameter("@phcode", "M"));
                cmd.Parameters.Add(new SqlParameter("@dosage", medItem.dosage));
                cmd.Parameters.Add(new SqlParameter("@qtyPerMonth", medItem.qtyPerMonth));
                cmd.Parameters.Add(new SqlParameter("@numRepeats", medItem.numRepeats));
                if (medItem.historyMed)
                {
                    cmd.Parameters.Add(new SqlParameter("@historyMed", "Y"));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@historyMed", "N"));
                }
                if (medItem.comorbid)
                {
                    cmd.Parameters.Add(new SqlParameter("@comorbid", "Y"));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@comorbid", "N"));
                }
                //if (medItem.condCode == null)
                //{
                //    cmd.Parameters.Add(new SqlParameter("@condCode", DBNull.Value));
                //}
                //else
                //{
                //    cmd.Parameters.Add(new SqlParameter("@condCode", medItem.condCode));
                //}
                cmd.Parameters.Add(new SqlParameter("@section", medItem.section));

                cmd.CommandText =
                 $" INSERT INTO {DRCDatabase}.dbo.CONDITION_MEDICATION ( " +
                  "  [CONDAPPNO] \n" +
                  " ,[SEQ] \n " +
                  " ,[DIAGCODE] \n" +
                  " ,[SVCCODE] \n" +
                  " ,[SVCDESCR] \n" +
                  " ,[STRENGTH] \n" +
                  " ,[PHCODE] \n" +
                  " ,[DOSAGE] \n" +
                  " ,[QTY] \n" +
                  " ,[NOREPEAT] \n" +
                  " ,[HISTORY] \n " +
                  " ,[COMORBID] \n " +
                  " ,[SECTION] \n " +
                  " ,[CREATEBY] \n " +
                  " ,[CREATEDATE] \n" +
                  " ,[LASTCHANGEBY] \n" +
                  " ,[LASTCHANGEDATE]) \n" +
                  " VALUES ( " +
                  "  @condAppNumber \n" +
                  " ,@sequence \n" +
                  " ,@icd10 \n" +
                  " ,@prescriptionSvcCode \n" +
                  " ,@svcCodeDescr \n" +
                  " ,@strength \n" +
                  " ,@phcode " +
                  " ,@dosage \n" +
                  " ,@qtyPerMonth \n" +
                  " ,@numRepeats \n" +
                  " ,@historyMed \n" +
                  " ,@comorbid \n" +
                  " ,@section \n" +
                  " ,555 \n " +
                  " ,GETDATE() \n" +
                  " ,555 \n " +
                  " ,GETDATE() ) \n";

                if (cn.State != ConnectionState.Open) cn.Open();
                cmd.ExecuteScalar();
            }
        }

        private void InsertFiles(string conditionApplicationNumber, string provId, string membId, List<FileItemModel> fileList, SqlConnection cn, SqlCommand cmd)
        {
            foreach (FileItemModel fileItem in fileList)
            {
                // Insert into CPS_FILES

                cmd = new SqlCommand();
                cmd.Connection = cn;

                byte[] file = Convert.FromBase64String(fileItem.base64Str);
                //string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

                cmd.Parameters.Add(new SqlParameter("@id", Guid.NewGuid()));
                cmd.Parameters.Add(new SqlParameter("@fileName", fileItem.name));
                cmd.Parameters.Add(new SqlParameter("@docBinary", file));


                cmd.CommandText =
                    $" INSERT INTO {DRCDatabase}.dbo.CPS_FILES ( " +
                     "  [ID] \n " +
                     " ,[FILENAME] \n " +
                     " ,[DOCBINARY] \n " +
                     " ,[CREATEBY] \n " +
                     " ,[CREATEDATE] \n " +
                     " ,[LASTCHANGEBY] \n " +
                     " ,[LASTCHANGEDATE]) \n " +
                     " OUTPUT INSERTED.ID \n " +
                     " VALUES ( @id, @fileName, @docBinary, 555, GETDATE(), 555, GETDATE() ) ";

                if (cn.State != ConnectionState.Open) cn.Open();

                string fileGuid = cmd.ExecuteScalar().ToString();

                // Insert into CONDITION_FILES
                cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@conditionApplicationNumber", conditionApplicationNumber));
                cmd.Parameters.Add(new SqlParameter("@id", fileGuid));
                cmd.Parameters.Add(new SqlParameter("@fileDesc", fileItem.fileDesc));
                cmd.Parameters.Add(new SqlParameter("@fileType", fileItem.type));

                cmd.CommandText =
                    $" INSERT INTO {DRCDatabase}.dbo.CONDITION_FILES ( " +
                     "  [CONDAPPNO] \n " +
                     " ,[ID] \n " +
                     " ,[FILETYPE] \n " +
                     " ,[FILEDESC] \n " +
                     " ,[CREATEBY] \n " +
                     " ,[CREATEDATE] \n " +
                     " ,[LASTCHANGEBY] \n " +
                     " ,[LASTCHANGEDATE]) \n " +
                     " VALUES ( @conditionApplicationNumber, @id, @fileType, @fileDesc, 555, GETDATE(), 555, GETDATE() ) ";

                if (cn.State != ConnectionState.Open) cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        private void InsertComorbidInfo(string conditionApplicationNumber, string condCode, string icd10, SqlConnection cn, SqlCommand cmd)
        {
            cmd = new SqlCommand();
            cmd.Connection = cn;

            cmd.Parameters.Add(new SqlParameter("@condAppNo", conditionApplicationNumber));
            cmd.Parameters.Add(new SqlParameter("@condCode", condCode));
            cmd.Parameters.Add(new SqlParameter("@icd10", icd10));

            cmd.CommandText =
                $" INSERT INTO {DRCDatabase}.dbo.CONDITION_COMORBIDS ( " +
                 "  [CONDAPPNO] \n " +
                 " ,[COMORBIDCONDCODE] \n " +
                 " ,[COMORBIDDIAGCODE] \n " +
                 " ,[CREATEBY] \n " +
                 " ,[CREATEDATE] \n " +
                 " ,[LASTCHANGEBY] \n " +
                 " ,[LASTCHANGEDATE]) \n " +
                 //" OUTPUT INSERTED.ID \n " +
                 " VALUES ( @condAppNo, @condCode, @icd10, 555, GETDATE(), 555, GETDATE() ) ";

            if (cn.State != ConnectionState.Open) cn.Open();

            cmd.ExecuteNonQuery();
        }

        /*Method used to log all errors that take place in this controller*/
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string controler = "CHRONICCONDITION";
            var root = _env.WebRootPath;

            string fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\CHRONICCONDITION\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}

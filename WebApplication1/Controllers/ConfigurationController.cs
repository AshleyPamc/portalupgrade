﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationController : NEWPAMCPORTAL.Controllers.Base.PAMCController
    {
        public ConfigurationController(IWebHostEnvironment env, IConfiguration con):base(env,con)
        {

        }

        //Loading all the settings
        [HttpGet]
        [Route("getSettings")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<SettingsModel> GetSettings()
        {
            DataTable dt = new DataTable();

            List<SettingsModel> settingsList = new List<SettingsModel>();


            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "  select * from settings order by createDate desc";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;


                        dt.Load(cmd.ExecuteReader());


                        foreach (DataRow dr in dt.Rows)
                        {
                            SettingsModel s = new SettingsModel();
                            s.settingsId = dr["settingsId"].ToString();
                            s.description = dr["description"].ToString();
                            s.required = Convert.ToBoolean(dr["required"]);
                            s.enabled = Convert.ToBoolean(dr["enabled"]);
                            s.createBy = dr["createBy"].ToString();
                            s.CreateDate = Convert.ToDateTime(dr["createDate"]);
                            s.ChangeBy = dr["changeBy"].ToString();
                            s.ChangeDate = Convert.ToDateTime(dr["changeDate"]);
                            if (dr["provider"] != DBNull.Value)
                            {
                                s.provider = Convert.ToBoolean(dr["provider"]);
                            }
                            if (dr["administrator"] != DBNull.Value)
                            {
                                s.administrator = Convert.ToBoolean(dr["administrator"]);
                            }
                            if (dr["providerRequired"] != DBNull.Value)
                            {
                                s.providerRequired = Convert.ToBoolean(dr["providerRequired"]);
                            }
                            if (dr["administratorRequired"] != DBNull.Value)
                            {
                                s.administratorRequired = Convert.ToBoolean(dr["administratorRequired"]);
                            }
                            settingsList.Add(s);
                        }


                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return settingsList;
        }

        [HttpGet]
        [Route("getProvExeptionSettings")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExeptionSettings> getProvExeptionSettings()
        {
            DataTable dt = new DataTable();

            List<ExeptionSettings> settingsList = new List<ExeptionSettings>();


            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "  select OPTNAME,SPECS from [PROVIDER_EXCEPTION_SETTINGS] order by createDate desc";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;


                        dt.Load(cmd.ExecuteReader());


                        foreach (DataRow dr in dt.Rows)
                        {
                            ExeptionSettings s = new ExeptionSettings();
                            s.opt = dr["OPTNAME"].ToString();
                            s.specs = dr["SPECS"].ToString();
                            settingsList.Add(s);
                        }


                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return settingsList;
        }

        //update the settings
        [HttpPost]
        [Route("UpdateSettings")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public SettingsModel UpdateSettings([FromBody] SettingsModel model)
        {
            if (model.enabled == false)
            {
                model.required = false;
            }
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        model.ChangeDate = DateTime.Now;
                        cmd.CommandType = CommandType.Text;

                        if ((model.settingsId == null) || (model.settingsId == string.Empty) || (model.description == string.Empty) || (model.description == null) || (model.ChangeBy == null))
                        {
                            throw new Exception();
                        }
                        string sqlTextToExecute = "update Settings set settingsId = '" + model.settingsId + "',[description] = '" + model.description + "'" + ",[required] = '" + model.required + "'" + ",[enabled] = '" + model.enabled + "'" + ",changeBy = '" + model.ChangeBy + "'" + ",changeDate = '" + model.ChangeDate + "'" +
                                                  " where settingsId = '" + model.origanalSettingsId + "'";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        try
                        {

                            int rowsAffected = cmd.ExecuteNonQuery();
                            if (rowsAffected != 1)
                            {
                                //throw new Exception();
                            }

                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            throw new Exception();

                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return model;
        }

        //Gets All the Manuals 
        [HttpGet]
        [Route("GetManuals")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ManualsModel> GetManuals()
        {
            DataTable dt = new DataTable();
            List<ManualsModel> listOfManuals = new List<ManualsModel>();
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select * from Downloads";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            ManualsModel manual = new ManualsModel();
                            manual.fileName = dr["fileName"].ToString();
                            manual.fileLink = dr["fileLink"].ToString();
                            listOfManuals.Add(manual);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return listOfManuals;
        }


        public static void LogError(string error, string stacktrace)
        {

            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "CONFIG";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\CONFIG\\{fileName}");
            //    fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception e)
            {


            }


        }

    }
}
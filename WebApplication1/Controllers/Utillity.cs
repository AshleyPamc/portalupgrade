﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Controllers
{
    public class Utillity
    {     
        /*Returns details of a specific setting*/
        public static SettingsModel GetSpecificSetting(string settingsId, string con)
        {
            DataTable dt = new DataTable();

            SettingsModel specificsetting = new SettingsModel();


            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "SELECT * from settings " +
                                              " WHERE settingsId = @settingsId";
                    cmd.Parameters.Add("@settingsId", SqlDbType.VarChar, 100).Value = settingsId;
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;


                    dt.Load(cmd.ExecuteReader());
                    if (dt.Rows.Count == 0)
                    {
                        throw new Exception($"Setting {settingsId} not found");
                    }

                    //dr.Rows.Count is definitel = 1
                    DataRow dr = dt.Rows[0];
                    specificsetting.settingsId = dr["settingsId"].ToString();
                    specificsetting.description = dr["description"].ToString();
                    specificsetting.enabled = Convert.ToBoolean(dr["enabled"]);
                    specificsetting.createBy = dr["createBy"].ToString();
                    specificsetting.CreateDate = Convert.ToDateTime(dr["createDate"]);
                    specificsetting.ChangeBy = dr["changeBy"].ToString();
                    specificsetting.ChangeDate = Convert.ToDateTime(dr["changeDate"]);
                }
                Sqlcon.Close();
            }
            return specificsetting;
        }

        /*Returns details of a depnedant*/
        public static List<MemberInfoModel> Dependantdetails(string familynum, string con, string DRCDatabase, string PamcPortalDatabase, string ReportingDatabase)
        {
            DataTable dt = new DataTable();
            List<MemberInfoModel> memberModelList = new List<MemberInfoModel>();

            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;

                    string sqlTextToExecute = "SELECT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.MI, MEMBERSHIP.FIRSTNM, MEMBERSHIP.HPFROMDT,HealthPlanSetup.HPName AS 'HEALTH_PLAN', MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \r\n" +
                                              " MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, MEMBERSHIP.SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \r\n" +
                                              " MEMBERSHIP.MemberStatus, MEMBERSHIP.PATID\r\n" +
                                              "from   (" +
                                             $"           SELECT        mm.MEMBID, mm.RLSHIP, mm.LASTNM, mm.MI, mm.FIRSTNM, mm.EMAIL, mh.HPFROMDT, mh.HPCODE, ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, mh.OPFROMDT, CASE WHEN isnull(OPTHRUDT, GETDATE())  \n " +
                                             $"                         >= GETDATE() THEN NULL ELSE OPTHRUDT END AS OPTHRUDT, mm.BIRTH, mm.SUBSSN, ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1)  \n " +
                                             $"                         AS DEPENDANT, mh.OPT AS HPName, {DRCDatabase}..HP_CONTRACTS.LOBCODE, mh.TERM_CODE, CASE WHEN mh.CURRHIST = 'c' AND isnull(OPTHRUDT, GETDATE()) >= GETDATE() \n " +
                                             $"                          THEN 'ACTIVE' ELSE 'TERMINATED' END AS MemberStatus, mm.PATID, mm.BROKERID, memberprefix AS LenOfPrefix, 0 AS PosOdDash \n " +
                                             $"           FROM {DRCDatabase}..MEMB_MASTERS AS mm INNER JOIN \n " +
                                             $"                {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID INNER JOIN \n " +
                                             $"                {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = {DRCDatabase}..HP_CONTRACTS.HPCODE LEFT OUTER JOIN \n " +
                                             $"                  (SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix \n " +
                                             $"                   FROM            {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n " +
                                             $"                             {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n " +
                                             $"                   WHERE({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') \n " +
                                             $"                  ) AS z ON {DRCDatabase}..HP_CONTRACTS.HPCODE = z.ezHpCode LEFT OUTER JOIN \n " +
                                             $"                {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName LEFT OUTER JOIN \n " +
                                             $"                {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                                             $"           WHERE(mh.CURRHIST = 'c') \n " +
                                             $"       ) AS MEMBERSHIP INNER JOIN\r\n" +
                                             $" {PamcPortalDatabase}..HealthPlanSetup ON MEMBERSHIP.HPCODE = HealthPlanSetup.HPCode\r\n" +
                                              "where MEMBERSHIP.SUBSSN ='" + familynum + "'\r\n";

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        MemberInfoModel memberModel = new MemberInfoModel();

                        memberModel.familynumber = dr["SUBSSN"].ToString();
                        memberModel.firstname = dr["FIRSTNM"].ToString();
                        memberModel.lastname = dr["LASTNM"].ToString();
                        memberModel.healthplan = dr["HEALTH_PLAN"].ToString();
                        memberModel.option = dr["HPName"].ToString();
                        memberModel.relation = dr["RELATION"].ToString();
                        memberModel.dependant = dr["DEPENDANT"].ToString();
                        memberModel.status = dr["MemberStatus"].ToString();

                        memberModelList.Add(memberModel);
                    }

                }
                Sqlcon.Close();
            }

            return memberModelList;
        }

        /* Email notification that will be sent upon successful Registeration PS email has been hardcoded to be sent to the following address - montie2206@gmail.com */
        public static void Sendnotification(string subject, string Body, string toaddress)
        {
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress("Servicerequest@pamc.co.za", "Portal notification");
            msg.To.Add(toaddress);

            msg.Subject = subject;
            msg.Body = Body;
            msg.IsBodyHtml = false;

            SmtpClient client = new SmtpClient();
            client.Host = "192.168.16.21";
            client.Port = 587;// 25;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("Servicerequest", "Serv12#$");
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            client.Send(msg);

            msg.Dispose();
        }

        /*Store procedure that  compares the health plans from the HP_Contracts table on the DRC database with the HealthPlanSetup table on the PAMCPortal database*/
        public static void CompareHealthPlans(string con, string DRCDatabase, string PamcPortalDatabase)
        {
            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    //string sqlTextToExecute = "EXECUTE HP_Comparison"; // Jaco 2023-09-08
                    string sqlTextToExecute =  " Create Table #tempHP_table " +
                                               "  (    " +
                                               "      HPCODE Varchar(4) NOT NULL, " +
                                               "      HPNAME Varchar(30) NULL, " +
                                               "      HPDATE DATETIME NULL " +
                                               "  )  " +

                                               " INSERT INTO #tempHP_table (HPCODE, HPNAME,HPDATE)  " +
                                               " SELECT HP_Contracts.HPCODE, HP_Contracts.HPNAME, DTE.HPDATE  " +
                                              $" FROM           {DRCDatabase}..HP_Contracts LEFT OUTER JOIN  " +
                                              $" {PamcPortalDatabase}..HealthPlanSetup ON HealthPlanSetup.HPCode = HP_Contracts.HPCODE  " +
                                               "                             AND HealthPlanSetup.HPName = HP_Contracts.HPName  " +
                                               " LEFT OUTER JOIN(SELECT HPCODE, ISNULL(HPDATE, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, -1)) AS HPDATE FROM  " +
                                              $" (select hpcode, max(todate) AS HPDATE from {DRCDatabase}..ben_options  " +
                                               " group by hpcode) DT  " +
                                               " ) DTE ON DTE.HPCODE = HP_Contracts.HPCODE  " +
                                               " WHERE(HealthPlanSetup.HPCode IS NULL OR HealthPlanSetup.HPName <> HP_Contracts.HPNAME OR DTE.HPDATE <> isnull(HealthPlanSetup.HPDATE, '2000/01/01'))  " +
                                               " AND DTE.HPDATE IS NOT NULL  " +

                                               " --SELECT DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, -1)  " +
                                               " DECLARE @HPCODE varchar(4)  " +
                                               " DECLARE @HPNAME Varchar(30)  " +
                                               " DECLARE @HPDATE DATETIME  " +

                                               "    WHILE EXISTS(select  HPCODE from #tempHP_table )            " +
                                               "                     begin  " +
                                               "                                     SELECT top 1 @HPCODE = HPCODE, @HPNAME = HPNAME, @HPDATE = HPDATE from #tempHP_table  " +
                                               "                                     print @HPCODE " +

                                               "                                         print @HPNAME " +

                                              $"                                    IF EXISTS(select HPCODE from {PamcPortalDatabase}..HealthPlanSetup WHERE HPCODE = @HPCODE) " +

                                               "                                         BEGIN " +
                                               "                                         print 'update' " +
                                               "                                             print @HPCODE " +
                                              $"                                            UPDATE {PamcPortalDatabase}..HealthPlanSetup " +
                                               "                                             SET  HPName = @HPNAME, HPDATE = @HPDATE " +
                                               "                                             WHERE HPCode = @HPCODE " +
                                               "                                         END  " +

                                               "                                     ELSE " +

                                               "                                         BEGIN " +
                                               "                                         print 'Insert' " +
                                              $"                                             INSERT INTO {PamcPortalDatabase}..HealthPlanSetup " +
                                               "                                                 (HPCode, HPName, isEnabledClaim, isEnabledMember, HPDATE) " +
                                               "                                                 VALUES(@HPCODE, @HPNAME, 'true', 'true', @HPDATE) " +
                                               "                                         END " +
                                               "                                 delete from #tempHP_table where HPCODE = @HPCODE " +
                                               "                     end " +

                                               " DROP TABLE #tempHP_table " +

                                               " UPDATE HS SET LOBCODE = HP.LOBCODE " +
                                               " --SELECT HS.*, HP.LOBCODE " +
                                              $" FROM {PamcPortalDatabase}..HealthPlanSetup HS INNER JOIN {DRCDatabase}..[HP_Contracts] HP ON HP.HPCODE = HS.HPCode " +

                                              $" Update {PamcPortalDatabase}..HealthPlanSetup " +
                                               " SET HPDATE = DT.HPDATE " +
                                              $" FROM  {PamcPortalDatabase}..HealthPlanSetup INNER JOIN " +
                                               "                                 (SELECT        HPCODE, MAX(FROMDATE) AS HPDATE " +
                                              $"                                 FROM            {DRCDatabase}..BEN_OPTIONS " +
                                               "                                 GROUP BY HPCODE) AS DT ON HealthPlanSetup.HPCode = DT.HPCODE  ";

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    try
                    {

                        int rowsAffected = cmd.ExecuteNonQuery();
                        if (rowsAffected != 1)
                        {
                            //throw new Exception();
                        }

                    }
                    catch (Exception ex)
                    {
                        var msg = ex.Message;
                        throw;


                    }
                }
                Sqlcon.Close();
            }
        }

        /*Return details of LOB codes*/
        public static List<string> LobDesc(string lobcode, string con)
        {
            DataTable dt = new DataTable();
            List<string> lobDescList = new List<string>();


            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (lobcode.Length > 1)
                    {
                        StringBuilder sb = new StringBuilder();
                        string[] splitLobDesc;

                        splitLobDesc = lobcode.Split(',');
                        foreach (var code in splitLobDesc)
                        {
                            string buildString = "'" + code + "'" + ",";
                            sb.Append(buildString);
                        }
                        lobcode = sb.ToString();
                        lobcode = lobcode.Remove(lobcode.Length - 1);

                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select CODE,DESCR from [LOB_CODES]\n"
                                                    + "where CODE in (" + lobcode + ")";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            string lobDesc = dr["DESCR"].ToString();
                            lobDescList.Add(lobDesc);
                        }
                    }
                    else
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select CODE,DESCR from [LOB_CODES]\n"
                                                    + "where CODE = '" + lobcode + "'";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        DataRow dr = dt.Rows[0];
                        string lobDesc = dr["DESCR"].ToString();
                        lobDescList.Add(lobDesc);
                    }



                }
                Sqlcon.Close();
            }
            return lobDescList;
        }

        /*Returns member details*/
        public static Tuple<string, string> membername(string IDnum, string con, string DRCDatabase, string PamcPortalDatabase, string ReportingDatabase)
        {
            DataTable dt = new DataTable();
            string memberLastname = "";
            string middleIntial = "";
            string membername = "";
            string fullName = "";

            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (IDnum == "")
                    {
                        membername = "No Member Found";
                        fullName = "No Member Found";
                    }
                    else
                    {
                        cmd.CommandType = CommandType.Text;

                        // Jaco comment out 2023-09-20 Databases hardcoded in MEMBERSHIP view
                        //string sqlTextToExecute = "select * from MEMBERSHIP\r\n" +
                        //                         "where PATID = '" + IDnum + "'\r\n";

                        // Jaco 2023-09-20
                        string sqlTextToExecute = "select * from   (" +
                                                             $"           SELECT        mm.MEMBID, mm.RLSHIP, mm.LASTNM, mm.MI, mm.FIRSTNM, mm.EMAIL, mh.HPFROMDT, mh.HPCODE, ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, mh.OPFROMDT, CASE WHEN isnull(OPTHRUDT, GETDATE())  \n " +
                                                             $"                         >= GETDATE() THEN NULL ELSE OPTHRUDT END AS OPTHRUDT, mm.BIRTH, mm.SUBSSN, ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1)  \n " +
                                                             $"                         AS DEPENDANT, mh.OPT AS HPName, {DRCDatabase}..HP_CONTRACTS.LOBCODE, mh.TERM_CODE, CASE WHEN mh.CURRHIST = 'c' AND isnull(OPTHRUDT, GETDATE()) >= GETDATE() \n " +
                                                             $"                          THEN 'ACTIVE' ELSE 'TERMINATED' END AS MemberStatus, mm.PATID, mm.BROKERID, memberprefix AS LenOfPrefix, 0 AS PosOdDash \n " +
                                                             $"           FROM {DRCDatabase}..MEMB_MASTERS AS mm INNER JOIN \n " +
                                                             $"                {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID INNER JOIN \n " +
                                                             $"                {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = {DRCDatabase}..HP_CONTRACTS.HPCODE LEFT OUTER JOIN \n " +
                                                             $"                  (SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix \n " +
                                                             $"                   FROM            {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n " +
                                                             $"                             {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n " +
                                                             $"                   WHERE({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') \n " +
                                                             $"                  ) AS z ON {DRCDatabase}..HP_CONTRACTS.HPCODE = z.ezHpCode LEFT OUTER JOIN \n " +
                                                             $"                {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName LEFT OUTER JOIN \n " +
                                                             $"                {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                                                             $"           WHERE(mh.CURRHIST = 'c') \n " +
                                                             $"       ) AS MEMBERSHIP \r\n" +
                                                 "where PATID = '" + IDnum + "'\r\n";

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());
                        if (dt.Rows.Count == 0)
                        {
                            membername = "No Member Found";
                        }
                        else
                        {
                            DataRow dr = dt.Rows[0];
                            membername = dr["FIRSTNM"].ToString();
                            memberLastname = dr["LASTNM"].ToString();
                            middleIntial = dr["MI"].ToString();

                            fullName = membername + " " + memberLastname;
                        }
                    }
                }
                Sqlcon.Close();
            }
            return Tuple.Create(membername, fullName);
        }

        /*Returns broker details*/
        public static string brokerName(string brokerId, string hpCode, string con)
        {
            DataTable dt = new DataTable();
            string FoundUser = "";
            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "select * from BROKER_MASTERS\r\n" +
                                              "where HPCODE = @hpCode and BROKERID = @brokerId\r\n";
                    cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = brokerId;
                    cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = hpCode;
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count == 0)
                    {
                        FoundUser = "";
                    }
                    else
                    {
                        DataRow dr = dt.Rows[0];
                        FoundUser = dr["NAME"].ToString();
                    }

                }
                Sqlcon.Close();
            }
            return FoundUser;
        }

        /*Gets the name of the Registered Bureau on our system*/
        public static BureauModel GetBureauId(string Name, string con)
        {
            BureauModel BureauInformation = new BureauModel();
            DataTable dt = new DataTable();

            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "SELECT [BUREAUID],[NAME] FROM [BUREAU_MASTERS]\r\n" +
                                              "WHERE [NAME] = '" + Name + "'\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        BureauInformation.Name = dr["NAME"].ToString();
                        BureauInformation.BureauID = Convert.ToInt32(dr["BUREAUID"]);
                    }
                }
                Sqlcon.Close();
            }
            return BureauInformation;
        }

        /*Returns provider details*/
        public static ProviderDetailModel GetProviderName(string email, string con, string prac, bool admin)
        {
            ProviderDetailModel providerlist = new ProviderDetailModel();
            DataTable dt = new DataTable();

            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    string sqlTextToExecute = "";

                    cmd.CommandType = CommandType.Text;
                    if (admin)
                    {
                        sqlTextToExecute = "select [PROVID],[LASTNAME],[FIRSTNAME],CASE WHEN [CONTRACT] = 2 THEN 'CONTRACTED' ELSE 'NON-CONTRACTED' END AS [CONTRACT],[CONTACT] from PROV_MASTERS where [PROVID] = '" + prac + "'";

                    }
                    else
                    {
                        sqlTextToExecute = "select [PROVID],[LASTNAME],[FIRSTNAME],CASE WHEN [CONTRACT] = 2 THEN 'CONTRACTED' ELSE 'NON-CONTRACTED' END AS [CONTRACT],[CONTACT] from PROV_MASTERS where EMAIL = '" + email + "'";

                    }
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    DataRow dr = dt.Rows[0];
                    providerlist.ProvID = dr["PROVID"].ToString();
                    providerlist.LastName = dr["LASTNAME"].ToString();
                    providerlist.FirstName = dr["FIRSTNAME"].ToString();
                    providerlist.Contact = dr["CONTACT"].ToString();
                    providerlist.Contract = dr["CONTRACT"].ToString();
                }
                Sqlcon.Close();
            }
            return providerlist;
        }

        /*Returns provider details*/
        public static ProviderDetailModel GetProviderNameLogIn(string provid, string con)
        {
            ProviderDetailModel providerlist = new ProviderDetailModel();
            DataTable dt = new DataTable();

            using (SqlConnection Sqlcon = new SqlConnection(con))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "select [PROVID],[LASTNAME],[FIRSTNAME],CASE WHEN [CONTRACT] = 2 THEN 'CONTRACTED' ELSE 'NON-CONTRACTED' END AS [CONTRACT],[EMAIL] from PROV_MASTERS where [PROVID] = '" + provid + "'";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    DataRow dr = dt.Rows[0];
                    providerlist.ProvID = dr["PROVID"].ToString();
                    providerlist.LastName = dr["LASTNAME"].ToString();
                    providerlist.FirstName = dr["FIRSTNAME"].ToString();
                    providerlist.Email = dr["EMAIL"].ToString();
                    providerlist.Contract = dr["CONTRACT"].ToString();

                    sqlTextToExecute = "select [PHONECODE] + [PHONE] as CONTACT from [PROV_ADDINFO] where [PROVID] = '" + provid + "' and TYPE = 'OFFICE 2' ";
                    cmd.CommandText = sqlTextToExecute;
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    if(dt.Rows.Count > 0)
                    {
                        dr = dt.Rows[0];
                        providerlist.Contact = dr["CONTACT"].ToString();
                    }                   
                }
                Sqlcon.Close();
            }
            return providerlist;
        }

        public static string MembershipQueryClient(string DRCDatabase, string PamcPortalDatabase, string ReportingDatabase, string whereClause)
        {
            string sql =

                // Jaco comment out 2024-08-29 - can not handle delimited lobcode
                //$" SELECT   u.username, u.lobcode, hp.HPCODE, Oxygen.memberPrefix, {PamcPortalDatabase}.dbo.GenerateSubSsn(memberPrefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membnum)) as possiblesubssn  \n " +
                // " into #x \n " +
                //$" FROM     {PamcPortalDatabase}..Users AS u \n " +
                //$"             INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp on(hp.LOBCODE = u.lobcode) \n " +
                // "             INNER JOIN( \n " +
                // "                         Select ezHpCode, isnull(MemberPrefix, '') as MemberPrefix \n " +
                //$"                         From {ReportingDatabase}..OxygenBenefitOptions \n " +
                // "                         Where scheme<> 'carecross' \n " +
                // "                         Group by ezHpCode, MemberPrefix \n " +
                // " 					    ) as Oxygen ON (hp.HPCODE = Oxygen.ezHpCode) \n " +
                //$" WHERE hp.LOBCODE IN (SELECT* FROM {DRCDatabase}..SPLIT_STRING(U.lobcode,',') )  AND \n " +
                //$"        u.username = @user \n\n" +

                // Jaco 2024-08-29 - delimited lobcode
                $" SELECT   u.username, lobs.lobcode, hp.HPCODE, Oxygen.memberPrefix, {PamcPortalDatabase}.dbo.GenerateSubSsn(memberPrefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membnum)) as possiblesubssn  \n " +
                 " into #x \n " +
                $" FROM     {PamcPortalDatabase}..Users AS u \n " +
                 "             INNER JOIN ( \n " +
                 "                           SELECT @user as USERNAME, [value] as lobcode \n " +
                $"                           FROM {DRCDatabase}..SPLIT_STRING((SELECT LOBCODE FROM {PamcPortalDatabase}..Users WHERE username = @user), ',') \n " +
                 "	                      ) as lobs on(u.Username = lobs.username) \n " +
                $"             INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp on(hp.LOBCODE = lobs.lobcode) \n " +
                 "             INNER JOIN( \n " +
                 "                         Select ezHpCode, isnull(MemberPrefix, '') as MemberPrefix \n " +
                $"                         From {ReportingDatabase}..OxygenBenefitOptions \n " +
                 "                         Where scheme<> 'carecross' \n " +
                 "                         Group by ezHpCode, MemberPrefix \n " +
                 " 					    ) as Oxygen ON (hp.HPCODE = Oxygen.ezHpCode) \n " +
                $" WHERE u.username = @user \n\n" +

                " SELECT DISTINCT  \n " +
                " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.EMAIL, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus,  \n " +
                " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS, b.FROMDATE, b.TODATE \n " +
                " FROM \n " +
                "            ( \n " +
                "                SELECT \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN 'IN WAIT PERIOD' \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'TERMINATED' \n " +
                "                    ELSE('TERMINATED') \n " +
                "                END AS MemberStatus, \n " +

                "                Base_Curr.MEMBID, Base_Curr.RLSHIP, Base_Curr.LASTNM, Base_Curr.FIRSTNM, Base_Curr.EMAIL, Base_Curr.BIRTH, \n " +
                "                Base_Curr.SUBSSN, Base_Curr.RELATION, Base_Curr.DEPENDANT, Base_Curr.HPName, Base_Curr.LOBCODE, Base_Curr.TERM_CODE, Base_Curr.ezHpCode, Base_Curr.Expr1, Base_Curr.MemberPrefix, \n " +
                "                Base_Curr.CONSULTATIONS, Wait.FROMDATE, Wait.TODATE, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPCODE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                END AS HPCODE, \n " +
                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                END AS OPT, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                END AS HPFROMDT, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                END AS OPFROMDT, \n " +

                "                CASE \n " +
                "                    WHEN(Hist.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Hist.OPTHRUDT)) THEN Hist.OPTHRUDT \n " +
                "                    WHEN(Base_Curr.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Base_Curr.OPTHRUDT)) THEN Base_Curr.OPTHRUDT \n " +
                "                    ELSE NULL \n " +
                "                END AS OPTHRUDT \n " +

                "                FROM(SELECT DISTINCT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.FIRSTNM, MEMBERSHIP.EMAIL, MEMBERSHIP.HPFROMDT, MEMBERSHIP.HPCODE, MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \n " +
                "                         MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, RTRIM(MEMBERSHIP.SUBSSN) AS SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \n " +
                "                         MEMBERSHIP.MemberStatus, \n " +
                "                         MEMBERSHIP.ezHpCode, MEMBERSHIP.Expr1, MEMBERSHIP.MemberPrefix, ISNULL(GP_Consultations.CONSULTATIONS, 0) AS CONSULTATIONS \n " +

                "                         FROM( \n " +
                "                                    SELECT        mm.MEMBID, \n " +
                "                                                  mm.RLSHIP, \n " +
                "                                                  mm.LASTNM, \n " +
                "                                                  mm.MI, \n " +
                "                                                  mm.FIRSTNM, \n " +
                "                                                  mm.EMAIL, \n " +
                "                                                  mh.HPFROMDT, \n " +
                "                                                  mh.HPCODE, \n " +
                "                                                  ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, \n " +
                "                                                  mh.OPFROMDT, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN NULL \n " +
                "                                                    ELSE mh.OPTHRUDT \n " +
                "                                                  END AS OPTHRUDT, \n " +
                "                                                  mm.BIRTH, \n " +
                "                                                  mm.SUBSSN, \n " +
                "                                                  ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, \n " +
                "                                                  RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1) AS DEPENDANT, \n " +
                "                                                  mh.OPT AS HPName, \n " +
                "                                                  HP_CONTRACTS.LOBCODE, \n " +
                "                                                  mh.TERM_CODE, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN mh.CURRHIST = 'c' AND isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN 'ACTIVE' \n " +
                "                                                    ELSE 'TERMINATED' \n " +
                "                                                  END AS MemberStatus, \n " +
                "                                                  mm.PATID, \n " +
                "                                                  mm.BROKERID, \n " +
                "                                                  oxy.MemberPrefix, \n " +
                "                                                  oxy.ezHpCode, \n " +
                "                                                  oxy.HPNAME AS Expr1 \n " +

               $"                                    FROM          {DRCDatabase}..MEMB_MASTERS AS mm \n " +
               $"                                                  INNER JOIN {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID \n " +
               $"                                                  INNER JOIN {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = HP_CONTRACTS.HPCODE \n " +
                "                                                  LEFT OUTER JOIN( \n " +
                "                                                                        SELECT DISTINCT oxygen.ezHpCode, hp_c.HPNAME, oxygen.MemberPrefix, hp_c.LOBCODE \n " +
               $"                                                                        FROM            {ReportingDatabase}..OxygenBenefitOptions as oxygen \n " +
               $"                                                                                        INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = oxygen.ezHpCode \n " +
                "                                                                        WHERE        oxygen.Scheme <> 'carecross' \n " +
               $"                                                                    ) AS oxy ON {DRCDatabase}..HP_CONTRACTS.HPCODE = oxy.ezHpCode \n " +
               $"                                                  LEFT OUTER JOIN   {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName \n " +
               $"                                                  LEFT OUTER JOIN   {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                "                                    WHERE(mh.CURRHIST = 'c') \n " +
                "                              ) AS MEMBERSHIP \n " +
                "                                LEFT OUTER JOIN( \n " +
                "                                                    SELECT      MEMBID, SUM(cd.QTY) AS CONSULTATIONS, HPCODE \n " +
               $"                                                    FROM           {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \n " +
               $"                                                                                {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO \n " +
               $"                                                                            INNER JOIN {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \n " +
                "                                                    WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \n " +
              //"                                                    AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \n " + //Jaco 2024-07-12
                "                                                    AND 1 = ( \r\n" +
                "                                                                  CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       ELSE 0 \r\n" +
                "                                                                  END \r\n" +
                "					                                            ) \r\n" +
                "                                                    AND(cm.SPEC IN('14', '15')) \n " +
                "                                                    AND cm.CONTRACT = \n " +
              //"                                                    CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN '2' ELSE  cm.CONTRACT END \n " + // Jaco 2024-07-25
                "                                                    CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE  cm.CONTRACT END \n " +
                "                                                    AND(cd.NET <> 0) \n " +
              //"                                                    AND cd.PHCODE = CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN 'P' ELSE  cd.PHCODE END \n " + // Jaco 2024-07-25
                "                                                    AND cd.PHCODE = 'P' \n " +
                "                                                    GROUP BY MEMBID, HPCODE \n " +
                "                                                ) as GP_Consultations ON MEMBERSHIP.MEMBID = GP_Consultations.MEMBID \n " +
                "                     ) AS Base_Curr \n " +
               $"                     LEFT OUTER JOIN {DRCDatabase}..MEMB_HPHISTS AS Hist ON Base_Curr.MEMBID = Hist.MEMBID AND GETDATE() BETWEEN Hist.OPFROMDT AND ISNULL(Hist.OPTHRUDT, GETDATE()) \n " +
                "                     LEFT OUTER JOIN(SELECT SUBSSN, MEMBID, MIN(FROMDATE) as FROMDATE, MAX(TODATE) as TODATE  --min from and max to dates for wait period and special exclusions \n " +
                "                                       FROM( \n " +
                "                                               SELECT SUBSSN, MW.MEMBID, 'GENERAL WAIT PERIOD' as [DESCRIPTION], FROMDATE, TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_WAIT MW \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MW.MEMBID = MM.MEMBID) \n " +
                "                                               where isnull(TERMSTATUS, 0) = 0 \n " +
                "                                               union all \n " +
                "                                               SELECT SUBSSN, MS.MEMB_ID as MEMBID, SS.DESCR as [DESCRIPTION], MS.FROM_DATE as FROMDATE, MS.TO_DATE as TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_SPECEXL MS \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MS.MEMB_ID = MM.MEMBID) \n " +
               $"                                               LEFT OUTER JOIN {DRCDatabase}..SPECEXL_SETS SS on(MS.SPECEXL_ID = SS.SPECEXL_ID) \n " +
                "                                            ) AS WaitandExcl \n " +
                "                                       GROUP BY MEMBID, SUBSSN \n " +
                "                                      ) as Wait on Base_Curr.SUBSSN = Wait.SUBSSN AND Base_Curr.MEMBID = Wait.MEMBID \n " +
                "                                                    AND GETDATE() <= DATEADD(month, 3, ISNULL(Wait.TODATE, GETDATE())) --Only show if in last 3 months \n " +
                "			) AS a INNER JOIN \n " +
                "            ( \n " +
                "                SELECT \n " +
                "                CASE \n " +
                "                    WHEN (GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN 'IN WAIT PERIOD' \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'TERMINATED' \n " +
                "                    ELSE('TERMINATED') \n " +
                "                END AS MemberStatus,  \n " +

                "                Base_Curr.MEMBID, Base_Curr.RLSHIP, Base_Curr.LASTNM, Base_Curr.FIRSTNM, Base_Curr.EMAIL, Base_Curr.BIRTH,  \n " +
                "                Base_Curr.SUBSSN, Base_Curr.RELATION, Base_Curr.DEPENDANT, Base_Curr.HPName, Base_Curr.LOBCODE, Base_Curr.TERM_CODE, Base_Curr.ezHpCode, Base_Curr.Expr1, Base_Curr.MemberPrefix,  \n " +
                "                Base_Curr.CONSULTATIONS, Wait.FROMDATE, Wait.TODATE, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPCODE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "              END AS HPCODE, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "              END AS OPT, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "              END AS HPFROMDT, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "              END AS OPFROMDT, \n " +

                "              CASE \n " +
                "                    WHEN(Hist.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Hist.OPTHRUDT)) THEN Hist.OPTHRUDT \n " +
                "                    WHEN(Base_Curr.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Base_Curr.OPTHRUDT)) THEN Base_Curr.OPTHRUDT \n " +
                "                  ELSE NULL \n " +
                "              END AS OPTHRUDT \n " +

                "                FROM(SELECT DISTINCT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.FIRSTNM, MEMBERSHIP.EMAIL, MEMBERSHIP.HPFROMDT, MEMBERSHIP.HPCODE, MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \n " +
                "                         MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, RTRIM(MEMBERSHIP.SUBSSN) AS SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \n " +
                "                         MEMBERSHIP.MemberStatus, \n " +
                "                         MEMBERSHIP.ezHpCode, MEMBERSHIP.Expr1, MEMBERSHIP.MemberPrefix, ISNULL(GP_Consultations.CONSULTATIONS, 0) AS CONSULTATIONS \n " +
                "                         FROM( \n " +
                "                                    SELECT        mm.MEMBID, \n " +
                "                                                  mm.RLSHIP, \n " +
                "                                                  mm.LASTNM, \n " +
                "                                                  mm.MI, \n " +
                "                                                  mm.FIRSTNM, \n " +
                "                                                  mm.EMAIL, \n " +
                "                                                  mh.HPFROMDT, \n " +
                "                                                  mh.HPCODE, \n " +
                "                                                  ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, \n " +
                "                                                  mh.OPFROMDT, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN NULL \n " +
                "                                                    ELSE mh.OPTHRUDT \n " +
                "                                                  END AS OPTHRUDT, \n " +
                "                                                  mm.BIRTH, \n " +
                "                                                  mm.SUBSSN, \n " +
                "                                                  ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, \n " +
                "                                                  RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1) AS DEPENDANT, \n " +
                "                                                  mh.OPT AS HPName, \n " +
                "                                                  HP_CONTRACTS.LOBCODE, \n " +
                "                                                  mh.TERM_CODE, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN mh.CURRHIST = 'c' AND isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN 'ACTIVE' \n " +
                "                                                    ELSE 'TERMINATED' \n " +
                "                                                  END AS MemberStatus, \n " +
                "                                                  mm.PATID, \n " +
                "                                                  mm.BROKERID, \n " +
                "                                                  oxy.MemberPrefix, \n " +
                "                                                  oxy.ezHpCode, \n " +
                "                                                  oxy.HPNAME AS Expr1 \n " +

               $"                                    FROM          {DRCDatabase}..MEMB_MASTERS AS mm \n " +
               $"                                                  INNER JOIN {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID \n " +
               $"                                                  INNER JOIN {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = HP_CONTRACTS.HPCODE \n " +
                "                                                  LEFT OUTER JOIN( \n " +
                "                                                                        SELECT DISTINCT oxygen.ezHpCode, hp_c.HPNAME, oxygen.MemberPrefix, hp_c.LOBCODE \n " +
               $"                                                                        FROM            {ReportingDatabase}..OxygenBenefitOptions as oxygen \n " +
               $"                                                                                        INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = oxygen.ezHpCode \n " +
                "                                                                        WHERE        oxygen.Scheme <> 'carecross' \n " +
               $"                                                                    ) AS oxy ON {DRCDatabase}..HP_CONTRACTS.HPCODE = oxy.ezHpCode \n " +
               $"                                                  LEFT OUTER JOIN   {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName \n " +
               $"                                                  LEFT OUTER JOIN   {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                "                                    WHERE(mh.CURRHIST = 'c') \n " +
                "                              ) AS MEMBERSHIP \n " +
                "                                LEFT OUTER JOIN( \n " +
                "                                                    SELECT      MEMBID, SUM(cd.QTY) AS CONSULTATIONS, HPCODE \n " +
               $"                                                    FROM           {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \n " +
               $"                                                                                {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO \n " +
               $"                                                                            INNER JOIN {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \n " +
                "                                                    WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \n " +
              //"                                                    AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \n " + //Jaco 2024-07-12
                "                                                    AND 1 = ( \r\n" +
                "                                                                  CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       ELSE 0 \r\n" +
                "                                                                  END \r\n" +
                "					                                            ) \r\n" +
                "                                                    AND(cm.SPEC IN('14', '15')) \n " +
                "                                                    AND cm.CONTRACT = \n " +
              //"                                                    CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN '2' ELSE  cm.CONTRACT END \n " + // Jaco 2024-07-25
                "                                                    CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE  cm.CONTRACT END \n " +
                "                                                    AND(cd.NET <> 0) \n " +
              //"                                                    AND cd.PHCODE = CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN 'P' ELSE  cd.PHCODE END \n " + // Jaco 2024-07-25
                "                                                    AND cd.PHCODE = 'P' \n " +
                "                                                    GROUP BY MEMBID, HPCODE \n " +
                "                                                ) as GP_Consultations ON MEMBERSHIP.MEMBID = GP_Consultations.MEMBID \n " +
                "                     ) AS Base_Curr \n " +
               $"                     LEFT OUTER JOIN {DRCDatabase}..MEMB_HPHISTS AS Hist ON Base_Curr.MEMBID = Hist.MEMBID AND GETDATE() BETWEEN Hist.OPFROMDT AND ISNULL(Hist.OPTHRUDT, GETDATE()) \n " +
                "                     LEFT OUTER JOIN(SELECT SUBSSN, MEMBID, MIN(FROMDATE) as FROMDATE, MAX(TODATE) as TODATE  --min from and max to dates for wait period and special exclusions \n " +
                "                                       FROM( \n " +
                "                                               SELECT SUBSSN, MW.MEMBID, 'GENERAL WAIT PERIOD' as [DESCRIPTION], FROMDATE, TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_WAIT MW \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MW.MEMBID = MM.MEMBID) \n " +
                "                                               where isnull(TERMSTATUS, 0) = 0 \n " +
                "                                               union all \n " +
                "                                               SELECT SUBSSN, MS.MEMB_ID as MEMBID, SS.DESCR as [DESCRIPTION], MS.FROM_DATE as FROMDATE, MS.TO_DATE as TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_SPECEXL MS \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MS.MEMB_ID = MM.MEMBID) \n " +
               $"                                               LEFT OUTER JOIN {DRCDatabase}..SPECEXL_SETS SS on(MS.SPECEXL_ID = SS.SPECEXL_ID) \n " +
                "                                            ) AS WaitandExcl \n " +
                "                                       GROUP BY MEMBID, SUBSSN \n " +
                "                                      ) as Wait on Base_Curr.SUBSSN = Wait.SUBSSN AND Base_Curr.MEMBID = Wait.MEMBID \n " +
                "                                                    AND GETDATE() <= DATEADD(month, 3, ISNULL(Wait.TODATE, GETDATE())) --Only show if in last 3 months \n " +
                "			) AS b ON a.SUBSSN = b.SUBSSN \n " +
                //"            INNER JOIN #x X ON b.HPCODE = X.HPCODE  and X.possiblesubssn = b.subssn " +
                "            INNER JOIN #x X ON b.HPCODE in (X.HPCODE)  and X.possiblesubssn = b.subssn " +
               $" {whereClause} " +
                " drop table #x ";  

            return sql;
        }

        public static string MembershipQueryProv(string DRCDatabase, string PamcPortalDatabase, string ReportingDatabase, string whereClause)
        {
            string sql =
                " declare @membid varchar(20) \n " +
                " declare @hpCode varchar(10) \n " +
                " declare @prefix varchar(10) \n " +
                " set @hpCode = @plan \n " +
                " set @membid = @membnum \n " +
                " set @prefix = " +
                "                 (SELECT distinct ISNULL(oxygen.MemberPrefix, '') \n " +
               $"                  FROM {ReportingDatabase}..OxygenBenefitOptions oxygen INNER JOIN  \n " +
               $"                             {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = oxygen.ezHpCode  \n " +
                "                  WHERE oxygen.ezHpCode = @hpCode AND oxygen.scheme <> 'carecross') \n " +
               $" set @membid = {PamcPortalDatabase}.dbo.RemoveDependantDash(@membid) \n " +
               $" set @membid = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,@membid) \n " +
                " SELECT DISTINCT \r\n" +
                "  b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.EMAIL, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                "  b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS, b.FROMDATE, b.TODATE\r\n" +
                " FROM \n " +
                "            ( \n " +
                "                SELECT \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN 'IN WAIT PERIOD' \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'TERMINATED' \n " +
                "                    ELSE('TERMINATED') \n " +
                "                END AS MemberStatus, \n " +

                "                Base_Curr.MEMBID, Base_Curr.RLSHIP, Base_Curr.LASTNM, Base_Curr.FIRSTNM, Base_Curr.EMAIL, Base_Curr.BIRTH, \n " +
                "                Base_Curr.SUBSSN, Base_Curr.RELATION, Base_Curr.DEPENDANT, Base_Curr.HPName, Base_Curr.LOBCODE, Base_Curr.TERM_CODE, Base_Curr.ezHpCode, Base_Curr.Expr1, Base_Curr.MemberPrefix, \n " +
                "                Base_Curr.CONSULTATIONS, Wait.FROMDATE, Wait.TODATE, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPCODE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                END AS HPCODE, \n " +
                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                END AS OPT, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                END AS HPFROMDT, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                END AS OPFROMDT, \n " +

                "                CASE \n " +
                "                    WHEN(Hist.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Hist.OPTHRUDT)) THEN Hist.OPTHRUDT \n " +
                "                    WHEN(Base_Curr.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Base_Curr.OPTHRUDT)) THEN Base_Curr.OPTHRUDT \n " +
                "                    ELSE NULL \n " +
                "                END AS OPTHRUDT \n " +

                "                FROM(SELECT DISTINCT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.FIRSTNM, MEMBERSHIP.EMAIL, MEMBERSHIP.HPFROMDT, MEMBERSHIP.HPCODE, MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \n " +
                "                         MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, RTRIM(MEMBERSHIP.SUBSSN) AS SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \n " +
                "                         MEMBERSHIP.MemberStatus, \n " +
                "                         MEMBERSHIP.ezHpCode, MEMBERSHIP.Expr1, MEMBERSHIP.MemberPrefix, ISNULL(GP_Consultations.CONSULTATIONS, 0) AS CONSULTATIONS \n " +

                "                         FROM( \n " +
                "                                    SELECT        mm.MEMBID, \n " +
                "                                                  mm.RLSHIP, \n " +
                "                                                  mm.LASTNM, \n " +
                "                                                  mm.MI, \n " +
                "                                                  mm.FIRSTNM, \n " +
                "                                                  mm.EMAIL, \n " +
                "                                                  mh.HPFROMDT, \n " +
                "                                                  mh.HPCODE, \n " +
                "                                                  ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, \n " +
                "                                                  mh.OPFROMDT, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN NULL \n " +
                "                                                    ELSE mh.OPTHRUDT \n " +
                "                                                  END AS OPTHRUDT, \n " +
                "                                                  mm.BIRTH, \n " +
                "                                                  mm.SUBSSN, \n " +
                "                                                  ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, \n " +
                "                                                  RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1) AS DEPENDANT, \n " +
                "                                                  mh.OPT AS HPName, \n " +
                "                                                  HP_CONTRACTS.LOBCODE, \n " +
                "                                                  mh.TERM_CODE, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN mh.CURRHIST = 'c' AND isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN 'ACTIVE' \n " +
                "                                                    ELSE 'TERMINATED' \n " +
                "                                                  END AS MemberStatus, \n " +
                "                                                  mm.PATID, \n " +
                "                                                  mm.BROKERID, \n " +
                "                                                  oxy.MemberPrefix, \n " +
                "                                                  oxy.ezHpCode, \n " +
                "                                                  oxy.HPNAME AS Expr1 \n " +

               $"                                    FROM          {DRCDatabase}..MEMB_MASTERS AS mm \n " +
               $"                                                  INNER JOIN {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID \n " +
               $"                                                  INNER JOIN {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = HP_CONTRACTS.HPCODE \n " +
                "                                                  LEFT OUTER JOIN( \n " +
                "                                                                        SELECT DISTINCT oxygen.ezHpCode, hp_c.HPNAME, oxygen.MemberPrefix, hp_c.LOBCODE \n " +
               $"                                                                        FROM            {ReportingDatabase}..OxygenBenefitOptions as oxygen \n " +
               $"                                                                                        INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = oxygen.ezHpCode \n " +
                "                                                                        WHERE        oxygen.Scheme <> 'carecross' \n " +
               $"                                                                    ) AS oxy ON {DRCDatabase}..HP_CONTRACTS.HPCODE = oxy.ezHpCode \n " +
               $"                                                  LEFT OUTER JOIN   {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName \n " +
               $"                                                  LEFT OUTER JOIN   {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                "                                    WHERE(mh.CURRHIST = 'c') \n " +
                "                              ) AS MEMBERSHIP \n " +
                "                                LEFT OUTER JOIN( \n " +
                "                                                    SELECT      MEMBID, SUM(cd.QTY) AS CONSULTATIONS, HPCODE \n " +
               $"                                                    FROM           {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \n " +
               $"                                                                                {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO \n " +
               $"                                                                            INNER JOIN {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \n " +
                "                                                    WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \n " +
              //"                                                    AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \n " + //Jaco 2024-07-12
                "                                                    AND 1 = ( \r\n" +
                "                                                                  CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       ELSE 0 \r\n" +
                "                                                                  END \r\n" +
                "					                                            ) \r\n" +
                "                                                    AND(cm.SPEC IN('14', '15')) \n " +
                "                                                    AND cm.CONTRACT = \n " +
              //"                                                    CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN '2' ELSE  cm.CONTRACT END \n " + // Jaco 2024-07-25
                "                                                    CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE  cm.CONTRACT END \n " +
                "                                                    AND(cd.NET <> 0) \n " +
              //"                                                    AND cd.PHCODE = CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN 'P' ELSE  cd.PHCODE END \n " + // Jaco 2024-07-25
                "                                                    AND cd.PHCODE = 'P' \n " +
                "                                                    GROUP BY MEMBID, HPCODE \n " +
                "                                                ) as GP_Consultations ON MEMBERSHIP.MEMBID = GP_Consultations.MEMBID \n " +
                "                     ) AS Base_Curr \n " +
               $"                     LEFT OUTER JOIN {DRCDatabase}..MEMB_HPHISTS AS Hist ON Base_Curr.MEMBID = Hist.MEMBID AND GETDATE() BETWEEN Hist.OPFROMDT AND ISNULL(Hist.OPTHRUDT, GETDATE()) \n " +
                "                     LEFT OUTER JOIN(SELECT SUBSSN, MEMBID, MIN(FROMDATE) as FROMDATE, MAX(TODATE) as TODATE  --min from and max to dates for wait period and special exclusions \n " +
                "                                       FROM( \n " +
                "                                               SELECT SUBSSN, MW.MEMBID, 'GENERAL WAIT PERIOD' as [DESCRIPTION], FROMDATE, TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_WAIT MW \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MW.MEMBID = MM.MEMBID) \n " +
                "                                               where isnull(TERMSTATUS, 0) = 0 \n " +
                "                                               union all \n " +
                "                                               SELECT SUBSSN, MS.MEMB_ID as MEMBID, SS.DESCR as [DESCRIPTION], MS.FROM_DATE as FROMDATE, MS.TO_DATE as TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_SPECEXL MS \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MS.MEMB_ID = MM.MEMBID) \n " +
               $"                                               LEFT OUTER JOIN {DRCDatabase}..SPECEXL_SETS SS on(MS.SPECEXL_ID = SS.SPECEXL_ID) \n " +
                "                                            ) AS WaitandExcl \n " +
                "                                       GROUP BY MEMBID, SUBSSN \n " +
                "                                      ) as Wait on Base_Curr.SUBSSN = Wait.SUBSSN AND Base_Curr.MEMBID = Wait.MEMBID \n " +
                "                                                    AND GETDATE() <= DATEADD(month, 3, ISNULL(Wait.TODATE, GETDATE())) --Only show if in last 3 months \n " +
                "			) AS a INNER JOIN \n " +
                "            ( \n " +
                "                SELECT \n " +
                "                CASE \n " +
                "                    WHEN (GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN 'IN WAIT PERIOD' \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN 'ACTIVE' \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN 'TERMINATED' \n " +
                "                    ELSE('TERMINATED') \n " +
                "                END AS MemberStatus,  \n " +

                "                Base_Curr.MEMBID, Base_Curr.RLSHIP, Base_Curr.LASTNM, Base_Curr.FIRSTNM, Base_Curr.EMAIL, Base_Curr.BIRTH,  \n " +
                "                Base_Curr.SUBSSN, Base_Curr.RELATION, Base_Curr.DEPENDANT, Base_Curr.HPName, Base_Curr.LOBCODE, Base_Curr.TERM_CODE, Base_Curr.ezHpCode, Base_Curr.Expr1, Base_Curr.MemberPrefix,  \n " +
                "                Base_Curr.CONSULTATIONS, Wait.FROMDATE, Wait.TODATE, \n " +

                "                CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPCODE \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPCODE \n " +
                "              END AS HPCODE, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPT \n " +
                "              END AS OPT, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.HPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.HPFROMDT \n " +
                "              END AS HPFROMDT, \n " +

                "              CASE \n " +
                "                    WHEN(GETDATE() >= Base_Curr.OPFROMDT) AND(GETDATE() <= isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Hist.OPFROMDT) AND(GETDATE() <= isnull(Hist.OPTHRUDT, GETDATE())) THEN Hist.OPFROMDT \n " +
                "                    WHEN(GETDATE() >= Wait.FROMDATE) AND(GETDATE() <= isnull(Wait.TODATE, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "                    WHEN(GETDATE() < Base_Curr.OPFROMDT) OR(GETDATE() > isnull(Base_Curr.OPTHRUDT, GETDATE())) THEN Base_Curr.OPFROMDT \n " +
                "              END AS OPFROMDT, \n " +

                "              CASE \n " +
                "                    WHEN(Hist.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Hist.OPTHRUDT)) THEN Hist.OPTHRUDT \n " +
                "                    WHEN(Base_Curr.OPTHRUDT is not null AND GETDATE() > DATEADD(DAY, -7, Base_Curr.OPTHRUDT)) THEN Base_Curr.OPTHRUDT \n " +
                "                  ELSE NULL \n " +
                "              END AS OPTHRUDT \n " +

                "                FROM(SELECT DISTINCT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.FIRSTNM, MEMBERSHIP.EMAIL, MEMBERSHIP.HPFROMDT, MEMBERSHIP.HPCODE, MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \n " +
                "                         MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, RTRIM(MEMBERSHIP.SUBSSN) AS SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \n " +
                "                         MEMBERSHIP.MemberStatus, \n " +
                "                         MEMBERSHIP.ezHpCode, MEMBERSHIP.Expr1, MEMBERSHIP.MemberPrefix, ISNULL(GP_Consultations.CONSULTATIONS, 0) AS CONSULTATIONS \n " +
                "                         FROM( \n " +
                "                                    SELECT        mm.MEMBID, \n " +
                "                                                  mm.RLSHIP, \n " +
                "                                                  mm.LASTNM, \n " +
                "                                                  mm.MI, \n " +
                "                                                  mm.FIRSTNM, \n " +
                "                                                  mm.EMAIL, \n " +
                "                                                  mh.HPFROMDT, \n " +
                "                                                  mh.HPCODE, \n " +
                "                                                  ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, \n " +
                "                                                  mh.OPFROMDT, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN NULL \n " +
                "                                                    ELSE mh.OPTHRUDT \n " +
                "                                                  END AS OPTHRUDT, \n " +
                "                                                  mm.BIRTH, \n " +
                "                                                  mm.SUBSSN, \n " +
                "                                                  ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, \n " +
                "                                                  RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1) AS DEPENDANT, \n " +
                "                                                  mh.OPT AS HPName, \n " +
                "                                                  HP_CONTRACTS.LOBCODE, \n " +
                "                                                  mh.TERM_CODE, \n " +
                "                                                  CASE \n " +
                "                                                    WHEN mh.CURRHIST = 'c' AND isnull(mh.OPTHRUDT, GETDATE()) >= GETDATE() THEN 'ACTIVE' \n " +
                "                                                    ELSE 'TERMINATED' \n " +
                "                                                  END AS MemberStatus, \n " +
                "                                                  mm.PATID, \n " +
                "                                                  mm.BROKERID, \n " +
                "                                                  oxy.MemberPrefix, \n " +
                "                                                  oxy.ezHpCode, \n " +
                "                                                  oxy.HPNAME AS Expr1 \n " +

               $"                                    FROM          {DRCDatabase}..MEMB_MASTERS AS mm \n " +
               $"                                                  INNER JOIN {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID \n " +
               $"                                                  INNER JOIN {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = HP_CONTRACTS.HPCODE \n " +
                "                                                  LEFT OUTER JOIN( \n " +
                "                                                                        SELECT DISTINCT oxygen.ezHpCode, hp_c.HPNAME, oxygen.MemberPrefix, hp_c.LOBCODE \n " +
               $"                                                                        FROM            {ReportingDatabase}..OxygenBenefitOptions as oxygen \n " +
               $"                                                                                        INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = oxygen.ezHpCode \n " +
                "                                                                        WHERE        oxygen.Scheme <> 'carecross' \n " +
               $"                                                                    ) AS oxy ON {DRCDatabase}..HP_CONTRACTS.HPCODE = oxy.ezHpCode \n " +
               $"                                                  LEFT OUTER JOIN   {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName \n " +
               $"                                                  LEFT OUTER JOIN   {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                "                                    WHERE(mh.CURRHIST = 'c') \n " +
                "                              ) AS MEMBERSHIP \n " +
                "                                LEFT OUTER JOIN( \n " +
                "                                                    SELECT      MEMBID, SUM(cd.QTY) AS CONSULTATIONS, HPCODE \n " +
               $"                                                    FROM           {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \n " +
               $"                                                                                {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO \n " +
               $"                                                                            INNER JOIN {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \n " +
                "                                                    WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \n " +
              //"                                                    AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \n " + //Jaco 2024-07-12
                "                                                    AND 1 = ( \r\n" +
                "                                                                  CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                "                                                                       ELSE 0 \r\n" +
                "                                                                  END \r\n" +
                "					                                            ) \r\n" +
                "                                                    AND(cm.SPEC IN('14', '15')) \n " +
                "                                                    AND cm.CONTRACT = \n " +
              //"                                                    CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN '2' ELSE  cm.CONTRACT END \n " + // Jaco 2024-07-25
                "                                                    CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE  cm.CONTRACT END \n " +
                "                                                    AND(cd.NET <> 0) \n " +
              //"                                                    AND cd.PHCODE = CASE WHEN HPCODE <> 'AFFI' AND HPCODE <> 'AFFD' THEN 'P' ELSE  cd.PHCODE END \n " + // Jaco 2024-07-25
                "                                                    AND cd.PHCODE = 'P' \n " +
                "                                                    GROUP BY MEMBID, HPCODE \n " +
                "                                                ) as GP_Consultations ON MEMBERSHIP.MEMBID = GP_Consultations.MEMBID \n " +
                "                     ) AS Base_Curr \n " +
               $"                     LEFT OUTER JOIN {DRCDatabase}..MEMB_HPHISTS AS Hist ON Base_Curr.MEMBID = Hist.MEMBID AND GETDATE() BETWEEN Hist.OPFROMDT AND ISNULL(Hist.OPTHRUDT, GETDATE()) \n " +
                "                     LEFT OUTER JOIN(SELECT SUBSSN, MEMBID, MIN(FROMDATE) as FROMDATE, MAX(TODATE) as TODATE  --min from and max to dates for wait period and special exclusions \n " +
                "                                       FROM( \n " +
                "                                               SELECT SUBSSN, MW.MEMBID, 'GENERAL WAIT PERIOD' as [DESCRIPTION], FROMDATE, TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_WAIT MW \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MW.MEMBID = MM.MEMBID) \n " +
                "                                               where isnull(TERMSTATUS, 0) = 0 \n " +
                "                                               union all \n " +
                "                                               SELECT SUBSSN, MS.MEMB_ID as MEMBID, SS.DESCR as [DESCRIPTION], MS.FROM_DATE as FROMDATE, MS.TO_DATE as TODATE \n " +
               $"                                               FROM {DRCDatabase}..MEMB_SPECEXL MS \n " +
               $"                                               INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MS.MEMB_ID = MM.MEMBID) \n " +
               $"                                               LEFT OUTER JOIN {DRCDatabase}..SPECEXL_SETS SS on(MS.SPECEXL_ID = SS.SPECEXL_ID) \n " +
                "                                            ) AS WaitandExcl \n " +
                "                                       GROUP BY MEMBID, SUBSSN \n " +
                "                                      ) as Wait on Base_Curr.SUBSSN = Wait.SUBSSN AND Base_Curr.MEMBID = Wait.MEMBID \n " +
                "                                                    AND GETDATE() <= DATEADD(month, 3, ISNULL(Wait.TODATE, GETDATE())) --Only show if in last 3 months \n " +
                "			) AS b ON a.SUBSSN = b.SUBSSN \n " +                                    
               $" {whereClause} ";          

            return sql;
        }

        public static string GetMemberPrefix(SqlConnection cn, string hpCode, string DRCDatabase, string ReportingDatabase)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;

            cmd.CommandText =
                                $" SELECT ISNULL(O.MemberPrefix, '') as Prefix \n" +
                                $" FROM {ReportingDatabase}..OxygenBenefitOptions as O \n" +
                                $"         INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = O.ezHpCode \n" +
                                $" where O.ezHpCode = @hpCode AND O.scheme <> 'carecross' ";
            
            cmd.Parameters.Add(new SqlParameter("@hpCode", hpCode));

            DataTable dt = new DataTable();
            if (cn.State != ConnectionState.Open) cn.Open();
            string prefix = cmd.ExecuteScalar().ToString();            
            cmd.Dispose();

            return prefix;
        }

        public static async Task WriteToSqlLog(string jsonRequest, string jsonResponse, string controllerName, string endpointName, string PamcPortalDatabase, string contentRootPath, string user)
        {
            await Task.Delay(1); // Jaco - Async not working correctly without this

            // Declare sqlConnection outside try catch so that we can close and dispose inside catch - otherwise it will remain open
            SqlCommand cmd = new SqlCommand("", new SqlConnection(PamcPortalDatabase));
            
            try
            {
                //if (log.Application == null) cmd.Parameters.Add(new SqlParameter("@Application", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@Application", log.Application));

                //if (log.User == null) cmd.Parameters.Add(new SqlParameter("@User", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@User", log.User));

                //if (log.IpAddress == null) cmd.Parameters.Add(new SqlParameter("@IpAddress", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@IpAddress", log.IpAddress));

                //if (log.Machine == null) cmd.Parameters.Add(new SqlParameter("@Machine", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@Machine", log.Machine));

                //if (log.RequestBody == null) cmd.Parameters.Add(new SqlParameter("@RequestBody", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestBody", log.RequestBody));

                //if (log.RequestContentType == null) cmd.Parameters.Add(new SqlParameter("@RequestContentType", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestContentType", log.RequestContentType));

                //if (log.RequestHeaders == null) cmd.Parameters.Add(new SqlParameter("@RequestHeaders", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestHeaders", log.RequestHeaders));

                //if (log.RequestMethod == null) cmd.Parameters.Add(new SqlParameter("@RequestMethod", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestMethod", log.RequestMethod));

                //if (log.RequestTimestamp == null) cmd.Parameters.Add(new SqlParameter("@RequestTimestamp", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestTimestamp", log.RequestTimestamp));

                //if (log.RequestUri == null) cmd.Parameters.Add(new SqlParameter("@RequestUri", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@RequestUri", log.RequestUri));

                //if (log.ResponseBody == null) cmd.Parameters.Add(new SqlParameter("@ResponseBody", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@ResponseBody", log.ResponseBody));

                //if (log.ResponseStatusCode == null) cmd.Parameters.Add(new SqlParameter("@ResponseStatusCode", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@ResponseStatusCode", log.ResponseStatusCode));

                //if (log.ResponseTimestamp == null) cmd.Parameters.Add(new SqlParameter("@ResponseTimestamp", DBNull.Value));
                //else cmd.Parameters.Add(new SqlParameter("@ResponseTimestamp", log.ResponseTimestamp));

                cmd.Parameters.Add(new SqlParameter("@User", user));
                cmd.Parameters.Add(new SqlParameter("@RequestBody", jsonRequest));
                cmd.Parameters.Add(new SqlParameter("@RequestUri", controllerName + "/" + endpointName));
                cmd.Parameters.Add(new SqlParameter("@ResponseBody", jsonResponse));

                cmd.CommandText =
                " INSERT INTO [dbo].[GENERIC_LOG] \r\n" +
                "            ([Application] \r\n" +
                "            ,[User] \r\n" +
                "            ,[IpAddress] \r\n" +
                "            ,[Machine] \r\n" +
                "            ,[RequestBody] \r\n" +
                "            ,[RequestContentType] \r\n" +
                "            ,[RequestHeaders] \r\n" +
                "            ,[RequestMethod] \r\n" +
                "            ,[RequestTimestamp] \r\n" +
                "            ,[RequestUri] \r\n" +
                "            ,[ResponseBody] \r\n" +
                "            ,[ResponseStatusCode] \r\n" +
                "            ,[ResponseTimestamp]) \r\n" +
                "      VALUES \r\n" +
                "            ('PamcPortal' \r\n" +
                "            ,@User \r\n" +
                "            ,NULL --@IpAddress \r\n" +
                "            ,NULL --@Machine \r\n" +
                "            ,@RequestBody \r\n" +
                "            ,NULL --@RequestContentType \r\n" +
                "            ,NULL --@RequestHeaders \r\n" +
                "            ,NULL --@RequestMethod \r\n" +
                "            ,GETDATE() --@RequestTimestamp \r\n" +
                "            ,@RequestUri \r\n" +
                "            ,@ResponseBody \r\n" +
                "            ,NULL --@ResponseStatusCode \r\n" +
                "            ,GETDATE()) --@ResponseTimestam) ";

                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                cmd.CommandTimeout = 60;
                cmd.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {               
                WriteErrorToFile(controllerName, "WriteToSqlLog", contentRootPath, ex, user);
                WriteRequestResponseToFile(jsonRequest, jsonResponse, controllerName, endpointName, contentRootPath, user);
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
            }
        }

        public static void WriteErrorToFile(string controllerName, string endpointName, string contentRootPath, Exception ex, string user)
        {
            try
            {
                string path = Path.Combine(contentRootPath, "GENERIC_LOG", controllerName);
                Directory.CreateDirectory(path);
                path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + "_" + endpointName + "_Error" + ".txt");
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine("Date : " + DateTime.Now.ToString());
                    writer.WriteLine("User: " + user);
                    writer.WriteLine();

                    while (ex != null)
                    {
                        writer.WriteLine(ex.GetType().FullName);
                        writer.WriteLine("Message : " + ex.Message);
                        writer.WriteLine("StackTrace : " + ex.StackTrace);
                        ex = ex.InnerException;
                    }
                }
            }
            catch (Exception Ex)
            {
                // TODO: can maybe send an email/sms       
            }
        }

        public static void WriteRequestResponseToFile(string jsonRequest, string jsonResponse, string controllerName, string endpointName, string contentRootpath, string user)
        {
            try
            {
                string bodyType = "";
                if (jsonRequest.Trim() != "") bodyType += "Request";                
                if (jsonResponse.Trim() != "") bodyType += "Response";

                string path = Path.Combine(contentRootpath, "GENERIC_LOG", controllerName);
                Directory.CreateDirectory(path);
                path = Path.Combine(path, DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + "_" + endpointName + "_" + bodyType + ".txt");
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine("Date: " + DateTime.Now.ToString());
                    writer.WriteLine("User: " + user);

                    if (jsonRequest.Trim() !=  "")
                    {
                        writer.WriteLine();
                        writer.WriteLine("Request:");
                        writer.WriteLine();
                        writer.Write(jsonRequest);
                    }

                    if (jsonResponse.Trim() != "")
                    {
                        writer.WriteLine();
                        writer.WriteLine("Response:");
                        writer.WriteLine();
                        writer.Write(jsonResponse);
                    }
                    
                }
            }
            catch (Exception Ex)
            {
                // TODO: can maybe send an email/sms       
            }
        }
    }
}

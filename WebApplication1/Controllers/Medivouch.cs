﻿using MediWalletWebsite_1.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using Newtonsoft.Json;
using PamcEzLinkLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Medivouch : Base.PAMCController
    {
        public Medivouch(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("GetBasketHpCodes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<VoucherHpModel> GetBasketHpCodes([FromBody] DefaultCodeModel data)
        {
            List<VoucherHpModel> list = new List<VoucherHpModel>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    if (data.provSpec == "") // Show all for voucheradmin 
                    {
                        cmd.CommandText =
                            $" SELECT HPCODE.HPCODE, HPCODE.OPT, HPCODE.FRONTFACINGOPTDESC, HPCODE.MEMBERPREFIX, HPCONTRACT.HPNAME, HPCONTRACT.LOBCODE FROM " +
                            $" (select distinct HPCODE, OPT, FRONTFACINGOPTDESC, MEMBERPREFIX from {MediDatabase}.[dbo].[SVC_CODES_SETUP_HEADER]  where Enabled = 1) as HPCODE " +
                            //$" LEFT JOIN (	SELECT DISTINCT ezHpCode, MEMBERPREFIX FROM [Reporting].[dbo].[OxygenBenefitOptions]) as PREFIX ON (HPCODE.HPCODE = PREFIX.ezHpCode) " +
                            $" LEFT JOIN (	SELECT DISTINCT HPCODE, HPNAME, LOBCODE FROM {DRCDatabase}.[dbo].[HP_CONTRACTS]) as HPCONTRACT ON (HPCODE.HPCODE = HPCONTRACT.HPCODE) ";
                    }
                    else // Show only those applicable to provider
                    {
                        cmd.CommandText =
                            $" SELECT HPCODE.HPCODE, HPCODE.OPT, HPCODE.FRONTFACINGOPTDESC, HPCODE.MEMBERPREFIX, HPCONTRACT.HPNAME, HPCONTRACT.LOBCODE FROM " +
                            $" (select distinct HPCODE, OPT, FRONTFACINGOPTDESC, MEMBERPREFIX from {MediDatabase}.[dbo].[SVC_CODES_SETUP_HEADER]  " +
                            $"  where Enabled = 1 " +
                            //$" and HPCODE <> 'MDV' " +
                            $" and Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%') as HPCODE  " +
                            //$" LEFT JOIN (	SELECT DISTINCT ezHpCode, MEMBERPREFIX FROM [Reporting].[dbo].[OxygenBenefitOptions]) as PREFIX ON (HPCODE.HPCODE = PREFIX.ezHpCode) " +
                            $" LEFT JOIN (	SELECT DISTINCT HPCODE, HPNAME, LOBCODE FROM {DRCDatabase}.[dbo].[HP_CONTRACTS]) as HPCONTRACT ON (HPCODE.HPCODE = HPCONTRACT.HPCODE) ";
                    }

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    cn.Close();
                    cmd.Dispose();

                    foreach (DataRow row in dt.Rows)
                    {
                        VoucherHpModel t = new VoucherHpModel();

                        t.hpCode = row["HPCODE"].ToString();
                        t.opt = row["OPT"].ToString();
                        t.frontFacingOptDesc = row["FRONTFACINGOPTDESC"].ToString();
                        t.prefix = row["MEMBERPREFIX"].ToString();
                        t.hpName = row["HPNAME"].ToString();
                        t.lobCode = row["LOBCODE"].ToString();

                        list.Add(t);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("IdentifyVoucher")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public DefaultCodeModel IdentifyVoucher([FromBody] DefaultCodeModel data)
        {            
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@voucherId", data.id.Trim()));
                    cmd.CommandText =
                        $" SELECT HPCODE, OPT, MEMBERPREFIX from {PamcPortalDatabase}.[dbo].[VOUCHER_MASTERS] VM " +
                        $" INNER JOIN {MediDatabase}.[dbo].[SVC_CODES_SETUP_HEADER] H ON (VM.BASKETID = H.ID) " +
                        $" WHERE VM.VOUCHERID = @voucherId ";
                      
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    cn.Close();
                    cmd.Dispose();

                    if(dt.Rows.Count > 0)
                    {
                        data.hpCode = dt.Rows[0]["HPCODE"].ToString();
                        data.option = dt.Rows[0]["OPT"].ToString();
                        data.prefix = dt.Rows[0]["MEMBERPREFIX"].ToString();
                    }
                    else
                    {
                        data.hpCode = "PAMC";
                        data.option = "MEDIVOUCH";
                        data.prefix = "MD";
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("ValidateMedivouchVoucher")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ValidationResultsModel> ValidateVoucher([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.valid = false;
            vr.message = "";
            try
            {
                OperationLog Operationlog = new OperationLog();
                Operationlog.LogMedivouchValidate("Validate Medivouch voucher", data.user, data.provid, _portalConnectionString);

                //bool dispens = false;
                //bool scritp = false;
                //DataTable d = new DataTable();

                //using (SqlConnection c = new SqlConnection(_drcConnectionString))
                //{
                //    if (c.State != ConnectionState.Open)
                //    {
                //        c.Open();
                //    }

                //    SqlCommand cm = new SqlCommand();
                //    cm.Connection = c;

                //    cm.CommandText = $"SELECT SP.SPECCODE,pm.CLASS FROM {DRCDatabase}..PROV_SPECINFO SP \r\n" +
                //        $"INNER JOIN {DRCDatabase}..PROV_MASTERS pm on pm.PROVID = sp.PROVID\r\n" +
                //        $" WHERE SP.PROVID = '{data.provid}'";

                //    d.Load(cm.ExecuteReader());

                //    if ((d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15") && (d.Rows[0][1].ToString() == "6"))
                //    {
                //        data.merchantID = "12";
                //        scritp = false;
                //        dispens = true;
                //    }
                //    else if ((d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15") && (d.Rows[0][1].ToString() == "7"))
                //    {
                //        data.merchantID = "11";
                //        scritp = true;
                //        dispens = false;
                //    }
                //    else
                //    {
                //        data.merchantID = "13";
                //    }

                //    // Determine API key to use
                //    if (d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15")
                //    {
                //        pamc_drc = "PAMC";
                //    }
                //    else if (d.Rows[0][0].ToString() == "54")
                //    {
                //        pamc_drc = "DRC";
                //    }
                //}

                // Determine merchant ID
                if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "6")) //Dispense
                {
                    data.merchantID = "12";
                }
                else if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "7")) //Script
                {
                    data.merchantID = "11";
                }
                else
                {
                    data.merchantID = "13";
                }

                // Determine PAMC/DRC
                string pamc_drc = "";
                if (data.provSpec == "54" || data.provSpec == "95")
                {
                    pamc_drc = "DRC";
                }
                else
                {
                    pamc_drc = "PAMC";
                }

                // Get API key and Uri to use
                string apiKey = "";
                string uri = "";
                SqlConnection con = new SqlConnection(_portalConnectionString);
                if (con.State != ConnectionState.Open) con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = $" SELECT description FROM {PamcPortalDatabase}..Settings where settingsId = 'MediVouch{pamc_drc}ApiKey' " +
                                  $" UNION ALL " +
                                  $" SELECT description FROM {PamcPortalDatabase}..Settings where settingsId = 'MediVouch{pamc_drc}Uri' ";

                DataTable dtApi = new DataTable();
                dtApi.Load(cmd.ExecuteReader());
                con.Close();

                if (dtApi.Rows.Count < 2)
                {
                    vr.valid = false;
                    vr.message = "Could not get Api key and/or Uri from settings.";
                }
                else
                {
                    apiKey = dtApi.Rows[0]["Description"].ToString();
                    uri = dtApi.Rows[1]["Description"].ToString();

                    // Test api
                    //uri = "https://vouch-api-stage.vouch.services/"; //Test uri
                    //if (pamc_drc == "DRC")
                    //{
                    //    apiKey = ""; //Test key
                    //}
                    //else
                    //{
                    //    apiKey = "v7ox0vMCLO1ciZClr75qv03uDm14fUuctbenOvOu5Rs"; //Test key
                    //}

                    /* HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("https://vouch-api-stage.azurewebsites.net/");
                    client.DefaultRequestHeaders.Add("x-api-key", "v7ox0vMCLO1ciZClr75qv03uDm14fUuctbenOvOu5Rs");
                    //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    // var content = "";
                    var body = "{ \"externalBranchId\": \"" + data.provid + "\", \"merchantBranchId\": null, \"voucherCode\": \"" + data.id + "\"}";
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    var result = await client.PostAsync("v1/voucher/validate", content);*/

                    HttpClient client = new HttpClient();
                    //client.BaseAddress = new Uri("https://vouch-api.vouch.services/");  //live
                    //client.BaseAddress = new Uri("https://vouch-api.vouch.services/");  //test
                    client.BaseAddress = new Uri(uri);
                    client.DefaultRequestHeaders
                          .Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

                    // Set API key
                    //string apiKey = "";
                    //if (pamc_drc == "PAMC")
                    //{
                    //    apiKey = "k76WSwqVUEzjL9iThzm2QCh7HnCF2jvX6qd56JWSKhA";
                    //}
                    //else if (pamc_drc == "DRC")
                    //{
                    //    apiKey = "7OdRnX38Klvxi2QcLvKeWdAXRYdeUaeoL0PMzmoox0Z";
                    //}

                    client.DefaultRequestHeaders.Add("x-api-key", apiKey);

                    //var body = "{ \"externalBranchId\": null, \"merchantBranchId\": " + data.merchantID + ", \"voucherCode\": \"" + data.id + "\"}";
                    var body = "{ \"externalBranchId\": \"" + data.provid + "\", \"merchantBranchId\": null, \"voucherCode\": \"" + data.id + "\"}";


                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "v1/voucher/validate");
                    request.Content = new StringContent(body,
                                                        Encoding.UTF8
                                                        );//CONTENT-TYPE header
                    request.Content.Headers.Remove("Content-Type");
                    request.Content.Headers.Add("Content-Type", "application/json");

                    await client.SendAsync(request)
                             .ContinueWith(responseTask =>
                             {
                                 var r = responseTask.Result;
                                 var c = responseTask.Result.Content.ReadAsStringAsync();
                                 var d = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(c.Result);

                                 // Log Api input and response                             
                                 LogApiCall(data.id, data.provid, pamc_drc, "VALIDATE", apiKey, body.ToString(), d.ToString());

                                 if (r.IsSuccessStatusCode)
                                 {
                                     vr.valid = Convert.ToBoolean(d.valid.Value);
                                     //vr.valid = true; //Test
                                     if (!vr.valid)
                                     {
                                         vr.message = "Voucher validation failed (status = OK but success = false)";
                                     }
                                     else
                                     {
                                         vr.message = Convert.ToString(d.voucher.voucherId.Value);

                                         con = new SqlConnection(_mediConnectionString);
                                         if (con.State != ConnectionState.Open) con.Open();
                                         cmd = new SqlCommand();
                                         cmd.Connection = con;

                                         cmd.Parameters.Add(new SqlParameter("@LISTINGID", Convert.ToString(d.voucher.voucherListingId.Value)));
                                         //cmd.Parameters.Add(new SqlParameter("@LISTINGID", "16")); //Test
                                         //cmd.Parameters.Add(new SqlParameter("@DISPENS", dispens));
                                         //cmd.Parameters.Add(new SqlParameter("@SCRIPT", scritp));

                                         //cmd.CommandText = $"SELECT id FROM {base.MediDatabase}.dbo.[SVC_CODES_SETUP_HEADER] where ExternalCode = @LISTINGID AND [Despensing] = @DISPENS AND [Scripting] = @SCRIPT";
                                         cmd.CommandText = $" SELECT * FROM {base.MediDatabase}.dbo.[SVC_CODES_SETUP_HEADER] where ExternalCode = @LISTINGID AND HpCode = '{data.hpCode.Trim()}' and Opt = '{data.option.Trim()}' and Enabled = 1 ";
                                         //$" Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and HpCode = '{data.hpCode.Trim()}' and Enabled = 1 ";
                                         DataTable dtBasket = new DataTable();
                                         dtBasket.Load(cmd.ExecuteReader());
                                         con.Close();

                                         if (dtBasket.Rows[0]["Spec"].ToString().Contains($"'{data.provSpec}'") &&
                                               dtBasket.Rows[0]["Class"].ToString().Contains($"'{data.provClass}'"))
                                         {
                                             vr.message = dtBasket.Rows[0]["id"].ToString();
                                         }
                                         else
                                         {
                                             vr.valid = false;
                                             vr.message = "This voucher can not be redeemed at this practice - incorrect speciality or class.";
                                         }
                                     }
                                 }
                                 else
                                 {
                                     vr.valid = false;

                                     try
                                     {
                                         vr.message = d.message;
                                     }
                                     catch
                                     {
                                         try
                                         {
                                             vr.message = d.errorMessage;
                                         }
                                         catch
                                         {

                                         }
                                     }
                                 }
                             });


                    /*REMOVE COMMENTS FROM BELOW LINES TO SKIP MEDIVOUCH VALIDATION for test*/
                    //if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "6")) //Dispensing
                    //{
                    //    vr.valid = true;
                    //    vr.message = "9"; // GP Dispense basket
                    //}
                    //else if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "7")) //Scripting
                    //{
                    //    vr.valid = true;
                    //    vr.message = "7"; // GP Script basket
                    //}
                    //else
                    //{
                    //    vr.valid = true;
                    //    vr.message = "2"; //Dental basket
                    //}

                }

                if (vr.valid)
                {
                    EventLoggingModel c = new EventLoggingModel();
                    c.amount = "0.00";
                    c.eventDesc = $"Voucher Validation succeeded {data.id} basket {vr.message}";
                    c.provid = data.provid;
                    c.member = "";
                    c.username = data.provid;
                    c.uuid = System.Guid.NewGuid().ToString();
                    LogEvent(c);
                }
                else
                {
                    EventLoggingModel c = new EventLoggingModel();
                    c.amount = "0.00";
                    c.eventDesc = $"Voucher Validation failed {data.id}";
                    c.provid = data.provid;
                    c.member = "";
                    c.username = data.provid;
                    c.uuid = System.Guid.NewGuid().ToString();
                    LogEvent(c);
                }

            }
            catch (Exception e)
            {
                // Send Error Email
                string paramsStr = $"\r\n\r\npublic string heading: {data.heading}\r\n" +
                                    $"public string membName: {data.membName}\r\n" +
                                    $"public string membSurname: {data.membSurname}\r\n" +
                                    $"public string membIDNo: {data.membIDNo}\r\n" +
                                    $"public string provid: {data.provid}\r\n" +
                                    $"public string user: {data.user}\r\n\r\n" +
                                    $"public string id: {data.id}\r\n" +
                                    $"public bool enabled: {data.enabled}\r\n" +
                                    $"public string code: {data.code}\r\n" +
                                    $"public string phcode: {data.phcode}\r\n" +
                                    $"public string desc: {data.desc}\r\n" +
                                    $"public string headDesc: {data.headDesc}\r\n" +
                                    $"public bool enterQty: {data.enterQty}\r\n" +
                                    $"public int defaultQty: {data.defaultQty}\r\n" +
                                    $"public bool enterTooth: {data.enterTooth}\r\n" +
                                    $"public bool dependantOnCode: {data.dependantOnCode}\r\n" +
                                    $"public string depCode: {data.depCode}\r\n" +
                                    $"public string price: {data.price}\r\n" +
                                    $"public string total: {data.total}\r\n" +
                                    $"public string membContact: {data.membContact}\r\n" +
                                    $"public string toothNo: {data.toothNo}\r\n" +
                                    $"public string merchantID: {data.merchantID}\r\n";

                string url = _EmailWebSvcConnection;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;
                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                
                //var c = client.SendEmailAsync("MEDIVOUCH VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: MEDIVOUCH VALIDATION ERROR", "jaco@pamc.co.za");

                if(_isTest)
                {
                    var c = client.SendEmailAsync("MEDIVOUCH VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL TEST: MEDIVOUCH VALIDATION ERROR", _testEmails);
                }
                else
                {
                    var c = client.SendEmailAsync("MEDIVOUCH VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: MEDIVOUCH VALIDATION ERROR", "jaco@pamc.co.za");
                }

                // Log error in text file
                LogError(e.Message, e.StackTrace);
            }

            if (vr.message.Trim() == "")
            {
                vr.message = "Unexpected error occured";
            }

            return vr;
        }

        [HttpPost]
        [Route("RedeemMedivouchVoucher")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ValidationResultsModel> RedeemVoucher([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.valid = false;
            vr.message = "";
            try
            {
                OperationLog Operationlog = new OperationLog();
                Operationlog.LogMedivouchRedeem("Redeem Medivouch voucher", data.user, data.provid, data.prefix + data.membIDNo + "-00", _portalConnectionString);
                //string pamc_drc = "";

                //using (SqlConnection c = new SqlConnection(_drcConnectionString))
                //{
                //    if (c.State != ConnectionState.Open)
                //    {
                //        c.Open();
                //    }

                //    SqlCommand cm = new SqlCommand();
                //    cm.Connection = c;

                //    cm.CommandText = $"SELECT SP.SPECCODE,pm.CLASS FROM {DRCDatabase}..PROV_SPECINFO SP \r\n" +
                //        $"INNER JOIN {DRCDatabase}..PROV_MASTERS pm on pm.PROVID = sp.PROVID\r\n" +
                //        $" WHERE SP.PROVID = '{data.provid}'";

                //    DataTable d = new DataTable();
                //    d.Load(cm.ExecuteReader());

                //    if ((d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15") && (d.Rows[0][1].ToString() == "6"))
                //    {
                //        data.merchantID = "12";
                //    }
                //    else if ((d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15") && (d.Rows[0][1].ToString() == "7"))
                //    {
                //        data.merchantID = "11";
                //    }
                //    else
                //    {
                //        data.merchantID = "13";

                //    }

                //    // Determine API key to use
                //    if (d.Rows[0][0].ToString() == "14" || d.Rows[0][0].ToString() == "15")
                //    {
                //        pamc_drc = "PAMC";
                //    }
                //    else if (d.Rows[0][0].ToString() == "54")
                //    {
                //        pamc_drc = "DRC";
                //    }
                //}

                //string firstname = data.membName;
                //string lastname = data.membSurname;
                //string idNo = data.membIDNo;

                // Create/Update Member
                SqlConnection con = new SqlConnection(_drcConnectionString);
                if (con.State != ConnectionState.Open) con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;

                if (data.membIDNo != null && data.membIDNo.Length > 1)
                {
                    cmd.Parameters.Add(new SqlParameter("@MEMBID", data.prefix + data.membIDNo + "-00"));
                    DataTable dt = new DataTable();
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.MEMB_MASTERS WHERE MEMBID = @MEMBID";
                    dt.Load(cmd.ExecuteReader());
                    if (dt.Rows.Count > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@IDNO", data.membIDNo));
                        cmd.Parameters.Add(new SqlParameter("@FIRSTNM", data.membName));
                        cmd.Parameters.Add(new SqlParameter("@LASTNM", data.membSurname));
                        cmd.Parameters.Add(new SqlParameter("@CONTACT", data.membContact));
                        cmd.CommandText = $"UPDATE MEMB_MASTERS SET FIRSTNM = @FIRSTNM,LASTNM = @LASTNM,CELL=@CONTACT WHERE PATID = @IDNO AND HPCODE = 'PAMC' AND OPT = 'MEDIVOUCH'";
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@IDNO", data.membIDNo));
                        cmd.Parameters.Add(new SqlParameter("@FIRSTNM", data.membName));
                        cmd.Parameters.Add(new SqlParameter("@LASTNM", data.membSurname));
                        cmd.Parameters.Add(new SqlParameter("@CONTACT", data.membContact));
                        cmd.Parameters.Add(new SqlParameter("@PREFIX", data.prefix));
                        cmd.Parameters.Add(new SqlParameter("@HPCODE", data.hpCode));
                        cmd.Parameters.Add(new SqlParameter("@OPTION", data.option));

                        int day = 0;
                        int month = 0;
                        int year = 0;

                        string prefix = "19";

                        int curentYear = DateTime.Now.Year % 100;

                        year = Convert.ToInt32(data.membIDNo.Substring(0, 2));
                        month = Convert.ToInt32(data.membIDNo.Substring(2, 2));
                        day = Convert.ToInt32(data.membIDNo.Substring(4, 2));

                        if (year < curentYear)
                        {
                            prefix = "20";
                        }

                        string dob = $"{prefix}{year.ToString().PadLeft(2, '0')}/{month.ToString().PadLeft(2, '0')}/{day.ToString().PadLeft(2, '0')}";

                        cmd.Parameters.Add(new SqlParameter("@BIRTH", dob));

                        //cmd.CommandText = $"EXEC {DRCDatabase}.dbo.CreateMediVouchMemb @IDNO,@LASTNM,@FIRSTNM,@BIRTH,@CONTACT ";
                        cmd.CommandText = $"EXEC {DRCDatabase}.dbo.CreateVoucherMemb @IDNO, @LASTNM, @FIRSTNM, @BIRTH, @CONTACT, @PREFIX, @HPCODE, @OPTION ";

                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    vr.valid = false;
                    vr.message = "Patient Id number required";
                    return vr;
                }
                con.Close();

                // Determine merchant ID
                if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "6")) // Dispense
                {
                    data.merchantID = "12";
                }
                else if ((data.provSpec == "14" || data.provSpec == "15") && (data.provClass == "7")) //Script
                {
                    data.merchantID = "11";
                }
                else
                {
                    data.merchantID = "13";
                }

                // Determine PAMC/DRC
                string pamc_drc = "";
                if (data.provSpec == "54" || data.provSpec == "95")
                {
                    pamc_drc = "DRC";
                }
                else
                {
                    pamc_drc = "PAMC";
                }

                // Get API key and Uri to use
                string apiKey = "";
                string uri = "";
                con = new SqlConnection(_portalConnectionString);
                if (con.State != ConnectionState.Open) con.Open();
                cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = $" SELECT description FROM {PamcPortalDatabase}..Settings where settingsId = 'MediVouch{pamc_drc}ApiKey' " +
                                  $" UNION ALL " +
                                  $" SELECT description FROM {PamcPortalDatabase}..Settings where settingsId = 'MediVouch{pamc_drc}Uri' ";

                DataTable dtApi = new DataTable();
                dtApi.Load(cmd.ExecuteReader());
                con.Close();
                con.Dispose();
                cmd.Dispose();

                if (dtApi.Rows.Count < 2)
                {
                    vr.valid = false;
                    vr.message = "Could not get Api key and/or Uri from settings.";
                }
                else
                {
                    apiKey = dtApi.Rows[0]["Description"].ToString();
                    uri = dtApi.Rows[1]["Description"].ToString();

                    // Test api
                    //uri = "https://vouch-api-stage.vouch.services/"; //Test uri
                    //if (pamc_drc == "DRC")
                    //{
                    //    apiKey = ""; //Test key
                    //}
                    //else
                    //{
                    //    apiKey = "v7ox0vMCLO1ciZClr75qv03uDm14fUuctbenOvOu5Rs"; //Test key
                    //}

                    /* HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("https://vouch-api-stage.azurewebsites.net/");
                    client.DefaultRequestHeaders.Add("x-api-key", "v7ox0vMCLO1ciZClr75qv03uDm14fUuctbenOvOu5Rs");
                    //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                    // var content = "";
                    var body = "{ \"externalBranchId\": \"" + data.provid + "\", \"merchantBranchId\": null, \"voucherCode\": \"" + data.id + "\"}";
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                    var result = await client.PostAsync("v1/voucher/validate", content);*/

                    HttpClient client = new HttpClient();
                    //client.BaseAddress = new Uri("https://vouch-api.vouch.services/"); //live
                    //client.BaseAddress = new Uri("https://vouch-api.vouch.services/"); //test
                    client.BaseAddress = new Uri(uri);
                    client.DefaultRequestHeaders
                          .Accept
                          .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header


                    //// Set API key
                    //string apiKey = "";

                    //if (pamc_drc == "PAMC")
                    //{
                    //    apiKey = "k76WSwqVUEzjL9iThzm2QCh7HnCF2jvX6qd56JWSKhA";
                    //}
                    //else if (pamc_drc == "DRC")
                    //{
                    //    apiKey = "7OdRnX38Klvxi2QcLvKeWdAXRYdeUaeoL0PMzmoox0Z";
                    //}

                    client.DefaultRequestHeaders.Add("x-api-key", apiKey);

                    //var body = "{ \"externalBranchId\": null, \"merchantBranchId\": "+data.merchantID + ", \"voucherCode\": \"" + data.id.Substring(0, data.id.IndexOf("_")) + "\"}";
                    var body = "{ \"externalBranchId\": \"" + data.provid + "\", \"merchantBranchId\": null, \"voucherCode\": \"" + data.id.Substring(0, data.id.IndexOf("_")) + "\"}";


                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "v1/voucher/redeem");
                    request.Content = new StringContent(body,
                                                        Encoding.UTF8
                                                        );//CONTENT-TYPE header
                    request.Content.Headers.Remove("Content-Type");
                    request.Content.Headers.Add("Content-Type", "application/json");

                    await client.SendAsync(request)
                             .ContinueWith(responseTask =>
                             {
                                 var r = responseTask.Result;
                                 var c = responseTask.Result.Content.ReadAsStringAsync();
                                 var d = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(c.Result);

                                 // Log Api input and response                             
                                 LogApiCall(data.id.Substring(0, data.id.IndexOf("_")), data.provid, pamc_drc, "REDEEM", apiKey, body.ToString(), d.ToString());

                                 if (r.IsSuccessStatusCode)
                                 {
                                     if (d.success == "false")
                                     {
                                         vr.valid = false;

                                         try
                                         {
                                             vr.message = d.errorMessage;
                                         }
                                         catch
                                         {
                                             try
                                             {
                                                 vr.message = d.message;
                                             }
                                             catch
                                             {

                                             }
                                         }
                                     }

                                     if (d.success == "true")
                                     {
                                         vr.valid = true;

                                         try
                                         {
                                             vr.message = d.voucher.firstname + "," + d.voucher.lastname;
                                         }
                                         catch
                                         {
                                             try
                                             {
                                                 vr.message = d.voucher.firstName + "," + d.voucher.lastName;
                                             }
                                             catch
                                             {

                                             }
                                         }
                                     }
                                 }
                                 else
                                 {
                                     vr.valid = false;

                                     try
                                     {
                                         vr.message = d.message;
                                     }
                                     catch
                                     {
                                         try
                                         {
                                             vr.message = d.errorMessage;
                                         }
                                         catch
                                         {

                                         }
                                     }
                                 }
                             });
                }


                /*REMOVE COMMENTS FROM BELOW LINES TO SKIP MEDIVOUCH REDEMTION TEST*/
                //vr.valid = true;

                if (vr.valid)
                {
                    EventLoggingModel c = new EventLoggingModel();
                    c.amount = "0.00";
                    c.eventDesc = $"Voucher redemption succeeded {data.id.Substring(0, data.id.Length - 2)} basket {data.id.Substring(data.id.Length - 1, 1)}";
                    c.provid = data.provid;
                    if (data.membIDNo != null && data.membIDNo.Length > 1)
                    {
                        c.member = data.membIDNo + "-" + "00";
                    }
                    else
                    {
                        c.member = "";
                    }
                    c.username = data.provid;
                    c.uuid = System.Guid.NewGuid().ToString();
                    LogEvent(c);
                }
                else
                {
                    EventLoggingModel c = new EventLoggingModel();
                    c.amount = "0.00";
                    c.eventDesc = $"Voucher redemption failed {data.id.Substring(0, data.id.Length - 2)}";
                    c.provid = data.provid;
                    c.member = "";
                    c.username = data.provid;
                    c.uuid = System.Guid.NewGuid().ToString();
                    LogEvent(c);
                }
            }
            catch (Exception e)
            {
                // Send Error Email
                string paramsStr = $"\r\n\r\npublic string heading: {data.heading}\r\n" +
                                    $"public string membName: {data.membName}\r\n" +
                                    $"public string membSurname: {data.membSurname}\r\n" +
                                    $"public string membIDNo: {data.membIDNo}\r\n" +
                                    $"public string provid: {data.provid}\r\n" +
                                    $"public string user: {data.user}\r\n\r\n" +
                                    $"public string id: {data.id}\r\n" +
                                    $"public bool enabled: {data.enabled}\r\n" +
                                    $"public string code: {data.code}\r\n" +
                                    $"public string phcode: {data.phcode}\r\n" +
                                    $"public string desc: {data.desc}\r\n" +
                                    $"public string headDesc: {data.headDesc}\r\n" +
                                    $"public bool enterQty: {data.enterQty}\r\n" +
                                    $"public int defaultQty: {data.defaultQty}\r\n" +
                                    $"public bool enterTooth: {data.enterTooth}\r\n" +
                                    $"public bool dependantOnCode: {data.dependantOnCode}\r\n" +
                                    $"public string depCode: {data.depCode}\r\n" +
                                    $"public string price: {data.price}\r\n" +
                                    $"public string total: {data.total}\r\n" +
                                    $"public string membContact: {data.membContact}\r\n" +
                                    $"public string toothNo: {data.toothNo}\r\n" +
                                    $"public string merchantID: {data.merchantID}\r\n";

                string url = _EmailWebSvcConnection;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;
                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                
                //var c = client.SendEmailAsync("MEDIVOUCH REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: MEDIVOUCH REDEEM ERROR", "jaco@pamc.co.za");

                if(_isTest)
                {
                    var c = client.SendEmailAsync("MEDIVOUCH REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL TEST: MEDIVOUCH REDEEM ERROR", _testEmails);
                }
                else
                {
                    var c = client.SendEmailAsync("MEDIVOUCH REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: MEDIVOUCH REDEEM ERROR", "jaco@pamc.co.za");
                }

                // Log error in text file
                LogError(e.Message, e.StackTrace);
            }

            if (vr.message.Trim() == "")
            {
                vr.message = "Unexpected error occured";
            }

            return vr;
        }

        [HttpPost]
        [Route("ValidateInternalVoucher")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ValidationResultsModel> ValidateInternalVoucher([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.valid = false;
            vr.message = "";

            try
            {
                // Log
                OperationLog Operationlog = new OperationLog();
                Operationlog.LogMedivouchValidate("Validate Interenal voucher", data.user, data.provid, _portalConnectionString);

                //// Get spec
                //bool dispens = false;
                //bool scritp = false;
                //string provSpec = "";
                //DataTable d = new DataTable();
                //string pamc_drc = "";
                //using (SqlConnection c = new SqlConnection(_drcConnectionString))
                //{
                //    if (c.State != ConnectionState.Open)
                //    {
                //        c.Open();
                //    }

                //    SqlCommand cm = new SqlCommand();
                //    cm.Connection = c;

                //    cm.CommandText = $"SELECT SP.SPECCODE,pm.CLASS FROM {DRCDatabase}..PROV_SPECINFO SP \r\n" +
                //        $"INNER JOIN {DRCDatabase}..PROV_MASTERS pm on pm.PROVID = sp.PROVID\r\n" +
                //        $" WHERE SP.PROVID = '{data.provid}'";

                //    d.Load(cm.ExecuteReader());

                //    provSpec = d.Rows[0]["SPECCODE"].ToString();

                //    if ((d.Rows[0]["SPECCODE"].ToString() == "14" || d.Rows[0]["SPECCODE"].ToString() == "15" || d.Rows[0]["SPECCODE"].ToString() == "88") && (d.Rows[0]["CLASS"].ToString() == "6"))
                //    {
                //        //data.merchantID = "12";
                //        scritp = false;
                //        dispens = true;
                //    }
                //    else if ((d.Rows[0]["SPECCODE"].ToString() == "14" || d.Rows[0]["SPECCODE"].ToString() == "15" || d.Rows[0]["SPECCODE"].ToString() == "88") && (d.Rows[0]["CLASS"].ToString() == "7"))
                //    {
                //        //data.merchantID = "11";
                //        scritp = true;
                //        dispens = false;
                //    }
                //    else
                //    {
                //        data.merchantID = "13";
                //    }
                //}

                // Validate voucher
                string query = $" SELECT * FROM {PamcPortalDatabase}.dbo.VOUCHER_MASTERS WHERE VOUCHERID = @voucherId ";
                SqlConnection cn = new SqlConnection(_portalConnectionString); 
                SqlCommand cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(new SqlParameter("@voucherId", data.id));
                DataTable datatable = new DataTable();
                using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                {
                    adpt.Fill(datatable);
                }
                cn.Close();

                if (datatable.Rows.Count < 1)
                {
                    vr.valid = false;
                    vr.message = "Voucher does not exist - please ensure Voucher ID is entered correctly.";
                }
                else
                {
                    if (datatable.Rows[0]["VOUCHERID"].ToString().Trim() == "")
                    {
                        vr.valid = false;
                        vr.message = "Voucher does not exist - please ensure Voucher ID is entered correctly.";
                    }
                    else
                    {
                        if (datatable.Rows[0]["REDEEMED"].ToString().Trim() == "True")
                        {
                            vr.valid = false;
                            vr.message = "Voucher already redeemed.";
                        }
                        else
                        {
                            // Update voucher masters validation fields
                            if (cn.State != ConnectionState.Open) cn.Open();
                            cmd = new SqlCommand();
                            cmd.Connection = cn;
                            cmd.Parameters.Add(new SqlParameter("@voucherId", datatable.Rows[0]["VOUCHERID"].ToString().Trim()));
                            cmd.Parameters.Add(new SqlParameter("@username", data.user));
                            cmd.CommandText = $" UPDATE {PamcPortalDatabase}.dbo.VOUCHER_MASTERS \n " +
                                              $" SET VALIDATEDBY = @username, LASTVALIDATED = GETDATE() \n " +
                                              $" WHERE VOUCHERID = @voucherId ";
                            cmd.ExecuteScalar();

                            // Do validation
                            query = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE id = @basketId and HpCode = @hpCode and Opt = @opt ";
                            cmd = new SqlCommand(query, cn);
                            cmd.Parameters.Add(new SqlParameter("@basketId", datatable.Rows[0]["BASKETID"].ToString()));
                            cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode));
                            cmd.Parameters.Add(new SqlParameter("@opt", data.option));
                            DataTable dtBasket = new DataTable();
                            using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                            {
                                adpt.Fill(dtBasket);
                            }

                            //if (dtBasket.Rows[0]["Dentist"].ToString() == "true" && !(data.provSpec == "54" || data.provSpec == "95"))
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can only be redeemed by a Dentist";
                            //}
                            //else if (dtBasket.Rows[0]["GP"].ToString() == "true" && (data.provSpec == "54" || data.provSpec == "95"))
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can not be redeemed by a Dentist";
                            //}
                            //else if ( dtBasket.Rows[0]["Nurse"].ToString() == "true" && !(provSpec == "88") )
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can only be redeemed by a Registered Nurse";
                            //}
                            if (dtBasket.Rows.Count < 1)
                            {
                                vr.valid = false;
                                vr.message = "No basket found - incorrect setup.";
                            }
                            else
                            {
                                if (dtBasket.Rows[0]["Enabled"].ToString() == "False")
                                {
                                    vr.valid = false;

                                    if (data.provSpec == "54" || data.provSpec == "95")
                                    {
                                        vr.message = "This type of voucher has been disabled - Please contact DRC";
                                    }
                                    else
                                    {
                                        vr.message = "This type of voucher has been disabled - Please contact PAMC";
                                    }
                                }
                                else if (dtBasket.Rows[0]["Spec"].ToString().Contains($"'{data.provSpec}'") &&
                                          dtBasket.Rows[0]["Class"].ToString().Contains($"'{data.provClass}'"))
                                {
                                    vr.valid = true;
                                    vr.message = datatable.Rows[0]["BASKETID"].ToString();
                                }
                                else
                                {
                                    vr.valid = false;
                                    vr.message = "This voucher can not be redeemed at this practice - incorrect speciality or class.";
                                }
                            }                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Send Error Email
                string paramsStr = $"\r\n\r\npublic string heading: {data.heading}\r\n" +
                                    $"public string membName: {data.membName}\r\n" +
                                    $"public string membSurname: {data.membSurname}\r\n" +
                                    $"public string membIDNo: {data.membIDNo}\r\n" +
                                    $"public string provid: {data.provid}\r\n" +
                                    $"public string user: {data.user}\r\n\r\n" +
                                    $"public string id: {data.id}\r\n" +
                                    $"public bool enabled: {data.enabled}\r\n" +
                                    $"public string code: {data.code}\r\n" +
                                    $"public string phcode: {data.phcode}\r\n" +
                                    $"public string desc: {data.desc}\r\n" +
                                    $"public string headDesc: {data.headDesc}\r\n" +
                                    $"public bool enterQty: {data.enterQty}\r\n" +
                                    $"public int defaultQty: {data.defaultQty}\r\n" +
                                    $"public bool enterTooth: {data.enterTooth}\r\n" +
                                    $"public bool dependantOnCode: {data.dependantOnCode}\r\n" +
                                    $"public string depCode: {data.depCode}\r\n" +
                                    $"public string price: {data.price}\r\n" +
                                    $"public string total: {data.total}\r\n" +
                                    $"public string membContact: {data.membContact}\r\n" +
                                    $"public string toothNo: {data.toothNo}\r\n" +
                                    $"public string merchantID: {data.merchantID}\r\n";

                string url = _EmailWebSvcConnection;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;
                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                
                //var c = client.SendEmailAsync("INTERNAL VOUCHER VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: INTERNAL VOUCHER VALIDATION ERROR", "jaco@pamc.co.za");

                if(_isTest)
                {
                    var c = client.SendEmailAsync("INTERNAL VOUCHER VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL TEST: INTERNAL VOUCHER VALIDATION ERROR", _testEmails);
                }
                else
                {
                    var c = client.SendEmailAsync("INTERNAL VOUCHER VALIDATION ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: INTERNAL VOUCHER VALIDATION ERROR", "jaco@pamc.co.za");
                }

                // Log error in text file
                LogError(e.Message, e.StackTrace);
            }

            if (vr.message.Trim() == "")
            {
                vr.message = "Unexpected error occured";
            }

            return vr;
        }

        [HttpPost]
        [Route("RedeemInternalVoucher")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ValidationResultsModel> RedeemInternalVoucher([FromBody] DefaultCodeModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.valid = false;
            vr.message = "";

            try
            {
                // Log
                OperationLog Operationlog = new OperationLog();
                Operationlog.LogMedivouchRedeem("Redeem Internal voucher", data.user, data.provid, data.prefix + data.membIDNo + "-" + data.depCode, _portalConnectionString);

                // Validate voucher                
                string query = $" SELECT * FROM {PamcPortalDatabase}.dbo.VOUCHER_MASTERS WHERE VOUCHERID = @voucherId ";
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                SqlCommand cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(new SqlParameter("@voucherId", data.id.Substring(0, data.id.IndexOf("_"))));
                DataTable dtVoucher = new DataTable();
                using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                {
                    adpt.Fill(dtVoucher);
                }
                cn.Close();

                if (dtVoucher.Rows.Count < 1)
                {
                    vr.valid = false;
                    vr.message = "Voucher does not exist - please ensure Voucher ID is entered correctly.";
                }
                else
                {
                    if (dtVoucher.Rows[0]["VOUCHERID"].ToString().Trim() == "")
                    {
                        vr.valid = false;
                        vr.message = "Voucher does not exist - please ensure Voucher ID is entered correctly.";
                    }
                    else
                    {
                        if (dtVoucher.Rows[0]["REDEEMED"].ToString().Trim() == "True")
                        {
                            vr.valid = false;
                            vr.message = "Voucher already redeemed.";
                        }
                        else
                        {
                            //// Get spec and class
                            //query = $" SELECT SP.SPECCODE,pm.CLASS FROM {DRCDatabase}..PROV_SPECINFO SP \r\n" +
                            //        $" INNER JOIN {DRCDatabase}..PROV_MASTERS pm on pm.PROVID = sp.PROVID \r\n" +
                            //        $" WHERE SP.PROVID = @provId ";

                            //cmd = new SqlCommand(query, cn);
                            //cmd.Parameters.Add(new SqlParameter("@provId", data.provid));
                            //DataTable dtSpecClass = new DataTable();
                            //using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                            //{
                            //    adpt.Fill(dtSpecClass);
                            //}
                            //cmd = null;

                            //if (dtSpecClass.Rows.Count < 1)
                            //{
                            //    vr.valid = false;
                            //    vr.message = "No Provider Spec or Class";
                            //}
                            //else
                            //{
                            //if (dtSpecClass.Rows[0]["SPECCODE"].ToString().Trim() == "" || dtSpecClass.Rows[0]["CLASS"].ToString().Trim() == "")
                            //{
                            //    vr.valid = false;
                            //    vr.message = "No Provider Spec or Class";
                            //}
                            //else
                            //{
                            //    string provSpec = dtSpecClass.Rows[0]["SPECCODE"].ToString().Trim();
                            //    string provClass = dtSpecClass.Rows[0]["CLASS"].ToString().Trim();

                            query = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE id = @basketId and HpCode = @hpCode and Opt = @opt ";
                            cmd = new SqlCommand(query, cn);
                            cmd.Parameters.Add(new SqlParameter("@basketId", dtVoucher.Rows[0]["BASKETID"].ToString()));
                            cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode));
                            cmd.Parameters.Add(new SqlParameter("@opt", data.option));
                            DataTable dtBasket = new DataTable();
                            using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                            {
                                adpt.Fill(dtBasket);
                            }
                            cn.Close();

                            //if (dtBasket.Rows[0]["Dentist"].ToString() == "True" && !(provSpec == "54" || provSpec == "95"))
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can only be redeemed by a Dentist";
                            //}
                            //else if (dtBasket.Rows[0]["GP"].ToString() == "True" && (provSpec == "14" || provSpec == "15"))
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can not be redeemed by a GP";
                            //}
                            //else if ( dtBasket.Rows[0]["Nurse"].ToString() == "true" && !(provSpec == "88") )
                            //{
                            //    vr.valid = false;
                            //    vr.message = "This voucher can only be redeemed by a Registered Nurse";
                            //}

                            if (dtBasket.Rows.Count < 0)
                            {
                                vr.valid = false;
                                vr.message = "No Basket found - incorrect setup";
                            }
                            else
                            {
                                if (dtBasket.Rows[0]["Enabled"].ToString() == "False")
                                {
                                    vr.valid = false;

                                    if (data.provSpec == "54" || data.provSpec == "95")
                                    {
                                        vr.message = "This type of voucher has been disabled - Please contact DRC";
                                    }
                                    else
                                    {
                                        vr.message = "This type of voucher has been disabled - Please contact PAMC";
                                    }
                                }
                                else if (!dtBasket.Rows[0]["Spec"].ToString().Contains($"'{data.provSpec}'") ||
                                          !dtBasket.Rows[0]["Class"].ToString().Contains($"'{data.provClass}'"))
                                {
                                    vr.valid = false;
                                    vr.message = "This voucher can not be redeemed at this practice - incorrect speciality or class.";
                                }
                                else if (data.membIDNo == null || data.membIDNo.Length < 1)
                                {
                                    vr.valid = false;
                                    vr.message = "No Member ID number entered";
                                }
                                else
                                {
                                    // Create or update memb
                                    cmd = new SqlCommand();
                                    cmd.Connection = cn;
                                    cmd.Parameters.Add(new SqlParameter("@MEMBID", data.prefix + data.membIDNo + "-" + data.depCode));
                                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.MEMB_MASTERS WHERE MEMBID = @MEMBID";
                                    DataTable dtMemb = new DataTable();
                                    using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                                    {
                                        adpt.Fill(dtMemb);
                                    }
                                    cn.Close();

                                    if (dtMemb.Rows.Count > 0)
                                    {
                                        // Don't update existing members Jaco 2022-07-28
                                        //query = $" UPDATE {DRCDatabase}.dbo.MEMB_MASTERS SET FIRSTNM = @FIRSTNM,LASTNM = @LASTNM,CELL=@CONTACT WHERE MEMBID = @MEMBID ";
                                        //cmd = new SqlCommand(query, cn);
                                        //cmd.Parameters.Add(new SqlParameter("@MEMBID", data.prefix + data.membIDNo + "-00"));
                                        //cmd.Parameters.Add(new SqlParameter("@FIRSTNM", data.membName));
                                        //cmd.Parameters.Add(new SqlParameter("@LASTNM", data.membSurname));
                                        //cmd.Parameters.Add(new SqlParameter("@CONTACT", data.membContact));

                                        //if (cn.State != ConnectionState.Open) cn.Open();
                                        //cmd.ExecuteNonQuery();
                                        //cn.Close();
                                    }
                                    else
                                    {
                                        query = $" EXEC {DRCDatabase}.dbo.CreateVoucherMemb @IDNO,@LASTNM,@FIRSTNM,@BIRTH,@CONTACT,@PREFIX,@HPCODE,@OPTION ";
                                        cmd = new SqlCommand(query, cn);
                                        cmd.Parameters.Add(new SqlParameter("@IDNO", data.membIDNo));
                                        cmd.Parameters.Add(new SqlParameter("@FIRSTNM", data.membName));
                                        cmd.Parameters.Add(new SqlParameter("@LASTNM", data.membSurname));
                                        cmd.Parameters.Add(new SqlParameter("@CONTACT", data.membContact));
                                        cmd.Parameters.Add(new SqlParameter("@PREFIX", data.prefix));
                                        cmd.Parameters.Add(new SqlParameter("@HPCODE", data.hpCode));
                                        cmd.Parameters.Add(new SqlParameter("@OPTION", data.option));

                                        string dob = "";

                                        // Try to get dob from ID no
                                        try
                                        {
                                            if (data.membIDNo.Length >= 6 && data.membIDNo.All(Char.IsDigit))
                                            {
                                                int day = 0;
                                                int month = 0;
                                                int year = 0;
                                                string century = "19";
                                                int curentYear = DateTime.Now.Year % 100;

                                                year = Convert.ToInt32(data.membIDNo.Substring(0, 2));
                                                month = Convert.ToInt32(data.membIDNo.Substring(2, 2));
                                                day = Convert.ToInt32(data.membIDNo.Substring(4, 2));
                                                if (year < curentYear)
                                                {
                                                    century = "20";
                                                }

                                                dob = $"{century}{year.ToString().PadLeft(2, '0')}/{month.ToString().PadLeft(2, '0')}/{day.ToString().PadLeft(2, '0')}";

                                                // Check if valid date                                  
                                                DateTime dateTime2;
                                                if (DateTime.TryParse(dob, out dateTime2))
                                                {
                                                    //Valida date
                                                }
                                                else
                                                {
                                                    dob = "1900/01/01";
                                                }
                                            }
                                            else
                                            {
                                                dob = "1900/01/01";
                                            }
                                        }
                                        catch (Exception Ex)
                                        {
                                            dob = "1900/01/01";
                                        }

                                        cmd.Parameters.Add(new SqlParameter("@BIRTH", dob));

                                        if (cn.State != ConnectionState.Open) cn.Open();
                                        cmd.ExecuteNonQuery();
                                        cn.Close();
                                    }

                                    // Update redeemed status
                                    query = $" UPDATE {PamcPortalDatabase}.dbo.VOUCHER_MASTERS \n" +
                                            $" SET REDEEMED = 1, REDEEMEDBY = @username, REDEEMEDDATE = GETDATE(), MEMBID = @membId \n " +
                                            $" WHERE VOUCHERID = @voucherId ";

                                    cmd = new SqlCommand(query, cn);
                                    cmd.Parameters.Add(new SqlParameter("@voucherId", dtVoucher.Rows[0]["VOUCHERID"].ToString().Trim()));
                                    cmd.Parameters.Add(new SqlParameter("@username", data.user));
                                    cmd.Parameters.Add(new SqlParameter("@membId", data.prefix + data.membIDNo + "-" + data.depCode));

                                    if (cn.State != ConnectionState.Open) cn.Open();
                                    cmd.ExecuteScalar();
                                    cn.Close();

                                    vr.valid = true;
                                    vr.message = "";
                                }
                            }
                            //}
                            //}
                            //}
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Send Error Email
                string paramsStr = $"\r\n\r\npublic string heading: {data.heading}\r\n" +
                                    $"public string membName: {data.membName}\r\n" +
                                    $"public string membSurname: {data.membSurname}\r\n" +
                                    $"public string membIDNo: {data.membIDNo}\r\n" +
                                    $"public string provid: {data.provid}\r\n" +
                                    $"public string user: {data.user}\r\n\r\n" +
                                    $"public string id: {data.id}\r\n" +
                                    $"public bool enabled: {data.enabled}\r\n" +
                                    $"public string code: {data.code}\r\n" +
                                    $"public string phcode: {data.phcode}\r\n" +
                                    $"public string desc: {data.desc}\r\n" +
                                    $"public string headDesc: {data.headDesc}\r\n" +
                                    $"public bool enterQty: {data.enterQty}\r\n" +
                                    $"public int defaultQty: {data.defaultQty}\r\n" +
                                    $"public bool enterTooth: {data.enterTooth}\r\n" +
                                    $"public bool dependantOnCode: {data.dependantOnCode}\r\n" +
                                    $"public string depCode: {data.depCode}\r\n" +
                                    $"public string price: {data.price}\r\n" +
                                    $"public string total: {data.total}\r\n" +
                                    $"public string membContact: {data.membContact}\r\n" +
                                    $"public string toothNo: {data.toothNo}\r\n" +
                                    $"public string merchantID: {data.merchantID}\r\n";

                string url = _EmailWebSvcConnection;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;
                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                
                //var c = client.SendEmailAsync("INTERNAL VOUCHER REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: INTERNAL VOUCHER REDEEM ERROR", "jaco@pamc.co.za");

                if(_isTest)
                {
                    var c = client.SendEmailAsync("INTERNAL VOUCHER REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL TEST: INTERNAL VOUCHER REDEEM ERROR", _testEmails);
                }
                else
                {
                    var c = client.SendEmailAsync("INTERNAL VOUCHER REDEEM ERROR \r\n\r\n" + "Message: " + e.Message + "\r\n\r\nStacktrace: " + e.StackTrace + paramsStr, "PORTAL: INTERNAL VOUCHER REDEEM ERROR", "jaco@pamc.co.za");
                }

                // Log error in text file
                LogError(e.Message, e.StackTrace);
            }

            if (vr.message.Trim() == "" && vr.valid == false)
            {
                vr.message = "Unexpected error occured";
            }

            return vr;
        }

        [HttpPost]
        [Route("LogEvent")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel LogEvent([FromBody] EventLoggingModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@prov", data.provid));
                    cmd.Parameters.Add(new SqlParameter("@event", data.eventDesc));
                    cmd.Parameters.Add(new SqlParameter("@memb", data.member));
                    cmd.Parameters.Add(new SqlParameter("@uuid", data.uuid));
                    cmd.Parameters.Add(new SqlParameter("@amount", data.amount));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:dd").Replace("T", " ")));

                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.MediVouch_Log (provid,uuid,event,memberid,amount,createdate,createby,changedate,changeby)\n" +
                        $"VALUES (@prov,@uuid,@event,@memb,@amount,@date,@user,@date,@user)";
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateProcCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProcCode([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@proc", data.code));

                    cmd.CommandText = $"SELECT DISTINCT SVCDESC FROM {DRCDatabase}.dbo.SERVICE_CODES WHERE REPLACE(SVCCODE,' ','') = @proc";

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        int count = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (count == 0)
                            {
                                vr.message = vr.message + "* " + row["SVCDESC"].ToString() + "\n\r";
                            }
                            else
                            {
                                vr.message = vr.message + "*" + row["SVCDESC"].ToString() + "\n\r";
                            }

                            count++;
                        }

                        DataTable dtbl = new DataTable();
                        cmd.CommandText = $"SELECT HPCODE,feeset FROM {MediDatabase}.dbo.MEDIWALLET_HP_SETUP WHERE ENABLED = '1'";
                        dtbl.Load(cmd.ExecuteReader());

                        string hp = dtbl.Rows[0][0].ToString();
                        string feeset = dtbl.Rows[0][1].ToString();
                        feeset = $"'{feeset.Replace(",", "','")}'";

                        dtbl = new DataTable();

                        cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET IN( {feeset}) AND PHCODE = 'P' AND BEGPROC = '{data.code.PadLeft(20, ' ')}' ";
                        dtbl.Load(cmd.ExecuteReader());
                        if (dtbl.Rows.Count > 0)
                        {
                            vr.message = dtbl.Rows[0][0].ToString();
                        }
                        else
                        {
                            vr.message = "";
                        }

                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateMemberExists")]
        public List<DependantsInfoModel> ValidateMemberExist([FromBody] DefaultCodeModel data)
        {
            List<DependantsInfoModel> lst = new List<DependantsInfoModel>();
            
            try
            {
                SqlConnection cn = new SqlConnection(_drcConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                DataTable dt = new DataTable();

                // Check if Scheme/Aid member by policynumber
                //string subssn = "M" + data.prefix + data.membIDNo;
                //cmd.Parameters.Add(new SqlParameter("@SUBSSN", subssn));
                //cmd.Parameters.Add(new SqlParameter("@HPCODE", data.hpCode));
                //cmd.CommandText = $"SELECT * FROM MEMB_MASTERS WHERE SUBSSN = @SUBSSN AND HPCODE = @HPCODE";
                //dt.Load(cmd.ExecuteReader());

                //if (dt.Rows.Count > 0)  // Scheme member
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        DependantsInfoModel item = new DependantsInfoModel();
                //        string membId = row["MEMBID"].ToString();
                //        string dep = membId.Substring(membId.IndexOf("-") + 1, 2);
                //        item.dependant = dep;
                //        item.firstname = row["FIRSTNM"].ToString();
                //        item.lastname = row["LASTNM"].ToString();
                //        item.cell = row["CELL"].ToString();
                //        item.option = row["OPT"].ToString();
                //        item.isSchemeMember = true;

                //        lst.Add(item);
                //    }
                //}
                //else 
                //{
                //    // Check if Scheme/Aid member by SA id number
                //    cmd.Parameters.Add(new SqlParameter("@PATID", data.membIDNo));
                //    cmd.CommandText = $"SELECT * FROM MEMB_MASTERS WHERE PATID = @PATID AND HPCODE = @HPCODE";
                //    dt = new DataTable();
                //    dt.Load(cmd.ExecuteReader());

                //    if (dt.Rows.Count > 0)  // Scheme member
                //    {
                //        foreach (DataRow row in dt.Rows)
                //        {
                //            DependantsInfoModel item = new DependantsInfoModel();
                //            string membId = row["MEMBID"].ToString();
                //            string dep = membId.Substring(membId.IndexOf("-") + 1, 2);
                //            item.dependant = dep;
                //            item.firstname = row["FIRSTNM"].ToString();
                //            item.lastname = row["LASTNM"].ToString();
                //            item.cell = row["CELL"].ToString();
                //            item.option = row["OPT"].ToString();
                //            item.isSchemeMember = true;

                //            lst.Add(item);
                //        }
                //    }
                //    else // Non-member
                {
                    //Check if previous voucher patient
                    string membid = data.prefix + data.membIDNo + "-00";
                        cmd.Parameters.Add(new SqlParameter("@MEMBID", membid));
                        cmd.CommandText = $"SELECT * FROM MEMB_MASTERS WHERE MEMBID = @MEMBID";
                        dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow row in dt.Rows)
                        {
                            DependantsInfoModel item = new DependantsInfoModel();
                            string membId = row["MEMBID"].ToString();
                            string dep = membId.Substring(membId.IndexOf("-") + 1, 2);
                            item.dependant = dep;
                            item.firstname = row["FIRSTNM"].ToString();
                            item.lastname = row["LASTNM"].ToString();
                            item.cell = row["CELL"].ToString();
                            item.option = row["OPT"].ToString();
                            item.isSchemeMember = false;

                            lst.Add(item);
                        }
                }
                //}

                //string membid = data.membIDNo;

                //cmd.Parameters.Add(new SqlParameter("@ID", data.membIDNo));
                ////cmd.CommandText = $"SELECT * FROM MEMB_MASTERS WHERE PATID = @ID AND HPCODE = 'MDV'";
                //cmd.CommandText = $"SELECT * FROM MEMB_MASTERS WHERE MEMBID = @ID";
                //DataTable dt = new DataTable();
                //dt.Load(cmd.ExecuteReader());
                cn.Close();
                cn.Dispose();
                cmd.Dispose();

                //if (dt.Rows.Count > 0)
                //{
                //    vr.valid = true;
                //    vr.message = dt.Rows[0]["FIRSTNM"].ToString() + "," + dt.Rows[0]["LASTNM"].ToString() + "," + dt.Rows[0]["CELL"].ToString();
                //}
                //else
                //{
                //    vr.valid = false;
                //}
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }
            return lst;
        }

        [HttpPost]
        [Route("ValidateDiagCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateDiagCode([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@diag", data.diag));

                    cmd.CommandText = $"SELECT DISTINCT DIAGDESC FROM {DRCDatabase}.dbo.DIAG_CODES WHERE DIAGCODE = @diag";

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        int count = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (count == 0)
                            {
                                vr.message = vr.message + "* " + row["DIAGDESC"].ToString() + "\n\r";
                            }
                            else
                            {
                                vr.message = vr.message + "*" + row["DIAGDESC"].ToString() + "\n\r";
                            }

                            count++;
                        }

                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("SaveData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveData([FromBody] List<ClaimCodesModel> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                // Get basket PHCODE for claim_master (P or H)
                string basketId = data[0].mediRef.ToString().Substring(data[0].mediRef.ToString().IndexOf("_") + 1);
                cmd.Parameters.Add(new SqlParameter("@basketId", basketId));

                cmd.CommandText = $" SELECT PhCode FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE id = @basketId ";
                string phCodeMaster = cmd.ExecuteScalar().ToString();                

                // Insert Claim
                cmd.Parameters.Add(new SqlParameter("@memb", data[0].membId));
                cmd.Parameters.Add(new SqlParameter("@uuid", data[0].id));
                cmd.Parameters.Add(new SqlParameter("@prov", data[0].provid));
                cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                cmd.Parameters.Add(new SqlParameter("@user", data[0].user));
                cmd.Parameters.Add(new SqlParameter("@medi", data[0].mediRef));
                cmd.Parameters.Add(new SqlParameter("@name", data[0].name));
                cmd.Parameters.Add(new SqlParameter("@surname", data[0].surname));
                cmd.Parameters.Add(new SqlParameter("@diag", data[0].diag));
                cmd.Parameters.Add(new SqlParameter("@dob", ""));
                cmd.Parameters.Add(new SqlParameter("@hpCode", data[0].hpCode));
                cmd.Parameters.Add(new SqlParameter("@opt", data[0].option));
                cmd.Parameters.Add(new SqlParameter("@phCodeMaster", phCodeMaster));                

                double total = 0.00;
                foreach (var item in data)
                {
                    total = total + Convert.ToDouble(item.total);
                }
                cmd.Parameters.Add(new SqlParameter("@total", total));

                cmd.CommandText = $" INSERT INTO {MediDatabase}.dbo.Claim_Header (uuid,mediRef,provid,memberId,firstname,lastname,dateofbirth, " +
                                  $" net,HpCode,Opt,PhCode,createDate,createBy,changeDate,changeBy) \n " +
                                  $" OUTPUT INSERTED.id \n " +
                                  $" VALUES (@uuid,@medi,@prov,@memb,@name,@surname,@dob,@total,@hpCode,@opt,@phCodeMaster,@date,@user,@date,@user) ";

                int id = (int)cmd.ExecuteScalar();

                cmd.CommandText = $"UPDATE {MediDatabase}.dbo.Claim_Header SET ref = '#DRC{id}' WHERE id = '{id}'";
                cmd.ExecuteNonQuery();

                foreach (ClaimCodesModel code in data)
                {
                    cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.Claim_Detail (headId,DiagCode,ProcCode,ToothNo,qty,singleUnitValue,total,PhCode,PayableByProv,PayableByMemb,createDate,createBy,changeDate,changeBy) \n " +
                                      $"VALUES ('{id}',@diag,'{code.code}','{code.toothNo}','{code.qty}','{code.singleUnitAmount}','{code.total}','{code.phCode}','{code.provpay}','{code.membpay}',@date,@user,@date,@user)";

                    cmd.ExecuteNonQuery();
                }

                vr.message = $"#DRC{id}";

                cmd = null;
                cn.Close();

                if (data[0].createClaim)
                {
                    SaveDataToDAT(id.ToString(), data[0].prefix, data[0].hpCode);
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("SendEmail")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SendEmail([FromBody] Emails data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {

                    if (cn.State != ConnectionState.Open) cn.Open();

                    DataTable dt = new DataTable();

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    if (data.email != null && data.email != "" && data.email.Length > 1 && data.email.Contains('@'))
                    {
                        cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.EmailListPerProvider  WHERE email = '{data.email}'";
                        cmd.ExecuteNonQuery();
                        dt.Load(cmd.ExecuteReader());
                        if (dt.Rows.Count == 0)
                        {
                            cmd.CommandText = $"INSERT INTO {MediDatabase}.dbo.EmailListPerProvider " +
                                                $"(provid,email,createdate,createby,changedate,changeby) VALUES " +
                                                $"('{data.provid}','{data.email}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','99'," +
                                                $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','99')";
                            cmd.ExecuteNonQuery();
                        }
                    }

                    cmd.CommandText = cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = "";
                    string username = "";
                    string password = "";
                    string from = "";
                    string port = "";
                    string subject = "";
                    string body = "";

                    subject = $"MEDIWALLET CLAIM FOR MEMBER {data.membid}";

                    body = $"Good day, \n\r" +
                        $"Please find below your refference number for you mediwallet claim, \n" +
                        $"submitted for member with id {data.membid} : \n\r" +
                        $"MEDIWALLET REF: {data.mediref} \n" +
                        $"DRC REF: {data.drcref}\n\r" +
                        $"Regards";

                    foreach (DataRow row in dt.Rows)
                    {
                        server = row["SERVERADDRESS"].ToString();
                        password = row["PASSWORD"].ToString();
                        username = row["USERNAME"].ToString();
                        from = row["MAILFROM"].ToString();
                        port = row["OUTGOINGPORT"].ToString();
                    }

                    cmd.CommandText = $"SELECT EMAIL FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = '{data.provid}'";
                    dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    string email = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        email = row["EMAIL"].ToString();
                    }

                    email = "jaco@pamc.co.za";

                    if (email != null && email != "")
                    {
                        if (data.email != null && data.email != "" && data.email.Length > 1 && data.email.Contains('@'))
                        {
                            email = email + ";" + data.email;
                        }
                        //bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);

                        if(_isTest)
                        {
                            bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from,"PORTAL TEST: " + subject, body, _testEmails, null);
                        }
                        else
                        {
                            bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);
                        }
                    }
                    else
                    {
                        if (data.email != null && data.email != "" && data.email.Length > 1 && data.email.Contains('@'))
                        {
                            email = data.email;
                                                        
                            //bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);
                            if (_isTest)
                            {
                                bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from,"PORTAL TEST: " + subject, body, _testEmails, null);
                            }
                            else
                            {
                                bool sended = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, email, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }
            return vr;
        }

        //[HttpGet]
        //[Route("GetDefaultCodes")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public List<DefaultCodeModel> GetDefaultCode()
        //{
        //    List<DefaultCodeModel> list = new List<DefaultCodeModel>();
        //    return list;
        //}

        //[HttpGet]
        //[Route("GetDefaultCodeGroups")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public List<DefaultCodeModel> GetDefaultCodeGroups()
        //{
        //    List<DefaultCodeModel> list = new List<DefaultCodeModel>();
        //    try
        //    {
        //        using (SqlConnection cn = new SqlConnection(_mediConnectionString))
        //        {
        //            if (cn.State != ConnectionState.Open) cn.Open();

        //            SqlCommand cmd = new SqlCommand();
        //            cmd.Connection = cn;     
        //            cmd.CommandText = $"SELECT id, Description, Enabled FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE Enabled = '1'";

        //            DataTable dt = new DataTable();
        //            dt.Load(cmd.ExecuteReader());

        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                DefaultCodeModel d = new DefaultCodeModel();

        //                d.id = dr["id"].ToString();
        //                d.heading = dr["Description"].ToString();

        //                list.Add(d);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogError(e.Message, e.StackTrace);
        //        throw;
        //    }
        //    return list;
        //}

        [HttpPost]
        [Route("GetDefaultCodeDetails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetDefaultCodeDetails([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                //using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                //{
                //if (cn.State != ConnectionState.Open)
                //{
                //    cn.Open();
                //}

                //string dentist = "";
                //string gp = "";

                //SqlCommand cmd = new SqlCommand();

                //cmd.Connection = cn;

                //DataTable dtbl = new DataTable();


                //DataTable tbl = new DataTable();
                //cmd.Parameters.Add(new SqlParameter("@id", data.id.Substring(data.id.IndexOf('_', 0) + 1, (data.id.Length - 1) - (data.id.IndexOf('_', 0)))));
                //cmd.Parameters.Add(new SqlParameter("@provid", data.provid));

                //SqlConnection cn2 = new SqlConnection(_portalConnectionString);
                //if (cn2.State != ConnectionState.Open)
                //{
                //    cn2.Open();
                //}
                //cmd.CommandText = $"SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO WHERE PROVID = @provid";
                //cmd.Connection = cn2;
                //tbl.Load(cmd.ExecuteReader());

                //string feeset = "";
                //string spec = tbl.Rows[0][0].ToString().Trim();

                //cmd.Connection = cn;
                //cmd.CommandText = $"SELECT [OpositeFeeset] FROM {base.MediDatabase}..SVC_CODES_SETUP_HEADER where id = @id";
                //tbl = new DataTable();
                //tbl.Load(cmd.ExecuteReader());

                //bool opFeeset = Convert.ToBoolean(tbl.Rows[0][0].ToString());
                //switch (spec)
                //{
                //    case "54":
                //    case "95":
                //        feeset = "55000";
                //        dentist = "1";
                //        gp = "0";
                //        break;
                //    case "14":
                //        tbl = new DataTable();
                //        cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //        tbl.Load(cmd.ExecuteReader());
                //        if (tbl.Rows[0][0].ToString() == "6" && (opFeeset == false))
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //        dentist = "0";
                //        gp = "1";
                //        break;
                //    case "15":
                //        if (tbl.Rows[0][0].ToString() == "6" && (opFeeset == false))
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //        dentist = "0";
                //        gp = "1";
                //        break;
                //    default:
                //        break;
                //}


                //dtbl = new DataTable();
                ////And a.provid ='{data.provid}'

                //SqlConnection cn1 = new SqlConnection(_drcConnectionString);
                //if (cn1.State != ConnectionState.Open)
                //{
                //    cn1.Open();
                //}

                // Get basket details
                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@id", data.id.Substring(data.id.IndexOf('_', 0) + 1, (data.id.Length - 1) - (data.id.IndexOf('_', 0)))));
                //cmd.CommandText = $"SELECT  h.OpositeFeeset,d.Code,d.Description,d.EnterQty,d.DefaultQty,d.EnterToothNo,d.DependantOnOtherCode,d.DependantCode, h.Description as [hDesc], h.CreateClaims " +
                //    $"FROM SVC_CODES_SETUP_DETAIL d \r\n " +
                //    $"INNER JOIN SVC_CODES_SETUP_HEADER h on h.id = d.headId \r\n " +
                //    $"WHERE d.HeadId = @id and d.DentistOnly = '{dentist}' and d.GPOnly = '{gp}'";
                cmd.CommandText = $" SELECT  h.Feeset, h.HpCode, d.PhCode, d.Code,d.Description,d.EnterQty,d.DefaultQty,d.EnterToothNo,d.DependantOnOtherCode,d.DependantCode, h.Description as [hDesc], h.CreateClaims " +
                                  $" FROM SVC_CODES_SETUP_DETAIL d \r\n " +
                                  $" INNER JOIN SVC_CODES_SETUP_HEADER h on h.id = d.headId \r\n " +
                                  $" WHERE d.HeadId = @id AND h.Enabled = 1 ";
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                cn.Close();

                // Get price from feeset
                cn = new SqlConnection(_drcConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                cmd = new SqlCommand();
                cmd.Connection = cn;
                DataTable dtFeeset = new DataTable();

                foreach (DataRow row in dt.Rows)
                {
                    DefaultCodeModel d = new DefaultCodeModel();
                    d.headDesc = row["hDesc"].ToString();
                    d.code = row["Code"].ToString();
                    d.desc = row["Description"].ToString();
                    d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                    d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                    d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                    d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                    d.depCode = row["DependantCode"].ToString();
                    d.createClaim = Convert.ToBoolean(row["CreateClaims"].ToString());
                    d.phcode = row["PhCode"].ToString();

                    //dtbl = new DataTable();
                    cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET = {Convert.ToInt32(row["Feeset"].ToString())} AND PHCODE = '{row["PhCode"].ToString()}' AND BEGPROC = '{d.code.PadLeft(20, ' ')}' ";
                    //cmd.Connection = cn1;
                    dtFeeset.Clear();
                    dtFeeset.Load(cmd.ExecuteReader());
                    if (dtFeeset.Rows.Count > 0)
                    {
                        d.price = dtFeeset.Rows[0][0].ToString();
                        d.total = (Convert.ToInt32(d.defaultQty) * Convert.ToDecimal(d.price)).ToString();
                        list.Add(d);
                    }
                    else
                    {
                        d.price = "";
                    }                   
                }
                cn.Close();
                cmd.Dispose();
                cn.Dispose();
                //}
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("GetAllClaims")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimCodesModel> GetAllClaims([FromBody] LoginDetailsModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    string date = DateTime.Now.AddDays(-14).ToString("yyyy/MM/dd");

                    string sql = " SELECT cd.headId,ch.memberId,ch.uuid,ch.createDate,\n"
                               + " SUM(CAST(ISNULL(cd.PayableByProv,'0.00') AS decimal(18,2))) AS [PayableByProv],\n"
                               + " SUM(CAST(ISNULL(cd.PayableByMemb,'0.00')AS DECIMAL(18, 2))) AS [PayableByMemb],\n"
                               + " SUM(CAST(ISNULL(cd.total,'0.00')AS DECIMAL(18, 2)))AS [total],ch.ref,ch.mediRef \n"
                               + $" FROM {MediDatabase}.dbo.Claim_Header ch\n"
                               + $" INNER JOIN {MediDatabase}.dbo.Claim_Detail cd on cd.headId = ch.id\n"
                               + " GROUP BY  cd.headId,ch.memberId,ch.ref,ch.mediRef,ch.uuid,ch.createDate";

                    cmd.CommandText = sql;
                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ClaimCodesModel cm = new ClaimCodesModel();

                        cm.id = row["headId"].ToString();
                        cm.desc = row["uuid"].ToString();
                        cm.membId = row["memberId"].ToString();
                        cm.total = row["total"].ToString();
                        cm.provpay = row["PayableByProv"].ToString();
                        cm.membpay = row["PayableByMemb"].ToString();
                        cm.qty = row["createDate"].ToString();
                        cm.mediRef = row["mediRef"].ToString();
                        cm.drcRef = row["ref"].ToString();

                        list.Add(cm);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("GetClaimDetail")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimCodesModel> GetClaimDetail([FromBody] ClaimCodesModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    //string date = DateTime.Now.AddDays(-14).ToString("yyyy/MM/dd");
                    cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.Claim_Detail WHERE headId = '{data.id}'";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ClaimCodesModel cm = new ClaimCodesModel();

                        cm.diag = row["DiagCode"].ToString();
                        cm.code = row["ProcCode"].ToString();
                        cm.toothNo = row["ToothNo"].ToString();
                        if (cm.toothNo == "" || cm.toothNo == null)
                        {
                            cm.toothNo = "N/A";
                        }
                        cm.qty = row["qty"].ToString();
                        cm.singleUnitAmount = row["singleUnitValue"].ToString();
                        cm.total = row["total"].ToString();
                        cm.membpay = row["PayableByMemb"].ToString();
                        cm.provpay = row["PayableByProv"].ToString();
                        list.Add(cm);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return list;
        }

        [HttpPost]
        [Route("ValidateToothNo")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateToothNo([FromBody] ClaimCodesModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    string[] tooth = data.toothNo.Split(',');

                    bool alltheethValid = false;
                    foreach (string toothno in tooth)
                    {
                        cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.CLINICAL_CODES WHERE CLINICALCODE = '{toothno}'";
                        DataTable dt = new DataTable();

                        dt.Load(cmd.ExecuteReader());

                        if (dt.Rows.Count > 0)
                        {
                            alltheethValid = alltheethValid || true;
                        }
                        else
                        {
                            alltheethValid = alltheethValid && false;
                        }
                    }

                    vr.valid = alltheethValid;
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return vr;
        }

        [HttpGet]
        [Route("GetSettingDiags")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DiagCodes> GetSettingDiags()
        {
            List<DiagCodes> list = new List<DiagCodes>();

            try
            {
                using (SqlConnection cn = new SqlConnection(base._mediConnection.ConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT SettingDesc FROM Settings WHERE SettingId = 'Diags' AND Enabled = 1";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string diags = "";

                    foreach (DataRow row in dt.Rows)
                    {
                        diags = row["SettingDesc"].ToString();
                    }

                    if (diags.Length > 0)
                    {
                        string[] codes = new string[diags.Split(";").Length];

                        codes = diags.Split(";");

                        for (int i = 0; i < codes.Length; i++)
                        {
                            cmd.CommandText = $"SELECT DIAGDESC FROM {DRCDatabase}.dbo.DIAG_CODES WHERE DIAGCODE = '{codes[i]}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            foreach (DataRow item in dt.Rows)
                            {
                                DiagCodes d = new DiagCodes();
                                d.code = codes[i];
                                d.desc = item["DIAGDESC"].ToString();
                                list.Add(d);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return list;
        }

        [HttpPost]
        [Route("GetBasketHeaders")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetBasketHeaders([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                //using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                //{
                //if (cn.State != ConnectionState.Open)
                //{
                //    cn.Open();
                //}



                //SqlCommand cmd = new SqlCommand();

                //cmd.Connection = cn;

                //DataTable dtbl = new DataTable();


                //DataTable tbl = new DataTable();

                //cmd.Parameters.Add(new SqlParameter("@provid", data.provid));

                //SqlConnection cn2 = new SqlConnection(_portalConnectionString);
                //if (cn2.State != ConnectionState.Open)
                //{
                //    cn2.Open();
                //}
                //cmd.CommandText = $"SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO WHERE PROVID = @provid";
                //cmd.Connection = cn2;
                //tbl.Load(cmd.ExecuteReader());

                //string spec = tbl.Rows[0][0].ToString().Trim();

                //string feeset = "";
                //string dentist = "";
                //string gp = "";
                //string despens = "0";
                //string script = "0";

                //switch (data.provSpec.Trim())
                //{
                //    case "54":
                //    case "95":

                //        //feeset = "55000";
                //        dentist = "1";
                //        gp = "0";
                //        break;

                //case "14":
                //    //tbl = new DataTable();
                //    //cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //    //tbl.Load(cmd.ExecuteReader());
                //    //if (tbl.Rows[0][0].ToString() == "6")
                //    if (data.provClass == "6")
                //    {
                //        feeset = "14962";
                //        despens = "1";
                //        script = "0";
                //    }
                //    else
                //    {
                //        feeset = "14972";
                //        despens = "0";
                //        script = "1";

                //    }
                //    dentist = "0";
                //    gp = "1";
                //    break;

                //case "15":
                //    //if (tbl.Rows[0][0].ToString() == "6")
                //    if (data.provClass == "6")
                //    {
                //        feeset = "14962";
                //        despens = "1";
                //        script = "0";
                //    }
                //    else
                //    {
                //        feeset = "14972";
                //        despens = "0";
                //        script = "1";
                //    }
                //    dentist = "0";
                //    gp = "1";
                //    break;

                //    default:
                //        if (data.provClass == "6")
                //        {
                //            //feeset = "14962";
                //            despens = "1";
                //            script = "0";
                //        }
                //        else
                //        {
                //            //feeset = "14972";
                //            despens = "0";
                //            script = "1";
                //        }
                //        dentist = "0";
                //        gp = "1";
                //        break;
                //}

                //dtbl = new DataTable();
                //And a.provid ='{data.provid}'

                //SqlConnection cn1 = new SqlConnection(_drcConnectionString);
                //if (cn1.State != ConnectionState.Open)
                //{
                //    cn1.Open();
                //}

                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (data.hpCode == "" && data.option == "")
                {
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER " +
                                      //$" WHERE  Dentist = '{dentist}' and GP = '{gp}' and Enabled = 1 AND [Despensing]='{despens}' and [Scripting] = '{script}' and [HpCode] = '{data.hpCode}' ";
                                      $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and Opt <> 'MEDIVOUCH' and Enabled = 1 ";
                }
                else if (data.option == "")
                {
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER " +
                                      //$" WHERE  Dentist = '{dentist}' and GP = '{gp}' and Enabled = 1 AND [Despensing]='{despens}' and [Scripting] = '{script}' and [HpCode] = '{data.hpCode}' ";
                                      $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and HpCode = '{data.hpCode.Trim()}' and Enabled = 1 ";
                }
                else
                {
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER " +
                                      //$" WHERE  Dentist = '{dentist}' and GP = '{gp}' and Enabled = 1 AND [Despensing]='{despens}' and [Scripting] = '{script}' and [HpCode] = '{data.hpCode}' ";
                                      $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and HpCode = '{data.hpCode.Trim()}' and Opt = '{data.option.Trim()}' and Enabled = 1 ";
                }

                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    DefaultCodeModel d = new DefaultCodeModel();
                    d.id = row["Id"].ToString();
                    d.desc = row["Description"].ToString();
                    list.Add(d);
                }

                cmd.Dispose();
                cn.Close();
                cn.Dispose();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetBasketDetails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetBasketDetails([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                //using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                //{
                //    if (cn.State != ConnectionState.Open)
                //    {
                //        cn.Open();
                //    }

                //    string dentist = "";
                //    string despens = "0";
                //    string script = "0";

                //    string gp = "";

                //    SqlCommand cmd = new SqlCommand();

                //    cmd.Connection = cn;

                //    DataTable dtbl = new DataTable();

                //    DataTable tbl = new DataTable();

                //    cmd.Parameters.Add(new SqlParameter("@provid", data.provid));

                //    SqlConnection cn2 = new SqlConnection(_portalConnectionString);
                //    if (cn2.State != ConnectionState.Open)
                //    {
                //        cn2.Open();
                //    }
                //    cmd.CommandText = $"SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO WHERE PROVID = @provid";
                //    cmd.Connection = cn2;
                //    tbl.Load(cmd.ExecuteReader());

                //    string feeset = "";
                //    string spec = tbl.Rows[0][0].ToString().Trim();
                //    switch (spec)
                //    {
                //        case "54":
                //            feeset = "55000";
                //            dentist = "1";
                //            gp = "0";
                //            break;
                //        case "14":
                //            tbl = new DataTable();
                //            cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //            tbl.Load(cmd.ExecuteReader());
                //            if (tbl.Rows[0]["CLASS"].ToString() == "6")
                //            {
                //                feeset = "14962";
                //                despens = "1";
                //                script = "0";
                //            }
                //            else
                //            {
                //                feeset = "14972";
                //                despens = "0";
                //                script = "1";
                //            }
                //            dentist = "0";
                //            gp = "1";
                //            break;
                //        case "15":
                //            tbl = new DataTable();
                //            cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //            tbl.Load(cmd.ExecuteReader());
                //            if (tbl.Rows[0][0].ToString() == "6")
                //            {
                //                feeset = "14962";
                //                despens = "1";
                //                script = "0";
                //            }
                //            else
                //            {
                //                feeset = "14972";
                //                despens = "0";
                //                script = "1";
                //            }
                //            dentist = "0";
                //            gp = "1";
                //            break;
                //        default:
                //            break;
                //    }

                //    dtbl = new DataTable();
                //And a.provid ='{data.provid}'
                SqlConnection cn1 = new SqlConnection(_drcConnectionString);
                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (data.hpCode == "" && data.option == "")
                {
                    cmd.CommandText = $" SELECT sd.*, sh.Feeset, sd.PhCode FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid " +
                                  $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and Opt <> 'MEDIVOUCH' and Enabled = 1 ";
                }
                else if (data.option == "")
                {
                    cmd.CommandText = $" SELECT sd.*, sh.Feeset, sd.PhCode FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid " +
                                  $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and HpCode = '{data.hpCode.Trim()}' and Enabled = 1 ";
                }
                else
                {
                    cmd.CommandText = $" SELECT sd.*, sh.Feeset, sd.PhCode FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid " +
                                  $" WHERE Spec like '%''{data.provSpec.Trim()}''%' and Class like '%''{data.provClass.Trim()}''%' and HpCode = '{data.hpCode.Trim()}' and Opt = '{data.option.Trim()}' and Enabled = 1 ";
                    //$" WHERE sh.[Enabled] = 1 and sd.DentistOnly = '{dentist}' and sd.GPOnly = '{gp}' AND sd.[Despensing]='{despens}' and sd.[Scripting] = '{script}'";
                }

                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    DefaultCodeModel d = new DefaultCodeModel();
                    d.id = row["HeadId"].ToString();
                    d.code = row["Code"].ToString();
                    d.desc = row["Description"].ToString();
                    d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                    d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                    d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                    d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                    d.depCode = row["DependantCode"].ToString();
                    d.phcode = row["PhCode"].ToString();

                    DataTable dtbl = new DataTable();
                    if (cn1.State != ConnectionState.Open)
                    {
                        cn1.Open();
                    }
                    cmd = new SqlCommand();
                    cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET = {Convert.ToInt32(row["Feeset"].ToString())} AND PHCODE = '{row["PhCode"].ToString()}' AND BEGPROC = '{d.code.PadLeft(20, ' ')}' ";
                    cmd.Connection = cn1;
                    dtbl.Load(cmd.ExecuteReader());
                    if (dtbl.Rows.Count > 0)
                    {
                        d.price = dtbl.Rows[0][0].ToString();
                    }
                    else
                    {
                        d.price = "";
                    }

                    d.total = (Convert.ToInt32(d.defaultQty) * Convert.ToDecimal(d.price)).ToString();
                    list.Add(d);
                }

                cmd.Dispose();
                cn.Close();
                cn1.Close();
                cn.Dispose();
                cn1.Dispose();

                //foreach (DataRow row in dt.Rows)
                //{
                //    if (!Convert.ToBoolean(row["OpositeFeeset"].ToString()))
                //    {
                //        DefaultCodeModel d = new DefaultCodeModel();
                //        d.id = row["HeadId"].ToString();
                //        d.code = row["Code"].ToString();
                //        d.desc = row["Description"].ToString();
                //        d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                //        d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                //        d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                //        d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                //        d.depCode = row["DependantCode"].ToString();

                //        dtbl = new DataTable();

                //        cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET IN({feeset}) AND PHCODE = 'P' AND BEGPROC = '{d.code.PadLeft(20, ' ')}' ";
                //        cmd.Connection = cn1;
                //        dtbl.Load(cmd.ExecuteReader());
                //        if (dtbl.Rows.Count > 0)
                //        {
                //            d.price = dtbl.Rows[0][0].ToString();
                //        }
                //        else
                //        {
                //            d.price = "";
                //        }

                //        d.total = (Convert.ToInt32(d.defaultQty) * Convert.ToDecimal(d.price)).ToString();
                //        list.Add(d);
                //    }
                //    else
                //    {
                //        DefaultCodeModel d = new DefaultCodeModel();
                //        d.id = row["HeadId"].ToString();
                //        d.code = row["Code"].ToString();
                //        d.desc = row["Description"].ToString();
                //        d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                //        d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                //        d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                //        d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                //        d.depCode = row["DependantCode"].ToString();

                //        dtbl = new DataTable();
                //        switch (spec)
                //        {
                //            case "54":
                //                feeset = "55000";
                //                dentist = "1";
                //                gp = "0";
                //                break;
                //            case "14":
                //                tbl = new DataTable();
                //                cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //                tbl.Load(cmd.ExecuteReader());
                //                if (tbl.Rows[0]["CLASS"].ToString() == "6")
                //                {
                //                    feeset = "14972";
                //                    despens = "1";
                //                    script = "0";
                //                }
                //                else
                //                {
                //                    feeset = "14972";
                //                    despens = "0";
                //                    script = "1";
                //                }
                //                dentist = "0";
                //                gp = "1";
                //                break;
                //            case "15":
                //                tbl = new DataTable();
                //                cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //                tbl.Load(cmd.ExecuteReader());
                //                if (tbl.Rows[0]["CLASS"].ToString() == "6")
                //                {
                //                    feeset = "14972";
                //                    despens = "1";
                //                    script = "0";
                //                }
                //                else
                //                {
                //                    feeset = "14972";
                //                    despens = "0";
                //                    script = "1";
                //                }
                //                dentist = "0";
                //                gp = "1";
                //                break;
                //            default:
                //                break;
                //        }
                //        cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET IN({feeset}) AND PHCODE = 'P' AND BEGPROC = '{d.code.PadLeft(20, ' ')}' ";
                //        cmd.Connection = cn1;
                //        dtbl.Load(cmd.ExecuteReader());
                //        if (dtbl.Rows.Count > 0)
                //        {
                //            d.price = dtbl.Rows[0]["USUALBILL"].ToString();
                //        }
                //        else
                //        {
                //            d.price = "";
                //        }

                //        d.total = (Convert.ToInt32(d.defaultQty) * Convert.ToDecimal(d.price)).ToString();
                //        list.Add(d);
                //    }
                //}
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetDiags")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimCodesModel> GetDiags([FromBody] DefaultCodeModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();
            try
            {
                //using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                //{
                //    if (cn.State != ConnectionState.Open)
                //    {
                //        cn.Open();
                //    }

                //SqlCommand cmd = new SqlCommand();

                //cmd.Connection = cn;

                //DataTable dtbl = new DataTable();

                //DataTable tbl = new DataTable();

                //cmd.Parameters.Add(new SqlParameter("@provid", data.provid));

                //    SqlConnection cn2 = new SqlConnection(_portalConnectionString);
                //    if (cn2.State != ConnectionState.Open)
                //    {
                //        cn2.Open();
                //    }

                //    // Get Spec
                //    cmd.CommandText = $"SELECT SPECCODE FROM {DRCDatabase}.dbo.PROV_SPECINFO WHERE PROVID = @provid";
                //    cmd.Connection = cn2;
                //    tbl.Load(cmd.ExecuteReader());
                //    string spec = tbl.Rows[0][0].ToString().Trim();

                ////Get class
                //    tbl = new DataTable();
                //    cmd.CommandText = $"SELECT CLASS FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = @provid";
                //    tbl.Load(cmd.ExecuteReader());

                //string dentist = "";
                //string gp = "";
                //string feeset = "";

                //switch (data.provSpec.Trim())
                //{
                //    case "95":
                //    case "54":
                // This piece of code will run if spec is 54 or 95 (stacking)

                //if (data.hpCode == "MDV")
                //{
                //    feeset = "55000";
                //}
                //else if (data.hpCode == "DRCV")
                //{
                //    feeset = "55000";
                //}
                //dentist = "1";
                //gp = "0";
                //break;

                //case "95":
                //    //if (data.hpCode == "MDV")
                //    //{
                //    //    feeset = "55000";
                //    //}
                //    //else if (data.hpCode == "DRCV")
                //    //{
                //    //    feeset = "55000";
                //    //}
                //    dentist = "1";
                //    gp = "0";
                //    break;

                //case "14":
                //    if (data.hpCode == "MDV")
                //    {
                //        //if (tbl.Rows[0][0].ToString() == "6")
                //        if (data.provClass == "6")
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //    }
                //    if (data.hpCode == "DRCV")
                //    {
                //        //if (tbl.Rows[0][0].ToString() == "6")
                //        if (data.provClass == "6")
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //    }
                //    dentist = "0";
                //    gp = "1";
                //    break;

                //case "15":
                //    //if (tbl.Rows[0][0].ToString() == "6")
                //    if (data.hpCode == "MDV")
                //    {
                //        if (data.provClass == "6")
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //    }
                //    else if (data.hpCode == "DRCV")
                //    {
                //        if (data.provClass == "6")
                //        {
                //            feeset = "14962";
                //        }
                //        else
                //        {
                //            feeset = "14972";
                //        }
                //    }
                //    dentist = "0";
                //    gp = "1";
                //    break;
                //    default:
                //        dentist = "0";
                //        gp = "1";
                //        break;
                //}

                //dtbl = new DataTable();
                //And a.provid ='{data.provid}'

                //SqlConnection cn1 = new SqlConnection(_drcConnectionString);
                //if (cn1.State != ConnectionState.Open)
                //{
                //    cn1.Open();
                //}

                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.Voucher_Diag_Settings "; //WHERE EnabledDentist = '{dentist}' and EnabledGP = '{gp}' and SettingId='DiagCodes'";
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cn.Dispose();
                cmd.Dispose();

                bool stop = false;
                foreach (DataRow row in dt.Rows)
                {
                    string[] specs = row["Spec"].ToString().Split(';');
                    foreach (string spec in specs)
                    {
                        if (spec.Trim() == data.provSpec.Trim())
                        {
                            string[] codes = row["SettingDesc"].ToString().Split(';');
                            foreach (string item in codes)
                            {
                                ClaimCodesModel c = new ClaimCodesModel();
                                c.diag = item;
                                list.Add(c);
                            }
                            stop = true;
                            break;
                        }
                    }

                    if (stop)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetVoucherRedemtionHistory")]
        public List<ClaimCodesModel> GetVoucherRedemtionHistory([FromBody] DefaultCodeModel data)
        {
            List<ClaimCodesModel> list = new List<ClaimCodesModel>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    cmd.Connection = cn;

                    cmd.CommandText = $" SELECT SUBSTRING(Mediref,0,CHARINDEX('_',Mediref,0) ) as VoucherNo, provid, " +
                                      $" SUBSTRING(memberId, 3, CHARINDEX('-', memberId, 0)-3) as PatientId, " +
                                      $" net as VoucherTotal,createdate from {MediDatabase}..Claim_Header " +
                                      $" where LEN(Mediref) > 4 and provid = '{data.provid}' order by createdate ";
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ClaimCodesModel c = new ClaimCodesModel();
                        c.mediRef = row["VoucherNo"].ToString();
                        c.membId = row["PatientId"].ToString();
                        c.total = row["VoucherTotal"].ToString();
                        c.drcRef = row["createdate"].ToString();
                        list.Add(c);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }
          
        [HttpPost]
        public bool SaveDataToDAT(string id, string prefix, string hpCode)
        {
            bool succes = false;
            string sex = "";
            string name = "";
            string surname = "";
            string dob = "";
            string idno = "";

            RecordType0 rec0 = new RecordType0();
            RecordType1 rec1 = new RecordType1();
            List<RecordType2> rec2 = new List<RecordType2>();
            RecordType5 rec5 = new RecordType5();

            try
            {
                SqlConnection cn = new SqlConnection(_mediConnectionString);

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                rec0.TYPE = 0;
                rec0.CONTROL = "BATCH";
                rec0.DATECREATED = DateTime.Now.ToString("yyyy/MM/dd");

                // Create a record0 (file record)
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = new SqlConnection(_drcConnectionString);
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                DataTable dt = new DataTable();

                cmd.Parameters.Add(new SqlParameter("@date", rec0.DATECREATED));
                cmd.Parameters.Add(new SqlParameter("@control", rec0.CONTROL));
                cmd.Parameters.Add(new SqlParameter("@type", rec0.TYPE));

                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record0 (TYPE,CONTROL,FILLER1,PROVIDER,CLRHOUSE,FILLER3," +
                                    $"DATECREATED,FILEER4,DATEACCESSED,BATCHNO,COMPLETED) " +
                                    $" OUTPUT INSERTED.FileID " +
                                    $" VALUES (@type,'','','','','',@date,'',NULL,NULL,0) ";

                rec0.Rec0ID = Convert.ToInt32(cmd.ExecuteScalar());


                // Get data from voucher claim header table to create datfile Record1 (claim_masters record)
                cmd = new SqlCommand();
                cmd.Connection = cn;
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                cmd.Parameters.Add(new SqlParameter("@id", id));
                cmd.CommandText = $"SELECT * FROM {MediDatabase}.dbo.Claim_Header WHERE id = @id";

                dt.Load(cmd.ExecuteReader());

                DataTable tbl = new DataTable();

                foreach (DataRow row in dt.Rows)
                {
                    rec1.Rec0ID = rec0.Rec0ID;
                    rec1.PLACE = "11";
                    //rec1.PHCODE = "P";
                    rec1.PHCODE = row["PhCode"].ToString();      
                    rec1.EZCAPPROV = row["provid"].ToString();
                    tbl = new DataTable();
                    cmd = new SqlCommand();
                    cmd.Connection = cn;
                    if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                    cmd.CommandText = $" SELECT TOP 1pv.PROVID, pv.VENDOR, pm.LASTNAME, pv.VENDOR +' - ' +pm.LASTNAME as DisplayName " +
                                      $" FROM {DRCDatabase}.dbo.PROV_VENDINFO AS pv " +
                                      $" INNER JOIN {DRCDatabase}.dbo.PROV_MASTERS AS pm ON pv.VENDOR = pm.PROVID " +
                                      $" WHERE pv.THRUDATE is null and(pv.PROVID = '{row["provid"].ToString()}') ";
                    tbl.Load(cmd.ExecuteReader());
                    if (tbl.Rows.Count > 0)
                    {
                        rec1.VENDORID = tbl.Rows[0]["VENDOR"].ToString();
                    }
                    else
                    {
                        rec1.VENDORID = row["provid"].ToString();
                    }

                    rec1.PROVCLAIM = row["mediRef"].ToString();

                    //if (hpCode == "MDV")
                    //{
                    //    rec1.EXT_SWITCH = "MEDIVOUCHWEB";
                    //}
                    //else
                    //{
                    //    rec1.EXT_SWITCH = hpCode + "VOUCHWEB";
                    //}
                    rec1.EXT_SWITCH = prefix + "VOUCHWEB";

                    rec1.CLAIMTYPE = "P";
                    rec1.CREATEBY = row["createby"].ToString();
                    rec1.DATERECD = row["createdate"].ToString().Substring(0, 10);
                    idno = row["memberId"].ToString();
                    name = row["firstname"].ToString();
                    surname = row["lastname"].ToString();
                    dob = row["dateofbirth"].ToString();
                }

                //cmd.CommandText = $"EXEC DRCJ.dbo.CreateMediWalletMemb '{surname}','{name}','{dob}','{idno}','{rec1.DATERECD}' ";
                //       int success = Convert.ToInt32(cmd.ExecuteScalar());

                //cmd.CommandText = $"SELECT MEMBID FROM {DRCDatabase}.dbo.MEMB_MASTERS WHERE MEMBID = 'MV{idno}' " +  // Jaco comment out 2022-06-02
                //                  $" AND HPCODE = 'MDV'";
                //cmd.Connection = new SqlConnection(_drcConnectionString);
                //cmd.Connection.Open();
                //dt = new DataTable();
                //dt.Load(cmd.ExecuteReader());
                //rec1.EZCAPMEMB = dt.Rows[0][0].ToString();

                //rec1.EZCAPMEMB = prefix + idno; // Jaco 2022-06-02
                rec1.EZCAPMEMB = idno; // Jaco 2022-08-01

                // Create Record1 (claim_masters record)
                cmd = new SqlCommand();
                cmd.Connection = cn;
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                cmd.Parameters.Add(new SqlParameter("@type", 1));
                cmd.Parameters.Add(new SqlParameter("@phcode", rec1.PHCODE));
                cmd.Parameters.Add(new SqlParameter("@ref", rec1.PROVCLAIM));
                cmd.Parameters.Add(new SqlParameter("@prov", rec1.EZCAPPROV));
                cmd.Parameters.Add(new SqlParameter("@memb", rec1.EZCAPMEMB));
                cmd.Parameters.Add(new SqlParameter("@place", rec1.PLACE));
                cmd.Parameters.Add(new SqlParameter("@claimtype", rec1.CLAIMTYPE));
                cmd.Parameters.Add(new SqlParameter("@vend", rec1.VENDORID));
                cmd.Parameters.Add(new SqlParameter("@daterec", rec1.DATERECD));
                cmd.Parameters.Add(new SqlParameter("@ext", rec1.EXT_SWITCH));
                cmd.Parameters.Add(new SqlParameter("@fileid", rec1.Rec0ID));
                cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd")));
                cmd.Parameters.Add(new SqlParameter("@by", rec1.CREATEBY));

                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record1 (TYPE,PHCODE,PROVCLAIM,EZCAPUATH,EZCAPPROV" +
                                    $",EZCAPMEMB,PLACE,OUTCOME,EDIREF,CASENUM,UNITS,CLAIMTYPE,REFPROVID,VENDORID,DATERECD,BATCH_NO,[EXT SWITCH]," +
                                    $"FileID,CREATEDATE,CREATEBY) " +
                                    $" OUTPUT INSERTED.Rec1ID " +
                                    $" VALUES (@type,@phcode,@ref,'',@prov,@memb,@place,'','','','',@claimtype,'',@vend,@daterec,''," +
                                    $"@ext,@fileid,@date,@by)";

                rec1.Rec1ID = Convert.ToInt32(cmd.ExecuteScalar());

                // Get data from voucher claim detail table to create datfile Record2's (claim_details records)
                cmd = new SqlCommand();
                cmd.Connection = cn;
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                cmd.Parameters.Add(new SqlParameter("@id", id));
                cmd.CommandText = $"SELECT * FROM  {MediDatabase}.dbo.Claim_Detail WHERE headId = @id";
                dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                // Create Records2's
                SqlConnection conDrc = new SqlConnection(_drcConnectionString);
                foreach (DataRow row in dt.Rows)
                {
                    RecordType2 r = new RecordType2();
                    rec5.DIAG = row["DiagCode"].ToString();
                    rec5.DIAGREF = 1;
                    rec5.TYPE = 5;
                    rec5.FieldID = rec1.Rec1ID;
                    r.TYPE = 2;
                    //r.PHCODE = "P";
                    r.PHCODE = row["PhCode"].ToString(); 
                    r.TODATE = row["CreateDate"].ToString().Substring(0, 10);
                    r.FROMDATE = row["CreateDate"].ToString().Substring(0, 10);
                    r.PROCCODE = row["ProcCode"].ToString();
                    r.QTY = row["qty"].ToString();
                    r.BILLED = row["PayableByProv"].ToString();
                    r.DIAGREF = 1;
                    r.Rec1ID = rec1.Rec1ID;
                    if (DBNull.Value.Equals(row["ToothNo"]))
                    {
                        r.CLINICALCODE1 = "";
                        r.CLINICALCODE2 = "";
                        r.CLINICALCODE3 = "";
                        r.CLINICALCODE4 = "";
                        r.CLINICALCODE5 = "";
                        r.CLINICALCODE6 = "";
                        r.CLINICALCODE7 = "";
                        r.CLINICALCODE8 = "";
                    }
                    else
                    {
                        string[] tooths = new string[row["ToothNo"].ToString().Split(",").Length];
                        tooths = row["ToothNo"].ToString().Split(",");
                        if (tooths.Length == 1)
                        {
                            r.CLINICALCODE1 = tooths[0];
                        }
                        else if (tooths.Length == 2)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                        }
                        else if (tooths.Length == 3)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                        }
                        else if (tooths.Length == 4)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                            r.CLINICALCODE4 = tooths[3];
                        }
                        else if (tooths.Length == 5)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                            r.CLINICALCODE4 = tooths[3];
                            r.CLINICALCODE5 = tooths[4];
                        }
                        else if (tooths.Length == 6)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                            r.CLINICALCODE4 = tooths[3];
                            r.CLINICALCODE5 = tooths[4];
                            r.CLINICALCODE6 = tooths[5];
                        }
                        else if (tooths.Length == 7)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                            r.CLINICALCODE4 = tooths[3];
                            r.CLINICALCODE5 = tooths[4];
                            r.CLINICALCODE6 = tooths[5];
                            r.CLINICALCODE7 = tooths[6];
                        }
                        else if (tooths.Length == 8)
                        {
                            r.CLINICALCODE1 = tooths[0];
                            r.CLINICALCODE2 = tooths[1];
                            r.CLINICALCODE3 = tooths[2];
                            r.CLINICALCODE4 = tooths[3];
                            r.CLINICALCODE5 = tooths[4];
                            r.CLINICALCODE6 = tooths[5];
                            r.CLINICALCODE7 = tooths[6];
                            r.CLINICALCODE8 = tooths[7];
                        }


                    }

                    cmd = new SqlCommand();
                    cmd.Connection = conDrc;
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record2 (TYPE,PHCODE,FROMDATE,TODATE,PROCCODE," +
                                        $"CURR_MODCODE,CURR_MODAMOUNT,MODCODE,MODAMOUNT,MODCODE2,MODAMOUNT2,MODCODE3,MODAMOUNT3,MODCODE4," +
                                        $"MODAMOUNT4,QTY,BILLED,DIAGREF,BUNDLER,BUNDLERTYP,COPAY,MANDISCOUNT,TRANSACT_NO,PROCCODE_DESC," +
                                        $"CLINICALCODE1,CLINICALCODE2,CLINICALCODE3,CLINICALCODE4,CLINICALCODE5,CLINICALCODE6,CLINICALCODE7," +
                                        $"CLINICALCODE8,MEMO1,MEMO2,MEMO3,MEMO4,MEMO5,MEMO6,MEMO7,MEMO8,MEMO9,MEMO10,Rec1ID)" +
                                        $" VALUES ('{r.TYPE}','{r.PHCODE}','{r.FROMDATE}','{r.TODATE}','{r.PROCCODE}','','','','','','','','','','','{r.QTY}'" +
                                        $",'{r.BILLED}'," +
                                        $"'{r.DIAGREF}','','','','','','','{r.CLINICALCODE1}','{r.CLINICALCODE2}','{r.CLINICALCODE3}'," +
                                        $"'{r.CLINICALCODE4}','{r.CLINICALCODE5}'," +
                                        $"'{r.CLINICALCODE6}','{r.CLINICALCODE7}','{r.CLINICALCODE8}','','','',''," +
                                        $"'','','','','','','{r.Rec1ID}')";
                    //cmd.Connection = new SqlConnection(_drcConnectionString);
                    if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                    cmd.ExecuteNonQuery();
                }

                conDrc.Close();
                conDrc = null;

                // Create Record5 (claim diags)
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Record5 (TYPE,DIAGREF,DIAG,Rec1ID) " +
                                  $"VALUES ('5','1','{rec5.DIAG}','{rec5.FieldID}')";
                cmd.Connection = new SqlConnection(_drcConnectionString);
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                cmd.ExecuteNonQuery();


                // Update Record0 to completed
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = $"UPDATE {PamcPortalDatabase}..Record0 set COMPLETED = 1 WHERE FileID = '{rec0.Rec0ID}'";
                cmd.Connection = new SqlConnection(_drcConnectionString);
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }
                cmd.ExecuteNonQuery();
                cmd = null;

                cn.Close();
                cn = null;

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }

            return succes;
        }

        public static void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "VOUCHER";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\VOUCHER\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {
            }
        }

        public void LogApiCall(string voucherId, string provId, string pamc_drc, string validate_redeem, string apiKey, string sentBody, string resultBody)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_mediConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@voucherId", voucherId));
                    cmd.Parameters.Add(new SqlParameter("@provId", provId));
                    cmd.Parameters.Add(new SqlParameter("@pamc_drc", pamc_drc));
                    cmd.Parameters.Add(new SqlParameter("@validate_redeem", validate_redeem));
                    cmd.Parameters.Add(new SqlParameter("@apiKey", apiKey));
                    cmd.Parameters.Add(new SqlParameter("@sentBody", sentBody));
                    cmd.Parameters.Add(new SqlParameter("@resultBody", resultBody));

                    cmd.CommandText =
                        $"INSERT INTO {MediDatabase}.dbo.API_LOG (VOUCHERID, PROVID, PAMCDRC, REDEEMVALIDATE, APIKEY, SENTBODY, RECEIVEDBODY, CREATEDATE) \n" +
                        $"VALUES(@voucherId, @provId, @pamc_drc, @validate_redeem, @apiKey, @sentBody, @resultBody, GETDATE())";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}

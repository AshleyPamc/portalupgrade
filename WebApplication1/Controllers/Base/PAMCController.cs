﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace NEWPAMCPORTAL.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class PAMCController : ControllerBase
    {
        public static IWebHostEnvironment _env;
        public IConfiguration _configuration;
        
        public static string _drcConnectionString;
        public static string _reportingConnectionString;
        public static string _dataWarehouseConnectionString;
        public static string _portalConnectionString;
        public static string _mediConnectionString;

        internal SqlConnection _drcConnection = null;
        internal SqlConnection _reportingConnection = null;
        internal SqlConnection _dataWarehouseConnection;
        internal SqlConnection _portalConnection = null;        
        internal SqlConnection _mediConnection = null;        

        public static string _RemmitanceWebSvcConnection;
        public static string _EmailWebSvcConnection;
        public static string _ReportWebSvcConnection;
        public static string _AuthWebSvcConnection;

        public static bool _allowAdmin;

        public static bool _isTest;
        public static string _testEmails;

        public static string _authId;
        public static string _releaseDate;      

        public PAMCController(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
            
            _drcConnectionString = _configuration.GetSection("ConnectionStrings:DrcConnection").Value;
            _reportingConnectionString = _configuration.GetSection("ConnectionStrings:ReportingConnection").Value;
            _dataWarehouseConnectionString = _configuration.GetSection("ConnectionStrings:DataWarehouseConnection").Value;
            _portalConnectionString = _configuration.GetSection("ConnectionStrings:PortalConnection").Value;
            _mediConnectionString = _configuration.GetSection("ConnectionStrings:MediConnection").Value;

            _drcConnection = new SqlConnection(_drcConnectionString);
            _reportingConnection = new SqlConnection(_reportingConnectionString);
            _dataWarehouseConnection = new SqlConnection(_dataWarehouseConnectionString);
            _portalConnection = new SqlConnection(_portalConnectionString);           
            _mediConnection = new SqlConnection(_mediConnectionString);

            _RemmitanceWebSvcConnection = _configuration.GetSection("WebConnections:RemittanceService").Value;
            _EmailWebSvcConnection = _configuration.GetSection("WebConnections:EmailService").Value;
            _ReportWebSvcConnection = _configuration.GetSection("WebConnections:ReportService").Value;
            _AuthWebSvcConnection = _configuration.GetSection("WebConnections:AuthService").Value;

            _allowAdmin = Convert.ToBoolean(_configuration.GetSection("AllowAdminLogin:allowed").Value);

            _isTest = Convert.ToBoolean(_configuration.GetSection("TestEnvironment:isTest").Value);
            _testEmails = _configuration.GetSection("TestEnvironment:testEmails").Value;

            _releaseDate = _configuration.GetSection("ReleaseDate:date").Value;
        }

        public string DRCDatabase
        {
            get
            {
                return this._drcConnection.Database;
            }
        }        
        public string ReportingDatabase
        {
            get
            {
                return this._reportingConnection.Database;
            }
        }
        public string DataWarehouseDatabase
        {
            get
            {
                return this._dataWarehouseConnection.Database;
            }
        }
        public string PamcPortalDatabase
        {
            get
            {
                return this._portalConnection.Database;
            }
        }
        public string MediDatabase
        {
            get
            {
                return this._mediConnection.Database;
            }
        }

    }
}
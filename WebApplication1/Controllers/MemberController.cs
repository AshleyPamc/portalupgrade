﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using NEWPAMCPORTAL.Models;
using System.Text;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : Base.PAMCController
    {
        public MemberController(IWebHostEnvironment env, IConfiguration con):base(env,con)
        {

        }


        [HttpPost]
        [Route("MemberDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<MemberEligibilityModel> MemberDetails([FromBody] MemberEligibilityModel model)
        {
            string log = $"MEMBER SEARCH BEGIN {DateTime.Now}";
            log += $"{model}";
            DataTable dt = new DataTable();
            List<MemberEligibilityModel> ListOfMembers = new List<MemberEligibilityModel>();
            string[] lobcodeList;
            StringBuilder sb = new StringBuilder();

            try
            {
                if (model.HPCODE == "000")
                {
                    model.HPCODE = "";
                    DataTable tbl = new DataTable();
                    SqlConnection con = new SqlConnection();
                    SqlCommand comm = new SqlCommand();

                    con.ConnectionString = _portalConnectionString;
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    comm.Connection = con;

                    // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                    //comm.CommandText = $"SELECT hpcode FROM UserLobCodesHpCodes Where username = @user";  

                    // Jaco 2023-09-11
                    comm.CommandText = $" SELECT hp.hpcode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " +  // Checked
                                       $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                       $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                       $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = @user ";

                    comm.Parameters.Add("@user", SqlDbType.VarChar).Value = model.USER;

                    using (SqlDataReader row = comm.ExecuteReader())
                    {
                        tbl.Load(row);
                    }
                    foreach (DataRow dr in tbl.Rows)
                    {
                        model.HPCODE = dr["hpcode"].ToString();
                    }
                    con.Close();
                }
                if (model.LOBCODE == "")
                {
                    model.LOBCODE = null;
                }
                log += $"DATEOFBIRTH---{model.BIRTH}";
                if (model.LOBCODE != null)
                {
                    lobcodeList = model.LOBCODE.Split(',');
                    foreach (var code in lobcodeList)
                    {
                        string x = "'" + code + "'" + ",";
                        sb.Append(x);
                    }
                    string lobCode = sb.ToString();

                    model.LOBCODE = lobCode.Remove(lobCode.Length - 1);
                }

                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region Build WhereClause
                        if (model.MEMBID != "")
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = model.MEMBID;
                        }
                        else
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.HPCODE != "")
                        {
                            cmd.Parameters.Add("@plan", SqlDbType.VarChar, 5).Value = model.HPCODE;
                        }
                        else
                        {
                            cmd.Parameters.Add("@plan", SqlDbType.VarChar, 5).Value = DBNull.Value;
                        }
                        if (model.FIRSTNM != "")
                        {
                            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = model.FIRSTNM;
                        }
                        else
                        {
                            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.LASTNM != "")
                        {
                            cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = model.LASTNM;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if ((model.BIRTH != null) && (model.BIRTH != "1900/01/01") && (model.BIRTH != "1900-01-01"))
                        {
                            cmd.Parameters.Add("@birth", SqlDbType.DateTime2).Value = model.BIRTH;
                            log += $"DATEOFBIRTH STEPPED INTO IF---{model.BIRTH}";
                        }
                        else
                        {
                            cmd.Parameters.Add("@birth", SqlDbType.DateTime2).Value = DBNull.Value;
                            log += $"DATEOFBIRTH MISED IF---{model.BIRTH}";
                        }
                        if (model.LOBCODE != null)
                        {
                            cmd.Parameters.AddWithValue("@lobCode", SqlDbType.VarChar).Value = model.LOBCODE;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.USER != "")
                        {
                            cmd.Parameters.Add("@user", SqlDbType.VarChar).Value = model.USER;
                        }
                        else
                        {
                            cmd.Parameters.Add("@user", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        string WhereClause = "";
                        
                        if (model.USERTYPE != 3)
                        {
                            if (model.MEMBID != "")
                            {                                
                                WhereClause = "where ( a.SUBSSN =  @membid  )";                                
                            }
                        }
                        if (model.USERTYPE != 3)
                        {
                            if (model.HPCODE != "")
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "a.HPCODE = '" + model.HPCODE + "'";                                    
                                }
                                else
                                {
                                    WhereClause = "where a.HPCODE = '" + model.HPCODE + "'";                                    
                                }
                            }
                        }
                        if (model.FIRSTNM != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.FIRSTNM = @name";                                
                            }
                            else
                            {
                                WhereClause = "where a.FIRSTNM = @name";                               
                            }
                        }
                        if (model.LASTNM != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.LASTNM = @lastName";                               
                            }
                            else
                            {
                                WhereClause = "where a.LASTNM = @lastName";                               
                            }
                        }
                        if ((model.BIRTH != null) && (model.BIRTH != "1900/01/01") && (model.BIRTH != "1900-01-01"))
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.BIRTH = @birth";                                
                            }
                            else
                            {
                                WhereClause = "where a.BIRTH = @birth";                                
                            }
                        }                        
                        if (model.USERTYPE != 1) 
                        {
                            if (model.USER != "")
                            {
                                if (WhereClause != "")
                                {
                                    //WhereClause = WhereClause + " AND " + "UserLobCodesHpCodes.username = '" + model.USER + "'"; // Jaco comment out 2023-09-18
                                    WhereClause = WhereClause + " AND " + "X.username = '" + model.USER + "'";

                                }
                                else
                                {
                                    //WhereClause = "where UserLobCodesHpCodes.username = '" + model.USER + "'"; // Jaco comment out 2023-09-18
                                    WhereClause = "where X.username = '" + model.USER + "'";
                                }
                            }
                        }

                        #endregion endregion

                        cmd.CommandType = CommandType.Text;
                        string sql = "";
                        if (model.USERTYPE == 3)
                        {
                            // Jaco comment out 2023-09-18 Databases hardcoded in sp and views
                            //sql = "SELECT username, lobcode, HPCODE, memberPrefix, dbo.GenerateSubSsn(memberPrefix,dbo.RemoveDependantDash(@membnum)) as possiblesubssn \r\n" +
                            //        "into #x\r\n" +
                            //        "FROM UserLobCodesHpCodes\r\n" +
                            //        "WHERE (username = @user)\r\n" +
                            //        "SELECT DISTINCT \r\n" +
                            //        " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.EMAIL, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                            //        " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS, b.FROMDATE, b.TODATE\r\n" +
                            //        "FROM MemberEligibility AS a INNER JOIN\r\n" +
                            //        " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN INNER JOIN\r\n" +
                            //        " #x UserLobCodesHpCodes ON b.HPCODE = UserLobCodesHpCodes.HPCODE  and UserLobCodesHpCodes.possiblesubssn = b.subssn\r\n" +
                            //        $"{WhereClause}" +
                            //        "drop table #x\r\n";

                            sql = Utillity.MembershipQueryClient(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause); // Jaco 2023-09-18 - tested from membereligibility screen with client login

                            log += $"{DateTime.Now}----MEMBERSEARCH-----{sql}-----";
                        }
                        else
                        {
                            // Jaco comment out 2023-09-18 Databases hardcoded in sp and views
                            //sql = "declare @membid varchar(20)\r\n" +
                            //           "declare @hpCode varchar(10)\r\n" +
                            //           "declare @prefix varchar(10)\r\n" +
                            //           "set @hpCode= @plan\r\n" +
                            //           "set @membid = @membnum\r\n" +
                            //           "set @prefix = dbo.GetMemberPrefix(@hpCode)\r\n" +
                            //           "set @membid = dbo.RemoveDependantDash(@membid)\r\n" +
                            //           "set @membid = dbo.GenerateSubSsn(@prefix,@membid)\r\n" +
                            //           "SELECT DISTINCT \r\n" +
                            //           " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.EMAIL, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                            //           " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS, b.FROMDATE, b.TODATE\r\n" +
                            //           "FROM MemberEligibility AS a INNER JOIN\r\n" +
                            //           " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN \r\n" +
                            //           $"{WhereClause}";

                            sql = Utillity.MembershipQueryProv(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause); // Jaco 2023-09-18 

                        }

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = 60;
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            MemberEligibilityModel member = new MemberEligibilityModel();
                            member.waitAndSpecExcls = new List<MemberWaitSpecExlModel>();

                            member.MEMBID = dr["MEMBID"].ToString().Replace(" ", "");
                            member.RLSHIP = Convert.ToInt16(dr["RLSHIP"]);
                            member.LASTNM = dr["LASTNM"].ToString();
                            member.FIRSTNM = dr["FIRSTNM"].ToString();
                            member.EMAIL = dr["EMAIL"].ToString();
                            member.HPFROMDT = Convert.ToDateTime(dr["HPFROMDT"]);
                            member.HPCODE = dr["HPCODE"].ToString();
                            member.OPT = dr["OPT"].ToString();
                            member.OPFROMDT = Convert.ToDateTime(dr["OPFROMDT"]);
                            if (dr["OPTHRUDT"] != DBNull.Value)
                            {
                                member.OPTHRUDT = Convert.ToDateTime(dr["OPTHRUDT"]).ToString("yyyy/MM/dd");
                            }
                            else
                            {
                                member.OPTHRUDT = "N/A";
                            }
                            member.BIRTH = dr["BIRTH"].ToString().Substring(0, 10);
                            member.SUBSSN = dr["SUBSSN"].ToString();
                            member.RELATION = dr["RELATION"].ToString();
                            member.DEPENDANT = dr["DEPENDANT"].ToString();
                            member.HPName = dr["HPName"].ToString();
                            member.LOBCODE = dr["LOBCODE"].ToString();
                            member.term_code = dr["term_code"].ToString();
                            member.MemberStatus = dr["MemberStatus"].ToString();
                            member.consultations = Convert.ToDecimal(dr["CONSULTATIONS"]);
                            if (dr["FROMDATE"] != DBNull.Value)
                            {
                                member.FROMDATE = Convert.ToDateTime(dr["FROMDATE"]).ToString("yyyy/MM/dd");
                            }
                            else
                            {
                                member.FROMDATE = "N/A";
                            }
                            if (dr["TODATE"] != DBNull.Value)
                            {
                                member.TODATE = Convert.ToDateTime(dr["TODATE"]).ToString("yyyy/MM/dd");
                            }
                            else
                            {
                                member.TODATE = "N/A";
                            }

                            // Do Wait Periods and Special Exclusions for member/dependant //Jaco 2022-11-09
                            if(member.FROMDATE != "N/A" || member.TODATE != "N/A")
                            {
                                SqlConnection sqlConWE = new SqlConnection(_drcConnectionString);
                                SqlCommand cmdWE = new SqlCommand();
                                DataTable dtWE = new DataTable();
                                cmdWE.Connection = sqlConWE;
                                cmdWE.Parameters.Add("@membid", SqlDbType.VarChar).Value = member.MEMBID;
                                cmdWE.CommandText =
                                 " SELECT MEMBID, 'GENERAL WAIT PERIOD' as [DESCRIPTION], FROMDATE, TODATE " +
                                $" FROM {DRCDatabase}.dbo.MEMB_WAIT " +
                                 " WHERE MEMBID = @membid AND isnull(TERMSTATUS, 0) = 0 " +
                                 " UNION ALL " +
                                 " SELECT MS.MEMB_ID as MEMBID, SS.DESCR as [DESCRIPTION], MS.FROM_DATE as FROMDATE, MS.TO_DATE as TODATE " +
                                $" FROM {DRCDatabase}.dbo.MEMB_SPECEXL MS " +
                                $" LEFT OUTER JOIN {DRCDatabase}.dbo.SPECEXL_SETS SS on (MS.SPECEXL_ID = SS.SPECEXL_ID) " +
                                 " WHERE MS.MEMB_ID = @membid ";

                                if (sqlConWE.State != ConnectionState.Open) sqlConWE.Open();
                                dtWE.Load(cmdWE.ExecuteReader());

                                sqlConWE.Close();
                                sqlConWE = null;
                                cmdWE = null;

                                foreach (DataRow row in dtWE.Rows)
                                {
                                    MemberWaitSpecExlModel waitAndSpecExcl = new MemberWaitSpecExlModel();
                                    waitAndSpecExcl.membId = row["MEMBID"].ToString().Trim();
                                    waitAndSpecExcl.dependant = member.DEPENDANT;
                                    waitAndSpecExcl.description = row["DESCRIPTION"].ToString().Trim();
                                    waitAndSpecExcl.fromDate = Convert.ToDateTime(row["FROMDATE"]).ToString("yyyy/MM/dd");
                                    waitAndSpecExcl.toDate = Convert.ToDateTime(row["TODATE"]).ToString("yyyy/MM/dd");                                    
                                    member.waitAndSpecExcls.Add(waitAndSpecExcl);
                                }
                            }

                            ListOfMembers.Add(member);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return ListOfMembers;
        }

        [HttpPost]
        [Route("MemberDetailsBenefitScreen")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<MemberEligibilityModel> MemberDetailsBenefitScreen([FromBody] MemberEligibilityModel model)
        {
            string log = $"MEMBER SEARCH BEGIN {DateTime.Now}";
            log += $"{model}";
            DataTable dt = new DataTable();
            List<MemberEligibilityModel> ListOfMembers = new List<MemberEligibilityModel>();
            string[] lobcodeList;
            StringBuilder sb = new StringBuilder();

            try
            {
                if (model.HPCODE == "000")
                {
                    model.HPCODE = "";
                    DataTable tbl = new DataTable();
                    SqlConnection con = new SqlConnection();
                    SqlCommand comm = new SqlCommand();

                    con.ConnectionString = _portalConnectionString;
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    comm.Connection = con;

                    // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                    //comm.CommandText = $"SELECT hpcode FROM UserLobCodesHpCodes Where username = @user";  
                    
                    // Jaco 2023-09-11
                    comm.CommandText = $" SELECT hp.hpcode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " +
                                       $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                       $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                       $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = @user ";

                    comm.Parameters.Add("@user", SqlDbType.VarChar).Value = model.USER;

                    using (SqlDataReader row = comm.ExecuteReader())
                    {
                        tbl.Load(row);

                    }
                    foreach (DataRow dr in tbl.Rows)
                    {
                        model.HPCODE = dr["hpcode"].ToString();
                    }
                    con.Close();
                }
                if (model.LOBCODE == "")
                {
                    model.LOBCODE = null;
                }
                log += $"DATEOFBIRTH---{model.BIRTH}";
                if (model.LOBCODE != null)
                {
                    lobcodeList = model.LOBCODE.Split(',');
                    foreach (var code in lobcodeList)
                    {
                        string x = "'" + code + "'" + ",";
                        sb.Append(x);

                    }
                    string lobCode = sb.ToString();

                    model.LOBCODE = lobCode.Remove(lobCode.Length - 1);
                }



                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region Build WhereClause
                        if (model.MEMBID != "")
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = model.MEMBID;
                        }
                        else
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.HPCODE != "")
                        {
                            cmd.Parameters.Add("@plan", SqlDbType.VarChar, 5).Value = model.HPCODE;
                        }
                        else
                        {
                            cmd.Parameters.Add("@plan", SqlDbType.VarChar, 5).Value = DBNull.Value;
                        }
                        if (model.FIRSTNM != "")
                        {
                            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = model.FIRSTNM;
                        }
                        else
                        {
                            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.LASTNM != "")
                        {
                            cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = model.LASTNM;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if ((model.BIRTH != null) && (model.BIRTH != "1900/01/01") && (model.BIRTH != "1900-01-01"))
                        {
                            cmd.Parameters.Add("@birth", SqlDbType.DateTime2).Value = model.BIRTH;
                            log += $"DATEOFBIRTH STEPPED INTO IF---{model.BIRTH}";
                        }
                        else
                        {
                            cmd.Parameters.Add("@birth", SqlDbType.DateTime2).Value = DBNull.Value;
                            log += $"DATEOFBIRTH MISED IF---{model.BIRTH}";
                        }
                        if (model.LOBCODE != null)
                        {
                            cmd.Parameters.AddWithValue("@lobCode", SqlDbType.VarChar).Value = model.LOBCODE;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.USER != "")
                        {
                            cmd.Parameters.Add("@user", SqlDbType.VarChar).Value = model.USER;
                        }
                        else
                        {
                            cmd.Parameters.Add("@user", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        string WhereClause = "";
                        //string UnionWhereClause = "";
                        if (model.USERTYPE != 3)
                        {
                            if (model.MEMBID != "")
                            {
                                //WhereClause = "where MEMBID ='" + membercredentials.MEMBID + "'";
                                WhereClause = "where ( a.SUBSSN =  @membid  )";
                                //UnionWhereClause = "where ( a.SUBSSN =  @membid  )";
                            }
                        }
                        if (model.USERTYPE != 3)
                        {
                            if (model.HPCODE != "")
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "a.HPCODE = '" + model.HPCODE + "'";
                                    //UnionWhereClause = UnionWhereClause + " AND " + "HPCODE = @plan";
                                }
                                else
                                {
                                    WhereClause = "where a.HPCODE = '" + model.HPCODE + "'";
                                    //UnionWhereClause = "where HPCODE = '@plan";
                                }
                            }
                        }
                        if (model.FIRSTNM != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.FIRSTNM = @name";
                                //UnionWhereClause = UnionWhereClause + " AND " + "FIRSTNM <> @name";
                            }
                            else
                            {
                                WhereClause = "where a.FIRSTNM = @name";
                                //UnionWhereClause = "where FIRSTNM <> @name";
                            }
                        }
                        if (model.LASTNM != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.LASTNM = @lastName";
                                //UnionWhereClause = UnionWhereClause + " AND " + "LASTNM <> @lastName";
                            }
                            else
                            {
                                WhereClause = "where a.LASTNM = @lastName";
                                //UnionWhereClause = "where LASTNM <> @lastName";
                            }
                        }
                        if ((model.BIRTH != null) && (model.BIRTH != "1900/01/01") && (model.BIRTH != "1900-01-01"))
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "a.BIRTH = @birth";
                                //UnionWhereClause = UnionWhereClause + " AND " + "BIRTH <> @birth";
                            }
                            else
                            {
                                WhereClause = "where a.BIRTH = @birth";
                                //UnionWhereClause = "where BIRTH <> @birth";
                            }
                        }
                        if (model.USERTYPE != 1)
                        {
                            if (model.USER != "")
                            {
                                if (WhereClause != "")
                                {
                                    //WhereClause = WhereClause + " AND " + "UserLobCodesHpCodes.username = '" + model.USER + "'"; // Jaco comment out 2023-09-20
                                    WhereClause = WhereClause + " AND " + "X.username = '" + model.USER + "'";
                                }
                                else
                                {
                                    //WhereClause = "where UserLobCodesHpCodes.username = '" + model.USER + "'"; // Jaco comment out 2023-09-20
                                    WhereClause = "where X.username = '" + model.USER + "'";
                                }
                            }
                        }

                        #endregion endregion



                        cmd.CommandType = CommandType.Text;
                        string sql = "";
                        if (model.USERTYPE == 3)
                        {
                            // Jaco comment out 2023-09-20 Databases hardcoded in sp and views
                            //sql = "SELECT username, lobcode, HPCODE, memberPrefix, dbo.GenerateSubSsn(memberPrefix,dbo.RemoveDependantDash(@membnum)) as possiblesubssn \r\n" +
                            //        "into #x\r\n" +
                            //        "FROM UserLobCodesHpCodes\r\n" +
                            //        "WHERE (username = @user)\r\n" +
                            //        "SELECT DISTINCT \r\n" +
                            //        " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                            //        " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS\r\n" +
                            //        "FROM MemberEligibility AS a INNER JOIN\r\n" +
                            //        " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN INNER JOIN\r\n" +
                            //        " #x UserLobCodesHpCodes ON b.HPCODE = UserLobCodesHpCodes.HPCODE  and UserLobCodesHpCodes.possiblesubssn = b.subssn\r\n" +
                            //        $"{WhereClause}" +
                            //        "drop table #x\r\n";

                            sql = Utillity.MembershipQueryClient(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause); // Jaco 2023-09-20

                            log += $"{DateTime.Now}----MEMBERSEARCH-----{sql}-----";
                        }
                        else
                        {
                            // Jaco comment out 2023-09-20 Databases hardcoded in sp and views
                            //sql = "declare @membid varchar(20)\r\n" +
                            //           "declare @hpCode varchar(10)\r\n" +
                            //           "declare @prefix varchar(10)\r\n" +
                            //           "set @hpCode= @plan\r\n" +
                            //           "set @membid = @membnum\r\n" +
                            //           "set @prefix = dbo.GetMemberPrefix(@hpCode)\r\n" +
                            //           "set @membid = dbo.RemoveDependantDash(@membid)\r\n" +
                            //           "set @membid = dbo.GenerateSubSsn(@prefix,@membid)\r\n" +
                            //           "SELECT DISTINCT \r\n" +
                            //           " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                            //           " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS\r\n" +
                            //           "FROM MemberEligibility AS a INNER JOIN\r\n" +
                            //           " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN \r\n" +
                            //           $"{WhereClause}";

                            sql = Utillity.MembershipQueryProv(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause); // Jaco 2023-09-18 
                        }


                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sql;
                        cmd.CommandTimeout = 60;
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            MemberEligibilityModel member = new MemberEligibilityModel();
                            member.MEMBID = dr["MEMBID"].ToString().Replace(" ", "");
                            member.RLSHIP = Convert.ToInt16(dr["RLSHIP"]);
                            member.LASTNM = dr["LASTNM"].ToString();
                            member.FIRSTNM = dr["FIRSTNM"].ToString();
                            member.HPFROMDT = Convert.ToDateTime(dr["HPFROMDT"]);
                            member.HPCODE = dr["HPCODE"].ToString();
                            member.OPT = dr["OPT"].ToString();
                            member.OPFROMDT = Convert.ToDateTime(dr["OPFROMDT"]);
                            if (dr["OPTHRUDT"] != DBNull.Value)
                            {
                                member.OPTHRUDT = Convert.ToDateTime(dr["OPTHRUDT"]).ToString("yyyy/MM/dd");

                            }
                            else
                            {
                                member.OPTHRUDT = "N/A";
                            }
                            member.BIRTH = dr["BIRTH"].ToString().Substring(0, 10);
                            member.SUBSSN = dr["SUBSSN"].ToString();
                            member.RELATION = dr["RELATION"].ToString();
                            member.DEPENDANT = dr["DEPENDANT"].ToString();
                            member.HPName = dr["HPName"].ToString();
                            member.LOBCODE = dr["LOBCODE"].ToString();
                            member.term_code = dr["term_code"].ToString();
                            member.MemberStatus = dr["MemberStatus"].ToString();
                            member.consultations = Convert.ToDecimal(dr["CONSULTATIONS"]);
                            ListOfMembers.Add(member);
                        }


                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return ListOfMembers;
        }

        [HttpPost]
        [Route("LoginMemberInfo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<MemberInfoModel> MemberLoginInfo([FromBody] LoginDetailsModel model)
        {
            DataTable dt = new DataTable();
            List<MemberInfoModel> memberModelList = new List<MemberInfoModel>();

            string familynum = "";

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        // Jaco comment out 2023-09-20 Databases hardcoded in MEMBERSHIP view
                        //string sqlTextToExecute = "SELECT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.MI, MEMBERSHIP.FIRSTNM, MEMBERSHIP.HPFROMDT,HealthPlanSetup.HPName AS 'HEALTH_PLAN', MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \r\n" +
                        //                          " MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, MEMBERSHIP.SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \r\n" +
                        //                          " MEMBERSHIP.MemberStatus, MEMBERSHIP.PATID\r\n" +
                        //                          "FROM MEMBERSHIP INNER JOIN\r\n" +
                        //                          " HealthPlanSetup ON MEMBERSHIP.HPCODE = HealthPlanSetup.HPCode\r\n" +
                        //                          "where PATID ='" + model.memberIdNumber + "'\r\n";

                        // Jaco 2023-09-20
                        string sqlTextToExecute = "SELECT MEMBERSHIP.MEMBID, MEMBERSHIP.RLSHIP, MEMBERSHIP.LASTNM, MEMBERSHIP.MI, MEMBERSHIP.FIRSTNM, MEMBERSHIP.HPFROMDT,HealthPlanSetup.HPName AS 'HEALTH_PLAN', MEMBERSHIP.OPT, MEMBERSHIP.OPFROMDT, \r\n" +
                                                  " MEMBERSHIP.OPTHRUDT, MEMBERSHIP.BIRTH, MEMBERSHIP.SUBSSN, MEMBERSHIP.RELATION, MEMBERSHIP.DEPENDANT, MEMBERSHIP.HPName, MEMBERSHIP.LOBCODE, MEMBERSHIP.TERM_CODE, \r\n" +
                                                  " MEMBERSHIP.MemberStatus, MEMBERSHIP.PATID\r\n" +
                                                  "FROM  (" +
                                                 $"           SELECT        mm.MEMBID, mm.RLSHIP, mm.LASTNM, mm.MI, mm.FIRSTNM, mm.EMAIL, mh.HPFROMDT, mh.HPCODE, ISNULL(OptionNameChanges.newName, mh.OPT) AS OPT, mh.OPFROMDT, CASE WHEN isnull(OPTHRUDT, GETDATE())  \n " +
                                                 $"                         >= GETDATE() THEN NULL ELSE OPTHRUDT END AS OPTHRUDT, mm.BIRTH, mm.SUBSSN, ISNULL(mr.DESCR, 'DEPENDANT') AS RELATION, RIGHT(mm.MEMBID, CHARINDEX('-', REVERSE(mm.MEMBID) + '-') - 1)  \n " +
                                                 $"                         AS DEPENDANT, mh.OPT AS HPName, {DRCDatabase}..HP_CONTRACTS.LOBCODE, mh.TERM_CODE, CASE WHEN mh.CURRHIST = 'c' AND isnull(OPTHRUDT, GETDATE()) >= GETDATE() \n " +
                                                 $"                          THEN 'ACTIVE' ELSE 'TERMINATED' END AS MemberStatus, mm.PATID, mm.BROKERID, memberprefix AS LenOfPrefix, 0 AS PosOdDash \n " +
                                                 $"           FROM {DRCDatabase}..MEMB_MASTERS AS mm INNER JOIN \n " +
                                                 $"                {DRCDatabase}..MEMB_HPHISTS AS mh ON mm.MEMBID = mh.MEMBID INNER JOIN \n " +
                                                 $"                {DRCDatabase}..HP_CONTRACTS ON mh.HPCODE = {DRCDatabase}..HP_CONTRACTS.HPCODE LEFT OUTER JOIN \n " +
                                                 $"                  (SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix \n " +
                                                 $"                   FROM            {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n " +
                                                 $"                             {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n " +
                                                 $"                   WHERE({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') \n " +
                                                 $"                  ) AS z ON {DRCDatabase}..HP_CONTRACTS.HPCODE = z.ezHpCode LEFT OUTER JOIN \n " +
                                                 $"                {PamcPortalDatabase}..OptionNameChanges ON mh.OPT = OptionNameChanges.oldName LEFT OUTER JOIN \n " +
                                                 $"                {DRCDatabase}..MEMB_RELATIONS AS mr ON mm.RLSHIP = mr.TYPE \n " +
                                                 $"           WHERE(mh.CURRHIST = 'c') \n " +
                                                 $"       ) AS MEMBERSHIP INNER JOIN\r\n" +
                                                 $" {PamcPortalDatabase}..HealthPlanSetup ON MEMBERSHIP.HPCODE = HealthPlanSetup.HPCode\r\n" +
                                                  "where PATID ='" + model.memberIdNumber + "'\r\n";

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        DataRow dr = dt.Rows[0];
                        familynum = dr["SUBSSN"].ToString();
                        memberModelList = Utillity.Dependantdetails(familynum, _portalConnectionString, DRCDatabase, PamcPortalDatabase, ReportingDatabase);
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return memberModelList;
        }

        [HttpPost]
        [Route("MemberValidation")]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel MemberValidation([FromBody] AuthModel data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@membnum", data.membno));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    // Jaco comment out 2023-09-18 - Databases harcoded in views and sp's 
                    //string sql = "SELECT username, lobcode, HPCODE, memberPrefix, dbo.GenerateSubSsn(memberPrefix,dbo.RemoveDependantDash(@membnum)) as possiblesubssn \r\n" +
                    //         "into #x\r\n" +
                    //         "FROM UserLobCodesHpCodes\r\n" +
                    //         "WHERE (username = @username)\r\n" +
                    //         "SELECT DISTINCT \r\n" +
                    //         " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                    //         " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS\r\n" +
                    //         "FROM MemberEligibility AS a INNER JOIN\r\n" +
                    //         " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN INNER JOIN\r\n" +
                    //         " #x UserLobCodesHpCodes ON b.HPCODE = UserLobCodesHpCodes.HPCODE  and UserLobCodesHpCodes.possiblesubssn = b.subssn\r\n" +
                    //         $"where UserLobCodesHpCodes.username = @username AND b.RLSHIP = '1' " +
                    //         "drop table #x\r\n";

                    // Jaco 2023-09-18
                    string WhereClause = "where X.username = @user AND b.RLSHIP = '1' ";
                    string sql = Utillity.MembershipQueryClient(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause);

                    cmd.CommandText = sql;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            vr.message = row["LASTNM"].ToString() + " Family";
                            vr.valid = true;
                        }
                    }
                    else
                    {
                        vr.message = "";
                        vr.valid = false; ;
                    }

                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("GetLastServDatePerMemb")]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<MemberEligibilityModel> GetLastServDatePerMemb([FromBody]  List<MemberEligibilityModel> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    string sql = "";

                    foreach (MemberEligibilityModel memb in data)
                    {
                        sql = $"SELECT  cd.FROMDATESVC, cd.TODATESVC FROM {DRCDatabase}.dbo.CLAIM_MASTERS cm\n"
                       + $"INNER JOIN  {DRCDatabase}.dbo.CLAIM_DETAILS cd ON cd.CLAIMNO = cm.CLAIMNO\n"
                       + $"INNER JOIN  {DRCDatabase}.dbo.PROV_SPECINFO sp ON sp.PROVID = cm.PROVID\n"
                       + "WHERE \n"
                       + $"MEMBID = '{memb.MEMBID}' AND \n"
                       + "cd.BENTYPE IN('05','10') AND SPECCODE IN('54','95')\n"
                       + "ORDER BY cd.TODATESVC DESC";
                        cmd.CommandText = sql;
                        dt.Clear();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt.Load(dr);
                        }
                        if (dt.Rows.Count > 0)
                        {
                            memb.ToServDate = dt.Rows[0][1].ToString();
                        }
                        else
                        {
                            string year = DateTime.Now.AddYears(-1).Year.ToString();
                            memb.ToServDate = $"{year}/01/01";
                        }


                        DateTime today = DateTime.Now;
                        DateTime last = Convert.ToDateTime(memb.ToServDate);

                        memb.days = Math.Round((today - last).TotalDays, 1);


                    }



                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return data;
        }

        [HttpPost]
        [Route("MemberValidationFamily")]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<MemberEligibilityModel> MemberValidationFamily([FromBody] AuthModel data)
        {
            List<MemberEligibilityModel> vr = new List<MemberEligibilityModel>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@membnum", data.membno));
                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    // Jaco comment out 2023-09-18 - Databases harcoded in views and sp's 
                    //string sql = "SELECT username, lobcode, HPCODE, memberPrefix, dbo.GenerateSubSsn(memberPrefix,dbo.RemoveDependantDash(@membnum)) as possiblesubssn \r\n" +
                    //         "into #x\r\n" +
                    //         "FROM UserLobCodesHpCodes\r\n" +
                    //         "WHERE (username = @username)\r\n" +
                    //         "SELECT DISTINCT \r\n" +
                    //         " b.MEMBID, b.RLSHIP, b.LASTNM, b.FIRSTNM, b.HPFROMDT, b.HPCODE, b.OPT, b.OPFROMDT, b.OPTHRUDT, b.BIRTH, b.SUBSSN, b.RELATION, b.DEPENDANT, b.HPName, b.LOBCODE, b.TERM_CODE, b.MemberStatus, \r\n" +
                    //         " b.ezHpCode, b.Expr1, b.MemberPrefix, b.CONSULTATIONS\r\n" +
                    //         "FROM MemberEligibility AS a INNER JOIN\r\n" +
                    //         " MemberEligibility AS b ON a.SUBSSN = b.SUBSSN INNER JOIN\r\n" +
                    //         " #x UserLobCodesHpCodes ON b.HPCODE = UserLobCodesHpCodes.HPCODE  and UserLobCodesHpCodes.possiblesubssn = b.subssn\r\n" +
                    //         $"where UserLobCodesHpCodes.username = @username " +
                    //         "drop table #x\r\n";

                    // Jaco 2023-09-18
                    string WhereClause = " where X.username = @user ";
                    string sql = Utillity.MembershipQueryClient(DRCDatabase, PamcPortalDatabase, ReportingDatabase, WhereClause);

                    cmd.CommandText = sql;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        MemberEligibilityModel mm = new MemberEligibilityModel();
                        mm.DEPENDANT = row["DEPENDANT"].ToString();
                        mm.FIRSTNM = row["FIRSTNM"].ToString();
                        mm.HPCODE = row["HPCODE"].ToString();
                        mm.OPT = row["OPT"].ToString();
                        vr.Add(mm);
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(msg, e.StackTrace);
            }

            return vr;
        }



        [HttpPost]
        public static void LogError(string error, string stacktrace)
        {

            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "MEMBERSHIP";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\MEMBERSHIP\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception )
            {


            }


        }
    }
}
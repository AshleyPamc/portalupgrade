﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.ServiceModel;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BenefitController : NEWPAMCPORTAL.Controllers.Base.PAMCController
    {
        public BenefitController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("GetBenRights")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<BenifitRights> GetBenRights([FromBody] MemberEligibilityModel model)
        {

            List<BenifitRights> rights = new List<BenifitRights>();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();
            List<string> objectIds = new List<string>();

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            try
            {
                cmd.Connection = cn;
                cmd.Parameters.Add(new SqlParameter("@hp", model.HPCODE));
                cmd.Parameters.Add(new SqlParameter("@opt", model.HPName.TrimEnd(' ')));
                cmd.Parameters.Add(new SqlParameter("@enabled", 1));
                cmd.Parameters.Add(new SqlParameter("@type", model.type));
                if (model.dentist)
                {
                    cmd.Parameters.Add(new SqlParameter("@dent", '1'));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@dent", '0'));
                }

                //     string sql = "SELECT DISTINCT bl.BENRULEID,bl.DESCR,bl.USEDEFAULTPROC,bl.ALLOWPREAUTH FROM USER_BENIFIT_DISPLAY ud\n"
                //+ "INNER JOIN BENEFIT_RULES bl ON bl.BENRULEID = ud.BenRowId\n"
                //+ "WHERE ud.BenSetupDisplay = @enabled AND ud.UserType = @type AND bl.HPCODE = @hp AND bl.OPT= @opt AND bl.DENTISTRULE = @dent";

                string sql = " SELECT DISTINCT BENRULEID,DESCR,USEDEFAULTPROC,ALLOWPREAUTH \n" +
                             " FROM BENEFIT_RULES \n" +
                             " WHERE HPCODE = @hp AND OPT = @opt AND DENTISTRULE = @dent and [ENABLED] = @enabled ";

                cmd.CommandText = sql;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    BenifitRights bn = new BenifitRights();
                    bn.benName = row["DESCR"].ToString();
                    bn.id = Convert.ToInt32(row["BENRULEID"].ToString());
                    bn.defaultProc = Convert.ToBoolean(row["USEDEFAULTPROC"].ToString());
                    bn.preAuth = Convert.ToBoolean(row["ALLOWPREAUTH"].ToString());
                    rights.Add(bn);
                }



                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;

            }

            return rights;
        }

        [HttpGet]
        [Route("GehpAndOpt")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<MembBenSetup> GethpAndOpt()
        {
            List<MembBenSetup> list = new List<MembBenSetup>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    string sql = "SELECT hp.HPCODE,hp.HPNAME,bo.opt\n"
           + "      \n"
           + $"  FROM [{DRCDatabase}].[dbo].[BEN_OPTIONS] bo\n"
           + $"  INNER JOIN {DRCDatabase}.dbo.HP_CONTRACTS hp ON hp.HPCODE = bo.HPCODE\n"
           + $"  where  GETDATE() between FROMDATE and ISNULL( TODATE,GETDATE())\n"
           + "  ORDER by hp.HPCODE";
                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        MembBenSetup b = new MembBenSetup();
                        b.hpcode = row["HPCODE"].ToString();
                        b.opt = row["opt"].ToString();
                        b.hpname = row["HPNAME"].ToString();

                        list.Add(b);

                    }


                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

            }
            return list;
        }

        [HttpGet]
        [Route("GetExisingMembBen")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<MembBenSetup> GetExisingMembBen()
        {
            List<MembBenSetup> list = new List<MembBenSetup>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    string sql = "SELECT *\n"
           + "      \n"
           + $"  FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES";
                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        MembBenSetup b = new MembBenSetup();
                        b.hpcode = row["HPCODE"].ToString();
                        b.opt = row["OPT"].ToString();
                        b.days = Convert.ToInt32(row["LIMITDAYS"].ToString());
                        b.descr = row["DESCR"].ToString();
                        if (row["FAMILY"].ToString() == "True")
                        {
                            b.family = true;
                        }
                        else
                        {
                            b.family = false;
                        }
                        if (row["ALLOWPREAUTH"].ToString() == "True")
                        {
                            b.preAuth = true;
                        }
                        else
                        {
                            b.preAuth = false;
                        }
                        //ALLOWPREAUTH
                        // b.family = Convert.ToBoolean(row["FAMILY"].ToString());
                        b.id = Convert.ToInt32(row["BENRULEID"].ToString());
                        b.fromAge = Convert.ToInt32(row["FROMAGE"].ToString());
                        b.toAge = Convert.ToInt32(row["TOAGE"].ToString());
                        b.procCodes = row["PROCCODE"].ToString();
                        if (row["USEDEFAULTPROC"].ToString() == "True")
                        {
                            b.useDefaultProc = true;
                        }
                        else
                        {
                            b.useDefaultProc = false;
                        }
                        // b.useDefaultProc = Convert.ToBoolean(row["USEDEFAULTPROC"].ToString());
                        b.procExc = row["PROCEXCL"].ToString();
                        b.limit = Convert.ToInt32(row["LIMITQTY"].ToString());
                        if (row["DENTISTRULE"].ToString().ToUpper() == "TRUE" || row["DENTISTRULE"].ToString().ToUpper() == "1")
                        {
                            b.dentist = "DENTIST";
                        }
                        else
                        {
                            b.dentist = "GP";
                        }


                        list.Add(b);

                    }


                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("InsertMembBen")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel InsertMembBen([FromBody] MembBenSetup data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;


                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.BENEFIT_RULES \n" +
                        $"(HPCODE,OPT,DESCR,PROCCODE,PROCEXCL,FROMAGE,TOAGE,LIMITQTY,LIMITDAYS,FAMILY,CREATEBY,CREATEDATE,LASTCHANGEBY,LASTCHANGEDATE,USEDEFAULTPROC," +
                        $"DENTISTRULE,ALLOWPREAUTH)\n" +
                        $"OUTPUT INSERTED.BENRULEID\n" +
                        $"VALUES\n" +
                        $"('{data.hpcode}','{data.opt}','{data.descr}','{data.procCodes}','{data.procExc}','{data.fromAge}','{data.toAge}','{data.limit}','{data.days}','{data.family}'," +
                        $"'1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
                        $"'{data.useDefaultProc}','{data.dentist}','{data.preAuth}')";

                    string id = cmd.ExecuteScalar().ToString();

                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.USER_BENIFIT_DISPLAY (BenrowId,Usertype,BenSetupDisplay,CreateDate,CreateBy,CHangeDAte,ChangeBy)\n" +
                        $"VALUES('{id}','1','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999')";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;

                }

            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }


            return vr;
        }

        [HttpPost]
        [Route("CopyRule")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel CopyRule([FromBody] MembBenSetup data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    List<MembBenSetup> list = new List<MembBenSetup>();

                    if (data.all)
                    {
                        list = new List<MembBenSetup>();
                        cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE\n" +
                            $"HPCODE = '{data.hpcode}' AND OPT = '{data.opt}' AND DENTISTRULE = '{data.dentist}'";
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow row in dt.Rows)
                        {
                            MembBenSetup d = new MembBenSetup();

                            d.hpcode = data.copyhp;
                            d.opt = data.copyopt;
                            d.descr = row["DESCR"].ToString();
                            d.fromAge = Convert.ToInt32(row["FROMAGE"].ToString());
                            d.toAge = Convert.ToInt32(row["TOAGE"].ToString());
                            d.limit = Convert.ToInt32(row["LIMITQTY"].ToString());
                            d.days = Convert.ToInt32(row["LIMITDAYS"].ToString());
                            d.family = Convert.ToBoolean(row["FAMILY"].ToString());
                            d.useDefaultProc = Convert.ToBoolean(row["USEDEFAULTPROC"].ToString());
                            d.procCodes = row["PROCCODE"].ToString();
                            d.procExc = row["PROCEXCL"].ToString();
                            d.dentist = row["DENTISTRULE"].ToString();
                            d.preAuth = Convert.ToBoolean(row["ALLOWPREAUTH"].ToString());
                            list.Add(d);


                        }

                        foreach (MembBenSetup item in list)
                        {
                            cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.BENEFIT_RULES \n" +
      $"(HPCODE,OPT,DESCR,PROCCODE,PROCEXCL,FROMAGE,TOAGE,LIMITQTY,LIMITDAYS,FAMILY,CREATEBY,CREATEDATE,LASTCHANGEBY,LASTCHANGEDATE,USEDEFAULTPROC,DENTISTRULE,ALLOWPREAUTH)\n" +
      $"OUTPUT INSERTED.BENRULEID\n" +
      $"VALUES\n" +
      $"('{item.hpcode}','{item.opt}','{item.descr}','{item.procCodes}','{item.procExc}','{item.fromAge}','{item.toAge}','{item.limit}','{item.days}','{item.family}'," +
      $"'1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'" +
      $",'{item.useDefaultProc}','{item.dentist}','{item.preAuth}')";

                            string id = cmd.ExecuteScalar().ToString();

                            cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.USER_BENIFIT_DISPLAY (BenrowId,Usertype,BenSetupDisplay,CreateDate,CreateBy,CHangeDAte,ChangeBy)\n" +
                                $"VALUES('{id}','1','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999')";

                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        list = new List<MembBenSetup>();
                        cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE\n" +
                            $"HPCODE = '{data.hpcode}' AND OPT = '{data.opt}' AND DESCR ='{data.copydesc}' AND DENTISTRULE = '{data.dentist}'";
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow row in dt.Rows)
                        {
                            MembBenSetup d = new MembBenSetup();

                            d.hpcode = data.copyhp;
                            d.opt = data.copyopt; ;
                            d.descr = row["DESCR"].ToString();
                            d.fromAge = Convert.ToInt32(row["FROMAGE"].ToString());
                            d.toAge = Convert.ToInt32(row["TOAGE"].ToString());
                            d.limit = Convert.ToInt32(row["LIMITQTY"].ToString());
                            d.days = Convert.ToInt32(row["LIMITDAYS"].ToString());
                            d.family = Convert.ToBoolean(row["FAMILY"].ToString());
                            d.useDefaultProc = Convert.ToBoolean(row["USEDEFAULTPROC"].ToString());
                            d.procCodes = row["PROCCODE"].ToString();
                            d.procExc = row["PROCEXCL"].ToString();
                            d.dentist = row["DENTISTRULE"].ToString();
                            d.preAuth = Convert.ToBoolean(row["ALLOWPREAUTH"].ToString());
                            list.Add(d);


                        }

                        foreach (MembBenSetup item in list)
                        {
                            cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.BENEFIT_RULES \n" +
      $"(HPCODE,OPT,DESCR,PROCCODE,PROCEXCL,FROMAGE,TOAGE,LIMITQTY,LIMITDAYS,FAMILY,CREATEBY,CREATEDATE,LASTCHANGEBY,LASTCHANGEDATE,USEDEFAULTPROC,DENTISTRULE,ALLOWPREAUTH)\n" +
      $"OUTPUT INSERTED.BENRULEID\n" +
      $"VALUES\n" +
      $"('{item.hpcode}','{item.opt}','{item.descr}','{item.procCodes}','{item.procExc}','{item.fromAge}','{item.toAge}','{item.limit}','{item.days}','{item.family}'," +
      $"'1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
      $"'{item.useDefaultProc}','{item.dentist}','{item.preAuth}')";

                            string id = cmd.ExecuteScalar().ToString();

                            cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.USER_BENIFIT_DISPLAY (BenrowId,Usertype,BenSetupDisplay,CreateDate,CreateBy,CHangeDAte,ChangeBy)\n" +
                                $"VALUES('{id}','1','1','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','999')";

                            cmd.ExecuteNonQuery();
                        }
                    }



                    vr.valid = true;

                }

            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }


            return vr;
        }


        [HttpPost]
        [Route("UpdateBenType")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel UpdateBenType([FromBody] MembBenSetup data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;


                    string sql = $"UPDATE {PamcPortalDatabase}.dbo.BENEFIT_RULES SET HPCODE='{data.hpcode}', OPT = '{data.opt}', DESCR ='{data.descr}', PROCCODE='{data.procCodes}'," +
                        $"PROCEXCL ='{data.procExc}', FROMAGE='{data.fromAge}', TOAGE = '{data.toAge}', LIMITQTY = '{data.limit}', " +
                        $"LIMITDAYS= '{data.days}',FAMILY = '{data.family}',ALLOWPREAUTH='{data.preAuth}'," +
                        $"USEDEFAULTPROC = '{data.useDefaultProc}',DENTISTRULE='{data.dentist}' ," +
                        $"LASTCHANGEBY='999',LASTCHANGEDATE='{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}' \n" +
                        $"WHERE BENRULEID = '{data.id}'";
                    cmd.CommandText = sql;

                    cmd.ExecuteNonQuery();



                    vr.valid = true;

                }

            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }


            return vr;
        }

        [HttpPost]
        [Route("DeleteMembBen")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel DeleteMembBen([FromBody] MembBenSetup data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"DELETE FROM  {PamcPortalDatabase}.dbo.USER_BENIFIT_DISPLAY WHERE BenrowId = '{data.id}' \n";

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = $"DELETE FROM  {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE BENRULEID = '{data.id}' \n";

                    cmd.ExecuteNonQuery();



                    vr.valid = true;

                }

            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }


            return vr;
        }


        [HttpPost]
        [Route("BenifitResult")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public BenifitRights BenifitResult([FromBody] BenifitRights data)
        {
            BenifitRights b = new BenifitRights();
            b.membid = data.membid.Replace(" ", "");
            b.provid = data.provid;
            b.hp = data.hp;
            b.id = data.id;
            b.benName = data.benName;
            b.defaultProc = data.defaultProc;
            b.preAuth = data.preAuth;

            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();
                    string logresult = "";

                    if (data.defaultProc)
                    {
                        // Jaco comment out 2023-11-22 - Databases hardcoded in PortalBenefitRules stored proc
                        // cmd.CommandText = $"EXEC {DRCDatabase}.dbo.PortalBenefitRules '{data.membid.Replace(" ", "")}','{data.id}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'";

                        // Jaco comment out 2023-11-22
                        cmd.CommandText =
                        $" DECLARE @MEMBID VARCHAR(40) = '{data.membid.Replace(" ", "")}' \n" +
                        $" DECLARE @BENRULEID INT = '{data.id}'  \n" +
                        $" DECLARE @REQUESTDATE DATETIME = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}' \n" +

                        $" --Eugene de Jager 24 / 03 / 2020 \n" +
                        $" DECLARE @FROMDATESVC DATETIME \n" +
                        $" DECLARE @PROVID[varchar](20) \n" +
                        $" DECLARE @HPCODE[varchar](4) \n" +
                        $" DECLARE @OPT[varchar](20) \n" +
                        $" DECLARE @INPROCCODE[varchar](500) \n" +
                        $" DECLARE @PROCEXCL[varchar](100) \n" +
                        $" DECLARE @FROMAGE[int] \n" +
                        $" DECLARE @TOAGE[int] \n" +
                        $" DECLARE @LIMITQTY[int] \n" +
                        $" DECLARE @LIMITDAYS[int] \n" +
                        $" DECLARE @SUBSSN[varchar](40) \n" +
                        $" DECLARE @BIRTH DATETIME \n" +
                        $" DECLARE @AGE INT \n" +
                        $" DECLARE @QTY INT \n" +
                        $" DECLARE @FAMILY INT \n" +
                        $" DECLARE @bWrongAge int \n" +
                        $" DECLARE @ROWCNT INT \n" +
                        $" DECLARE @LEN_PROCEXCL INT \n" +
                        $" DECLARE @YES_NO VARCHAR(1) \n" +
                        $" DECLARE @AGE_MESSAGE VARCHAR(30) \n" +
                        $" SET @bWrongAge = 0 \n" +
                        $" SET @ROWCNT = 0 \n" +
                        $" CREATE TABLE #EXLPROCCODE_STRING (PROCCODE VARCHAR(100)) \n" +
                        $" CREATE TABLE #EXLPROCCODE (PROCCODE VARCHAR(20)) \n" +
                        $" CREATE TABLE #INPROCCODE_STRING (PROCCODE VARCHAR(500)) \n" +
                        $" CREATE TABLE #INPROCCODE (PROCCODE VARCHAR(20)) \n" +
                        $" --SELECT * FROM {PamcPortalDatabase}..BENEFIT_RULES \n" +
                        $" --SELECT DATEDIFF(YEAR, BIRTH, GETDATE()), FLOOR((CAST(GETDATE() AS INTEGER) - CAST(BIRTH AS INTEGER)) / 365.25) AS Age,*FROM {DRCDatabase}..MEMB_MASTERS WHERE MEMBID = 'ME7961876-02' \n" +
                        $" --SELECT SUBSSN, FLOOR((CAST(GETDATE() AS INTEGER) - CAST(BIRTH AS INTEGER)) / 365.25) FROM {DRCDatabase}..MEMB_MASTERS WHERE MEMBID = 'ME7961876-02' \n" +
                        $" SELECT @SUBSSN = SUBSSN, @AGE = FLOOR((CAST(@REQUESTDATE AS INTEGER) - CAST(BIRTH AS INTEGER)) / 365.25) FROM {DRCDatabase}..MEMB_MASTERS WHERE MEMBID = @MEMBID \n" +
                        $" SELECT @HPCODE = HPCODE, @OPT = OPT FROM {DRCDatabase}..MEMB_HPHISTS WHERE MEMBID = @MEMBID AND @REQUESTDATE BETWEEN OPFROMDT AND ISNULL(OPTHRUDT,'2099-12-31') \n" +
                        $" -- > 12 between 11 and 999 \n" +
                        $" SELECT @INPROCCODE = LTRIM(RTRIM(PROCCODE)), @FROMAGE = FROMAGE, @TOAGE = TOAGE, @LIMITQTY = LIMITQTY, @LIMITDAYS = LIMITDAYS, @FAMILY = FAMILY, @PROCEXCL = PROCEXCL \n" +
                        $" FROM {PamcPortalDatabase}..BENEFIT_RULES WHERE BENRULEID = @BENRULEID \n" +
                        $"  /*GET LIST OF PROCCODES'S*/ \n" +
                        $" INSERT INTO #EXLPROCCODE_STRING \n" +
                        $" SELECT @PROCEXCL \n" +
                        $" DECLARE @SQLQUERY NVARCHAR(MAX) \n" +
                        $" SET @SQLQUERY = (SELECT PROCCODE \n" +
                        $"                 FROM   #EXLPROCCODE_STRING)  \n" +
                        $" INSERT INTO #EXLPROCCODE  \n" +
                        $" SELECT * \n" +
                        $" FROM   SPLIT_DELIMITED_STRING(@SQLQUERY, ',') \n" +
                        $"  /*GET LIST OF PROCCODES'S*/ \n" +

                        $" /*GET LIST OF PROCCODES'S*/ \n" +
                        $" INSERT INTO #INPROCCODE_STRING \n" +
                        $" SELECT @INPROCCODE \n" +
                        $" DECLARE @INSQLQUERY NVARCHAR(MAX) \n" +
                        $" SET @INSQLQUERY = (SELECT PROCCODE \n" +
                        $"                  FROM   #INPROCCODE_STRING)  \n" +
                        $" INSERT INTO #INPROCCODE  \n" +
                        $" SELECT * \n" +
                        $" FROM   SPLIT_DELIMITED_STRING(@INSQLQUERY, ',') \n" +
                        $" /*GET LIST OF PROCCODES'S*/ \n" +
                        $" -- 'ME7961876-02' \n" +
                        $" -- do age check \n" +
                        $" IF LEN(@FROMAGE) = 0 OR LEN(@FROMAGE) IS NULL \n" +
                        $" BEGIN \n" +
                        $"         --no age rule present \n" +
                        $"         SET @AGE_MESSAGE = '' \n" +
                        $"         SET @bWrongAge = 0 \n" +
                        $" END \n" +
                        $" ELSE \n" +
                        $" BEGIN \n" +
                        $"     IF @AGE BETWEEN @FROMAGE AND @TOAGE \n" +
                        $"         BEGIN \n" +
                        $"             SET @AGE_MESSAGE = '' \n" +
                        $"             SET @bWrongAge = 0 \n" +
                        $"         END \n" +
                        $"     ELSE \n" +
                        $"         BEGIN \n" +
                        $"             SET @bWrongAge = 1 \n" +
                        $"             SET @AGE_MESSAGE = 'AGE OUT OF RANGE' \n" +
                        $"             SET @YES_NO = 'N' \n" +
                        $"         END \n" +
                        $" END \n" +

                        $" SET @LIMITDAYS = @LIMITDAYS * -1 \n" +

                        $" IF @bWrongAge = 0--acge is correct \n" +
                        $" BEGIN \n" +
                        $"     IF @LIMITQTY = 1 AND(LEN(@PROCEXCL) = 0 OR LEN(@PROCEXCL) IS NULL)--ONCE PER x DAYS, bv 8101 \n" +
                        $"     BEGIN \n" +
                        $"         --once per 180 days(no need for count only once),  \n" +
                        $"         SELECT @PROVID = CM.PROVID, @FROMDATESVC = CD.FROMDATESVC \n" +
                        $"         FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"         WHERE CM.MEMBID = @MEMBID \n" +
                        $"         AND LTRIM(RTRIM(CD.PROCCODE)) IN(SELECT PROCCODE FROM #INPROCCODE) --@PROCCODE \n" +
                        $"         AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"         --'ME5525373-01' \n" +
                        $"         IF @@ROWCOUNT > 0 \n" +
                        $"         BEGIN \n" +
                        $"             SET @YES_NO = 'N' \n" +
                        $"         END \n" +
                        $"         ELSE \n" +
                        $"         BEGIN \n" +
                        $"             SET @YES_NO = 'Y' \n" +
                        $"         END \n" +
                        $"     END \n" +

                        $"     IF @LIMITQTY = 1 AND LEN(@PROCEXCL) > 0--Not within 28 days of previous 8104 / 8101 \n" +
                        $"     BEGIN \n" +
                        $"         --SELECT LTRIM(RTRIM(PROCCODE)) FROM #PROCCODE \n" +
                        $"         SELECT @PROVID = CM.PROVID, @FROMDATESVC = CD.FROMDATESVC \n" +
                        $"         FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"         WHERE  CM.MEMBID = @MEMBID \n" +
                        $"         --AND LTRIM(RTRIM(CD.PROCCODE)) = @PROCCODE \n" +
                        $"         AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"         AND LTRIM(RTRIM(CD.PROCCODE)) IN \n" +
                        $"                --Get Include / Exclude svccode for that set \n" +
                        $"         (SELECT LTRIM(RTRIM(PROCCODE)) FROM #EXLPROCCODE ) \n" +
                        $"         --'ME5525373-01' \n" +
                        $"           IF @@ROWCOUNT > 0 \n" +
                        $"           BEGIN \n" +
                        $"               SET @YES_NO = 'N' \n" +
                        $"           END \n" +
                        $"           ELSE \n" +
                        $"           BEGIN \n" +
                        $"               SET @YES_NO = 'Y' \n" +
                        $"           END \n" +
                        $"       END \n" +
                        $"       IF @FAMILY = 1 and @LIMITQTY > 1--8307 2 Per family per 365 days \n" +
                        $"       BEGIN \n" +
                        $"           --'MS71200604682' \n" +
                        $"           SET @QTY = 0 \n" +
                        $"           SELECT @QTY = ISNULL(SUM(CD.QTY), 0) \n" +
                        $"           FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"           WHERE  CM.SUBSSN = @SUBSSN \n" +
                        $"           AND LTRIM(RTRIM(CD.PROCCODE)) IN(SELECT PROCCODE FROM #INPROCCODE) \n" +
                        $"         AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"           --AND CD.FROMDATESVC >= DATEADD(DAY, -365, GETDATE()) \n" +
                        $"           SET @QTY = @QTY + 1 \n" +
                        $"           IF @QTY > @LIMITQTY \n" +
                        $"           BEGIN \n" +
                        $"               SELECT @PROVID = CM.PROVID, @FROMDATESVC = CD.FROMDATESVC \n" +
                        $"               FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"               WHERE  CM.SUBSSN = @SUBSSN \n" +
                        $"               AND LTRIM(RTRIM(CD.PROCCODE))IN(SELECT PROCCODE FROM #INPROCCODE) \n" +
                        $"             AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"               SET @YES_NO = 'N' \n" +
                        $"           END \n" +
                        $"           ELSE \n" +
                        $"           BEGIN \n" +
                        $"               SET @YES_NO = 'Y' \n" +
                        $"           END \n" +
                        $"       END \n" +
                        $"       IF @FAMILY = 0 and @LIMITQTY > 1--8115 2 per 730 days \n" +
                        $"       BEGIN \n" +
                        $"           SET @QTY = 0 \n" +
                        $"           SELECT @QTY = ISNULL(SUM(CD.QTY), 0) \n" +
                        $"           FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"           WHERE  CM.MEMBID = @MEMBID \n" +
                        $"           AND LTRIM(RTRIM(CD.PROCCODE)) IN(SELECT PROCCODE FROM #INPROCCODE) \n" +
                        $"         AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"           --AND CD.FROMDATESVC >= DATEADD(DAY, -365, GETDATE()) \n" +
                        $"           SET @QTY = @QTY + 1 \n" +
                        $"           IF @QTY > @LIMITQTY \n" +
                        $"           BEGIN \n" +
                        $"               SELECT @PROVID = CM.PROVID, @FROMDATESVC = CD.FROMDATESVC \n" +
                        $"               FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CLAIM_DETAILS CD ON CD.CLAIMNO = CM.CLAIMNO \n" +
                        $"               WHERE  CM.MEMBID = @MEMBID \n" +
                        $"               AND LTRIM(RTRIM(CD.PROCCODE)) IN(SELECT PROCCODE FROM #INPROCCODE) \n" +
                        $"             AND CD.FROMDATESVC >= DATEADD(DAY, @LIMITDAYS, @REQUESTDATE) \n" +
                        $"               SET @YES_NO = 'N' \n" +
                        $"           END \n" +
                        $"           ELSE \n" +
                        $"           BEGIN \n" +
                        $"               SET @YES_NO = 'Y' \n" +
                        $"           END \n" +
                        $"       END \n" +
                        $"   END \n" +
                        $"   DROP TABLE #EXLPROCCODE \n" +
                        $" DROP TABLE #INPROCCODE \n" +
                        $" DROP TABLE #EXLPROCCODE_STRING \n" +
                        $" DROP TABLE #INPROCCODE_STRING \n" +
                        $" SELECT @YES_NO AS 'Y/N',@AGE_MESSAGE AS 'Age Message' ,@AGE AS 'Age',@PROVID as 'Provider',@FROMDATESVC AS 'Service Date' \n";




                          dt.Load(cmd.ExecuteReader());

                        foreach (DataRow row in dt.Rows)
                        {
                            string indicator = "";
                            string ageMsg = "";
                            string age = "";
                            string prov = "";
                            string svcDate = "";

                            indicator = row["Y/N"].ToString();
                            ageMsg = row["Age Message"].ToString();
                            age = row["Age"].ToString();
                            prov = row["Provider"].ToString();
                            svcDate = row["Service Date"].ToString();

                            if ((ageMsg == "") || (ageMsg == null))
                            {
                                ageMsg = "N/A";
                            }
                            if ((age == "") || (age == null))
                            {
                                age = "N/A";
                            }
                            if ((prov == "") || (prov == null))
                            {
                                prov = "N/A";
                            }
                            else
                            {
                                cmd.CommandText = $"SELECT LASTNAME FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = '{prov}'";
                                DataTable tb = new DataTable();
                                tb.Load(cmd.ExecuteReader());
                                foreach (DataRow rr in tb.Rows)
                                {
                                    prov = rr["LASTNAME"].ToString();
                                }
                            }
                            if ((svcDate == "") || (svcDate == null))
                            {
                                svcDate = "N/A";
                            }

                            string r = "";
                            r = $"{indicator};{ageMsg};{age};{prov};{svcDate}";
                            b.results = new List<string>();

                            b.results.Add(r);


                        }
                        foreach (string d in b.results)
                        {
                            logresult = logresult + "\n" + d;
                        }
                    }
                    else
                    {
                        if (data.benName.ToUpper() == "RESTORATIONS")
                        {

                            cmd.CommandText = $"SELECT TOAGE FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE BENRULEID = '{data.id}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            int ageTo = Convert.ToInt32(dt.Rows[0][0].ToString());

                            cmd.CommandText = $"SELECT LEFT(CONVERT(VARCHAR,BIRTH,111),4) FROM {DRCDatabase}.dbo.MEMB_MASTERS WHERE MEMBID = '{data.membid}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            int yearDOB = Convert.ToInt32(dt.Rows[0][0].ToString());

                            int ageMeb = DateTime.Now.Year - yearDOB;
                            if (ageMeb <= ageTo)
                            {
                                cmd.CommandText = $"SELECT LIMITDAYS FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE BENRULEID = '{data.id}'";
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                decimal days = Convert.ToDecimal(dt.Rows[0][0].ToString()) / 30;
                                days = Math.Round(days, 0, MidpointRounding.AwayFromZero);





                                cmd.CommandText = $"SELECT pm.LASTNAME,CM.PROVID,  CD.FROMDATESVC, DATEDIFF(MONTH,CD.FROMDATESVC,GETDATE()) AS MONTHS, CC.CLINICALCODE  FROM {DRCDatabase}.dbo.CLAIM_MASTERS CM\n"
                + $"INNER JOIN {DRCDatabase}.dbo.CLAIM_DETAILS CD ON (CM.CLAIMNO = CD.CLAIMNO)\n" +
                $"INNER JOIN {DRCDatabase}.dbo.PROV_MASTERS pm ON pm.PROVID = cm.PROVID\n"
                + $"RIGHT JOIN {DRCDatabase}.dbo.CLAIM_CLINICALCODES CC ON (CD.CLAIMNO = CC.CLAIMNO AND CD.TBLROWID = CC.CLAIMTBLROW)\n"
                + $"LEFT JOIN {DRCDatabase}.dbo.SERVICE_CODEGROUPS SCG ON(LTRIM(CD.PROCCODE) = SCG.SVCCODE)\n"
                + "\n"
                + $"WHERE MEMBID = '{data.membid}' AND LTRIM(CD.PROCCODE) IN\n"
                + "\n"
                + $"(SELECT SVCCODE FROM {DRCDatabase}.dbo.SERVICE_CODEGROUPS WHERE PROCEDURE1 = 'Restorations') AND LTRIM(CD.PROCCODE) <> '8163'";

                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                b.results = new List<string>();

                                foreach (DataRow row in dt.Rows)
                                {
                                    if (Convert.ToInt32(row["MONTHS"]) < days)
                                    {
                                        string r = row["LASTNAME"].ToString() + ";" + row["CLINICALCODE"].ToString() + ";" + row["FROMDATESVC"].ToString() + ";" + row["MONTHS"].ToString();
                                        b.results.Add(r);
                                    }

                                }
                                foreach (string d in b.results)
                                {
                                    logresult = logresult + "\n" + d;
                                }
                            }
                            else
                            {
                                b.results = new List<string>();
                                string r = $"INCORRECT AGE;N/A;{DateTime.Now.ToString("yyyy/MM/dd")};N/A";
                                b.results.Add(r);
                            }

                        }

                        if (data.benName.ToUpper() == "FISSURE SEALANT")
                        {
                            cmd.CommandText = $"SELECT TOAGE FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE BENRULEID = '{data.id}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            int ageTo = Convert.ToInt32(dt.Rows[0][0].ToString());

                            cmd.CommandText = $"SELECT LEFT(CONVERT(VARCHAR,BIRTH,111),4) FROM {DRCDatabase}.dbo.MEMB_MASTERS WHERE MEMBID = '{data.membid}'";
                            dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            int yearDOB = Convert.ToInt32(dt.Rows[0][0].ToString());

                            int ageMeb = DateTime.Now.Year - yearDOB;

                            if (ageMeb <= ageTo)
                            {
                                cmd.CommandText = $"SELECT LIMITDAYS FROM {PamcPortalDatabase}.dbo.BENEFIT_RULES WHERE BENRULEID = '{data.id}'";
                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                decimal days = Convert.ToDecimal(dt.Rows[0][0].ToString()) / 30;
                                days = Math.Round(days, 0, MidpointRounding.AwayFromZero);

                                cmd.CommandText = $"SELECT pm.LASTNAME,CM.PROVID,  CD.FROMDATESVC, CC.CLINICALCODE,DATEDIFF(MONTH,CD.FROMDATESVC,GETDATE()) AS MONTHS  FROM {DRCDatabase}.dbo.CLAIM_MASTERS CM\n"
     + $"INNER JOIN {DRCDatabase}.dbo.CLAIM_DETAILS CD ON (CM.CLAIMNO = CD.CLAIMNO)\n" +
     $"INNER JOIN {DRCDatabase}.dbo.PROV_MASTERS pm ON pm.PROVID = cm.PROVID\n"
     + $"RIGHT JOIN {DRCDatabase}.dbo.CLAIM_CLINICALCODES CC ON (CD.CLAIMNO = CC.CLAIMNO AND CD.TBLROWID = CC.CLAIMTBLROW)\n"
     + $"LEFT JOIN {DRCDatabase}.dbo.SERVICE_CODEGROUPS SCG ON(LTRIM(CD.PROCCODE) = SCG.SVCCODE)\n"
     + "\n"
     + $"WHERE MEMBID = '{data.membid}' AND LTRIM(CD.PROCCODE) IN\n"
     + "\n"
     + $"(SELECT SVCCODE FROM {DRCDatabase}.dbo.SERVICE_CODEGROUPS WHERE LTRIM(SCG.SVCCODE) = '8163')";


                                dt = new DataTable();
                                dt.Load(cmd.ExecuteReader());

                                b.results = new List<string>();

                                foreach (DataRow row in dt.Rows)
                                {
                                    if (Convert.ToInt32(row["MONTHS"].ToString()) < days)
                                    {
                                        string r = row["LASTNAME"].ToString() + ";" + row["CLINICALCODE"].ToString() + ";" + row["FROMDATESVC"].ToString();
                                        b.results.Add(r);
                                    }


                                }
                                foreach (string d in b.results)
                                {
                                    logresult = logresult + "\n" + d;
                                }
                            }
                            else
                            {
                                b.results = new List<string>();
                                string r = $"INCORRECT AGE;N/A;{DateTime.Now.ToString("yyyy/MM/dd")}";
                                b.results.Add(r);
                            }

                        }
                    }





                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.BENIFIT_LOOKUP_LOG (Methode,ProvId,MembId,Hpcode,BenifitId,ReturnedDetail,LogDate,LogBy)" +
                        $"OUTPUT INSERTED.ID \n" +
                        $"VALUES ('{data.benName}','{data.provid}','{data.membid}','{data.hp}','{data.id}','{logresult}'," +
                        $"'{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}',999) \n";

                    string id = cmd.ExecuteScalar().ToString();
                    if (data.dentice)
                    {
                        b.reff = "DRC#" + id;
                    }
                    else
                    {
                        b.reff = "PAMC#" + id;
                    }
                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.BENIFIT_LOOKUP_LOG SET ref = '{b.reff}' WHERE ID = '{id}'";
                    cmd.ExecuteNonQuery();

                    SettingsModel st = new SettingsModel();
                    st = NEWPAMCPORTAL.Controllers.Utillity.GetSpecificSetting("BenRefEmail", _portalConnectionString);



                    if (st.enabled)
                    {
                        string email = "";

                        cmd.CommandText = $"SELECT EMAIL FROM {DRCDatabase}.dbo.PROV_MASTERS WHERE PROVID = '{data.provid}'";
                        dt = new DataTable();

                        dt.Load(cmd.ExecuteReader());

                        email = dt.Rows[0]["EMAIL"].ToString();

                        string subject = "";
                        string body = "";
                        string result = "";

                        if (data.benName.ToUpper() == "FISSURE SEALANT" || data.benName.ToUpper() == "RESTORATIONS")
                        {
                            string[] r = logresult.Split("\n");
                            foreach (string e in r)
                            {
                                string[] c = e.Split(";");
                                result = $"LAST PRACTICE: {c[0]}\r\n" +
                                         $"TOOTH NO: {c[1]}\r\n" +
                                         $"LAST SERVICE DATE: {c[2]}\n\r";
                            }
                        }
                        else
                        {

                            string[] r = logresult.Split(";");
                            result = $"ELIGIBLE: {r[0]}\r" +
                                $"MESSAGE: {r[1]}\r" +
                                $"AGE: {r[2]}\r" +
                                $"LAST PRACTICE: {r[3]}\r" +
                                $"LAST SERVICE DATE: {r[4]}";


                        }

                        subject = "BENIFIT LOOKUP REFERENCE";

                        body = "Good day, \n\r\n\r" +
                            $"Please find below you reference for your benifit lookup done on {DateTime.Now.ToString()}\n\r\n\r" +
                            $"MEMBER NO: {data.membid.Replace(" ", "")}\n\r" +
                            $"BENIFIT LOOKUP TYPE: {data.benName}\n\r" +
                            $"RESULTS: \n\r" +
                            $"---------------------------------------- \r\n" +
                            $"{result} \n\r" +
                            $"---------------------------------------- \r\n" +
                            $"REFERENCE: {b.reff}\n\r\n\r" +
                            $"Regards\n\r" +
                            $"DRC/PAMC Team";

                        try
                        {
                            string url = _EmailWebSvcConnection;
                            EndpointAddress address = new EndpointAddress(url);
                            BasicHttpBinding binding = new BasicHttpBinding();
                            binding.MaxReceivedMessageSize = 2147483647;
                            var time = new TimeSpan(1, 30, 0);
                            binding.CloseTimeout = time;
                            binding.OpenTimeout = time;
                            binding.ReceiveTimeout = time;
                            binding.SendTimeout = time;

                            EmailService.EmailServiceSoapClient em = new EmailService.EmailServiceSoapClient(binding, address);

                            //var a = em.SendEmailAsync(body, subject, email);

                            //a.Wait();

                            if (_isTest)
                            {                                
                                var a = em.SendEmailAsync(body, "PORTAL TEST: " + subject, _testEmails);
                                a.Wait();
                            }
                            else
                            {
                                var a = em.SendEmailAsync(body, subject, email);
                                a.Wait();
                            }

                        }
                        catch (Exception)
                        {


                        }

                    }

                    //cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return b;
        }

        [HttpPost]
        [Route("GetExistingBef")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<specmasters> GetExistingBef([FromBody] LoginDetailsModel data)
        {
            List<specmasters> list = new List<specmasters>();
            try
            {
                DataTable tbl = new DataTable();
                DataTable dt = new DataTable();
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@user", data.username));

                    // Jaco comment out 2023-09-08 View is hardcoded to DRC
                    //string sql = "SELECT i.ID, i.SetDesc,i.hpcode FROM dbo.UserLobCodesHpCodes u\n"   
                    //             + "INNER JOIN Insurance_Templates_Master i on i.hpcode = u.HPCODE\n"
                    //            + "WHERE u.username = @user";

                    //cmd.CommandText = sql;

                    // Jaco 2023-09-08
                    cmd.CommandText =   " SELECT i.ID, i.SetDesc, i.hpcode " +
                                        " FROM( " +
                                        "        SELECT        u.username, u.lobcode, hp.HPCODE " +
                                       $"        FROM          {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                       $"                         {DRCDatabase}..HP_CONTRACTS AS hp " +
                                       $"        where hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode, ',')) " +
                                        "      ) u " +
                                       $" INNER JOIN {PamcPortalDatabase}..Insurance_Templates_Master i on i.hpcode = u.HPCODE " +
                                        " WHERE u.username = @user ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        specmasters sp = new specmasters();
                        sp.id = Convert.ToInt32(row["ID"].ToString());
                        sp.desc = row["SetDesc"].ToString();
                        sp.hpcode = row["HPCODE"].ToString();
                        list.Add(sp);
                    }

                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetBenifitListForHp")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<specmasters> GetBenifitListForHp([FromBody] HealthplanModelcs data)
        {
            List<specmasters> list = new List<specmasters>();
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@hp", data.code));

                    string sql = "SELECT ID, SetDesc,hpcode FROM\n"
           + $"    {PamcPortalDatabase}.dbo.Insurance_Templates_Master \n"
           + "WHERE hpcode = @hp";

                    cmd.CommandText = sql;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        specmasters sp = new specmasters();

                        sp.id = Convert.ToInt32(row["ID"].ToString());
                        sp.desc = row["SetDesc"].ToString();
                        sp.hpcode = row["HPCODE"].ToString();

                        list.Add(sp);
                    }


                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("GetDetailBen")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<SpecCodes> GetDetailBen([FromBody] specmasters data)
        {
            List<SpecCodes> list = new List<SpecCodes>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@id", data.id));

                    cmd.CommandText = $"SELECT DISTINCT  SpecCode,ProcCode,BenType, MasterId, bc.DESCR  FROM \n" +
                        $"{PamcPortalDatabase}.dbo.Insurance_Templates_Detail itd\n" +
                        $"LEFT OUTER JOIN {DRCDatabase}.dbo.PROC_CODES pc on REPLACE(pc.SVCCODE,' ','') = itd.ProcCode \n" +
                        $"LEFT OUTER JOIN {DRCDatabase}.dbo.BENTRACK_CODES bc ON bc.CODE = itd.BenType \n" +
                        $"\n WHERE MasterId = @id";

                    DataTable dt = new DataTable();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        SpecCodes s = new SpecCodes();
                        s.code = row["SpecCode"].ToString();
                        s.id = Convert.ToInt32(row["MasterId"].ToString());
                        s.procCode = row["ProcCode"].ToString();
                        s.benCode = row["DESCR"].ToString() + "-" + row["BenType"].ToString();
                        list.Add(s);
                    }
                }

            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("InsuranceMasterSubmit")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterSubmit([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data.user));
                    cmd.Connection = cn;
                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Master  (SetDesc,hpcode,CreateDate,CreateBy,ChangedDate,ChangeBy) \n" +
                        $"OUTPUT INSERTED.ID \n" +
                        $"VALUES(@desc,@hpcode,@date,@user,@date,@user)";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceMasterDelete")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterDelete([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Connection = cn;
                    cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Master WHERE ID = @id";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceDetailDelete")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceDetailDelete([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Connection = cn;
                    cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE MasterId = @id";
                    data.id = (int)cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceMasterUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public specmasters InsuranceMasterUpdate([FromBody] specmasters data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));
                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")));
                    cmd.Parameters.Add(new SqlParameter("@user", data.user));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));

                    cmd.Connection = cn;
                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Insurance_Templates_Master SET SetDesc = @desc ,hpcode = @hpcode ," +
                        $"ChangedDate = @date ,ChangeBy = @user WHERE ID = @id \n";
                    cmd.ExecuteNonQuery();


                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return data;
        }

        [HttpPost]
        [Route("InsuranceDetailSubmit")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel InsuranceDetailSubmit([FromBody] List<SpecCodes> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    bool spec_1 = false;
                    bool spec_2 = false;

                    foreach (SpecCodes item in data)
                    {
                        if ((item.code == "14") && (item.selected == true))
                        {
                            spec_1 = true;
                        }
                        if ((item.code == "15") && (item.selected == true))
                        {
                            spec_2 = true;
                        }
                    }

                    if (spec_1 && spec_2)
                    {
                        foreach (SpecCodes item in data)
                        {
                            if ((item.code == "15"))
                            {
                                item.selected = false;
                            }
                        }
                    }
                    foreach (SpecCodes item in data)
                    {
                        cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Detail (MasterId,ProcCode,BenType,SpecCode,CreateDate,CreateBy,ChangedDate,ChangedBy) \n" +
                            $"VALUES('{item.id}','{item.procCode}','{item.benCode}','{item.code}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.user}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{item.user}')";

                        if (item.selected == true)
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("ValidateProcCode")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProcCode([FromBody] SpecCodes data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@proc", data.procCode.PadLeft(20, ' ')));
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.SERVICE_CODES WHERE SVCCODE = @proc";

                    dt.Load(cmd.ExecuteReader());

                    if (dt.Rows.Count > 0)
                    {
                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("InsuranceDetailUpdate")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel InsuranceDetailUPdate([FromBody] List<SpecCodes> data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                string ph = data[0].phcode;
                List<string> newList = new List<string>();

                string username = "";
                foreach (SpecCodes item in data)
                {
                    if (item.selected == true)
                    {
                        username = item.user;
                        newList.Add(item.code);
                    }
                }

                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                    int id = 0;
                    cmd.Connection = cn;
                    foreach (SpecCodes item in data)
                    {
                        if (item.selected == true)
                        {
                            id = item.id;
                        }
                    }

                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    cmd.CommandText = $"SELECT SpecCode FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail WHERE MasterId = @id ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    List<string> codes = new List<string>();

                    foreach (DataRow row in dt.Rows)
                    {
                        string code = "";
                        code = row["SpecCode"].ToString();
                        if (code.Length == 1)
                        {
                            code = "0" + code;
                        }
                        codes.Add(code);
                    }
                    List<string> delete = new List<string>();
                    List<string> newCodes = new List<string>();
                    List<string> update = new List<string>();

                    bool found = false;
                    foreach (string item in newList)
                    {

                        if (codes.Contains(item))
                        {
                            update.Add(item);
                        }
                        else
                        {
                            newCodes.Add(item);
                        }

                    }
                    foreach (string item in codes)
                    {
                        if (newList.Contains(item))
                        {

                        }
                        else
                        {
                            delete.Add(item);
                        }

                    }


                    foreach (string code in update)
                    {

                        foreach (SpecCodes item in data)
                        {

                            if (item.code == code)
                            {
                                if ((item.benCode != null) && (item.procCode != null) && (item.benCode != "") && (item.procCode != "") && (item.benCode.Length < 4))
                                {
                                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Insurance_Templates_Detail SET \n" +
                                         $"ProcCode = '{item.procCode}' , BenType = '{item.benCode}'," +
                                         $"ChangedDate = '{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}' WHERE   MasterId = '{id}' AND SpecCode = '{code}'\n";
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }

                    }

                    foreach (string code in delete)
                    {
                        string deletecode = code;
                        if (code.Length == 2)
                        {
                            if (code.Substring(0, 1) == "0")
                            {

                                deletecode = code.Substring(1, 1);
                            }
                        }
                        cmd.CommandText = $"DELETE FROM {PamcPortalDatabase}.dbo.Insurance_Templates_Detail  WHERE MasterId = '{id}' AND SpecCode = '{deletecode}'";
                        cmd.ExecuteNonQuery();

                    }

                    foreach (string code in newCodes)
                    {

                        foreach (SpecCodes item in data)
                        {

                            if (item.code == code)
                            {
                                cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.Insurance_Templates_Detail " +
                                             $"(MasterId,ProcCode,BenType,SpecCode,CreateDate,CreateBy,ChangedDate,ChangedBy) \n" +
                                          $"VALUES('{id}','{item.procCode}','{item.benCode}','{code}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}'," +
                                          $"'{username}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}','{username}')";
                                cmd.ExecuteNonQuery();
                            }
                        }

                    }

                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }

        [HttpGet]
        [Route("GetBenTypeCodes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<BenTrackCodes> GetBenTypeCodes()
        {
            List<BenTrackCodes> list = new List<BenTrackCodes>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_drcConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}.dbo.BENTRACK_CODES WHERE CODE IN('96','103','90') ORDER BY DESCR ASC";

                    DataTable dt = new DataTable();

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        BenTrackCodes t = new BenTrackCodes();

                        t.code = row["CODE"].ToString();
                        t.desc = row["DESCR"].ToString() + $" ({t.code})";

                        list.Add(t);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

            }
            return list;
        }

        [HttpPost]
        [Route("EntireBenifitHistSearch")]
        public List<BenHist> EntireBenifitHistSearch([FromBody] LoginDetailsModel data)
        {
            List<BenHist> list = new List<BenHist>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@prov", data.provID));

                    string sql = "SELECT bll.ID,bll.LogDate,hp.HPName,bll.MembId,bll.Methode,bll.ReturnedDetail \n"
           + $"FROM {PamcPortalDatabase}.dbo.BENIFIT_LOOKUP_LOG bll\n"
           + $"INNER JOIN {PamcPortalDatabase}.dbo.HealthPlanSetup hp ON hp.HPCode = bll.Hpcode\n"
           + "WHERE bll.ProvId = @prov ORDER BY bll.LogDate DESC";

                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        BenHist b = new BenHist();
                        if (data.dentist)
                        {
                            b.reff = "DRC#" + row["ID"].ToString();
                        }
                        if (data.specialist)
                        {
                            b.reff = "PAMC#" + row["ID"].ToString();
                        }
                        b.logDate = row["LogDate"].ToString();
                        b.hp = row["HPName"].ToString();
                        b.membId = row["MembId"].ToString().Replace(" ", "");
                        b.benName = row["Methode"].ToString().ToUpper().Trim();
                        if (b.benName == "RESTORATIONS" || b.benName == "FISSURE SEALANT")
                        {
                            b.results = new string[row["ReturnedDetail"].ToString().Split("\n").Length];
                            b.results = row["ReturnedDetail"].ToString().Trim().Split("\n");
                        }
                        else
                        {
                            b.results = new string[row["ReturnedDetail"].ToString().Split(";").Length];
                            b.results = row["ReturnedDetail"].ToString().Split(";");
                        }


                        list.Add(b);
                    }

                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "BENEFIT";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\BENEFIT\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }
    }
}
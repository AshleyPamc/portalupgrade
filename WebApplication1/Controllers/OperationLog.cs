﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NEWPAMCPORTAL.Models
{
    public class OperationLog
    {
        public void LogLogin(string operation, DateTime operationDate, string userName, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {

                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;

                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username)\r\n" +
                                           "VALUES (@operation,getdate(),@username)\r\n";

                    cmd.Parameters.Add("@operation", SqlDbType.VarChar).Value = operation;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = userName;

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void registration(string operation, DateTime operationDate, string userName, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void membRegistration(string operation, DateTime operationDate, string userName, string membNum, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username, memberNum)\r\n" +
                                           "VALUES (@operation,getdate(),@username,@membernumber)\r\n";
                    cmd.Parameters.Add("@operation", SqlDbType.VarChar).Value = operation;
                    cmd.Parameters.Add("@membernumber", SqlDbType.VarChar).Value = membNum;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = userName;
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void passwordChange(string operation, DateTime operationDate, string userName, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void forgetPassword(string operation, DateTime operationDate, string userName, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username)\r\n" +
                                           "VALUES (@operation ,getdate(),@username)\r\n";

                    cmd.Parameters.Add("@operation", SqlDbType.VarChar).Value = operation;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar, 20).Value = userName;

                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void LogRemittance(string operation, string userName, DateTime PaidDate, string dbaConnection)
        {
            string paidDateString = PaidDate.ToString("yyyy-MM-dd");

            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                         "(operation, operationDate, username,datePaid)\r\n" +
                         $"VALUES ('{ operation }',getdate(),'{ userName }',{ paidDateString })\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void LogClaim(string operation, DateTime operationDate, string userName, string practiceNumber, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username,practiceNumber)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "','" + practiceNumber + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void LogAuth(string operation, string userName, string practiceNumber, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username,practiceNumber)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "','" + practiceNumber + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void ErrorLog(string operation, string userName, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }

        }

        public void BrokerRegistration(string operation, string userName, string brokerId, string hpCode, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username,brokerId,brokerHpcode)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "','" + brokerId + "','" + hpCode + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void LogMedivouchValidate(string operation, string userName, string provid, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username,provid)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "','" + provid + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

        public void LogMedivouchRedeem(string operation, string userName, string provid, string membid, string dbaConnection)
        {
            using (SqlConnection Sqlcon = new SqlConnection(dbaConnection))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "INSERT INTO OperationalLog\r\n" +
                                           "(operation, operationDate, username,provid, memberNum)\r\n" +
                                           "VALUES ('" + operation + "',getdate(),'" + userName + "','" + provid + "','" + membid + "')\r\n";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;
                    cmd.ExecuteNonQuery();
                }
                Sqlcon.Close();
            }
        }

    }
}

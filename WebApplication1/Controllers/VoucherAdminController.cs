﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using WebApplication1.Models;
using MediWalletWebsite_1.Models;
using WebApplication1.Controllers;
using System.ServiceModel;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VoucherAdminController : Base.PAMCController
    {
        public VoucherAdminController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpGet]
        [Route("GetVoucherClients")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<VoucherClientModel> GetVoucherClients()
        {
            List<VoucherClientModel> list = new List<VoucherClientModel>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                   
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS ORDER BY CLIENTNAME";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    cn.Close();
                    cmd.Dispose();

                    foreach (DataRow row in dt.Rows)
                    {
                        VoucherClientModel t = new VoucherClientModel();

                        t.clientId = row["CLIENTID"].ToString();
                        t.clientName = row["CLIENTNAME"].ToString();
                        t.contactPerson = row["CONTACTPERSON"].ToString();
                        t.telNo = row["TELNO"].ToString();
                        t.email = row["EMAIL"].ToString();
                        t.hpCode = row["HPCODE"].ToString();
                        t.opt = row["OPT"].ToString();
                        t.createDate = (Convert.ToDateTime(row["CREATEDATE"].ToString())).ToString("yyyy/MM/dd");
                        //t.balance = "0.00";

                        list.Add(t);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("CreateNewClient")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public VoucherClientModel CreateNewClient([FromBody] VoucherClientModel details)
        {
            details.success = false;
            details.message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@clientName", details.clientName));
                    cmd.Parameters.Add(new SqlParameter("@contactPerson", details.contactPerson));
                    cmd.Parameters.Add(new SqlParameter("@telNo", details.telNo));
                    cmd.Parameters.Add(new SqlParameter("@email", details.email));
                    cmd.Parameters.Add(new SqlParameter("@hpcode", details.hpCode));
                    cmd.Parameters.Add(new SqlParameter("@opt", details.opt));
                    cmd.Parameters.Add(new SqlParameter("@user", details.user));
                    cmd.CommandText =
                        $"INSERT INTO {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS (CLIENTNAME, CONTACTPERSON, TELNO, EMAIL, HPCODE, OPT, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE) \n" +
                        $"OUTPUT INSERTED.CLIENTID \n " +
                        $"VALUES(@clientName, @contactPerson, @telNo, @email, @hpcode, @opt, @user, GETDATE(), @user, GETDATE())";

                    details.createDate = DateTime.Now.ToString("yyyy/MM/dd");
                    details.clientId = cmd.ExecuteScalar().ToString();
                    cn.Close();
                    cmd.Dispose();

                    details.success = true;
                    details.message = "";
                }
            }
            catch (Exception e)
            {
                details.success = false;
                details.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return details;
        }

        [HttpPost]
        [Route("UpdateClient")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public VoucherClientModel UpdateClient([FromBody] VoucherClientModel details)
        {
            details.success = false;
            details.message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                   
                    SqlCommand cmd = new SqlCommand();     
                    cmd.Connection = cn;
                    int id = Convert.ToInt32(details.clientId);
                    cmd.Parameters.Add(new SqlParameter("@clientId", id));
                    cmd.Parameters.Add(new SqlParameter("@clientName", details.clientName));
                    cmd.Parameters.Add(new SqlParameter("@contactPerson", details.contactPerson));
                    cmd.Parameters.Add(new SqlParameter("@telNo", details.telNo));
                    cmd.Parameters.Add(new SqlParameter("@email", details.email));
                    cmd.Parameters.Add(new SqlParameter("@hpCode", details.hpCode));
                    cmd.Parameters.Add(new SqlParameter("@opt", details.opt));
                    cmd.Parameters.Add(new SqlParameter("@user", details.user));

                    cmd.CommandText =
                        $"UPDATE {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS \n" +
                        $"SET CLIENTNAME = @clientName, CONTACTPERSON = @contactPerson, TELNO = @telNo, EMAIL = @email, HPCODE = @hpCode, OPT = @opt, LASTCHANGEBY = @user, LASTCHANGEDATE = GETDATE() \n " +
                        $"WHERE CLIENTID = @clientId ";

                    cmd.ExecuteScalar();
                    cn.Close();
                    cmd.Dispose();

                    details.success = true;
                    details.message = "";
                }
            }
            catch (Exception e)
            {
                details.success = false;
                details.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return details;
        }

        [HttpPost]
        [Route("DeleteClient")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteClient([FromBody] VoucherClientModel details)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.valid = false;
            vr.message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    int id = Convert.ToInt32(details.clientId);
                    cmd.Parameters.Add(new SqlParameter("@clientId", id));
                    cmd.CommandText =
                        $"SELECT * FROM {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS \n" +
                        $"WHERE CLIENTID = @clientId ";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    cn.Close();
                    
                    if (dt.Rows.Count > 0)
                    {
                        vr.valid = false;
                        vr.message = "Cannot delete this client because there are transactions for this client.";
                    }
                    else
                    {
                        if (cn.State != ConnectionState.Open) cn.Open();
                        cmd.CommandText =
                            $"DELETE FROM {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS \n" +
                            $"WHERE CLIENTID = @clientId ";

                        cmd.ExecuteNonQuery();

                        vr.valid = true;
                    }
                    cn.Close();
                    cmd.Dispose();                    
                }
            }
            catch (Exception e)
            {
                vr.valid = false;
                vr.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("GetClientTransactions")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<VoucherTransactionModel> GetClientTransactions([FromBody] VoucherClientModel details)
        {
            List<VoucherTransactionModel> list = new List<VoucherTransactionModel>();

            details.success = false;
            details.message = "";

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    int id = Convert.ToInt32(details.clientId);
                    cmd.Parameters.Add(new SqlParameter("@clientId", id));
                    cmd.CommandText =
                        $"SELECT VT.TRANID, VT.TRANCODE, TC.DESCRIPTION, VT.AMOUNT, VT.CLIENTID, VT.VOUCHERID, VT.VOUCHERBATCH, VT.NOTES, VT.CREATEDATE " +
                        $"FROM {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS VT \n" +
                        $" INNER JOIN {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTCODES TC ON (VT.TRANCODE = TC.TRANCODE) " +
                        $"WHERE CLIENTID = @clientId ";

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    cn.Close();

                    string balance = CalcClientBalance(id);

                    foreach (DataRow row in dt.Rows)
                    {
                        VoucherTransactionModel t = new VoucherTransactionModel();

                        t.tranId = row["TRANID"].ToString();
                        t.tranCode = row["TRANCODE"].ToString();
                        t.description = row["DESCRIPTION"].ToString();
                        t.amount = row["AMOUNT"].ToString();
                        t.clientId = row["CLIENTID"].ToString();
                        t.voucherId = row["VOUCHERID"].ToString();
                        t.voucherBatch = row["VOUCHERBATCH"].ToString();
                        t.notes = row["NOTES"].ToString();
                        t.createDate = (Convert.ToDateTime(row["CREATEDATE"].ToString())).ToString("yyyy/MM/dd");
                        t.balance = balance;

                        list.Add(t);
                    }

                    details.success = true;
                    details.message = "";
                }
            }
            catch (Exception e)
            {
                details.success = false;
                details.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return list;
        }

        [HttpPost]
        [Route("SaveDeposit")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public VoucherTransactionModel SaveDeposit([FromBody] VoucherTransactionModel details)
        {
            details.success = false;
            details.message = "";
            try
            {
                // See if aount is valid
                decimal amt;
                details.amount = details.amount.Replace(',', '.');
                if (Decimal.TryParse(details.amount, out amt))
                {
                    // get absolute value
                    amt = Math.Abs(amt);

                    // convert to negative
                    amt = amt * -1;
                }
                else
                {
                    details.success = false;
                    details.message = "Invalid amount";
                    return details;
                }

                if (details.notes == null) { details.notes = ""; }

                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();
                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@amount", amt));
                    cmd.Parameters.Add(new SqlParameter("@clientId", details.clientId));
                    cmd.Parameters.Add(new SqlParameter("@notes", details.notes));
                    cmd.Parameters.Add(new SqlParameter("@user", details.user));
                    cmd.CommandText =
                        $"INSERT INTO {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS (TRANCODE, AMOUNT, CLIENTID, NOTES, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE) \n" +
                        $"OUTPUT INSERTED.TRANID \n " +
                        $"VALUES('DEP', @amount, @clientId, @notes, @user, GETDATE(), @user, GETDATE())";

                    cmd.ExecuteScalar();
                    cn.Close();
                    cmd.Dispose();

                    details.success = true;
                    details.message = "";
                }
            }
            catch (Exception e)
            {
                details.success = false;
                details.message = e.Message;
                LogError(e.Message, e.StackTrace);
            }
            return details;
        }

        [HttpPost]
        [Route("GetBasketHeaders")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetBasketHeaders([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {      
                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                //cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE  Enabled = 1 AND HPCODE <> 'MDV' ";
                
                if (data.hpCode.Trim() != "" && data.option != null)
                {
                    cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@opt", data.option.Trim()));
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE  Enabled = 1 AND HPCODE = @hpCode and OPT = @opt ";
                }
                else if (data.hpCode.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode.Trim()));                    
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE  Enabled = 1 AND HPCODE = @hpCode ";
                }
                else
                {
                    cmd.CommandText = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER WHERE  Enabled = 1 AND OPT <> 'MEDIVOUCH' ";
                }
                                
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                cn.Close();
                cn.Dispose();
                cmd.Dispose();

                foreach (DataRow row in dt.Rows)
                {
                    DefaultCodeModel d = new DefaultCodeModel();
                    d.id = row["Id"].ToString();
                    d.desc = row["Description"].ToString();
                    d.fullPrice = row["PurchasePrice"].ToString();
                    d.hpCode = row["HpCode"].ToString();
                    d.option = row["Opt"].ToString();
                    list.Add(d);
                }                
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("GetBasketDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DefaultCodeModel> GetBasketDetails([FromBody] DefaultCodeModel data)
        {
            List<DefaultCodeModel> list = new List<DefaultCodeModel>();
            try
            {
                // Get basket details
                SqlConnection cn = new SqlConnection(_mediConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                if (data.hpCode.Trim() != "" && data.option != null)
                {
                    cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@opt", data.option.Trim()));
                    cmd.CommandText = $" SELECT sd.*,sh.Feeset FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd\r\n " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid\r\n " +
                                  $" WHERE  sh.Enabled = 1 AND sh.HPCODE = @hpCode AND sh.OPT = @opt ";
                }
                else if (data.hpCode.Trim() != "")
                {
                    cmd.Parameters.Add(new SqlParameter("@hpCode", data.hpCode.Trim()));                    
                    cmd.CommandText = $" SELECT sd.*,sh.Feeset FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd\r\n " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid\r\n " +
                                  $" WHERE  sh.Enabled = 1 AND sh.HPCODE = @hpCode ";
                }
                else
                {
                    cmd.CommandText = $" SELECT sd.*,sh.Feeset FROM {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL sd\r\n " +
                                  $" INNER JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER sh on sh.id = sd.headid\r\n " +
                                  $" WHERE  sh.Enabled = 1 AND sh.OTP <> 'MEDIVOUCH' ";
                }
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                cn.Close();

                // Get price from feeset
                cn = new SqlConnection(_drcConnectionString);
                if (cn.State != ConnectionState.Open) cn.Open();
                cmd = new SqlCommand();
                cmd.Connection = cn;
                DataTable dtFeeset = new DataTable();

                foreach (DataRow row in dt.Rows)
                {
                    DefaultCodeModel d = new DefaultCodeModel();
                    d.id = row["HeadId"].ToString();
                    d.code = row["Code"].ToString();
                    d.desc = row["Description"].ToString();
                    d.enterQty = Convert.ToBoolean(row["EnterQty"].ToString());
                    d.defaultQty = Convert.ToInt32(row["DefaultQty"].ToString());
                    d.enterTooth = Convert.ToBoolean(row["EnterToothNo"].ToString());
                    d.dependantOnCode = Convert.ToBoolean(row["DependantOnOtherCode"].ToString());
                    d.depCode = row["DependantCode"].ToString();

                    cmd.CommandText = $"SELECT USUALBILL FROM {DRCDatabase}.dbo.FEE_SCHEDS WHERE FEESET = {Convert.ToInt32(row["Feeset"].ToString())} AND PHCODE = '{row["PhCode"].ToString()}' AND BEGPROC = '{d.code.PadLeft(20, ' ')}' ";
                    dtFeeset.Clear();
                    dtFeeset.Load(cmd.ExecuteReader());
                                       
                    if (dtFeeset.Rows.Count > 0)
                    {
                        d.price = dtFeeset.Rows[0][0].ToString();
                    }
                    else
                    {
                        d.price = "";
                    }

                    d.total = (Convert.ToInt32(d.defaultQty) * Convert.ToDecimal(d.price)).ToString();
                    list.Add(d);
                }
                cn.Close();
                cn.Dispose();
                cmd.Dispose();                
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("AllocateVouchers")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public AllocationListModel AllocateVouchers([FromBody] AllocationListModel[] details)
        {
            AllocationListModel result = new AllocationListModel();
            result.success = false;
            result.message = "";
            try
            {
                SqlConnection cn = new SqlConnection(_portalConnectionString);
                SqlCommand cmd = new SqlCommand();
                DataTable datatable = new DataTable();
                string query = "";
                int b_ase = 36;
                int newVoucherIdInt = 0;
                string maxVouchIdStr = "";
                string newVoucherIdStr = "";
                Random rnd = new Random();
                int rd3 = 0;
                string delimitedNewVouchersStr = "";
                string maxVoucherBatch = ""; // Starting batch

                // Get/Generate voucher batchnumber
                query = $" SELECT MAX(VOUCHERBATCH) AS VOUCHERBATCH FROM {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS ";
                cmd = new SqlCommand(query, cn);
                if (cn.State != ConnectionState.Open) cn.Open();
                datatable = new DataTable();
                using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                {
                    adpt.Fill(datatable);
                }
                if(datatable.Rows[0]["VOUCHERBATCH"].ToString() == "")
                {
                    maxVoucherBatch = "V00001"; // Starting batch
                }
                else
                {
                    maxVoucherBatch = datatable.Rows[0]["VOUCHERBATCH"].ToString();
                    string batchPrefix = maxVoucherBatch.Substring(0, 1);
                    maxVoucherBatch = maxVoucherBatch.Substring(1);     
                    maxVoucherBatch = batchPrefix + (Convert.ToInt32(maxVoucherBatch) + 1).ToString().PadLeft(5, '0');
                }

                foreach (AllocationListModel voucherLine in details)
                {
                    if (voucherLine.qty > 0)
                    {
                        for (int x = 0; x < voucherLine.qty; x++)
                        {
                            // Get Basket (voucherid) details
                            query = $" SELECT * FROM {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER where id = @basketId ";
                            cmd = new SqlCommand(query, cn);
                            cmd.Parameters.Add(new SqlParameter("@basketId", voucherLine.basketId));
                            datatable = new DataTable();
                            if (cn.State != ConnectionState.Open) cn.Open();
                            using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                            {
                                adpt.Fill(datatable);
                            }
                            string provSpec = datatable.Rows[0]["Spec"].ToString();
                            string provClass = datatable.Rows[0]["Class"].ToString();

                            // Get max voucherid from database
                            query = $" SELECT MAX(VOUCHERID) AS MAX_VOUCHERID FROM {PamcPortalDatabase}.dbo.VOUCHER_MASTERS ";
                            cmd = new SqlCommand(query, cn);
                            if (cn.State != ConnectionState.Open) cn.Open();     
                            datatable = new DataTable();
                            using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                            {
                                adpt.Fill(datatable);
                            }
                            query = "";
                            maxVouchIdStr = "";
                            if (datatable.Rows[0]["MAX_VOUCHERID"].ToString().Trim() != "")
                            {
                                maxVouchIdStr = datatable.Rows[0]["MAX_VOUCHERID"].ToString();
                            }
                            else
                            {
                                maxVouchIdStr = "BB111"; // when table is empty, start at BC111
                            }

                            // Remove last 7 digits (3 random, 3 spec, 1 class)
                            maxVouchIdStr = maxVouchIdStr.Substring(0, maxVouchIdStr.Length - 7);

                            // Get decimal value and increment
                            newVoucherIdInt = toDeci(maxVouchIdStr, b_ase);
                            newVoucherIdInt++;

                            // Get string of incremented value 
                            newVoucherIdStr = fromDeci(b_ase, newVoucherIdInt);

                            // Generate and add last 3 random digits to add to new voucherid
                            rd3 = rnd.Next(100, 999);
                            newVoucherIdStr += rd3.ToString();

                            // Add spec (3 chars) to new voucherid
                            if (provSpec.Contains("'14'"))
                            {
                                provSpec = "014";
                            }
                            else
                            {
                                provSpec = "000";
                            }
                            newVoucherIdStr += provSpec;

                            // Add class (1 char) to new voucherid
                            if (provClass.Contains("'6'"))
                            {
                                newVoucherIdStr += "D";
                            }
                            else if (provClass.Contains("'7'"))
                            {
                                newVoucherIdStr += "S";
                            }
                            else
                            {
                                newVoucherIdStr += "U";
                            }

                            ////*** Create new voucher in VOUCHER_MASTERS table ***////                                                                                                                          
                            cmd = new SqlCommand();
                            cmd.Connection = cn;
                            if (cn.State != ConnectionState.Open) cn.Open();

                            cmd.Parameters.Add(new SqlParameter("@voucherId", newVoucherIdStr));
                            cmd.Parameters.Add(new SqlParameter("@basketId", voucherLine.basketId));
                            cmd.Parameters.Add(new SqlParameter("@clientId", voucherLine.clientId));
                            cmd.Parameters.Add(new SqlParameter("@user", voucherLine.user));

                            cmd.CommandText =
                                $" INSERT INTO {PamcPortalDatabase}.dbo.VOUCHER_MASTERS (VOUCHERID, BASKETID, CLIENTID, VALIDATEDBY, LASTVALIDATED, REDEEMED, REDEEMEDBY, REDEEMEDDATE, MEMBID, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE) \n" +
                                $" VALUES(@voucherId, @basketid, @clientId, NULL, NULL, 0, NULL, NULL, NULL, @user, GETDATE(), @user, GETDATE())";
                                                        
                            cmd.ExecuteScalar();                            

                            ////*** Create new transaction in VOUCHER_TRANSACTION table ***////                                                                                                                          
                            cmd = new SqlCommand();
                            cmd.Connection = cn;
                            if (cn.State != ConnectionState.Open) cn.Open();

                            cmd.Parameters.Add(new SqlParameter("@voucherId", newVoucherIdStr));
                            cmd.Parameters.Add(new SqlParameter("@voucherBatch", maxVoucherBatch));
                            //cmd.Parameters.Add(new SqlParameter("@amount", voucherLine.basketAmount));
                            cmd.Parameters.Add(new SqlParameter("@amount", voucherLine.fullPrice));
                            cmd.Parameters.Add(new SqlParameter("@clientId", voucherLine.clientId));
                            //cmd.Parameters.Add(new SqlParameter("@notes", voucherLine.notes));
                            cmd.Parameters.Add(new SqlParameter("@user", voucherLine.user));

                            cmd.CommandText =
                                    $"INSERT INTO {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS (TRANCODE, AMOUNT, CLIENTID, VOUCHERID, VOUCHERBATCH, NOTES, CREATEBY, CREATEDATE, LASTCHANGEBY, LASTCHANGEDATE) \n" +
                                    $"OUTPUT INSERTED.TRANID \n " +
                                    $"VALUES('PUR', @amount, @clientId, @voucherId, @voucherBatch, NULL, @user, GETDATE(), @user, GETDATE())";
                                                       
                            cmd.ExecuteScalar();                            

                            // Build delimited list of voucher string for SQL query
                            delimitedNewVouchersStr += "'" + newVoucherIdStr + "',";
                        }
                    }
                }

                ////*** Create file for newly created vouchers ***////

                // Remove last comma from delemited string before using in SQL query
                delimitedNewVouchersStr = delimitedNewVouchersStr.Remove(delimitedNewVouchersStr.Length - 1);

                //delimitedNewVouchersStr = "'BG537014D','BH592014D','BI211014S','BJ534014S'";
                //maxVoucherBatch = "V00002";

                // Get new vouchers and their info from database for email
                query = $" SELECT VC.CLIENTNAME AS COMPANY, VT.VOUCHERBATCH as BATCH, VM.VOUCHERID, SM.DESCRIPTION as BASKET, VT.AMOUNT as [PURCHASE AMOUNT], sum(FS.USUALBILL) as [PROVIDER AMOUNT] " +
                        $" FROM {PamcPortalDatabase}.dbo.VOUCHER_MASTERS VM " +
                        $" LEFT JOIN {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS VT ON(VM.VOUCHERID = VT.VOUCHERID and VM.CLIENTID = VT.CLIENTID) " +
                        $" LEFT JOIN {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS VC ON(VM.CLIENTID = VC.CLIENTID) " +
                        $" LEFT JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_HEADER SM on(VM.BASKETID = SM.id)  " +
                        $" LEFT JOIN {MediDatabase}.dbo.SVC_CODES_SETUP_DETAIL SD on (SM.id = SD.headid) " +
                        $" LEFT JOIN {DRCDatabase}.dbo.FEE_SCHEDS FS on (FS.FEESET = SM.Feeset AND ltrim(rtrim(FS.BEGPROC)) = SD.Code) " +
                        $" WHERE VM.VOUCHERID IN({delimitedNewVouchersStr}) and VT.VOUCHERBATCH = @voucherBatch " +
                        $" GROUP BY VC.CLIENTNAME, VT.VOUCHERBATCH, VM.VOUCHERID, SM.DESCRIPTION, VT.AMOUNT ";

                cmd = new SqlCommand(query, cn);
                cmd.Parameters.Add(new SqlParameter("@voucherBatch", maxVoucherBatch));
                if (cn.State != ConnectionState.Open) cn.Open();
                datatable = new DataTable();
                using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                {
                    adpt.Fill(datatable);
                }

                //// Get new vouchers and their info from database for file
                //query = $" SELECT VT.VOUCHERBATCH as BATCH, VM.VOUCHERID, VM.BASKETID as BASKET " +
                //        $" FROM {PamcPortalDatabase}.dbo.VOUCHER_MASTERS VM " +
                //        $" LEFT JOIN {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS VT ON(VM.VOUCHERID = VT.VOUCHERID and VM.CLIENTID = VT.CLIENTID) " +
                //        $" LEFT JOIN {PamcPortalDatabase}.dbo.VOUCHER_CLIENTS VC ON(VM.CLIENTID = VC.CLIENTID) " +
                //        $" LEFT JOIN {MediDatabase}.[dbo].[SVC_CODES_SETUP_HEADER] BA on(VM.BASKETID = BA.id) " +
                //        $" WHERE VM.VOUCHERID IN({delimitedNewVouchersStr}) and VT.VOUCHERBATCH = @voucherBatch ";

                //cmd = new SqlCommand(query, cn);
                //cmd.Parameters.Add(new SqlParameter("@voucherBatch", maxVoucherBatch));
                //if (cn.State != ConnectionState.Open) cn.Open();
                //DataTable datatableFile = new DataTable();
                //using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                //{
                //    adpt.Fill(datatableFile);
                //}

                if (datatable.Rows[0]["VOUCHERID"].ToString().Trim() != "")
                {
                    // Create Excel File
                    string clientName = datatable.Rows[0]["COMPANY"].ToString().Trim();
                    //clientName = clientName.Replace(" ", "_");
                    List<DataTable> lstDt = new List<DataTable>();
                    datatable.TableName = "VOUCHERS";
                    lstDt.Add(datatable);

                    // Create assets path
                    var root = _env.ContentRootPath;

                    FileInfo fiExcel;

                    if (Directory.Exists($"{root}\\ClientApp\\src\\assets\\Vouchers")) // scr folder only exists in dev environment (and frontend uses the assets folder in src while in dev mode)
                    {
                        fiExcel = new FileInfo($"{root}\\ClientApp\\src\\assets\\Vouchers\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{clientName.Replace(" ", "_")}_Vouchers.xlsx");
                    }
                    else // only a dist folder exist in live enviroment (and the frontend uses the assets folder in dist while in live mode)
                    {
                        fiExcel = new FileInfo($"{root}\\ClientApp\\dist\\assets\\Vouchers\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{clientName.Replace(" ", "_")}_Vouchers.xlsx");
                    }

                    //DirectoryInfo di = new DirectoryInfo($"{root}\\ClientApp\\..\\assets\\Vouchers");
                    //FileInfo fiExcel = new FileInfo($"{root}\\ClientApp\\..\\assets\\Vouchers\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{clientName.Replace(" ", "_")}_Vouchers.xlsx");
                    try
                    {
                        if (fiExcel.Exists)
                        {
                            fiExcel.Delete();
                        }
                    }
                    catch
                    {
                        throw;
                    }

                    // Create and Write Excel file to assets
                    bool freezeTopRow = true;
                    bool wrapText = false;
                    ExcelReport xrep = new ExcelReport(fiExcel, lstDt, freezeTopRow, wrapText);
                    xrep.CreateReport();

                    double minWidth = 0.00;
                    double maxWidth = 60.00;
                    xrep.SpecifyMinMaxWidth(fiExcel, minWidth, maxWidth);

                    // Create text file
                    FileInfo fiTextFile = new FileInfo($"{root}\\ClientApp\\dist\\assets\\Vouchers\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_{clientName.Replace(" ", "_")}_Vouchers.txt");
                    
                    if (!fiTextFile.Exists)
                    {
                        //Create a file to write to.
                        using (StreamWriter sw = fiTextFile.CreateText())
                        {
                            sw.WriteLine("VOUCHERBATCH|VOUCHERID|DESCRIPTION");                            
                        }
                    }
                                        
                    foreach (DataRow row in datatable.Rows)
                    {
                        using (StreamWriter sw = fiTextFile.AppendText())
                        {
                            sw.WriteLine(row["BATCH"].ToString() + "|" + row["VOUCHERID"].ToString() + "|" + row["BASKET"].ToString());                            
                        }
                    }

                    //Create email
                    //string body = $"Good day\r\n\r\nThe following vouchers were allocated for {clientName}:\r\n\r\n";
                    //foreach (DataRow row in datatable.Rows)
                    //{
                    //    body += " VOUCHER ID: " + row["VOUCHERID"].ToString() + " BATCH: " + row["BATCH"].ToString() + " PURCHASE AMOUNT: " + row["PURCHASE AMOUNT"].ToString() + " PROVIDER AMOUNT: " + row["PROVIDER AMOUNT"].ToString() + " BASKET: " + row["BASKET"].ToString() + "\r\n\r\n";
                    //}
                    //body += "\r\nRegards\r\nWeb Portal\r\n\r\n";
                    //string subject = $"Voucher allocation for {clientName}";
                    //string url = _EmailWebSvcConnection;
                    //EndpointAddress address = new EndpointAddress(url);
                    //BasicHttpBinding binding = new BasicHttpBinding();
                    //binding.MaxReceivedMessageSize = 2147483647;
                    //var time = new TimeSpan(0, 30, 0);
                    //binding.CloseTimeout = time;
                    //binding.OpenTimeout = time;
                    //binding.ReceiveTimeout = time;
                    //binding.SendTimeout = time;
                    //EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);
                    //testEmail.EmailServiceSoapClient emialclient = new testEmail.EmailServiceSoapClient(binding, address);
                    //try
                    //{
                    //    var q = client.SendEmailAsync(body, subject, "jaco@pamc.co.za");
                    //    q.Wait();
                    //}
                    //catch (Exception e)
                    //{
                    //}

                    // Prepare mail message
                    string subject = $"Voucher allocation for {clientName}";
                    string body = $"Good day\r\n\r\nThe following vouchers were allocated for {clientName}:\r\n\r\n";
                    foreach (DataRow row in datatable.Rows)
                    {
                        body += " VOUCHER ID: " + row["VOUCHERID"].ToString() + " BATCH: " + row["BATCH"].ToString() + " PURCHASE AMOUNT: " + row["PURCHASE AMOUNT"].ToString() + " PROVIDER AMOUNT: " + row["PROVIDER AMOUNT"].ToString() + " BASKET: " + row["BASKET"].ToString() + "\r\n\r\n";
                    }
                    body += "\r\nRegards\r\nWeb Portal\r\n\r\n";

                    // Get emailaddress to send to
                    cmd = new SqlCommand();
                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT description From {PamcPortalDatabase}.dbo.Settings where settingsId = 'VoucherAllocationEmail' ";
                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    string to = dt.Rows[0]["description"].ToString();

                    // Get mailserver to send from
                    cmd = new SqlCommand();
                    if (cn.State != ConnectionState.Open) cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    string server = dt.Rows[0]["SERVERADDRESS"].ToString();
                    string username = dt.Rows[0]["USERNAME"].ToString();
                    string password = dt.Rows[0]["PASSWORD"].ToString();
                    string from = dt.Rows[0]["MAILFROM"].ToString();
                    string port = dt.Rows[0]["OUTGOINGPORT"].ToString();

                    List<FileInfo> attachments = new List<FileInfo>();
                    attachments.Add(fiExcel);
                    attachments.Add(fiTextFile);

                    bool emailSuccess = false;

                    if (_isTest)
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PORTAL TEST: " + subject, body, _testEmails, attachments);
                    }
                    else
                    {
                        emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, to, attachments);
                    }

                    result.success = true;
                    result.message = fiExcel.Name;
                }
                else
                {
                    result.success = false;
                    result.message = "Unexpected Error Occurred";
                }

                cn.Close();
                cn.Dispose();
                cmd.Dispose();
            }
            catch (Exception e)
            {
                result.success = false;
                result.message = e.Message;

                LogError(e.Message, e.StackTrace);
            }

            return result;
        }

        // Function to convert a
        // number from given base
        // 'b' to decimal
        static int toDeci(string str, int b_ase)
        {
            int len = str.Length;
            int power = 1; // Initialize
                           // power of base
            int num = 0; // Initialize result
            int i;

            // Decimal equivalent is
            // str[len-1]*1 + str[len-2] *
            // base + str[len-3]*(base^2) + ...
            for (i = len - 1; i >= 0; i--)
            {
                // A digit in input number
                // must be less than
                // number's base
                if (val(str[i]) >= b_ase)
                {
                    Console.WriteLine("Invalid Number");
                    return -1;
                }

                num += val(str[i]) * power;
                power = power * b_ase;
            }

            return num;
        }

        // To return value of a char.
        // For example, 2 is returned
        // for '2'. 10 is returned
        // for 'A', 11 for 'B'
        static int val(char c)
        {
            if (c >= '0' && c <= '9')
                return (int)c - '0';
            else
                return (int)c - 'A' + 10;
        }

        static string fromDeci(int base1, int inputNum)
        {
            string s = "";

            // Convert input number is given
            // base by repeatedly dividing it
            // by base and taking remainder
            while (inputNum > 0)
            {
                s += reVal(inputNum % base1);
                inputNum /= base1;
            }
            char[] res = s.ToCharArray();

            // Reverse the result
            Array.Reverse(res);
            return new String(res);
        }

        // To return char for a value. For
        // example '2' is returned for 2.
        // 'A' is returned for 10. 'B' for 11
        static char reVal(int num)
        {
            if (num >= 0 && num <= 9)
                return (char)(num + 48);
            else
                return (char)(num - 10 + 65);
        }

        public string CalcClientBalance(int clientId)
        {
            string balance = "";

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open) cn.Open();                    
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@clientId", clientId));
                    cmd.CommandText = $"SELECT SUM(AMOUNT) FROM {PamcPortalDatabase}.dbo.VOUCHER_TRANSACTIONS WHERE CLIENTID = @clientId";
                    balance = cmd.ExecuteScalar().ToString();
                    cn.Close();
                    cmd.Dispose();
                }
            }
            catch (Exception e)
            {
                balance = "";
                LogError(e.Message, e.StackTrace);
            }

            return balance;
        }

        /*Method used to log all errors that take place in this controller*/
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "VOUCHERADMIN";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\VOUCHERADMIN\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}

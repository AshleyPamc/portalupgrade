﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using PamcEzLinkLibrary;
using NEWPAMCPORTAL.Models;
using WebApplication1.Controllers;
using System.Globalization;
using MediWalletWebsite_1.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClaimCaptureController : Base.PAMCController
    {
        public ClaimCaptureController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        public string fileIDRec0;
        public string fileIDRec1;

        [HttpPost]
        [Route("SubmitClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]

        public async Task<ActionResult<ClaimMasterModel>> SubmitClaim([FromBody] ClaimMasterModel receivedClaim)
        {
            // Write to log
            string jsonRequest = JsonConvert.SerializeObject(receivedClaim);
            Utillity.WriteToSqlLog(jsonRequest, "", "CLAIMCAPTURE", "SubmitClaim", _portalConnectionString, _env.ContentRootPath, User.Identity.Name);

            // Start claim capture
            ClaimMasterModel submittedClaim = new ClaimMasterModel();
            submittedClaim = receivedClaim;
            SqlConnection cn = new SqlConnection(_portalConnectionString);
            SqlCommand cmd = new SqlCommand("", cn);

            try
            {
                submittedClaim.apiCreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

                // Insert Record0
                cmd.CommandText =
                    " INSERT INTO [dbo].[Record0] \r\n" +
                    "           ([TYPE] \r\n" +
                    "           ,[CONTROL] \r\n" +
                    "           ,[FILLER1] \r\n" +
                    "           ,[PROVIDER] \r\n" +
                    "           ,[FILLER2] \r\n" +
                    "           ,[CLRHOUSE] \r\n" +
                    "           ,[FILLER3] \r\n" +
                    "           ,[DATECREATED] \r\n" +
                    "           ,[FILEER4]) \r\n" +
                    "           --,[DATEACCESSED] \r\n" +
                    "           --,[BATCHNO] \r\n" +
                    "           --,[COMPLETED]) \r\n" +
                    " OUTPUT INSERTED.FileID \r\n" +
                    "     VALUES \r\n" +
                    "           (0--<TYPE, int,> \r\n" +
                    "           ,'BATCH'--<CONTROL, varchar(5),> \r\n" +
                    "           ,'' --<FILLER1, varchar(1),> \r\n" +
                    "           ,'' --<PROVIDER, varchar(15),> \r\n" +
                    "           ,'' --<FILLER2, varchar(1),> \r\n" +
                    "           ,'' --<CLRHOUSE, varchar(10),> \r\n" +
                    "           ,'' --<FILLER3, varchar(1),> \r\n" +
                   $"           ,'{submittedClaim.apiCreateDate}' --<DATECREATED, varchar(30),> \r\n" +
                    "           ,'') --<FILEER4, varchar(38),> \r\n" +
                    "           --, <DATEACCESSED, varchar(10),> \r\n" +
                    "           --,<BATCHNO, varchar(20),> \r\n" +
                    "           --,<COMPLETED, bit,>) ";

                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                submittedClaim.fileId = Int32.Parse(cmd.ExecuteScalar().ToString());

                // Insert Record1 (Claim Master)
                cmd.Parameters.Add(new SqlParameter("@phCode", submittedClaim.provHosp));
                cmd.Parameters.Add(new SqlParameter("@provClaim", submittedClaim.provClaim));
                cmd.Parameters.Add(new SqlParameter("@authNo", submittedClaim.authNo));
                cmd.Parameters.Add(new SqlParameter("@provId", submittedClaim.provId));
                cmd.Parameters.Add(new SqlParameter("@membId", submittedClaim.membId));
                cmd.Parameters.Add(new SqlParameter("@claimType", submittedClaim.claimType));
                cmd.Parameters.Add(new SqlParameter("@refProvId", submittedClaim.refProvId));
                cmd.Parameters.Add(new SqlParameter("@vendor", submittedClaim.vendor));
                cmd.Parameters.Add(new SqlParameter("@dateReceived", submittedClaim.dateReceived.ToString("yyyy/MM/dd")));
                cmd.Parameters.Add(new SqlParameter("@fileId", submittedClaim.fileId));
                cmd.Parameters.Add(new SqlParameter("@userName", submittedClaim.userName));

                cmd.CommandText =
                " INSERT INTO [dbo].[Record1] \r\n" +
                "           ([TYPE] \r\n" +
                "           ,[PHCODE] \r\n" +
                "           ,[PROVCLAIM] \r\n" +
                "           ,[EZCAPUATH] \r\n" +
                "           ,[EZCAPPROV] \r\n" +
                "           ,[EZCAPMEMB] \r\n" +
                "           ,[PLACE] \r\n" +
                "           ,[OUTCOME] \r\n" +
                "           ,[EDIREF] \r\n" +
                "           ,[CASENUM] \r\n" +
                "           ,[UNITS] \r\n" +
                "           ,[CLAIMTYPE] \r\n" +
                "           ,[REFPROVID] \r\n" +
                "           ,[VENDORID] \r\n" +
                "           ,[DATERECD] \r\n" +
                "           ,[BATCH_NO] \r\n" +
                "           ,[EXT SWITCH] \r\n" +
                "           ,[FileID] \r\n" +
                "           ,[CREATEDATE] \r\n" +
                "           ,[CREATEBY]) \r\n" +
                " OUTPUT INSERTED.Rec1ID \r\n" +
                "     VALUES \r\n" +
                "           (1 --<TYPE, int,> \r\n" +
                "           ,@phCode  --<PHCODE, varchar(1),> \r\n" +
                "           ,@provClaim --<PROVCLAIM, varchar(20),> \r\n" +
                "           ,@authNo --<EZCAPUATH, varchar(16),> \r\n" +
                "           ,@provId --<EZCAPPROV, varchar(20),> \r\n" +
                "           ,@membId --<EZCAPMEMB, varchar(20),> \r\n" +
                "           ,'11' --<PLACE, varchar(2),> \r\n" +
                "           ,'' --<OUTCOME, varchar(2),> \r\n" +
                "           ,'' --<EDIREF, varchar(10),> \r\n" +
                "           ,'' --<CASENUM, varchar(50),> \r\n" +
                "           ,'' --<UNITS, varchar(50),> \r\n" +
                "           ,@claimType --<CLAIMTYPE, varchar(1),> \r\n" +
                "           ,@refProvId --<REFPROVID, varchar(20),> \r\n" +
                "           ,@vendor --<VENDORID, varchar(20),> \r\n" +
                "           ,@dateReceived --<DATERECD, varchar(10),> \r\n" +
                "           ,'' --<BATCH_NO, varchar(20),> \r\n" +
                "           ,'EZWEB' --<EXT SWITCH, varchar(20),> \r\n" +
                "           ,@fileId --<FileID, int,> \r\n" +
               $"           ,'{submittedClaim.apiCreateDate}' --<CREATEDATE, varchar(25),> \r\n" +
                "           ,@userName )--<CREATEBY, varchar(50),>) ";

                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                submittedClaim.rec1Id = Int32.Parse(cmd.ExecuteScalar().ToString());
                cmd.Connection.Close();

                // Insert Record5 (Claim Diags)                
                int diagRefCounter = 1;
                List<(int, string)> lstDiags = new List<(int, string)>();
                if (submittedClaim.diag1.Trim() != "" && lstDiags.FirstOrDefault(o => o.Item2 == submittedClaim.diag1).Item2 == null) lstDiags.Add((diagRefCounter++, submittedClaim.diag1.Trim()));
                if (submittedClaim.diag2.Trim() != "" && lstDiags.FirstOrDefault(o => o.Item2 == submittedClaim.diag2).Item2 == null) lstDiags.Add((diagRefCounter++, submittedClaim.diag2.Trim()));
                if (submittedClaim.diag3.Trim() != "" && lstDiags.FirstOrDefault(o => o.Item2 == submittedClaim.diag3).Item2 == null) lstDiags.Add((diagRefCounter++, submittedClaim.diag3.Trim()));
                if (submittedClaim.diag4.Trim() != "" && lstDiags.FirstOrDefault(o => o.Item2 == submittedClaim.diag4).Item2 == null) lstDiags.Add((diagRefCounter++, submittedClaim.diag4.Trim()));
                //if (submittedClaim.diag5.Trim() != "" && lstDiags.FirstOrDefault(o => o.Item2 == submittedClaim.diag5).Item2 == null) lstDiags.Add((diagRefCounter++, submittedClaim.diag5.Trim()));

                foreach (var item in lstDiags)
                {
                    cmd = new SqlCommand("", cn);
                    cmd.Parameters.Add(new SqlParameter("@diagRef", item.Item1));
                    cmd.Parameters.Add(new SqlParameter("@diag", item.Item2));
                    cmd.Parameters.Add(new SqlParameter("@rec1Id", submittedClaim.rec1Id));
                    cmd.CommandText =
                    " INSERT INTO [dbo].[Record5] \r\n" +
                    "           ([TYPE] \r\n" +
                    "           ,[DIAGREF] \r\n" +
                    "           ,[DIAG] \r\n" +
                    "           ,[Rec1ID]) \r\n" +
                    "     VALUES \r\n" +
                    "           (5 --<TYPE, int,> \r\n" +
                    "           ,@diagRef --<DIAGREF, int,> \r\n" +
                    "           ,@diag --<DIAG, varchar(10),> \r\n" +
                    "           ,@rec1Id) --<Rec1ID, int,>) ";

                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    cmd.ExecuteScalar();
                    cmd.Connection.Close();
                }

                // Insert Record2 (Claim Details)
                foreach (ClaimDetailModel line in submittedClaim.claimDetails)
                {
                    cmd = new SqlCommand("", cn);
                    cmd.Parameters.Add(new SqlParameter("@phCode", line.phCode));
                    cmd.Parameters.Add(new SqlParameter("@svcDateFrom", line.svcDateFrom.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new SqlParameter("@svcDateTo", line.svcDateTo.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new SqlParameter("@procCode", line.procCodePadded));
                    cmd.Parameters.Add(new SqlParameter("@procCodeDesc", line.procCodeDesc));
                    cmd.Parameters.Add(new SqlParameter("@modifier1", line.modifier1));
                    cmd.Parameters.Add(new SqlParameter("@modifier2", line.modifier2));
                    cmd.Parameters.Add(new SqlParameter("@modifier3", line.modifier3));
                    cmd.Parameters.Add(new SqlParameter("@modifier4", line.modifier4));
                    cmd.Parameters.Add(new SqlParameter("@qty", line.qty));
                    cmd.Parameters.Add(new SqlParameter("@billed", line.billed));
                    int diagRef = lstDiags.First(item => item.Item2.Trim() == line.diag).Item1;
                    cmd.Parameters.Add(new SqlParameter("@diagref", diagRef));
                    cmd.Parameters.Add(new SqlParameter("@tooth1", line.tooth1));
                    cmd.Parameters.Add(new SqlParameter("@tooth2", line.tooth2));
                    cmd.Parameters.Add(new SqlParameter("@tooth3", line.tooth3));
                    cmd.Parameters.Add(new SqlParameter("@tooth4", line.tooth4));
                    cmd.Parameters.Add(new SqlParameter("@tooth5", line.tooth5));
                    cmd.Parameters.Add(new SqlParameter("@tooth6", line.tooth6));
                    cmd.Parameters.Add(new SqlParameter("@tooth7", line.tooth7));
                    cmd.Parameters.Add(new SqlParameter("@tooth8", line.tooth8));
                    cmd.Parameters.Add(new SqlParameter("@note", line.note.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@rec1Id", submittedClaim.rec1Id));

                    cmd.CommandText =
                    " INSERT INTO [dbo].[Record2] \r\n" +
                    "           ([TYPE] \r\n" +
                    "           ,[PHCODE] \r\n" +
                    "           ,[FROMDATE] \r\n" +
                    "           ,[TODATE] \r\n" +
                    "           ,[PROCCODE] \r\n" +
                    "           ,[CURR_MODCODE] \r\n" +
                    "           ,[CURR_MODAMOUNT] \r\n" +
                    "           ,[MODCODE] \r\n" +
                    "           ,[MODAMOUNT] \r\n" +
                    "           ,[MODCODE2] \r\n" +
                    "           ,[MODAMOUNT2] \r\n" +
                    "           ,[MODCODE3] \r\n" +
                    "           ,[MODAMOUNT3] \r\n" +
                    "           ,[MODCODE4] \r\n" +
                    "           ,[MODAMOUNT4] \r\n" +
                    "           ,[QTY] \r\n" +
                    "           ,[BILLED] \r\n" +
                    "           ,[DIAGREF] \r\n" +
                    "           ,[BUNDLER] \r\n" +
                    "           ,[BUNDLERTYP] \r\n" +
                    "           ,[COPAY] \r\n" +
                    "           ,[MANDISCOUNT] \r\n" +
                    "           ,[TRANSACT_NO] \r\n" +
                    "           ,[PROCCODE_DESC] \r\n" +
                    "           ,[CLINICALCODE1] \r\n" +
                    "           ,[CLINICALCODE2] \r\n" +
                    "           ,[CLINICALCODE3] \r\n" +
                    "           ,[CLINICALCODE4] \r\n" +
                    "           ,[CLINICALCODE5] \r\n" +
                    "           ,[CLINICALCODE6] \r\n" +
                    "           ,[CLINICALCODE7] \r\n" +
                    "           ,[CLINICALCODE8] \r\n" +
                    "           ,[MEMO1] \r\n" +
                    "           ,[MEMO2] \r\n" +
                    "           ,[MEMO3] \r\n" +
                    "           ,[MEMO4] \r\n" +
                    "           ,[MEMO5] \r\n" +
                    "           ,[MEMO6] \r\n" +
                    "           ,[MEMO7] \r\n" +
                    "           ,[MEMO8] \r\n" +
                    "           ,[MEMO9] \r\n" +
                    "           ,[MEMO10] \r\n" +
                    "           ,[Rec1ID]) \r\n" +
                    " OUTPUT INSERTED.Rec2ID \r\n" +
                    "     VALUES  \r\n" +
                    "           (2 --<TYPE, int,> \r\n" +
                    "           ,@phCode --<PHCODE, varchar(1),> \r\n" +
                    "           ,@svcDateFrom --<FROMDATE, varchar(10),> \r\n" +
                    "           ,@svcDateTo --<TODATE, varchar(10),> \r\n" +
                    "           ,@procCode --<PROCCODE, varchar(20),> \r\n" +
                    "           ,'' --<CURR_MODCODE, varchar(7),> \r\n" +
                    "           ,'' --<CURR_MODAMOUNT, varchar(10),> \r\n" +
                    "           ,@modifier1 --<MODCODE, varchar(7),> \r\n" +
                    "           ,'' --<MODAMOUNT, varchar(10),> \r\n" +
                    "           ,@modifier2 --<MODCODE2, varchar(7),> \r\n" +
                    "           ,'' --<MODAMOUNT2, varchar(10),> \r\n" +
                    "           ,@modifier3 --<MODCODE3, varchar(7),> \r\n" +
                    "           ,'' --<MODAMOUNT3, varchar(10),> \r\n" +
                    "           ,@modifier4 --<MODCODE4, varchar(7),> \r\n" +
                    "           ,'' --<MODAMOUNT4, varchar(10),> \r\n" +
                    "           ,@qty --<QTY, varchar(10),> \r\n" +
                    "           ,@billed --<BILLED, varchar(10),> \r\n" +
                    "           ,@diagref --<DIAGREF, int,> \r\n" +
                    "           ,'' --<BUNDLER, varchar(5),> \r\n" +
                    "           ,'' --<BUNDLERTYP, varchar(1),> \r\n" +
                    "           ,'' --<COPAY, varchar(10),> \r\n" +
                    "           ,'' --<MANDISCOUNT, varchar(10),> \r\n" +
                    "           ,'' --<TRANSACT_NO, varchar(14),> \r\n" +
                    "           ,@procCodeDesc --<PROCCODE_DESC, varchar(200),> \r\n" +
                    "           ,@tooth1 --<CLINICALCODE1, varchar(10),> \r\n" +
                    "           ,@tooth2 --<CLINICALCODE2, varchar(10),> \r\n" +
                    "           ,@tooth3 --<CLINICALCODE3, varchar(10),> \r\n" +
                    "           ,@tooth4 --<CLINICALCODE4, varchar(10),> \r\n" +
                    "           ,@tooth5 --<CLINICALCODE5, varchar(10),> \r\n" +
                    "           ,@tooth6 --<CLINICALCODE6, varchar(10),> \r\n" +
                    "           ,@tooth7 --<CLINICALCODE7, varchar(10),> \r\n" +
                    "           ,@tooth8 --<CLINICALCODE8, varchar(10),> \r\n" +
                    "           ,'' --<MEMO1, varchar(40),> \r\n" +
                    "           ,'' --<MEMO2, varchar(40),> \r\n" +
                    "           ,'' --<MEMO3, varchar(40),> \r\n" +
                    "           ,'' --<MEMO4, varchar(40),> \r\n" +
                    "           ,'' --<MEMO5, varchar(40),> \r\n" +
                    "           ,'' --<MEMO6, varchar(40),> \r\n" +
                    "           ,'' --<MEMO7, varchar(40),> \r\n" +
                    "           ,'' --<MEMO8, varchar(40),> \r\n" +
                    "           ,'' --<MEMO9, varchar(40),> \r\n" +
                    "           ,@note --<MEMO10, varchar(40),> \r\n" +
                    "           ,@rec1Id) --<Rec1ID, int,>) ";

                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    line.rec2Id = Int32.Parse(cmd.ExecuteScalar().ToString());
                    cmd.Connection.Close();
                }

                // Insert Record4 (Claim Notes)  - We put detail line notes in MEMO10 now              
                //foreach (ClaimDetailModel line in submittedClaim.claimDetails)
                //{
                //    if (line.note.Trim() != "")
                //    {
                //        cmd = new SqlCommand("", cn);
                //        cmd.Parameters.Add(new SqlParameter("@note", line.note.Trim()));
                //        cmd.Parameters.Add(new SqlParameter("@lineNo", line.lineNo));
                //        cmd.Parameters.Add(new SqlParameter("@rec2Id", line.rec2Id));
                //        cmd.CommandText =
                //        " INSERT INTO [dbo].[Record4] \r\n" +
                //        "           ([TYPE] \r\n" +
                //        "           ,[NOTE] \r\n" +
                //        "           ,[LINE] \r\n" +
                //        "           ,[Rec2ID]) \r\n" +
                //        "     VALUES \r\n" +
                //        "           (4 --<TYPE, int,> \r\n" +
                //        "           ,@note --<NOTE, varchar(max),> \r\n" +
                //        "           ,@lineNo-- <LINE, int,> \r\n" +
                //        "           ,@rec2Id ) --<Rec2ID, int,>) ";

                //        if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                //        cmd.ExecuteScalar();
                //        cmd.Connection.Close();
                //    }
                //}

                // Insert Record8 (Claim Rejections)                
                foreach (ClaimDetailModel line in submittedClaim.claimDetails)
                {
                    if (line.rejCode.Trim() != "")
                    {
                        cmd = new SqlCommand("", cn);
                        cmd.Parameters.Add(new SqlParameter("@rejLineCounter", line.lineNo));
                        cmd.Parameters.Add(new SqlParameter("@rejCode", line.rejCode));
                        cmd.Parameters.Add(new SqlParameter("@billed", line.billed));
                        //cmd.Parameters.Add(new SqlParameter("@comment", line.rejCode.Trim() + "-" + line.rejCodeDesc.Trim()));
                        string comment = (line.rejCode.Trim() + "-" + line.rejCodeDesc.Trim()).Substring(0, Math.Min(200, (line.rejCode.Trim() + "-" + line.rejCodeDesc.Trim()).Length));
                        cmd.Parameters.Add(new SqlParameter("@comment", comment));
                        cmd.Parameters.Add(new SqlParameter("@rec2Id", line.rec2Id));
                        cmd.CommandText =
                        " INSERT INTO [dbo].[Record8] \r\n" +
                        "           ([TYPE] \r\n" +
                        "           ,[LINE] \r\n" +
                        "           ,[ADJCODE] \r\n" +
                        "           ,[ADJUST] \r\n" +
                        "           ,[COMMENT] \r\n" +
                        "           ,[Rec2ID]) \r\n" +
                        "     VALUES \r\n" +
                        "           (8 --<TYPE, int,> \r\n" +
                        "           ,@rejLineCounter --<LINE, int,> \r\n" +
                        "           ,@rejCode --<ADJCODE, varchar(7),> \r\n" +
                        "           ,@billed --<ADJUST, varchar(10),> \r\n" +
                        "           ,@comment --<COMMENT, varchar(200),> \r\n" +
                        "           ,@rec2Id )--<Rec2ID, int,>) ";

                        if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                        cmd.ExecuteScalar();
                        cmd.Connection.Close();
                    }
                }

                // Update Record0 completed column to true for EDI process to use claim
                cmd = new SqlCommand("", cn);
                cmd.Parameters.Add(new SqlParameter("@fileId", submittedClaim.fileId));
                cmd.CommandText = "Update [dbo].[Record0] SET COMPLETED = 1 WHERE FileID = @fileId";
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                cmd.ExecuteScalar();

                // Close and dispose connections
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
                cn.Close();
                cn.Dispose();

                // Write response to generic log
                string jsonResponse = JsonConvert.SerializeObject(submittedClaim);
                Utillity.WriteToSqlLog("", jsonResponse, "CLAIMCAPTURE", "SubmitClaim", _portalConnectionString, _env.ContentRootPath, User.Identity.Name);

                return Ok(submittedClaim);
            }
            catch (Exception ex)
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
                cmd.Dispose();
                cn.Close();
                cn.Dispose();

                // Log to old file
                LogError(ex.Message, ex.StackTrace);

                // Add error to Exception modelstate
                ModelState.AddModelError("Exception", ex.Message);
                ModelState.AddModelError("Exception", ex.StackTrace);

                // Write response to generic log
                string jsonResponse = JsonConvert.SerializeObject(ModelState);
                Utillity.WriteToSqlLog("", jsonResponse, "CLAIMCAPTURE", "SubmitClaim", _portalConnectionString, _env.ContentRootPath, User.Identity.Name);

                SendClaimFailedEmail(ex.Message, ex.StackTrace, JsonConvert.SerializeObject(receivedClaim));

                return ValidationProblem(ModelState);
            }
        }

        private void SendClaimFailedEmail(string errMsg, string stacktrace, string claimJson)
        {
            SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));

            try
            {
                // Get mailserver to send from
               
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();                
                cmd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                cmd.Connection.Close();

                string subject = "Portal Claim Capture Failed";
                string body = errMsg + "\r\n" + stacktrace + "\r\n\r\n" + claimJson + "\r\n\r\n";
                string server = dt.Rows[0]["SERVERADDRESS"].ToString();
                string username = dt.Rows[0]["USERNAME"].ToString();
                string password = dt.Rows[0]["PASSWORD"].ToString();
                string from = dt.Rows[0]["MAILFROM"].ToString();
                string port = dt.Rows[0]["OUTGOINGPORT"].ToString();

                List<FileInfo> attachments = new List<FileInfo>();

                bool emailSuccess = false;

                if (_isTest)
                {
                    emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, "PORTAL TEST: " + subject, body, _testEmails, attachments);
                }
                else
                {
                    emailSuccess = PAMC.Utilities.Email.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, "support@pamc.co.za;jaco@pamc.co.za", attachments);
                }
            }
            catch(Exception ex)
            {
                cmd.Connection.Close();
            }
        }

        [HttpGet]
        [Route("LoadAllDiagCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DiagCodes> GetAllDiagCodes()
        {
            List<DiagCodes> result = new List<DiagCodes>();

            try
            {
                SqlCommand cmd = new SqlCommand(" SELECT distinct DIAGCODE, DIAGDESC FROM DIAG_CODES ", new SqlConnection(_drcConnectionString));
                DataTable dtDiagCodes = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtDiagCodes.Load(cmd.ExecuteReader());

                foreach (DataRow row in dtDiagCodes.Rows)
                {
                    DiagCodes diag = new DiagCodes();
                    diag.code = row["DIAGCODE"].ToString();
                    diag.desc = row["DIAGDESC"].ToString();
                    result.Add(diag);
                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpGet]
        [Route("LoadAllModifierCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ModifierCodeModel> LoadAllModifierCodes()
        {
            List<ModifierCodeModel> result = new List<ModifierCodeModel>();

            try
            {
                SqlCommand cmd = new SqlCommand(" SELECT distinct MODIF, MODDESC FROM MODIF_CODES ", new SqlConnection(_drcConnectionString));
                DataTable dtModifCodes = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtModifCodes.Load(cmd.ExecuteReader());

                foreach (DataRow row in dtModifCodes.Rows)
                {
                    ModifierCodeModel mod = new ModifierCodeModel();
                    mod.modCode = row["MODIF"].ToString();
                    mod.modCodeDesc = row["MODDESC"].ToString();
                    result.Add(mod);
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpGet]
        [Route("GetRefProvSpecAndName")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProviderInfoModel GetRefProvSpecAndName(string provid)
        {
            ProviderInfoModel provInfo = new ProviderInfoModel();

            try
            {
                SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));
                cmd.Parameters.Add(new SqlParameter("@provid", provid));
                cmd.CommandText =
                " SELECT PM.PROVID,PM.LASTNAME + CASE WHEN RTRIM(LTRIM(isnull(PM.FIRSTNAME,''))) <> '' THEN ', ' + PM.FIRSTNAME ELSE '' END as [NAME], PS.SPECCODE, PC.DESCR \r\n" +
                " FROM PROV_MASTERS PM \r\n" +
                "   INNER JOIN PROV_SPECINFO PS on(PM.PROVID = PS.PROVID AND PS.TYPE = 'PRIMARY') \r\n" +
                "   INNER JOIN PROV_SPECCODES PC on(PS.SPECCODE = PC.CODE) \r\n" +
                " WHERE PM.PROVID = @provid";

                DataTable dtProvider = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtProvider.Load(cmd.ExecuteReader());

                if (dtProvider.Rows.Count > 0)
                {
                    provInfo.provid = dtProvider.Rows[0]["PROVID"].ToString();
                    provInfo.lastName = dtProvider.Rows[0]["NAME"].ToString();
                    provInfo.specCode = dtProvider.Rows[0]["SPECCODE"].ToString();
                    provInfo.specCodeDescr = dtProvider.Rows[0]["DESCR"].ToString();
                }
                else
                {
                    provInfo.provid = "";
                    provInfo.lastName = "";
                    provInfo.specCode = "";
                    provInfo.specCodeDescr = "";
                }

                cmd.Connection.Close();
                cmd.Dispose();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return provInfo;
        }

        [HttpGet]
        [Route("GetProvSpecAndNameAndVendors")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProviderAndVendorInfoModel GetProvSpecAndNameAndVendors(string provid)
        {
            ProviderAndVendorInfoModel provInfo = new ProviderAndVendorInfoModel();

            try
            {
                SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));
                cmd.Parameters.Add(new SqlParameter("@provid", provid));
                cmd.CommandText =
                " SELECT PM.PROVID,PM.LASTNAME + CASE WHEN RTRIM(LTRIM(isnull(PM.FIRSTNAME,''))) <> '' THEN ', ' + PM.FIRSTNAME ELSE '' END as [NAME], PS.SPECCODE, PC.DESCR \r\n" +
                " FROM PROV_MASTERS PM \r\n" +
                "   INNER JOIN PROV_SPECINFO PS on(PM.PROVID = PS.PROVID AND PS.TYPE = 'PRIMARY') \r\n" +
                "   INNER JOIN PROV_SPECCODES PC on(PS.SPECCODE = PC.CODE) \r\n" +
                " WHERE PM.PROVID = @provid";

                DataTable dtProvider = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtProvider.Load(cmd.ExecuteReader());

                if (dtProvider.Rows.Count > 0)
                {
                    provInfo.provid = dtProvider.Rows[0]["PROVID"].ToString();
                    provInfo.lastName = dtProvider.Rows[0]["NAME"].ToString();
                    provInfo.specCode = dtProvider.Rows[0]["SPECCODE"].ToString();
                    provInfo.specCodeDescr = dtProvider.Rows[0]["DESCR"].ToString();

                    // Get vendors
                    cmd.CommandText =
                          " SELECT PV.PROVID, PV.VENDOR, PM.LASTNAME, PM.PROVID + ' - ' + PM.LASTNAME + CASE WHEN RTRIM(LTRIM(isnull(PM.FIRSTNAME,''))) <> '' THEN ', ' + PM.FIRSTNAME ELSE '' END as DISPLAYNAME \r\n" +
                          " FROM PROV_MASTERS PM \r\n" +
                          "     INNER JOIN PROV_VENDINFO PV on(PM.PROVID = PV.VENDOR) \r\n" +
                          " WHERE isnull(PV.THRUDATE, '2099-12-31') > GETDATE() AND PV.PROVID = @provid \r\n" +
                          " ORDER BY PV.SEQUENCE";

                    dtProvider = new DataTable();
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    dtProvider.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dtProvider.Rows)
                    {
                        VendoridsModel vendor = new VendoridsModel();
                        vendor.provider = row["PROVID"].ToString();
                        vendor.vendor = row["VENDOR"].ToString();
                        vendor.lastName = row["LASTNAME"].ToString();
                        vendor.displayItem = row["DISPLAYNAME"].ToString();
                        provInfo.vendors.Add(vendor);
                    }
                }
                else
                {
                    provInfo.provid = "";
                    provInfo.lastName = "";
                    provInfo.specCode = "";
                    provInfo.specCodeDescr = "";
                }

                cmd.Connection.Close();
                cmd.Dispose();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return provInfo;
        }

        [HttpGet]
        [Route("ValidateTarrifCode")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<ProcCodeModel>> ValidateTarrifCode(string procCode)
        {
            try
            {
                ProcCodeModel procCodeDesc = new ProcCodeModel();

                SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));
                cmd.Parameters.Add(new SqlParameter("@procCode", procCode.Trim()));
                cmd.CommandText =
                " SELECT TOP 1 SVCCODE, SVCDESC \r\n" +
                " FROM PROC_CODES with(nolock) WHERE LTRIM(RTRIM(SVCCODE)) = @procCode \r\n" +
                " AND GETDATE() between FROMDATE and isnull(TODATE, '9999-12-31') ";

                DataTable dtProcCodes = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtProcCodes.Load(cmd.ExecuteReader());

                if (dtProcCodes.Rows.Count > 0)
                {
                    procCodeDesc.ProcCode = dtProcCodes.Rows[0]["SVCCODE"].ToString().ToUpper();
                    procCodeDesc.ProcCodeDesc = dtProcCodes.Rows[0]["SVCDESC"].ToString().ToUpper();
                }
                cmd.Connection.Close();
                cmd.Dispose();

                return Ok(procCodeDesc);
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

                ModelState.AddModelError("Exception", e.Message);
                return ValidationProblem(ModelState);
            }
        }

        [HttpGet]
        [Route("GetPhCodesByHpCodeAndOpt")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<List<ServicetypesModel>>> GetPhCodesByHpCodeAndOpt(string hpCode, string opt)
        {
            try
            {
                List<ServicetypesModel> listOfPhCodes = new List<ServicetypesModel>();
                SqlCommand cmd = new SqlCommand("", new SqlConnection(_portalConnectionString));
                cmd.Parameters.Add(new SqlParameter("@hpCode", hpCode));
                cmd.Parameters.Add(new SqlParameter("@opt", opt));
                cmd.CommandText = @"SELECT HPCODE, OPT, PHCODE, DESCR, [TYPE]
                                    FROM SERVICE_TYPES
                                    WHERE HPCODE = @hpCode AND
                                          (OPT = @opt OR OPT in ('X', 'Y')) AND
                                          SHOW = 1 
                                    ORDER BY DESCR";

                DataTable dtPhCodes = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtPhCodes.Load(cmd.ExecuteReader());

                foreach (DataRow dr in dtPhCodes.Rows)
                {
                    ServicetypesModel phCode = new ServicetypesModel();
                    phCode.phCode = dr["PHCODE"].ToString().ToUpper();
                    phCode.phDesc = dr["DESCR"].ToString().ToUpper();
                    phCode.phOpt = dr["OPT"].ToString().ToUpper();
                    phCode.hpCode = dr["HPCODE"].ToString().ToUpper();
                    phCode.type = dr["TYPE"].ToString().ToUpper();
                    listOfPhCodes.Add(phCode);
                }
                cmd.Connection.Close();
                cmd.Dispose();

                return Ok(listOfPhCodes);
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

                ModelState.AddModelError("Exception", e.Message);
                return ValidationProblem(ModelState);
            }
        }

        [HttpGet]
        [Route("GetRejectionCodesByLobCode")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<List<RejectionCodesModel>>> GetRejectionCodesByLobCode(string lobCode)
        {
            SqlCommand cmd = new SqlCommand("", new SqlConnection(_portalConnectionString));
            try
            {
                //cmd.Parameters.Add(new SqlParameter("@lobCode", lobCode));
                
                lobCode = lobCode.Replace(" ", "");
                if (lobCode.Contains("J") && lobCode.Contains("AU") && lobCode.Contains("LOA")) // Jaco 2024-09-10 Hardcode for Africa Assist
                {
                    lobCode = "'AU','LOA'";
                    cmd.CommandText = $" SELECT [ADJUST CODE], REASON, LOBCODE FROM RejectionReasons WHERE LOBCODE in ({lobCode})";
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@lobCode", lobCode));
                    cmd.CommandText = $" SELECT [ADJUST CODE], REASON, LOBCODE FROM RejectionReasons WHERE LOBCODE = @lobCode";
                }

                List<RejectionCodesModel> listRejCodes = new List<RejectionCodesModel>();
                         
                DataTable dtRejCodes = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtRejCodes.Load(cmd.ExecuteReader());

                foreach (DataRow dr in dtRejCodes.Rows)
                {
                    RejectionCodesModel rejCode = new RejectionCodesModel();
                    rejCode.code = dr["ADJUST CODE"].ToString().ToUpper();
                    rejCode.reason = dr["REASON"].ToString().ToUpper();
                    rejCode.lobCode = dr["LOBCODE"].ToString().ToUpper();
                    listRejCodes.Add(rejCode);
                }
                cmd.Connection.Close();
                cmd.Dispose();

                return Ok(listRejCodes);
            }
            catch (Exception e)
            {
                cmd.Connection.Close();

                LogError(e.Message, e.StackTrace);

                ModelState.AddModelError("Exception", e.Message);
                return ValidationProblem(ModelState);
            }
        }

        [HttpGet]
        [Route("LoadAllToothNumbers")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<List<ToothNumbersModel>>> LoadAllToothNumbers()
        {
            try
            {
                List<ToothNumbersModel> listOfToothNumbers = new List<ToothNumbersModel>();
                SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));
                cmd.CommandText = " SELECT CLINICALCODE,CLINICALDESC \r\n" +
                                  " FROM CLINICAL_CODES \r\n" +
                                  " WHERE CLINICALDESC not like '%INVALID%' \r\n" +
                                  " ORDER BY CONVERT(INT, CLINICALCODE)";
                DataTable dtToothNumbers = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dtToothNumbers.Load(cmd.ExecuteReader());
                foreach (DataRow dr in dtToothNumbers.Rows)
                {
                    ToothNumbersModel tooth = new ToothNumbersModel();
                    tooth.toothNum = dr["CLINICALCODE"].ToString().ToUpper();
                    tooth.toothNumDesc = dr["CLINICALDESC"].ToString().ToUpper();
                    listOfToothNumbers.Add(tooth);
                }
                cmd.Connection.Close();
                cmd.Dispose();

                return Ok(listOfToothNumbers);
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

                ModelState.AddModelError("Exception", e.Message);
                return ValidationProblem(ModelState);
            }
        }

        [HttpGet]
        [Route("GetPlacesOfService")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<PlaceOfServiceModel> GetPlacesOfService()
        {
            //Local Declarations
            DataTable dt = new DataTable();
            List<PlaceOfServiceModel> lstPlacesToBeReturned = new List<PlaceOfServiceModel>();

            try
            {
                //Prepare Connection 
                SqlConnection cn = new SqlConnection();
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }

                //Prepare sql command
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "SELECT * FROM placesvc_codes";

                //Fill DataTable
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                //Populate List
                foreach (DataRow dr in dt.Rows)
                {
                    PlaceOfServiceModel pc = new PlaceOfServiceModel();
                    pc.Code = (dr["CODE"]).ToString();
                    pc.Descr = dr["Descr"].ToString().ToUpper();
                    object testDefualt = dr["Default_YN"];
                    if (!DBNull.Value.Equals(dr["Default_YN"]))
                    {
                        pc.Default_YN = Convert.ToBoolean(dr["Default_YN"]);
                    }
                    else { pc.Default_YN = false; }
                    lstPlacesToBeReturned.Add(pc);
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            return lstPlacesToBeReturned;
        }

        [HttpPost]
        [Route("GetRejectionCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<RejectionCodesModel> GetRejectionCodes([FromBody] RejectionCodesModel lobCode)
        {
            List<RejectionCodesModel> returnedList = new List<RejectionCodesModel>();
            DataTable dataTable = new DataTable();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string lob = "";

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            try
            {
                cmd.Connection = cn;

                // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                //cmd.CommandText = $"SELECT lobcode FROM  UserLobCodesHpCodes where username = '{lobCode.username}'";  

                // Jaco 2023-09-11
                cmd.CommandText = $" SELECT u.lobcode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + //Tested with client login
                                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                  $"       {DRCDatabase}..HP_CONTRACTS AS hp " +
                                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{lobCode.username}' ";

                using (SqlDataReader drow = cmd.ExecuteReader())
                {
                    dt.Load(drow);
                }
                foreach (DataRow rows in dt.Rows)
                {
                    lob = rows["lobcode"].ToString();

                }
                dt.Clear();
                cmd.CommandText = $"SELECT * FROM RejectionReasons WHERE LOBCODE = '{lob}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dataTable.Load(dr);
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    RejectionCodesModel reason = new RejectionCodesModel();
                    reason.code = row["ADJUST CODE"].ToString();
                    reason.reason = row["REASON"].ToString().ToUpper();
                    returnedList.Add(reason);

                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return returnedList;

        }

        [HttpPost]
        [Route("GetHpCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<HealthplanModelcs> GetHpCodes([FromBody] RejectionCodesModel lobData)
        {
            DataTable dt = new DataTable();
            DataTable tabledt = new DataTable();
            List<HealthplanModelcs> lstHpCodesToBeReturned = new List<HealthplanModelcs>();
            string lob = "";
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            try
            {
                if (lobData.userType == 1) // Provider
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    //Prepare sql command
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT HPCODE, HPNAME, LOBCODE  FROM  {PamcPortalDatabase}..HealthPlanSetup where allowProviderClaimCapture = 1";

                    //Fill DataTable
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    //Populate List
                    foreach (DataRow dr in dt.Rows)
                    {
                        HealthplanModelcs pc = new HealthplanModelcs();
                        pc.code = (dr["HPCODE"]).ToString().ToUpper();
                        pc.name = dr["HPNAME"].ToString().ToUpper();
                        pc.lobcode = dr["LOBCODE"].ToString().ToUpper();
                        lstHpCodesToBeReturned.Add(pc);
                    }
                    cn.Close();


                }
                else // Should be only userType 3 - Client
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    //Prepare sql command
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    // Jaco comment out 2023-09-08 - View is hardcoded to DRC
                    //cmd.CommandText = $" SELECT lobcode FROM  UserLobCodesHpCodes where username = '{lobData.username}' "; 

                    // Jaco 2023-09-08
                    //cmd.CommandText = $" SELECT u.lobcode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " + // Tested with client login
                    //                  $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                    //                  $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                    //                  $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{lobData.username}' ";

                    // Jaco 2024-08-28
                    cmd.Parameters.Add(new SqlParameter("@userName", lobData.username));
                    cmd.CommandText = $" SELECT lobcode FROM Users where username = @userName ";

                    //Fill DataTable
                    using (SqlDataReader drow = cmd.ExecuteReader())
                    {
                        tabledt.Load(drow);
                    }

                    if (tabledt.Rows.Count > 0)
                    {
                        //Populate List
                        List<string> lstLobCodes = new List<string>();
                        foreach (DataRow rows in tabledt.Rows)
                        {
                            lob = rows["lobcode"].ToString();
                            lob = lob.Replace(" ", "");
                            string[] Split = lob.Split(",");
                            foreach (var val in Split)
                            {
                                if (!lstLobCodes.Contains(val.Trim()))
                                {
                                    lstLobCodes.Add(val);
                                }
                            }
                        }
                        //tabledt.Clear();
                        //if (lob.Contains(","))
                        //{
                        //    StringBuilder delimitedString = new StringBuilder();
                        //    string[] Split = lob.Split(",");
                        //    foreach (var val in Split)
                        //    {
                        //        delimitedString.Append("'" + val + "'" + ",");
                        //        string x = delimitedString.ToString();
                        //        x = x.Remove(x.Length - 1);
                        //        lob = x;
                        //    }
                        //    cmd.CommandText = $"SELECT  * FROM INSURANCE_CLIENTS WHERE LOBCODE in ({lob})";
                        //}
                        //else
                        //{
                        //    cmd.CommandText = $"SELECT  * FROM INSURANCE_CLIENTS WHERE LOBCODE = '{lob}'";
                        //}

                        string lobCodesParam = "";
                        foreach (string lobCode in lstLobCodes)
                        {
                            lobCodesParam += $"'{lobCode}',";
                        }
                        lobCodesParam = lobCodesParam.Substring(0, lobCodesParam.Length - 1); // Remove delimiter at the end

                        cmd.CommandText = $"SELECT  * FROM INSURANCE_CLIENTS WHERE LOBCODE in ({lobCodesParam})";
                        
                        //Fill DataTable
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt.Load(dr);
                        }

                        //Populate List
                        foreach (DataRow dr in dt.Rows)
                        {
                            HealthplanModelcs pc = new HealthplanModelcs();
                            pc.code = (dr["HPCODE"]).ToString().ToUpper();
                            pc.name = dr["HPNAME"].ToString().ToUpper();
                            pc.lobcode = dr["LOBCODE"].ToString().ToUpper();
                            lstHpCodesToBeReturned.Add(pc);
                        }
                    }
                    cn.Close();
                }

            }
            catch (Exception e)
            {
                cn.Close();
                LogError(e.Message, e.StackTrace);
                throw;
            }
            return lstHpCodesToBeReturned;

        }

        [HttpPost]
        [Route("GetPrefix")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetPrefix([FromBody] RecordType1 data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlConnection cn = new SqlConnection();
            try
            {
                // Jaco comment out 2023-09-27 (Seems this endpoint is not being called from anywhere in the front end anyways)
                //cn.ConnectionString = "Server=192.168.16.249;Database=Reporting;User Id=sa;Password = galnetdata; ";

                cn.ConnectionString = _reportingConnectionString; // Jaco 2023-09-27
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = $"SELECT MemberPrefix From OxygenBenefitOptions WHERE ezHPCode ='{data.hpCode}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    vr.message = dt.Rows[0][0].ToString();
                    if (vr.message == null)
                    {
                        vr.message = "";
                    }
                }
                dt.Reset();
                cn.Close();
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return vr;
        }

        [HttpPost]
        [Route("GetVendorIDs")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<VendoridsModel> GetVendorIDs([FromBody] RecordType1 provId)
        {
            //Local DEclarations
            DataTable dt = new DataTable();
            List<VendoridsModel> lstVendorIDsReturned = new List<VendoridsModel>();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            try
            {
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }

                //Prepare sql command
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = $"SELECT pv.PROVID, pv.VENDOR, pm.LASTNAME, pv.VENDOR +' - ' +pm.LASTNAME as DisplayName " +
                    $"FROM PROV_VENDINFO AS pv " +
                    $"INNER JOIN PROV_MASTERS AS pm ON pv.VENDOR = pm.PROVID " +
                    $"WHERE pv.THRUDATE is null and(pv.PROVID = '{provId.EZCAPPROV}')";
                //Fill DataTable

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                //Populate List
                foreach (DataRow dr in dt.Rows)
                {
                    VendoridsModel venIds = new VendoridsModel();
                    venIds.provider = dr["PROVID"].ToString();
                    venIds.vendor = dr["VENDOR"].ToString();
                    venIds.lastName = dr["LASTNAME"].ToString().ToUpper();
                    venIds.displayItem = dr["DisplayName"].ToString();
                    lstVendorIDsReturned.Add(venIds);
                }
                cn.Close();
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return lstVendorIDsReturned;
        }


        //[HttpPost] //Jaco comment out 2024-03-04
        //[Route("ValidateEzcapProv")]
        //[Authorize(Roles = "NormalUser,Administrator")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public ValidationResultsModel ValidateEzcapProv([FromBody] RecordType1 inputData)
        //{
        //    ValidationResultsModel result = new ValidationResultsModel();
        //    string name;
        //    string sqlStatement = $"SELECT * FROM PROV_MASTERS WHERE PROVID = '{inputData.EZCAPPROV}'";
        //    try
        //    {
        //        bool dbsValidData = ExecuteSql(inputData, sqlStatement);

        //        if (dbsValidData == false)
        //        {
        //            result.valid = false;
        //            result.message = "Practice Number is incorrect";
        //            return result;
        //        }
        //        else
        //        {
        //            result.valid = true;
        //            sqlStatement = $"SELECT LASTNAME FROM PROV_MASTERS WHERE PROVID ='{inputData.EZCAPPROV}'";
        //            name = ReturnValidDataRecordType1(inputData, sqlStatement);
        //            result.message = $"{name}";
        //            return result;
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        LogError(e.Message, e.StackTrace);
        //    }
        //    return result;
        //}

        [HttpPost] //Jaco 2024-03-04
        [Route("ValidateEzcapProv")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProviderInfoModel ValidateEzcapProv([FromBody] RecordType1 inputData)
        {
            ProviderInfoModel result = new ProviderInfoModel();

            try
            {
                SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));
                cmd.Parameters.Add(new SqlParameter("@provId", inputData.EZCAPPROV));
                cmd.CommandText =
                    " SELECT PM.PROVID, PM.LASTNAME, PS.SPECCODE, PC.DESCR \n" +
                    " FROM PROV_MASTERS PM \n" +
                    "  LEFT JOIN PROV_SPECINFO PS on(PM.PROVID = PS.PROVID) \n" +
                    "  LEFT JOIN PROV_SPECCODES PC on(PS.SPECCODE = PC.CODE) \n" +
                    " WHERE PM.PROVID = @provId AND TYPE = 'PRIMARY' ";

                DataTable dt = new DataTable();
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                dt.Load(cmd.ExecuteReader());
                cmd.Connection.Close();
                cmd.Dispose();

                if (dt.Rows.Count > 0)
                {
                    result.provid = dt.Rows[0]["PROVID"].ToString();
                    result.lastName = dt.Rows[0]["LASTNAME"].ToString();
                    result.specCode = dt.Rows[0]["SPECCODE"].ToString();
                    result.specCodeDescr = dt.Rows[0]["DESCR"].ToString();
                }
                else
                {
                    result.provid = inputData.EZCAPPROV;
                    result.lastName = "Practice Number is incorrect";
                    result.specCode = "";
                    result.specCodeDescr = "";
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }

            return result;
        }

        //[HttpPost] // Jaco comment out 2024-01-05
        //[Route("ValidateEzcapMemb")]
        //[Authorize(Roles = "NormalUser,Administrator")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public ValidationResultsModel ValidateEzcapMemb([FromBody] TestClaimHeaderModel inputData)
        //{
        //    ValidationResultsModel result = new ValidationResultsModel();
        //    string name;
        //    try
        //    {
        //        // Jaco 2023-09-27 Parameterise databases
        //        string sqlStatement =
        //           "declare @membid varchar(20)\n" // Tested with client login
        //          + "declare @hpcode varchar(20)\n"
        //          + $"set @membid ='{inputData.membNum}'\n"
        //          + $"set @hpcode = '{inputData.hpCode}'\n"
        //          + $"select LASTNM from {DRCDatabase}..MEMB_MASTERS \n"
        //          + "inner join (\n"
        //          + $"select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix from {ReportingDatabase}..[OxygenBenefitOptions] inner join " + this.DRCDatabase + $"..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\n"
        //          + ") op on op.ezHpCode = HPCODE\n"
        //          + "where \n"
        //          + "MEMB_MASTERS.HPCODE = @hpcode\n"
        //          + "and\n"
        //          + "(SUBSSN = 'M' + isnull([MemberPrefix],'') + @membid) \n"
        //           + "AND RLSHIP = '1'";
        //        bool dbsValidData = ExecuteSqlTest(inputData, sqlStatement);
        //        if (dbsValidData == false)
        //        {
        //            result.valid = false;
        //            result.message = "Family Number is incorrect";
        //            return result;
        //        }
        //        else
        //        {
        //            result.valid = true;
        //            // Jaco 2023-09-27 Parameterise databases
        //            sqlStatement = "\n"
        //       + "declare @membid varchar(20)\n" // Tested with client login
        //       + "declare @hpcode varchar(20)\n"
        //       + $"set @membid ='{inputData.membNum}'\n"
        //       + $"set @hpcode = '{inputData.hpCode}'\n"
        //       + $"select LASTNM from {DRCDatabase}..MEMB_MASTERS \n"
        //       + "inner join (\n"
        //       + $"select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix from {ReportingDatabase}..[OxygenBenefitOptions] inner join " + this.DRCDatabase + $"..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\n"
        //       + ") op on op.ezHpCode = HPCODE\n"
        //       + "where \n"
        //       + "MEMB_MASTERS.HPCODE = @hpcode\n"
        //       + "and\n"
        //       + "(SUBSSN = 'M' + isnull([MemberPrefix],'') + @membid OR SUBSSN = 'M'+ @membid OR MEMBID = @membid OR isnull([MemberPrefix],'') + @membid = MEMBID) \n"
        //       + "AND RLSHIP = '1'";

        //            name = ReturnValidDataRecordType1Test(inputData, sqlStatement);
        //            result.message = $"{name} Family";

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogError(e.Message, e.StackTrace);
        //    }
        //    return result;
        //}


        //[HttpPost] // Jaco comment out 2024-01-05
        //[Route("ValidateEzcapDepCode")]
        //[Authorize(Roles = "NormalUser,Administrator")]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public ValidationResultsModel ValidateEzcapDepCode([FromBody] TestClaimHeaderModel inputData)
        //{
        //    ValidationResultsModel result = new ValidationResultsModel();
        //    string name;
        //    try
        //    {
        //        // Jaco 2023-09-27 - Parameterise hardcoded databases
        //        string sqlStatement = "\n"
        //+ "declare @membid varchar(20)\n" // Tested with client login
        //+ "declare @hpcode varchar(20)\n"
        //+ $"set @membid ='{inputData.membNum}'\n"
        //+ $"set @hpcode = '{inputData.hpCode}'\n"
        //+ $"select LASTNM,FIRSTNM from {DRCDatabase}..MEMB_MASTERS \n"
        //+ "inner join (\n"
        //+ $"select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix from {ReportingDatabase}..[OxygenBenefitOptions] inner join " + this.DRCDatabase + $"..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\n"
        //+ ") op on op.ezHpCode = HPCODE\n"
        //+ "where \n"
        //+ "MEMB_MASTERS.HPCODE = @hpcode\n"
        //+ "and\n"
        //+ "(isnull([MemberPrefix],'') + @membid = MEMBID)";

        //        bool dbsValidData = ExecuteSqlTest(inputData, sqlStatement);

        //        if (dbsValidData == false)
        //        {
        //            result.valid = false;
        //            result.message = "Dependant Number is incorrect";
        //            return result;
        //        }
        //        else
        //        {
        //            result.valid = true;

        //            // Jaco 2023-09-27 - Parameterise hardcoded databases
        //            sqlStatement = "\n"
        //       + "declare @membid varchar(20)\n" // Tested with client login
        //       + "declare @hpcode varchar(20)\n"
        //       + $"set @membid ='{inputData.membNum}'\n"
        //       + $"set @hpcode = '{inputData.hpCode}'\n"
        //       + $"select FIRSTNM, LASTNM from {DRCDatabase}..MEMB_MASTERS \n"
        //       + "inner join (\n"
        //       + $"select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix from {ReportingDatabase}..[OxygenBenefitOptions] inner join " + this.DRCDatabase + $"..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\n"
        //       + ") op on op.ezHpCode = HPCODE\n"
        //       + "where \n"
        //       + "MEMB_MASTERS.HPCODE = @hpcode\n"
        //       + "and\n"
        //       + "(isnull([MemberPrefix],'') + @membid = MEMBID)";
        //            name = ReturnValidDataRecordType1Test(inputData, sqlStatement);
        //            result.message = $"{name}";
        //            return result;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogError(e.Message, e.StackTrace);
        //    }
        //    return result;
        //}

        [HttpPost]
        [Route("ValidateRefPRov")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateRefPRov([FromBody] RecordType1 inputData)
        {
            ValidationResultsModel result = new ValidationResultsModel();
            string name;
            try
            {
                string sqlState = $"SELECT * FROM PROV_MASTERS WHERE PROVID ='{inputData.REFPROVID}'";
                bool dbsValidData = ExecuteSql(inputData, sqlState);

                if (dbsValidData == false)
                {
                    result.valid = false;
                    result.message = "Reffering Practice Number  is incorrect";
                    return result;
                }
                else
                {
                    result.valid = true;
                    sqlState = $"SELECT LASTNAME FROM PROV_MASTERS WHERE PROVID ='{inputData.REFPROVID}'";
                    name = ReturnValidDataRecordType1(inputData, sqlState);
                    result.message = $"{name}";
                    return result;
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("ValidateEzcapAuth")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateEzcapAuth([FromBody] RecordType1 inputData)
        {
            ValidationResultsModel result = new ValidationResultsModel();

            try
            {
                string sqlStatement = $"SELECT * FROM AUTH_MASTERS WHERE AUTHNO = '{inputData.EZCAPAUTH}'";
                bool dbsValidData = ExecuteSql(inputData, sqlStatement);

                if (dbsValidData == false)
                {
                    result.valid = false;
                    result.message = "Auth Number is incorrect";
                    return result;
                }
                else
                {
                    result.valid = true;
                    result.message = $"{inputData.EZCAPAUTH}";
                    return result;
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("GetAllClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimReport> GetAllClaims([FromBody] ClaimReport data)
        {
            List<ClaimReport> listToReturn = new List<ClaimReport>();
            try
            {
                //ClaimReport claim = new ClaimReport();                
                DataTable dt = new DataTable();
                SqlConnection cn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();

                DateTime capturedFrom = Convert.ToDateTime(data.capturedFrom);
                DateTime capturedTo = Convert.ToDateTime(data.capturedTo);

                cmd.Parameters.Add(new SqlParameter("@capturedFrom", capturedFrom));
                cmd.Parameters.Add(new SqlParameter("@capturedTo", capturedTo.AddDays(1)));
                cmd.Parameters.Add(new SqlParameter("@username", data.username));
                cmd.CommandTimeout = 60;

                string[] hpcodes;
                string hpcodesDelimted = "";

                cn.ConnectionString = _portalConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                if (data.hpcode.Contains(';'))
                {
                    hpcodes = new string[data.hpcode.Split(';').Length];
                    hpcodes = data.hpcode.Split(';');
                    for (int i = 0; i < hpcodes.Length; i++)
                    {
                        hpcodes[i] = hpcodes[i].TrimEnd();

                        if (i == (hpcodes.Length - 1))
                        {
                            hpcodesDelimted += $"'{hpcodes[i]}'";
                        }
                        else
                        {
                            hpcodesDelimted += $"'{hpcodes[i]}',";
                        }
                    }
                }
                else
                {
                    hpcodes = new string[1];
                    hpcodes[0] = data.hpcode;
                    hpcodesDelimted = $"'{hpcodes[0]}'";
                }
                // whereclaus = $"HPCODE = '{hpcodes[0]}'";
                cmd.Connection = cn;

                // Jaco comment out 2023-09-14 - View hardcoded to DRC
                //string sql = $"SELECT DISTINCT Record1.Rec1ID,EZCAPPROV,EZCAPMEMB,SUM(CONVERT(FLOAT,Record2.BILLED)) AS NET, COUNT(*) AS CLAIMDETAILS,ISNULL(Record1.CREATEDATE,'') AS CREATEDATE ,ISNULL(BATCHNO,'') AS BATCHNO,Record1.CREATEBY  \n"
                //+ $",CLAIMNO FROM {PamcPortalDatabase}..Record1  Record1 \n"
                //+ "INNER JOIN\n"
                //+ $"{PamcPortalDatabase}..Record2 as Record2 ON Record2.Rec1ID = Record1.Rec1ID\n"
                //+ "INNER JOIN \n"
                //+ $"{PamcPortalDatabase}..Record0 Record0 ON Record0.FileID = Record1.FileID\n"
                //+ $"left outer join {DRCDatabase}..claim_details cd\n"
                //+ "on cd.MEMO7 = convert(varchar(20) ,Record1.Rec1ID) and cd.MEMO9 = convert(varchar(20) ,Record2.Rec2ID)\n"
                //+ $"WHERE Record1.CREATEBY IN (SELECT username  FROM {PamcPortalDatabase}..UserLobCodesHpCodes WHERE HPCODE IN ({whereclaus}))\n"
                //+ "GROUP BY EZCAPPROV,EZCAPMEMB,Record1.CREATEDATE,BATCHNO,Record1.CREATEBY,CLAIMNO,Record1.Rec1ID ";

                //cmd.CommandText = sql;
                string whereClause = "";

                if (data.userClaimsOnly)
                {
                    whereClause = $" WHERE Record1.CREATEBY = @username \n";
                }
                else
                {
                    whereClause = " WHERE Record1.CREATEBY IN ( \n" +
                                    "                               SELECT        u.username \n" +
                                   $"                               FROM          {PamcPortalDatabase}..Users AS u CROSS JOIN \n" +
                                   $"                                              {DRCDatabase}..HP_CONTRACTS AS hp \n" +
                                   $"                               WHERE hp.LOBCODE IN(SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) AND hp.HPCODE in ({hpcodesDelimted}) \n" +
                                    "                            ) \n";
                }

                // Jaco 2023-09-14
                cmd.CommandText = " SELECT DISTINCT   Record1.Rec1ID, \n" +
                                    "                   Record1.EZCAPPROV, \n" +
                                    "                   Record1.EZCAPMEMB, \n" +
                                    "                   SUM(CONVERT(DECIMAL(15,2),Record2.BILLED)) AS BILLED, \n" +
                                    "                   COUNT(*) AS CLAIMDETAILS, \n" +
                                    "                   ISNULL(Record1.CREATEDATE,'') AS CREATEDATE, \n" +
                                    "                   ISNULL(Record0.BATCHNO,'') AS BATCHNO, \n" +
                                    "                   Record1.CREATEBY, \n" +
                                    "                   cd.CLAIMNO \n" +
                                   $" FROM {PamcPortalDatabase}..Record1  Record1 " +
                                   $"       INNER JOIN {PamcPortalDatabase}..Record2 as Record2 ON Record2.Rec1ID = Record1.Rec1ID \n" +
                                   $"       INNER JOIN {PamcPortalDatabase}..Record0 Record0 ON Record0.FileID = Record1.FileID \n" +
                                   $"       left outer join {DRCDatabase}..claim_details cd on cd.MEMO7 = convert(varchar(20) ,Record1.Rec1ID) and cd.MEMO9 = convert(varchar(20) ,Record2.Rec2ID) \n" +
                                    whereClause +
                                    " AND convert(datetime, Record1.CREATEDATE) between @capturedFrom and @capturedTo \n" +
                                    " GROUP BY EZCAPPROV, EZCAPMEMB, Record1.CREATEDATE, BATCHNO, Record1.CREATEBY, CLAIMNO, Record1.Rec1ID  ";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    ClaimReport claim = new ClaimReport();

                    claim.batchNo = row["BATCHNO"].ToString();
                    claim.dateCreated = row["CREATEDATE"].ToString().Replace("-", "/");
                    claim.detailLines = row["CLAIMDETAILS"].ToString();
                    claim.memb = row["EZCAPMEMB"].ToString(); ;
                    claim.billed = row["BILLED"].ToString();
                    claim.provid = row["EZCAPPROV"].ToString();
                    claim.user = row["CREATEBY"].ToString();
                    claim.ezcapClaimNo = row["CLAIMNO"].ToString();

                    listToReturn.Add(claim);
                }

                cn.Close();
            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(e.Message, e.StackTrace);
                throw;
            }

            return listToReturn;
        }

        [HttpPost]
        [Route("ExportCapturedClaimsToExcel")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ExportCapturedClaimsToExcel([FromBody] ClaimReport[] capturedClaims)
        {
            ValidationResultsModel result = new ValidationResultsModel();

            try
            {
                DataTable dtCapturedClaims = new DataTable("Captured Claims");
                dtCapturedClaims.Columns.Add("CAPTURED DATE", typeof(DateTime));
                dtCapturedClaims.Columns.Add("CAPTURED BY", typeof(string));
                dtCapturedClaims.Columns.Add("PRACTICE NO", typeof(string));
                dtCapturedClaims.Columns.Add("MEMBER NO", typeof(string));
                dtCapturedClaims.Columns.Add("DETAIL LINES", typeof(int));
                dtCapturedClaims.Columns.Add("BILLED", typeof(decimal));
                dtCapturedClaims.Columns.Add("CLAIMNO", typeof(string));
                dtCapturedClaims.Columns.Add("BATCHNO", typeof(string));

                foreach (ClaimReport claim in capturedClaims)
                {
                    DataRow row = dtCapturedClaims.NewRow();
                    row["CAPTURED DATE"] = claim.dateCreated;
                    row["CAPTURED BY"] = claim.user;
                    row["PRACTICE NO"] = claim.provid;
                    row["MEMBER NO"] = claim.memb;
                    row["DETAIL LINES"] = claim.detailLines;
                    row["BILLED"] = claim.billed;
                    //row["BILLED"] = Convert.ToDecimal(claim.billed, new CultureInfo("en-US")); // if web server region settings decimal symbol is comma instead of fullstop 
                    row["CLAIMNO"] = claim.ezcapClaimNo;
                    row["BATCHNO"] = claim.batchNo;
                    dtCapturedClaims.Rows.Add(row);
                }

                List<DataTable> lstDt = new List<DataTable>();
                lstDt.Add(dtCapturedClaims);

                // Create assets path
                var root = _env.ContentRootPath;

                FileInfo fiExcel;

                if (Directory.Exists($"{root}\\ClientApp\\src\\assets\\CapturedClaimsReports")) // scr folder only exists in dev environment (and frontend uses the assets folder in src while in dev mode)
                {
                    fiExcel = new FileInfo($"{root}\\ClientApp\\src\\assets\\CapturedClaimsReports\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_CapturedClaims.xlsx");
                }
                else // only a dist folder exist in live enviroment (and the frontend uses the assets folder in dist while in live mode)
                {
                    fiExcel = new FileInfo($"{root}\\ClientApp\\dist\\assets\\CapturedClaimsReports\\{DateTime.Now.ToString("yyyyMMddHHmmssfff")}_CapturedClaims.xlsx");
                }

                try
                {
                    if (fiExcel.Exists)
                    {
                        fiExcel.Delete();
                    }
                }
                catch
                {
                    throw;
                }

                // Create and Write Excel file to assets
                bool freezeTopRow = true;
                bool wrapText = false;
                ExcelReport xrep = new ExcelReport(fiExcel, lstDt, freezeTopRow, wrapText);
                xrep.CreateReport();

                double minWidth = 0.00;
                double maxWidth = 60.00;
                xrep.SpecifyMinMaxWidth(fiExcel, minWidth, maxWidth);

                result.valid = true;
                result.message = fiExcel.Name;

            }
            catch (Exception e)
            {
                var msg = e.Message;
                LogError(e.Message, e.StackTrace);
                result.valid = false;
                result.message = e.Message;
            }

            return result;
        }

        [HttpPost]
        [Route("GetStalePeriod")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetStalePeriod([FromBody] LoginDetailsModel currentUser)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string hpcode = "";
            string[] hpcodes;
            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            try
            {
                cmd.Connection = cn;

                if (currentUser.userType == 1) // Provider
                {
                    // Hardcoded for BOMAID demo - must pass different model with HpCode and Option
                    cmd.CommandText = $"SELECT HPCODE, STLPERIOD FROM {DRCDatabase}..HP_CONTRACTS WHERE HPCODE = '' ";

                    using (SqlDataReader dread = cmd.ExecuteReader())
                    {
                        dtable.Load(dread);
                    }
                    vr.message = "";
                    foreach (DataRow rows in dtable.Rows)
                    {
                        vr.message += rows["HPCODE"].ToString() + ":" + rows["STLPERIOD"].ToString() + ":";

                    }
                    cn.Close();
                }
                else  // Should be only userType 3 - Client
                {
                    // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                    //cmd.CommandText = $"SELECT hpCode FROM " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes WHERE username = '{currentUser.username}' ";  

                    // Jaco 2023-09-11
                    cmd.CommandText = $" SELECT hp.hpCode " +      //u.username,u.lobcode,dbo.GetMemberPrefix(hp.HPCODE) AS memberPrefix, hp.HPCODE " +
                                      $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                      $"        {DRCDatabase}..HP_CONTRACTS AS hp " +
                                      $" where hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',') ) and u.username = '{currentUser.username}' ";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }
                    int count = 0;
                    hpcodes = new string[dt.Rows.Count];
                    foreach (DataRow row in dt.Rows)
                    {
                        if (dt.Rows.Count > 1)
                        {

                            hpcodes[count] = row["hpCode"].ToString();
                            count++;


                        }
                        else
                        {
                            hpcode = row["hpCode"].ToString();
                        }

                    }

                    if (hpcode != "")
                    {
                        cmd.CommandText = $"SELECT HPCODE, STLPERIOD FROM {DRCDatabase}..HP_CONTRACTS WHERE HPCODE = '{hpcode}' ";
                    }
                    else
                    {
                        string where = "";

                        for (int i = 0; i < hpcodes.Length; i++)
                        {
                            if (i == 0)
                            {
                                where = $"HPCODE = '{hpcodes[i]}'";
                            }
                            else
                            {
                                where = where + $" OR HPCODE = '{hpcodes[i]}'";
                            }
                        }

                        cmd.CommandText = $"SELECT HPCODE,STLPERIOD FROM {DRCDatabase}..HP_CONTRACTS WHERE {where} ";
                    }



                    using (SqlDataReader dread = cmd.ExecuteReader())
                    {
                        dtable.Load(dread);
                    }
                    vr.message = "";
                    foreach (DataRow rows in dtable.Rows)
                    {
                        vr.message += rows["HPCODE"].ToString() + ":" + rows["STLPERIOD"].ToString() + ":";

                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            return vr;
        }

        [HttpPost]
        [Route("ValidateProccode")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateProccode([FromBody] RecordType2 claimDetails)
        {
            string name;
            ValidationResultsModel result = new ValidationResultsModel();
            try
            {
                string sqlStatement = $"SELECT * FROM  SERVICE_CODES WHERE SVCCODE ='{claimDetails.PROCCODE}'";
                bool dbsValidData = ExecuteSqlRecordType2(claimDetails, sqlStatement);
                if (dbsValidData == true)
                {
                    result.valid = true;
                    sqlStatement = $"SELECT SVCDESC FROM  SERVICE_CODES WHERE SVCCODE ='{claimDetails.PROCCODE}'";
                    name = ReturnValidDataRecordType2(claimDetails, sqlStatement);
                    result.message = $"{name}";
                    return result;
                }
                else
                {
                    result.valid = false;
                    result.message = "";
                    return result;
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("ValidateDiagref")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateDiagref([FromBody] RecordType5 claimDetails)
        {
            ValidationResultsModel result = new ValidationResultsModel();
            string name;
            string sqlStatement = $"SELECT * FROM DIAG_CODES WHERE DIAGCODE = '{ claimDetails.DIAG}'";

            try
            {
                bool dbsValidData = ExecuteSqlForDiagCode(claimDetails, sqlStatement);

                if (dbsValidData == true)
                {
                    result.valid = true;
                    sqlStatement = $"SELECT DIAGDESC FROM DIAG_CODES WHERE DIAGCODE = '{ claimDetails.DIAG}'";
                    name = GetDiagDescription(claimDetails, sqlStatement);
                    result.message = $"{name}";
                    return result;
                }
                else
                {
                    result.valid = false;
                    result.message = "Invalid Diagnostic code";
                    return result;
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("GetPhCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ServicetypesModel> GetPhCodes([FromBody] RecordType2 claimDetails)
        {
            DataTable dt = new DataTable();
            List<ServicetypesModel> listOfPhCodes = new List<ServicetypesModel>();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;

            using (SqlConnection Sqlcon = new SqlConnection(cn.ConnectionString))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();
                }
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = $"SELECT [PHCODE],[DESCR],[TYPE] FROM SERVICE_TYPES WHERE ([HPCODE] = '{claimDetails.hpCode}' AND [SHOW] = 'true')";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        dt.Load(cmd.ExecuteReader());
                        foreach (DataRow dr in dt.Rows)
                        {
                            ServicetypesModel phCode = new ServicetypesModel();
                            phCode.phCode = dr["PHCODE"].ToString().ToUpper();
                            phCode.phDesc = dr["DESCR"].ToString().ToUpper();
                            phCode.phOpt = dr["TYPE"].ToString().ToUpper();
                            listOfPhCodes.Add(phCode);
                        }
                    }
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }

                Sqlcon.Close();
            }
            return listOfPhCodes;
        }

        [HttpPost]
        [Route("ValidateModCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateModCodes([FromBody] ModifierModel modCodes)
        {
            ValidationResultsModel result = new ValidationResultsModel();
            string name;
            string sqlStatement = $"SELECT * FROM MODIF_CODES WHERE MODIF = '{ modCodes.modCode}'";
            try
            {

                bool dbsValidData = ExecuteSqlModifiers(modCodes, sqlStatement);

                if (dbsValidData == true)
                {
                    result.valid = true;
                    sqlStatement = $"SELECT MODDESC FROM MODIF_CODES WHERE MODIF = '{ modCodes.modCode}'";
                    name = ReturnValidDataModifiers(modCodes, sqlStatement);
                    result.message = $"{name}";
                    return result;
                }
                else
                {
                    result.valid = false;
                    result.message = modCodes.modCode + " is valid";
                    return result;
                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("ValidateToothNum")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ValidateToothNum([FromBody] ToothNumbersModel toothNum)
        {
            ValidationResultsModel result = new ValidationResultsModel();
            string name;
            string sqlStatement = $"SELECT * FROM CLINICAL_CODES WHERE CLINICALCODE = '{ toothNum.toothNum}'";

            try
            {
                bool dbsValidData = ExecuteSqlToothNumber(toothNum, sqlStatement);

                if (dbsValidData == true)
                {
                    result.valid = true;
                    sqlStatement = $"SELECT CLINICALDESC FROM CLINICAL_CODES WHERE CLINICALCODE = '{ toothNum.toothNum}'";
                    name = ReturnValidDataToothNumber(toothNum, sqlStatement);
                    result.message = $"{name}";
                    return result;
                }
                else
                {
                    result.valid = false;
                    result.message = toothNum.toothNum + " is not valid.";
                    return result;
                }
            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }
            return result;
        }

        [HttpPost]
        [Route("UpdateBankingDetails")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateBankingDetails([FromBody] BankingDetailModel details)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable tbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            string subject = "";
            string body = "";
            string to = "";
            string memb = "";
            try
            {
                cn.ConnectionString = _drcConnectionString;

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                if (details.claimType.ToUpper() == "M")
                {
                    // Jaco comment out 2023-09-28 - Databases is hardcoded in GetMemberPrefix function
                    //cmd.CommandText = $"DECLARE @membID AS VARCHAR(MAX) \r\n SELECT @membID = ({PamcPortalDatabase}.dbo.GenerateSubSsn({PamcPortalDatabase}.dbo.GetMemberPrefix('{details.hpcode}'), {PamcPortalDatabase}.dbo.RemoveDependantDash('{details.mainPolicyNo}')))\r\n" +
                    //                  $"EXECUTE {DRCDatabase}.dbo.CreateVendorBankHist @membID,'{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{DateTime.Now.ToString("yyyy/MM/dd")}',NULL,999";

                    // Jaco 2023-09-28
                    cmd.CommandText = $" DECLARE @membID AS VARCHAR(40) \n " +
                                        $" DECLARE @prefix AS VARCHAR(10) \n " +
                                        $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                        $"   FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                        $"        {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                        $"   where OxygenBenefitOptions.ezHpCode = '{details.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross' \n" +
                                        $" SET @membID = '{details.mainPolicyNo}' \n" +
                                        //$" SET @membID = {PamcPortalDatabase}.dbo.RemoveDependantDash(@membID) \n" + // Jaco 2024-07-01 - dont pass dependant code aymore
                                        $" SET @membID = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,@membID) \n" +
                                        $" EXECUTE {DRCDatabase}.dbo.CreateVendorBankHist @membID,'{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{DateTime.Now.ToString("yyyy/MM/dd")}',NULL,999";
                    cmd.ExecuteNonQuery();

                    // Jaco comment out 2023-09-28 - Databases is hardcoded in GetMemberPrefix function
                    //cmd.CommandText = $"DECLARE @membID AS VARCHAR(MAX) \r\n SELECT @membID = ({PamcPortalDatabase}.dbo.GenerateSubSsn({PamcPortalDatabase}.dbo.GetMemberPrefix('{details.hpcode}'), {PamcPortalDatabase}.dbo.RemoveDependantDash('{details.mainPolicyNo}')))\r\n" +
                    //    $"INSERT INTO {PamcPortalDatabase}..BankingDetailsLog (VendorId,BankName,BranchCode,AccNo,AccType,ChangedBy,ChangedDate,Completed) " +
                    //    $"VALUES(@membID,'{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{details.user}','{DateTime.Now.ToString("yyyy/MM/dd")}',1)";

                    // Jaco 2023-09-28
                    cmd.CommandText = $" DECLARE @membID AS VARCHAR(40) \n " +
                                        $" DECLARE @prefix AS VARCHAR(10) \n " +
                                        $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                        $"   FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                        $"        {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                        $"   where OxygenBenefitOptions.ezHpCode = '{details.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross' \n" +
                                        $" SET @membID = '{details.mainPolicyNo}' \n" +
                                        //$" SET @membID = {PamcPortalDatabase}.dbo.RemoveDependantDash(@membID) \n" + // Jaco 2024-07-01 - dont pass dependant code aymore
                                        $" SET @membID = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,@membID) \n" +
                                        $" INSERT INTO {PamcPortalDatabase}..BankingDetailsLog (VendorId,BankName,BranchCode,AccNo,AccType,ChangedBy,ChangedDate,Completed) " +
                                        $" VALUES(@membID,'{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{details.user}','{DateTime.Now.ToString("yyyy/MM/dd")}',1)";

                    cmd.ExecuteNonQuery();
                    vr.valid = true;
                }
                if (details.claimType.ToUpper() == "P")
                {
                    cmd.CommandText = $"\r\n" +
                        $"INSERT INTO {PamcPortalDatabase}..BankingDetailsLog (VendorId,BankName,BranchCode,AccNo,AccType,ChangedBy,ChangedDate,Completed) " +
                        $"VALUES('{details.provId}','{details.bankName}','{details.branchCode}','{details.accountNo}','{details.accType}','{details.user}','{DateTime.Now.ToString("yyyy/MM/dd")}',0)";
                    cmd.ExecuteNonQuery();
                    vr.valid = true;
                }

                cmd.CommandText = $"SELECT CheckrunEmail FROM {PamcPortalDatabase}.dbo.HealthPlanSetup WHERE HPCode = '{details.hpcode}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    to = row["CheckrunEmail"].ToString();
                }

                if (details.claimType.ToUpper() == "P")
                {
                    to += ";Providerbd@dentalrisk.com"; // Jaco - Hardcode because we need a new column 2024-08-08
                }

                // Jaco comment out 2023-09-28 - Databases is hardcoded in GetMemberPrefix function
                //cmd.CommandText = $"DECLARE @membID AS VARCHAR(MAX) \r\n SELECT @membID = ({PamcPortalDatabase}.dbo.GenerateSubSsn({PamcPortalDatabase}.dbo.GetMemberPrefix('{details.hpcode}'), {PamcPortalDatabase}.dbo.RemoveDependantDash('{details.mainPolicyNo}'))) \r\n SELECT @membID AS MEMB";

                // Jaco 2023-09-28
                cmd.CommandText = $" DECLARE @membID AS VARCHAR(40) \n " +
                                    $" DECLARE @prefix AS VARCHAR(10) \n " +
                                    $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n" +
                                    $"   FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                                    $"        {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                                    $"   where OxygenBenefitOptions.ezHpCode = '{details.hpcode}' AND OxygenBenefitOptions.scheme <> 'carecross' \n" +
                                    $" SET @membID = '{details.mainPolicyNo}' \n" +
                                    //$" SET @membID = {PamcPortalDatabase}.dbo.RemoveDependantDash(@membID) \n" + // Jaco 2024-07-01 - dont pass dependant code aymore
                                    $" SET @membID = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,@membID) \n" +
                                    $" SELECT @membID AS MEMB ";

                using (SqlDataReader rr = cmd.ExecuteReader())
                {
                    tbl.Load(rr);
                }

                foreach (DataRow ro in tbl.Rows)
                {
                    memb = ro["MEMB"].ToString();
                }
                subject = "Banking Details Update";

                if (details.claimType.ToUpper() == "M")
                {
                    body = $"Good day \r\n\n The following banking details was updated via Claim Capturing on the PAMC website.\r\n\n USER: {details.user}\r\n MEMBERID:{memb}\r\n BANK NAME:{details.bankName}\r\n" +
                                       $"BRANCH CODE: {details.branchCode}\r\n ACCNO: {details.accountNo} \r\n ACC TYPE: {details.accType} \r\n\n Regards";

                }
                if (details.claimType.ToUpper() == "P")
                {
                    body = $"Good day \r\n\n The following banking details was captured via Claim Capturing on the PAMC website and needs to be updated.\r\n\n USER: {details.user}\r\n PROVIDER ID:{details.provId}\r\n BANK NAME:{details.bankName}\r\n" +
                                       $"BRANCH CODE: {details.branchCode}\r\n ACCNO: {details.accountNo} \r\n ACC TYPE: {details.accType} \r\n\n Regards";

                }

                string url = _EmailWebSvcConnection;

                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;

                EmailService.EmailServiceSoapClient client = new EmailService.EmailServiceSoapClient(binding, address);

                //var q = client.SendEmailAsync(body, subject, to);
                //q.Wait();

                if (_isTest)
                {
                    var q = client.SendEmailAsync(body, "PORTAL TEST: " + subject, _testEmails);
                    q.Wait();
                }
                else
                {
                    var q = client.SendEmailAsync(body, subject, to);
                    q.Wait();
                }

                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                //var msg = e.Message;
                vr.valid = false;
                vr.message = e.Message;
            }

            return vr;
        }

        [HttpGet]
        [Route("SaveRecords")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public string SaveRecords()
        {
            SqlCommand cmd = new SqlCommand();
            string sqlSelectRec0;
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            RecordType0 rec0 = new RecordType0();
            DataTable dTable = new DataTable();
            string log = "";
            #region Dummie Data For Record0
            rec0.TYPE = 0;
            rec0.CONTROL = "BATCH";
            rec0.FILLER1 = "";
            rec0.FILLER2 = "";
            rec0.FILLER3 = "";
            rec0.FILLER4 = "";
            rec0.PROVIDER = "";
            rec0.CLRHOUSE = rec0.PROVIDER;
            rec0.DATECREATED = DateTime.Today.ToShortDateString();
            #endregion
            try
            {
                try
                {
                    log += $"HEADER SAVE STARTED----{DateTime.Now.ToString("yyyy/MM/dd")}-------------- \n\n";
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    cmd.Connection = cn;
                    cmd.CommandText = $"INSERT INTO Record0 (TYPE,CONTROL, FILLER1, PROVIDER, FILLER2, CLRHOUSE, FILLER3, DATECREATED, FILEER4)" +
                        $"VALUES ('{rec0.TYPE}', '{rec0.CONTROL}', '{rec0.FILLER1}', '{rec0.PROVIDER}', '{rec0.FILLER2}', '{rec0.CLRHOUSE}', '{rec0.FILLER3}', '{rec0.DATECREATED}', '{rec0.FILLER4}')";

                    log += $"HEADER SAVE SQL----{cmd.CommandText}-------------- \n\n";
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    log += $"HEADER SAVE ERROR----{e.Message}-------- \n\n -----STACK TRACE ----- {e.StackTrace}------ \n\n";
                    var msg = e.Message;
                }

                sqlSelectRec0 = $"SELECT FileID FROM Record0 WHERE CONVERT(VARCHAR, PROVIDER) = '{rec0.PROVIDER}'";
                cmd.CommandText = sqlSelectRec0;

                log += $"HEADER SAVE FILEID SQL----{cmd.CommandText}------ \n\n";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        fileIDRec0 = dTable.Rows[dTable.Rows.Count - 1][0].ToString();
                        log += $"HEADER SAVE FILEID ----{fileIDRec0}------ \n\n";
                        return fileIDRec0;
                    }
                    else
                    {
                        cn.Close();
                        dTable.Reset();
                        fileIDRec0 = "";

                        return "unsucceffull";
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                log += $"HEADER SAVE GENERAL ERROR ----{ex.Message}------ \n\n ------- STACK TRACE ----- {ex.StackTrace} ---- \n\n";
                throw;
            }
            finally
            {
                log += $"HEADER SAVE END ---- \n\n";


            }
        }

        [HttpPost]
        [Route("SaveFinalHeader")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveFinalHeader([FromBody] RecordType1 finalClaimHeader)
        {
            SqlCommand cmd = new SqlCommand();
            ValidationResultsModel vr = new ValidationResultsModel();
            //Prepare Connection 
            string log = $"{DateTime.Now.ToShortDateString()}---------HEADER INSERT BEGIN------------------------\r\n\n";
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            string sqlSelectRec1;
            DataTable dTable = new DataTable();
            #region RecordType1
            try
            {
                log += $"{DateTime.Now.ToShortDateString()}---------Connection string {_portalConnectionString}------------------------\r\n\n";
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                var defaultDate = Convert.ToDateTime(finalClaimHeader.DATERECD);
                string recDate = defaultDate.ToString("yyyy/MM/dd");
                log += $"{DateTime.Now.ToShortDateString()}---------Connection string {recDate}------------------------\r\n\n";
                finalClaimHeader.EXT_SWITCH = "EZWEB";

                cmd.Connection = cn;
                log += $"{DateTime.Now.ToShortDateString()}---------Date string {defaultDate}------------------------\r\n\n";

                cmd.CommandText = $"INSERT INTO Record1 (TYPE, PHCODE, PROVCLAIM, EZCAPUATH, EZCAPPROV, EZCAPMEMB, PLACE, OUTCOME, EDIREF, CASENUM, UNITS, CLAIMTYPE, REFPROVID," +
                    $" VENDORID,DATERECD, BATCH_NO, [EXT SWITCH], FileID,CREATEBY,CREATEDATE) VALUES " +
                    $"('{finalClaimHeader.TYPE}', '{finalClaimHeader.PHCODE}', '{finalClaimHeader.PROVCLAIM}', '{finalClaimHeader.EZCAPAUTH}', '{finalClaimHeader.EZCAPPROV}'," +
                    $" '{finalClaimHeader.EZCAPMEMB}', '{finalClaimHeader.PLACE}', '', '', '', '', '{finalClaimHeader.CLAIMTYPE}', " +
                    $"'{finalClaimHeader.REFPROVID}','{finalClaimHeader.VENDORID}' ,'{recDate}', '', '{ finalClaimHeader.EXT_SWITCH}', '{finalClaimHeader.Rec0ID}', '{finalClaimHeader.CREATEBY}','{DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ")}')";
                log += $"{DateTime.Now.ToShortDateString()}---------sql string {cmd.CommandText}------------------------\r\n\n";
                cmd.ExecuteNonQuery();

                sqlSelectRec1 = $"SELECT Rec1ID FROM Record1 WHERE CONVERT(VARCHAR, EZCAPPROV) = '{finalClaimHeader.EZCAPPROV}'";

                log += $"{DateTime.Now.ToShortDateString()}---------sql string {sqlSelectRec1}------------------------\r\n\n";
                cmd.CommandText = sqlSelectRec1;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        fileIDRec1 = dTable.Rows[dTable.Rows.Count - 1][0].ToString();
                        vr.message = fileIDRec1;
                        log += $"{DateTime.Now.ToShortDateString()}---------sql recid1 {fileIDRec1}------------------------\r\n\n";
                    }
                    else
                    {
                        vr.message = "error";
                        log += $"{DateTime.Now.ToShortDateString()}---------sql error {fileIDRec1}------------------------\r\n\n";
                    }
                    cn.Close();
                    dTable.Reset();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
                log += $"{DateTime.Now.ToShortDateString()}--------- error {e.Message}------------------------";
                log += $"{DateTime.Now.ToShortDateString()}--------- error {e.StackTrace}------------------------";

            }
            finally
            {
                finalClaimHeader.Rec1ID = Convert.ToInt32(vr.message);
                JsonResult js = new JsonResult(finalClaimHeader);

                string fileName = "";
                var root = _env.WebRootPath;
                string folderName = DateTime.Now.ToString("yyyyMMdd");
                DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\Header\\{folderName}");

                if (di.Exists == false)
                {
                    di.Create();
                }

                int countFiles = di.GetFiles().Length + 1;

                fileName = $"Header_{countFiles}_{finalClaimHeader.EZCAPMEMB}_{vr.message}.txt";
                FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\Header\\{folderName}\\{ fileName }");

                // Create a new file   

                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);
                    fs.Write(c);
                }

            }
            #endregion
            return vr;
        }

        [HttpPost]
        [Route("SaveFinalDetail")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<RecordType2> SaveFinalDetail([FromBody] RecordType2[] detailToSave)
        {
            List<RecordType2> updatedDetails = new List<RecordType2>();
            SqlCommand cmd = new SqlCommand();
            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            DataTable dTable = new DataTable();

            string log = "";
            try
            {
                log += $"SAVE DETAIL BEGIN --------  {DateTime.Now.ToString("yyyy/MM/dd")}------- \n\n";

                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                foreach (var detailLine in detailToSave)
                {
                    var lineID = detailLine.Rec1ID;
                    var fromDate = Convert.ToDateTime(detailLine.FROMDATE).ToString("yyyy/MM/dd");
                    var toDate = Convert.ToDateTime(detailLine.TODATE).ToString("yyyy/MM/dd");
                    //detailLine.MODCODE2 = "myMod";
                    cmd.CommandText = $"INSERT INTO Record2 (TYPE, PHCODE, FROMDATE, TODATE, PROCCODE, CURR_MODCODE, CURR_MODAMOUNT, MODCODE, MODAMOUNT," +
                                   $"MODCODE2, MODAMOUNT2, MODCODE3, MODAMOUNT3, MODCODE4, MODAMOUNT4, QTY, BILLED, DIAGREF, BUNDLER, BUNDLERTYP, COPAY, MANDISCOUNT, TRANSACT_NO, PROCCODE_DESC, " +
                                   $"CLINICALCODE1, CLINICALCODE2, CLINICALCODE3, CLINICALCODE4, CLINICALCODE5, CLINICALCODE6, CLINICALCODE7, CLINICALCODE8, " +
                                   $"MEMO1, MEMO2, MEMO3, MEMO4, MEMO5, MEMO6, MEMO7, MEMO8, MEMO9,MEMO10, Rec1ID) " +
                                   $"OUTPUT	INSERTED.Rec2ID\r\n" +
                                   $"VALUES ('{detailLine.TYPE}', '{detailLine.PHCODE}',CAST('{fromDate}' AS date), CAST('{toDate}' AS date) , '{detailLine.PROCCODE}'" +
                                   $", '', '', '{detailLine.MODCODE}', '', '{detailLine.MODCODE2}', '', '{detailLine.MODCODE3}', '', '{detailLine.MODCODE4}', '', '{detailLine.QTY}', '{detailLine.BILLED}', '{detailLine.DIAGREF}', '', '', '', '', ''," +
                                   $" '{detailLine.PROCCODE_DESC}', '{detailLine.CLINICALCODE1}', '{detailLine.CLINICALCODE2}', '{detailLine.CLINICALCODE3}', '{detailLine.CLINICALCODE4}', '{detailLine.CLINICALCODE5}', '{detailLine.CLINICALCODE6}', '{detailLine.CLINICALCODE7}', '{detailLine.CLINICALCODE8}'" +
                                   $", '', '', '', '', '', '', '', '', '', '', '{lineID}')";
                    detailLine.Rec2IDDb = (int)cmd.ExecuteScalar();

                    log += $"SAVE DETAIL SQL --------  {cmd.CommandText}------- \n\n";

                    updatedDetails.Add(detailLine);
                    log += $"SAVE DETAIL SQL --------  {detailLine.Rec2IDDb}------- \n\n";

                }

                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"SAVE DETAIL ERROR --------  {e.Message}------- \n\n ------- {e.StackTrace}----\n\n";

                var msg = e.Message;
            }
            finally
            {
                JsonResult js = new JsonResult(detailToSave);

                string fileName = "";
                var root = _env.WebRootPath;
                string folderName = DateTime.Now.ToString("yyyyMMdd");
                DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\Details\\{folderName}");

                if (di.Exists == false)
                {
                    di.Create();
                }

                int countFiles = di.GetFiles().Length + 1;

                fileName = $"DETAIL_{countFiles}_{detailToSave[0].Rec1ID}.txt";
                FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\Details\\{folderName}\\{ fileName }");

                // Create a new file   

                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);
                    fs.Write(c);
                }
                log += $"SAVE DETAIL END -------- \n\n";
            }
            return updatedDetails;
        }

        [HttpPost]
        [Route("SaveFinalDiagCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveFinalDiagCodes([FromBody] RecordType5[] DiagCodesToSave)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            SqlCommand cmd = new SqlCommand();
            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            DataTable dTable = new DataTable();
            string log = "";
            try
            {
                log += $"DIAGCODES SAVE STARTED  --------{DateTime.Now.ToString("yyyy/MM/dd")}------\n\n";
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                foreach (var line in DiagCodesToSave)
                {
                    var lineID = line.FieldID;
                    cmd.CommandText = $"INSERT INTO Record5 (TYPE, DIAGREF, DIAG, Rec1ID) VALUES ('{line.TYPE}', '{line.DIAGREF}', '{line.DIAG}', '{lineID}')";
                    log += $"DIAGCODES SAVE SQL  --------{cmd.CommandText}------\n\n";
                    cmd.ExecuteNonQuery();
                }
                vr.message = "success";
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"DIAGCODES SAVE ERROR  --------{e.Message}---- \n\n -----STACK TRACE ---  {e.StackTrace}----------\n\n";
                var msg = e.Message;
            }
            finally
            {
                JsonResult js = new JsonResult(DiagCodesToSave);

                string fileName = "";
                var root = _env.WebRootPath;
                string folderName = DateTime.Now.ToString("yyyyMMdd");
                DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\Diags\\{folderName}");

                if (di.Exists == false)
                {
                    di.Create();
                }

                int countFiles = di.GetFiles().Length + 1;

                fileName = $"DETAIL_{countFiles}_{DiagCodesToSave[0].FieldID}.txt";
                FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\Diags\\{folderName}\\{ fileName }");

                // Create a new file   

                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);
                    fs.Write(c);
                }
                log += $"DIAGCODES SAVE END----------\n\n";
            }
            return vr;
        }

        [HttpPost]
        [Route("SaveFinalRejectCodes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveFinalRejectCodes([FromBody] RecordType8[] RejectCodesToSave)
        {

            ValidationResultsModel vr = new ValidationResultsModel();
            SqlCommand cmd = new SqlCommand();
            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            DataTable dTable = new DataTable();
            string log = "";
            try
            {
                log += $"SAVE REJECTION STARTED --------{DateTime.Now.ToString("yyyy/MM/dd")}--------- \n\n";
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                foreach (var line in RejectCodesToSave)
                {
                    var lineID = line.Rec2IDdb;
                    log += $"SAVE REJECTION LINEID REC2--------{line.Rec2IDdb}--------- \n\n";
                    cmd.CommandText = $"INSERT INTO Record8 (TYPE, LINE, ADJCODE, ADJUST, COMMENT, Rec2ID) VALUES ('8', '{line.LINE}', '{line.ADJCODE}', '{line.ADJUST}', '{line.COMMENT}', '{lineID}')";
                    log += $"SAVE REJECTION SQL--------{cmd.CommandText}--------- \n\n";
                    cmd.ExecuteNonQuery();
                }
                vr.message = "success";
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"SAVE REJECTION ERROR--------{e.Message}------- \n\n ----- STACKTRACE ---{e.StackTrace}---- \n\n";
                var msg = e.Message;
            }
            finally
            {
                if (RejectCodesToSave.Length > 0)
                {
                    JsonResult js = new JsonResult(RejectCodesToSave);

                    string fileName = "";
                    var root = _env.WebRootPath;
                    string folderName = DateTime.Now.ToString("yyyyMMdd");
                    DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\Rejections\\{folderName}");

                    if (di.Exists == false)
                    {
                        di.Create();
                    }

                    int countFiles = di.GetFiles().Length + 1;

                    fileName = $"DETAIL_{countFiles}_{RejectCodesToSave[0].Rec2IDdb}.txt";
                    FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\Rejections\\{folderName}\\{ fileName }");

                    // Create a new file   

                    using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                    {
                        var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);
                        fs.Write(c);
                    }
                }
                log += $"SAVE REJECTION END---- \n\n";
            }
            return vr;
        }

        [HttpPost]
        [Route("UpdateHeader")]
        [Authorize(Roles = "NormalUser,Administrator")]
        public ValidationResultsModel UpdateHeader([FromBody] RecordType1 data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"UPDATE {PamcPortalDatabase}.dbo.Record0 SET COMPLETED = '1' WHERE FileID = '{data.Rec0ID}'";

                    cmd.ExecuteNonQuery();
                    vr.valid = true;
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("SaveFinalNotes")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel SaveFinalNotes([FromBody] RecordType4[] RejectCodesToSave)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            SqlCommand cmd = new SqlCommand();
            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _portalConnectionString;
            DataTable dTable = new DataTable();
            string log = "";
            try
            {
                log += $" NOTES SAVE STARTED ----------- {DateTime.Now.ToString("yyyy/MM/dd")}-------- \n\n";
                if (cn.State != System.Data.ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                foreach (var line in RejectCodesToSave)
                {
                    var lineID = line.Rec2ID;
                    log += $" NOTES SAVE LINEID REC2 ----------- {line.Rec2ID}-------- \n\n";
                    cmd.CommandText = $"INSERT INTO Record4 (TYPE, LINE, NOTE, Rec2ID) VALUES ('4', '{line.LINE}', '{line.NOTE}', '{lineID}')";
                    log += $" NOTES SAVE SQL ----------- {cmd.CommandText}-------- \n\n";
                    cmd.ExecuteNonQuery();
                }
                vr.message = "success";
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            finally
            {
                if (RejectCodesToSave.Length > 0)
                {
                    JsonResult js = new JsonResult(RejectCodesToSave);

                    string fileName = "";
                    var root = _env.WebRootPath;
                    string folderName = DateTime.Now.ToString("yyyyMMdd");
                    DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\Notes\\{folderName}");

                    if (di.Exists == false)
                    {
                        di.Create();
                    }

                    int countFiles = di.GetFiles().Length + 1;

                    fileName = $"NOTE_{countFiles}_{RejectCodesToSave[0].Rec2ID}.txt";
                    FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\Notes\\{folderName}\\{ fileName }");

                    // Create a new file   

                    using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                    {
                        var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);
                        fs.Write(c);
                    }
                }
                log += $" NOTES SAVE END ----------- \n\n";
            }
            return vr;
        }

        [HttpPost]
        [Route("DoAuditOnClaimsSaved")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DoAuditOnClaimsSaved([FromBody] RecordType2[] data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            vr.message = "";
            DataTable dt = new DataTable();
            DataTable dtabel = new DataTable();
            DataTable dtble = new DataTable();
            DataTable datatb = new DataTable();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            string log = "";

            try
            {
                int reciId = data[0].Rec1ID;
                log += $"AUDIT CLAIM COUNT STARTED -------------{DateTime.Now.ToString("yyyyMMdd")}------------ \n\n";

                cn.ConnectionString = _portalConnectionString;

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT COUNT(*) AS c FROM RECORD1 WHERE Rec1ID = {reciId}";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    int count = Convert.ToInt32(row["C"].ToString());
                    if (count < 1)
                    {
                        vr.valid = false;
                        vr.message = "Claim Saved Unsuccessfull";
                    }
                    else
                    {
                        vr.message += $"{count} Claim with \n";
                        vr.valid = true;



                    }
                }
                if (vr.valid)
                {
                    cmd.CommandText = $"SELECT COUNT(*) AS c FROM RECORD2 WHERE Rec1ID = {reciId}";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtabel.Load(dr);
                    }
                    foreach (DataRow row in dtabel.Rows)
                    {
                        int count = Convert.ToInt32(row["C"].ToString());
                        if (count < 1)
                        {
                            vr.valid = false;
                            vr.message = "Claim Saved Unsuccessfull";
                        }
                        else
                        {
                            vr.message += $"{count} detail line/s in total and \n";
                            vr.valid = true;
                        }
                    }
                }

                if (vr.valid)
                {
                    cmd.CommandText = $"SELECT COUNT(*) AS c FROM RECORD5 WHERE Rec1ID = {reciId}";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dtble.Load(dr);
                    }
                    foreach (DataRow row in dtble.Rows)
                    {
                        int count = Convert.ToInt32(row["C"].ToString());
                        if (count < 1)
                        {
                            vr.valid = false;
                            vr.message = "Claim Saved Unsuccessfull";
                        }
                        else
                        {
                            vr.message += $"{count} diagnostic code/s saved \n";
                            vr.valid = true;
                        }
                    }
                }
                if (vr.valid)
                {
                    cmd.CommandText = $"SELECT SUM(CONVERT(DECIMAL(8,2),BILLED)) AS c FROM RECORD2 WHERE Rec1ID = {reciId}";
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        datatb.Load(dr);
                    }
                    foreach (DataRow row in datatb.Rows)
                    {
                        decimal count = Convert.ToDecimal(row["C"].ToString());

                        vr.message += $" with a total billed amount of R {count} \n";
                        vr.valid = true;

                    }
                    cmd.CommandText = $"SELECT FileId FROM RECORD1 WHERE Rec1ID = {reciId}";
                    dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());
                    string fileID = "";
                    foreach (DataRow row in dt.Rows)
                    {
                        fileID = row["FileId"].ToString();
                    }

                    cmd.CommandText = $"UPDATE RECORD0 SET COMPLETED = 1 WHERE FileId = '{fileID}'";
                    cmd.ExecuteNonQuery();
                }



                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                log += $"------CLAIM AUDIT COUNT GENERAL ERROR ------- {e.Message}------- \n\n ----------- STACK TRACE ------ {e.StackTrace} --- \n\n";
                throw;
            }
            finally
            {
                log += $"------CLAIM AUDIT COUNT MESSAGE {vr.message} -------------------";
                log += $"------CLAIM AUDIT COUNT COMPLETE -------------------";
            }



            return vr;
        }

        [HttpPost]
        [Route("LogObject")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel LogObject([FromBody] objectlog objects)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                JsonResult js = new JsonResult(objects);

                string fileName = "";
                var root = _env.WebRootPath;
                string folderName = DateTime.Now.ToString("yyyyMMdd");
                DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\{folderName}");

                if (di.Exists == false)
                {
                    di.Create();
                }

                int countFiles = di.GetFiles().Length + 1;

                fileName = $"FILE_{countFiles}_{objects.header.EZCAPMEMB}_{objects.header.EZCAPPROV}_{objects.detail.Length}.json";
                FileInfo fi = new FileInfo($"{root}\\tmp\\LogObjects\\{folderName}\\{ fileName }");

                // Create a new file   

                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    var c = Newtonsoft.Json.JsonConvert.SerializeObject(js, Formatting.Indented);

                    fs.Write(c);
                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);

                //throw;
            }
            return vr;
        }


        [HttpPost]
        [Route("FindClaims")]
        [Authorize(Roles = "Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<objectlog> FindClaims([FromBody] files date)
        {
            List<objectlog> logs = new List<objectlog>();

            try
            {
                // string fileName = "";
                var root = _env.WebRootPath;
                string folderName = Convert.ToDateTime(date.date).ToString("yyyyMMdd");
                DirectoryInfo di = new DirectoryInfo($"{root}\\tmp\\LogObjects\\{folderName}");
                if (di.Exists)
                {
                    var allFiles = di.GetFiles();
                    foreach (FileInfo item in allFiles)
                    {
                        using (StreamReader fs = new StreamReader(item.FullName))
                        {
                            string replace = "{\r\n" +
                                             "  \"ContentType\"" + ": null,\r\n" +
                                             "  \"SerializerSettings\"" + ": null,\r\n" +
                                             "  \"StatusCode\"" + ": null,\r\n" +
                                             "  \"Value\"" + ":";
                            string c = fs.ReadToEnd();
                            c = c.Replace(replace, "");
                            int last = c.Length - 1;
                            c = c.Remove(last);
                            var d = Newtonsoft.Json.JsonConvert.DeserializeObject<objectlog>(c);
                            logs.Add(d);
                        }
                    }
                }


            }
            catch (Exception e)
            {

                LogError(e.Message, e.StackTrace);
            }

            return logs;
        }

        private static bool ExecuteSql(RecordType1 dataToVal, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
            }
            return dbsValidData;
        }

        private static bool ExecuteSqlTest(TestClaimHeaderModel dataToVal, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
            }
            return dbsValidData;
        }

        private static bool ExecuteSqlRecordType2(RecordType2 claimDetails, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
                //errorMessage = "EZCAPAUTH is incorrect, ";
            }
            return dbsValidData;
        }

        private static bool ExecuteSqlModifiers(ModifierModel claimDetails, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
                //errorMessage = "EZCAPAUTH is incorrect, ";
            }
            return dbsValidData;
        }

        private static bool ExecuteSqlToothNumber(ToothNumbersModel claimDetails, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
                //errorMessage = "EZCAPAUTH is incorrect, ";
            }
            return dbsValidData;
        }

        private static bool ExecuteSqlForDiagCode(RecordType5 claimDetails, string sqlStatement)
        {
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            cmd.Connection = cn;
            bool dbsValidData;
            cmd.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                    if (dt.Rows.Count != 0)
                    {
                        dbsValidData = true;
                    }
                    else { dbsValidData = false; }
                    cn.Close();
                    dt.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
                dbsValidData = false;
                //errorMessage = "EZCAPAUTH is incorrect, ";
            }

            return dbsValidData;
        }

        private static string ReturnValidDataRecordType2(RecordType2 dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command

            command.Connection = cn;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][1].ToString() + " " + dTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }
                    dTable.Reset();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        private static string ReturnValidDataModifiers(ModifierModel dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command

            command.Connection = cn;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][1].ToString() + " " + dTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }
                    dTable.Reset();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        private static string ReturnValidDataToothNumber(ToothNumbersModel dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command

            command.Connection = cn;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][1].ToString() + " " + dTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }
                    dTable.Reset();
                }
                cn.Close();
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        private static string GetDiagDescription(RecordType5 dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            command.Connection = cn;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][1].ToString() + " " + dTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }
                    cn.Close();
                    dTable.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        private static string ReturnValidDataRecordType1(RecordType1 dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            command.Connection = cn;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][0].ToString() + " " + dTable.Rows[0][1].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }

                    cn.Close();
                    dTable.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        private static string ReturnValidDataRecordType1Test(TestClaimHeaderModel dataToVal, string sqlStatement)
        {
            SqlCommand command = new SqlCommand();
            DataTable dTable = new DataTable();
            string value = "";

            //Prepare Connection 
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            if (cn.State != System.Data.ConnectionState.Open)
            {
                cn.Open();
            }

            //Prepare sql command
            command.Connection = cn;
            // command.CommandTimeout = 300;
            command.CommandText = sqlStatement;
            try
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dTable.Load(dr);
                    if (dTable.Rows.Count != 0)
                    {
                        if (dTable.Columns.Count > 1)
                        {
                            value = dTable.Rows[0][0].ToString() + " " + dTable.Rows[0][1].ToString();
                        }
                        else
                        {
                            value = dTable.Rows[0][0].ToString();
                        }
                    }

                    cn.Close();
                    dTable.Reset();
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var errorMsg = e.Message;
            }
            return value;
        }

        public static void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "CLAIMCAPTURE";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\CLAIMCAPTURE\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;

namespace WebApplication1.Controllers
{
    public class ExcelReport
    {
        private FileInfo pathToSave { get; set; }
        private string connectionStr { get; set; }
        private DataSet data { get; set; }
        private bool dataset { get; set; }
        private bool datatables { get; set; }
        private bool freezeTopRow { get; set; }
        private bool wraptext { get; set; }
        private List<DataTable> tables { get; set; }

        public ExcelReport(FileInfo filepath, DataSet ds, bool iFreezeTopRow = false, bool iWrapText = true)
        {
            pathToSave = filepath;
            data = ds;
            dataset = true;
            datatables = false;
            freezeTopRow = iFreezeTopRow;
            wraptext = iWrapText;
        }
        public ExcelReport(FileInfo filepath, List<DataTable> ds, bool iFreezeTopRow = false, bool iWrapText = true)
        {
            pathToSave = filepath;
            tables = ds;
            dataset = false;
            datatables = true;
            freezeTopRow = iFreezeTopRow;
            wraptext = iWrapText;
        }

        public void CreateReport()
        {
            try
            {
                if (dataset)
                {

                    using (XLWorkbook package = new XLWorkbook())
                    {
                        //ExcelPackage.LicenseContext = LicenseContext.Commercial;
                        foreach (DataTable td in data.Tables)
                        {
                            IXLWorksheet wss = package.AddWorksheet(td.TableName);
                            package.Save();
                            wss.Cell("A1").InsertTable(td);
                            wss.Range(1, 1, 1, td.Columns.Count).Style.Font.FontSize = 11;
                            wss.Range(1, 1, 1, td.Columns.Count).Style.Fill.BackgroundColor = XLColor.CornflowerBlue;
                            if (td.Rows.Count > 0)
                            {
                                wss.Columns().AdjustToContents();
                                //   wss.Range(1, 1, 1, td.Columns.Count).RangeUsed().SetAutoFilter(true) ;
                                wss.Range(2, 1, td.Rows.Count + 1, td.Columns.Count).Style.Alignment.WrapText = wraptext;

                            }

                            if (freezeTopRow) wss.SheetView.Freeze(1, 0);
                        }
                        package.Worksheets.Delete("Sheet1");
                        package.SaveAs(pathToSave.FullName);
                        int count = package.Worksheets.Count;
                    }
                }
                if (datatables)
                {
                    using (XLWorkbook package = new XLWorkbook())
                    {
                        int count = 1;

                        foreach (DataTable td in tables)
                        {
                            IXLWorksheet wss;
                            if (td.TableName == "" || td.TableName == null)
                            {
                                wss = package.Worksheets.Add($"Sheet_{count}");
                                count++;
                            }
                            else
                            {
                                wss = package.Worksheets.Add(td.TableName);
                            }


                            // package.Save();
                            wss.Cell("A1").Style.NumberFormat.Format = "@";
                            wss.Cell("A1").InsertTable(td);
                            wss.Range(1, 1, 1, td.Columns.Count).Style.Font.Bold = true;
                            wss.Range(1, 1, 1, td.Columns.Count).Style.Font.FontSize = 11;
                            wss.Range(1, 1, 1, td.Columns.Count).Style.Fill.BackgroundColor = XLColor.CornflowerBlue;
                            if (td.Rows.Count > 0)
                            {
                                wss.Columns().AdjustToContents();

                                //wss.Range(1, 1, 1, td.Columns.Count).SetAutoFilter(true);
                                wss.Range(2, 1, td.Rows.Count + 1, td.Columns.Count).Style.Alignment.WrapText = wraptext;

                            }

                            if (freezeTopRow) wss.SheetView.Freeze(1, 0);
                        }
                        // package.Workbook.Worksheets.Delete("Sheet1");
                        package.SaveAs(pathToSave.FullName);

                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public void SpecifyMinMaxWidth(FileInfo fi, double minWidth, double maxWidth)
        {
            using (XLWorkbook package = new XLWorkbook(fi.FullName))
            {
                foreach (IXLWorksheet wss in package.Worksheets)
                {
                    wss.Columns().AdjustToContents(minWidth, maxWidth);
                }

                package.SaveAs(fi.FullName);
            }
        }

    }
}

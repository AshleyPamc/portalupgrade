﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using NEWPAMCPORTAL.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Base.PAMCController
    {
        public ReportController(IWebHostEnvironment env, IConfiguration con):base(env,con)
        {

        }


        [HttpPost]
        [Route("GetAgeReport")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetAgeReport([FromBody] ClaimReport data)
        {
            string fileName = "AGEANALYSISREPORT.xlsx";
            var root = _env.ContentRootPath;
            FileInfo f = new FileInfo($"{root}\\tmp\\Reports\\{ fileName }");
            List<CheckRunModel> filteredList = new List<CheckRunModel>();
            DataTable tbl = new DataTable();
            ValidationResultsModel vr = new ValidationResultsModel();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();
            DataTable dtabel = new DataTable();
            string whereclaus = "";
            string[] hpcodes;
            string specification = "";

            decimal[,] currDataArray = new decimal[(DateTime.Now.Month), 7];
            decimal[,] oldDataArray = new decimal[12, 7];
            decimal[,] totals = new decimal[1, 7];
            decimal[,] prevDataArray = new decimal[12, 7];

            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            DirectoryInfo dir = new DirectoryInfo($"{root}\\ClientApp\\src\\assets\\Reports");
            if (dir.Exists == true)
            {
                f = new FileInfo(dir.FullName + "\\" + fileName);
            }
            else
            {
                f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\Reports\\{ fileName }");
            }

            if (data.hpcode.Contains(';'))
            {
                hpcodes = new string[data.hpcode.Split(';').Length];
                hpcodes = data.hpcode.Split(';');
                for (int i = 0; i < hpcodes.Length; i++)
                {
                    hpcodes[i] = hpcodes[i].TrimEnd();

                    if (i == (hpcodes.Length - 1))
                    {
                        whereclaus += $"'{hpcodes[i]}'";
                    }
                    else
                    {
                        whereclaus += $"'{hpcodes[i]}',";
                    }


                }
            }
            else
            {
                hpcodes = new string[1];
                hpcodes[0] = data.hpcode;
                whereclaus = $"'{hpcodes[0]}'";
            }
            try
            {

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{data.user}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtabel.Load(dr);

                }
                foreach (DataRow row in dtabel.Rows)
                {
                    specification = row["specifications"].ToString();
                    if (specification.Length == 0)
                    {
                        specification = "1=1";
                    }
                }
                cn.Close();
                //cn.ConnectionString = _reportConnectionString;  //Jaco 2023-09-11
                cn.ConnectionString = _dataWarehouseConnectionString; //Jaco 2023-09-11
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                string sql = "SELECT        YEAR(DATERECD) AS RecYear,MONTH(DATERECD) AS RecMonth,\n"
               + " SUM(NET) AS Amount, HPCODE, OPT,\n"
               + "CASE WHEN DATEDIFF(DAY ,DATERECD,GETDATE())<=30 THEN 1\n"
               + "WHEN DATEDIFF(DAY ,DATERECD,GETDATE())>30 AND DATEDIFF(DAY ,DATERECD,GETDATE())<=60 THEN 2\n"
               + "WHEN DATEDIFF(DAY ,DATERECD,GETDATE())>60 AND DATEDIFF(DAY ,DATERECD,GETDATE())<=90 THEN 3\n"
               + "WHEN DATEDIFF(DAY ,DATERECD,GETDATE())>90 AND DATEDIFF(DAY ,DATERECD,GETDATE())<=120 THEN 4\n"
               + "WHEN DATEDIFF(DAY ,DATERECD,GETDATE())>120 AND DATEDIFF(DAY ,DATERECD,GETDATE())<=180 THEN 5\n"
               + "WHEN DATEDIFF(DAY ,DATERECD,GETDATE())>180  THEN 6\n"
               + "END AS CATEGORY,isRiskOpt\n"
               + "FROM FactClaims                        \n"
              + $"WHERE  HPCODE IN ({whereclaus}) \n"
               + " --AND DATERECD >= dateadd(MONTH, -24, GETDATE()) \n"
               + " --AND isnull( DATEPAID,getdate()) >= dateadd(MONTH, -24, GETDATE())\n"
               + "AND NET > 0 \n"
               + $"AND STATUS <> 9 \n  AND isRiskOpt = 'NonRisk'\n"
               + "GROUP BY YEAR(DATERECD),MONTH(DATERECD), HPCODE, OPT,DATERECD,isRiskOpt\n"
               + "\n --HAVING SUM(NET) > 0\n"
               + "ORDER BY RecYear,RecMonth,CATEGORY";

                cmd.CommandTimeout = 3600;
                cmd.CommandText = sql;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            int r = 0;
            int c = 0;
            string recYear = "";

            foreach (DataRow row in dt.Rows)
            {
                recYear = row["RecYear"].ToString();
                //  category = row["CATEGORY"].ToString();

                if (recYear == (DateTime.Now.Year - 2).ToString())
                {
                    r = Convert.ToInt32(row["RecMonth"].ToString());
                    c = Convert.ToInt32(row["CATEGORY"].ToString());

                    oldDataArray[(r - 1), (c - 1)] = oldDataArray[(r - 1), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    oldDataArray[(r - 1), (6)] = oldDataArray[(r - 1), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (c - 1)] = totals[(0), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (6)] = totals[(0), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                }
                if (recYear == (DateTime.Now.Year - 1).ToString())
                {
                    r = Convert.ToInt32(row["RecMonth"].ToString());
                    c = Convert.ToInt32(row["CATEGORY"].ToString());

                    prevDataArray[(r - 1), (c - 1)] = prevDataArray[(r - 1), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    prevDataArray[(r - 1), (6)] = prevDataArray[(r - 1), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (c - 1)] = totals[(0), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (6)] = totals[(0), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                }
                if (recYear == (DateTime.Now.Year).ToString())
                {
                    r = Convert.ToInt32(row["RecMonth"].ToString());
                    c = Convert.ToInt32(row["CATEGORY"].ToString());

                    currDataArray[(r - 1), (c - 1)] = currDataArray[(r - 1), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    currDataArray[(r - 1), (6)] = currDataArray[(r - 1), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (c - 1)] = totals[(0), (c - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                    totals[(0), (6)] = totals[(0), (6)] + Convert.ToDecimal(row["Amount"].ToString());
                }




            }

            using (var excelPackage = new ExcelPackage(f))
            {

                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                int hiddenHorRow = 0;

                //  excelPackage.Workbook.Properties.Title = "TEST EXPORT";
                //  excelPackage.Workbook.Properties.Created = DateTime.Now;
                excelPackage.Workbook.Worksheets[0].Name = "sheet";
                excelPackage.Save();
                
                
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("AGE REPORT");
                excelPackage.Workbook.Worksheets.Delete("sheet");
                excelPackage.Save();
                worksheet.Cells[1, 1].Value = "AGE ANALYSIS ON UNPAID CLAIMS";
                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Style.Font.Size = 15;
                worksheet.Cells[1, 1, 1, (prevDataArray.GetLength(1) + 2)].Merge = true;
                worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int excelCol = (prevDataArray.GetLength(1) + 1);
                int excelRow = 3;
                worksheet.Cells[excelRow, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[excelRow, 1].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                worksheet.Cells[excelRow, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[excelRow, 2].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);

                for (int i = 3; i <= excelCol + 1; i++)
                {
                    if (i == 3)
                    {
                        worksheet.Cells[excelRow, i].Value = "CURRENT";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 4)
                    {
                        worksheet.Cells[excelRow, i].Value = ">30 DAYS";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 5)
                    {
                        worksheet.Cells[excelRow, i].Value = ">60 DAYS";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 6)
                    {
                        worksheet.Cells[excelRow, i].Value = ">90 DAYS";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 7)
                    {
                        worksheet.Cells[excelRow, i].Value = ">120 DAYS";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 8)
                    {
                        worksheet.Cells[excelRow, i].Value = ">180 DAYS";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }
                    if (i == 9)
                    {
                        worksheet.Cells[excelRow, i].Value = "TOTAL";
                        worksheet.Cells[excelRow, i].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    }


                }

                int temp = 1;
                for (int t = 4; t <= DateTime.Now.Month + 27; t++)
                {
                    for (int s = 1; s <= 2; s++)
                    {
                        if (t == 4)
                        {
                            worksheet.Cells[t, 1].Value = (DateTime.Now.Year - 2).ToString();
                            worksheet.Cells[t, 1].Style.Font.Bold = true;
                            worksheet.Cells[t, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[t, 1].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);

                        }
                        if (t == 16)
                        {
                            hiddenHorRow = t + DateTime.Now.Month - 1;
                            worksheet.Cells[t, 1].Value = (DateTime.Now.Year - 1).ToString();
                            worksheet.Cells[t, 1].Style.Font.Bold = true;
                            worksheet.Cells[t, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[t, 1].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);
                            temp = 1;
                        }
                        if (t == 28)
                        {
                            worksheet.Cells[t, 1].Value = (DateTime.Now.Year).ToString();
                            worksheet.Cells[t, 1].Style.Font.Bold = true;
                            worksheet.Cells[t, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[t, 1].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);
                            temp = 1;
                        }
                        worksheet.Cells[t, 2].Value = temp;
                        worksheet.Cells[t, 2].Style.Font.Bold = true;


                    }

                    temp++;
                }
                // excelRow++;
                for (int e = 4; e < oldDataArray.GetLength(0) + 4; e++)
                {
                    for (int p = 3; p < oldDataArray.GetLength(1) + 3; p++)
                    {
                        if (oldDataArray[e - 4, p - 3] == 0)
                        {
                            worksheet.Cells[e, p].Value = "";
                        }
                        else
                        {
                            worksheet.Cells[e, p].Value = oldDataArray[e - 4, p - 3];
                            worksheet.Cells[e, p].Style.Numberformat.Format = "R #,##0.00";
                        }
                        if ((p) == (oldDataArray.GetLength(1) + 2))
                        {
                            worksheet.Cells[e, p].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[e, p].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);
                        }
                    }

                }
                for (int e = 16; e < prevDataArray.GetLength(0) + 16; e++)
                {
                    for (int p = 3; p < prevDataArray.GetLength(1) + 3; p++)
                    {

                        if (prevDataArray[e - 16, p - 3] == 0)
                        {
                            worksheet.Cells[e, p].Value = "";
                        }
                        else
                        {
                            worksheet.Cells[e, p].Value = prevDataArray[e - 16, p - 3];
                            worksheet.Cells[e, p].Style.Numberformat.Format = "R #,##0.00";
                        }
                        if ((p) == (prevDataArray.GetLength(1) + 2))
                        {
                            worksheet.Cells[e, p].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[e, p].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);
                        }
                    }

                }
                for (int e = 28; e < currDataArray.GetLength(0) + 28; e++)
                {
                    for (int p = 3; p < currDataArray.GetLength(1) + 3; p++)
                    {
                        if (currDataArray[e - 28, p - 3] == 0)
                        {
                            worksheet.Cells[e, p].Value = "";
                        }
                        else
                        {
                            worksheet.Cells[e, p].Value = currDataArray[e - 28, p - 3];
                            worksheet.Cells[e, p].Style.Numberformat.Format = "R #,##0.00";
                        }
                        if ((p) == (currDataArray.GetLength(1) + 2))
                        {
                            worksheet.Cells[e, p].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[e, p].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);
                        }
                    }

                }

                for (int i = 3; i < totals.GetLength(1) + 3; i++)
                {
                    worksheet.Cells[28 + DateTime.Now.Month, i].Formula = $"=SUM({worksheet.Cells[4, i].Address}:{worksheet.Cells[28 + DateTime.Now.Month - 1, i].Address})";
                    worksheet.Cells[28 + DateTime.Now.Month, i].Style.Numberformat.Format = "R #,##0.00";
                    worksheet.Cells[28 + DateTime.Now.Month, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[28 + DateTime.Now.Month, i].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                    worksheet.Cells[28 + DateTime.Now.Month, i].Style.Font.Bold = true;
                }
                worksheet.Cells[28 + DateTime.Now.Month, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[28 + DateTime.Now.Month, 1].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);
                worksheet.Cells[28 + DateTime.Now.Month, 1].Value = "TOTAL";

                worksheet.Cells[28 + DateTime.Now.Month, 1].Style.Font.Bold = true;
                worksheet.Cells[28 + DateTime.Now.Month, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[28 + DateTime.Now.Month, 2].Style.Fill.BackgroundColor.SetColor(Color.LightSeaGreen);

                for (int i = 4; i < hiddenHorRow; i++)
                {
                    worksheet.Row(i).Hidden = true;
                }
                worksheet.Cells[hiddenHorRow, 1].Value = (DateTime.Now.Year - 1).ToString();
                worksheet.Cells[hiddenHorRow, 1].Style.Font.Bold = true;
                worksheet.Cells[hiddenHorRow, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[hiddenHorRow, 1].Style.Fill.BackgroundColor.SetColor(Color.LightCyan);


                //  FileInfo fi = new FileInfo("C:/Users/Ashley/Desktop/test2.xlsx");
                worksheet.Calculate();
                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                try
                {
                    cn.Close();
                    excelPackage.SaveAs(f);
                    vr.message = f.Name;
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }

            }
            return vr;
        }

        [HttpPost("GetGraphReport")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimsSummary> GetGraphReport([FromBody] ClaimReport data)
        {
            List<ClaimsSummary> returnedList = new List<ClaimsSummary>();
            EndpointAddress address = new EndpointAddress(_ReportWebSvcConnection);
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 2147483647;
            var time = new TimeSpan(0, 300, 0);
            binding.CloseTimeout = time;
            binding.OpenTimeout = time;
            binding.ReceiveTimeout = time;
            binding.SendTimeout = time;
            string hpcodes = "";

            hpcodes = data.hpcode;


            try
            {
                ReportService.ManagementDataSoapClient client = new ReportService.ManagementDataSoapClient(binding, address);



                var w = client.ServiceDateDataAsync(hpcodes);
                w.Wait();

                //ds.ReadXml(new StringReader ));
                //var wc = w.GetType();

                //  returnedList =  ;
                var c = w.Result.Body.ServiceDateDataResult;
                foreach (var item in c)
                {
                    ClaimsSummary cs = new ClaimsSummary();
                    cs.Billed = item.Billed;
                    cs.Hpcode = item.Hpcode;
                    cs.IsRiskOpt = item.IsRiskOpt;
                    cs.Net = item.Net;
                    cs.Opt = item.Opt;
                    cs.PaidMonth = item.PaidMonth;
                    cs.PaidYear = item.PaidYear;
                    cs.ReceivedMonth = item.ReceivedMonth;
                    cs.ReceivedYear = item.ReceivedYear;
                    cs.ServiceMonth = item.ServiceMonth;
                    cs.ServiceYear = item.ServiceYear;
                    cs.ServiceYM = item.ServiceYM;
                    cs.Status = item.Status;
                    returnedList.Add(cs);


                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            //returnedList = ;

            return returnedList;
        }


        [HttpPost]
        [Route("GetLagReport")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetReport([FromBody] ClaimReport data)
        {
            string fileName = "LAGREPORT.xlsx";
            var root = _env.ContentRootPath;
            FileInfo f = new FileInfo($"{root}\\tmp\\Reports\\{ fileName }");
            List<CheckRunModel> filteredList = new List<CheckRunModel>();
            DataTable tbl = new DataTable();
            ValidationResultsModel vr = new ValidationResultsModel();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();
            DataTable dtabel = new DataTable();
            string whereclaus = "";
            string[] hpcodes;
            string specification = "";

            decimal[,] currDataArray = new decimal[(DateTime.Now.Month), (DateTime.Now.Month + 25)];
            decimal[,] oldDataArray = new decimal[12, (DateTime.Now.Month + 25)];
            decimal[,] totals = new decimal[1, (DateTime.Now.Month + 26)];
            decimal[,] prevDataArray = new decimal[12, (DateTime.Now.Month + 25)];
            decimal[,] currUnpaidArray = new decimal[DateTime.Now.Month, 1];
            decimal[,] prevUnpaidArray = new decimal[12, 1];
            decimal[,] oldUnpaidArray = new decimal[12, 1];

            cn.ConnectionString = _portalConnectionString;
            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }


            if (data.hpcode.Contains(';'))
            {
                hpcodes = new string[data.hpcode.Split(';').Length];
                hpcodes = data.hpcode.Split(';');
                for (int i = 0; i < hpcodes.Length; i++)
                {
                    hpcodes[i] = hpcodes[i].TrimEnd();

                    if (i == (hpcodes.Length - 1))
                    {
                        whereclaus += $"'{hpcodes[i]}'";
                    }
                    else
                    {
                        whereclaus += $"'{hpcodes[i]}',";
                    }


                }
            }
            else
            {
                hpcodes = new string[1];
                hpcodes[0] = data.hpcode;
                whereclaus = $"'{hpcodes[0]}'";
            }
            try
            {
                DirectoryInfo dir = new DirectoryInfo($"{root}\\ClientApp\\src\\assets\\Reports");
                if (dir.Exists == true)
                {
                    f = new FileInfo(dir.FullName + "\\" + fileName);
                }
                else
                {
                    f = new FileInfo($"{root}\\ClientApp\\dist\\assets\\Reports\\{ fileName }");
                }
                cmd.Connection = cn;

                cmd.CommandText = $"SELECT specifications FROM " + this.PamcPortalDatabase + $"..Users WHERE username = '{data.user}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtabel.Load(dr);

                }
                foreach (DataRow row in dtabel.Rows)
                {
                    specification = row["specifications"].ToString();
                    if (specification.Length == 0)
                    {
                        specification = "1=1";
                    }
                }
                cn.Close();
                //cn.ConnectionString = _reportConnectionString; //Jaco 2023-09-11
                cn.ConnectionString = _dataWarehouseConnectionString;  //Jaco 2023-09-11
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }

            string sql = "SELECT        YEAR(FROMDATESVC) AS SvcYear, MONTH(FROMDATESVC) AS SvcMonth, CASE WHEN STATUS = 9 THEN YEAR(DATEPAID) ELSE NULL END AS PaidYear, \n"
               + "MONTH(DATEPAID) AS PaidMonth, SUM(NET) AS Amount, HPCODE, OPT\n"
               + "FROM            FactClaims                        \n"
               + $"WHERE        HPCODE IN ({whereclaus})  AND isRiskOpt = 'NonRisk'\n"
               + " AND FROMDATESVC >= dateadd(MONTH, -24, GETDATE()) \n"
               + " AND isnull(DATEPAID,getdate()) >= dateadd(MONTH, -24, GETDATE()) \n"
               + "GROUP BY YEAR(FROMDATESVC), MONTH(FROMDATESVC), YEAR(DATEPAID), MONTH(DATEPAID), HPCODE, OPT,STATUS\n"
               + "\n"
               + "ORDER BY SvcYear,SvcMonth,PaidYear,PaidMonth";

            //string sql = "SELECT        YEAR(FROMDATESVC) AS SvcYear, MONTH(FROMDATESVC) AS SvcMonth, CASE WHEN STATUS = 9 THEN YEAR(DATEPAID) ELSE NULL END AS PaidYear, \n"
            //  + "MONTH(DATEPAID) AS PaidMonth, SUM(cd.NET) AS Amount, HPCODE, OPT\n"
            //  + "FROM            DRC..CLAIM_MASTERS cm\n"
            //  + "INNER JOIN DRC..CLAIM_DETAILS cd ON cd.CLAIMNO =    cm.CLAIMNO                     \n"
            //  + $"WHERE        HPCODE IN ({whereclaus})\n"
            //  + " AND FROMDATESVC >= dateadd(MONTH, -24, GETDATE()) \n"
            //  + " AND isnull(DATEPAID,getdate()) >= dateadd(MONTH, -24, GETDATE()) \n"
            //  + "GROUP BY YEAR(FROMDATESVC), MONTH(FROMDATESVC), YEAR(DATEPAID), MONTH(DATEPAID), HPCODE, OPT,STATUS\n"
            //  + "\n"
            //  + "ORDER BY SvcYear,SvcMonth,PaidYear,PaidMonth";
            cmd.CommandText = sql;
            cmd.Connection = cn;
            cmd.CommandTimeout = 3600;
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }

            int r = 0;
            int c = 0;
            string payYear = "";
            string servYear = "";
            string servMonth = "";
            string paymonth = "";
            //int status;

            foreach (DataRow row in dt.Rows)
            {
                payYear = row["PaidYear"].ToString();
                servYear = row["SvcYear"].ToString();
                servMonth = row["SvcMonth"].ToString();
                paymonth = row["PaidMonth"].ToString();
                //status = Convert.ToInt32(row["STATUS"].ToString());

                if (servYear == (DateTime.Now.Year - 2).ToString())
                {
                    if (row.IsNull("PaidYear"))
                    {

                        r = Convert.ToInt32(row["SvcMonth"].ToString());
                        oldUnpaidArray[r - 1, 0] = oldUnpaidArray[r - 1, 0] + Convert.ToDecimal(row["Amount"].ToString());
                        oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] = oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());



                    }
                    else
                    {
                        if (payYear == (DateTime.Now.Year - 2).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString());
                            oldDataArray[r - 1, c - 1] = oldDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] = oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());




                        }

                        if (payYear == (DateTime.Now.Year - 1).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString()) + 12;
                            oldDataArray[r - 1, c - 1] = oldDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] = oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());




                        }

                        if (payYear == (DateTime.Now.Year).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = (Convert.ToInt32(row["PaidMonth"].ToString()) - 1) + 24;
                            oldDataArray[r - 1, c] = oldDataArray[r - 1, c] + Convert.ToDecimal(row["Amount"].ToString());
                            oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] = oldDataArray[r - 1, (oldDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                        }
                    }
                }
                if (servYear == (DateTime.Now.Year - 1).ToString())
                {
                    if (row.IsNull("PaidYear"))
                    {

                        r = Convert.ToInt32(row["SvcMonth"].ToString());
                        prevUnpaidArray[r - 1, 0] = prevUnpaidArray[r - 1, 0] + Convert.ToDecimal(row["Amount"].ToString());
                        prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] = prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());


                    }
                    else
                    {
                        if (payYear == (DateTime.Now.Year - 2).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString());
                            prevDataArray[r - 1, c - 1] = prevDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] = prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());




                        }

                        if (payYear == (DateTime.Now.Year - 1).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString()) + 12;
                            prevDataArray[r - 1, c - 1] = prevDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] = prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());




                        }

                        if (payYear == (DateTime.Now.Year).ToString())
                        {

                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = (Convert.ToInt32(row["PaidMonth"].ToString()) - 1) + 24;
                            prevDataArray[r - 1, c] = prevDataArray[r - 1, c] + Convert.ToDecimal(row["Amount"].ToString());
                            prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] = prevDataArray[r - 1, (prevDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                        }
                    }
                }
                if (servYear == (DateTime.Now.Year).ToString())
                {

                    if (row.IsNull("PaidYear"))
                    {

                        r = Convert.ToInt32(row["SvcMonth"].ToString());
                        currUnpaidArray[r - 1, 0] = currUnpaidArray[r - 1, 0] + Convert.ToDecimal(row["Amount"].ToString());
                        currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] = currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());


                    }
                    else
                    {
                        if (payYear == (DateTime.Now.Year - 2).ToString())
                        {
                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString());
                            currDataArray[r - 1, c - 1] = currDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] = currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());


                        }
                        if (payYear == (DateTime.Now.Year - 1).ToString())
                        {
                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = Convert.ToInt32(row["PaidMonth"].ToString()) + 12;
                            currDataArray[r - 1, c - 1] = currDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] = currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());


                        }
                        if (payYear == (DateTime.Now.Year).ToString())
                        {
                            r = Convert.ToInt32(row["SvcMonth"].ToString());
                            c = (Convert.ToInt32(row["PaidMonth"].ToString())) + 24;
                            currDataArray[r - 1, c - 1] = currDataArray[r - 1, c - 1] + Convert.ToDecimal(row["Amount"].ToString());
                            currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] = currDataArray[r - 1, (currDataArray.GetLength(1) - 1)] + Convert.ToDecimal(row["Amount"].ToString());
                        }
                    }

                }


            }

            using (var excelPackage = new ExcelPackage(f))
            {
                int hiddenVerCol = 0;
                int hiddenHorRow = 0;

                // excelPackage.Workbook.Properties.Title = "TEST EXPORT";
                //excelPackage.Workbook.Properties.Created = DateTime.Now;
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                excelPackage.Workbook.Worksheets[0].Name = "sheet";
                excelPackage.Save();
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("LAG REPORT");
                excelPackage.Workbook.Worksheets.Delete("sheet");

                //int startRow = worksheet.Dimension.Start.Row + 1;
                //int endRow = worksheet.Dimension.End.Row;
                //int startCol = worksheet.Dimension.Start.Column;
                //int endCol = worksheet.Dimension.End.Column;
                //for (int i = startRow; i <= endRow; i++)
                //{
                //    for (int j = startCol; j <= endCol; j++)
                //    {
                //        worksheet.Cells[i, j].Clear();
                //    }
                //}

                worksheet.Cells[1, 1].Value = "LAG REPORT";
                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Style.Font.Size = 15;
                worksheet.Cells[1, 1, 1, (prevDataArray.GetLength(1) + 3)].Merge = true;
                worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int excelCol = (prevDataArray.GetLength(1) + 1);
                int excelRow = 3;
                worksheet.Cells[3, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[3, 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                worksheet.Cells[3, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[3, 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                for (int i = 3; i <= excelCol; i++)
                {
                    worksheet.Cells[excelRow, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[excelRow, i].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                    if (i == 3)
                    {
                        worksheet.Cells[excelRow, i + 1].Value = (DateTime.Now.Year - 2).ToString();
                        worksheet.Cells[excelRow, i + 1].Style.Font.Bold = true;
                    }
                    if (i == 14)
                    {
                        worksheet.Cells[excelRow, i + 2].Value = (DateTime.Now.Year - 1).ToString();
                        worksheet.Cells[excelRow, i + 2].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);

                    }
                    if (i == 26)
                    {
                        worksheet.Cells[excelRow, i + 2].Value = (DateTime.Now.Year).ToString();
                        worksheet.Cells[excelRow, i + 2].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);

                    }
                    if (i == (excelCol))
                    {
                        worksheet.Cells[excelRow, i + 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i + 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                        worksheet.Cells[excelRow, i + 2].Value = "Grand Total";
                        worksheet.Cells[excelRow, i + 2].Style.Font.Bold = true;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[excelRow, i + 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                    }

                }
                excelRow++;
                int tempCount = 1;
                worksheet.Cells[excelRow, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[excelRow, 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                worksheet.Cells[excelRow, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[excelRow, 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                //  worksheet.Cells[excelRow, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                //worksheet.Cells[excelRow, 3].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);

                for (int j = 1; j <= excelCol; j++)
                {
                    //   Console.WriteLine(worksheet.Cells[excelRow - 1, j + 3].GetValue<string>());
                    var val = worksheet.Cells[excelRow - 1, j + 3].Value;
                    if (j == 3)
                    {
                        worksheet.Cells[excelRow, j].Value = "UNPAID";
                        worksheet.Cells[excelRow, j].Style.Font.Bold = true;
                    }
                    if ((j < 13))
                    {
                        worksheet.Cells[excelRow, j + 3].Value = tempCount;
                        worksheet.Cells[excelRow, j + 3].Style.Font.Bold = true;
                    }
                    else
                    {
                        if (j == 13)
                        {
                            tempCount = 1;
                            worksheet.Cells[excelRow, j + 3].Value = tempCount;
                            worksheet.Cells[excelRow, j + 3].Style.Font.Bold = true;


                        }
                        if ((j > 13) && (j < (25)))
                        {
                            if (tempCount == DateTime.Now.Month)
                            {
                                hiddenVerCol = j + 3;
                                worksheet.Cells[excelRow - 1, j + 3].Value = (DateTime.Now.Year - 1).ToString();
                                worksheet.Cells[excelRow - 1, j + 3].Style.Font.Bold = true;
                            }
                            worksheet.Cells[excelRow, j + 3].Value = tempCount;
                            worksheet.Cells[excelRow, j + 3].Style.Font.Bold = true;
                        }
                        if (j == 25)
                        {
                            tempCount = 1;
                            worksheet.Cells[excelRow, j + 3].Value = tempCount;
                            worksheet.Cells[excelRow, j + 3].Style.Font.Bold = true;
                        }
                        if ((j > 25) && (j < (excelCol - 1)))
                        {
                            worksheet.Cells[excelRow, j + 3].Value = tempCount;
                            worksheet.Cells[excelRow, j + 3].Style.Font.Bold = true;
                        }

                    }
                    worksheet.Cells[excelRow, j + 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[excelRow, j + 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                    tempCount++;
                }
                excelRow++;
                tempCount = excelRow;
                worksheet.Cells[tempCount, 1].Value = (DateTime.Now.Year - 2).ToString();
                worksheet.Cells[tempCount, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                worksheet.Cells[tempCount, 1].Style.Font.Bold = true;

                for (int h = 0; h < oldDataArray.GetLength(0); h++)
                {
                    for (int k = 0; k < oldDataArray.GetLength(1); k++)
                    {
                        if (oldDataArray[h, k] == 0)
                        {
                            if (k == oldDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = "";
                        }
                        else
                        {
                            if (k == oldDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = oldDataArray[h, k];
                            worksheet.Cells[tempCount, k + 4].Style.Numberformat.Format = "R #,##0.00";
                        }


                    }

                    if (oldUnpaidArray[h, 0] == 0)
                    {
                        worksheet.Cells[tempCount, 3].Value = "";
                    }
                    else
                    {
                        worksheet.Cells[tempCount, 3].Value = oldUnpaidArray[h, 0];
                        worksheet.Cells[tempCount, 3].Style.Numberformat.Format = "R #,##0.00";
                    }
                    worksheet.Cells[tempCount, 2].Value = (h + 1);
                    worksheet.Cells[tempCount, 2].Style.Font.Bold = true;
                    tempCount++;
                }
                worksheet.Cells[tempCount, 1].Value = (DateTime.Now.Year - 1).ToString();
                worksheet.Cells[tempCount, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                worksheet.Cells[tempCount, 1].Style.Font.Bold = true;
                for (int h = 0; h < prevDataArray.GetLength(0); h++)
                {
                    for (int k = 0; k < prevDataArray.GetLength(1); k++)
                    {
                        if (prevDataArray[h, k] == 0)
                        {
                            if (k == prevDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = "";
                        }
                        else
                        {
                            if (k == prevDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = prevDataArray[h, k];
                            worksheet.Cells[tempCount, k + 4].Style.Numberformat.Format = "R #,##0.00";
                        }


                    }

                    if (prevUnpaidArray[h, 0] == 0)
                    {
                        worksheet.Cells[tempCount, 3].Value = "";
                    }
                    else
                    {
                        worksheet.Cells[tempCount, 3].Value = prevUnpaidArray[h, 0];
                        worksheet.Cells[tempCount, 3].Style.Numberformat.Format = "R #,##0.00";
                    }

                    if ((h + 1) == DateTime.Now.Month)
                    {
                        hiddenHorRow = tempCount;
                        worksheet.Cells[tempCount, 1].Value = (DateTime.Now.Year - 1).ToString();
                        worksheet.Cells[tempCount, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                        worksheet.Cells[tempCount, 1].Style.Font.Bold = true;
                    }
                    worksheet.Cells[tempCount, 2].Value = (h + 1);
                    worksheet.Cells[tempCount, 2].Style.Font.Bold = true;
                    tempCount++;
                }


                worksheet.Cells[tempCount, 1].Value = DateTime.Now.Year.ToString();
                worksheet.Cells[tempCount, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                worksheet.Cells[tempCount, 1].Style.Font.Bold = true;
                for (int h = 0; h < currDataArray.GetLength(0); h++)
                {
                    for (int k = 0; k < currDataArray.GetLength(1); k++)
                    {
                        if (currDataArray[h, k] == 0)
                        {
                            if (k == prevDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = "";
                        }
                        else
                        {
                            if (k == currDataArray.GetLength(1) - 1)
                            {
                                worksheet.Cells[tempCount, k + 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[tempCount, k + 4].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            }
                            worksheet.Cells[tempCount, k + 4].Value = currDataArray[h, k];
                            worksheet.Cells[tempCount, k + 4].Style.Numberformat.Format = "R #,##0.00";
                        }


                    }
                    if (currUnpaidArray[h, 0] == 0)
                    {
                        worksheet.Cells[tempCount, 3].Value = "";
                    }
                    else
                    {
                        worksheet.Cells[tempCount, 3].Value = currUnpaidArray[h, 0];
                        worksheet.Cells[tempCount, 3].Style.Numberformat.Format = "R #,##0.00";
                    }


                    worksheet.Cells[tempCount, 2].Value = (h + 1);
                    worksheet.Cells[tempCount, 2].Style.Font.Bold = true;
                    tempCount++;
                }

                for (int i = 0; i < oldUnpaidArray.GetLength(0); i++)
                {
                    for (int j = 0; j < oldUnpaidArray.GetLength(1); j++)
                    {
                        totals[0, j] = totals[0, j] + oldUnpaidArray[i, j];
                    }
                }
                for (int i = 0; i < prevUnpaidArray.GetLength(0); i++)
                {
                    for (int j = 0; j < prevUnpaidArray.GetLength(1); j++)
                    {
                        totals[0, j] = totals[0, j] + prevUnpaidArray[i, j];
                    }
                }
                for (int i = 0; i < currUnpaidArray.GetLength(0); i++)
                {
                    for (int j = 0; j < currUnpaidArray.GetLength(1); j++)
                    {
                        totals[0, j] = totals[0, j] + currUnpaidArray[i, j];
                    }
                }
                for (int i = 0; i < prevDataArray.GetLength(0); i++)
                {
                    for (int j = 0; j < prevDataArray.GetLength(1); j++)
                    {
                        totals[0, j + 1] = totals[0, j + 1] + prevDataArray[i, j];
                    }
                }
                for (int i = 0; i < currDataArray.GetLength(0); i++)
                {
                    for (int j = 0; j < currDataArray.GetLength(1); j++)
                    {
                        totals[0, j + 1] = totals[0, j + 1] + currDataArray[i, j];
                    }
                }
                for (int i = 0; i < oldDataArray.GetLength(0); i++)
                {
                    for (int j = 0; j < oldDataArray.GetLength(1); j++)
                    {
                        totals[0, j + 1] = totals[0, j + 1] + oldDataArray[i, j];
                    }
                }



                for (int j = 0; j < totals.GetLength(1); j++)
                {

                    worksheet.Cells[tempCount, j + 3].Value = totals[0, j];
                    worksheet.Cells[tempCount, j + 3].Style.Numberformat.Format = "R #,##0.00";
                    worksheet.Cells[tempCount, j + 3].Style.Font.Bold = true;
                    worksheet.Cells[tempCount, j + 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[tempCount, j + 3].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);

                }


                worksheet.Cells[tempCount, 1].Value = "Grand Total";
                worksheet.Cells[tempCount, 1].Style.Font.Bold = true;
                worksheet.Cells[tempCount, 2].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[tempCount, 2].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                worksheet.Cells[tempCount, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                worksheet.Cells[tempCount, 1].Style.Fill.BackgroundColor.SetColor(Color.LightBlue);
                //  FileInfo fi = new FileInfo("C:/Users/Ashley/Desktop/test.xlsx");
                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                for (int i = 5; i < hiddenHorRow; i++)
                {
                    worksheet.Row(i).Hidden = true;
                }
                for (int j = 4; j < hiddenVerCol; j++)
                {
                    worksheet.Column(j).Hidden = true;
                }
                try
                {
                    cn.Close();
                    excelPackage.SaveAs(f);
                    vr.message = f.Name;
                }
                catch (Exception e)
                {
                    LogError(e.Message, e.StackTrace);
                    var msg = e.Message;
                }

            }




            return vr;
        }

        [HttpPost]
        public static void LogError(string error, string stacktrace)
        {

            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "REPORT";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\REPORT\\{fileName}");
            // fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName, append: true))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }
    }
}
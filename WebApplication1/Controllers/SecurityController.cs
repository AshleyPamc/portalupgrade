﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEWPAMCPORTAL.Models;
using MediWalletWebsite_1.Models;

namespace NEWPAMCPORTAL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : Base.PAMCController
    {

        public SecurityController(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }


        [Route("GetFaqs")]
        [HttpPost]
        public List<Faq> GetFaqs([FromBody] Faq data)
        {
            List<Faq> list = new List<Faq>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("USERTYPE", data.usertype));

                    cmd.CommandText = "SELECT * FROM FAQ WHERE USERTYPE = @USERTYPE AND ENABLED = 1";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Faq f = new Faq();

                        f.answer = row["ANSWER"].ToString();
                        f.question = row["QUESTION"].ToString();

                        list.Add(f);
                    }

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return list;
        }

        /*Web Call to get all the healthplans on the member eligibilty search*/
        [HttpPost]
        [Route("MemberDropdown")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<HP_ContractsModel> HPContracts([FromBody] HP_ContractsModel model)
        {
            DataTable dtmember = new DataTable();
            List<HP_ContractsModel> ListOfHPCodesmember = new List<HP_ContractsModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open) Sqlcon.Open();
                   
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        if (model.isDentist == true)
                        {
                            string contract = "";
                            if (model.contracted)
                            {
                                contract = $" and isContract = 'true'";
                            }
                            else
                            {
                                contract = $" and isNotContract = 'true'";
                            }
                            cmd.CommandType = CommandType.Text;
                            string SQLDentistToExecute = $"Select [HPCode],[HPName],[chronicConditionsContract] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                    "Where isEnabledMember = '" + model.isEnabledMember + $"' and Dentists = 'true' {contract}\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = SQLDentistToExecute;

                            dtmember.Load(cmd.ExecuteReader());
                        }
                        if (model.isSpecialist == true)
                        {
                            string contract = "";
                            if (model.contracted)
                            {
                                contract = $" and isContract = 'true'";
                            }
                            else
                            {
                                contract = $" and isNotContract = 'true'";
                            }

                            cmd.CommandType = CommandType.Text;
                            string SQLDentistToExecute = $"Select [HPCode],[HPName],[chronicConditionsContract] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                    "Where isEnabledMember = '" + model.isEnabledMember + $"' and Specialists = 'true' {contract}\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = SQLDentistToExecute;

                            dtmember.Load(cmd.ExecuteReader());
                        }
                        if (model.isHospital == true)
                        {
                            string contract = "";
                            if (model.contracted)
                            {
                                contract = $" and isContract = 'true'";
                            }
                            else
                            {
                                contract = $" and isNotContract = 'true'";
                            }
                            cmd.CommandType = CommandType.Text;
                            string SQLDentistToExecute = $"Select [HPCode],[HPName],[chronicConditionsContract] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                    "Where isEnabledMember = '" + model.isEnabledMember + $"' and Hospitals = 'true' {contract}\r\n";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = SQLDentistToExecute;
                            dtmember.Load(cmd.ExecuteReader());
                        }
                        foreach (DataRow dr in dtmember.Rows)
                        {
                            HP_ContractsModel hpcontractmember = new HP_ContractsModel();
                            hpcontractmember.HPCODE = dr["HPCODE"].ToString();
                            hpcontractmember.HPNAME = dr["HPNAME"].ToString();                           
                            if (dr["chronicConditionsContract"] != DBNull.Value)
                            {
                                hpcontractmember.chronicConditionsContract = Convert.ToBoolean(dr["chronicConditionsContract"]);
                            }
                            else
                            {
                                hpcontractmember.chronicConditionsContract = false;
                            }

                            ListOfHPCodesmember.Add(hpcontractmember);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }

            return ListOfHPCodesmember;
        }

        /*Gets all healthPlans for the claim serach dropdown*/
        [HttpPost]
        [Route("ClaimDropdown")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "NormalUser,Administrator")]
        public List<HP_ContractsModel> HPContractsClaims([FromBody] HP_ContractsModel model)
        {
            DataTable dtclaim = new DataTable();
            List<HP_ContractsModel> ListOfHPCodesclaim = new List<HP_ContractsModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        if (model.userType == 2)
                        {
                            cmd.CommandType = CommandType.Text;
                            string sqlTextToExecute = $"Select [HPCode],[HPName] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                    "Where isEnabledClaim = '" + model.isEnabledClaim + "'\r\n";

                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            dtclaim.Load(cmd.ExecuteReader());

                            foreach (DataRow dr in dtclaim.Rows)
                            {
                                HP_ContractsModel hpcontractclaim = new HP_ContractsModel();
                                hpcontractclaim.HPCODE = dr["HPCODE"].ToString();
                                hpcontractclaim.HPNAME = dr["HPNAME"].ToString();
                                ListOfHPCodesclaim.Add(hpcontractclaim);

                            }
                        }
                        if (model.userType == 1)
                        {
                            if (model.isDentist == true)
                            {
                                string contract = "";
                                if (model.contracted)
                                {
                                    contract = $" and isContract = 'true'";
                                }
                                else
                                {
                                    contract = $" and isNotContract = 'true'";
                                }
                                cmd.CommandType = CommandType.Text;
                                string sqlTextToExecute = $"Select [HPCode],[HPName] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                        "Where isEnabledClaim = '" + model.isEnabledClaim + $"' and Dentists = 'true' {contract}\r\n";

                                cmd.Connection = Sqlcon;
                                cmd.CommandText = sqlTextToExecute;

                                dtclaim.Load(cmd.ExecuteReader());
                            }
                            if (model.isSpecialist == true)
                            {
                                string contract = "";
                                if (model.contracted)
                                {
                                    contract = $" and isContract = 'true'";
                                }
                                else
                                {
                                    contract = $" and isNotContract = 'true'";
                                }

                                cmd.CommandType = CommandType.Text;
                                string sqlTextToExecute = $"Select [HPCode],[HPName] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                        "Where isEnabledClaim = '" + model.isEnabledClaim + $"' and Specialists = 'true' {contract}\r\n";

                                cmd.Connection = Sqlcon;
                                cmd.CommandText = sqlTextToExecute;

                                dtclaim.Load(cmd.ExecuteReader());
                            }
                            if (model.isHospital == true)
                            {
                                string contract = "";
                                if (model.contracted)
                                {
                                    contract = $" and isContract = 'true'";
                                }
                                else
                                {
                                    contract = $" and isNotContract = 'true'";
                                }
                                cmd.CommandType = CommandType.Text;
                                string sqlTextToExecute = $"Select [HPCode],[HPName] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                        "Where isEnabledClaim = '" + model.isEnabledClaim + $"' and Hospitals = 'true' {contract}\r\n";

                                cmd.Connection = Sqlcon;
                                cmd.CommandText = sqlTextToExecute;

                                dtclaim.Load(cmd.ExecuteReader());
                            }
                            if (model.isPharmacy == true)
                            {
                                string contract = "";
                                if (model.contracted)
                                {
                                    contract = $" and isContract = 'true'";
                                }
                                else
                                {
                                    contract = $" and isNotContract = 'true'";
                                }
                                cmd.CommandType = CommandType.Text;
                                string sqlTextToExecute = $"Select [HPCode],[HPName] from {PamcPortalDatabase}..[HealthPlanSetup]\r\n" +
                                        "Where isEnabledClaim = '" + model.isEnabledClaim + $"' and Pharmacy = 'true' {contract}\r\n";

                                cmd.Connection = Sqlcon;
                                cmd.CommandText = sqlTextToExecute;

                                dtclaim.Load(cmd.ExecuteReader());
                            }
                            foreach (DataRow dr in dtclaim.Rows)
                            {
                                HP_ContractsModel hpcontractclaim = new HP_ContractsModel();
                                hpcontractclaim.HPCODE = dr["HPCODE"].ToString();
                                hpcontractclaim.HPNAME = dr["HPNAME"].ToString();
                                ListOfHPCodesclaim.Add(hpcontractclaim);

                            }
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return ListOfHPCodesclaim;
        }

        /*Loads all healthplans from the healthplan setup table in the database*/
        [HttpGet]
        [Route("LoadHealthPlans")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<HealthPlanSetupModel> HealthPlanSetup()
        {
            Utillity.CompareHealthPlans(_portalConnectionString, DRCDatabase, PamcPortalDatabase);
            DataTable dt = new DataTable();
            List<HealthPlanSetupModel> ListOfHPCodes = new List<HealthPlanSetupModel>();
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = $"select [HPCode],[HPName],[isEnabledMember],[isEnabledClaim],[Dentists],[Specialists],[isContract],[isNotContract],[membRegistration], [brokerRgistration], year(HPDATE) AS HPDATE  FROM {PamcPortalDatabase}..[HealthPlanSetup]";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            HealthPlanSetupModel healthplansetup = new HealthPlanSetupModel();
                            healthplansetup.HPCode = dr["HPCode"].ToString();
                            healthplansetup.HPName = dr["HPName"].ToString();
                            healthplansetup.isEnabledMember = Convert.ToBoolean(dr["isEnabledMember"]);
                            healthplansetup.isEnabledClaim = Convert.ToBoolean(dr["isEnabledClaim"]);
                            if (dr["Dentists"] != DBNull.Value)
                            {
                                healthplansetup.dentists = Convert.ToBoolean(dr["Dentists"]);
                            }
                            if (dr["Specialists"] != DBNull.Value)
                            {
                                healthplansetup.specialists = Convert.ToBoolean(dr["Specialists"]);
                            }
                            if (dr["isContract"] != DBNull.Value)
                            {
                                healthplansetup.contracted = Convert.ToBoolean(dr["isContract"]);
                            }
                            if (dr["isNotContract"] != DBNull.Value)
                            {
                                healthplansetup.notcontracted = Convert.ToBoolean(dr["isNotContract"]);
                            }
                            if (dr["membRegistration"] != DBNull.Value)
                            {
                                healthplansetup.membRegistration = Convert.ToBoolean(dr["membRegistration"]);
                            }
                            if (dr["brokerRgistration"] != DBNull.Value)
                            {
                                healthplansetup.isEnabledBroker = Convert.ToBoolean(dr["brokerRgistration"]);
                            }
                            //healthplansetup.HPDATE = Convert.ToDateTime(dr["HPDATE"]);
                            healthplansetup.HPDATE = Convert.ToInt32(dr["HPDATE"]);
                            ListOfHPCodes.Add(healthplansetup);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                //throw;
            }

            return ListOfHPCodes;
        }

        /*Updating the health plans*/
        [HttpPost]
        [Route("UpdateHealthPlans")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public HealthPlanSetupModel UpdateHealthPlans([FromBody] HealthPlanSetupModel model)
        {
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        string sqlTextToExecute = $"UPDATE {PamcPortalDatabase}..HealthPlanSetup\r\n" +
                                                  "SET isContract = '" + model.contracted + "', isNotContract = '" + model.notcontracted + "', isEnabledMember = '" + model.isEnabledMember + "', isEnabledClaim = '" + model.isEnabledClaim + "', Dentists = '" + model.dentists + "', Specialists = '" + model.specialists + "'\r\n" +
                                                  "WHERE (HPCode = '" + model.HPCode + "') AND (HPName = '" + model.HPName + "');\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        try
                        {

                            int rowsAffected = cmd.ExecuteNonQuery();
                            if (rowsAffected != 1)
                            {
                                //throw new Exception();
                            }

                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            throw new Exception();

                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return model;
        }

        /*Method that loads remittances for the accounting bureau*/
        [HttpPost]
        [Route("GetAccountingBureauRemittances")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AccountingBureauRemittancesModel> getRemittances([FromBody] LoginDetailsModel model)
        {
            SettingsModel returnedRow = Utillity.GetSpecificSetting("payrunlag", _portalConnectionString);
            SettingsModel returnedremittancemonths = Utillity.GetSpecificSetting("remitlag", _portalConnectionString);

            DataTable dt = new DataTable();
            List<AccountingBureauRemittancesModel> remittanceslist = new List<AccountingBureauRemittancesModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "SELECT  COUNT(ch.VENDORID) AS 'VENDORID', sum(ch.AMOUNT) AS 'AMOUNT', ch.DATEPAID from\r\n" +
                                                    "(select  CHECK_MASTERS.VENDORID, CHECK_MASTERS.AMOUNT, CHECK_MASTERS.DATEPAID\r\n" +
                                                    "FROM CHECK_MASTERS INNER JOIN\r\n" +
                                                    "VEND_MASTERS ON CHECK_MASTERS.VENDORID = VEND_MASTERS.VENDORID \r\n" +
                                                    "WHERE (VEND_MASTERS.BUREAUID = @bureauId) AND (CHECK_MASTERS.DATEPAID BETWEEN DATEADD(Month, - @months, GETDATE()) AND DATEADD(day, @desc, GETDATE()) ) AND CHECK_MASTERS.PAYEE <> '* * * VOID * * *' \r\n" +
                                                    "union all\r\n" +
                                                    "select  vm.VENDORID, cm.NET, cm.DATEPAID\r\n" +
                                                    "FROM VEND_MASTERS vm\r\n" +
                                                    "iNNER JOIN CLAIM_MASTERS cm  ON cm.VENDOR = vm.VENDORID \r\n" +
                                                    "WHERE (vm.BUREAUID = @bureauId) AND (cm.DATEPAID BETWEEN DATEADD(Month, - @months, GETDATE()) AND DATEADD(day, @desc, GETDATE()) ) AND cm.chprefix = 0 and cm.BATCH is not null\r\n" +
                                                    ") ch\r\n" +
                                                    "GROUP BY ch.DATEPAID\r\n" +
                                                    "ORDER BY ch.DATEPAID DESC\r\n";

                        cmd.Parameters.Add("@bureauId", SqlDbType.VarChar).Value = model.bureauId;
                        cmd.Parameters.Add("@months", SqlDbType.Int).Value = returnedremittancemonths.description;
                        cmd.Parameters.Add("@desc", SqlDbType.Int).Value = returnedRow.description;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            AccountingBureauRemittancesModel A = new AccountingBureauRemittancesModel();
                            A.VENDORID = dr["VENDORID"].ToString();
                            A.AMOUNT = Convert.ToSingle(dr["AMOUNT"]);
                            A.DATEPAID = Convert.ToDateTime(dr["DATEPAID"]);
                            remittanceslist.Add(A);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return remittanceslist;
        }

        //Detailed remittance view for bureaus
        [HttpPost]
        [Route("DetailedRemittances")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AccountingBureauRemittancesModel> detailedremittances([FromBody] LoginDetailsModel model)
        {
            DataTable dt = new DataTable();
            List<AccountingBureauRemittancesModel> remittanceslist = new List<AccountingBureauRemittancesModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        string sqlTextToExecute = "select ch.VENDORID,sum(ch.amount) as AMOUNT, ch.DATEPAID,ch.PREFIX,ch.PAYEE from \r\n" +
                                "(SELECT CHECK_MASTERS.VENDORID, CHECK_MASTERS.AMOUNT, CHECK_MASTERS.DATEPAID, CHECK_MASTERS.PREFIX, CHECK_MASTERS.PAYEE\r\n" +
                                "FROM CHECK_MASTERS INNER JOIN\r\n" +
                                "VEND_MASTERS ON CHECK_MASTERS.VENDORID = VEND_MASTERS.VENDORID AND CHECK_MASTERS.VENDORID = VEND_MASTERS.VENDORID AND CHECK_MASTERS.VENDORID = VEND_MASTERS.VENDORID\r\n" +
                                "WHERE (VEND_MASTERS.BUREAUID = '" + model.bureauId + "') AND (CHECK_MASTERS.DATEPAID = '" + model.transactionDate + "') AND CHECK_MASTERS.PAYEE <> '* * * VOID * * *'\r\n" +
                                "union all\r\n" +
                                "select  vm.VENDORID, cm.NET as AMOUNT, cm.DATEPAID,(CASE\r\n" +
                                "    WHEN cm.CHPREFIX = 0 THEN cm.BATCH\r\n" +
                                "    ELSE cm.CHPREFIX\r\n" +
                                "END) as  PREFIX,vm.VENDORNM as PAYEE\r\n" +
                                "FROM VEND_MASTERS vm\r\n" +
                                "iNNER JOIN CLAIM_MASTERS cm  ON cm.VENDOR = vm.VENDORID \r\n" +
                                "WHERE (vm.BUREAUID = '" + model.bureauId + "') AND (cm.DATEPAID  = '" + model.transactionDate + "') AND cm.chprefix = 0 and cm.BATCH is not null) ch\r\n" +
                                "group by ch.VENDORID,ch.DATEPAID,ch.PREFIX,ch.PAYEE\r\n";

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            AccountingBureauRemittancesModel remittance = new AccountingBureauRemittancesModel();
                            remittance.PREFIX = Convert.ToInt32(dr["PREFIX"]);
                            remittance.PAYEE = dr["PAYEE"].ToString();
                            remittance.VENDORID = dr["VENDORID"].ToString();
                            remittance.AMOUNT = Convert.ToSingle(dr["AMOUNT"]);
                            remittance.DATEPAID = Convert.ToDateTime(dr["DATEPAID"]);
                            remittanceslist.Add(remittance);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);

            }
            return remittanceslist;
        }

        /*Method that loads remittances for the Providers*/
        [HttpPost]
        [Route("GetProviderRemittances")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ProviderRemittancesModel> getProviderRemittances([FromBody] LoginDetailsModel model)
        {
            SettingsModel returnedRow = Utillity.GetSpecificSetting("payrunlag_prov", _portalConnectionString);
            SettingsModel returnedremittancemonths = Utillity.GetSpecificSetting("remitlag_prov", _portalConnectionString);

            DataTable dt = new DataTable();

            List<ProviderRemittancesModel> remittanceslist = new List<ProviderRemittancesModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //cmd.CommandType = CommandType.Text;

                        //string sqlTextToExecute = "select PROVID, DATEPAID, SUM(NET) AS NET,PREFIX \r\n" +
                        //        "from (SELECT PROVID, cm.DATEPAID, NET,(CASE\r\n" +
                        //        "WHEN cm.CHPREFIX = 0 THEN cm.BATCH\r\n" +
                        //        "ELSE cm.CHPREFIX\r\n" +
                        //        "END) as  PREFIX\r\n" +
                        //        "FROM  CLAIM_MASTERS  cm\r\n" +
                        //        "inner join CHECK_MASTERS ch on ch.PREFIX = cm.CHPREFIX and ch.CHECKNO = cm.CHECKNO and ch.VOID = 'N'\r\n" +
                        //        "WHERE (VENDOR = '" + model.provID + "') and [status] = '9'  \r\n" +
                        //        "union all\r\n" +
                        //        "SELECT PROVID, cm.DATEPAID, NET,(CASE\r\n" +
                        //        "WHEN cm.CHPREFIX = 0 THEN cm.BATCH\r\n" +
                        //        "ELSE cm.CHPREFIX\r\n" +
                        //        "END) as  PREFIX\r\n" +
                        //        "FROM  CLAIM_MASTERS  cm\r\n" +
                        //        "WHERE (VENDOR = '" + model.provID + "') and [status] = '9'  and CHPREFIX = 0 and BATCH is not null\r\n" +
                        //        ") als\r\n" +
                        //        "GROUP BY PROVID, DATEPAID,PREFIX\r\n" +
                        //        "ORDER BY DATEPAID DESC\r\n";

                        //Jaco 2023-12-06
                        string sqlTextToExecute =
                        " select PROVID, DATEPAID, SUM(NET) AS NET, PREFIX \r\n" +
                        " from( \r\n" +
                        "        SELECT PROVID, cm.DATEPAID, NET, (CASE \r\n" +
                        "                                            WHEN cm.CHPREFIX = 0 THEN cm.BATCH \r\n" +
                        "                                            ELSE cm.CHPREFIX \r\n" +
                        "                                            END \r\n" +
                        "                                         ) as PREFIX \r\n" +
                        "        FROM CLAIM_MASTERS  cm \r\n" +
                        "           inner join CHECK_MASTERS ch on ch.PREFIX = cm.CHPREFIX and ch.CHECKNO = cm.CHECKNO and ch.VOID = 'N' \r\n" +
                        "        WHERE(VENDOR = @provId) and[status] = '9'  AND(ch.DATEPAID BETWEEN DATEADD(Month, -@months, GETDATE()) AND DATEADD(day, @desc, GETDATE())) AND ch.PAYEE <> '* * * VOID * * *' \r\n" +

                        "        union all \r\n" +

                        "        SELECT PROVID, cm.DATEPAID, NET,(CASE \r\n" +
                        "                                            WHEN cm.CHPREFIX = 0 THEN cm.BATCH \r\n" +
                        "                                            ELSE cm.CHPREFIX \r\n" +
                        "                                         END) as PREFIX \r\n" +
                        "        FROM CLAIM_MASTERS  cm \r\n" +
                        "        WHERE(VENDOR = @provId) and[status] = '9'  and CHPREFIX = 0 and BATCH is not null AND(cm.DATEPAID BETWEEN DATEADD(Month, -@months, GETDATE()) AND DATEADD(day, @desc, GETDATE())) \r\n" +
                        "	 ) als \r\n" +
                        " GROUP BY PROVID, DATEPAID, PREFIX \r\n" +
                        " ORDER BY DATEPAID DESC \r\n";

                        cmd.Parameters.Add("@provId", SqlDbType.VarChar).Value = model.provID;
                        cmd.Parameters.Add("@months", SqlDbType.Int).Value = returnedremittancemonths.description;
                        cmd.Parameters.Add("@desc", SqlDbType.Int).Value = returnedRow.description;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            ProviderRemittancesModel remittance = new ProviderRemittancesModel();
                            remittance.PROVID = dr["PROVID"].ToString();
                            remittance.DATEPAID = Convert.ToDateTime(dr["DATEPAID"]);
                            remittance.NET = Convert.ToDouble(dr["NET"]);
                            remittance.CHPREFIX = Convert.ToInt32(dr["PREFIX"]);
                            remittanceslist.Add(remittance);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }
            return remittanceslist;
        }

        /*Method that will get detailed remiitance info for providers*/
        [HttpPost]
        [Route("DetailedProviderRemittances")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DetailedProviderRemittanceModel> detailedproviderremittances([FromBody] LoginDetailsModel model)
        {
            DataTable dt = new DataTable();
            List<DetailedProviderRemittanceModel> remittanceslist = new List<DetailedProviderRemittanceModel>();
            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string sqlTextToExecute = "";

                        if (model.provID != model.loginProvId)
                        {                           
                            sqlTextToExecute = "SELECT [CLAIMNO],PROVCLAIM,PROVID,MEMBID,BILLED,NET,DATEFROM,DATEPAID\r\n" +
                                                      "FROM " + this.DRCDatabase + $".[dbo].[CLAIM_MASTERS]\r\n" +
                                                       "where DATEPAID = '" + model.transactionDate + "' and (PROVID = '" + model.provID + "') and (VENDOR = '" + model.loginProvId + "') and [status] = '9' and (CHPREFIX = '" + model.chprefix + "' or isnull(BATCH,'') = '" + model.chprefix + "')\r\n";
                        }
                        else
                        {
                            sqlTextToExecute = "SELECT [CLAIMNO],PROVCLAIM,PROVID,MEMBID,BILLED,NET,DATEFROM,DATEPAID\r\n" +
                                                  "FROM " + this.DRCDatabase + $".[dbo].[CLAIM_MASTERS]\r\n" +
                                                   "where DATEPAID = '" + model.transactionDate + "' and (PROVID = '" + model.provID + "') and [status] = '9' and (CHPREFIX = '" + model.chprefix + "' or isnull(BATCH,'') = '" + model.chprefix + "')\r\n";
                        }



                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;

                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            DetailedProviderRemittanceModel detailedproviderremittance = new DetailedProviderRemittanceModel();
                            detailedproviderremittance.CLAIMNO = dr["CLAIMNO"].ToString();
                            detailedproviderremittance.MEMBID = dr["MEMBID"].ToString();
                            detailedproviderremittance.DATEFROM = Convert.ToDateTime(dr["DATEFROM"]);
                            detailedproviderremittance.BILLED = Convert.ToDouble(dr["BILLED"]);
                            detailedproviderremittance.NET = Convert.ToDouble(dr["NET"]);
                            detailedproviderremittance.DATEPAID = Convert.ToDateTime(dr["DATEPAID"]);
                            remittanceslist.Add(detailedproviderremittance);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return remittanceslist;
        }

        /*Loads all providers for the logged in bureau*/
        [HttpPost]
        [Route("getProviders")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AccountingBureauProvidersModel> LinkedProviders([FromBody] LoginDetailsModel model)
        {
            DataTable dt = new DataTable();
            List<AccountingBureauProvidersModel> ListOfProviders = new List<AccountingBureauProvidersModel>();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select [VENDORID],\r\n" +
                                                  "[VENDORNM]\r\n" +
                                                  "from VEND_MASTERS\r\n" +
                                                   "where BUREAUID = @bureauId\r\n";
                        cmd.Parameters.Add("@bureauId", SqlDbType.VarChar).Value = model.bureauId;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;


                        dt.Load(cmd.ExecuteReader());


                        foreach (DataRow dr in dt.Rows)
                        {
                            AccountingBureauProvidersModel Provider = new AccountingBureauProvidersModel();
                            Provider.VENDORID = dr["VENDORID"].ToString();
                            Provider.VENDORNM = dr["VENDORNM"].ToString();
                            ListOfProviders.Add(Provider);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return ListOfProviders;
        }


        /*Gets members linked to broker*/

        [HttpPost]
        [Route("getMembers")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<LinkedBrokerMembersModel> LinkedMembers([FromBody] LoginDetailsModel model)
        {
            DataTable dt = new DataTable();
            List<LinkedBrokerMembersModel> listOfmembers = new List<LinkedBrokerMembersModel>();
            try
            {

                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToExecute = "select * from MEMB_MASTERS" +
                                                  "where BROKERID = " + model.brokerId + " and [RLSHIP] = 1"; ;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;


                        dt.Load(cmd.ExecuteReader());


                        foreach (DataRow dr in dt.Rows)
                        {
                            LinkedBrokerMembersModel member = new LinkedBrokerMembersModel();
                            member.memberNumber = dr["SUBSSN"].ToString();
                            member.lastName = dr["LASTNM"].ToString();
                            member.firstName = dr["FIRSTNM"].ToString();
                            member.email = dr["EMAIL"].ToString();
                            member.phoneNumber = dr["CELL"].ToString();
                            listOfmembers.Add(member);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return listOfmembers;
        }

        //Will download txt/pdf remittance
        [HttpPost]
        [Route("DownloadRemitance")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ReturnedBureauInformationModel downloadremittance([FromBody]BureauRemittanceExtractModel model)
        {
            string OperationDesc = "";
            OperationLog Operationlog = new OperationLog();
            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : DownloadRemittance---------------\r\n";
            string paidDate = model.PaidDate.Date.ToString("yyyy/MM/dd");
            string fileName = BuildRemitZipFileName(paidDate, model.File, true);
            string contentRoot = _env.ContentRootPath;


            if (model.File == true)
            {
                OperationDesc = "Text Remittance Downloaded";
            }
            else
            {
                OperationDesc = "PDF Remittance Downloaded";
            }
            Operationlog.LogRemittance(OperationDesc, model.Username, model.PaidDate, _portalConnectionString);


            ReturnedBureauInformationModel returnedfileinfo = new ReturnedBureauInformationModel();

            string rootFolder = _env.WebRootPath;

            //returnedfileinfo.ParsedDate = long.Parse(model.Date.ToString("yyyyMMdd"));
            returnedfileinfo.ParsedDate = model.Date.ToString("yyyyMMddHHmmssfff"); // Jaco Users should not share folders

            string RemittanceFolder = returnedfileinfo.ParsedDate.ToString();

            DirectoryInfo RemittanceFolderDir = new DirectoryInfo(RemittanceFolder);
            DirectoryInfo content = new DirectoryInfo(contentRoot + $"\\ClientApp\\src\\assets\\Remittances");
            if (content.Exists == false)
            {
                content = new DirectoryInfo(contentRoot + $"\\ClientApp\\dist\\assets\\Remittances");
            }

            //Clear all folders and files older than 24hours
            try
            {
                DirectoryInfo dirr = new DirectoryInfo(content.FullName);

                foreach (var folder in dirr.GetDirectories())
                {
                    if (folder.CreationTime < DateTime.Now.AddDays(-1))
                    {
                        foreach (var file in folder.GetFiles())
                        {
                            file.Delete();
                        }

                        folder.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                // Don't let this break the process
            }

            DirectoryInfo RootDir = new DirectoryInfo($"{content.FullName}\\{RemittanceFolderDir}");

            OperationDesc = "The full path" + RootDir;

            Operationlog.ErrorLog(OperationDesc, model.Username, _portalConnectionString);

            if (!RootDir.Exists)
            {
                RootDir.Create();
                OperationDesc = "Folder created";

                Operationlog.ErrorLog(OperationDesc, model.Username, _portalConnectionString);
            }
            else
            {
                OperationDesc = "Folder already created";

                Operationlog.ErrorLog(OperationDesc, model.Username, _portalConnectionString);
            }
            foreach (var file in RootDir.GetFiles())
            {
                file.Delete();
            }

            try
            {
                string url = _RemmitanceWebSvcConnection;

                byte[] fileInfoarray;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(1, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;

                RemittanceService.RemittanceServiceSoapClient remittanceextractor = new RemittanceService.RemittanceServiceSoapClient(binding, address);

                var r = remittanceextractor.RemitancePDfDownloadAsync(model.BureauID, paidDate, model.ProvID, model.File, null, 0, model.ProvID);

                //var r = remittanceextractor.RemitancePDfDownloadAsync(2, "2019-04-12T00:00:00", "", false,"");

                r.Wait();

                fileInfoarray = r.Result.Body.RemitancePDfDownloadResult;

                returnedfileinfo.FileName = fileName;
                DirectoryInfo d = new DirectoryInfo(RootDir.ToString());

                FileInfo f = new FileInfo(d.ToString() + "\\" + returnedfileinfo.FileName);

                System.IO.File.WriteAllBytes(f.FullName, fileInfoarray);

                // Check if file exist (slow servers take a while to write the file)
                for (int i = 0; i < 12; i++)
                {
                    if (!System.IO.File.Exists(f.FullName))
                    {
                        System.Threading.Thread.Sleep(1000);
                   }
                    else
                    {
                        break; // File exists so exit for loop
                    }
                }
            }
            catch (Exception ex)
            {
                LogRemitError(ex.Message, model.Username, "DownloadRemitance", $"{model.BureauID}, {paidDate}, {model.ProvID}, {model.File}, null");
                LogError(ex.Message, ex.StackTrace);
                Console.WriteLine(ex.StackTrace);
                throw;
            }
            return returnedfileinfo;
        }


        [HttpPost]
        [Route("DownloadClaimRemitance")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ReturnedBureauInformationModel downloadclaimremittance([FromBody]ClaimRemittance model)
        {
            string paidDate = model.PaidDate.Date.ToString("yyyy/MM/dd");
            string fileName = BuildRemitZipFileName(paidDate, model.File, false);
            string OperationDesc = "";
            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : DownloadRemittance---------------\r\n";

            if (model.File == true)
            {
                OperationDesc = "Text Remittance Downloaded";
            }
            else
            {
                OperationDesc = "PDF Remittance Downloaded";
            }
            if (model.paidTo == "P")
            {
                model.memProv = true;
            }
            else
            {
                model.memProv = false;
            }

            OperationLog Operationlog = new OperationLog();
            Operationlog.LogRemittance(OperationDesc, model.Username, model.PaidDate, _portalConnectionString);

            ReturnedBureauInformationModel returnedfileinfo = new ReturnedBureauInformationModel();

            string rootFolder = _env.WebRootPath;
            string contentRoot = _env.ContentRootPath;

            //returnedfileinfo.ParsedDate = long.Parse(model.Date.ToString("yyyyMMdd"));
            returnedfileinfo.ParsedDate = model.Date.ToString("yyyyMMddHHmmssfff"); // Jaco Users should not share folders

            string RemittanceFolder =  returnedfileinfo.ParsedDate.ToString();

            DirectoryInfo RemittanceFolderDir = new DirectoryInfo(RemittanceFolder);

            DirectoryInfo content = new DirectoryInfo(contentRoot + $"\\ClientApp\\src\\assets\\Remittances");
            if (content.Exists == false)
            {
                content = new DirectoryInfo(contentRoot + $"\\ClientApp\\dist\\assets\\Remittances");
            }

            //Clear all folders and files older than 24hours
            try
            {
                DirectoryInfo dirr = new DirectoryInfo(content.FullName);

                foreach (var folder in dirr.GetDirectories())
                {
                    if (folder.CreationTime < DateTime.Now.AddDays(-1))
                    {
                        foreach (var file in folder.GetFiles())
                        {
                            file.Delete();
                        }

                        folder.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                // Don't let this break the process
            }

            DirectoryInfo RootDir = new DirectoryInfo($"{content.FullName}\\{RemittanceFolderDir}");
            try
            {

                if (!RootDir.Exists)
                {
                    RootDir.Create();
                }
                foreach (var file in RootDir.GetFiles())
                {
                    file.Delete();
                }

                string url = _RemmitanceWebSvcConnection;

                byte[] fileInfoarray;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 300, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;

                RemittanceService.RemittanceServiceSoapClient remittanceextractor = new RemittanceService.RemittanceServiceSoapClient(binding, address);
                dynamic r;

                if (model.UserType == 3 && model.isMember == true)
                {
                    r = remittanceextractor.RemitanceClientClaimDownloadAsync(model.File, model.ClaimNumber, model.memProv);
                }
                else
                {
                    r = remittanceextractor.RemitanceClientClaimDownloadAsync(model.File, model.ClaimNumber, model.memProv);
                }

                r.Wait();

                fileInfoarray = r.Result.Body.RemitanceClientClaimDownloadResult;

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Byte Array Recieved - {fileInfoarray.Length}\r\n";

                returnedfileinfo.FileName = fileName;

                DirectoryInfo d = new DirectoryInfo(RootDir.ToString());
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :File Directory - {d}\r\n";

                FileInfo f = new FileInfo(d.ToString() + "\\" + returnedfileinfo.FileName);
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Filename - {f}\r\n";

                System.IO.File.WriteAllBytes(f.FullName, fileInfoarray);

                // Check if file exist (slow servers take a while to write the file)
                for (int i = 0; i < 6; i++)
                {
                    if (!System.IO.File.Exists(f.FullName))
                    {                        
                        System.Threading.Thread.Sleep(500);
                    }
                    else
                    {
                        break; // File exists so exit for loop
                    }
                }               

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                LogRemitError(ex.Message, model.Username, "DownloadClaimRemitance", $"{model.File}, {model.ClaimNumber}, {model.memProv}");
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :general Error - {ex.Message}\r\n{ex.StackTrace}\r\n";
                Console.WriteLine(ex.StackTrace);
                throw;
            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} Member Remittance Method Done\r\n";
            }
            return returnedfileinfo;
        }


        [HttpPost]
        [Route("DownloadProviderRemitance")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ProvRemitDownloadInfoModel downloadproviderremittance([FromBody]ProviderRemittanceExtractModel model)
        {
            string paidDate = model.PaidDate.Date.ToString("yyyy/MM/dd");
            string fileName = BuildRemitZipFileName(paidDate, model.File, false);

            string OperationDesc = "";

            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : DownloadProviderRemittance---------------\r\n";

            if (model.File == true)
            {
                OperationDesc = "Text Remittance Downloaded";
            }
            else
            {
                OperationDesc = "PDF Remittance Downloaded";
            }

            OperationLog operationLog = new OperationLog();
            operationLog.LogRemittance(OperationDesc, model.Username, model.PaidDate, _portalConnectionString);

            ProvRemitDownloadInfoModel returnedfileinfo = new ProvRemitDownloadInfoModel();

            string rootFolder = _env.WebRootPath;
            string contentRoot = _env.ContentRootPath;

            //returnedfileinfo.ParsedDate = long.Parse(model.Date.ToString("yyyyMMdd"));
            returnedfileinfo.ParsedDate = model.Date.ToString("yyyyMMddHHmmssfff"); // Jaco Users should not share folders

            string RemittanceFolder = returnedfileinfo.ParsedDate.ToString();

            DirectoryInfo RemittanceFolderDir = new DirectoryInfo(RemittanceFolder);

            DirectoryInfo content = new DirectoryInfo(contentRoot + $"\\ClientApp\\src\\assets\\Remittances");
            if (content.Exists == false)
            {
                content = new DirectoryInfo(contentRoot + $"\\ClientApp\\dist\\assets\\Remittances");
            }

            //Clear all folders and files older than 24hours
            try
            {                
                DirectoryInfo dirr = new DirectoryInfo(content.FullName);

                foreach (var folder in dirr.GetDirectories())
                {
                    if (folder.CreationTime < DateTime.Now.AddDays(-1))
                    {
                        foreach (var file in folder.GetFiles())
                        {
                            file.Delete();
                        }

                        folder.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
               // Don't let this break the process
            }

            DirectoryInfo RootDir = new DirectoryInfo($"{content.FullName}\\{RemittanceFolderDir}");

            if (!RootDir.Exists)
            {
                RootDir.Create();
            }
            foreach (var file in RootDir.GetFiles())
            {
                file.Delete();
            }

            try
            {
                string url = _RemmitanceWebSvcConnection;

                byte[] fileInfoarray;
                EndpointAddress address = new EndpointAddress(url);
                BasicHttpBinding binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                var time = new TimeSpan(0, 30, 0);
                binding.CloseTimeout = time;
                binding.OpenTimeout = time;
                binding.ReceiveTimeout = time;
                binding.SendTimeout = time;

                RemittanceService.RemittanceServiceSoapClient remittanceextractor = new RemittanceService.RemittanceServiceSoapClient(binding, address);

                var r = remittanceextractor.RemitancePDfDownloadAsync(0, paidDate, model.ProvID, model.File, null, model.prefix, model.loginProvId);
                //var r = remittanceextractor.RemitancePDfDownloadAsync(bureauID, paidDate, providerId, false);

                r.Wait();

                fileInfoarray = r.Result.Body.RemitancePDfDownloadResult;

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Byte Array Recieved - {fileInfoarray.Length}\r\n";

                returnedfileinfo.FileName = fileName;
                DirectoryInfo d = new DirectoryInfo(RootDir.ToString());

                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :File Directory - {d}\r\n";

                FileInfo f = new FileInfo(d.ToString() + "\\" + returnedfileinfo.FileName);
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Filename - {f}\r\n";

                System.IO.File.WriteAllBytes(f.FullName, fileInfoarray);

                // Check if file exist (slow servers take a while to write the file)
                for (int i = 0; i < 6; i++)
                {
                    if (!System.IO.File.Exists(f.FullName))
                    {
                        System.Threading.Thread.Sleep(500);
                    }
                    else
                    {
                        break; // File exists so exit for loop
                    }
                }
            }
            catch (Exception e)
            {
                //try
                //{
                //    string url = _RemmitanceWebSvcConnection;


                //    byte[] fileInfoarray;
                //    EndpointAddress address = new EndpointAddress(url);
                //    BasicHttpBinding binding = new BasicHttpBinding();
                //    binding.MaxReceivedMessageSize = 2147483647;
                //    var time = new TimeSpan(0, 30, 0);
                //    binding.CloseTimeout = time;
                //    binding.OpenTimeout = time;
                //    binding.ReceiveTimeout = time;
                //    binding.SendTimeout = time;





                //    RemittanceService.RemittanceServiceSoapClient remittanceextractor = new RemittanceService.RemittanceServiceSoapClient(binding, address);

                //    var r = remittanceextractor.RemitancePDfDownloadAsync(0, paidDate, model.loginProvId, model.File, null, model.prefix, model.loginProvId);
                //    //var r = remittanceextractor.RemitancePDfDownloadAsync(bureauID, paidDate, providerId, false);

                //    r.Wait();


                //    fileInfoarray = r.Result.Body.RemitancePDfDownloadResult;

                //    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Byte Array Recieved - {fileInfoarray.Length}\r\n";

                //    returnedfileinfo.FileName = fileName;
                //    DirectoryInfo d = new DirectoryInfo(RootDir.ToString());

                //    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :File Directory - {d}\r\n";

                //    FileInfo f = new FileInfo(d.ToString() + "\\" + returnedfileinfo.FileName);
                //    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Filename - {f}\r\n";

                //    System.IO.File.WriteAllBytes(f.FullName, fileInfoarray);
                //}
                //catch (Exception ex)
                //{

                //LogError(ex.Message, ex.StackTrace);
                //LogRemitError(ex.Message, model.Username, "DownloadProviderRemitance", $"{0} ,{paidDate}, {model.ProvID}, {model.File}, {null}");
                //log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Error caught in the provider download method - {ex.Message}\r\n{ex.StackTrace}\r\n";
                //Console.WriteLine(ex.StackTrace);
                //throw;

                LogError(e.Message, e.StackTrace);
                LogRemitError(e.Message, model.Username, "DownloadProviderRemitance", $"{0} ,{paidDate}, {model.ProvID}, {model.File}, {null}");
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Error caught in the provider download method - {e.Message}\r\n{e.StackTrace}\r\n";
                Console.WriteLine(e.StackTrace);
                throw;
                //}

            }
            finally
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} Provider Remittance Method Done\r\n";

            }
            return returnedfileinfo;
        }

        /*Will be called when a user searches for a claim*/
        [HttpPost]
        [Route("MemberClaimSearch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimSearchModel> MemberClaimSearch([FromBody] ClaimSearchModel model)
        {

            OperationLog operationLog = new OperationLog();
            operationLog.LogClaim("Claim Search", DateTime.Now, model.userName, model.practice, _portalConnectionString);

            SettingsModel returnedClaimlag = Utillity.GetSpecificSetting("Claimlag", _portalConnectionString);
            SettingsModel returnInprogressToggle = new SettingsModel();

            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : MemberClaimSearch---------------\r\n";

            log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :ClaimSearchModel values  \r\n {model.practice} \r\n {model.doctor}\r\n{model.memberNo}\r\n{model.dependent}\r\n{model.memberName}\r\n{model.plan}\r\n{model.receivedDate}\r\n{model.serviceDate}\r\n{model.reversedDate}\r\n{model.claimNo}\r\n{model.charged}\r\n{model.paid}\r\n{model.accountNo}\r\n{model.datePaid}\r\n{model.checkNo}\r\n{model.isPaid}\r\n{model.status}\r\n{model.paidTo}";

            if (model.userType == 1)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("ProviderInProgressSwitch", _portalConnectionString);
            }

            if (model.userType == 2)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("BureauInProgressSwitch", _portalConnectionString);
            }
            if (model.userType == 3)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("AdministratorInProgressSwitch", _portalConnectionString);
            }

            if (model.paidTo == "")
            {
                model.paidTo = null;
            }

            DataTable dt = new DataTable();
            List<ClaimSearchModel> ListOfClaims = new List<ClaimSearchModel>();
            string[] lobcodeList;
            StringBuilder sb = new StringBuilder();

            if (model.plan == "000")
            {
                model.plan = "";
            }
            if (model.lobCode == "")
            {
                model.lobCode = null;
            }

            if (model.lobCode != null)
            {
                lobcodeList = model.lobCode.Split(',');
                foreach (var code in lobcodeList)
                {
                    string x = "'" + code + "'" + ",";
                    sb.Append(x);
                }
                string lobCode = sb.ToString();

                model.lobCode = lobCode.Remove(lobCode.Length - 1);
            }

            try
            {
                if (model.serviceDate != null)
                {
                    model.serviceDate = model.serviceDate.Replace("-", "/");
                    model.datePaid = model.datePaid.Replace("-", "/");

                }
            }
            catch (Exception)
            {
            }

            #region Build WhereClause 

            string WhereClause = "";

            if (model.plan != "")
            {
                WhereClause = "where [Plan] ='" + model.plan + "'";
            }
            if (model.memberNo != "")
            {
                if (WhereClause != "")
                {
                    WhereClause = WhereClause + " AND " + "([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";
                }
                else
                {
                    WhereClause = "WHERE ([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";
                }
            }
            if (model.serviceDate != "" && model.serviceDate != null)
            {
                if (WhereClause != "")
                {
                    WhereClause = WhereClause + " AND " + "[Service Date] = '" + model.serviceDate + "'";
                }
                else
                {
                    WhereClause = "where [Service Date] ='" + model.serviceDate + "'";
                }
            }
            if (model.practice != "")
            {
                if (WhereClause != "")
                {
                    WhereClause = WhereClause + " AND " + "[Practice] = '" + model.practice + "'";
                }
                else
                {
                    WhereClause = "where [Practice] ='" + model.practice + "'";
                }
            }
            if (model.datePaid != null)
            {
                if (WhereClause != "")
                {
                    WhereClause = WhereClause + " AND " + "[Date Paid] ='" + model.datePaid + "'";
                }
                else
                {
                    WhereClause = "where [Date Paid] = '" + model.datePaid + "'";
                }
            }
            if (model.lobCode != null)
            {
                if (model.lobCode.Length > 1)
                {
                    if (WhereClause != "")
                    {
                        WhereClause = WhereClause + " AND " + "cm.[LOBCODE] in (" + model.lobCode + ")";
                    }
                    else
                    {
                        WhereClause = "where cm.[LOBCODE] in (" + model.lobCode + ")";
                    }
                }
                else
                {
                    if (WhereClause != "")
                    {
                        WhereClause = WhereClause + " AND " + "cm.[LOBCODE] ='" + model.lobCode + "'";
                    }
                    else
                    {
                        WhereClause = "where cm.[LOBCODE] = '" + model.lobCode + "'";
                    }
                }

            }

            log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Member Search Where Clause - {WhereClause}\r\n";
            #endregion endregion


            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :dba connection member claim search  - {_portalConnectionString}\r\n";
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;

                        // Jaco comment out 2023-09-20 Databases hardcoded in string
                        //string sqlTextToExecute = " declare @membid varchar(20)\r\n" +
                        //                            "set @membid = '" + model.memberNo + "'\r\n" +
                        //                            "SELECT CASE when PAY.CLAIMNO is null THEN 'NO' ELSE 'YES' END AS isPaid, MEMBERSHIP.OPT,\r\n" +
                        //                            "cm.Practice, cm.Doctor, cm.[Family Number], RIGHT([Member No],2) as DependentCode,  cm.[Member No], " +
                        //                            "op.HPNAME, cm.[Member Name] ,cm.[Received Date], cm.[Service Date],PAY.DATETO," +
                        //                            " CONVERT(varchar, cm.REVERSEDATE, 111) as 'Reversed Date', " +
                        //                            "cm.[Claim No] , cm.Charged, cm.Paid, cm.[Paid To],cm.[Account No], cm.[Date Paid], cm.CHECKNO,  op.ezHpCode, " +
                        //                            "op.MemberPrefix, " +
                        //                            "case op.MemberPrefix when null then 0 else len(memberprefix) end as prefixlength,cm.STATUS,csd.[DESC], " +
                        //                            "CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No],op.MemberPrefix,'')END " +
                        //                            "AS MEMB,ISNULL(pm.FIRSTNAME,'') AS PROVNAME,MEMBERSHIP.FIRSTNM,MEMBERSHIP.LASTNM , pm.LASTNAME AS PROVSURNAME\r\n" +
                        //                            $"from CLAIMS_HEADER cm INNER JOIN {DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.Practice INNER JOIN MEMBERSHIP ON cm.[Member No] = MEMBERSHIP.MEMBID\r\n" +
                        //                            "inner join \r\n" +
                        //                            "(\r\n" +
                        //                            "select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix,ezOpt \r\n" +
                        //                            "from [Reporting].[dbo].[OxygenBenefitOptions] inner join HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\r\n" +
                        //                            ") op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt\r\n" +
                        //                            $"INNER JOIN {PamcPortalDatabase}.dbo.CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS " +
                        //                            "LEFT OUTER JOIN\r\n" +
                        //                            "(\r\n" +
                        //                            "SELECT CM.CLAIMNO,CH.CHECKNO, cm.STATUS,cm.DATETO FROM " + this.DRCDatabase + $"..CLAIM_MASTERS CM INNER " +
                        //                            $"JOIN " + this.DRCDatabase + $"..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX\r\n" +
                        //                            ") PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO\r\n" +
                        //                            WhereClause + "AND CM.[Service Date] >= DATEADD(Month, - " + returnedClaimlag.description + ", GETDATE()) order by cm.[Service Date] desc --24\r\n";

                        // Jaco 2023-09-20
                        string sqlTextToExecute =
                                                    $" declare @membid varchar(20) \n " + //Jaco 2023-11-24 tested with scheme login
                                                    $" set @membid = '{model.memberNo}' \n " +

                                                    $" SELECT CASE when PAY.CLAIMNO is null THEN 'NO' ELSE 'YES' END AS isPaid,  \n " +
                                                    $"     cm.OPT, \n " +
                                                    $"     cm.Practice,  \n " +
                                                    $"     cm.Doctor,  \n " +
                                                    $" 	   cm.[Family Number],  \n " +
                                                    $" 	   RIGHT([Member No],2) as DependentCode,   \n " +
                                                    $" 	   cm.[Member No],  \n " +
                                                    $" 	   op.HPNAME,  \n " +
                                                    $" 	   cm.[Member Name], \n " +
                                                    $" 	   cm.[Received Date],  \n " +
                                                    $" 	   cm.[Service Date],  \n " +
                                                    //$"     PAY.DATETO,  \n " +
                                                    $"     cm.[Service Date To], \n " +
                                                    $" 	   CONVERT(varchar, cm.REVERSEDATE, 111) as 'Reversed Date',  \n " +
                                                    $" 	   cm.[Claim No] ,  \n " +
                                                    $" 	   cm.Charged,  \n " +
                                                    $" 	   cm.Paid,  \n " +
                                                    $" 	   cm.[Paid To], \n " +
                                                    $" 	   cm.[Account No],  \n " +
                                                    $" 	   cm.[Date Paid],  \n " +
                                                    $" 	   cm.CHECKNO,   \n " +
                                                    $" 	   op.ezHpCode,  \n " +
                                                    $" 	   op.MemberPrefix,  \n " +
                                                    $" 	   case op.MemberPrefix when null then 0 else len(memberprefix) end as prefixlength, \n " +
                                                    $" 	   cm.STATUS,csd.[DESC],  \n " +
                                                    $" 	   CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No], op.MemberPrefix,'')END AS MEMB, \n " +
                                                    $" 	   ISNULL(pm.FIRSTNAME, '') AS PROVNAME, \n " +
                                                    $"     mm.FIRSTNM, \n " +
                                                    $" 	   mm.LASTNM,  \n " +
                                                    $" 	   pm.LASTNAME AS PROVSURNAME \n " +
                                                    $" FROM {PamcPortalDatabase}..CLAIMS_HEADER cm \n " +
                                                    $"         INNER JOIN {DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.Practice \n " +
                                                    $"         INNER JOIN {DRCDatabase}..MEMB_MASTERS mm ON cm.[Member No] = mm.MEMBID \n " +
                                                    $"         INNER JOIN( \n " +
                                                    $"                         select DISTINCT ezHpCode, hp_c.HPNAME, MemberPrefix, ezOpt \n " +
                                                    $"                         from {ReportingDatabase}..OxygenBenefitOptions \n " +
                                                    $"                             inner join {DRCDatabase}..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode \n " +
                                                    $" 					) op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt \n " +
                                                    $"         INNER JOIN {PamcPortalDatabase}..CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS \n " +
                                                    $"         LEFT OUTER JOIN( \n " +
                                                    $"                             SELECT CM.CLAIMNO, CH.CHECKNO, cm.STATUS, cm.DATETO FROM {DRCDatabase}..CLAIM_MASTERS CM \n " +
                                                    $"                                 INNER JOIN {DRCDatabase}..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX \n " +
                                                    $"                         ) PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO  \n " +
                                                    $" {WhereClause} \n" +
                                                    $" AND CM.[Service Date] >= DATEADD(Month, -{returnedClaimlag.description}, GETDATE()) \n " +
                                                    $" order by cm.[Service Date] desc --24  ";

                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :SQL execute command - {sqlTextToExecute}\r\n";

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        cmd.CommandTimeout = 6000;
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            ClaimSearchModel claim = new ClaimSearchModel();
                            claim.practice = dr["Practice"].ToString();
                            claim.doctor = dr["Doctor"].ToString();
                            claim.memberNo = dr["MEMB"].ToString();
                            claim.dependent = dr["DependentCode"].ToString();
                            claim.memberFirstName = dr["FIRSTNM"].ToString();
                            claim.memberName = dr["Member Name"].ToString();
                            claim.plan = dr["HPNAME"].ToString();
                            claim.receivedDate = dr["Received Date"].ToString().Replace('/', '-');
                            claim.serviceDate = dr["Service Date"].ToString().Replace('/', '-');
                            claim.serviceDateFrom = dr["Service Date"].ToString().Replace('/', '-');
                            //claim.serviceDateTo = dr["DATETO"].ToString().Replace('/', '-').Replace("00:00:00.000", "").Replace("00:00:00", "");
                            claim.serviceDateTo = dr["Service Date To"].ToString().Replace('/', '-');
                            claim.opt = dr["OPT"].ToString();
                            if (dr["Reversed Date"] != DBNull.Value)
                            {
                                claim.reversedDate = dr["Reversed Date"].ToString();
                            }
                            claim.claimNo = dr["Claim No"].ToString();
                            claim.charged = dr["Charged"].ToString();
                            claim.paid = dr["Paid"].ToString();
                            claim.datePaid = dr["Date Paid"].ToString().Replace('/', '-');
                            if (returnInprogressToggle.enabled == true)
                            {
                                if (claim.datePaid == "In Process")
                                {
                                    if (model.userType != 3)
                                    {
                                        claim.paid = "In Process";
                                    }
                                }
                            }
                            claim.accountNo = dr["Account No"].ToString();
                            if (claim.datePaid != "In Process")
                            {
                                claim.datePaid = claim.datePaid.Replace("/", "-");
                            }
                            claim.provname = dr["PROVNAME"].ToString() + " " + dr["PROVSURNAME"].ToString();
                            claim.checkNo = Convert.ToInt32(dr["CHECKNO"]);
                            claim.isPaid = dr["isPaid"].ToString();
                            claim.status = dr["STATUS"].ToString() + "  (" + dr["DESC"].ToString() + ")";
                            claim.paidTo = dr["Paid To"].ToString();
                            ListOfClaims.Add(claim);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex.Message, ex.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :exception caught in the member search claim method - {ex.Message}\r\n{ex.StackTrace}\r\n";
                    throw;
                }
                finally
                {
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} Member Claim Search Method Done\r\n";

                }
                Sqlcon.Close();
            }
            return ListOfClaims;
        }


        [HttpPost]
        [Route("ProviderClaimSearch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimSearchModel> ProviderClaimSearch([FromBody] ClaimSearchModel model)
        {
            OperationLog operationLog = new OperationLog();
            operationLog.LogClaim("Claim Search", DateTime.Now, model.userName, model.practice, _portalConnectionString);

            SettingsModel returnedClaimlag = Utillity.GetSpecificSetting("Claimlag", _portalConnectionString);
            SettingsModel returnInprogressToggle = new SettingsModel();



            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : ProviderClaimSearch---------------\r\n";


            if (model.userType == 1)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("ProviderInProgressSwitch", _portalConnectionString);
            }

            if (model.userType == 2)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("BureauInProgressSwitch", _portalConnectionString);
            }
            if (model.userType == 3)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("AdministratorInProgressSwitch", _portalConnectionString);
            }

            if (model.paidTo == "")
            {
                model.paidTo = null;
            }

            DataTable dtble = new DataTable();
            DataTable dt = new DataTable();
            List<ClaimSearchModel> ListOfClaims = new List<ClaimSearchModel>();
            string[] lobcodeList;
            StringBuilder sb = new StringBuilder();

            if (model.plan == "000")
            {

                model.plan = "";
            }
            if (model.lobCode == "")
            {
                model.lobCode = null;
            }

            if (model.lobCode != null)
            {
                lobcodeList = model.lobCode.Split(',');
                foreach (var code in lobcodeList)
                {
                    string x = "'" + code + "'" + ",";
                    sb.Append(x);
                }
                string lobCode = sb.ToString();

                model.lobCode = lobCode.Remove(lobCode.Length - 1);
            }

            try
            {
                if (model.serviceDate != null)
                {
                    model.serviceDate = model.serviceDate.Replace("-", "/");
                    model.datePaid = model.datePaid.Replace("-", "/");
                }

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }



            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :dba connection provider claim search  - {_portalConnectionString}\r\n";
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region Build WhereClause 
                        if (model.memberNo != "")
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = model.memberNo;
                        }
                        else
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.plan != "")
                        {
                            cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = model.plan;
                        }
                        else
                        {
                            cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = DBNull.Value;
                        }
                        if (model.serviceDate != "" && model.serviceDate != null)
                        {
                            cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = model.serviceDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.practice != "")
                        {
                            cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = model.practice;
                        }
                        else
                        {
                            cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.datePaid != null)
                        {
                            cmd.Parameters.Add("@datePaid", SqlDbType.VarChar).Value = model.datePaid;
                        }
                        else
                        {
                            cmd.Parameters.Add("@datePaid", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.lobCode != null)
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = model.lobCode;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.paidTo != null)
                        {
                            cmd.Parameters.Add("@paidTo", SqlDbType.VarChar).Value = model.paidTo;
                        }
                        else
                        {
                            cmd.Parameters.Add("@paidTo", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.brokerId != null)
                        {
                            cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = model.brokerId;
                        }
                        else
                        {
                            cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = DBNull.Value;
                        }

                        cmd.Parameters.Add("@desc", SqlDbType.Int).Value = returnedClaimlag.description;

                        string WhereClause = "";

                        if (model.plan != "")
                        {
                            WhereClause = "where [Plan] = @hpCode";
                        }
                        if (model.memberNo != "")
                        {
                            if (WhereClause != "")
                            {
                                //WhereClause = WhereClause + " AND " + "[Member No] = '" + claimcredentials.MemberNo + "'";
                                // WhereClause = WhereClause + " AND " + "([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid) ";
                                WhereClause = WhereClause + " AND " + "([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";

                            }
                            else
                            {
                                //WhereClause = "where [Member No] = '" + claimcredentials.MemberNo + "'";
                                //WhereClause = "WHERE ([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";
                                WhereClause = " WHERE ([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";


                            }
                        }
                        if (model.serviceDate != "" && model.serviceDate != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Service Date] = @serviceDate";
                            }
                            else
                            {
                                WhereClause = "where [Service Date] = @serviceDate";
                            }
                        }
                        if (model.practice != "")
                        {
                            if (WhereClause != "")
                            {
                                //WhereClause = WhereClause + " AND " + " [Practice] = @practice ";                          // Jaco 2023-12-01 VENDOR practice must also be able to see claim
                                WhereClause = WhereClause + " AND " + " ([Practice] = @practice OR [VENDOR] = @practice) ";  // Jaco 2023-12-01 VENDOR practice must also be able to see claim
                            }
                            else
                            {
                                //WhereClause = "where [Practice] = @practice ";                         // Jaco 2023-12-01 VENDOR practice must also be able to see claim
                                WhereClause = "where ([Practice] = @practice OR [VENDOR] = @practice) "; // Jaco 2023-12-01 VENDOR practice must also be able to see claim
                            }
                        }
                        if (model.datePaid != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Date Paid] = @datePaid";
                            }
                            else
                            {
                                WhereClause = "where [Date Paid] = @datePaid";
                            }
                        }
                        if (model.lobCode != null)
                        {
                            if (model.lobCode.Length > 1)
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "cm.LOBCODE in (" + model.lobCode + ")";
                                }
                                else
                                {
                                    WhereClause = "where cm.LOBCODE in (" + model.lobCode + ")";
                                }
                            }
                            else
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "cm.LOBCODE = " + model.lobCode + "";
                                }
                                else
                                {
                                    WhereClause = "where cm.LOBCODE = " + model.lobCode + "";
                                }
                            }

                        }
                        if (model.paidTo != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Paid To] = @paidTo ";
                            }
                            else
                            {
                                WhereClause = "where [Paid To] = @paidTo";
                            }
                        }
                        if (model.brokerId != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[BROKERID] = @brokerId";
                            }
                            else
                            {
                                WhereClause = "where [BROKERID] = @brokerId";
                            }
                        }
                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Member Search Where Clause - {WhereClause}\r\n";

                        #endregion endregion

                        cmd.CommandType = CommandType.Text;

                        // Jaco comment out 2023-09-20 Databases hardcoded in string
                        //string sqlTextToExecute = "declare @membid varchar(20)\r\n" +
                        //                            "set @membid = @membnum\r\n" +
                        //                            "SELECT DISTINCT CASE WHEN PAY.CLAIMNO IS NULL THEN 'NO' ELSE 'YES' END AS isPaid,MEMBERSHIP.OPT, " +
                        //                            "cm.Practice, cm.Doctor, cm.[Family Number], RIGHT(cm.[Member No], 2) AS DependentCode, cm.[Member No], " +
                        //                            "op.HPNAME, cm.[Member Name], \r\n" +
                        //                            " cm.[Received Date], cm.[Service Date],PAY.DATETO, CONVERT(varchar, cm.REVERSEDATE, 111) AS 'Reversed Date', " +
                        //                            "cm.[Claim No], cm.Charged, cm.Paid, cm.[Paid To], cm.[Account No], cm.[Date Paid], cm.CHECKNO, op.ezHpCode, \r\n" +
                        //                            " op.MemberPrefix, CASE op.MemberPrefix WHEN NULL THEN 0 ELSE len(memberprefix) END AS prefixlength, cm.status,csd.[DESC],MEMBERSHIP.FIRSTNM,MEMBERSHIP.LASTNM, --MEMBERSHIP.BROKERID, \r\n" +
                        //                            "CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No],op.MemberPrefix,'')END AS MEMB, ISNULL(pm.FIRSTNAME,'') AS PROVNAME , pm.LASTNAME AS PROVSURNAME\r\n" +
                        //                            "FROM CLAIMS_HEADER AS cm INNER JOIN\r\n" +
                        //                            $"{DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.[Practice] INNER JOIN\r\n" +
                        //                            "MEMBERSHIP ON cm.[Member No] = MEMBERSHIP.MEMBID\r\n" +
                        //                            "inner join \r\n" +
                        //                            "(\r\n" +
                        //                            "select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix,ezOpt \r\n" +
                        //                            "from [Reporting].[dbo].[OxygenBenefitOptions] inner join HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\r\n" +
                        //                            ") op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt\r\n" +
                        //                            $"INNER JOIN {PamcPortalDatabase}.dbo.CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS " +
                        //                            "LEFT OUTER JOIN\r\n" +
                        //                            "(\r\n" +
                        //                            "SELECT CM.CLAIMNO,CH.PREFIX,CH.CHECKNO,CM.DATETO, cm.STATUS FROM " + this.DRCDatabase + $"..CLAIM_MASTERS CM INNER JOIN " + this.DRCDatabase + $"..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX\r\n" +
                        //                            "union all\r\n" +
                        //                            "SELECT CM2.CLAIMNO, cm2.BATCH as PREFIX, CM2.CHECKNO,CM2.DATETO, cm2.STATUS    FROM " + this.DRCDatabase + $"..CLAIM_MASTERS CM2 where cm2.CHPREFIX = 0\r\n" +
                        //                            ") PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO\r\n" + WhereClause + "AND CM.[Service Date] >= DATEADD(Month, - @desc, GETDATE()) order by cm.[Service Date] desc --24\r\n";

                        // Jaco 2023-09-20
                        string sqlTextToExecute =
                                                $" declare @membid varchar(20) \n " +
                                                $" set @membid = @membnum \n " +

                                                $" SELECT DISTINCT \n " +
                                                $"  CASE WHEN PAY.CLAIMNO IS NULL THEN 'NO' ELSE 'YES' END AS isPaid, \n " +
                                                $" 	cm.OPT,  \n " +
                                                $" 	cm.Practice,  \n " +
                                                $" 	cm.Doctor,  \n " +
                                                $" 	cm.[Family Number], \n " +
                                                $" 	RIGHT(cm.[Member No], 2) AS DependentCode, \n " +
                                                $"  cm.[Member No],  \n " +
                                                $" 	op.HPNAME,  \n " +
                                                $" 	cm.[Member Name], \n " +
                                                $" 	cm.[Received Date], \n " +
                                                $" 	cm.[Service Date], \n " +
                                                //$" 	PAY.DATETO, \n " +
                                                $" cm.[Service Date To], \n " +
                                                $" 	CONVERT(varchar, cm.REVERSEDATE, 111) AS 'Reversed Date', \n " +
                                                $" 	cm.[Claim No], \n " +
                                                $" 	cm.Charged, \n " +
                                                $" 	cm.Paid, \n " +
                                                $" 	cm.[Paid To], \n " +
                                                $" 	cm.[Account No], \n " +
                                                $" 	cm.[Date Paid], \n " +
                                                $" 	cm.CHECKNO, \n " +
                                                $" 	op.ezHpCode, \n " +
                                                $" 	op.MemberPrefix, \n " +
                                                $" 	CASE op.MemberPrefix WHEN NULL THEN 0 ELSE len(memberprefix) END AS prefixlength, \n " +
                                                $" 	cm.status, \n " +
                                                $" 	csd.[DESC], \n " +
                                                $" 	mm.FIRSTNM, \n " +
                                                $" 	mm.LASTNM,  \n " +
                                                $" 	CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No], op.MemberPrefix,'')END AS MEMB,  \n " +
                                                $" 	ISNULL(pm.FIRSTNAME, '') AS PROVNAME, pm.LASTNAME AS PROVSURNAME \n " +
                                                $"  FROM {PamcPortalDatabase}..CLAIMS_HEADER AS cm \n " +
                                                $"         INNER JOIN {DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.[Practice] \n " +
                                                $"         INNER JOIN {DRCDatabase}..MEMB_MASTERS mm ON cm.[Member No] = mm.MEMBID \n " +
                                                $"         INNER JOIN( \n " +
                                                $"                         select DISTINCT ezHpCode, hp_c.HPNAME, MemberPrefix, ezOpt \n " +
                                                $"                         from {ReportingDatabase}..OxygenBenefitOptions \n " +
                                                $"                                 inner join {DRCDatabase}..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode \n " +
                                                $" 					) op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt \n " +
                                                $"         INNER JOIN {PamcPortalDatabase}..CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS \n " +
                                                $"         LEFT OUTER JOIN( \n " +
                                                $"                             SELECT CM.CLAIMNO, CH.PREFIX, CH.CHECKNO, CM.DATETO, cm.STATUS FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX \n " +
                                                $"                             union all  \n " +
                                                $"                            SELECT CM2.CLAIMNO, cm2.BATCH as PREFIX, CM2.CHECKNO, CM2.DATETO, cm2.STATUS    FROM {DRCDatabase}..CLAIM_MASTERS CM2 where cm2.CHPREFIX = 0  \n " +
                                                $"                        ) PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO \n " +
                                                $" {WhereClause} AND CM.[Service Date] >= DATEADD(Month, -@desc, GETDATE()) order by cm.[Service Date] desc --24  ";

                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :SQL execute command - {sqlTextToExecute}\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        cmd.CommandTimeout = 6000;
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            ClaimSearchModel claim = new ClaimSearchModel();
                            claim.practice = dr["Practice"].ToString();
                            claim.doctor = dr["Doctor"].ToString();
                            claim.memberNo = dr["MEMB"].ToString();
                            claim.dependent = dr["DependentCode"].ToString();
                            claim.memberName = dr["Member Name"].ToString();
                            claim.memberFirstName = dr["FIRSTNM"].ToString();
                            claim.plan = dr["HPNAME"].ToString();
                            claim.receivedDate = dr["Received Date"].ToString().Replace('/', '-');
                            claim.serviceDate = dr["Service Date"].ToString().Replace('/', '-');
                            claim.serviceDateFrom = dr["Service Date"].ToString().Replace('/', '-');
                            //claim.serviceDateTo = dr["DATETO"].ToString().Replace('/', '-').Replace("00:00:00.000", "").Replace("00:00:00", "");
                            claim.serviceDateTo = dr["Service Date To"].ToString().Replace('/', '-');
                            claim.opt = dr["OPT"].ToString();
                            if (dr["Reversed Date"] != DBNull.Value)
                            {
                                claim.reversedDate = dr["Reversed Date"].ToString();
                            }
                            claim.provname = dr["PROVNAME"].ToString() + " " + dr["PROVSURNAME"].ToString();
                            claim.claimNo = dr["Claim No"].ToString();
                            claim.charged = dr["Charged"].ToString();
                            claim.paid = dr["Paid"].ToString();
                            claim.datePaid = dr["Date Paid"].ToString().Replace('/', '-');
                            if (returnInprogressToggle.enabled == true)
                            {
                                if (claim.datePaid == "In Process")
                                {
                                    if (model.userType != 3)
                                    {
                                        claim.paid = "In Process";
                                    }

                                }
                            }
                            claim.accountNo = dr["Account No"].ToString();
                            if (claim.datePaid != "In Process")
                            {
                                claim.datePaid = claim.datePaid.Replace("/", "-");
                            }
                            claim.checkNo = Convert.ToInt32(dr["CHECKNO"]);
                            claim.isPaid = dr["isPaid"].ToString();
                            claim.status = dr["STATUS"].ToString() + "  (" + dr["DESC"].ToString() + ")"; ;
                            claim.paidTo = dr["Paid To"].ToString();
                            ListOfClaims.Add(claim);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex.Message, ex.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :exception caught in the provider search claim method - {ex.Message}\r\n{ex.StackTrace}\r\n";
                }
                finally
                {
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} Member Claim Search Method Done\r\n";
                }
                Sqlcon.Close();
            }
            return ListOfClaims;
        }

        [HttpPost]
        [Route("ProviderClaimHistorySearch")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ClaimSearchModel> ProviderClaimHistorySearch([FromBody] ClaimSearchModel model)
        {
            OperationLog operationLog = new OperationLog();
            operationLog.LogClaim("Claim Search", DateTime.Now, model.userName, model.practice, _portalConnectionString);

            SettingsModel returnedClaimlag = Utillity.GetSpecificSetting("Claimlag", _portalConnectionString);
            SettingsModel returnInprogressToggle = new SettingsModel();



            string log = $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}-----Starting : ProviderClaimSearch---------------\r\n";


            if (model.userType == 1)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("ProviderInProgressSwitch", _portalConnectionString);
            }

            if (model.userType == 2)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("BureauInProgressSwitch", _portalConnectionString);
            }
            if (model.userType == 3)
            {
                returnInprogressToggle = Utillity.GetSpecificSetting("AdministratorInProgressSwitch", _portalConnectionString);
            }

            if (model.paidTo == "")
            {
                model.paidTo = null;
            }

            DataTable dtble = new DataTable();
            DataTable dt = new DataTable();
            List<ClaimSearchModel> ListOfClaims = new List<ClaimSearchModel>();
            string[] lobcodeList;
            StringBuilder sb = new StringBuilder();

            if (model.plan == "000")
            {

                model.plan = "";
            }
            if (model.lobCode == "")
            {
                model.lobCode = null;
            }

            if (model.lobCode != null)
            {
                lobcodeList = model.lobCode.Split(',');
                foreach (var code in lobcodeList)
                {
                    string x = "'" + code + "'" + ",";
                    sb.Append(x);
                }
                string lobCode = sb.ToString();

                model.lobCode = lobCode.Remove(lobCode.Length - 1);
            }

            try
            {
                if (model.serviceDate != null)
                {
                    model.serviceDate = model.serviceDate.Replace("-", "/");
                    model.datePaid = model.datePaid.Replace("-", "/");
                }

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }



            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {
                log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :dba connection provider claim search  - {_portalConnectionString}\r\n";
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region Build WhereClause 
                        if (model.memberNo != "")
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = model.memberNo;
                        }
                        else
                        {
                            cmd.Parameters.Add("@membnum", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.plan != "")
                        {
                            cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = model.plan;
                        }
                        else
                        {
                            cmd.Parameters.Add("@hpCode", SqlDbType.VarChar, 5).Value = DBNull.Value;
                        }
                        if (model.serviceDate != "" && model.serviceDate != null)
                        {
                            cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = model.serviceDate;
                        }
                        else
                        {
                            cmd.Parameters.Add("@serviceDate", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.practice != "")
                        {
                            cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = model.practice;
                        }
                        else
                        {
                            cmd.Parameters.Add("@practice", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.datePaid != null)
                        {
                            cmd.Parameters.Add("@datePaid", SqlDbType.VarChar).Value = model.datePaid;
                        }
                        else
                        {
                            cmd.Parameters.Add("@datePaid", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.lobCode != null)
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = model.lobCode;
                        }
                        else
                        {
                            cmd.Parameters.Add("@lobCode", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.paidTo != null)
                        {
                            cmd.Parameters.Add("@paidTo", SqlDbType.VarChar).Value = model.paidTo;
                        }
                        else
                        {
                            cmd.Parameters.Add("@paidTo", SqlDbType.VarChar).Value = DBNull.Value;
                        }
                        if (model.brokerId != null)
                        {
                            cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = model.brokerId;
                        }
                        else
                        {
                            cmd.Parameters.Add("@brokerId", SqlDbType.VarChar, 20).Value = DBNull.Value;
                        }

                        cmd.Parameters.Add("@desc", SqlDbType.Int).Value = returnedClaimlag.description;

                        string WhereClause = "";

                        if (model.plan != "")
                        {
                            WhereClause = "where [Plan] = @hpCode";
                        }
                        if (model.memberNo != "")
                        {
                            if (WhereClause != "")
                            {
                                //WhereClause = WhereClause + " AND " + "[Member No] = '" + claimcredentials.MemberNo + "'";
                                // WhereClause = WhereClause + " AND " + "([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid) ";
                                WhereClause = WhereClause + " AND " + "([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";

                            }
                            else
                            {
                                //WhereClause = "where [Member No] = '" + claimcredentials.MemberNo + "'";
                                //WhereClause = "WHERE ([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";
                                WhereClause = " WHERE ([Family Number] = isnull([MemberPrefix],'') + @membid OR [Member No] = isnull([MemberPrefix],'') + @membid or [Family Number] =  @membid or   [Member No] =  @membid) ";


                            }
                        }
                        if (model.serviceDate != "" && model.serviceDate != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Service Date] = @serviceDate";
                            }
                            else
                            {
                                WhereClause = "where [Service Date] = @serviceDate";
                            }
                        }
                        if (model.practice != "")
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + " [Practice] = @practice ";
                            }
                            else
                            {
                                WhereClause = "where [Practice] = @practice ";
                            }
                        }
                        if (model.datePaid != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Date Paid] = @datePaid";
                            }
                            else
                            {
                                WhereClause = "where [Date Paid] = @datePaid";
                            }
                        }
                        if (model.lobCode != null)
                        {
                            if (model.lobCode.Length > 1)
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "cm.LOBCODE in (" + model.lobCode + ")";
                                }
                                else
                                {
                                    WhereClause = "where cm.LOBCODE in (" + model.lobCode + ")";
                                }
                            }
                            else
                            {
                                if (WhereClause != "")
                                {
                                    WhereClause = WhereClause + " AND " + "cm.LOBCODE = " + model.lobCode + "";
                                }
                                else
                                {
                                    WhereClause = "where cm.LOBCODE = " + model.lobCode + "";
                                }
                            }

                        }
                        if (model.paidTo != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[Paid To] = @paidTo ";
                            }
                            else
                            {
                                WhereClause = "where [Paid To] = @paidTo";
                            }
                        }
                        if (model.brokerId != null)
                        {
                            if (WhereClause != "")
                            {
                                WhereClause = WhereClause + " AND " + "[BROKERID] = @brokerId";
                            }
                            else
                            {
                                WhereClause = "where [BROKERID] = @brokerId";
                            }
                        }
                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :Member Search Where Clause - {WhereClause}\r\n";

                        #endregion endregion

                        cmd.CommandType = CommandType.Text;

                        // Jaco comment out 2023-09-20 Databases hardcoded in string
                        //string sqlTextToExecute = "declare @membid varchar(20)\r\n" +
                        //                            "set @membid = @membnum\r\n" +
                        //                            "SELECT DISTINCT CASE WHEN PAY.CLAIMNO IS NULL THEN 'NO' ELSE 'YES' END AS isPaid,MEMBERSHIP.OPT, " +
                        //                            "cm.Practice, cm.Doctor, cm.[Family Number], RIGHT(cm.[Member No], 2) AS DependentCode, cm.[Member No], " +
                        //                            "op.HPNAME, cm.[Member Name], \r\n" +
                        //                            " cm.[Received Date], cm.[Service Date],PAY.DATETO, CONVERT(varchar, cm.REVERSEDATE, 111) AS 'Reversed Date', " +
                        //                            "cm.[Claim No], cm.Charged, cm.Paid, cm.[Paid To], cm.[Account No], cm.[Date Paid], cm.CHECKNO, op.ezHpCode, \r\n" +
                        //                            " op.MemberPrefix, CASE op.MemberPrefix WHEN NULL THEN 0 ELSE len(memberprefix) END AS prefixlength, cm.status,csd.[DESC],MEMBERSHIP.FIRSTNM,MEMBERSHIP.LASTNM, --MEMBERSHIP.BROKERID, \r\n" +
                        //                            "CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No],op.MemberPrefix,'')END AS MEMB, ISNULL(pm.FIRSTNAME,'') AS PROVNAME , pm.LASTNAME AS PROVSURNAME\r\n" +
                        //                            "FROM CLAIMS_HEADER AS cm INNER JOIN\r\n" +
                        //                            $"{DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.[Practice] INNER JOIN\r\n" +
                        //                            "MEMBERSHIP ON cm.[Member No] = MEMBERSHIP.MEMBID\r\n" +
                        //                            "inner join \r\n" +
                        //                            "(\r\n" +
                        //                            "select DISTINCT ezHpCode,hp_c.HPNAME,MemberPrefix,ezOpt \r\n" +
                        //                            "from [Reporting].[dbo].[OxygenBenefitOptions] inner join HP_Contracts hp_c on hp_c.HPCODE = ezHpCode\r\n" +
                        //                            ") op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt\r\n" +
                        //                            $"INNER JOIN {PamcPortalDatabase}.dbo.CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS " +
                        //                            "LEFT OUTER JOIN\r\n" +
                        //                            "(\r\n" +
                        //                            "SELECT CM.CLAIMNO,CH.PREFIX,CH.CHECKNO,CM.DATETO, cm.STATUS FROM " + this.DRCDatabase + $"..CLAIM_MASTERS CM INNER JOIN " + this.DRCDatabase + $"..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX\r\n" +
                        //                            "union all\r\n" +
                        //                            "SELECT CM2.CLAIMNO, cm2.BATCH as PREFIX, CM2.CHECKNO,CM2.DATETO, cm2.STATUS    FROM " + this.DRCDatabase + $"..CLAIM_MASTERS CM2 where cm2.CHPREFIX = 0\r\n" +
                        //                            ") PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO\r\n" + WhereClause + "AND CM.[Service Date] >= DATEADD(Month, - @desc, GETDATE()) order by cm.[Service Date] desc --24\r\n";

                        // Jaco 2023-09-20
                        string sqlTextToExecute =
                                                $" declare @membid varchar(20) \n " +
                                                $" set @membid = @membnum \n " +

                                                $" SELECT DISTINCT \n " +
                                                $"  CASE WHEN PAY.CLAIMNO IS NULL THEN 'NO' ELSE 'YES' END AS isPaid, \n " +
                                                $" 	cm.OPT,  \n " +
                                                $" 	cm.Practice,  \n " +
                                                $" 	cm.Doctor,  \n " +
                                                $" 	cm.[Family Number], \n " +
                                                $" 	RIGHT(cm.[Member No], 2) AS DependentCode, \n " +
                                                $"  cm.[Member No],  \n " +
                                                $" 	op.HPNAME,  \n " +
                                                $" 	cm.[Member Name], \n " +
                                                $" 	cm.[Received Date], \n " +
                                                $" 	cm.[Service Date], \n " +
                                                $" 	PAY.DATETO, \n " +
                                                $" 	CONVERT(varchar, cm.REVERSEDATE, 111) AS 'Reversed Date', \n " +
                                                $" 	cm.[Claim No], \n " +
                                                $" 	cm.Charged, \n " +
                                                $" 	cm.Paid, \n " +
                                                $" 	cm.[Paid To], \n " +
                                                $" 	cm.[Account No], \n " +
                                                $" 	cm.[Date Paid], \n " +
                                                $" 	cm.CHECKNO, \n " +
                                                $" 	op.ezHpCode, \n " +
                                                $" 	op.MemberPrefix, \n " +
                                                $" 	CASE op.MemberPrefix WHEN NULL THEN 0 ELSE len(memberprefix) END AS prefixlength, \n " +
                                                $" 	cm.status, \n " +
                                                $" 	csd.[DESC], \n " +
                                                $" 	mm.FIRSTNM, \n " +
                                                $" 	mm.LASTNM,  \n " +
                                                $" 	CASE WHEN op.MemberPrefix IS NULL THEN cm.[Member No] ELSE REPLACE(cm.[Member No], op.MemberPrefix,'')END AS MEMB,  \n " +
                                                $" 	ISNULL(pm.FIRSTNAME, '') AS PROVNAME, pm.LASTNAME AS PROVSURNAME \n " +
                                                $"  FROM {PamcPortalDatabase}..CLAIMS_HEADER AS cm \n " +
                                                $"         INNER JOIN {DRCDatabase}..PROV_MASTERS AS pm ON pm.PROVID = cm.[Practice] \n " +
                                                $"         INNER JOIN {DRCDatabase}..MEMB_MASTERS mm ON cm.[Member No] = mm.MEMBID \n " +
                                                $"         INNER JOIN( \n " +
                                                $"                         select DISTINCT ezHpCode, hp_c.HPNAME, MemberPrefix, ezOpt \n " +
                                                $"                         from {ReportingDatabase}..OxygenBenefitOptions \n " +
                                                $"                                 inner join {DRCDatabase}..HP_Contracts hp_c on hp_c.HPCODE = ezHpCode \n " +
                                                $" 					) op on op.ezHpCode = cm.[Plan] and cm.OPT = op.ezOpt \n " +
                                                $"         INNER JOIN {PamcPortalDatabase}..CLAIM_STATUS_DESC csd ON csd.STATUS = cm.STATUS \n " +
                                                $"         LEFT OUTER JOIN( \n " +
                                                $"                             SELECT CM.CLAIMNO, CH.PREFIX, CH.CHECKNO, CM.DATETO, cm.STATUS FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {DRCDatabase}..CHECK_MASTERS CH ON CH.CHECKNO = CM.CHECKNO AND CH.PREFIX = CM.CHPREFIX \n " +
                                                $"                             union all  \n " +
                                                $"                            SELECT CM2.CLAIMNO, cm2.BATCH as PREFIX, CM2.CHECKNO, CM2.DATETO, cm2.STATUS    FROM {DRCDatabase}..CLAIM_MASTERS CM2 where cm2.CHPREFIX = 0  \n " +
                                                $"                        ) PAY ON PAY.CLAIMNO = CM.[Claim No] AND PAY.CHECKNO = CM.CHECKNO \n " +
                                                $" {WhereClause} AND CM.[Service Date] >= DATEADD(Month, -@desc, GETDATE()) order by cm.[Service Date] desc --24  ";

                        log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :SQL execute command - {sqlTextToExecute}\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        cmd.CommandTimeout = 6000;
                        dt.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dt.Rows)
                        {
                            ClaimSearchModel claim = new ClaimSearchModel();
                            claim.practice = dr["Practice"].ToString();
                            claim.doctor = dr["Doctor"].ToString();
                            claim.memberNo = dr["MEMB"].ToString();
                            claim.dependent = dr["DependentCode"].ToString();
                            claim.memberName = dr["Member Name"].ToString();
                            claim.memberFirstName = dr["FIRSTNM"].ToString();
                            claim.plan = dr["HPNAME"].ToString();
                            claim.receivedDate = dr["Received Date"].ToString().Replace('/', '-');
                            claim.serviceDate = dr["Service Date"].ToString().Replace('/', '-');
                            claim.serviceDateFrom = dr["Service Date"].ToString().Replace('/', '-');
                            claim.serviceDateTo = dr["DATETO"].ToString().Replace('/', '-').Replace("00:00:00.000", "").Replace("00:00:00", "");
                            claim.opt = dr["OPT"].ToString();
                            if (dr["Reversed Date"] != DBNull.Value)
                            {
                                claim.reversedDate = dr["Reversed Date"].ToString();
                            }
                            claim.provname = dr["PROVNAME"].ToString() + " " + dr["PROVSURNAME"].ToString();
                            claim.claimNo = dr["Claim No"].ToString();
                            claim.charged = dr["Charged"].ToString();
                            claim.paid = dr["Paid"].ToString();
                            claim.datePaid = dr["Date Paid"].ToString().Replace('/', '-');
                            if (returnInprogressToggle.enabled == true)
                            {
                                if (claim.datePaid == "In Process")
                                {
                                    if (model.userType != 3)
                                    {
                                        claim.paid = "In Process";
                                    }

                                }
                            }
                            claim.accountNo = dr["Account No"].ToString();
                            if (claim.datePaid != "In Process")
                            {
                                claim.datePaid = claim.datePaid.Replace("/", "-");
                            }
                            claim.checkNo = Convert.ToInt32(dr["CHECKNO"]);
                            claim.isPaid = dr["isPaid"].ToString();
                            claim.status = dr["STATUS"].ToString() + "  (" + dr["DESC"].ToString() + ")"; ;
                            claim.paidTo = dr["Paid To"].ToString();
                            ListOfClaims.Add(claim);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex.Message, ex.StackTrace);
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} :exception caught in the provider search claim method - {ex.Message}\r\n{ex.StackTrace}\r\n";
                }
                finally
                {
                    log += $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")} Member Claim Search Method Done\r\n";
                }
                Sqlcon.Close();
            }
            return ListOfClaims;
        }


        [HttpPost]
        [Route("ProviderClaimSearchFamilyName")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ClaimSearchModel ProviderClaimSearchFamilyName([FromBody] ClaimSearchModel model)
        {
            ClaimSearchModel cd = new ClaimSearchModel();
            cd.memberLastName = "";
            cd.memberFirstName = "";

            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();

            string[] hpcodes;

            try
            {

                cn.ConnectionString = _drcConnectionString;

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;


                if (model.userType == 3)
                {
                    // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                    //cmd.CommandText = $"SELECT hpcode FROM {PamcPortalDatabase}.dbo.UserLobCodesHpCodes WHERE username = '{model.userName}' "; 

                    // Jaco 2023-09-14
                    cmd.CommandText = $" SELECT hp.hpcode " + //Testted with client login //Jaco 2023-11-24 tested with scheme login
                                      $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                      $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                      $" WHERE hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',')  ) AND u.username = '{model.userName}' ";

                    using (SqlDataReader tr = cmd.ExecuteReader())
                    {
                        dtbl.Load(tr);
                    }

                    hpcodes = new string[dtbl.Rows.Count];
                    int count = 0;
                    foreach (DataRow rows in dtbl.Rows)
                    {
                        if (count < hpcodes.Length)
                        {
                            hpcodes[count] = rows["hpcode"].ToString();
                        }
                        count++;
                    }

                    if (hpcodes.Length > 1)
                    {
                        for (int i = 0; i < hpcodes.Length; i++)
                        {
                            // Jaco comment out 2023-09-28 - Databases hardcoded in GetMemberPrefix function
                            //string sql = "DECLARE @membno AS VARCHAR(MAX)\n"
                            //+ $"SET @membno = '{model.memberNo}'\n"
                            //+ "DECLARE @familyno AS VARCHAR(MAX)\n"
                            //+ "DECLARE @prefix AS VARCHAR(MAX)\n"
                            //+ $"SET @prefix = {PamcPortalDatabase}.dbo.GetMemberPrefix('{hpcodes[i]}')\n"
                            //+ $"SET @familyno = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,{PamcPortalDatabase}.dbo.RemoveDependantDash(@membno))\n"
                            //+ "\n"
                            //+ "\n"
                            //+ $"SELECT FIRSTNM,LASTNM FROM MEMB_MASTERS WHERE RLSHIP = '1' AND HPCODE = '{hpcodes[i]}' AND SUBSSN = @familyno";
                            //cmd.CommandText = sql;

                            // Jaco 2023-09-28
                            cmd.CommandText =
                                " DECLARE @membno AS VARCHAR(MAX) \n" + // Jaco 2023-11-24 Tested with client login
                               $" SET @membno = '{model.memberNo}' \n" +
                                " DECLARE @familyno AS VARCHAR(MAX) \n" +
                                " DECLARE @prefix AS VARCHAR(MAX) \n" +
                               $" SELECT @prefix = ISNULL(MemberPrefix, '') \n" +
                               $"   FROM {ReportingDatabase}..OxygenBenefitOptions \n" +
                               $"       INNER JOIN {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n" +
                               $"   where OxygenBenefitOptions.ezHpCode  = '{hpcodes[i]}' AND OxygenBenefitOptions.scheme <> 'carecross' \n" +
                               $" SET @familyno = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix, {PamcPortalDatabase}.dbo.RemoveDependantDash(@membno)) \n" +
                               $" SELECT FIRSTNM, LASTNM FROM {DRCDatabase}..MEMB_MASTERS WHERE RLSHIP = '1' AND HPCODE = '{hpcodes[i]}' AND SUBSSN = @familyno ";

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                dt.Load(dr);
                            }
                            foreach (DataRow row in dt.Rows)
                            {
                                cd.memberLastName = row["LASTNM"].ToString();
                                cd.memberFirstName = row["FIRSTNM"].ToString();
                            }

                            if (dt.Rows.Count > 0)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        string sql = "DECLARE @membno AS VARCHAR(MAX)\n"
                                + $"SET @membno = '{model.memberNo}'\n"
                                + "DECLARE @familyno AS VARCHAR(MAX)\n"
                                + "DECLARE @prefix AS VARCHAR(MAX)\n"

                                // Jaco comment out 2023-09-28 - Databases harcoded in GetMemberPrefix function
                                //+ $"SET @prefix = {PamcPortalDatabase}.dbo.GetMemberPrefix('{hpcodes[0]}')\n"

                                // Jaco 2023-09-28
                                + $" SELECT @prefix = ISNULL(OxygenBenefitOptions.MemberPrefix, '') \n"
                                + $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n"
                                + $" {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = OxygenBenefitOptions.ezHpCode \n"
                                + $" where OxygenBenefitOptions.ezHpCode = '{hpcodes[0]}' AND OxygenBenefitOptions.scheme <> 'carecross' \n"

                                + $"SET @familyno = {PamcPortalDatabase}.dbo.GenerateSubSsn(@prefix,{PamcPortalDatabase}.dbo.RemoveDependantDash(@membno)) \n"
                                + $"SELECT FIRSTNM, LASTNM FROM {DRCDatabase}..MEMB_MASTERS WHERE RLSHIP = '1' AND HPCODE = '{hpcodes[0]}' AND SUBSSN = @familyno";

                        cmd.CommandText = sql;

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt.Load(dr);
                        }

                        foreach (DataRow row in dt.Rows)
                        {
                            cd.memberLastName = row["LASTNM"].ToString();
                            cd.memberFirstName = row["FIRSTNM"].ToString();
                        }
                    }
                }

                if (model.userType == 1 || model.userType == 2)
                {
                    // Jaco comment out 2024/06/06 - overlapping policynumbers
                    //SELECT * FROM MEMB_MASTERS WHERE SUBSSN LIKE '%7904065594083'   AND RLSHIP = '1'
                    //string sql = $"SELECT FIRSTNM,LASTNM FROM MEMB_MASTERS WHERE RLSHIP = '1' AND HPCODE = AND SUBSSN LIKE '%{model.memberNo}' OR MEMBID LIKE '%{model.memberNo}'";

                    string sql =
                        " SELECT distinct MS.MEMBID, MS.RLSHIP, MS.FIRSTNM, MS.LASTNM, MS.HPCODE, MS.OPT \r\n" +
                        " FROM(  \r\n" +
                        "         Select distinct ezHpCode, ezOpt, isnull(MemberPrefix, '') as MemberPrefix  \r\n" +
                       $"         From {ReportingDatabase}..OxygenBenefitOptions O  \r\n" +
                        " 		  Where ezhpcode = @hpCode \r\n" +
                        "     ) as O  \r\n" +
                       $" INNER JOIN {DRCDatabase}..MEMB_MASTERS MM on(MM.HPCODE = O.ezHpCode AND MM.OPT = O.ezOpt)  \r\n" +
                       $" INNER JOIN {DRCDatabase}..MEMB_MASTERS MS on(MM.SUBSSN = MS.SUBSSN)  \r\n" +
                        " WHERE SUBSTRING(  \r\n" +
                        "                     REVERSE(SUBSTRING(REVERSE(MM.MEMBID), CHARINDEX('-', REVERSE(MM.MEMBID)) +1, LEN(MM.MEMBID))), --Remove dep code from membid  \r\n" +
                        "                     LEN(O.MemberPrefix) + 1, --Prefix legth  \r\n" +
                        "                     LEN(REVERSE(SUBSTRING(REVERSE(MM.MEMBID), CHARINDEX('-', REVERSE(MM.MEMBID)) + 1, LEN(MM.MEMBID))))--Length of membid with prefix removed   \r\n" +
                        "                 )  = @policyNum  \r\n" +
                        " order by MS.MEMBID ";

                    cmd.Parameters.Add(new SqlParameter("@policyNum", model.memberNo));
                    cmd.Parameters.Add(new SqlParameter("@hpCode", model.plan));
                    cmd.CommandText = sql;

                    dt.Load(cmd.ExecuteReader());

                    // Jaco comment out 2024/06/06
                    //using (SqlDataReader dr = cmd.ExecuteReader())
                    //{
                    //    dt.Load(dr);
                    //}

                    if (dt.Rows.Count > 1)
                    {

                        DataRow[] hpHistsRows = dt.Select($"RLSHIP = '1'");

                        if (hpHistsRows.Count() == 1)
                        {
                            // Only one main member (relationship 1) is what we expect - great!

                            cd.memberLastName = hpHistsRows[0]["LASTNM"].ToString();
                            cd.memberFirstName = hpHistsRows[0]["FIRSTNM"].ToString();
                        }
                        else if (hpHistsRows.Count() > 1)
                        {
                            // if there is more that one main members (relationship 1), this loop will use the last one which is probably more accurate - dep 00 might have left and then dep 01 became the new main member

                            foreach (DataRow row in hpHistsRows)
                            {
                                cd.memberLastName = row["LASTNM"].ToString();
                                cd.memberFirstName = row["FIRSTNM"].ToString();
                            }
                        }
                        else
                        {
                            // No relationship 1 on policy - what can we do? - so just use dep 00
                            cd.memberLastName = dt.Rows[0]["LASTNM"].ToString();
                            cd.memberFirstName = dt.Rows[0]["FIRSTNM"].ToString();
                        }
                    }
                }
                //if (model.userType == 2) // Jaco comment out 2024-06-06 unnecessary duplication of code - code is wrong anyways (overlapping policynumbers)
                //{
                //    //SELECT * FROM MEMB_MASTERS WHERE SUBSSN LIKE '%7904065594083'   AND RLSHIP = '1'
                //    string sql = $"SELECT FIRSTNM,LASTNM FROM MEMB_MASTERS WHERE RLSHIP = '1' AND SUBSSN LIKE '%{model.memberNo}' OR MEMBID LIKE '%{model.memberNo}' ";
                //    cmd.CommandText = sql;

                //    using (SqlDataReader dr = cmd.ExecuteReader())
                //    {
                //        dt.Load(dr);
                //    }

                //    foreach (DataRow row in dt.Rows)
                //    {
                //        cd.memberLastName = row["LASTNM"].ToString();
                //        cd.memberFirstName = row["FIRSTNM"].ToString();
                //    }
                //}
                cn.Close();

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return cd;
        }

        /*Will be called when a user reverses  a claim*/
        [HttpPost]
        [Route("ClaimReversal")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ClaimSearchModel ClaimReversal([FromBody] ClaimReversalModel model)
        {
            ClaimSearchModel claimDetails = new ClaimSearchModel();
            try
            {

                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandTimeout = 3600;

                        cmd.CommandType = CommandType.Text;
                        string reverseClaimproc = "EXECUTE  [dbo].[ReverseClaim] '" + model.claimNumber + "',999";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = reverseClaimproc;
                        cmd.ExecuteNonQuery();

                        cmd.CommandTimeout = 3600;
                        string getReversedclaim = "select * from [CLAIM_MASTERS]\r\n" +
                                                   "where CLAIMORIGINAL = '" + model.claimNumber + "'\r\n";
                        cmd.CommandText = getReversedclaim;
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        DataRow dr = dt.Rows[0];

                        claimDetails.claimNo = dr["CLAIMNO"].ToString();


                    }
                    Sqlcon.Close();
                }
                using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandTimeout = 3600;
                        cmd.CommandType = CommandType.Text;

                        // Jaco 2023-09-27 - Databases hardcoded in stored proc
                        //string updateClaimheaderTable = "EXECUTE  [dbo].[populateClaimsHeaderPerClaim] '" + claimDetails.claimNo + "', '" + model.claimNumber + "'";

                        // Jaco 2023-09-27 
                        string updateClaimheaderTable =
                           $" DECLARE @REVERSED_CLAIMNO VARCHAR(16) = '{claimDetails.claimNo}' \n" +   //Jaco tested 2023-11-28
                           $" DECLARE @ORIGINAL_CLAIMNO VARCHAR(16) = '{model.claimNumber}' \n" +
                            " UPDATE HH SET REVERSEDATE = CM.REVERSEDATE,CLAIMREVERSAL = CM.CLAIMREVERSAL \n" +
                           $" FROM {DRCDatabase}..CLAIM_MASTERS CM INNER JOIN {PamcPortalDatabase}..CLAIMS_HEADER HH ON HH.[Claim No] = CM.CLAIMNO \n" +
                            " WHERE CM.CLAIMNO = @ORIGINAL_CLAIMNO \n" +
                            " INSERT INTO [dbo].[CLAIMS_HEADER] \n" +
                            "                   ([Practice] \n" +
                            "                   ,[Doctor] \n" +
                            "                   ,[Family Number] \n" +
                            "                   ,[Member No] \n" +
                            "                   ,[Member Name] \n" +
                            "                   ,[Plan] \n" +
                            "                   ,[Service Date] \n" +
                            "                   ,[Claim No] \n" +
                            "                   ,[Charged] \n" +
                            "                   ,[status] \n" +
                            "                   ,[Paid] \n" +
                            "                   ,[Date Paid] \n" +
                            "                   ,[Account No] \n" +
                            "                   ,[LOBCODE] \n" +
                            "                   ,[Paid To] \n" +
                            "                   ,[CHECKNO] \n" +
                            "                   ,[OPT] \n" +
                            "                   ,[CHPREFIX] \n" +
                            "                   ,[VENDOR] \n" +
                            "                   ,[Received Date] \n" +
                            "                   ,[REVERSEDATE] \n" +
                            "                   ,[CLAIMREVERSAL]) \n" +
                            "        SELECT CM_1.PROVID AS Practice, \n" +
                            "                    pm.LASTNAME AS 'Doctor', \n" +
                            "                    CM_1.subssn AS [Family Number], \n" +
                            "                    CM_1.MEMBID AS [Member No], \n" +
                            "                    UPPER(CM_1.MEMBNAME) AS [Member Name],  \n" +
                            "                    CM_1.HPCODE AS [Plan], \n" +
                            "                    CONVERT(VARCHAR, CM_1.FROMDATE, 111) AS [Service Date], \n" +
                            "                    CM_1.CLAIMNO AS [Claim No], \n" +
                            "                    CONVERT(varchar, CONVERT(money, CM_1.BILLED), 1) AS Charged, \n" +
                            "                    CM_1.status,  \n" +
                            "                    CONVERT(varchar, CONVERT(money, NET), 1)  AS 'Paid', \n" +
                            "                    CASE WHEN status = '9' THEN CONVERT(VARCHAR, CM_1.DATEPAID, 111) ELSE 'In Process' END AS [Date Paid], \n" +                                               
                            "                    CM_1.PROVCLAIM AS [Account No], \n" +
                            "                    CM_1.LOBCODE,  \n" +
                            "                    CM_1.CLAIMTYPE AS 'Paid To',   \n" +
                            "                    ISNULL(CHECKNO, '0') AS CHECKNO, \n" +
                            "                    OPT, \n" +
                            "                    CHPREFIX, \n" +
                            "                    CM_1.VENDOR,  \n" +
                            "                    CONVERT(VARCHAR, CM_1.DATERECD, 111) AS [Received Date], \n" +
                            "                    CM_1.REVERSEDATE, \n" +
                            "                    CM_1.CLAIMREVERSAL \n" +
                            "        FROM \n" +
                            "        (     \n" +
                            "            SELECT     CM.LOBCODE, SUBSTRING(CM.SUBSSN, 2, 50) AS subssn, CM.CLAIMNO, SUBSTRING(CM.MEMBID, 1, 50) AS MEMBID, CM.PROVID, CM.MEMBNAME, CM.DATEFROM AS FROMDATE,  \n" +
                            "                        CM.DATEPAID, CM.BILLED AS BILLED, CM.NET AS NET, CM.HPCODE, CM.OPT, CM.STATUS, CM.PROVCLAIM, CM.CLAIMTYPE, CM.CHECKNO, \n" +
                            "                        (CASE WHEN CM.CHPREFIX = 0 THEN CM.BATCH ELSE CM.CHPREFIX END) as CHPREFIX, cm.VENDOR,CM.DATERECD,REVERSEDATE, CLAIMREVERSAL \n" +
                           $"                         FROM {DRCDatabase}..CLAIM_MASTERS AS CM \n" +
                            "                         WHERE(CM.PROVID <> REPLICATE('9', 20)) AND(CM.CREATEDATE > DATEADD(year, -2, GETDATE())) \n" +
                            "                         and((isnull(CM.BATCH, 999) <> 0 and status < 9) or (CM.BATCH is not null and status = 9)) \n" +
                            "                         AND CM.CLAIMNO = @REVERSED_CLAIMNO \n" +
                           $"        ) AS CM_1 INNER JOIN {DRCDatabase}..PROV_MASTERS AS PM ON PM.PROVID = CM_1.PROVID \n" +
                           $"            inner join {PamcPortalDatabase}..HealthPlanSetup hps on hps.HPCode = CM_1.HPCODE and hps.isEnabledClaim = 1 ";
                      
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = updateClaimheaderTable;
                        cmd.ExecuteNonQuery();
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);

                throw;
            }
            return claimDetails;
        }

        /*Will be used to get the detail of selected claim*/
        [HttpPost]
        [Route("ClaimInfo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public Tuple<List<ClaimInformation>, List<ClaimAdjusts>> GetClaimInfo([FromBody] ClaimSearchModel model)
        {
            List<ClaimInformation> listOfClaimInfo = new List<ClaimInformation>();
            List<ClaimAdjusts> listOfClaimAdjusts = new List<ClaimAdjusts>();

            DataTable dtClaiminfo = new DataTable();
            DataTable dtClaimadjusts = new DataTable();

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();

                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        string sqlTextToGetClaimDetail = "select PROCCODE,FROMDATESVC,BILLED,NET,QTY,DIAGCODE,TBLROWID  from CLAIM_DETAILS\r\n" +
                                                         "where CLAIMNO = '" + model.claimNo + "'\r\n";

                        string sqlTextToGetClaimAdjust = "select ADJCODE,COMMENTS,CLAIMTBLROW  from CLAIM_ADJUSTS\r\n" +
                                        "where CLAIMNO = '" + model.claimNo + "'\r\n" +
                                        "union all\r\n" +
                                        "select ADJCODEWH,'BENEFITS EXEEDED', TBLROWID from CLAIM_DETAILS\r\n" +
                                        "where CLAIMNO = '" + model.claimNo + "' AND ADJUSTWH <> 0\r\n" +
                                        "UNION ALL \r\n" +
                                        "select 'COPAY','MEMBER CO-PAYMENT', TBLROWID from CLAIM_DETAILS\r\n" +
                                        "where CLAIMNO = '" + model.claimNo + "' AND COPAY <> 0\r\n" +
                                        "UNION ALL \r\n" +
                                        "select '104','PROVIDER CHARGED ABOVE SCHEME RATE', TBLROWID from CLAIM_DETAILS\r\n" +
                                        "where CLAIMNO = '" + model.claimNo + "' AND TARRIFREDUCT <> 0\r\n";
                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToGetClaimDetail;

                        dtClaiminfo.Load(cmd.ExecuteReader());
                        cmd.CommandText = sqlTextToGetClaimAdjust;
                        dtClaimadjusts.Load(cmd.ExecuteReader());

                        foreach (DataRow dr in dtClaiminfo.Rows)
                        {
                            ClaimInformation selectedClaim = new ClaimInformation();
                            selectedClaim.PROCCODE = dr["PROCCODE"].ToString();
                            selectedClaim.FROMDATESVC = Convert.ToDateTime(dr["FROMDATESVC"]);
                            selectedClaim.BILLED = Convert.ToDecimal(dr["BILLED"]);
                            selectedClaim.NET = Convert.ToDecimal(dr["NET"]);
                            selectedClaim.QTY = Convert.ToDecimal(dr["QTY"]);
                            selectedClaim.DIAGCODE = dr["DIAGCODE"].ToString();
                            selectedClaim.TBLROWID = Convert.ToInt32(dr["TBLROWID"]);

                            listOfClaimInfo.Add(selectedClaim);
                        }
                        foreach (DataRow dr in dtClaimadjusts.Rows)
                        {
                            ClaimAdjusts selectedClaimAdjusts = new ClaimAdjusts();
                            selectedClaimAdjusts.CLAIMTBLROW = Convert.ToInt32(dr["CLAIMTBLROW"]);
                            selectedClaimAdjusts.ADJCODE = dr["ADJCODE"].ToString();
                            selectedClaimAdjusts.COMMENTS = dr["COMMENTS"].ToString();

                            listOfClaimAdjusts.Add(selectedClaimAdjusts);
                        }
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return new Tuple<List<ClaimInformation>, List<ClaimAdjusts>>(listOfClaimInfo, listOfClaimAdjusts);
        }
                
        [HttpPost]
        [Route("GetVendorInfo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public VendorInfoModel GetVendorInfo([FromBody] ClaimSearchModel model)
        {
            VendorInfoModel vendInfo = new VendorInfoModel();            
            DataTable dt = new DataTable();       

            try
            {
                using (SqlConnection Sqlcon = new SqlConnection(_drcConnectionString))
                {
                    if (Sqlcon.State != ConnectionState.Open) { Sqlcon.Open(); }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText =
                            " SELECT CM.CLAIMNO, CM.VENDOR, CM.CLAIMTYPE, CASE WHEN CM.CLAIMTYPE = 'P' THEN 'Practice' WHEN CM.CLAIMTYPE = 'M' THEN 'Member' ELSE CM.CLAIMTYPE END as CLAIMTYPEDESCR, PS.SPECCODE, PC.DESCR, VM.VENDORNM \n" +
                            " FROM CLAIM_MASTERS CM \n" +
                            "   LEFT JOIN PROV_SPECINFO PS ON CM.VENDOR = PS.PROVID \n" +
                            "   LEFT JOIN PROV_SPECCODES PC ON PS.SPECCODE = PC.CODE \n" +
                            "   LEFT JOIN VEND_MASTERS VM ON CM.VENDOR = VM.VENDORID \n" +
                            " WHERE CLAIMNO = '" + model.claimNo + "' \n";                                                
                        cmd.Connection = Sqlcon;                        

                        dt.Load(cmd.ExecuteReader());

                        vendInfo.vendorType = dt.Rows[0]["CLAIMTYPE"].ToString();
                        vendInfo.vendorTypeDescr = dt.Rows[0]["CLAIMTYPEDESCR"].ToString();
                        vendInfo.vendorName = dt.Rows[0]["VENDORNM"].ToString();
                        vendInfo.vendorId = dt.Rows[0]["VENDOR"].ToString();
                        vendInfo.specCode = dt.Rows[0]["SPECCODE"].ToString();
                        vendInfo.specCodeDescr = dt.Rows[0]["DESCR"].ToString();       
                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
            }
            return vendInfo;
        }

        /*Will be used to get the Refering provider details of a claim*/
        [HttpPost]
        [Route("GetDetailsOfRefProv")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public DetailLinesModel GetDetailsOfRefProv([FromBody] CheckRunDetailsModel data)
        {
            DetailLinesModel lines = new DetailLinesModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                // Jaco Comment out 2024/01/10 wrong when REFROVID in claim masters is null or '' or not in other provider tables
                //cmd.CommandText = "SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_MASTERS.REFPROVID, ISNULL(PROV_MASTERS.FIRSTNAME, '') + ' ' + PROV_MASTERS.LASTNAME AS NAME,\n"
                //           + " PROV_SPECINFO.SPECCODE, PROV_SPECCODES.DESCR,CLAIM_MASTERS.CLAIMORIGINAL,CLAIM_MASTERS.CLAIMREVERSAL,CLAIM_MASTERS.REVERSEDATE\n"
                //           + "FROM CLAIM_MASTERS INNER JOIN\n"
                //           + "PROV_MASTERS ON CLAIM_MASTERS.REFPROVID = PROV_MASTERS.PROVID INNER JOIN\n"
                //           + "PROV_SPECINFO ON PROV_MASTERS.PROVID = PROV_SPECINFO.PROVID INNER JOIN\n"
                //           + "PROV_SPECCODES ON PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE AND \n"
                //           + "PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE AND \n"
                //           + "PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE\n"
                //           + $"WHERE (CLAIM_MASTERS.CLAIMNO = '{data.claimNo}')"
                //           + "UNION ALL (\r\n"
                //           + "SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_MASTERS.PROVID, ISNULL(PROV_MASTERS.FIRSTNAME, '') + ' ' + PROV_MASTERS.LASTNAME AS NAME,\n"
                //           + " PROV_SPECINFO.SPECCODE, PROV_SPECCODES.DESCR,CLAIM_MASTERS.CLAIMORIGINAL,CLAIM_MASTERS.CLAIMREVERSAL,CLAIM_MASTERS.REVERSEDATE\n"
                //           + "FROM CLAIM_MASTERS INNER JOIN\n"
                //           + "PROV_MASTERS ON CLAIM_MASTERS.PROVID = PROV_MASTERS.PROVID INNER JOIN\n"
                //           + "PROV_SPECINFO ON PROV_MASTERS.PROVID = PROV_SPECINFO.PROVID INNER JOIN\n"
                //           + "PROV_SPECCODES ON PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE AND \n"
                //           + "PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE AND \n"
                //           + "PROV_SPECINFO.SPECCODE = PROV_SPECCODES.CODE\n"
                //           + $"WHERE (CLAIM_MASTERS.CLAIMNO = '{data.claimNo}'))";


                // Get PROVID, REFPROVID and REVERSAL info from CLAIM_MASTERS
                cmd.CommandText = " SELECT PROVID, LTRIM(RTRIM(isnull(REFPROVID, ''))) as REFPROVID, CLAIMORIGINAL, CLAIMREVERSAL, REVERSEDATE \n" +
                                  " FROM CLAIM_MASTERS with (nolock) \n" +
                                  " WHERE CLAIMNO = @claimNo \n";
                cmd.Parameters.Add(new SqlParameter("@claimNo", data.claimNo));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                lines.provid = dt.Rows[0]["PROVID"].ToString();
                lines.refProvId = dt.Rows[0]["REFPROVID"].ToString();
                lines.claimOriginal = dt.Rows[0]["CLAIMORIGINAL"].ToString();
                lines.reversedate = dt.Rows[0]["REVERSEDATE"].ToString();
                lines.claimreverse = dt.Rows[0]["CLAIMREVERSAL"].ToString();

                // Get treating provider info
                cmd.Parameters.Add(new SqlParameter("@provId", lines.provid));
                cmd.CommandText = " SELECT PM.PROVID, PS.SPECCODE, PC.DESCR, CASE \n" +
                                  "                                             WHEN PM.FIRSTNAME is null OR LTRIM(RTRIM(PM.FIRSTNAME)) = '' THEN PM.LASTNAME \n" +
                                  "                                             ELSE PM.FIRSTNAME + ' ' + PM.LASTNAME \n" +
                                  "                                          END as [NAME] \n" +
                                  " FROM PROV_MASTERS PM with (nolock) \n" +
                                  "     LEFT JOIN PROV_SPECINFO PS with (nolock) ON PM.PROVID = PS.PROVID AND PS.TYPE = 'PRIMARY' \n" +
                                  "     LEFT JOIN PROV_SPECCODES PC with (nolock) ON PS.SPECCODE = PC.CODE \n" +
                                  " WHERE PM.PROVID = @provId ";

                dt = new DataTable();
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                if (dt.Rows.Count > 0)
                {
                    lines.provname = dt.Rows[0]["NAME"].ToString();
                    lines.provspec = dt.Rows[0]["SPECCODE"].ToString();
                    lines.provdesc = dt.Rows[0]["DESCR"].ToString();
                }
                else
                {
                    lines.provname = "";
                    lines.provspec = "";
                    lines.provdesc = "";
                }

                // Get referring provider info
                lines.refProfName = "";
                lines.specCode = "";
                lines.desc = "";

                if (lines.refProvId.Trim() != "")
                { 
                    cmd.Parameters.Add(new SqlParameter("@refProvId", lines.refProvId.Trim()));
                    cmd.CommandText = " SELECT PM.PROVID, PS.SPECCODE, PC.DESCR, CASE \n" +
                                      "                                             WHEN PM.FIRSTNAME is null OR LTRIM(RTRIM(PM.FIRSTNAME)) = '' THEN PM.LASTNAME \n" +
                                      "                                             ELSE PM.FIRSTNAME + ' ' + PM.LASTNAME \n" +
                                      "                                          END as [NAME] \n" +
                                      " FROM PROV_MASTERS PM with (nolock) \n" +
                                      "     LEFT JOIN PROV_SPECINFO PS with (nolock) ON PM.PROVID = PS.PROVID AND PS.TYPE = 'PRIMARY' \n" +
                                      "     LEFT JOIN PROV_SPECCODES PC with (nolock) ON PS.SPECCODE = PC.CODE \n" +
                                      " WHERE PM.PROVID = @refProvId ";

                    dt = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        lines.refProfName = dt.Rows[0]["NAME"].ToString();
                        lines.specCode = dt.Rows[0]["SPECCODE"].ToString();
                        lines.desc = dt.Rows[0]["DESCR"].ToString();
                    }
                }

                // Jaco Comment out 2024/01/10 wrong when REFROVID in claim masters is null or '' or not in other provider tables
                //int count = 0;
                //foreach (DataRow rows in dt.Rows)
                //{
                //    count++;
                //    if (count == 1)
                //    {
                //        lines.claimNo = rows["CLAIMNO"].ToString();
                //        lines.refProvId = rows["REFPROVID"].ToString();
                //        lines.refProfName = rows["NAME"].ToString();
                //        lines.specCode = rows["SPECCODE"].ToString();
                //        lines.desc = rows["DESCR"].ToString();
                //        lines.claimOriginal = rows["CLAIMORIGINAL"].ToString();
                //        lines.reversedate = rows["REVERSEDATE"].ToString();
                //        lines.claimreverse = rows["CLAIMREVERSAL"].ToString();
                //    }
                //    if (count == 2)
                //    {
                //        lines.provid = rows["REFPROVID"].ToString();
                //        lines.provname = rows["NAME"].ToString();
                //        lines.provspec = rows["SPECCODE"].ToString();
                //        lines.provdesc = rows["DESCR"].ToString();
                //    }
                //}

                dt.Clear();
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return lines;
        }

        /*Will be used to get the claim detail lines of the claim*/
        [HttpPost]
        [Route("GetDetailsOfClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DetailLinesModel> GetDetailsOfClaim([FromBody] CheckRunDetailsModel data)
        {
            DetailLinesModel returned = new DetailLinesModel();
            List<DetailLinesModel> lines = new List<DetailLinesModel>();
            DataTable dtable = new DataTable();
            DataTable datatable = new DataTable();
            DataTable tbldata = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string tblRowId = "";
            string adjcodes = "";
            string rejected = "";
            bool isReject = false;
            try
            {
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                //Jaco comment out 2024-01-11 had to get proc code description, then also removed unncessasry join on PamcPortalDatabase
                //cmd.CommandText = "  SELECT CLAIM_MASTERS.CLAIMNO, CLAIM_DETAILS.TBLROWID, CLAIM_DETAILS.BILLED, CLAIM_DETAILS.TARRIFREDUCT,CLAIM_DETAILS.PROCCODE, CLAIM_DETAILS.FROMDATESVC, CLAIM_DETAILS.TODATESVC, \n"
                //                + "CLAIM_DETAILS.COPAY, CLAIM_MASTERS.DATERECD, CLAIM_DETAILS.ADJUST, \n"
                //                + "CLAIM_DETAILS.ADJUSTWH AS ADJUST, CLAIM_DETAILS.NET,CLAIM_MASTERS.PROVCLAIM, " + this.PamcPortalDatabase + $"..CLAIMS_HEADER.[Date Paid],CLAIM_DETAILS.DIAGCODE, DIAG_CODES.DIAGDESC, \n"
                //                + "dbo.ConcatToothNumbers(CLAIM_DETAILS.CLAIMNO, CLAIM_DETAILS.TBLROWID) as ClinicalCodes \n"
                //                + "FROM CLAIM_MASTERS  INNER JOIN\n"
                //                + "CLAIM_DETAILS ON CLAIM_MASTERS.CLAIMNO = CLAIM_DETAILS.CLAIMNO INNER JOIN \n DIAG_CODES on CLAIM_DETAILS.DIAGCODE = DIAG_CODES.DIAGCODE INNER JOIN \n "
                //                + "" + this.PamcPortalDatabase + $"..CLAIMS_HEADER ON CLAIM_MASTERS.CLAIMNO = " + this.PamcPortalDatabase + $"..CLAIMS_HEADER.[Claim No]\n"
                //                + $"WHERE (CLAIM_MASTERS.CLAIMNO = '{data.claimNo}') ";

                // Jaco comment out 2024-01-11
                cmd.CommandText =
                " SELECT CM.CLAIMNO, \n" +
                "		CM.PROVCLAIM, \n" +
                "		CM.DATERECD, \n" +
                "		CASE WHEN CM.[STATUS] = '9' THEN  CONVERT(VARCHAR, CM.DATEPAID, 111) ELSE 'In Process' END AS [Date Paid], \n" +
                "		CD.TBLROWID, \n" +
                "		CD.PROCCODE, \n" +
                "		CD.PHCODE, \n" +
                "		CD.FROMDATESVC, \n" +
                "		CD.TODATESVC, \n" +
                "		CD.BILLED, \n" +
                "		CD.TARRIFREDUCT, \n" +
                "		CD.COPAY, \n" +
                "		CD.ADJUST, \n" +
                "		CD.ADJUSTWH AS ADJUST, \n" +
                "		CD.NET, \n" +
                "		CD.DIAGCODE, \n" +
                "		dbo.ConcatToothNumbers(CD.CLAIMNO, CD.TBLROWID) as ClinicalCodes, \n" +
                "		DC.DIAGDESC, \n" +
                "		PC.SVCDESC \n" +
                " FROM CLAIM_MASTERS CM with (nolock) \n" +
                "   INNER JOIN CLAIM_DETAILS CD with (nolock) ON CM.CLAIMNO = CD.CLAIMNO \n" +
                "   LEFT JOIN DIAG_CODES DC with (nolock) ON CD.DIAGCODE = DC.DIAGCODE \n" +
                "   LEFT JOIN PROC_CODES PC with (nolock) ON CD.PHCODE = PC.PHCODE and CD.PROCCODE = PC.SVCCODE AND CD.FROMDATESVC between PC.FROMDATE and isnull(PC.TODATE, '9999-12-31') \n" +
                " WHERE CM.CLAIMNO = @claimNo ";

                cmd.Parameters.Add(new SqlParameter("@claimNo", data.claimNo.Trim()));

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                foreach (DataRow row in dtable.Rows)
                {
                    tblRowId = row["TBLROWID"].ToString();
                    cmd.CommandText = $"SELECT [dbo].GetAdjustString('{data.claimNo}',{tblRowId})AS CODES";

                    using (SqlDataReader drows = cmd.ExecuteReader())
                    {
                        tbldata.Load(drows);
                    }
                    foreach (DataRow codes in tbldata.Rows)
                    {
                        adjcodes = codes["CODES"].ToString();
                    }
                    tbldata.Clear();
                    cmd.CommandText = $"SELECT [dbo].VALIDATEREJECTION('{data.claimNo}','{tblRowId}') AS REJECT";
                    using (SqlDataReader drows = cmd.ExecuteReader())
                    {
                        datatable.Load(drows);
                    }
                    foreach (DataRow reject in datatable.Rows)
                    {
                        rejected = reject["REJECT"].ToString();
                    }
                    if (rejected == "True")
                    {
                        isReject = true;
                    }
                    else
                    {
                        isReject = false;
                    }
                    datatable.Clear();
                    returned = new DetailLinesModel();
                    returned.diagcode = row["DIAGCODE"].ToString();
                    returned.diagdesc = row["DIAGDESC"].ToString();
                    returned.claimNo = row["CLAIMNO"].ToString();
                    returned.fromDate = Convert.ToDateTime(row["FROMDATESVC"]).ToString("yyyy/MM/dd");
                    returned.tooDate = Convert.ToDateTime(row["TODATESVC"]).ToString("yyyy/MM/dd");
                    returned.provClaim = row["PROVCLAIM"].ToString();
                    returned.srvCode = row["PROCCODE"].ToString();
                    returned.srvCodeDesc = row["SVCDESC"].ToString();
                    returned.tblRowID = row["TBLROWID"].ToString();
                    returned.billed = row["BILLED"].ToString();
                    returned.coPay = row["COPAY"].ToString();
                    DateTime dtRec = Convert.ToDateTime(row["DATERECD"]);
                    returned.dateRec = dtRec.ToString("yyyy/MM/dd");
                    returned.adjust = row["ADJUST"].ToString();
                    returned.tarrif = row["TARRIFREDUCT"].ToString();
                    returned.net = row["NET"].ToString();
                    returned.clinicalCodes = row["CLINICALCODES"].ToString();
                    returned.adjCode = adjcodes;
                    returned.isRejected = isReject;
                    returned.rejectable = isReject;
                    if (returned.rejectable == false)
                    {
                        if ((returned.net == "0.00") && (returned.net == "0,00"))
                        {
                            returned.rejectable = false;
                        }
                        else
                        {
                            if ((returned.net != "0.00") && (returned.net != "0,00") && (row["Date Paid"].ToString() == "In Process"))
                            {
                                returned.rejectable = true;
                            }
                        }
                    }
                    lines.Add(returned);
                }
                dtable.Clear();
                cn.Close();

            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return lines;
        }

        [HttpPost]
        [Route("GetClaimDiags")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DiagCodes> GetClaimDiags(string claimNo)
        {                        
            List<DiagCodes> diags = new List<DiagCodes>();                 
            SqlCommand cmd = new SqlCommand("", new SqlConnection(_drcConnectionString));            
            try
            {
                cmd.Parameters.Add(new SqlParameter("@claimno", claimNo));
                cmd.CommandText = "SELECT DIAG, DIAGDESC FROM CLAIM_DIAGS WHERE CLAIMNO = @claimno ";
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    DiagCodes diag = new DiagCodes();
                    diag.code = row["DIAG"].ToString();
                    diag.desc = row["DIAGDESC"].ToString();

                    diags.Add(diag);
                }
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                cmd.Connection.Close();
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return diags;
        }

        [HttpPost]
        [Route("GetAdjustOfClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<DetailLinesModel> GetAdjustOfClaim([FromBody] CheckRunDetailsModel data)
        {
            DetailLinesModel returned = new DetailLinesModel();
            List<DetailLinesModel> lines = new List<DetailLinesModel>();
            DataTable dtable = new DataTable();
            DataTable datatable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = _drcConnectionString;
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;


                cmd.CommandText = $"SELECT * FROM [CLAIM_ADJ] WHERE CLAIMNO = '{data.claimNo}'";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    datatable.Load(dr);
                }
                foreach (DataRow row in datatable.Rows)
                {
                    returned = new DetailLinesModel();
                    returned.claimNo = row["CLAIMNO"].ToString();
                    returned.tblRowID = row["TBLROWID"].ToString();
                    returned.adjCode2 = row["ADJCODE"].ToString();
                    returned.comments = row["COMMENTS"].ToString();
                    lines.Add(returned);
                }
                datatable.Clear();
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return lines;
        }

        /*Rejects a claim on claimserach for member and provider*/
        [HttpPost]
        [Route("RejectClaim")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel RejectClaim([FromBody] RejectClaimDetailModel claim)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string code = "";
            char[] notes;
            int indexOfCode;

            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.CommandTimeout = 300;
            cmd.Connection = cn;



            if (claim.reverse == "1")
            {
                cmd.CommandText = $"SELECT ADJCODE FROM " + this.DRCDatabase + $"..CLAIM_ADJUSTS WHERE CLAIMNO = '{claim.claimNo}' AND CLAIMTBLROW = {Convert.ToInt32(claim.tblRowId)} AND CREATEBY = 991";
                using (SqlDataReader rw = cmd.ExecuteReader())
                {
                    dtable.Load(rw);
                }

                foreach (DataRow row in dtable.Rows)
                {
                    code = row["ADJCODE"].ToString();
                }
                dtable.Clear();
            }
            else
            {
                if ((claim.rejectionReason != null) && (claim.reverse != "1"))
                {
                    indexOfCode = claim.rejectionReason.IndexOf('-');
                    notes = claim.rejectionReason.ToCharArray();
                    for (int i = 0; i < notes.Length; i++)
                    {
                        if (i < indexOfCode)
                        {
                            code = code + notes[i];
                        }
                    }
                }
            }
            claim.rejectCode = code;

            try
            {
                if ((claim.notes == null) || (claim.notes == ""))
                {
                    cmd.CommandText = $"EXEC " + this.DRCDatabase + $".dbo.AdjustWebClaim {claim.claimNo}, {claim.tblRowId},'{claim.rejectCode}','{claim.rejectionReason}', {claim.reverse} \n";
                }
                else
                {
                    cmd.CommandText = $"EXEC " + this.DRCDatabase + $".dbo.AdjustWebClaim {claim.claimNo}, {claim.tblRowId},'{claim.rejectCode}','{claim.notes}', {claim.reverse} \n";
                }

                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }


            return vr;
        }

        [HttpPost]
        [Route("ConfirmReversal")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel ConfirmReversal([FromBody] RejectClaimDetailModel claim)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            try
            {

                cn.ConnectionString = _drcConnectionString;

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.CommandText = $"SELECT * FROM WEBCLAIMS_REVERSE WHERE CLAIMNO = '{claim.claimNo}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                cn.Close();
                dtable.Clear();
                if (dtable.Rows.Count > 0)
                {
                    vr.valid = false;
                }
                else
                {
                    vr.valid = true;
                }
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return vr;
        }

        /*refresh claim header table*/
        [HttpPost]
        [Route("RefreshTabel")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel RefreshTabel([FromBody] RejectClaimDetailModel claim)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = _portalConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            string sql = "declare @newNet decimal(18,2)\n"
           + "\n"
           + $"SELECT @newNet = SUM(NET) FROM " + this.PamcPortalDatabase + $".dbo.CLAIM_DETAILS WHERE CLAIMNO = '{claim.claimNo}'\n"
           + "\n"
           + $"UPDATE CLAIMS_HEADER SET [Paid] = @newNet WHERE [Claim No] = '{claim.claimNo}'";

            try
            {
                cmd.Connection = cn;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }





            return vr;

        }

        /*Check if a claim was Rejected*/
        [HttpPost]
        [Route("VerifyRejection")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel VerifyRejection([FromBody] RejectClaimDetailModel claim)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            string result = "";
            cn.ConnectionString = _drcConnectionString;

            if (cn.State != ConnectionState.Open)
            {
                cn.Open();
            }

            cmd.Connection = cn;
            cmd.CommandTimeout = 300;

            try
            {
                string sql = "DECLARE @ISREJECTED bit\n"
       + $"IF EXISTS(SELECT CLAIMNO FROM WEBCLAIMS_REVERSE WHERE CLAIMNO = '{claim.claimNo}' AND TBLROWID = '{claim.tblRowId}')\n"
       + "BEGIN\n"
       + "SELECT @ISREJECTED = 1;\n"
       + "END\n"
       + "ELSE\n"
       + "BEGIN\n"
       + "SELECT @ISREJECTED = 0;\n"
       + "END\n"
       + "SELECT @ISREJECTED AS RESULT;";

                cmd.CommandText = sql;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

                foreach (DataRow row in dt.Rows)
                {
                    result = row["RESULT"].ToString();

                    if (result == "True")
                    {
                        vr.valid = true;
                    }
                    else
                    {
                        vr.valid = false;
                    }
                }
                dt.Clear();
                cn.Close();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }

            return vr;

        }

        /*Will be used to get mebers visit counts*/
        [HttpPost]
        [Route("GetVisitClaims")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public MemberNoModel GetVisitCount([FromBody] MemberNoModel membNo)
        {
            MemberNoModel num = new MemberNoModel();
            //string visitCounts = "";
            DataTable dtable = new DataTable();
            DataTable dt = new DataTable();
            DataTable dtbl = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;
            string prefix = "";

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;
                if (membNo.usertype == "3")
                {
                    // Jaco comment out 2023-09-11 - View is hardcoded to DRC
                    //cmd.CommandText = $"SELECT HPCODE FROM  " + this.PamcPortalDatabase + $"..UserLobCodesHpCodes where username = '{membNo.hpcode}'"; 

                    // Jaco 2023-09-11
                    cmd.CommandText = $" SELECT hp.HPCODE " + //Jaco tested 2023-11-28
                                      $" FROM {PamcPortalDatabase}..Users AS u CROSS JOIN " +
                                      $" {DRCDatabase}..HP_CONTRACTS AS hp " +
                                      $" WHERE hp.LOBCODE IN (SELECT * FROM {DRCDatabase}.dbo.SPLIT_STRING(U.lobcode,',')  ) AND u.username = '{membNo.hpcode}' ";
                }
                else
                {
                    cmd.CommandText = $"SELECT HPCODE FROM  CLAIM_MASTERS where MEMBID LIKE '%{membNo.memberNo}%'";
                }
                cmd.CommandTimeout = 6000;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtbl.Load(dr);
                }
                foreach (DataRow row in dtbl.Rows)
                {
                    membNo.hpcode = row["HPCODE"].ToString();
                }
                string sql = 
                    $" SELECT DISTINCT {ReportingDatabase}..OxygenBenefitOptions.ezHpCode, hp_c.HPNAME, {ReportingDatabase}..OxygenBenefitOptions.MemberPrefix, hp_c.LOBCODE \n" + //Jaco tested 2023-11-28
                    $" FROM {ReportingDatabase}..OxygenBenefitOptions INNER JOIN \n" +
                    $"      {DRCDatabase}..HP_CONTRACTS AS hp_c ON hp_c.HPCODE = {ReportingDatabase}..OxygenBenefitOptions.ezHpCode \n"+
                    $" WHERE ({ReportingDatabase}..OxygenBenefitOptions.Scheme <> 'carecross') AND {ReportingDatabase}..OxygenBenefitOptions.ezHpCode = '{membNo.hpcode}'";
                cmd.CommandText = sql;
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }
                foreach (DataRow row in dt.Rows)
                {
                    prefix = row["MemberPrefix"].ToString();
                }
                dtable.Clear();

                // Jaco comment out 2023-09-27 - Database harcoded in view
                //cmd.CommandText = $" SELECT TOP (1000) [MEMBID] \n" +
                //                  $"      ,[CONSULTATIONS] \n" +
                //                  $" FROM {PamcPortalDatabase}..GP_Consultations \n" +
                //                  $" WHERE MEMBID = '{prefix + membNo.memberNo}'";

                //Jaco 2023-09-27
                cmd.CommandText = " SELECT MEMBID, CONSULTATIONS \n" +                                            //Jaco tested 2023-11-28
                                   " FROM ( SELECT      MEMBID,  SUM(cd.QTY) AS CONSULTATIONS, HPCODE \r\n" +
                                  $"        FROM {DRCDatabase}..CLAIM_MASTERS AS cm INNER JOIN \r\n" +
                                  $"             {DRCDatabase}..CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO INNER JOIN \r\n" +
                                  $"             {DRCDatabase}..PROV_MASTERS pm ON pm.PROVID = cm.PROVID \r\n" +
                                   "        WHERE(YEAR(cd.FROMDATESVC) = YEAR(GETDATE())) \r\n" +
                                  //"            AND(LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011')) \r\n" + //Jaco 2024-07-12
                                  $"            AND 1 = ( \r\n" +
                                  $"                              CASE WHEN HPCODE in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0130', '0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                                  $"                                   WHEN HPCODE not in ('ESSE', 'AGS') and LTRIM(cd.PROCCODE) IN('0146', '0147', '0148', '0190', '0191', '0192', '0199', '56301', '57301', '58301', '561010', '561011') THEN 1 \r\n" +
                                  $"                                   ELSE 0 \r\n" +
                                  $"                               END \r\n" +
                                  $"					   ) \r\n" +
                                   "            AND(cm.SPEC IN('14', '15')) \r\n" +
                                   "            AND cm.CONTRACT = \r\n" +
                                   //"            CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN '2' ELSE cm.CONTRACT END \r\n" + // Jaco 2024-07-25 
                                   "            CASE WHEN HPCODE <> 'AGS' THEN '2' ELSE cm.CONTRACT END \r\n" +
                                   "            AND(cd.NET <> 0) \r\n" +
                                   //"           AND cd.PHCODE = CASE WHEN HPCODE<> 'AFFI' AND HPCODE<> 'AFFD' THEN 'P' ELSE cd.PHCODE END \r\n" + // Jaco 2024-07-25 
                                   "           AND cd.PHCODE = 'P' \r\n" +
                                   "            GROUP BY MEMBID, HPCODE  \r\n" +
                                   "        ) as GP_Consultations \r\n" +
                                  $" WHERE MEMBID = '{prefix + membNo.memberNo}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }
                foreach (DataRow row in dtable.Rows)
                {
                    num.memberNo = row["CONSULTATIONS"].ToString();
                }
                dtable.Clear();
                if (num.memberNo == null)
                {
                    num.memberNo = "0";
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;
            }
            cn.Close();
            return num;

        }

        /*will be used to get AuthNo of Claim*/
        [HttpPost]
        [Route("GetAuthNo")]
        [Authorize(Roles = "NormalUser,Administrator")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel GetAuthNo([FromBody] ValidationResultsModel claimNo)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            DataTable dtable = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = _drcConnectionString;

            try
            {
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                cmd.Connection = cn;

                cmd.CommandText = $"SELECT AUTHNO FROM CLAIM_MASTERS WHERE CLAIMNO = '{claimNo.message}'";

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dtable.Load(dr);
                }

                foreach (DataRow rows in dtable.Rows)
                {
                    vr.message = rows["AUTHNO"].ToString();
                }

                if ((DBNull.Value.Equals(vr.message)) || (vr.message == ""))
                {
                    vr.message = "No Auth";
                }
                cn.Close();
                dtable.Clear();
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                var msg = ex.Message;

            }

            return vr;
        }

        [HttpGet]
        [Route("LoadUserTypes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "Administrator")]
        public List<UsertypeModel> LoadUserTypes()
        {
            DataTable dt = new DataTable();
            List<UsertypeModel> ListOfUserTypes = new List<UsertypeModel>();
            using (SqlConnection Sqlcon = new SqlConnection(_portalConnectionString))
            {
                if (Sqlcon.State != ConnectionState.Open)
                {
                    Sqlcon.Open();

                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    string sqlTextToExecute = "select * from UserType";
                    cmd.Connection = Sqlcon;
                    cmd.CommandText = sqlTextToExecute;

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow dr in dt.Rows)
                    {
                        UsertypeModel type = new UsertypeModel();
                        type.userTypeId = dr["userTypeId"].ToString();
                        type.userType = dr["userType"].ToString();
                        type.createBy = dr["createBy"].ToString();
                        type.createDate = dr["createDate"].ToString();
                        type.changeBy = dr["changeBy"].ToString();
                        type.changeDate = dr["changeDate"].ToString();
                        type.userRegistration = Convert.ToBoolean(dr["userRegistration"]);
                        ListOfUserTypes.Add(type);
                    }
                }
                Sqlcon.Close();
                return ListOfUserTypes;
            }
        }



        /*Method used to log all errors that take place in this controller*/
        [HttpPost]
        [Route("LogError")]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "SECURITY";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\SECURITY\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }

        [HttpPost]
        [Route("LogRemitError")]
        public void LogRemitError(string error, string user, string method, string details)
        {
            string date = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss").Replace("T", " ");


            try
            {
                using (SqlConnection cn = new SqlConnection(_portalConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", date));
                    cmd.Parameters.Add(new SqlParameter("@error", error));
                    cmd.Parameters.Add(new SqlParameter("@user", user));
                    cmd.Parameters.Add(new SqlParameter("@method", method));
                    cmd.Parameters.Add(new SqlParameter("@details", details));

                    cmd.CommandText = $"INSERT INTO {PamcPortalDatabase}.dbo.ErrorLog (CreateDate,username,method,error,details)" +
                        $"VALUES (@date,@user,@method,@error,@details)";

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {


            }


        }

        [HttpPost]
        [Route("BuildRemitZipFileName")]
        private string BuildRemitZipFileName(string date, Boolean file, bool zip)
        {

            char[] splitArr = new char[] { '/', '-' };
            string[] dateParts = date.Split(splitArr);
            string year = dateParts[0];
            string month = dateParts[1];
            string day = dateParts[2];
            string fileType;
            if (file == true)
            {
                fileType = ".txt";
            }
            else if (zip)
            {
                fileType = ".zip";
            }
            else
            {
                fileType = ".pdf";
            }
            //Random i = new Random();

            string fileName = "remit" + "_" + year + month + day + fileType;

            return fileName;
        }
    }
}